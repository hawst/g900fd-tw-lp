.class public Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field _ICheckAppUpgradeCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand$ICheckAppUpgradeValidateCommandData;

.field protected _NewCheckAppUpgradeValue:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

.field private _OldCheckAppUpgradeValue:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand$ICheckAppUpgradeValidateCommandData;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 23
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 19
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_OldCheckAppUpgradeValue:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    .line 20
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_NewCheckAppUpgradeValue:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    .line 24
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_ICheckAppUpgradeCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand$ICheckAppUpgradeValidateCommandData;

    .line 25
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_ICheckAppUpgradeCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand$ICheckAppUpgradeValidateCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand$ICheckAppUpgradeValidateCommandData;->getCheckAppUpgradeResult()Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_OldCheckAppUpgradeValue:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    .line 26
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;Z)V
    .locals 0

    .prologue
    .line 17
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;Z)V
    .locals 0

    .prologue
    .line 17
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->odcUpdateCheck()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;Z)V
    .locals 0

    .prologue
    .line 17
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;Z)V
    .locals 0

    .prologue
    .line 17
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;Z)V
    .locals 0

    .prologue
    .line 17
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;Z)V
    .locals 0

    .prologue
    .line 17
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->onFinalResult(Z)V

    return-void
.end method

.method private odcUpdateCheck()V
    .locals 3

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_NewCheckAppUpgradeValue:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->odcNeedsUpdating()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_ICheckAppUpgradeCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand$ICheckAppUpgradeValidateCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand$ICheckAppUpgradeValidateCommandData;->odcUpdateCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/c;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 73
    :cond_0
    return-void
.end method


# virtual methods
.method protected callCheckAppUpgrade()V
    .locals 3

    .prologue
    .line 43
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_NewCheckAppUpgradeValue:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_ICheckAppUpgradeCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand$ICheckAppUpgradeValidateCommandData;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_NewCheckAppUpgradeValue:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand$ICheckAppUpgradeValidateCommandData;->requestCheckAppUpgradeCommand(Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/b;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 57
    return-void
.end method

.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 0

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->callCheckAppUpgrade()V

    .line 31
    return-void
.end method

.method protected notifyChangedStoreTypeAndRestart()V
    .locals 3

    .prologue
    .line 188
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_ICheckAppUpgradeCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand$ICheckAppUpgradeValidateCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand$ICheckAppUpgradeValidateCommandData;->notifyChangedStoreTypeAndRestart()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/g;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/g;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 195
    return-void
.end method

.method protected notifyCheckAppUpgradeUpdated()V
    .locals 1

    .prologue
    .line 215
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->checkAppUpgradeUpdated()V

    .line 216
    return-void
.end method

.method protected notifyFlextibleTabChanged()V
    .locals 1

    .prologue
    .line 210
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->flexibleTabChanged()V

    .line 211
    return-void
.end method

.method protected notifyLimitedStoreTypeAndRestart()V
    .locals 3

    .prologue
    .line 199
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_ICheckAppUpgradeCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand$ICheckAppUpgradeValidateCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand$ICheckAppUpgradeValidateCommandData;->notifyLimitedStoreTypeAndRestart()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/h;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/h;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 206
    return-void
.end method

.method protected onReceiveCheckAppResult(Z)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 77
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_NewCheckAppUpgradeValue:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->setCheckAppUpgradeResult(Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;)V

    .line 78
    if-nez p1, :cond_0

    .line 80
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->onFinalResult(Z)V

    .line 174
    :goto_0
    return-void

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_NewCheckAppUpgradeValue:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->save()V

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_OldCheckAppUpgradeValue:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    iget v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->flexibleTabClsf:I

    if-eq v0, v4, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_OldCheckAppUpgradeValue:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    iget v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->flexibleTabClsf:I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_NewCheckAppUpgradeValue:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    iget v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->flexibleTabClsf:I

    if-eq v0, v1, :cond_1

    .line 87
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->notifyFlextibleTabChanged()V

    .line 89
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_ICheckAppUpgradeCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand$ICheckAppUpgradeValidateCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand$ICheckAppUpgradeValidateCommandData;->isDontSelftUpdateCondition()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_NewCheckAppUpgradeValue:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->unaNeedsUpdating()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_ICheckAppUpgradeCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand$ICheckAppUpgradeValidateCommandData;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_NewCheckAppUpgradeValue:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->odcNeedsUpdating()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand$ICheckAppUpgradeValidateCommandData;->unaUpdateCommand(Z)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/d;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0

    .line 104
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_ICheckAppUpgradeCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand$ICheckAppUpgradeValidateCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand$ICheckAppUpgradeValidateCommandData;->isDontSelftUpdateCondition()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_NewCheckAppUpgradeValue:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->odcNeedsUpdating()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_ICheckAppUpgradeCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand$ICheckAppUpgradeValidateCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand$ICheckAppUpgradeValidateCommandData;->odcUpdateCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/e;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/e;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0

    .line 116
    :cond_3
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isTestMode()Z

    move-result v0

    if-nez v0, :cond_7

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_ICheckAppUpgradeCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand$ICheckAppUpgradeValidateCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand$ICheckAppUpgradeValidateCommandData;->getSavedClientType()Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;

    move-result-object v0

    .line 119
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_NewCheckAppUpgradeValue:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->getClientType()Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;

    move-result-object v1

    .line 121
    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/i;->a:[I

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 148
    :cond_4
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_ICheckAppUpgradeCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand$ICheckAppUpgradeValidateCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand$ICheckAppUpgradeValidateCommandData;->getOldFreeStoreType()I

    move-result v0

    .line 149
    if-eq v0, v4, :cond_7

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_NewCheckAppUpgradeValue:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    iget v2, v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->freeStoreClsf:I

    if-eq v0, v2, :cond_7

    .line 151
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;->Open:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;

    if-eq v1, v0, :cond_5

    .line 153
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->saveClientType(Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;)V

    .line 155
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_NewCheckAppUpgradeValue:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    iget v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->freeStoreClsf:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->saveStoreType(I)V

    .line 156
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->notifyChangedStoreTypeAndRestart()V

    goto/16 :goto_0

    .line 124
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->notifyChangedStoreTypeAndRestart()V

    goto/16 :goto_0

    .line 127
    :pswitch_2
    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;->UNC:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;

    if-ne v0, v2, :cond_4

    .line 129
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->saveClientType(Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;)V

    .line 130
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->notifyChangedStoreTypeAndRestart()V

    goto/16 :goto_0

    .line 138
    :pswitch_3
    if-eqz v0, :cond_6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;->ODC:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;

    if-ne v0, v2, :cond_4

    .line 140
    :cond_6
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->saveClientType(Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;)V

    .line 141
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->notifyLimitedStoreTypeAndRestart()V

    goto/16 :goto_0

    .line 162
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_ICheckAppUpgradeCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand$ICheckAppUpgradeValidateCommandData;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_NewCheckAppUpgradeValue:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->disclaimerVer:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand$ICheckAppUpgradeValidateCommandData;->validateDisclaimerCommand(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/f;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/f;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_ICheckAppUpgradeCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand$ICheckAppUpgradeValidateCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand$ICheckAppUpgradeValidateCommandData;->notifyCheckAppUpgradeFinished()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 171
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->notifyCheckAppUpgradeUpdated()V

    goto/16 :goto_0

    .line 121
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected saveClientType(Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;)V
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_ICheckAppUpgradeCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand$ICheckAppUpgradeValidateCommandData;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand$ICheckAppUpgradeValidateCommandData;->saveClientType(Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;)V

    .line 179
    return-void
.end method

.method protected saveStoreType(I)V
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand;->_ICheckAppUpgradeCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand$ICheckAppUpgradeValidateCommandData;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand$ICheckAppUpgradeValidateCommandData;->saveStoreType(I)V

    .line 184
    return-void
.end method
