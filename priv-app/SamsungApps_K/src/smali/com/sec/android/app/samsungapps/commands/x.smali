.class final Lcom/sec/android/app/samsungapps/commands/x;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/commands/x;->b:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;

    iput-object p2, p0, Lcom/sec/android/app/samsungapps/commands/x;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;I)V
    .locals 3

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/x;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->setDisclaimerAgreed(Z)V

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/x;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v0

    const-string v1, ""

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->setDisclaimerVersion(Ljava/lang/String;)V

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/x;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v0

    const-string v1, ""

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeLastMCC(Ljava/lang/String;)V

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/x;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v0

    const-string v1, ""

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeLastMNC(Ljava/lang/String;)V

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/x;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->clearCache()V

    .line 106
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/commands/x;->a:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/samsungapps/Main;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 107
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 108
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/commands/x;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 109
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->exitSamsungApps()V

    .line 110
    return-void
.end method
