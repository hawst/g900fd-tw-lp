.class public Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTable;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTable;->b:Ljava/util/ArrayList;

    .line 9
    return-void
.end method


# virtual methods
.method public addCarrierMCCMNC(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTable;->b:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/b;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTable;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 28
    return-void
.end method

.method public matchCondition(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 52
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTable;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTable;->a:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v2, v1

    :goto_0
    if-eqz v2, :cond_3

    .line 56
    :goto_1
    return v0

    .line 52
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTable;->a:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v2, v0

    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 56
    goto :goto_1
.end method

.method public matchCondition(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTable;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/b;

    iget-object v4, v0, Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/b;->a:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/b;->b:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    move v0, v1

    .line 36
    :goto_1
    return v0

    :cond_1
    move v0, v2

    .line 32
    goto :goto_0

    :cond_2
    move v0, v2

    .line 36
    goto :goto_1
.end method

.method public matchCondition(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 74
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTable;->matchCondition(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 82
    :cond_0
    :goto_0
    return v0

    .line 78
    :cond_1
    invoke-virtual {p0, p3}, Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTable;->matchCondition(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 82
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setCarrierCSC(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 22
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTable;->a:Ljava/lang/String;

    .line 23
    return-void
.end method
