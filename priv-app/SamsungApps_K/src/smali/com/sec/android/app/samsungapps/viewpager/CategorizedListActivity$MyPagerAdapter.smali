.class public Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;
.super Landroid/support/v4/app/FragmentPagerAdapter;
.source "ProGuard"


# instance fields
.field a:Landroid/content/Context;

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field d:Ljava/lang/Object;

.field e:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

.field f:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

.field g:Ljava/util/ArrayList;

.field h:Ljava/util/ArrayList;

.field i:Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment$OnActionBarConfigurationListener;

.field j:[I

.field final synthetic k:Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 468
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->k:Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;

    .line 469
    invoke-direct {p0, p3}, Landroid/support/v4/app/FragmentPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 455
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->b:Ljava/lang/String;

    .line 456
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->c:Ljava/lang/String;

    .line 460
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->g:Ljava/util/ArrayList;

    .line 461
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->h:Ljava/util/ArrayList;

    .line 462
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->i:Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment$OnActionBarConfigurationListener;

    .line 464
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->j:[I

    .line 470
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->a:Landroid/content/Context;

    .line 471
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->e:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    .line 472
    iput-object p5, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->c:Ljava/lang/String;

    .line 473
    iput-object p6, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->b:Ljava/lang/String;

    .line 474
    iput-object p8, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->f:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    .line 475
    iput-object p7, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->d:Ljava/lang/Object;

    .line 477
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->a()V

    .line 478
    return-void

    .line 464
    :array_0
    .array-data 4
        0x7f0802b3
        0x7f08029d
        0x7f08029b
        0x7f08029c
    .end array-data
.end method

.method private a()V
    .locals 8

    .prologue
    const v5, 0x7f0802b3

    const v4, 0x7f08029d

    const v3, 0x7f08029c

    const v2, 0x7f08029b

    .line 481
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->h:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 513
    :cond_0
    return-void

    .line 485
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 492
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->f:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    sget-object v1, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->ALL_NEW:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->k:Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isFreeStore()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 493
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->h:Ljava/util/ArrayList;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 494
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->h:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 510
    :goto_0
    const/4 v0, 0x0

    move v6, v0

    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v6, v0, :cond_0

    .line 511
    iget-object v7, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->g:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->e:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->k:Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->g(Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->d:Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->h:Ljava/util/ArrayList;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->newInstance(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 510
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1

    .line 495
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->f:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    sget-object v1, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->ALL_PAID_FREE_NEW:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    if-ne v0, v1, :cond_5

    .line 496
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->h:Ljava/util/ArrayList;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 497
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->k:Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->f(Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 498
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->h:Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 499
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->h:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 504
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->h:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 501
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->h:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 502
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->h:Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 506
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->h:Ljava/util/ArrayList;

    const v1, 0x7f0802f5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 522
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .locals 2

    .prologue
    .line 527
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;

    .line 528
    if-nez p1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->i:Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment$OnActionBarConfigurationListener;

    if-eqz v1, :cond_0

    .line 529
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->i:Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment$OnActionBarConfigurationListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment;->setOnActionBarConfigurationListener(Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment$OnActionBarConfigurationListener;)V

    .line 531
    :cond_0
    return-object v0
.end method

.method public getPageTitle(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 517
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setOnActionBarConfigurationListener(Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment$OnActionBarConfigurationListener;)V
    .locals 0

    .prologue
    .line 535
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->i:Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment$OnActionBarConfigurationListener;

    .line 536
    return-void
.end method
