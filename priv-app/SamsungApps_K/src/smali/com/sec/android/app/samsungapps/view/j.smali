.class final Lcom/sec/android/app/samsungapps/view/j;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/j;->a:Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/16 v2, 0x9

    const/16 v3, 0x8

    const v6, 0x7f0c0192

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 142
    invoke-virtual {p2, v4}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/j;->a:Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "pen_hovering"

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v5, :cond_0

    .line 144
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/j;->a:Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;

    iget-object v1, v0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->mContext:Landroid/content/Context;

    monitor-enter v1

    .line 146
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/j;->a:Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->a(Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;Z)Z

    .line 147
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 148
    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0c0047

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 149
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/j;->a:Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;

    iget-object v2, v1, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/j;->a:Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/view/j;->a:Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;

    invoke-virtual {v3, p1}, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->findItemPosition(Landroid/view/View;)I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-static {v2, p1, v1, v0, v5}, Lcom/sec/android/app/samsungapps/view/SamsungAppsHoveringView;->setBubbleHovering(Landroid/content/Context;Landroid/view/View;Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;Landroid/widget/ScrollView;I)V

    .line 150
    const v0, 0x7f0c019a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 151
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 152
    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 183
    :cond_0
    :goto_0
    return v4

    .line 147
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 154
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/j;->a:Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;

    iget-object v1, v0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->mContext:Landroid/content/Context;

    monitor-enter v1

    .line 156
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/j;->a:Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->a(Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;Z)Z

    .line 157
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 158
    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 157
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    .line 162
    :cond_2
    invoke-virtual {p2, v4}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    if-ne v0, v5, :cond_0

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/j;->a:Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->isEnabledAirView(Landroid/content/ContentResolver;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v2, :cond_3

    .line 165
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/j;->a:Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;

    iget-object v1, v0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->mContext:Landroid/content/Context;

    monitor-enter v1

    .line 166
    :try_start_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/j;->a:Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->a(Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;Z)Z

    .line 167
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 168
    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0c0047

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 169
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/j;->a:Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;

    iget-object v2, v1, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/j;->a:Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/view/j;->a:Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;

    invoke-virtual {v3, p1}, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->findItemPosition(Landroid/view/View;)I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-static {v2, p1, v1, v0, v5}, Lcom/sec/android/app/samsungapps/view/SamsungAppsHoveringView;->setBubbleHovering(Landroid/content/Context;Landroid/view/View;Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;Landroid/widget/ScrollView;I)V

    .line 170
    const v0, 0x7f0c019a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 171
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 172
    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 167
    :catchall_2
    move-exception v0

    monitor-exit v1

    throw v0

    .line 174
    :cond_3
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/j;->a:Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;

    iget-object v1, v0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->mContext:Landroid/content/Context;

    monitor-enter v1

    .line 176
    :try_start_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/j;->a:Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->a(Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;Z)Z

    .line 177
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 178
    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 177
    :catchall_3
    move-exception v0

    monitor-exit v1

    throw v0
.end method
