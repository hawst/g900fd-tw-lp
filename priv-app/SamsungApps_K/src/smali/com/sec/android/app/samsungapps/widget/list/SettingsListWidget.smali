.class public Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;
.super Lcom/sec/android/app/samsungapps/widget/CommonWidget;
.source "ProGuard"


# instance fields
.field a:Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;

.field b:Lcom/sec/android/app/samsungapps/widget/interfaces/ISettingsListWidgetClickListener;

.field d:Landroid/preference/Preference$OnPreferenceChangeListener;

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;)V

    .line 26
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->a:Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;

    .line 27
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/ISettingsListWidgetClickListener;

    .line 28
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->d:Landroid/preference/Preference$OnPreferenceChangeListener;

    .line 30
    const-string v0, "SettingsListWidget"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->e:Ljava/lang/String;

    .line 35
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->a(Landroid/content/Context;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 40
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->a:Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;

    .line 27
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/ISettingsListWidgetClickListener;

    .line 28
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->d:Landroid/preference/Preference$OnPreferenceChangeListener;

    .line 30
    const-string v0, "SettingsListWidget"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->e:Ljava/lang/String;

    .line 41
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->a(Landroid/content/Context;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->a:Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;

    .line 27
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/ISettingsListWidgetClickListener;

    .line 28
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->d:Landroid/preference/Preference$OnPreferenceChangeListener;

    .line 30
    const-string v0, "SettingsListWidget"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->e:Ljava/lang/String;

    .line 47
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->a(Landroid/content/Context;)V

    .line 48
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->mContext:Landroid/content/Context;

    .line 57
    const v0, 0x7f04005e

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->initView(Landroid/content/Context;I)V

    .line 58
    return-void
.end method

.method private a(Landroid/preference/PreferenceScreen;Ljava/lang/String;II)V
    .locals 2

    .prologue
    .line 239
    new-instance v0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;-><init>(Landroid/content/Context;)V

    .line 240
    invoke-virtual {v0, p2}, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->setKey(Ljava/lang/String;)V

    .line 241
    invoke-virtual {v0, p3}, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->setType(I)V

    .line 242
    invoke-virtual {v0, p4}, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->setResourceId(I)V

    .line 243
    new-instance v1, Lcom/sec/android/app/samsungapps/widget/list/an;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/list/an;-><init>(Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 244
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->d:Landroid/preference/Preference$OnPreferenceChangeListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 245
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 246
    return-void
.end method

.method private a(Landroid/preference/PreferenceScreen;)Z
    .locals 2

    .prologue
    .line 273
    const v0, 0x7f0c0132

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 276
    invoke-virtual {p1}, Landroid/preference/PreferenceScreen;->getRootAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 277
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->bind(Landroid/widget/ListView;)V

    .line 279
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public loadWidget()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->a:Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/ISettingsListWidgetClickListener;

    if-nez v0, :cond_2

    .line 75
    :cond_0
    const-string v0, "SettingsListWidget::loadWidget::Not Ready Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 228
    :cond_1
    :goto_0
    return-void

    .line 79
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->a:Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;->getRootScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 80
    if-nez v0, :cond_3

    .line 82
    const-string v0, "SettingsListWidget::loadWidget::Screen is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 86
    :cond_3
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->a(Landroid/preference/PreferenceScreen;)Z

    .line 91
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->a:Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;->hasAutoUpdate()Z

    move-result v1

    if-ne v1, v4, :cond_4

    .line 93
    const-string v1, "AutoUpdate"

    const/16 v2, 0xa

    const v3, 0x7f0400c7

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->a(Landroid/preference/PreferenceScreen;Ljava/lang/String;II)V

    .line 101
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->a:Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;->hasNotifyAppUpdates()Z

    move-result v1

    if-ne v1, v4, :cond_5

    .line 103
    const-string v1, "NotifyAppUpdates"

    const/16 v2, 0xb

    const v3, 0x7f0400c8

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->a(Landroid/preference/PreferenceScreen;Ljava/lang/String;II)V

    .line 111
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->a:Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;->hasNotifyStoreActivities()Z

    move-result v1

    if-ne v1, v4, :cond_6

    .line 113
    const-string v1, "NotifyStoreActivities"

    const/16 v2, 0xc

    const v3, 0x7f0400c9

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->a(Landroid/preference/PreferenceScreen;Ljava/lang/String;II)V

    .line 121
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->a:Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;->hasAdPreference()Z

    move-result v1

    if-ne v1, v4, :cond_7

    .line 123
    const-string v1, "AdPreference"

    const/4 v2, 0x5

    const v3, 0x7f0400c6

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->a(Landroid/preference/PreferenceScreen;Ljava/lang/String;II)V

    .line 131
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->a:Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;->hasPurchaseProtection()Z

    move-result v1

    if-ne v1, v4, :cond_8

    .line 133
    const-string v1, "PurchaseProtection"

    const/16 v2, 0xe

    const v3, 0x7f0400ca

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->a(Landroid/preference/PreferenceScreen;Ljava/lang/String;II)V

    .line 202
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->a:Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;->hasSearchSetting()Z

    move-result v1

    if-ne v1, v4, :cond_9

    .line 204
    const-string v1, "SearchSetting"

    const/16 v2, 0xd

    const v3, 0x7f0400ce

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->a(Landroid/preference/PreferenceScreen;Ljava/lang/String;II)V

    .line 212
    :cond_9
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->a:Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;->hasAccountSetting()Z

    move-result v1

    if-ne v1, v4, :cond_a

    .line 214
    const-string v1, "SamsungAcc"

    const v2, 0x7f0400cd

    invoke-direct {p0, v0, v1, v4, v2}, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->a(Landroid/preference/PreferenceScreen;Ljava/lang/String;II)V

    .line 222
    :cond_a
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->a:Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;->hasAbout()Z

    move-result v1

    if-ne v1, v4, :cond_1

    .line 224
    const-string v1, "About"

    const/16 v2, 0x9

    const v3, 0x7f0400c5

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->a(Landroid/preference/PreferenceScreen;Ljava/lang/String;II)V

    goto/16 :goto_0
.end method

.method public onViewUpdate()V
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->a:Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;

    if-nez v0, :cond_0

    .line 320
    const-string v0, "SettingsListWidget::onViewUpdate::mWidgetHelper is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 332
    :goto_0
    return-void

    .line 324
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->a:Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;->getRootScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 325
    if-nez v0, :cond_1

    .line 327
    const-string v0, "SettingsListWidget::onViewUpdate::Screen is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 331
    :cond_1
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->a(Landroid/preference/PreferenceScreen;)Z

    goto :goto_0
.end method

.method public refreshWidget()V
    .locals 0

    .prologue
    .line 292
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 63
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->a:Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;

    .line 64
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/ISettingsListWidgetClickListener;

    .line 65
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->d:Landroid/preference/Preference$OnPreferenceChangeListener;

    .line 67
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->release()V

    .line 68
    return-void
.end method

.method public setWidgetClickListener(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 300
    check-cast p1, Lcom/sec/android/app/samsungapps/widget/interfaces/ISettingsListWidgetClickListener;

    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/ISettingsListWidgetClickListener;

    .line 301
    return-void
.end method

.method public setWidgetData(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 315
    check-cast p1, Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;

    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->a:Lcom/sec/android/app/samsungapps/widget/SettingsListWidgetHelper;

    .line 316
    return-void
.end method

.method public setWidgetPreferenceChangedListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V
    .locals 0

    .prologue
    .line 309
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->d:Landroid/preference/Preference$OnPreferenceChangeListener;

    .line 310
    return-void
.end method

.method public updateWidget()V
    .locals 0

    .prologue
    .line 286
    return-void
.end method
