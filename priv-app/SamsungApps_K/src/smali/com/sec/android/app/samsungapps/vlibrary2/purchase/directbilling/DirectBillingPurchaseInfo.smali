.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private mConfirmResult:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmResult;

.field private mInitResult:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;->mInitResult:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;

    .line 15
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;->mConfirmResult:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmResult;

    .line 19
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;->mInitResult:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;

    .line 20
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmResult;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmResult;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;->mConfirmResult:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmResult;

    .line 21
    return-void
.end method


# virtual methods
.method public getConfirmResult()Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;->mConfirmResult:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmResult;

    return-object v0
.end method

.method public getInitResult()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;->mInitResult:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;

    return-object v0
.end method

.method public getOptUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;->mInitResult:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;->getOptUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOrderID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;->mInitResult:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;->getOrderID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPaymentID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;->mInitResult:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;->getPaymentID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getResponseTime()J
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;->mInitResult:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;->getResponseTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public getRetryCount()I
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;->mInitResult:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;->getRetryCount()I

    move-result v0

    return v0
.end method

.method public isFinalMapSuccess()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 64
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;->mConfirmResult:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmResult;

    const-string v3, "successYn"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmResult;->findString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 67
    if-eqz v2, :cond_0

    const-string v3, "1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 78
    :cond_1
    :goto_0
    return v0

    .line 72
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;->mConfirmResult:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmResult;

    const-string v3, "downloadUri"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmResult;->findString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 73
    if-eqz v2, :cond_3

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-ne v2, v0, :cond_1

    :cond_3
    move v0, v1

    .line 75
    goto :goto_0
.end method
