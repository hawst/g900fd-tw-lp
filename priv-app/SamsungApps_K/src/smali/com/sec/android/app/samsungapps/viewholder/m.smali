.class final Lcom/sec/android/app/samsungapps/viewholder/m;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;)V
    .locals 0

    .prologue
    .line 307
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/viewholder/m;->a:Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 311
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/m;->a:Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/m;->a:Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Context:Landroid/content/Context;

    const v2, 0x7f08015a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 312
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/m;->a:Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->isKNOXApp()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/m;->a:Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Context:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getKnoxAPI()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/m;->a:Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Context:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewholder/m;->a:Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getGUID()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;->launch(Landroid/content/Context;Ljava/lang/String;)Z

    .line 326
    :goto_0
    return-void

    .line 315
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/m;->a:Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/m;->a:Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Context:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewholder/m;->a:Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getGUID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->launch(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_0

    .line 318
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/m;->a:Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->a(Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 319
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/m;->a:Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/m;->a:Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->downloadContent(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    goto :goto_0

    .line 321
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/m;->a:Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Context:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/m;->a:Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getGUID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->cancelDownload(Ljava/lang/String;)Z

    .line 322
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/m;->a:Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->btnRightly:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 323
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/m;->a:Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->btnRightly:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/m;->a:Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/viewholder/PurchasedListViewHolder;->_Context:Landroid/content/Context;

    const v2, 0x7f0801e4

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
