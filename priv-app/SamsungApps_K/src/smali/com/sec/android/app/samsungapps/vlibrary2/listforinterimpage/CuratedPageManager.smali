.class public Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private mContext:Landroid/content/Context;

.field mHandler:Landroid/os/Handler;

.field private mObserver:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;

.field private mPageInfo:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

.field mState:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;

.field p:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

.field private productSetId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mState:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;

    .line 40
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mPageInfo:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    .line 192
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mHandler:Landroid/os/Handler;

    .line 361
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->p:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    .line 44
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mContext:Landroid/content/Context;

    .line 45
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->productSetId:Ljava/lang/String;

    .line 46
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;

    .line 47
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;Z)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->sendRequest(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;)Z
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->parseCachedXml()Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->onCacheLoadSuccess()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->onCahceLoadFailed()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;)Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mPageInfo:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;)Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mPageInfo:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;)Ljava/io/File;
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->createFile()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;Ljava/io/File;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->save(Ljava/io/File;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->onReceiveSuccess()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->onReceiveFailed()V

    return-void
.end method

.method private createFile()Ljava/io/File;
    .locals 3

    .prologue
    .line 315
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mContext:Landroid/content/Context;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->getProductSetId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "output.xml"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/FileCreator;->internalStorage(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 316
    return-object v0
.end method

.method private entry()V
    .locals 2

    .prologue
    .line 62
    const-string v0, "CuratedPageManagerStateMachine"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mState:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/h;->a:[I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mState:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 111
    :goto_0
    :pswitch_0
    return-void

    .line 68
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->showLoading()V

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/a;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 81
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->showLoadingFailed()V

    goto :goto_0

    .line 84
    :pswitch_3
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->showLoadingSuccess()V

    goto :goto_0

    .line 87
    :pswitch_4
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->fileExist()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;->CHACHE_LOADING:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->tran(Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;)V

    goto :goto_0

    .line 93
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;->LOADING:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->tran(Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;)V

    goto :goto_0

    .line 97
    :pswitch_5
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->showLoading()V

    .line 98
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->loadCache()V

    goto :goto_0

    .line 101
    :pswitch_6
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->showMoreLoading()V

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/b;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 63
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private exit()V
    .locals 1

    .prologue
    .line 219
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/h;->a:[I

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mState:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;->ordinal()I

    .line 222
    return-void
.end method

.method private fileExist()Z
    .locals 1

    .prologue
    .line 321
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->createFile()Ljava/io/File;

    move-result-object v0

    .line 322
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    return v0
.end method

.method private isValid(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 265
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private loadCache()V
    .locals 2

    .prologue
    .line 115
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/c;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 141
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 142
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 143
    return-void
.end method

.method private loadXml(Ljava/io/File;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 327
    const-string v3, ""

    .line 328
    const/4 v1, 0x0

    .line 330
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 331
    :try_start_1
    new-instance v1, Ljava/io/ObjectInputStream;

    invoke-direct {v1, v2}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 332
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    .line 333
    :try_start_2
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    .line 341
    :goto_0
    if-eqz v2, :cond_0

    .line 344
    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 349
    :cond_0
    :goto_1
    return-object v0

    .line 334
    :catch_0
    move-exception v0

    move-object v2, v3

    :goto_2
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v0, v2

    move-object v2, v1

    .line 340
    goto :goto_0

    .line 337
    :catch_1
    move-exception v0

    move-object v2, v1

    :goto_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v3

    goto :goto_0

    .line 345
    :catch_2
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 337
    :catch_3
    move-exception v0

    goto :goto_3

    :catch_4
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    goto :goto_3

    .line 334
    :catch_5
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    goto :goto_2

    :catch_6
    move-exception v1

    move-object v4, v1

    move-object v1, v2

    move-object v2, v0

    move-object v0, v4

    goto :goto_2
.end method

.method private onCacheLoadSuccess()V
    .locals 2

    .prologue
    .line 146
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/h;->a:[I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mState:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 159
    :goto_0
    return-void

    .line 149
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mPageInfo:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mPageInfo:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 150
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->showLoadingSuccess()V

    .line 152
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/f;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/f;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 146
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch
.end method

.method private onCahceLoadFailed()V
    .locals 2

    .prologue
    .line 174
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/h;->a:[I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mState:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 178
    :goto_0
    return-void

    .line 177
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;->LOADING:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->tran(Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;)V

    goto :goto_0

    .line 174
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch
.end method

.method private onReceiveFailed()V
    .locals 2

    .prologue
    .line 431
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/h;->a:[I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mState:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    sparse-switch v0, :sswitch_data_0

    .line 438
    :goto_0
    :sswitch_0
    return-void

    .line 437
    :sswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;->LOAD_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->tran(Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;)V

    goto :goto_0

    .line 431
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x7 -> :sswitch_1
    .end sparse-switch
.end method

.method private onReceiveSuccess()V
    .locals 2

    .prologue
    .line 413
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/h;->a:[I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mState:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 421
    :goto_0
    :pswitch_0
    return-void

    .line 420
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;->LOAD_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->tran(Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;)V

    goto :goto_0

    .line 413
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private parseCachedXml()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 294
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;-><init>()V

    .line 295
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLParser;

    invoke-direct {v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLParser;-><init>()V

    .line 297
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->createFile()Ljava/io/File;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->loadXml(Ljava/io/File;)Ljava/lang/String;

    move-result-object v3

    .line 298
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    .line 310
    :cond_0
    :goto_0
    return v0

    .line 301
    :cond_1
    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLParser;->parseXML(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;

    move-result-object v2

    .line 302
    if-eqz v2, :cond_0

    .line 303
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfoGenerator;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfoGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;)V

    .line 305
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mPageInfo:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    .line 306
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;

    invoke-direct {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 307
    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/MapContainerGenerator;->onReceiveParsingResult(Lcom/sec/android/app/samsungapps/vlibrary/xml/result/IResponseParseResult;)V

    .line 308
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private save(Ljava/io/File;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 270
    const/4 v2, 0x0

    .line 272
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 273
    :try_start_1
    new-instance v0, Ljava/io/ObjectOutputStream;

    invoke-direct {v0, v1}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 274
    invoke-virtual {v0, p2}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 275
    invoke-virtual {v0}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    .line 283
    :goto_0
    if-eqz v1, :cond_0

    .line 286
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 291
    :cond_0
    :goto_1
    return-void

    .line 276
    :catch_0
    move-exception v0

    move-object v1, v2

    :goto_2
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 279
    :catch_1
    move-exception v0

    move-object v1, v2

    :goto_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 287
    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 279
    :catch_3
    move-exception v0

    goto :goto_3

    .line 276
    :catch_4
    move-exception v0

    goto :goto_2
.end method

.method private sendRequest(Z)V
    .locals 7

    .prologue
    .line 364
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mPageInfo:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    if-nez v0, :cond_0

    .line 365
    const-string v0, "mPageInfo is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 409
    :goto_0
    return-void

    .line 369
    :cond_0
    new-instance v6, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    invoke-direct {v6}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;-><init>()V

    .line 370
    new-instance v4, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfoGenerator;

    invoke-direct {v4, v6}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfoGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;)V

    .line 372
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mPageInfo:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->getNextStartNumber()I

    move-result v1

    .line 373
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mPageInfo:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;->getNextEndNumber()I

    move-result v2

    .line 374
    if-eqz p1, :cond_1

    .line 375
    const/4 v1, 0x1

    .line 376
    const/16 v2, 0xf

    .line 379
    :cond_1
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->getProductSetId()Ljava/lang/String;

    move-result-object v3

    new-instance v5, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/g;

    invoke-direct {v5, p0, v6, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/g;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;Z)V

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->curatedProductSetList2Notc(IILjava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->p:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    .line 408
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->p:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    goto :goto_0
.end method

.method private showLoading()V
    .locals 2

    .prologue
    .line 207
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;->STATE_LOADING:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;->setState(Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;)V

    .line 210
    :cond_0
    return-void
.end method

.method private showLoadingFailed()V
    .locals 2

    .prologue
    .line 201
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;->STATE_LOADFAIL:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;->setState(Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;)V

    .line 204
    :cond_0
    return-void
.end method

.method private showLoadingSuccess()V
    .locals 2

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;->STATE_LOADCOMPLETE:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;->setState(Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;)V

    .line 198
    :cond_0
    return-void
.end method

.method private showMoreLoading()V
    .locals 2

    .prologue
    .line 213
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;

    if-eqz v0, :cond_0

    .line 214
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;->STATE_LOADINGMORE:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;->setState(Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;)V

    .line 216
    :cond_0
    return-void
.end method

.method private tran(Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->exit()V

    .line 57
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mState:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;

    .line 58
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->entry()V

    .line 59
    return-void
.end method


# virtual methods
.method public getCuratedPageInfo()Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mPageInfo:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageInfo;

    return-object v0
.end method

.method public getProductSetId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->productSetId:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->isValid(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 356
    const-string v0, "APPSFORGALAXY"

    .line 358
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->productSetId:Ljava/lang/String;

    goto :goto_0
.end method

.method public isLoading()Z
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mState:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;->LOAD_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mState:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;->LOAD_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load()V
    .locals 2

    .prologue
    .line 234
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/h;->a:[I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mState:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 243
    :goto_0
    :pswitch_0
    return-void

    .line 237
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;->CHECK_CACHE:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->tran(Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;)V

    goto :goto_0

    .line 242
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;->LOADING:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->tran(Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;)V

    goto :goto_0

    .line 234
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public release()V
    .locals 0

    .prologue
    .line 449
    return-void
.end method

.method public requestMore()V
    .locals 2

    .prologue
    .line 257
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->mState:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;->MORE_LOADING:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;

    if-eq v0, v1, :cond_0

    .line 259
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;->MORE_LOADING:Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager;->tran(Lcom/sec/android/app/samsungapps/vlibrary2/listforinterimpage/CuratedPageManager$State;)V

    .line 261
    :cond_0
    return-void
.end method
