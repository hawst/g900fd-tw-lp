.class final Lcom/sec/android/app/samsungapps/vlibrary2/comment/k;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;)V
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/k;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceiveResult(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/k;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->mICommentModifyView:Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$ICommentModifyView;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->access$300(Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$ICommentModifyView;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$ICommentModifyView;->onCommentModifyResult(Z)V

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/k;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->onFinalResult(Z)V
    invoke-static {v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->access$600(Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;Z)V

    .line 130
    if-eqz p2, :cond_0

    .line 132
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/k;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->mIModifyCommentCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$IModifyCommentCommandData;
    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$IModifyCommentCommandData;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$IModifyCommentCommandData;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->notifyCommentChanged(Ljava/lang/String;)V

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/k;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->mIModifyCommentCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$IModifyCommentCommandData;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$IModifyCommentCommandData;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/k;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->mCommentError:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;
    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->access$700(Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;->getProhibitWords()[Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$IModifyCommentCommandData;->setProhibitWords([Ljava/lang/String;)V

    .line 136
    return-void
.end method
