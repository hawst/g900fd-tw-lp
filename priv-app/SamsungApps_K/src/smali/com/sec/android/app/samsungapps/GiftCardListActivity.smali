.class public Lcom/sec/android/app/samsungapps/GiftCardListActivity;
.super Lcom/sec/android/app/samsungapps/ActionBarActivity;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/widget/interfaces/GetGiftCardCountObserver;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/view/GiftCardListView;

.field protected mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 21
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;-><init>()V

    .line 23
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardListActivity;->mContext:Landroid/content/Context;

    .line 24
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardListActivity;->a:Lcom/sec/android/app/samsungapps/view/GiftCardListView;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/GiftCardListActivity;)Lcom/sec/android/app/samsungapps/view/GiftCardListView;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardListActivity;->a:Lcom/sec/android/app/samsungapps/view/GiftCardListView;

    return-object v0
.end method


# virtual methods
.method protected displayData()V
    .locals 6

    .prologue
    .line 31
    const v0, 0x7f040086

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/GiftCardListActivity;->setMainView(I)V

    .line 35
    new-instance v2, Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-direct {v2}, Lcom/sec/android/app/samsungapps/view/ViewList;-><init>()V

    .line 36
    new-instance v3, Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-direct {v3}, Lcom/sec/android/app/samsungapps/view/ViewList;-><init>()V

    .line 37
    new-instance v4, Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-direct {v4}, Lcom/sec/android/app/samsungapps/view/ViewList;-><init>()V

    .line 39
    const v0, 0x7f0c0080

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/GiftCardListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/view/ViewList;->add(Ljava/lang/Object;)Z

    .line 40
    const v0, 0x7f0c007f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/GiftCardListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/sec/android/app/samsungapps/view/ViewList;->add(Ljava/lang/Object;)Z

    .line 41
    const v0, 0x7f0c0084

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/GiftCardListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/sec/android/app/samsungapps/view/ViewList;->add(Ljava/lang/Object;)Z

    .line 43
    const v0, 0x7f0c0169

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/GiftCardListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 44
    const v1, 0x7f0c0148

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/GiftCardListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    .line 85
    :cond_0
    :goto_0
    return-void

    .line 47
    :cond_1
    const v1, 0x7f0c0096

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 48
    const v1, 0x7f0c01f1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/GiftCardListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 49
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 52
    iget-object v5, p0, Lcom/sec/android/app/samsungapps/GiftCardListActivity;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getEmailID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/GiftCardListActivity;->mContext:Landroid/content/Context;

    const v5, 0x7f080237

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 54
    new-instance v1, Lcom/sec/android/app/samsungapps/ee;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/ee;-><init>(Lcom/sec/android/app/samsungapps/GiftCardListActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    const v0, 0x7f0c01f2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/GiftCardListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/view/GiftCardListView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardListActivity;->a:Lcom/sec/android/app/samsungapps/view/GiftCardListView;

    .line 66
    const v0, 0x7f0c0086

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/GiftCardListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 67
    new-instance v1, Lcom/sec/android/app/samsungapps/ef;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/ef;-><init>(Lcom/sec/android/app/samsungapps/GiftCardListActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardListActivity;->a:Lcom/sec/android/app/samsungapps/view/GiftCardListView;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardListActivity;->a:Lcom/sec/android/app/samsungapps/view/GiftCardListView;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->setContext(Landroid/content/Context;)V

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardListActivity;->a:Lcom/sec/android/app/samsungapps/view/GiftCardListView;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->setObserver(Lcom/sec/android/app/samsungapps/widget/interfaces/GetGiftCardCountObserver;)V

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardListActivity;->a:Lcom/sec/android/app/samsungapps/view/GiftCardListView;

    invoke-virtual {v0, v2, v3, v4}, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->setEmptyViews(Lcom/sec/android/app/samsungapps/view/ViewList;Lcom/sec/android/app/samsungapps/view/ViewList;Lcom/sec/android/app/samsungapps/view/ViewList;)V

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardListActivity;->a:Lcom/sec/android/app/samsungapps/view/GiftCardListView;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->refresh()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 90
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 92
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/GiftCardListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080187

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    new-array v4, v6, [I

    const v5, 0xb0003

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/GiftCardListActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    .line 94
    iput-object p0, p0, Lcom/sec/android/app/samsungapps/GiftCardListActivity;->mContext:Landroid/content/Context;

    .line 96
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;-><init>()V

    .line 97
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/GiftCardListActivity;->checkLoginState()Z

    move-result v1

    if-nez v1, :cond_1

    .line 98
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;->createForceLogin()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/eg;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/eg;-><init>(Lcom/sec/android/app/samsungapps/GiftCardListActivity;)V

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 121
    :cond_0
    :goto_0
    return-void

    .line 116
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/GiftCardListActivity;->displayData()V

    .line 117
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/GiftCardListActivity;->checkLoginState()Z

    move-result v0

    if-nez v0, :cond_0

    .line 118
    invoke-static {p0, v6}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardListActivity;->a:Lcom/sec/android/app/samsungapps/view/GiftCardListView;

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardListActivity;->a:Lcom/sec/android/app/samsungapps/view/GiftCardListView;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/GiftCardListView;->release()V

    .line 137
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onDestroy()V

    .line 138
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 126
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onResume()V

    .line 127
    return-void
.end method

.method public onSamsungAppsCreateOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;)Z
    .locals 1

    .prologue
    .line 143
    const/4 v0, 0x0

    return v0
.end method

.method public showGiftCardNotiArea()V
    .locals 3

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardListActivity;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 164
    :cond_0
    :goto_0
    return-void

    .line 153
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardListActivity;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isNotUsingGooglePlayCountry()Z

    move-result v0

    if-nez v0, :cond_0

    .line 158
    const v0, 0x7f0c01f0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/GiftCardListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 159
    if-eqz v0, :cond_0

    .line 161
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/GiftCardListActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08016d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 162
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
