.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/mock/MockSMSSender;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ISMSSender;


# instance fields
.field private _DstMessage:Ljava/lang/String;

.field private _DstNumber:Ljava/lang/String;

.field private _Receiver:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

.field public _bCalledSendSMS:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/mock/MockSMSSender;->_bCalledSendSMS:Z

    return-void
.end method


# virtual methods
.method public getDstMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/mock/MockSMSSender;->_DstMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getDstNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/mock/MockSMSSender;->_DstNumber:Ljava/lang/String;

    return-object v0
.end method

.method public notifySMSResult(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 31
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/mock/MockSMSSender;->_Receiver:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    invoke-interface {v0, v1, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;->onReceiveResult(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V

    .line 32
    return-void
.end method

.method public sendSMS(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V
    .locals 1

    .prologue
    .line 13
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/mock/MockSMSSender;->_bCalledSendSMS:Z

    .line 14
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/mock/MockSMSSender;->_DstNumber:Ljava/lang/String;

    .line 15
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/mock/MockSMSSender;->_DstMessage:Ljava/lang/String;

    .line 16
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/mock/MockSMSSender;->_Receiver:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    .line 17
    return-void
.end method
