.class public final enum Lcom/sec/android/app/samsungapps/uieventmanager/MainTabEvent$MainTabEventType;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum ShowFeaturedTab:Lcom/sec/android/app/samsungapps/uieventmanager/MainTabEvent$MainTabEventType;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/uieventmanager/MainTabEvent$MainTabEventType;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 6
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/MainTabEvent$MainTabEventType;

    const-string v1, "ShowFeaturedTab"

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/uieventmanager/MainTabEvent$MainTabEventType;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/MainTabEvent$MainTabEventType;->ShowFeaturedTab:Lcom/sec/android/app/samsungapps/uieventmanager/MainTabEvent$MainTabEventType;

    .line 4
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/uieventmanager/MainTabEvent$MainTabEventType;

    const/4 v1, 0x0

    sget-object v2, Lcom/sec/android/app/samsungapps/uieventmanager/MainTabEvent$MainTabEventType;->ShowFeaturedTab:Lcom/sec/android/app/samsungapps/uieventmanager/MainTabEvent$MainTabEventType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/MainTabEvent$MainTabEventType;->a:[Lcom/sec/android/app/samsungapps/uieventmanager/MainTabEvent$MainTabEventType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 4
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/uieventmanager/MainTabEvent$MainTabEventType;
    .locals 1

    .prologue
    .line 4
    const-class v0, Lcom/sec/android/app/samsungapps/uieventmanager/MainTabEvent$MainTabEventType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/uieventmanager/MainTabEvent$MainTabEventType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/uieventmanager/MainTabEvent$MainTabEventType;
    .locals 1

    .prologue
    .line 4
    sget-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/MainTabEvent$MainTabEventType;->a:[Lcom/sec/android/app/samsungapps/uieventmanager/MainTabEvent$MainTabEventType;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/uieventmanager/MainTabEvent$MainTabEventType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/uieventmanager/MainTabEvent$MainTabEventType;

    return-object v0
.end method
