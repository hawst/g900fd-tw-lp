.class public Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionValidator;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _PermissionActivity:Ljava/lang/Class;

.field private _PermissionData:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;Ljava/lang/Class;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionValidator;->_PermissionData:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

    .line 15
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionValidator;->_PermissionActivity:Ljava/lang/Class;

    .line 16
    return-void
.end method


# virtual methods
.method public getPermissionData()Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionValidator;->_PermissionData:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

    return-object v0
.end method

.method public impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 20
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionValidator;->_PermissionData:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionValidator;->_PermissionData:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-nez v0, :cond_1

    .line 22
    :cond_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionValidator;->onFinalResult(Z)V

    .line 39
    :goto_0
    return-void

    .line 25
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionValidator;->_PermissionData:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;->existPermission()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 28
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionValidator;->_Context:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionValidator;->invokePermissionUI(Landroid/content/Context;)V

    goto :goto_0

    .line 32
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionValidator;->isCanceled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 34
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionValidator;->onFinalResult(Z)V

    .line 37
    :cond_3
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionValidator;->onFinalResult(Z)V

    goto :goto_0
.end method

.method protected invokePermissionUI(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionValidator;->_PermissionActivity:Ljava/lang/Class;

    invoke-static {p1, v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->startActivityWithObject(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/Object;)V

    .line 50
    return-void
.end method

.method public userAgreed(Z)V
    .locals 0

    .prologue
    .line 44
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionValidator;->onFinalResult(Z)V

    .line 45
    return-void
.end method
