.class public interface abstract Lcom/sec/android/app/samsungapps/widget/interfaces/ICommentDetailWidgetData;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract getOldComment()Ljava/lang/String;
.end method

.method public abstract getOldRating()D
.end method

.method public abstract getProhibitWords()[Ljava/lang/String;
.end method

.method public abstract isModifyComment()Z
.end method

.method public abstract isMyReviewDuplicated()Z
.end method

.method public abstract sendRequest(Ljava/lang/String;ILcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
.end method
