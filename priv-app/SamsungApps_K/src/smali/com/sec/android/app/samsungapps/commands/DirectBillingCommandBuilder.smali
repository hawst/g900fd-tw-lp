.class public Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;->a:Landroid/content/Context;

    .line 28
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;

    .line 29
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    .line 34
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;->d:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;

    .line 35
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;->a:Landroid/content/Context;

    .line 36
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;

    .line 37
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    .line 38
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    return-object v0
.end method


# virtual methods
.method protected confirm()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 3

    .prologue
    .line 124
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;->d:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;)V

    return-object v0
.end method

.method protected confirmMsg()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 2

    .prologue
    .line 69
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/DelayedNotiCommand;

    new-instance v1, Lcom/sec/android/app/samsungapps/commands/ba;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/commands/ba;-><init>(Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;)V

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/commands/DelayedNotiCommand;-><init>(Lcom/sec/android/app/samsungapps/commands/DelayedNotiCommand$IDelayedNotiCommandInfo;)V

    return-object v0
.end method

.method public directBillingPayment()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 2

    .prologue
    .line 46
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;-><init>()V

    .line 54
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;->init()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;->addNext(Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V

    .line 55
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;->confirmMsg()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;->addNext(Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V

    .line 56
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;->urlDelivery()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;->addNext(Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V

    .line 57
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;->confirm()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;->addNext(Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V

    .line 59
    return-object v0
.end method

.method protected init()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 4

    .prologue
    .line 64
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitCommand;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;->d:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;->c:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;)V

    return-object v0
.end method

.method protected urlDelivery()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 3

    .prologue
    .line 119
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingUrlDeliveryCommand;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;->d:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingUrlDeliveryCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;)V

    return-object v0
.end method
