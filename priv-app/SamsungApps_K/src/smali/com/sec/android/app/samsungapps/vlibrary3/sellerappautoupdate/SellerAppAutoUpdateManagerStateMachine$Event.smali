.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum DL_FAILED_AND_EMPTY:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

.field public static final enum DL_FAILED_AND_NOT_EMPTY:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

.field public static final enum DL_SUCCESS_AND_EMPTY:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

.field public static final enum DL_SUCCESS_AND_NOT_EMPTY:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

.field public static final enum EMPTY_QUEUE:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

.field public static final enum EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

.field public static final enum LOGIN_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

.field public static final enum LOGIN_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

.field public static final enum NOT_SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

.field public static final enum REQUEST_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

.field public static final enum REQUEST_SUCCESS_EMPTY_QUEUE:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

.field public static final enum REQUEST_SUCCESS_NOT_EMPTY_QUEUE:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

.field public static final enum SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

.field public static final enum USER_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 22
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    const-string v1, "EXECUTE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    const-string v1, "SYSTEM_APP"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    const-string v1, "NOT_SYSTEM_APP"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->NOT_SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    const-string v1, "LOGIN_SUCCESS"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->LOGIN_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    const-string v1, "LOGIN_FAILED"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->LOGIN_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    const-string v1, "REQUEST_FAILED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->REQUEST_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    const-string v1, "REQUEST_SUCCESS_EMPTY_QUEUE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->REQUEST_SUCCESS_EMPTY_QUEUE:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    const-string v1, "REQUEST_SUCCESS_NOT_EMPTY_QUEUE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->REQUEST_SUCCESS_NOT_EMPTY_QUEUE:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    const-string v1, "EMPTY_QUEUE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->EMPTY_QUEUE:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    const-string v1, "DL_SUCCESS_AND_EMPTY"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->DL_SUCCESS_AND_EMPTY:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    const-string v1, "DL_SUCCESS_AND_NOT_EMPTY"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->DL_SUCCESS_AND_NOT_EMPTY:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    const-string v1, "DL_FAILED_AND_NOT_EMPTY"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->DL_FAILED_AND_NOT_EMPTY:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    const-string v1, "DL_FAILED_AND_EMPTY"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->DL_FAILED_AND_EMPTY:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    const-string v1, "USER_CANCEL"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->USER_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    .line 20
    const/16 v0, 0xe

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->NOT_SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->LOGIN_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->LOGIN_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->REQUEST_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->REQUEST_SUCCESS_EMPTY_QUEUE:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->REQUEST_SUCCESS_NOT_EMPTY_QUEUE:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->EMPTY_QUEUE:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->DL_SUCCESS_AND_EMPTY:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->DL_SUCCESS_AND_NOT_EMPTY:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->DL_FAILED_AND_NOT_EMPTY:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->DL_FAILED_AND_EMPTY:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->USER_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManagerStateMachine$Event;

    return-object v0
.end method
