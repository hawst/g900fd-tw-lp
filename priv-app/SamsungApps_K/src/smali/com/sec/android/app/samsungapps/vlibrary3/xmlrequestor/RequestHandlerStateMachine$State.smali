.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;

.field public static final enum IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;

.field public static final enum REQUEST:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;

.field public static final enum SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 11
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;

    const-string v1, "REQUEST"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;->REQUEST:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;

    .line 9
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;->REQUEST:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/RequestHandlerStateMachine$State;

    return-object v0
.end method
