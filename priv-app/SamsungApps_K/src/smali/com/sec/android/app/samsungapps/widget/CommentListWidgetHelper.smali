.class public Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/widget/interfaces/ICommentListWidgetData;


# static fields
.field public static final REQUEST_TYPE_COMMENT_DELETE:I = 0x8

.field public static final REQUEST_TYPE_COMMENT_EXPERT_LIST:I = 0x1

.field public static final REQUEST_TYPE_COMMENT_MODIFY:I = 0x4

.field public static final REQUEST_TYPE_COMMENT_USER_LIST:I = 0x2


# instance fields
.field a:Landroid/content/Context;

.field b:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

.field c:Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper$IDataLoadingObserver;

.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->a:Landroid/content/Context;

    .line 16
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->b:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    .line 17
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->c:Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper$IDataLoadingObserver;

    .line 19
    const-string v0, "CommentListWidgetHelper"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->d:Ljava/lang/String;

    .line 37
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->a:Landroid/content/Context;

    .line 38
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    invoke-direct {v0, p2, p3}, Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->b:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    .line 39
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 159
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->a:Landroid/content/Context;

    .line 160
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->c:Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper$IDataLoadingObserver;

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->b:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->b:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;->getCommentList()Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->clear()V

    .line 165
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->b:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    .line 167
    :cond_0
    return-void
.end method

.method public clearCommentList()V
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->b:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    if-eqz v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->b:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;->getCommentList()Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->clear()V

    .line 178
    :cond_0
    return-void
.end method

.method public getComment(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;
    .locals 2

    .prologue
    .line 230
    const/4 v0, 0x0

    .line 232
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->b:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    if-nez v1, :cond_0

    .line 234
    const-string v1, "CommentListWidgetHelper::getComment::Command builder is null"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 241
    :goto_0
    return-object v0

    .line 238
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->b:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;->getCommentList()Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;

    goto :goto_0
.end method

.method public getCommentCount()I
    .locals 2

    .prologue
    .line 187
    const/4 v0, 0x0

    .line 189
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->b:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    if-nez v1, :cond_0

    .line 191
    const-string v1, "CommentListWidgetHelper::getCommentCount::Command builder is null"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 198
    :goto_0
    return v0

    .line 195
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->b:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;->getCommentList()Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->getTotalCount()I

    move-result v0

    goto :goto_0
.end method

.method public getCommentList()Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;
    .locals 2

    .prologue
    .line 208
    const/4 v0, 0x0

    .line 210
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->b:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    if-nez v1, :cond_0

    .line 212
    const-string v1, "CommentListWidgetHelper::getCommentList::Command builder is null"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 219
    :goto_0
    return-object v0

    .line 216
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->b:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;->getCommentList()Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    move-result-object v0

    goto :goto_0
.end method

.method public sendRequest(ILcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 3

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->a:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->b:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    if-nez v0, :cond_1

    .line 56
    :cond_0
    const-string v0, "CommentListWidgetHelper::sendRequest::Not Ready Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 109
    :goto_0
    return-void

    .line 60
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CommentListWidgetHelper::sendRequest::Invalid request type="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 63
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->b:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;->loadExpertCommend()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    .line 75
    :goto_1
    if-nez v0, :cond_2

    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CommentListWidgetHelper::sendRequest::command is null, request type="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 67
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->b:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;->loadComment()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    goto :goto_1

    .line 81
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->c:Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper$IDataLoadingObserver;

    if-eqz v1, :cond_3

    .line 83
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->c:Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper$IDataLoadingObserver;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper$IDataLoadingObserver;->onStartCommentListLoading()V

    .line 87
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->a:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/widget/k;

    invoke-direct {v2, p0, p2}, Lcom/sec/android/app/samsungapps/widget/k;-><init>(Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0

    .line 60
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public sendRequest(Ljava/lang/String;ILcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->a:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->b:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    if-nez v0, :cond_1

    .line 126
    :cond_0
    const-string v0, "CommentListWidgetHelper::sendRequest::Not Ready Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 152
    :goto_0
    return-void

    .line 130
    :cond_1
    sparse-switch p2, :sswitch_data_0

    .line 141
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CommentListWidgetHelper::sendRequest::Invalid request type="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 133
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->b:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;->modifyComment(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    .line 145
    :goto_1
    if-nez v0, :cond_2

    .line 147
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CommentListWidgetHelper::sendRequest::command is null, request type="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 137
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->b:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;->deleteComment(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    goto :goto_1

    .line 151
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->a:Landroid/content/Context;

    invoke-virtual {v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0

    .line 130
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public setDataLoadingObserver(Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper$IDataLoadingObserver;)V
    .locals 0

    .prologue
    .line 250
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->c:Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper$IDataLoadingObserver;

    .line 251
    return-void
.end method
