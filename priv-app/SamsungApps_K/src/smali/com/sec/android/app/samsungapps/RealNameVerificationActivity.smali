.class public Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;
.super Lcom/sec/android/app/samsungapps/CommonActivity;
.source "ProGuard"


# instance fields
.field a:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;

.field b:Landroid/webkit/WebView;

.field c:Lcom/sec/android/app/samsungapps/LoadingDialog;

.field private d:Ljava/lang/String;

.field private e:I

.field private f:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;-><init>()V

    .line 29
    const-string v0, "-1"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;->d:Ljava/lang/String;

    .line 30
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;->e:I

    .line 84
    new-instance v0, Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;->c:Lcom/sec/android/app/samsungapps/LoadingDialog;

    .line 181
    new-instance v0, Lcom/sec/android/app/samsungapps/gj;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/gj;-><init>(Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;->f:Landroid/os/Handler;

    .line 201
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;I)I
    .locals 0

    .prologue
    .line 26
    iput p1, p0, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;->e:I

    return p1
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 26
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;->d:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;)V
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;->c:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->end()V

    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;)V
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;->c:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->start()V

    return-void
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;)I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;->e:I

    return v0
.end method

.method static synthetic e(Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;->f:Landroid/os/Handler;

    return-object v0
.end method

.method public static start(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 80
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 81
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 82
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;->failed()V

    .line 100
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;

    .line 102
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->onBackPressed()V

    .line 103
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 39
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/CommonActivity;->onCreate(Landroid/os/Bundle;)V

    .line 40
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->readObject(Landroid/content/Intent;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;

    if-nez v0, :cond_0

    .line 43
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;->finish()V

    .line 72
    :goto_0
    return-void

    .line 46
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;->invokeCompleted()V

    .line 47
    const v0, 0x7f0400f1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;->setContentView(I)V

    .line 49
    const v0, 0x7f0c0309

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;->b:Landroid/webkit/WebView;

    .line 50
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;->b:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 51
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 52
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setSavePassword(Z)V

    .line 53
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 54
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setLoadWithOverviewMode(Z)V

    .line 55
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    .line 56
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setJavaScriptCanOpenWindowsAutomatically(Z)V

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;->b:Landroid/webkit/WebView;

    new-instance v1, Lcom/sec/android/app/samsungapps/gk;

    invoke-direct {v1, p0, v3}, Lcom/sec/android/app/samsungapps/gk;-><init>(Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;B)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;->b:Landroid/webkit/WebView;

    new-instance v1, Lcom/sec/android/app/samsungapps/gl;

    invoke-direct {v1, p0, p0}, Lcom/sec/android/app/samsungapps/gl;-><init>(Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;->b:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->requestFocus()Z

    .line 63
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->_isLogedIn()Z

    move-result v0

    if-nez v0, :cond_1

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;->failed()V

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;

    .line 66
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;->finish()V

    goto :goto_0

    .line 70
    :cond_1
    const-string v0, "http://apps.samsung.com/mercury/nice/selectNice.as?COUNTRY_CODE=KOR&emailID=%s&channelCD=2"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getEmailID()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 71
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;->b:Landroid/webkit/WebView;

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 76
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->onDestroy()V

    .line 77
    return-void
.end method
