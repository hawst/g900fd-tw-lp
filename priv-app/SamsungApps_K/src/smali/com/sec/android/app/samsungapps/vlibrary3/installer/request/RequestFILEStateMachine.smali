.class public Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;
.source "ProGuard"


# static fields
.field private static a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;-><init>()V

    .line 33
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;

    if-nez v0, :cond_0

    .line 39
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;

    .line 41
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;

    return-object v0
.end method


# virtual methods
.method protected entry(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 3

    .prologue
    .line 240
    const-string v0, "RequestFILEStateMachine"

    const-string v1, "entry"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 241
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/k;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 303
    :goto_0
    :pswitch_0
    return-void

    .line 246
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->CREATE_FILE_WRITER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 249
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->NOTIFY_COMPLETE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 252
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 255
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->CREATE_NORAML_HTTPGET:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 256
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->SEND_REQUEST:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 259
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->CREATE_RESUME_HTTPGET:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 260
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->SEND_REQUEST:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 263
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->CLOSE_FILE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 264
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->DELETE_FILE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 265
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->DOWNLOADING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 268
    :pswitch_7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->REMEMBER_NETCONNECTION:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 269
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->CLEAR_RETRYCOUNT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 270
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->DOWNLOADING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 273
    :pswitch_8
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->SET_CANCELING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 276
    :pswitch_9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->NOTIFY_COMPLETE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 279
    :pswitch_a
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->HTTP_ABORT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 280
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->DELETE_FILE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 281
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->NOTIFY_SIG_CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 284
    :pswitch_b
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->CHECK_RETRY_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 287
    :pswitch_c
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->INC_RETRY_COUNT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 288
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->START_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 291
    :pswitch_d
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->REQUEST_URL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 294
    :pswitch_e
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->ENQUEUE_IN_WAIT_NETWORACTIVATE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 297
    :pswitch_f
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->CHECK_WIFI_TO_3G_HANDOVER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 300
    :pswitch_10
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->NOTIFY_3G_CONNECTION:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 241
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_7
        :pswitch_8
        :pswitch_a
        :pswitch_2
        :pswitch_3
        :pswitch_6
        :pswitch_9
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method public execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;)Z
    .locals 3

    .prologue
    .line 47
    const-string v0, "RequestFILEStateMachine"

    const-string v1, "execute"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 48
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/k;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 235
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    return v0

    .line 51
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/k;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 54
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 59
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/k;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 62
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->COMPLETE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 65
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->RESUME_DOWNLOAD:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 68
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 71
    :pswitch_7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->NORMAL_DOWNLOAD:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 74
    :pswitch_8
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 79
    :pswitch_9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/k;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    goto :goto_0

    .line 91
    :pswitch_a
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 82
    :pswitch_b
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 85
    :pswitch_c
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->DOWNLOADING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 88
    :pswitch_d
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 96
    :pswitch_e
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/k;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_4

    goto :goto_0

    .line 111
    :pswitch_f
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 99
    :pswitch_10
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 102
    :pswitch_11
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->RESUME_TO_NORMAL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 105
    :pswitch_12
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->DOWNLOADING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 108
    :pswitch_13
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->RETRYCHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 116
    :pswitch_14
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/k;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    sparse-switch v0, :sswitch_data_0

    goto/16 :goto_0

    .line 119
    :sswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->CANCELLING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 122
    :sswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->RETRYCHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 125
    :sswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 130
    :pswitch_15
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/k;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_5

    goto/16 :goto_0

    .line 136
    :pswitch_16
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 133
    :pswitch_17
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 139
    :pswitch_18
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 154
    :pswitch_19
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/k;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_6

    goto/16 :goto_0

    .line 157
    :pswitch_1a
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 160
    :pswitch_1b
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 163
    :pswitch_1c
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->RETRY:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 168
    :pswitch_1d
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/k;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    sparse-switch v0, :sswitch_data_1

    goto/16 :goto_0

    .line 177
    :sswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 171
    :sswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->HANDOVER_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 174
    :sswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 182
    :pswitch_1e
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/k;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    sparse-switch v0, :sswitch_data_2

    goto/16 :goto_0

    .line 194
    :sswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 185
    :sswitch_7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->WAIT_NETWORK_ACTIVATE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 188
    :sswitch_8
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->RETRYCHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 191
    :sswitch_9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 199
    :pswitch_1f
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/k;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    sparse-switch v0, :sswitch_data_3

    goto/16 :goto_0

    .line 205
    :sswitch_a
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 202
    :sswitch_b
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->RETRYCHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 210
    :pswitch_20
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/k;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_7

    goto/16 :goto_0

    .line 213
    :pswitch_21
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->ASK_3G_CONNECTIONOK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 216
    :pswitch_22
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->REQUEST_URL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 221
    :pswitch_23
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/k;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    sparse-switch v0, :sswitch_data_4

    goto/16 :goto_0

    .line 224
    :sswitch_c
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 227
    :sswitch_d
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->REQUEST_URL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 230
    :sswitch_e
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 48
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_9
        :pswitch_e
        :pswitch_14
        :pswitch_15
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_19
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_23
    .end packed-switch

    .line 51
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch

    .line 59
    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 79
    :pswitch_data_3
    .packed-switch 0x6
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch

    .line 96
    :pswitch_data_4
    .packed-switch 0x6
        :pswitch_f
        :pswitch_10
        :pswitch_12
        :pswitch_13
        :pswitch_11
    .end packed-switch

    .line 116
    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0xb -> :sswitch_1
        0xc -> :sswitch_2
    .end sparse-switch

    .line 130
    :pswitch_data_5
    .packed-switch 0xb
        :pswitch_16
        :pswitch_18
        :pswitch_17
    .end packed-switch

    .line 154
    :pswitch_data_6
    .packed-switch 0xe
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
    .end packed-switch

    .line 168
    :sswitch_data_1
    .sparse-switch
        0x6 -> :sswitch_3
        0x11 -> :sswitch_4
        0x12 -> :sswitch_5
    .end sparse-switch

    .line 182
    :sswitch_data_2
    .sparse-switch
        0x6 -> :sswitch_6
        0x12 -> :sswitch_7
        0x13 -> :sswitch_8
        0x14 -> :sswitch_9
    .end sparse-switch

    .line 199
    :sswitch_data_3
    .sparse-switch
        0x6 -> :sswitch_a
        0x15 -> :sswitch_b
    .end sparse-switch

    .line 210
    :pswitch_data_7
    .packed-switch 0x16
        :pswitch_21
        :pswitch_22
    .end packed-switch

    .line 221
    :sswitch_data_4
    .sparse-switch
        0x6 -> :sswitch_c
        0x18 -> :sswitch_d
        0x19 -> :sswitch_e
    .end sparse-switch
.end method

.method public bridge synthetic execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 9
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;)Z

    move-result v0

    return v0
.end method

.method protected exit(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 3

    .prologue
    .line 307
    const-string v0, "RequestFILEStateMachine"

    const-string v1, "exit"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 308
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/k;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 316
    :goto_0
    :pswitch_0
    return-void

    .line 313
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;->STOP_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 308
    nop

    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
