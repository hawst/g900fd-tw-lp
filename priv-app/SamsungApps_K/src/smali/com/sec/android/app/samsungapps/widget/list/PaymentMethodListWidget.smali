.class public Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;
.super Lcom/sec/android/app/samsungapps/widget/CommonWidget;
.source "ProGuard"


# static fields
.field public static final MENU_GIFTCARD_VOUCHER:I = 0x1

.field public static final MENU_PAYMENT_METHOD:I


# instance fields
.field a:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;

.field b:Lcom/sec/android/app/samsungapps/view/MenuView;

.field d:Landroid/widget/Button;

.field e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

.field private f:Lcom/sec/android/app/samsungapps/MenuItemList;

.field protected filter:Landroid/text/InputFilter;

.field private g:I

.field private h:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

.field private i:Landroid/view/View;

.field private final j:I

.field private k:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardCommandBuilder;

.field private l:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/RegisterGiftCardResponseItem;

.field protected lengthFilter:Landroid/text/InputFilter;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x4

    const/4 v0, 0x0

    .line 59
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;)V

    .line 52
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->h:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    .line 53
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->i:Landroid/view/View;

    .line 54
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->j:I

    .line 55
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->k:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardCommandBuilder;

    .line 56
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->l:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/RegisterGiftCardResponseItem;

    .line 273
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/list/r;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/list/r;-><init>(Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

    .line 383
    new-instance v0, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v0, v1}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->lengthFilter:Landroid/text/InputFilter;

    .line 384
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/list/u;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/list/u;-><init>(Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->filter:Landroid/text/InputFilter;

    .line 60
    const v0, 0x7f04008c

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->initView(Landroid/content/Context;I)V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x4

    const/4 v0, 0x0

    .line 64
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->h:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    .line 53
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->i:Landroid/view/View;

    .line 54
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->j:I

    .line 55
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->k:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardCommandBuilder;

    .line 56
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->l:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/RegisterGiftCardResponseItem;

    .line 273
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/list/r;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/list/r;-><init>(Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

    .line 383
    new-instance v0, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v0, v1}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->lengthFilter:Landroid/text/InputFilter;

    .line 384
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/list/u;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/list/u;-><init>(Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->filter:Landroid/text/InputFilter;

    .line 65
    const v0, 0x7f04008c

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->initView(Landroid/content/Context;I)V

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x4

    const/4 v0, 0x0

    .line 69
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 52
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->h:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    .line 53
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->i:Landroid/view/View;

    .line 54
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->j:I

    .line 55
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->k:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardCommandBuilder;

    .line 56
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->l:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/RegisterGiftCardResponseItem;

    .line 273
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/list/r;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/list/r;-><init>(Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

    .line 383
    new-instance v0, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v0, v1}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->lengthFilter:Landroid/text/InputFilter;

    .line 384
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/list/u;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/list/u;-><init>(Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->filter:Landroid/text/InputFilter;

    .line 70
    const v0, 0x7f04008c

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->initView(Landroid/content/Context;I)V

    .line 71
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->h:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->h:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400b1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->i:Landroid/view/View;

    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->h:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->h:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08018d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->h:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->h:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->h:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0802e7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->e:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->h:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositveButtonDisable()V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->h:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08023d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/widget/list/t;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/widget/list/t;-><init>(Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setNegativeButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->i:Landroid/view/View;

    const v1, 0x7f0c0277

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    new-array v1, v5, [Landroid/text/InputFilter;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->filter:Landroid/text/InputFilter;

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->lengthFilter:Landroid/text/InputFilter;

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/list/v;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/list/v;-><init>(Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->i:Landroid/view/View;

    const v1, 0x7f0c0278

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    new-array v1, v5, [Landroid/text/InputFilter;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->filter:Landroid/text/InputFilter;

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->lengthFilter:Landroid/text/InputFilter;

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/list/w;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/list/w;-><init>(Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->i:Landroid/view/View;

    const v1, 0x7f0c0279

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    new-array v1, v5, [Landroid/text/InputFilter;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->filter:Landroid/text/InputFilter;

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->lengthFilter:Landroid/text/InputFilter;

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/list/x;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/list/x;-><init>(Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->i:Landroid/view/View;

    const v1, 0x7f0c027a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    new-array v1, v5, [Landroid/text/InputFilter;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->filter:Landroid/text/InputFilter;

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->lengthFilter:Landroid/text/InputFilter;

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/list/y;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/list/y;-><init>(Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->h:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->h:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->show()V

    :cond_1
    return-void
.end method

.method private a()Z
    .locals 1

    .prologue
    .line 175
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isIran()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 179
    :goto_0
    return v0

    .line 176
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 179
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;)Landroid/view/View;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->i:Landroid/view/View;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardCommandBuilder;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->k:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardCommandBuilder;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/RegisterGiftCardResponseItem;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->l:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/RegisterGiftCardResponseItem;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;)Z
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v2, 0x0

    .line 43
    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->i:Landroid/view/View;

    const v3, 0x7f0c0277

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-eq v0, v4, :cond_0

    move v1, v2

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->i:Landroid/view/View;

    const v3, 0x7f0c0278

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-eq v0, v4, :cond_1

    move v1, v2

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->i:Landroid/view/View;

    const v3, 0x7f0c0279

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-eq v0, v4, :cond_2

    move v1, v2

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->i:Landroid/view/View;

    const v3, 0x7f0c027a

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-eq v0, v4, :cond_3

    :goto_0
    return v2

    :cond_3
    move v2, v1

    goto :goto_0
.end method

.method static synthetic i(Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;)Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->h:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    return-object v0
.end method

.method static synthetic j(Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;)Z
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->a()Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected addCreditCardMenu(Lcom/sec/android/app/samsungapps/MenuItemList;)V
    .locals 3

    .prologue
    .line 212
    if-eqz p1, :cond_0

    .line 214
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    .line 215
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v0

    .line 216
    if-eqz v0, :cond_0

    .line 218
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->isThereCardInfo()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->a:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;->createCreditCardMenu()Lcom/sec/android/app/samsungapps/MenuItem;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/MenuItemList;->add(Lcom/sec/android/app/samsungapps/MenuItem;)Z

    .line 228
    :cond_0
    :goto_0
    return-void

    .line 223
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UPBillingConditionChecker;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UPBillingConditionChecker;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UPBillingConditionChecker;->isUPBillingCondition()Z

    move-result v0

    if-nez v0, :cond_0

    .line 224
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->a:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;->createCreditCardRegisterMenu()Lcom/sec/android/app/samsungapps/MenuItem;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/MenuItemList;->add(Lcom/sec/android/app/samsungapps/MenuItem;)Z

    goto :goto_0
.end method

.method protected addGiftCardMenu(Lcom/sec/android/app/samsungapps/MenuItemList;)V
    .locals 1

    .prologue
    .line 191
    if-eqz p1, :cond_0

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->a:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;->createGiftCardMenu()Lcom/sec/android/app/samsungapps/MenuItem;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/MenuItemList;->add(Lcom/sec/android/app/samsungapps/MenuItem;)Z

    .line 194
    :cond_0
    return-void
.end method

.method protected addMyVoucherMenu(Lcom/sec/android/app/samsungapps/MenuItemList;)V
    .locals 1

    .prologue
    .line 205
    if-eqz p1, :cond_0

    .line 206
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->a:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;->createMyVoucherMenu()Lcom/sec/android/app/samsungapps/MenuItem;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/MenuItemList;->add(Lcom/sec/android/app/samsungapps/MenuItem;)Z

    .line 208
    :cond_0
    return-void
.end method

.method protected addVoucherMenu(Lcom/sec/android/app/samsungapps/MenuItemList;)V
    .locals 1

    .prologue
    .line 198
    if-eqz p1, :cond_0

    .line 199
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->a:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;->createVoucherMenu()Lcom/sec/android/app/samsungapps/MenuItem;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/MenuItemList;->add(Lcom/sec/android/app/samsungapps/MenuItem;)Z

    .line 201
    :cond_0
    return-void
.end method

.method public createMenuItem(I)V
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->b:Lcom/sec/android/app/samsungapps/view/MenuView;

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->b:Lcom/sec/android/app/samsungapps/view/MenuView;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->createMenuItemList(I)Lcom/sec/android/app/samsungapps/MenuItemList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/MenuView;->setMenuItemList(Lcom/sec/android/app/samsungapps/MenuItemList;)V

    .line 133
    :cond_0
    return-void
.end method

.method protected createMenuItemList(I)Lcom/sec/android/app/samsungapps/MenuItemList;
    .locals 3

    .prologue
    .line 137
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->g:I

    .line 138
    const/4 v0, 0x0

    .line 141
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->f:Lcom/sec/android/app/samsungapps/MenuItemList;

    if-eqz v1, :cond_0

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->f:Lcom/sec/android/app/samsungapps/MenuItemList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/MenuItemList;->getSelId()I

    move-result v0

    .line 143
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->f:Lcom/sec/android/app/samsungapps/MenuItemList;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/MenuItemList;->clear()V

    .line 146
    :cond_0
    new-instance v1, Lcom/sec/android/app/samsungapps/MenuItemList;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/MenuItemList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->f:Lcom/sec/android/app/samsungapps/MenuItemList;

    .line 148
    const/4 v1, 0x1

    if-ne p1, v1, :cond_2

    .line 149
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->f:Lcom/sec/android/app/samsungapps/MenuItemList;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->addGiftCardMenu(Lcom/sec/android/app/samsungapps/MenuItemList;)V

    .line 150
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->f:Lcom/sec/android/app/samsungapps/MenuItemList;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->addVoucherMenu(Lcom/sec/android/app/samsungapps/MenuItemList;)V

    .line 162
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->f:Lcom/sec/android/app/samsungapps/MenuItemList;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/MenuItemList;->setSelById(I)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->f:Lcom/sec/android/app/samsungapps/MenuItemList;

    :goto_1
    return-object v0

    .line 152
    :cond_2
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isFreeStore()Z

    move-result v1

    if-nez v1, :cond_1

    .line 154
    const-string v1, "450"

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMCC()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 156
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->f:Lcom/sec/android/app/samsungapps/MenuItemList;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->addCreditCardMenu(Lcom/sec/android/app/samsungapps/MenuItemList;)V

    .line 158
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->f:Lcom/sec/android/app/samsungapps/MenuItemList;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->addMyVoucherMenu(Lcom/sec/android/app/samsungapps/MenuItemList;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 165
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->f:Lcom/sec/android/app/samsungapps/MenuItemList;

    goto :goto_1

    .line 168
    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->f:Lcom/sec/android/app/samsungapps/MenuItemList;

    goto :goto_1
.end method

.method public loadWidget()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/16 v9, 0x8

    const/4 v8, 0x0

    .line 79
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/GiftCardCommandBuilder;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/commands/GiftCardCommandBuilder;-><init>()V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->setAddGiftCardCommandBuilder(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardCommandBuilder;)V

    .line 81
    const v0, 0x7f0c0132

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/view/MenuView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->b:Lcom/sec/android/app/samsungapps/view/MenuView;

    .line 82
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;-><init>(Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->a:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget$MenuCreator;

    .line 84
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->g:I

    if-ne v0, v10, :cond_0

    .line 85
    const v0, 0x7f0c0207

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 86
    const v0, 0x7f0c0206

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 87
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0802e7

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v0, 0x7f0c0146

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const v0, 0x7f0c0142

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const v0, 0x7f0c0144

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v5

    const v0, 0x7f0c0140

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getNegativeButtonID(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v7

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getPositiveButtonID(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->d:Landroid/widget/Button;

    if-eqz v3, :cond_0

    if-eqz v4, :cond_0

    if-eqz v5, :cond_0

    if-eqz v6, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->d:Landroid/widget/Button;

    if-nez v0, :cond_1

    .line 89
    :cond_0
    :goto_0
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->g:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->createMenuItem(I)V

    .line 90
    return-void

    .line 87
    :cond_1
    invoke-static {v1}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getAlertDialogButtonOrder(Landroid/content/Context;)I

    move-result v0

    if-ne v0, v10, :cond_2

    invoke-virtual {v6, v9}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v3, v9}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->d:Landroid/widget/Button;

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->d:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->d:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->onPositive()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v7, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_2
    invoke-virtual {v5, v9}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v4, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public onClickRegisterGiftCard(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 297
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/RegisterGiftCardResponseItem;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/RegisterGiftCardResponseItem;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->l:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/RegisterGiftCardResponseItem;

    .line 298
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->k:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardCommandBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->l:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/RegisterGiftCardResponseItem;

    invoke-interface {v0, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardCommandBuilder;->registerGiftCard(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/RegisterGiftCardResponseItem;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    .line 299
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/widget/list/s;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/widget/list/s;-><init>(Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 330
    return-void
.end method

.method public onPositive()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 263
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/list/q;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/list/q;-><init>(Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;)V

    return-object v0
.end method

.method public reArrangeMenuItem()V
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->b:Lcom/sec/android/app/samsungapps/view/MenuView;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->b:Lcom/sec/android/app/samsungapps/view/MenuView;

    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->g:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->createMenuItemList(I)Lcom/sec/android/app/samsungapps/MenuItemList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/MenuView;->setMenuItemList(Lcom/sec/android/app/samsungapps/MenuItemList;)V

    .line 127
    :cond_0
    return-void
.end method

.method public refreshWidget()V
    .locals 0

    .prologue
    .line 105
    return-void
.end method

.method public release()V
    .locals 0

    .prologue
    .line 116
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->release()V

    .line 117
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->removeAllViews()V

    .line 118
    return-void
.end method

.method public setAddGiftCardCommandBuilder(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardCommandBuilder;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->k:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardCommandBuilder;

    .line 94
    return-void
.end method

.method public setType(I)V
    .locals 0

    .prologue
    .line 74
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->g:I

    .line 75
    return-void
.end method

.method public setWidgetData(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 111
    return-void
.end method

.method public updateWidget()V
    .locals 0

    .prologue
    .line 99
    return-void
.end method
