.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum CHECK_AGE_LIMIT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

.field public static final enum CHECK_DETAIL_EXIST:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

.field public static final enum CHECK_FREE_STORAGE_SPACE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

.field public static final enum CHECK_LOGIN:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

.field public static final enum CHECK_MULTIPLE_DOWNLOAD_COUNT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

.field public static final enum CHECK_NEED_TO_SHOW_ALREADY_PURCHASED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

.field public static final enum CHECK_P2_CONFIRM:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

.field public static final enum CHECK_REAL_NAME_AGE_NEED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

.field public static final enum CHECK_TURKEY_NETCONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

.field public static final enum NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

.field public static final enum NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

.field public static final enum PERMISSION_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

.field public static final enum REMEMBER_FREE_PAID_STATE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

.field public static final enum REQUEST_CHECK_NETWORK_LIMIT_SIZE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

.field public static final enum VALIDATE_COMPATIBLE_OS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 24
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    const-string v1, "CHECK_LOGIN"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;->CHECK_LOGIN:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    const-string v1, "NOTIFY_FAILED"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    const-string v1, "CHECK_TURKEY_NETCONDITION"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;->CHECK_TURKEY_NETCONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    const-string v1, "CHECK_DETAIL_EXIST"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;->CHECK_DETAIL_EXIST:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    const-string v1, "CHECK_FREE_STORAGE_SPACE"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;->CHECK_FREE_STORAGE_SPACE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    const-string v1, "REQUEST_CHECK_NETWORK_LIMIT_SIZE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;->REQUEST_CHECK_NETWORK_LIMIT_SIZE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    const-string v1, "CHECK_MULTIPLE_DOWNLOAD_COUNT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;->CHECK_MULTIPLE_DOWNLOAD_COUNT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    const-string v1, "CHECK_P2_CONFIRM"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;->CHECK_P2_CONFIRM:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    const-string v1, "CHECK_AGE_LIMIT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;->CHECK_AGE_LIMIT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    const-string v1, "VALIDATE_COMPATIBLE_OS"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;->VALIDATE_COMPATIBLE_OS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    const-string v1, "CHECK_REAL_NAME_AGE_NEED"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;->CHECK_REAL_NAME_AGE_NEED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    const-string v1, "PERMISSION_CHECK"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;->PERMISSION_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    const-string v1, "NOTIFY_SUCCESS"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;->NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    const-string v1, "REMEMBER_FREE_PAID_STATE"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;->REMEMBER_FREE_PAID_STATE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    const-string v1, "CHECK_NEED_TO_SHOW_ALREADY_PURCHASED"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;->CHECK_NEED_TO_SHOW_ALREADY_PURCHASED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    .line 22
    const/16 v0, 0xf

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;->CHECK_LOGIN:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;->CHECK_TURKEY_NETCONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;->CHECK_DETAIL_EXIST:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;->CHECK_FREE_STORAGE_SPACE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;->REQUEST_CHECK_NETWORK_LIMIT_SIZE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;->CHECK_MULTIPLE_DOWNLOAD_COUNT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;->CHECK_P2_CONFIRM:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;->CHECK_AGE_LIMIT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;->VALIDATE_COMPATIBLE_OS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;->CHECK_REAL_NAME_AGE_NEED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;->PERMISSION_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;->NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;->REMEMBER_FREE_PAID_STATE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;->CHECK_NEED_TO_SHOW_ALREADY_PURCHASED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    return-object v0
.end method
