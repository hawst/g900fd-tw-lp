.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/Downloader$IDownloadSingleItemResult;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract onDownloadCanceled()V
.end method

.method public abstract onDownloadFailed()V
.end method

.method public abstract onDownloadSuccess()V
.end method

.method public abstract onInstallFailedWithErrCode(Ljava/lang/String;)V
.end method

.method public abstract onPaymentSuccess()V
.end method

.method public abstract onProgress(JJ)V
.end method

.method public abstract onStateChanged()V
.end method
