.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseDetailRequestXML;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
.source "ProGuard"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;I)V
    .locals 6

    .prologue
    .line 18
    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getPurchased()Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    move-result-object v0

    iget-object v2, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->orderID:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->getPurchased()Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    move-result-object v0

    iget-object v3, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->ordItemSeq:Ljava/lang/String;

    iget-object v4, p2, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->productID:Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseDetailRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 19
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 9
    const-string v2, "purchaseDetail"

    const-string v3, "2150"

    move-object v0, p0

    move-object v1, p1

    move v4, p5

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 10
    const-string v0, "orderID"

    invoke-virtual {p0, v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    const-string v0, "ordItemSeq"

    invoke-virtual {p0, v0, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    const-string v0, "productID"

    invoke-virtual {p0, v0, p4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    const-string v0, "imgWidth"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getWidth()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseDetailRequestXML;->toStr(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    const-string v0, "imgHeight"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getHeight()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseDetailRequestXML;->toStr(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseDetailRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    return-void
.end method
