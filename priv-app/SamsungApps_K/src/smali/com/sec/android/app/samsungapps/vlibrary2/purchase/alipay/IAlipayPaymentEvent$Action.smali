.class public final enum Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

.field public static final enum ASK_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

.field public static final enum REQUEST_COMPLETE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

.field public static final enum REQUEST_INIT:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

.field public static final enum REQUEST_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

.field public static final enum REQUEST_PROCESS:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

.field public static final enum SEND_FAIL_SIGNAL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

.field public static final enum SEND_SUCCESS_SIGNAL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

.field public static final enum START_LOADING:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

.field public static final enum STOP_LOADING:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 34
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    const-string v1, "ASK_INSTALL"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;->ASK_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    const-string v1, "SEND_FAIL_SIGNAL"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;->SEND_FAIL_SIGNAL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    const-string v1, "SEND_SUCCESS_SIGNAL"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;->SEND_SUCCESS_SIGNAL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    const-string v1, "REQUEST_INIT"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;->REQUEST_INIT:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    const-string v1, "REQUEST_INSTALL"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;->REQUEST_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    const-string v1, "REQUEST_PROCESS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;->REQUEST_PROCESS:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    const-string v1, "REQUEST_COMPLETE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;->REQUEST_COMPLETE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    const-string v1, "START_LOADING"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;->START_LOADING:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    const-string v1, "STOP_LOADING"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;->STOP_LOADING:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    .line 32
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;->ASK_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;->SEND_FAIL_SIGNAL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;->SEND_SUCCESS_SIGNAL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;->REQUEST_INIT:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;->REQUEST_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;->REQUEST_PROCESS:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;->REQUEST_COMPLETE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;->START_LOADING:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;->STOP_LOADING:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Action;

    return-object v0
.end method
