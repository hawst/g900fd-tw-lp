.class public final enum Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;

.field public static final enum REQUEST_DELETE_CARD:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;

.field public static final enum SEND_SIG_CARD_DELETED:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;

.field public static final enum SHOW_ASK_DELETE_CARD_POPUP:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;

.field public static final enum START_LOADING:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;

.field public static final enum STOP_LOADING:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 23
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;

    const-string v1, "SEND_SIG_CARD_DELETED"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;->SEND_SIG_CARD_DELETED:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;

    const-string v1, "REQUEST_DELETE_CARD"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;->REQUEST_DELETE_CARD:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;

    const-string v1, "START_LOADING"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;->START_LOADING:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;

    const-string v1, "STOP_LOADING"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;->STOP_LOADING:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;

    const-string v1, "SHOW_ASK_DELETE_CARD_POPUP"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;->SHOW_ASK_DELETE_CARD_POPUP:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;

    .line 21
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;->SEND_SIG_CARD_DELETED:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;->REQUEST_DELETE_CARD:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;->START_LOADING:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;->STOP_LOADING:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;->SHOW_ASK_DELETE_CARD_POPUP:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;

    return-object v0
.end method
