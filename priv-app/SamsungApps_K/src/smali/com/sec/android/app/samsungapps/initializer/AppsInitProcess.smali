.class public Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field protected _Activity:Landroid/app/Activity;

.field private a:Z

.field private b:Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess$IAppsInitProcessData;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Z)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-boolean p2, p0, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->a:Z

    .line 24
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->_Activity:Landroid/app/Activity;

    .line 25
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;)V
    .locals 3

    .prologue
    .line 18
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->_Activity:Landroid/app/Activity;

    const-class v2, Lcom/sec/android/app/samsungapps/UncListActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->_Activity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;)V
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->_Activity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;)Z
    .locals 3

    .prologue
    .line 18
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->_Activity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;-><init>(Landroid/content/Context;)V

    const-string v1, "1"

    const-string v2, "IS_ALARM_SET_CHECKED"

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;)V
    .locals 4

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->_Activity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getAutoUpdateTriggerFactory()Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerFactory;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->_Activity:Landroid/app/Activity;

    new-instance v2, Lcom/sec/android/app/samsungapps/initializer/a;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/initializer/a;-><init>(Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;)V

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerFactory;->createAutoUpdateChecker(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateTriggerChecker$IAutoUpdateTriggerManagerObserver;)Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateTriggerChecker;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->_Activity:Landroid/app/Activity;

    new-instance v3, Lcom/sec/android/app/samsungapps/initializer/b;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/initializer/b;-><init>(Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;)V

    invoke-interface {v0, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerFactory;->createPreloadAutoUpdateChecker(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateTriggerChecker$IAutoUpdateTriggerManagerObserver;)Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateTriggerChecker;

    move-result-object v0

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateTriggerChecker;->check()V

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateTriggerChecker;->check()V

    return-void
.end method

.method static synthetic e(Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;)V
    .locals 3

    .prologue
    .line 18
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->_Activity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;-><init>(Landroid/content/Context;)V

    const-string v1, "IS_ALARM_SET_CHECKED"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    return-void
.end method

.method static synthetic f(Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;)Z
    .locals 1

    .prologue
    .line 18
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->a:Z

    return v0
.end method

.method static synthetic g(Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;)Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess$IAppsInitProcessData;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->b:Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess$IAppsInitProcessData;

    return-object v0
.end method


# virtual methods
.method protected createInitializerFactory()Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializerFactory;
    .locals 2

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->_Activity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->_Activity:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->initializer(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializerFactory;

    move-result-object v0

    return-object v0
.end method

.method protected startInitializing()V
    .locals 4

    .prologue
    .line 115
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->_Activity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isUncStore()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 117
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/samsungapps/initializer/c;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/initializer/c;-><init>(Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 190
    :goto_0
    return-void

    .line 129
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->createInitializerFactory()Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializerFactory;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->_Activity:Landroid/app/Activity;

    new-instance v2, Lcom/sec/android/app/samsungapps/initializer/d;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/initializer/d;-><init>(Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;)V

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializerFactory;->createInitializer(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializer$IIntializerObserver;)Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializer;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializer;->execute()V

    goto :goto_0
.end method

.method public startThreadForInitializingData(Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess$IAppsInitProcessData;)V
    .locals 2

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->b:Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess$IAppsInitProcessData;

    .line 31
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->reinit()V

    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->_Activity:Landroid/app/Activity;

    const-string v1, "odc9820938409234.apk"

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->delete(Landroid/content/Context;Ljava/lang/String;)Z

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->_Activity:Landroid/app/Activity;

    const-string v1, "odc9820938409234a.apk"

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->delete(Landroid/content/Context;Ljava/lang/String;)Z

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->_Activity:Landroid/app/Activity;

    const-string v1, "odc9820938409234Self.apk"

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->delete(Landroid/content/Context;Ljava/lang/String;)Z

    .line 35
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->_Activity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->reInit(Landroid/content/Context;)V

    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->_Activity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->_Activity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->createDeepLink(Landroid/content/Intent;)V

    .line 37
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->startInitializing()V

    .line 38
    return-void
.end method
