.class public Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/EulaAgreementFactory;
.super Ljava/lang/Object;
.source "ProGuard"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createTncAgreementChecker(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTableFactory;)Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/PreloadTnCAgreementChecker;
    .locals 3

    .prologue
    .line 10
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/CTnCCheckCondition;

    invoke-direct {v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/CTnCCheckCondition;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTableFactory;)V

    .line 11
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/CFOTATnCAgreementChecker;

    invoke-direct {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/CFOTATnCAgreementChecker;-><init>(Landroid/content/Context;)V

    .line 12
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/CPreloadTnCAgreementChecker;

    invoke-direct {v2, v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/CPreloadTnCAgreementChecker;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/FOTATnCAgreementChecker;Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/TncCheckConditionForEULA;)V

    return-object v2
.end method
