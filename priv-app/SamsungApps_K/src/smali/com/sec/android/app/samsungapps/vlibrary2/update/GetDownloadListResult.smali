.class public Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;
.super Ljava/util/ArrayList;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private _AppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

.field private _Context:Landroid/content/Context;

.field private _CurMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

.field private _InternalMap:Ljava/util/ArrayList;

.field private _iLastUpdateItemIndex:I

.field private iCount:I

.field private mGear2Api:Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->_CurMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 20
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->_InternalMap:Ljava/util/ArrayList;

    .line 23
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->_iLastUpdateItemIndex:I

    .line 29
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->_Context:Landroid/content/Context;

    .line 30
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->_Context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->_AppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;)V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->_CurMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 20
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->_InternalMap:Ljava/util/ArrayList;

    .line 23
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->_iLastUpdateItemIndex:I

    .line 35
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->_Context:Landroid/content/Context;

    .line 36
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->_Context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->_AppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    .line 37
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->mGear2Api:Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;

    .line 38
    return-void
.end method

.method private getPurchaseItemCountInOnePage()I
    .locals 1

    .prologue
    .line 80
    const/16 v0, 0x19

    return v0
.end method


# virtual methods
.method public addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->_CurMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->_CurMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 89
    :cond_0
    return-void
.end method

.method public clearContainer()V
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->iCount:I

    .line 94
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->clear()V

    .line 95
    return-void
.end method

.method public closeMap()V
    .locals 3

    .prologue
    .line 47
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->iCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->iCount:I

    .line 48
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->_CurMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    if-eqz v0, :cond_0

    .line 50
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->_CurMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 51
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->_InternalMap:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 52
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->_CurMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 53
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->mGear2Api:Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;

    if-eqz v1, :cond_1

    .line 55
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->_AppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->mGear2Api:Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->isUpdatable(Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 57
    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->iCount:I

    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->_iLastUpdateItemIndex:I

    .line 58
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->add(Ljava/lang/Object;)Z

    .line 71
    :cond_0
    :goto_0
    return-void

    .line 63
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->_AppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->isUpdatable(Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 66
    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->iCount:I

    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->_iLastUpdateItemIndex:I

    .line 67
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public findString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMinCountToRequestPurchaseItemList()I
    .locals 3

    .prologue
    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "LastUpdatableItemIndex:: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->_iLastUpdateItemIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 76
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->getPurchaseItemCountInOnePage()I

    move-result v0

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->_iLastUpdateItemIndex:I

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->getPurchaseItemCountInOnePage()I

    move-result v2

    div-int/2addr v1, v2

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->getPurchaseItemCountInOnePage()I

    move-result v2

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    return v0
.end method

.method public openMap()V
    .locals 1

    .prologue
    .line 42
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListResult;->_CurMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 43
    return-void
.end method

.method public setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 112
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 99
    invoke-super {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
