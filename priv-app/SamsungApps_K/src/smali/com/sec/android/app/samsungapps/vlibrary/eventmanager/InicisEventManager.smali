.class public Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisEventManager;
.super Ljava/lang/Object;
.source "ProGuard"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public inicisPaymentFailed()V
    .locals 3

    .prologue
    .line 11
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent;

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;->FINALFAILED:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;

    invoke-direct {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->notifyBroadcastEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V

    .line 12
    return-void
.end method

.method public inicisPaymentSucceed()V
    .locals 3

    .prologue
    .line 7
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent;

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;->FINALSUCCESS:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;

    invoke-direct {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->notifyBroadcastEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V

    .line 8
    return-void
.end method
