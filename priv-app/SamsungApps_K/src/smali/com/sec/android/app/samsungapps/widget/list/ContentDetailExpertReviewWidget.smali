.class public Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;
.super Lcom/sec/android/app/samsungapps/widget/CommonWidget;
.source "ProGuard"


# static fields
.field public static final MAX_REVIEW_LINE_COUNT:I = 0x3

.field public static final URL_HEADER:Ljava/lang/String; = "http"

.field public static final URL_HEADER_APPEND:Ljava/lang/String; = "http://"


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailExpertReviewWidgetClickListener;

.field private b:Landroid/content/Context;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

.field private final e:I

.field private f:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;)V

    .line 27
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->e:I

    .line 200
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/list/d;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/list/d;-><init>(Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->f:Landroid/view/View$OnClickListener;

    .line 35
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->b:Landroid/content/Context;

    .line 36
    const v0, 0x7f04003d

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->initView(Landroid/content/Context;I)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->e:I

    .line 200
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/list/d;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/list/d;-><init>(Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->f:Landroid/view/View$OnClickListener;

    .line 41
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->b:Landroid/content/Context;

    .line 42
    const v0, 0x7f04003d

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->initView(Landroid/content/Context;I)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->e:I

    .line 200
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/list/d;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/list/d;-><init>(Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->f:Landroid/view/View$OnClickListener;

    .line 48
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->b:Landroid/content/Context;

    .line 49
    const v0, 0x7f04003d

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->initView(Landroid/content/Context;I)V

    .line 50
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->b:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;)Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailExpertReviewWidgetClickListener;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->a:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailExpertReviewWidgetClickListener;

    return-object v0
.end method


# virtual methods
.method public isEmptyReview()Z
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 212
    :cond_0
    const/4 v0, 0x1

    .line 214
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadWidget()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    if-nez v0, :cond_1

    .line 75
    const-string v0, "ExpertReview::loadWidget::CommentList is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 101
    :cond_0
    :goto_0
    return-void

    .line 79
    :cond_1
    const v0, 0x7f0c0120

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 80
    const v1, 0x7f0c011e

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 82
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 87
    const v2, 0x7f0c011f

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 88
    if-eqz v2, :cond_2

    .line 89
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->b:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0801fc

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%d"

    new-array v5, v8, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    invoke-virtual {v6}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->getTotalCount()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->size()I

    move-result v2

    const/4 v3, 0x2

    if-le v2, v3, :cond_3

    .line 93
    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 94
    invoke-virtual {v1, v8}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 97
    :cond_3
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->clearFocus()V

    .line 98
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 99
    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    goto/16 :goto_0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->updateWidget()V

    .line 220
    return-void
.end method

.method public refreshWidget()V
    .locals 0

    .prologue
    .line 118
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 63
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->b:Landroid/content/Context;

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->clear()V

    .line 66
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    .line 68
    :cond_0
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->f:Landroid/view/View$OnClickListener;

    .line 69
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->release()V

    .line 70
    return-void
.end method

.method public setReviewsWidgetListener(Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailExpertReviewWidgetClickListener;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->a:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailExpertReviewWidgetClickListener;

    .line 59
    return-void
.end method

.method public setWidgetData(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 54
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    .line 55
    return-void
.end method

.method public updateWidget()V
    .locals 11

    .prologue
    const v10, 0x7f0c0122

    const/4 v0, 0x2

    const/16 v9, 0x8

    const/4 v5, 0x0

    .line 106
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 113
    :cond_0
    return-void

    .line 110
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->getTotalCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 111
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->getTotalCount()I

    move-result v1

    if-le v1, v0, :cond_3

    move v3, v0

    :goto_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    const v0, 0x7f0c0121

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const v1, 0x7f0c0123

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-virtual {v1, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    move v4, v5

    :goto_1
    if-ge v4, v3, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const v2, 0x7f0c0110

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const v2, 0x7f0c0111

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/widget/TextView;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;->getExpertUpdateDate()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->b:Landroid/content/Context;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;->getExpertUpdateDate()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Lcom/sec/android/app/samsungapps/vlibrary2/util/AppsDateFormat;->getSystemDateItem(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    if-nez v7, :cond_5

    :cond_2
    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_3
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const v7, 0x7f0c0112

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->clearData()V

    const/4 v7, 0x3

    invoke-virtual {v0, v7}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->setMaxLineCount(I)V

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;->getComment()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const v7, 0x7f0c010f

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;->getComment()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const v2, 0x7f0c0114

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;->getExpertTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const v2, 0x7f0c0115

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v7, "<u>"

    invoke-direct {v2, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;->getExpertUrl()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "</u>"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const v2, 0x7f0c0113

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;->getExpertTitle()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;->getExpertUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v7, 0x7f0800cf

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/list/c;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/list/c;-><init>(Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-lez v4, :cond_6

    invoke-virtual {p0, v10}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    :goto_4
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto/16 :goto_1

    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->getTotalCount()I

    move-result v0

    move v3, v0

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->b:Landroid/content/Context;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;->getDate()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Lcom/sec/android/app/samsungapps/vlibrary2/util/AppsDateFormat;->getSystemDateItem(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    :cond_5
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_6
    invoke-virtual {p0, v10}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4
.end method
