.class final Lcom/sec/android/app/samsungapps/downloadservice/f;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/Downloader$IDownloadSingleItemResult;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadResultCallback;

.field final synthetic b:Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadResultCallback;)V
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/downloadservice/f;->b:Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;

    iput-object p2, p0, Lcom/sec/android/app/samsungapps/downloadservice/f;->a:Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadResultCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDownloadCanceled()V
    .locals 1

    .prologue
    .line 195
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadservice/f;->a:Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadResultCallback;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadResultCallback;->onDownloadCanceled()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 200
    :goto_0
    return-void

    .line 196
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public final onDownloadFailed()V
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadservice/f;->b:Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadservice/f;->a:Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadResultCallback;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/downloadservice/SamsungAppsDownloadService;->a(Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadResultCallback;)V

    .line 190
    return-void
.end method

.method public final onDownloadSuccess()V
    .locals 1

    .prologue
    .line 180
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadservice/f;->a:Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadResultCallback;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadResultCallback;->onDownloadSuccess()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 185
    :goto_0
    return-void

    .line 181
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public final onInstallFailedWithErrCode(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 170
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadservice/f;->a:Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadResultCallback;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadResultCallback;->onInstallFailed(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 175
    :goto_0
    return-void

    .line 171
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public final onPaymentSuccess()V
    .locals 0

    .prologue
    .line 165
    return-void
.end method

.method public final onProgress(JJ)V
    .locals 1

    .prologue
    .line 154
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadservice/f;->a:Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadResultCallback;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/sec/android/app/samsungapps/downloadservice/aidl/IDownloadResultCallback;->onProgress(JJ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 159
    :goto_0
    return-void

    .line 155
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public final onStateChanged()V
    .locals 0

    .prologue
    .line 148
    return-void
.end method
