.class public Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;


# instance fields
.field private _IPriceCalculatorInfo:Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator$IPriceCalculatorInfo;

.field private _IPriceRelatedFields:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator$IPriceCalculatorInfo;Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator;->_IPriceCalculatorInfo:Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator$IPriceCalculatorInfo;

    .line 16
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator;->_IPriceRelatedFields:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;

    .line 17
    return-void
.end method


# virtual methods
.method public getBuyPrice()D
    .locals 6

    .prologue
    const-wide/16 v0, 0x0

    .line 28
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator;->_IPriceCalculatorInfo:Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator$IPriceCalculatorInfo;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator$IPriceCalculatorInfo;->getSelectedCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    move-result-object v2

    .line 29
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator;->_IPriceCalculatorInfo:Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator$IPriceCalculatorInfo;

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator$IPriceCalculatorInfo;->getSelectedGiftCard()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    move-result-object v3

    .line 30
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator;->_IPriceCalculatorInfo:Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator$IPriceCalculatorInfo;

    invoke-interface {v4}, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator$IPriceCalculatorInfo;->getSelectedPaymentMethod()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    move-result-object v4

    .line 31
    if-eqz v2, :cond_1

    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator;->_IPriceRelatedFields:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;

    invoke-virtual {v4, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->getPrice(Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;)Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;->getSellingPrice()D

    move-result-wide v0

    .line 34
    invoke-interface {v2, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->calcPaymentPrice(D)D

    move-result-wide v0

    .line 48
    :cond_0
    :goto_0
    return-wide v0

    .line 37
    :cond_1
    if-eqz v3, :cond_2

    .line 39
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator;->_IPriceRelatedFields:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;

    invoke-virtual {v4, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->getPrice(Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;)Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;->getSellingPrice()D

    move-result-wide v4

    .line 40
    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;->getBalanceAmount()D

    move-result-wide v2

    sub-double v2, v4, v2

    .line 41
    cmpg-double v4, v2, v0

    if-ltz v4, :cond_0

    move-wide v0, v2

    .line 46
    goto :goto_0

    .line 48
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator;->_IPriceRelatedFields:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;

    invoke-virtual {v4, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->getPrice(Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;)Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;->getSellingPrice()D

    move-result-wide v0

    goto :goto_0
.end method

.method public getDiscountedPrice()D
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator;->_IPriceCalculatorInfo:Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator$IPriceCalculatorInfo;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator$IPriceCalculatorInfo;->getSelectedPaymentMethod()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    move-result-object v0

    .line 102
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator;->_IPriceRelatedFields:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->getPrice(Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;)Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;->getDiscountPrice()D

    move-result-wide v0

    return-wide v0
.end method

.method public getDisplayBuyPrice()D
    .locals 6

    .prologue
    const-wide/16 v0, 0x0

    .line 53
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator;->_IPriceCalculatorInfo:Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator$IPriceCalculatorInfo;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator$IPriceCalculatorInfo;->getSelectedCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    move-result-object v2

    .line 54
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator;->_IPriceCalculatorInfo:Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator$IPriceCalculatorInfo;

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator$IPriceCalculatorInfo;->getSelectedGiftCard()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    move-result-object v3

    .line 56
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator;->_IPriceCalculatorInfo:Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator$IPriceCalculatorInfo;

    invoke-interface {v4}, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator$IPriceCalculatorInfo;->getSelectedPaymentMethod()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    move-result-object v4

    .line 58
    if-eqz v2, :cond_2

    .line 60
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    .line 62
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isPoland()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 64
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_GLOBAL_CREDIT_CARD:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator;->_IPriceRelatedFields:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->getPrice(Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;)Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;->getSellingPrice()D

    move-result-wide v0

    .line 71
    :goto_0
    invoke-interface {v2, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->calcPaymentPrice(D)D

    move-result-wide v0

    .line 84
    :cond_0
    :goto_1
    return-wide v0

    .line 68
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator;->_IPriceRelatedFields:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;

    invoke-virtual {v4, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->getPrice(Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;)Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;->getSellingPrice()D

    move-result-wide v0

    goto :goto_0

    .line 74
    :cond_2
    if-eqz v3, :cond_3

    .line 76
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator;->_IPriceRelatedFields:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;

    invoke-virtual {v4, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->getPrice(Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;)Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;->getSellingPrice()D

    move-result-wide v4

    .line 77
    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;->getBalanceAmount()D

    move-result-wide v2

    sub-double v2, v4, v2

    .line 78
    cmpg-double v4, v2, v0

    if-ltz v4, :cond_0

    move-wide v0, v2

    .line 82
    goto :goto_1

    .line 84
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator;->_IPriceRelatedFields:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;

    invoke-virtual {v4, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->getPrice(Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;)Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;->getSellingPrice()D

    move-result-wide v0

    goto :goto_1
.end method

.method public getNormalPrice()D
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator;->_IPriceCalculatorInfo:Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator$IPriceCalculatorInfo;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator$IPriceCalculatorInfo;->getSelectedPaymentMethod()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    move-result-object v0

    .line 90
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator;->_IPriceRelatedFields:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->getPrice(Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;)Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;->getNormalPrice()D

    move-result-wide v0

    return-wide v0
.end method

.method public isDiscounted()Z
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator;->_IPriceCalculatorInfo:Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator$IPriceCalculatorInfo;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator$IPriceCalculatorInfo;->getSelectedPaymentMethod()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    move-result-object v0

    .line 96
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator;->_IPriceRelatedFields:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->getPrice(Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;)Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;->isDiscounted()Z

    move-result v0

    return v0
.end method
