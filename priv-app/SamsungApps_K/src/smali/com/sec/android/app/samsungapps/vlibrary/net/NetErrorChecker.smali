.class public Lcom/sec/android/app/samsungapps/vlibrary/net/NetErrorChecker;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field mSuccessErrCode:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetErrorChecker;->mSuccessErrCode:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public addSuccessErrorCode(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetErrorChecker;->mSuccessErrCode:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 12
    return-void
.end method

.method public isError(Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 15
    iget-object v0, p1, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->e:Ljava/lang/Exception;

    if-eqz v0, :cond_0

    move v0, v1

    .line 35
    :goto_0
    return v0

    .line 18
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getType()Lcom/sec/android/app/samsungapps/vlibrary/net/NetError$ErrorType;

    move-result-object v0

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError$ErrorType;->PARSINGERROR:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError$ErrorType;

    if-ne v0, v3, :cond_1

    move v0, v1

    .line 20
    goto :goto_0

    .line 22
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getType()Lcom/sec/android/app/samsungapps/vlibrary/net/NetError$ErrorType;

    move-result-object v0

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError$ErrorType;->HTTPERROR:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError$ErrorType;

    if-ne v0, v3, :cond_2

    move v0, v1

    .line 24
    goto :goto_0

    .line 26
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetErrorChecker;->mSuccessErrCode:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 28
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_3

    move v0, v2

    .line 29
    goto :goto_0

    .line 32
    :cond_4
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorCode()Ljava/lang/String;

    move-result-object v0

    const-string v3, "0"

    invoke-virtual {v0, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    .line 33
    goto :goto_0

    :cond_5
    move v0, v2

    .line 35
    goto :goto_0
.end method
