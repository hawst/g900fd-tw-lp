.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceCalculator$IPriceCalculatorInfo;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseCommandBuilder;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardParam;


# instance fields
.field private _CVC:Ljava/lang/String;

.field private _Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field private _CurrentPaymentMethod:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

.field private _FreeString:Ljava/lang/String;

.field private _PermissionData:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

.field private _SelCoupon:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

.field private _SelGiftCard:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_Normal:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_CurrentPaymentMethod:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    .line 26
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_SelCoupon:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    .line 30
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_SelGiftCard:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    .line 33
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_PermissionData:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

    .line 34
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 35
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_FreeString:Ljava/lang/String;

    .line 36
    return-void
.end method

.method private getFormattedPrice(D)Ljava/lang/String;
    .locals 3

    .prologue
    .line 164
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatCreator;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatCreator;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getDetail()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->currencyUnit:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_FreeString:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatCreator;->createFormatter(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceFormatter;

    move-result-object v0

    .line 165
    invoke-interface {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceFormatter;->getFormattedPrice(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAppliedCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_SelCoupon:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    return-object v0
.end method

.method public getAppliedGiftCard()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;
    .locals 1

    .prologue
    .line 194
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getSelectedGiftCard()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    move-result-object v0

    return-object v0
.end method

.method public getBuyPrice()D
    .locals 6

    .prologue
    const-wide/16 v0, 0x0

    .line 65
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_SelCoupon:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    if-eqz v2, :cond_1

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_CurrentPaymentMethod:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->getPrice(Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;)Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;->getSellingPrice()D

    move-result-wide v0

    .line 68
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_SelCoupon:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    invoke-interface {v2, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->calcPaymentPrice(D)D

    move-result-wide v0

    .line 81
    :cond_0
    :goto_0
    return-wide v0

    .line 71
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_SelGiftCard:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    if-eqz v2, :cond_2

    .line 73
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_CurrentPaymentMethod:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->getPrice(Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;)Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;->getSellingPrice()D

    move-result-wide v2

    .line 74
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_SelGiftCard:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    invoke-interface {v4}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;->getBalanceAmount()D

    move-result-wide v4

    sub-double/2addr v2, v4

    .line 75
    cmpg-double v4, v2, v0

    if-ltz v4, :cond_0

    move-wide v0, v2

    goto :goto_0

    .line 81
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_CurrentPaymentMethod:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->getPrice(Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;)Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;->getSellingPrice()D

    move-result-wide v0

    goto :goto_0
.end method

.method public getCVC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_CVC:Ljava/lang/String;

    return-object v0
.end method

.method public getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    return-object v0
.end method

.method public bridge synthetic getContent()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    return-object v0
.end method

.method public getDetail()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    return-object v0
.end method

.method public getDisplayBuyPrice()D
    .locals 3

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_SelCoupon:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    if-eqz v0, :cond_1

    .line 88
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isPoland()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_GLOBAL_CREDIT_CARD:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->getPrice(Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;)Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;->getSellingPrice()D

    move-result-wide v0

    .line 98
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_SelCoupon:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    invoke-interface {v2, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->calcPaymentPrice(D)D

    move-result-wide v0

    .line 101
    :goto_1
    return-wide v0

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_CurrentPaymentMethod:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->getPrice(Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;)Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;->getSellingPrice()D

    move-result-wide v0

    goto :goto_0

    .line 101
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_CurrentPaymentMethod:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->getPrice(Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;)Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;->getSellingPrice()D

    move-result-wide v0

    goto :goto_1
.end method

.method public getFormatedPaymentPrice()Ljava/lang/String;
    .locals 2

    .prologue
    .line 154
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getBuyPrice()D

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getFormattedPrice(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFormattedNormalPrice()Ljava/lang/String;
    .locals 2

    .prologue
    .line 159
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getNormalPrice()D

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getFormattedPrice(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNormalPrice()D
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_CurrentPaymentMethod:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getDetail()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->getPrice(Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;)Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;->getNormalPrice()D

    move-result-wide v0

    return-wide v0
.end method

.method public getPermissionData()Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_PermissionData:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

    return-object v0
.end method

.method public getProduct()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    return-object v0
.end method

.method public getProductToBuy()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    return-object v0
.end method

.method public getSelectedCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_SelCoupon:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    return-object v0
.end method

.method public getSelectedGiftCard()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_SelGiftCard:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    return-object v0
.end method

.method public getSelectedPaymentMethod()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_CurrentPaymentMethod:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    return-object v0
.end method

.method public getSellingPrice()D
    .locals 2

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getBuyPrice()D

    move-result-wide v0

    return-wide v0
.end method

.method public hasCVC()Z
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_CVC:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_CVC:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDiscountedContent()Z
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    iget-boolean v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->discountFlag:Z

    return v0
.end method

.method public setCVC(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_CVC:Ljava/lang/String;

    .line 146
    return-void
.end method

.method public setPaymentMethod(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;)V
    .locals 0

    .prologue
    .line 131
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_CurrentPaymentMethod:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    .line 132
    return-void
.end method

.method public setSelCoupon(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;)V
    .locals 0

    .prologue
    .line 140
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_SelCoupon:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    .line 141
    return-void
.end method

.method public setSelGiftCard(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;)V
    .locals 0

    .prologue
    .line 189
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->_SelGiftCard:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    .line 190
    return-void
.end method
