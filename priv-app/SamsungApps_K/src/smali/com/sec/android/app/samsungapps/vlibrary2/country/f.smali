.class final Lcom/sec/android/app/samsungapps/vlibrary2/country/f;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;)V
    .locals 0

    .prologue
    .line 202
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/f;->a:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCommandResult(Z)V
    .locals 2

    .prologue
    .line 206
    if-eqz p1, :cond_0

    .line 208
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/f;->a:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->GEOIP_COUNTRY_SEARCH_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;)V

    .line 228
    :goto_0
    return-void

    .line 212
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/f;->a:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->mCountrySearchCommand:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/ReqCountrySearchExCommand;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 220
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/ReqCountrySearchExCommand;->getError()Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/ReqCountrySearchExCommand;->getError()Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorCode()Ljava/lang/String;

    move-result-object v0

    const-string v1, "1009"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 222
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/f;->a:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->GEOIP_COUNTRY_SEARCH_FAILED_BY_IP:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;)V

    .line 223
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/f;->a:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->mCountrySearchCommand:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    goto :goto_0

    .line 216
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/f;->a:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->GEOIP_COUNTRY_SEARCH_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;)V

    goto :goto_0

    .line 225
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/f;->a:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->GEOIP_COUNTRY_SEARCH_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchCommand;->sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;)V

    goto :goto_0
.end method
