.class public Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _PermissionData:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

.field requestor:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionRequestor;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;->requestor:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionRequestor;

    .line 27
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;->_PermissionData:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

    .line 28
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;Z)V
    .locals 0

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;->_PermissionData:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;Z)V
    .locals 0

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;Z)V
    .locals 0

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;Z)V
    .locals 0

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method protected askPermissionInfo()V
    .locals 3

    .prologue
    .line 37
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/a;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;->_Context:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;->_PermissionData:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;->getProductID()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;->requestor:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionRequestor;

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;->requestor:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionRequestor;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionRequestor;->request()V

    .line 68
    return-void
.end method

.method public cancel()V
    .locals 0

    .prologue
    .line 78
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->cancel()V

    .line 79
    return-void
.end method

.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 0

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;->askPermissionInfo()V

    .line 33
    return-void
.end method

.method protected onCancel()V
    .locals 0

    .prologue
    .line 72
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->onCancel()V

    .line 73
    return-void
.end method
