.class public Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;
.super Landroid/widget/TextView;
.source "ProGuard"


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field protected mListener:Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView$ITextSingleLineChanged;

.field protected mMaxLineCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 21
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 13
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->mMaxLineCount:I

    .line 14
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->a:I

    .line 15
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->b:I

    .line 16
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->c:I

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->mListener:Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView$ITextSingleLineChanged;

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 26
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 13
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->mMaxLineCount:I

    .line 14
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->a:I

    .line 15
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->b:I

    .line 16
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->c:I

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->mListener:Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView$ITextSingleLineChanged;

    .line 27
    return-void
.end method

.method private a(Z)V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->mListener:Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView$ITextSingleLineChanged;

    if-eqz v0, :cond_0

    .line 46
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/an;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/samsungapps/widget/an;-><init>(Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;Z)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->post(Ljava/lang/Runnable;)Z

    .line 53
    :cond_0
    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 7

    .prologue
    .line 59
    :try_start_0
    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->onMeasure(II)V

    .line 61
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->b:I

    .line 63
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->b:I

    if-lez v0, :cond_2

    .line 65
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 66
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->b:I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v0, v2

    iget v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->c:I

    sub-int/2addr v0, v2

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->a:I

    .line 68
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v0, v2, :cond_4

    .line 69
    iget v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->a:I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->getTextSize()F

    move-result v0

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    new-instance v4, Ljava/util/StringTokenizer;

    const-string v0, "\r\n"

    invoke-direct {v4, v1, v0}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v0

    const/4 v5, 0x1

    int-to-float v2, v2

    const/4 v6, 0x0

    invoke-virtual {v3, v1, v5, v2, v6}, Landroid/graphics/Paint;->breakText(Ljava/lang/String;ZF[F)I

    move-result v1

    :cond_0
    :goto_0
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v1, :cond_0

    div-int/2addr v2, v1

    add-int/2addr v0, v2

    goto :goto_0

    :cond_1
    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->mMaxLineCount:I

    if-le v0, v1, :cond_3

    .line 70
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->a(Z)V

    .line 86
    :cond_2
    :goto_1
    return-void

    .line 72
    :cond_3
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->a(Z)V

    goto :goto_1

    .line 86
    :catch_0
    move-exception v0

    goto :goto_1

    .line 75
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    iget v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->mMaxLineCount:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Landroid/text/Layout;->getLineVisibleEnd(I)I

    move-result v0

    if-lez v0, :cond_5

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    iget v2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->mMaxLineCount:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Landroid/text/Layout;->getLineVisibleEnd(I)I

    move-result v0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 77
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->a(Z)V

    goto :goto_1

    .line 79
    :cond_5
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->a(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1
.end method

.method public setExtraWidth(I)V
    .locals 0

    .prologue
    .line 36
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->c:I

    .line 37
    return-void
.end method

.method public setMaxLineCount(I)V
    .locals 0

    .prologue
    .line 31
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->mMaxLineCount:I

    .line 32
    return-void
.end method

.method public setOnTextSingleLineChangedListener(Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView$ITextSingleLineChanged;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->mListener:Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView$ITextSingleLineChanged;

    .line 42
    return-void
.end method
