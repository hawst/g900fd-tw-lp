.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum CHECK_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

.field public static final enum DOESNT_MATCH_CONDITON:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

.field public static final enum KOREA_AND_RESTRICTED_CONTENT:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

.field public static final enum LOGEDIN_AND_OVER19:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

.field public static final enum LOGEDIN_AND_UNDER19:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

.field public static final enum LOGED_OUT:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

.field public static final enum LOGIN_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

.field public static final enum LOGIN_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

.field public static final enum NEED_TO_IDENTIFY_USER:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

.field public static final enum NOT_LOGEDIN:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

.field public static final enum USER_AGREE:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

.field public static final enum USER_DISAGREE:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

.field public static final enum VERIFICATION_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

.field public static final enum VERIFICATION_SUCCEED:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 13
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    const-string v1, "CHECK_CONDITION"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->CHECK_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    const-string v1, "DOESNT_MATCH_CONDITON"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->DOESNT_MATCH_CONDITON:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    const-string v1, "KOREA_AND_RESTRICTED_CONTENT"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->KOREA_AND_RESTRICTED_CONTENT:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    const-string v1, "LOGEDIN_AND_OVER19"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->LOGEDIN_AND_OVER19:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    const-string v1, "LOGEDIN_AND_UNDER19"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->LOGEDIN_AND_UNDER19:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    const-string v1, "NOT_LOGEDIN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->NOT_LOGEDIN:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    const-string v1, "USER_AGREE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->USER_AGREE:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    const-string v1, "USER_DISAGREE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->USER_DISAGREE:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    const-string v1, "LOGIN_SUCCESS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->LOGIN_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    const-string v1, "LOGIN_FAILED"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->LOGIN_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    const-string v1, "LOGED_OUT"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->LOGED_OUT:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    const-string v1, "NEED_TO_IDENTIFY_USER"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->NEED_TO_IDENTIFY_USER:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    const-string v1, "VERIFICATION_FAILED"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->VERIFICATION_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    const-string v1, "VERIFICATION_SUCCEED"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->VERIFICATION_SUCCEED:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    .line 11
    const/16 v0, 0xe

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->CHECK_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->DOESNT_MATCH_CONDITON:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->KOREA_AND_RESTRICTED_CONTENT:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->LOGEDIN_AND_OVER19:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->LOGEDIN_AND_UNDER19:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->NOT_LOGEDIN:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->USER_AGREE:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->USER_DISAGREE:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->LOGIN_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->LOGIN_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->LOGED_OUT:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->NEED_TO_IDENTIFY_USER:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->VERIFICATION_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->VERIFICATION_SUCCEED:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlockerStm$Event;

    return-object v0
.end method
