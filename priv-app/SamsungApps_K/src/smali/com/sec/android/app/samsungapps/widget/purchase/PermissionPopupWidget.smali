.class public Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;
.super Lcom/sec/android/app/samsungapps/widget/CommonWidget;
.source "ProGuard"


# instance fields
.field a:Ljava/util/HashMap;

.field b:Landroid/widget/TextView;

.field d:Landroid/widget/TextView;

.field e:Z

.field f:[Ljava/lang/Object;

.field private g:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

.field private h:Ljava/util/ArrayList;

.field private i:Ljava/util/ArrayList;

.field private j:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field private k:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 43
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;)V

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->h:Ljava/util/ArrayList;

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->i:Ljava/util/ArrayList;

    .line 40
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->k:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    .line 44
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->mContext:Landroid/content/Context;

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->e:Z

    .line 46
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->b:Landroid/widget/TextView;

    .line 47
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->d:Landroid/widget/TextView;

    .line 48
    const v0, 0x7f0400a9

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->initView(Landroid/content/Context;I)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->h:Ljava/util/ArrayList;

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->i:Ljava/util/ArrayList;

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->k:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    .line 53
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->mContext:Landroid/content/Context;

    .line 54
    const v0, 0x7f0400a9

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->initView(Landroid/content/Context;I)V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->h:Ljava/util/ArrayList;

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->i:Ljava/util/ArrayList;

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->k:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    .line 60
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->mContext:Landroid/content/Context;

    .line 61
    const v0, 0x7f0400a9

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->initView(Landroid/content/Context;I)V

    .line 62
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;)Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->k:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;I)V
    .locals 11

    .prologue
    .line 29
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->k:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->e:Z

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->mContext:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f040096

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const v1, 0x7f0c0217

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->mContext:Landroid/content/Context;

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    if-lez p1, :cond_4

    add-int/lit8 v3, p1, -0x1

    move v4, v3

    :goto_0
    const/4 v5, 0x0

    const/4 v3, 0x0

    move v6, v5

    move v5, v3

    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->i:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v5, v3, :cond_7

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->i:Ljava/util/ArrayList;

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;->getGroupTitle()Ljava/lang/String;

    move-result-object v7

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->h:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;->getGroupTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    const v3, 0x7f040097

    const/4 v7, 0x0

    invoke-virtual {v2, v3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    const v3, 0x7f0c0219

    invoke-virtual {v7, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->b:Landroid/widget/TextView;

    const v3, 0x7f0c021a

    invoke-virtual {v7, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->d:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->b:Landroid/widget/TextView;

    const/4 v8, 0x0

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setSingleLine(Z)V

    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->i:Ljava/util/ArrayList;

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;->getLabel()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_5

    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->i:Ljava/util/ArrayList;

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;->getPermissionID()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    const-string v3, ""

    :cond_1
    :goto_2
    iget-object v8, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->b:Landroid/widget/TextView;

    invoke-virtual {v8, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->i:Ljava/util/ArrayList;

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;->getDescription()Ljava/lang/String;

    move-result-object v3

    iget-object v8, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->d:Landroid/widget/TextView;

    if-nez v3, :cond_2

    const-string v3, ""

    :cond_2
    invoke-virtual {v8, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_3
    if-nez v6, :cond_6

    const v3, 0x7f0c0218

    invoke-virtual {v7, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/16 v8, 0x8

    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    :goto_4
    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v6, v6, 0x1

    :cond_3
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto/16 :goto_1

    :cond_4
    const/4 v3, 0x0

    move v4, v3

    goto/16 :goto_0

    :cond_5
    const/4 v8, 0x0

    :try_start_1
    invoke-virtual {v3, v8}, Ljava/lang/String;->charAt(I)C

    move-result v8

    invoke-static {v8}, Ljava/lang/Character;->isLowerCase(C)Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-static {v8}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v8}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v8

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v9, v8}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v3

    goto :goto_2

    :cond_6
    const v3, 0x7f0c0218

    invoke-virtual {v7, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/4 v8, 0x0

    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    :cond_7
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->k:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->h:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;->getGroupTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->k:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setCancelable(Z)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->k:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->k:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->mContext:Landroid/content/Context;

    const v2, 0x7f0802ef

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->k:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->show()V

    return-void

    :catch_0
    move-exception v3

    goto :goto_3
.end method


# virtual methods
.method public loadWidget()V
    .locals 0

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->updateWidget()V

    .line 68
    return-void
.end method

.method public refreshWidget()V
    .locals 0

    .prologue
    .line 152
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 225
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->release()V

    .line 226
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->g:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

    .line 228
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->h:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 230
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->h:Ljava/util/ArrayList;

    .line 233
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->i:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 234
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 235
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->i:Ljava/util/ArrayList;

    .line 238
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->a:Ljava/util/HashMap;

    if-eqz v0, :cond_2

    .line 239
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 240
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->a:Ljava/util/HashMap;

    .line 243
    :cond_2
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->f:[Ljava/lang/Object;

    .line 244
    return-void
.end method

.method public setContent(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V
    .locals 0

    .prologue
    .line 247
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->j:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 249
    return-void
.end method

.method public setPermission(Ljava/util/HashMap;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 127
    invoke-virtual {p1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 128
    invoke-interface {v0}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->f:[Ljava/lang/Object;

    .line 130
    invoke-virtual {p1}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    move v1, v2

    .line 131
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->f:[Ljava/lang/Object;

    array-length v0, v0

    if-ge v1, v0, :cond_0

    .line 132
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->h:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->f:[Ljava/lang/Object;

    aget-object v0, v0, v1

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 133
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->i:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->f:[Ljava/lang/Object;

    aget-object v0, v0, v1

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 131
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 136
    :cond_0
    return-void
.end method

.method public setPermissionData(Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;)V
    .locals 1

    .prologue
    .line 139
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->g:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->g:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;->getPermissionList()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->a:Ljava/util/HashMap;

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->g:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;->existPermission()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->a:Ljava/util/HashMap;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->setPermission(Ljava/util/HashMap;)V

    .line 147
    :cond_0
    return-void
.end method

.method public setWidgetData(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 157
    return-void
.end method

.method public updateWidget()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 72
    const v0, 0x7f0c01fb

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 74
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->j:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    if-nez v1, :cond_0

    .line 117
    :goto_0
    return-void

    .line 80
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->j:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getPermission()Lcom/sec/android/app/samsungapps/vlibrary3/doc/Permission;

    move-result-object v2

    .line 84
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->mContext:Landroid/content/Context;

    const v3, 0x7f080166

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->g:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 87
    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/doc/Permission;->hasNewPermission()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 89
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->mContext:Landroid/content/Context;

    const v3, 0x7f080199

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->g:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 94
    :cond_1
    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/doc/Permission;->getPermissionMap()Ljava/util/HashMap;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/doc/Permission;->getPermissionMap()Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    if-nez v2, :cond_3

    .line 96
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->mContext:Landroid/content/Context;

    const v2, 0x7f080147

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->j:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 99
    :goto_1
    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setFocusableInTouchMode(Z)V

    .line 100
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->mContext:Landroid/content/Context;

    const-string v3, "layout_inflater"

    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 101
    const v3, 0x7f0400a8

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 102
    const v1, 0x7f0c0258

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 103
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 104
    invoke-virtual {v0, v3}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 106
    new-instance v1, Lcom/sec/android/app/samsungapps/view/PermissionPopupArrayAdapter;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->mContext:Landroid/content/Context;

    const v3, 0x7f0400a7

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;->h:Ljava/util/ArrayList;

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/view/PermissionPopupArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    .line 107
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 108
    new-instance v1, Lcom/sec/android/app/samsungapps/widget/purchase/y;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/y;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/PermissionPopupWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto/16 :goto_0

    :cond_3
    move-object v2, v1

    goto :goto_1
.end method
