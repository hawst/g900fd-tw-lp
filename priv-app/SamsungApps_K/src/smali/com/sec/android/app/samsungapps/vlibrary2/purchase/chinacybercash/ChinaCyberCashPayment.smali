.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _CyberCashActibity:Ljava/lang/Class;

.field private _FinalMap:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

.field private _ICyberCashView:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ICyberCashView;

.field private _PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Ljava/lang/Class;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 28
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;->_CyberCashActibity:Ljava/lang/Class;

    .line 29
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;->_FinalMap:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    .line 30
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    .line 31
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ICyberCashView;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;->_ICyberCashView:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ICyberCashView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;Z)V
    .locals 0

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method public getAppliedCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getAppliedCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    move-result-object v0

    return-object v0
.end method

.method public getCyberCashInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getDetail()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getCyberCashInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;

    move-result-object v0

    return-object v0
.end method

.method public getPaymentPrice()D
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getBuyPrice()D

    move-result-wide v0

    return-wide v0
.end method

.method public getProduct()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getProductToBuy()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    move-result-object v0

    return-object v0
.end method

.method public impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 0

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;->invokeUI(Landroid/content/Context;)V

    .line 36
    return-void
.end method

.method public invokeCompleted(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ICyberCashView;)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;->_ICyberCashView:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ICyberCashView;

    .line 46
    return-void
.end method

.method protected invokeUI(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;->_CyberCashActibity:Ljava/lang/Class;

    invoke-static {p1, v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->startActivityWithObject(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/Object;)V

    .line 41
    return-void
.end method

.method public requestPayment(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ICyberCashParam;)V
    .locals 3

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;->_ICyberCashView:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ICyberCashView;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ICyberCashView;->onPaymentRequest()V

    .line 56
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;->_FinalMap:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/a;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;)V

    invoke-virtual {v0, p1, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->cyberCashPurchase(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ICyberCashParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 65
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinacybercash/ChinaCyberCashPayment;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 67
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 68
    return-void
.end method
