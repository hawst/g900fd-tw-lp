.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/DirectBillingPurchaseXML;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
.source "ProGuard"


# direct methods
.method private constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 19
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 20
    return-void
.end method

.method public static confirm(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/IDirectBillingConfirmParam;Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/DirectBillingPurchaseXML;
    .locals 4

    .prologue
    .line 72
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/DirectBillingPurchaseXML;

    const-string v1, "ConfirmOptBillingPurchase"

    const-string v2, "3160"

    invoke-direct {v0, p1, v1, v2, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/DirectBillingPurchaseXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;I)V

    .line 74
    const-string v1, "orderID"

    invoke-interface {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/IDirectBillingConfirmParam;->getOrderID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/DirectBillingPurchaseXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    const-string v1, "paymentID"

    invoke-interface {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/IDirectBillingConfirmParam;->getPaymentID()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/DirectBillingPurchaseXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 77
    return-object v0
.end method

.method public static init(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/IDirectBillingInitParam;Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/DirectBillingPurchaseXML;
    .locals 7

    .prologue
    .line 31
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/xml/DirectBillingPurchaseXML;

    const-string v0, "InitiateOptBillingPurchase"

    const-string v1, "3150"

    invoke-direct {v2, p1, v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/DirectBillingPurchaseXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;I)V

    .line 33
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v3

    .line 34
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v4

    .line 35
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getIMEI()Ljava/lang/String;

    move-result-object v5

    .line 36
    const-string v0, ""

    .line 37
    if-eqz v3, :cond_0

    .line 39
    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getMSISDN()Ljava/lang/String;

    move-result-object v0

    .line 42
    :cond_0
    const-string v1, ""

    .line 43
    if-eqz v3, :cond_1

    .line 45
    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getIMSI()Ljava/lang/String;

    move-result-object v1

    .line 48
    :cond_1
    const-string v3, "productID"

    invoke-interface {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/IDirectBillingInitParam;->getProduct()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    move-result-object v6

    invoke-interface {v6}, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;->getProductID()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v3, v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/DirectBillingPurchaseXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    const-string v3, "imei"

    const/4 v6, 0x1

    invoke-virtual {v2, v3, v5, v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/DirectBillingPurchaseXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 50
    const-string v3, "guid"

    invoke-interface {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/IDirectBillingInitParam;->getProduct()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    move-result-object v5

    invoke-interface {v5}, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;->getGUID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/DirectBillingPurchaseXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    const-string v3, "mcc"

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMCC()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/DirectBillingPurchaseXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    const-string v3, "mnc"

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMNC()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/DirectBillingPurchaseXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    const-string v3, "msisdn"

    invoke-virtual {v2, v3, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/DirectBillingPurchaseXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    const-string v0, "imsi"

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/DirectBillingPurchaseXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    const-string v0, "reserved01"

    const-string v1, ""

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/DirectBillingPurchaseXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    const-string v0, "reserved02"

    const-string v1, ""

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/DirectBillingPurchaseXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    const-string v0, "reserved03"

    const-string v1, ""

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/DirectBillingPurchaseXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    const-string v0, "reserved04"

    const-string v1, ""

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/DirectBillingPurchaseXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    return-object v2
.end method
