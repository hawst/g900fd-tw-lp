.class public abstract Lcom/sec/android/app/samsungapps/view/MyListView;
.super Landroid/widget/ListView;
.source "ProGuard"


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/view/ViewList;

.field private b:Lcom/sec/android/app/samsungapps/view/ViewList;

.field private c:Lcom/sec/android/app/samsungapps/view/ViewList;

.field private d:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/MyListView;->d:Ljava/util/ArrayList;

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/MyListView;->d:Ljava/util/ArrayList;

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/MyListView;->d:Ljava/util/ArrayList;

    .line 21
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MyListView;->c:Lcom/sec/android/app/samsungapps/view/ViewList;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MyListView;->c:Lcom/sec/android/app/samsungapps/view/ViewList;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 82
    :cond_0
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MyListView;->a:Lcom/sec/android/app/samsungapps/view/ViewList;

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MyListView;->a:Lcom/sec/android/app/samsungapps/view/ViewList;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 89
    :cond_0
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MyListView;->b:Lcom/sec/android/app/samsungapps/view/ViewList;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MyListView;->b:Lcom/sec/android/app/samsungapps/view/ViewList;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 96
    :cond_0
    return-void
.end method


# virtual methods
.method public addStateObserver(Lcom/sec/android/app/samsungapps/view/MyListView$ListViewObserver;)V
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MyListView;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 107
    return-void
.end method

.method protected abstract empty()Z
.end method

.method public notifyLoadCompleted()V
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MyListView;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/view/MyListView$ListViewObserver;

    .line 126
    invoke-interface {v0, p0}, Lcom/sec/android/app/samsungapps/view/MyListView$ListViewObserver;->loadCompleted(Landroid/view/View;)V

    goto :goto_0

    .line 128
    :cond_0
    return-void
.end method

.method public notifyLoading()V
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MyListView;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/view/MyListView$ListViewObserver;

    .line 118
    invoke-interface {v0, p0}, Lcom/sec/android/app/samsungapps/view/MyListView$ListViewObserver;->loading(Landroid/view/View;)V

    goto :goto_0

    .line 120
    :cond_0
    return-void
.end method

.method public release()V
    .locals 0

    .prologue
    .line 102
    return-void
.end method

.method public removeStateObserver(Lcom/sec/android/app/samsungapps/view/MyListView$ListViewObserver;)V
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MyListView;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 112
    return-void
.end method

.method public setEmptyViews(Lcom/sec/android/app/samsungapps/view/ViewList;Lcom/sec/android/app/samsungapps/view/ViewList;Lcom/sec/android/app/samsungapps/view/ViewList;)V
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/MyListView;->a:Lcom/sec/android/app/samsungapps/view/ViewList;

    .line 34
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/view/MyListView;->b:Lcom/sec/android/app/samsungapps/view/ViewList;

    .line 35
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/view/MyListView;->c:Lcom/sec/android/app/samsungapps/view/ViewList;

    .line 36
    return-void
.end method

.method public setLoadingEmptyView()V
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MyListView;->b:Lcom/sec/android/app/samsungapps/view/ViewList;

    if-eqz v0, :cond_1

    .line 42
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/MyListView;->b()V

    .line 43
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/MyListView;->a()V

    .line 44
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/MyListView;->empty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MyListView;->b:Lcom/sec/android/app/samsungapps/view/ViewList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 47
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MyListView;->b:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ViewList;->getTopView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/MyListView;->setEmptyView(Landroid/view/View;)V

    .line 49
    :cond_1
    return-void
.end method

.method public setNoDataEmptyView()V
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MyListView;->a:Lcom/sec/android/app/samsungapps/view/ViewList;

    if-eqz v0, :cond_1

    .line 55
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/MyListView;->c()V

    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/MyListView;->a()V

    .line 57
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/MyListView;->empty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MyListView;->a:Lcom/sec/android/app/samsungapps/view/ViewList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MyListView;->a:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ViewList;->getTopView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/MyListView;->setEmptyView(Landroid/view/View;)V

    .line 62
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/MyListView;->invalidate()V

    .line 63
    return-void
.end method

.method public setRetryView()V
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MyListView;->c:Lcom/sec/android/app/samsungapps/view/ViewList;

    if-eqz v0, :cond_1

    .line 67
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/MyListView;->c()V

    .line 68
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/MyListView;->b()V

    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/MyListView;->empty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MyListView;->c:Lcom/sec/android/app/samsungapps/view/ViewList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MyListView;->c:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ViewList;->getTopView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/MyListView;->setEmptyView(Landroid/view/View;)V

    .line 74
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/MyListView;->invalidate()V

    .line 75
    return-void
.end method
