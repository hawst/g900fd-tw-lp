.class final Lcom/sec/android/app/samsungapps/widget/list/s;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;)V
    .locals 0

    .prologue
    .line 299
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/s;->a:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCommandResult(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 304
    if-ne p1, v2, :cond_0

    .line 306
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/s;->a:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->d(Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;)Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_1

    .line 328
    :cond_0
    :goto_0
    return-void

    .line 312
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/s;->a:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->e(Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/RegisterGiftCardResponseItem;

    move-result-object v0

    iget v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/RegisterGiftCardResponseItem;->registrationType:I

    if-nez v0, :cond_2

    .line 314
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/s;->a:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->f(Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f08018f

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 315
    invoke-static {}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->getInstance()Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->showGiftCards()V

    goto :goto_0

    .line 318
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/s;->a:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->e(Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/RegisterGiftCardResponseItem;

    move-result-object v0

    iget v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/RegisterGiftCardResponseItem;->registrationType:I

    if-ne v0, v2, :cond_0

    .line 320
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/s;->a:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->g(Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0801ad

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 321
    invoke-static {}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->getInstance()Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->showVouchers()V

    goto :goto_0
.end method
