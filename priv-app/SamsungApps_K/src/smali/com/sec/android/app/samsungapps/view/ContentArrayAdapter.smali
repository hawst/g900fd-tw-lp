.class public Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;
.super Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/view/IContentArrayAdapter;
.implements Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$ContentListQueryObserver;
.implements Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOptionQuery;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue$DLStateQueueObserver;


# instance fields
.field a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

.field b:Z

.field c:Z

.field final d:Ljava/lang/String;

.field final e:Ljava/lang/String;

.field f:Z

.field private g:Z

.field private h:Landroid/view/View;

.field private i:Lcom/sec/android/app/samsungapps/widget/interfaces/IAdapterObserver;


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 59
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;-><init>(Landroid/content/Context;II)V

    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    .line 33
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->b:Z

    .line 34
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->c:Z

    .line 35
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->g:Z

    .line 335
    const-string v0, "Junos Pulse for Galaxy S and Tab 7.0"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->d:Ljava/lang/String;

    .line 336
    const-string v0, "Junos Pulse"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->e:Ljava/lang/String;

    .line 338
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->f:Z

    .line 60
    new-instance v0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->_ListViewInfo:Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;

    .line 61
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->addObserver(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue$DLStateQueueObserver;)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 65
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;I)V

    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    .line 33
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->b:Z

    .line 34
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->c:Z

    .line 35
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->g:Z

    .line 335
    const-string v0, "Junos Pulse for Galaxy S and Tab 7.0"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->d:Ljava/lang/String;

    .line 336
    const-string v0, "Junos Pulse"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->e:Ljava/lang/String;

    .line 338
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->f:Z

    .line 66
    new-instance v0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->_ListViewInfo:Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;

    .line 67
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->addObserver(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue$DLStateQueueObserver;)V

    .line 68
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)Landroid/view/View;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->h:Landroid/view/View;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)Z
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->g:Z

    return v0
.end method

.method public static create(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;)Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;
    .locals 3

    .prologue
    .line 95
    new-instance v0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    const v1, 0x7f04004d

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;-><init>(Landroid/content/Context;II)V

    .line 96
    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->setQuery(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;)V

    .line 98
    return-object v0
.end method

.method public static createBannerProductList(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;)Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;
    .locals 3

    .prologue
    .line 124
    new-instance v0, Lcom/sec/android/app/samsungapps/view/BannerArrayAdapter;

    const v1, 0x7f04004d

    const/16 v2, 0x63

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/samsungapps/view/BannerArrayAdapter;-><init>(Landroid/content/Context;II)V

    .line 125
    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/view/BannerArrayAdapter;->setQuery(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;)V

    .line 127
    return-object v0
.end method

.method public static createContenViewList(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;)Ljava/util/ArrayList;
    .locals 3

    .prologue
    .line 250
    if-nez p0, :cond_0

    .line 251
    const/4 v0, 0x0

    .line 259
    :goto_0
    return-object v0

    .line 254
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 255
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 256
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 259
    goto :goto_0
.end method

.method public static createContenViewList(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 3

    .prologue
    .line 263
    if-nez p0, :cond_0

    .line 264
    const/4 v0, 0x0

    .line 272
    :goto_0
    return-object v0

    .line 267
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 268
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 269
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 272
    goto :goto_0
.end method

.method public static createPurchase(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter$IUpdateAllButton;Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;)Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;
    .locals 2

    .prologue
    .line 103
    new-instance v0, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;

    const v1, 0x7f040010

    invoke-direct {v0, p0, v1, p2, p3}, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;-><init>(Landroid/content/Context;ILcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter$IUpdateAllButton;Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;)V

    .line 104
    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/view/PurchaseContentArrayAdapter;->setQuery(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;)V

    .line 105
    return-object v0
.end method

.method public static createSellerPageProductList(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;)Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;
    .locals 3

    .prologue
    .line 133
    new-instance v0, Lcom/sec/android/app/samsungapps/view/SellerPageProductArrayAdapter;

    const v1, 0x7f04004d

    const/16 v2, 0x63

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/samsungapps/view/SellerPageProductArrayAdapter;-><init>(Landroid/content/Context;II)V

    .line 134
    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/view/SellerPageProductArrayAdapter;->setQuery(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;)V

    .line 136
    return-object v0
.end method

.method public static createSellerProductList(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;)Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;
    .locals 3

    .prologue
    .line 116
    new-instance v0, Lcom/sec/android/app/samsungapps/view/SellerArrayAdapter;

    const v1, 0x7f04004d

    const/16 v2, 0x63

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/samsungapps/view/SellerArrayAdapter;-><init>(Landroid/content/Context;II)V

    .line 117
    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/view/SellerArrayAdapter;->setQuery(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;)V

    .line 119
    return-object v0
.end method

.method public static createSeriesList(Landroid/content/Context;Ljava/util/ArrayList;)Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;
    .locals 4

    .prologue
    .line 110
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->createContenViewList(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    .line 111
    new-instance v1, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    const v2, 0x7f04004d

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, v0, v3}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;I)V

    return-object v1
.end method


# virtual methods
.method public changeSortOrder(Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;)Z
    .locals 1

    .prologue
    .line 461
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-eqz v0, :cond_0

    .line 462
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->changeSortOrder(Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;)Z

    .line 464
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public contentListGetCompleted(Z)V
    .locals 2

    .prologue
    .line 167
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->b:Z

    .line 168
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->loadCompleted(Z)V

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->h:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->h:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 173
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->updateView(ZZ)V

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->i:Lcom/sec/android/app/samsungapps/widget/interfaces/IAdapterObserver;

    if-eqz v0, :cond_1

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->i:Lcom/sec/android/app/samsungapps/widget/interfaces/IAdapterObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IAdapterObserver;->onDataLoadCompleted()V

    .line 178
    :cond_1
    return-void
.end method

.method public contentListLoading()V
    .locals 1

    .prologue
    .line 183
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->b:Z

    .line 184
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->clear()V

    .line 185
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->loading()V

    .line 186
    return-void
.end method

.method public doesSupportSortOption()Z
    .locals 1

    .prologue
    .line 453
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-eqz v0, :cond_0

    .line 454
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->doesSupportSortOption()Z

    move-result v0

    .line 456
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public finishGettingMoreContent(Z)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 234
    if-eqz p1, :cond_1

    .line 235
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->h:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->h:Landroid/view/View;

    const v1, 0x7f0c0177

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 237
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->h:Landroid/view/View;

    const v1, 0x7f0c007f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 238
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->h:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 240
    :cond_0
    invoke-virtual {p0, p1, v2}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->updateView(ZZ)V

    .line 241
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->b:Z

    .line 242
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->c:Z

    .line 247
    :goto_0
    return-void

    .line 244
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->retryMoreItem(Z)V

    goto :goto_0
.end method

.method public getContentListQuery()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    return-object v0
.end method

.method public getIndexOfFirstItem()I
    .locals 1

    .prologue
    .line 444
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v0

    if-nez v0, :cond_1

    .line 445
    :cond_0
    const/4 v0, 0x0

    .line 447
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->getIndexOfFirstItem()I

    move-result v0

    goto :goto_0
.end method

.method public getListType()I
    .locals 1

    .prologue
    .line 499
    iget v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->mListType:I

    return v0
.end method

.method public getPaidTypeFilter()I
    .locals 1

    .prologue
    .line 479
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-eqz v0, :cond_0

    .line 480
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getPaidTypeFilter()I

    move-result v0

    .line 482
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSortOrderToString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 468
    const/4 v0, 0x0

    .line 470
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-eqz v1, :cond_0

    .line 471
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getSortOrder()Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 474
    :cond_0
    return-object v0
.end method

.method public isCompleteEmptyList()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 519
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->isCompleted()Z

    move-result v1

    if-ne v1, v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->size()I

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLoading()Z
    .locals 1

    .prologue
    .line 495
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->b:Z

    return v0
.end method

.method public isMoreLoadingViewShown()Z
    .locals 1

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->c:Z

    return v0
.end method

.method public isSearchList()Z
    .locals 1

    .prologue
    .line 514
    const/4 v0, 0x0

    return v0
.end method

.method public onDLStateAdded(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V
    .locals 0

    .prologue
    .line 504
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->notifyDataSetChanged()V

    .line 505
    return-void
.end method

.method public onDLStateRemoved(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V
    .locals 0

    .prologue
    .line 509
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->notifyDataSetChanged()V

    .line 510
    return-void
.end method

.method public refreshUpdateAllMenu(I)V
    .locals 0

    .prologue
    .line 88
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    .line 417
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-eqz v0, :cond_1

    .line 418
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getType()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    move-result-object v0

    .line 419
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopPaid:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    if-ne v0, v1, :cond_3

    .line 420
    sget-object v0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopFree:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getContentQuery(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v0

    .line 421
    if-eqz v0, :cond_0

    .line 422
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->initAllListFirstItem()V

    .line 430
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->initAllListFirstItem()V

    .line 433
    :cond_1
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->release()V

    .line 434
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->removeObserver(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue$DLStateQueueObserver;)V

    .line 435
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-eqz v0, :cond_2

    .line 436
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->removeObserver(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$ContentListQueryObserver;)Z

    .line 438
    :cond_2
    return-void

    .line 424
    :cond_3
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopFree:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    if-ne v0, v1, :cond_0

    .line 425
    sget-object v0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopPaid:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getContentQuery(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v0

    .line 426
    if-eqz v0, :cond_0

    .line 427
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->initAllListFirstItem()V

    goto :goto_0
.end method

.method public requestDataGet()V
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    sget-object v1, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->requestDataGet(Landroid/content/Context;)V

    .line 82
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->b:Z

    .line 84
    :cond_0
    return-void
.end method

.method public requestMoreData()V
    .locals 2

    .prologue
    .line 403
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    sget-object v1, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->requestMoreData(Landroid/content/Context;)V

    .line 404
    return-void
.end method

.method public retryMoreItem(Z)V
    .locals 5

    .prologue
    const v4, 0x7f0c0177

    const v3, 0x7f0c007f

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->h:Landroid/view/View;

    if-nez v0, :cond_0

    .line 228
    :goto_0
    return-void

    .line 205
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 206
    if-eqz p1, :cond_1

    .line 207
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->g:Z

    .line 208
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->h:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 209
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->h:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->h:Landroid/view/View;

    const v1, 0x7f0c0086

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 213
    new-instance v1, Lcom/sec/android/app/samsungapps/view/k;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/view/k;-><init>(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 224
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->h:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 225
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->h:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public setAdapterObserver(Lcom/sec/android/app/samsungapps/widget/interfaces/IAdapterObserver;)V
    .locals 0

    .prologue
    .line 161
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->i:Lcom/sec/android/app/samsungapps/widget/interfaces/IAdapterObserver;

    .line 162
    return-void
.end method

.method public setIndexOfFirstItem(I)V
    .locals 1

    .prologue
    .line 410
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 411
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->setIndexOfFirstItem(I)V

    .line 413
    :cond_0
    return-void
.end method

.method public setMoreView(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->h:Landroid/view/View;

    .line 47
    return-void
.end method

.method public setPaidTypeFilter(I)V
    .locals 1

    .prologue
    .line 487
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-eqz v0, :cond_0

    .line 488
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->setPaidTypeFilter(I)V

    .line 490
    :cond_0
    return-void
.end method

.method public setQuery(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;)V
    .locals 2

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->addObserver(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$ContentListQueryObserver;)V

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    sget-object v1, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->requestDataGet(Landroid/content/Context;)V

    .line 75
    return-void
.end method

.method public showMoreLoadingView(Z)V
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->h:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 148
    if-eqz p1, :cond_1

    .line 149
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->g:Z

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->retryMoreItem(Z)V

    .line 156
    :cond_0
    :goto_0
    return-void

    .line 152
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->h:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public startGettingMoreContent()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->h:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 192
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->retryMoreItem(Z)V

    .line 194
    :cond_0
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->b:Z

    .line 195
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->c:Z

    .line 196
    return-void
.end method

.method public updateView(ZZ)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 343
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 344
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v3

    .line 345
    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->lock()V

    .line 347
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->getCount()I

    move-result v2

    .line 348
    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->size()I

    move-result v4

    .line 360
    iget-boolean v5, p0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->f:Z

    if-eqz v5, :cond_0

    move v0, v1

    :cond_0
    add-int/2addr v0, v2

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_4

    .line 361
    invoke-virtual {v3, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 362
    if-eqz v0, :cond_2

    sget-object v5, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;->isRemoveContent(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 363
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->add(Ljava/lang/Object;)V

    .line 360
    :cond_1
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 365
    :cond_2
    if-eqz v0, :cond_1

    .line 366
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "JunosPulse checked : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->i(Ljava/lang/String;)V

    goto :goto_1

    .line 399
    :catch_0
    move-exception v0

    :cond_3
    :goto_2
    return-void

    .line 371
    :cond_4
    if-eqz p1, :cond_7

    .line 374
    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->isCompleted()Z

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->neverLoaded()Z

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_6

    .line 377
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->getCount()I

    move-result v0

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->getIndexOfLastItem()I

    move-result v2

    if-ge v0, v2, :cond_7

    .line 378
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->setHasLoading(Z)V

    .line 388
    :goto_3
    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->unlock()V

    .line 392
    :cond_5
    if-ne p2, v1, :cond_3

    .line 393
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->onUpdateView()V

    goto :goto_2

    .line 383
    :cond_6
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->setHasLoading(Z)V

    goto :goto_3

    .line 386
    :cond_7
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->setHasLoading(Z)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3
.end method
