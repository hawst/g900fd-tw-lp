.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilderFactory;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private mContext:Landroid/content/Context;

.field protected mErrorPreProcessor:Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;

.field protected mRequestPostFactory:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOSTFactory;

.field protected mRequestSendingProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOSTFactory;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilderFactory;->mErrorPreProcessor:Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;

    .line 13
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilderFactory;->mRequestSendingProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    .line 14
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilderFactory;->mRequestPostFactory:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOSTFactory;

    .line 22
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilderFactory;->mContext:Landroid/content/Context;

    .line 23
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilderFactory;->mErrorPreProcessor:Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;

    .line 24
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilderFactory;->mRequestSendingProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    .line 25
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilderFactory;->mRequestPostFactory:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOSTFactory;

    .line 26
    return-void
.end method


# virtual methods
.method public createRequestBuilder(Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;)Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;
    .locals 7

    .prologue
    .line 29
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilderFactory;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilderFactory;->mErrorPreProcessor:Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilderFactory;->mRequestSendingProgressObserver:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilderFactory;->mRequestPostFactory:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOSTFactory;

    move-object v2, p2

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOSTFactory;Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;)V

    .line 32
    return-object v0
.end method
