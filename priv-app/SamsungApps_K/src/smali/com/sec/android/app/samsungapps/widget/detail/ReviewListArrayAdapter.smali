.class public Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ProGuard"


# instance fields
.field a:I

.field b:I

.field private c:Landroid/content/Context;

.field private d:Ljava/util/ArrayList;

.field private e:Landroid/view/LayoutInflater;

.field private f:Lcom/sec/android/app/samsungapps/widget/interfaces/ICommentListWidgetClickListener;

.field private g:Ljava/lang/String;

.field private h:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailUserReviewWidgetClickListener;

.field private i:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 33
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 23
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->a:I

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->g:Ljava/lang/String;

    .line 30
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->i:Z

    .line 34
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->i:Z

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 39
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 23
    iput v6, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->a:I

    .line 28
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->g:Ljava/lang/String;

    .line 30
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->i:Z

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v4, p3

    move-object v5, v3

    .line 40
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->init(Landroid/content/Context;ILjava/util/ArrayList;I[Z)V

    .line 41
    iput-boolean v6, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->i:Z

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;ILcom/sec/android/app/samsungapps/widget/interfaces/ICommentListWidgetClickListener;[Z)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 46
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 23
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->a:I

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->g:Ljava/lang/String;

    .line 30
    iput-boolean v6, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->i:Z

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p6

    .line 47
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->init(Landroid/content/Context;ILjava/util/ArrayList;I[Z)V

    .line 49
    iput-object p5, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->f:Lcom/sec/android/app/samsungapps/widget/interfaces/ICommentListWidgetClickListener;

    .line 50
    iput-boolean v6, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->i:Z

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;ILcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailUserReviewWidgetClickListener;)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 55
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 23
    iput v6, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->a:I

    .line 28
    iput-object v5, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->g:Ljava/lang/String;

    .line 30
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->i:Z

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    .line 56
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->init(Landroid/content/Context;ILjava/util/ArrayList;I[Z)V

    .line 58
    iput-object p5, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailUserReviewWidgetClickListener;

    .line 59
    iput-boolean v6, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->i:Z

    .line 60
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;)Lcom/sec/android/app/samsungapps/widget/interfaces/ICommentListWidgetClickListener;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->f:Lcom/sec/android/app/samsungapps/widget/interfaces/ICommentListWidgetClickListener;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;)Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailUserReviewWidgetClickListener;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailUserReviewWidgetClickListener;

    return-object v0
.end method


# virtual methods
.method public getCommentView(ILandroid/view/View;Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;I)Landroid/view/View;
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 106
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;

    .line 108
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;->isSeller()Z

    move-result v4

    .line 110
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->g:Ljava/lang/String;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->g:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;->compareUserID(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    move v1, v2

    .line 113
    :goto_0
    if-eq p4, v7, :cond_2

    .line 116
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;->getExpertUpdateDate()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 119
    iget-object v5, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->c:Landroid/content/Context;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;->getExpertUpdateDate()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/samsungapps/vlibrary2/util/AppsDateFormat;->getSystemDateItem(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p3, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->date:Ljava/lang/String;

    .line 125
    :goto_1
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;->getComment()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p3, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->review:Ljava/lang/String;

    .line 127
    if-ne p4, v7, :cond_6

    .line 129
    if-nez p1, :cond_3

    .line 130
    iget-object v3, p3, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->v_itemDivider:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    .line 131
    iget-boolean v3, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->i:Z

    if-nez v3, :cond_0

    .line 132
    iget-object v3, p3, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->v_itemGap:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    .line 138
    :cond_0
    :goto_2
    if-eqz v4, :cond_4

    .line 139
    if-eqz p3, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->c:Landroid/content/Context;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;->getDate()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/util/AppsDateFormat;->getSystemDateItem(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p3, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->sellerdate:Ljava/lang/String;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;->getComment()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p3, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->sellerreview:Ljava/lang/String;

    .line 170
    :cond_1
    :goto_3
    invoke-virtual {p3, p4, v1, v4}, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->setHolder(IZZ)V

    .line 172
    return-object p2

    .line 121
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->c:Landroid/content/Context;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;->getDate()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/samsungapps/vlibrary2/util/AppsDateFormat;->getSystemDateItem(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p3, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->date:Ljava/lang/String;

    goto :goto_1

    .line 135
    :cond_3
    iget-object v5, p3, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->v_itemDivider:Landroid/view/View;

    invoke-virtual {v5, v3}, Landroid/view/View;->setVisibility(I)V

    .line 136
    iget-object v5, p3, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->v_itemGap:Landroid/view/View;

    invoke-virtual {v5, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 141
    :cond_4
    if-eqz p3, :cond_1

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;->getComment()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p3, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->review:Ljava/lang/String;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;->getLoginID()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p3, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->c:Landroid/content/Context;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;->getDate()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/android/app/samsungapps/vlibrary2/util/AppsDateFormat;->getSystemDateItem(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p3, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->date:Ljava/lang/String;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;->getRating()I

    move-result v3

    int-to-float v3, v3

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v3, v5

    iput v3, p3, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->ratingCntr:F

    if-eqz v1, :cond_1

    iget-boolean v3, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->i:Z

    if-ne v3, v2, :cond_5

    iget-object v2, p3, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->btn_editBtn:Landroid/widget/Button;

    new-instance v3, Lcom/sec/android/app/samsungapps/widget/detail/aa;

    invoke-direct {v3, p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/aa;-><init>(Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p3, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->btn_deleteBtn:Landroid/widget/Button;

    new-instance v3, Lcom/sec/android/app/samsungapps/widget/detail/ab;

    invoke-direct {v3, p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ab;-><init>(Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_3

    :cond_5
    iget-object v2, p3, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->btn_editBtn:Landroid/widget/Button;

    new-instance v3, Lcom/sec/android/app/samsungapps/widget/detail/ac;

    invoke-direct {v3, p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ac;-><init>(Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p3, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->btn_deleteBtn:Landroid/widget/Button;

    new-instance v3, Lcom/sec/android/app/samsungapps/widget/detail/ad;

    invoke-direct {v3, p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ad;-><init>(Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_3

    .line 146
    :cond_6
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;->getName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p3, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->name:Ljava/lang/String;

    .line 147
    iget-object v2, p3, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->ly_expert_main:Landroid/widget/LinearLayout;

    new-instance v3, Lcom/sec/android/app/samsungapps/widget/detail/y;

    invoke-direct {v3, p0, p3}, Lcom/sec/android/app/samsungapps/widget/detail/y;-><init>(Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;)V

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 157
    iget-object v2, p3, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->ly_expert_link:Landroid/widget/LinearLayout;

    new-instance v3, Lcom/sec/android/app/samsungapps/widget/detail/z;

    invoke-direct {v3, p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/z;-><init>(Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;)V

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 166
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;->getExpertTitle()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p3, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->cafeName:Ljava/lang/String;

    .line 167
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;->getExpertUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p3, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->cafeUrl:Ljava/lang/String;

    goto/16 :goto_3

    :cond_7
    move v1, v3

    goto/16 :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 89
    .line 92
    if-nez p2, :cond_0

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->e:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->a:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 94
    new-instance v0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->c:Landroid/content/Context;

    invoke-direct {v0, p0, v1, p2}, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;-><init>(Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;Landroid/content/Context;Landroid/view/View;)V

    .line 95
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 100
    :goto_0
    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->b:I

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->getCommentView(ILandroid/view/View;Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;I)Landroid/view/View;

    move-result-object v0

    .line 102
    return-object v0

    .line 98
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;

    goto :goto_0
.end method

.method public init(Landroid/content/Context;ILjava/util/ArrayList;I[Z)V
    .locals 2

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->c:Landroid/content/Context;

    .line 65
    iput p2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->a:I

    .line 66
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->d:Ljava/util/ArrayList;

    .line 68
    iput p4, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->b:I

    .line 69
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->e:Landroid/view/LayoutInflater;

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    .line 73
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->c:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->isLogedIn()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 74
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->userID:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->g:Ljava/lang/String;

    .line 76
    :cond_0
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 79
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->c:Landroid/content/Context;

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 82
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->d:Ljava/util/ArrayList;

    .line 84
    :cond_0
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->e:Landroid/view/LayoutInflater;

    .line 85
    return-void
.end method
