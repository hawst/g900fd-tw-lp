.class public Lcom/sec/android/app/samsungapps/view/MenuView;
.super Landroid/widget/ListView;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/MenuItemList$MenuItemListObserver;


# instance fields
.field a:Lcom/sec/android/app/samsungapps/view/ar;

.field b:Lcom/sec/android/app/samsungapps/MenuItemList;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 23
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/MenuView;->a:Lcom/sec/android/app/samsungapps/view/ar;

    .line 24
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/MenuView;->b:Lcom/sec/android/app/samsungapps/MenuItemList;

    .line 37
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/view/MenuView;->a(Landroid/content/Context;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/MenuView;->a:Lcom/sec/android/app/samsungapps/view/ar;

    .line 24
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/MenuView;->b:Lcom/sec/android/app/samsungapps/MenuItemList;

    .line 32
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/view/MenuView;->a(Landroid/content/Context;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 26
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/MenuView;->a:Lcom/sec/android/app/samsungapps/view/ar;

    .line 24
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/MenuView;->b:Lcom/sec/android/app/samsungapps/MenuItemList;

    .line 27
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/view/MenuView;->a(Landroid/content/Context;)V

    .line 28
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/view/MenuView;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MenuView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 42
    new-instance v0, Lcom/sec/android/app/samsungapps/view/ar;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/samsungapps/view/ar;-><init>(Lcom/sec/android/app/samsungapps/view/MenuView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/MenuView;->a:Lcom/sec/android/app/samsungapps/view/ar;

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MenuView;->a:Lcom/sec/android/app/samsungapps/view/ar;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/MenuView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 44
    new-instance v0, Lcom/sec/android/app/samsungapps/view/aq;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/view/aq;-><init>(Lcom/sec/android/app/samsungapps/view/MenuView;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/MenuView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 52
    return-void
.end method


# virtual methods
.method public getMenuItemList()Lcom/sec/android/app/samsungapps/MenuItemList;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MenuView;->b:Lcom/sec/android/app/samsungapps/MenuItemList;

    return-object v0
.end method

.method public menuItemAdded(Lcom/sec/android/app/samsungapps/MenuItem;)V
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MenuView;->a:Lcom/sec/android/app/samsungapps/view/ar;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/view/ar;->add(Ljava/lang/Object;)V

    .line 160
    return-void
.end method

.method public menuItemRemoved(Lcom/sec/android/app/samsungapps/MenuItem;)V
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MenuView;->a:Lcom/sec/android/app/samsungapps/view/ar;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/view/ar;->remove(Ljava/lang/Object;)V

    .line 166
    return-void
.end method

.method public release()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MenuView;->a:Lcom/sec/android/app/samsungapps/view/ar;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ar;->getCount()I

    move-result v2

    .line 77
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MenuView;->a:Lcom/sec/android/app/samsungapps/view/ar;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ar;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/MenuItem;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/MenuItem;->clear()V

    .line 77
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MenuView;->a:Lcom/sec/android/app/samsungapps/view/ar;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ar;->clear()V

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MenuView;->b:Lcom/sec/android/app/samsungapps/MenuItemList;

    if-eqz v0, :cond_1

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MenuView;->b:Lcom/sec/android/app/samsungapps/MenuItemList;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/MenuItemList;->setObserver(Lcom/sec/android/app/samsungapps/MenuItemList$MenuItemListObserver;)V

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MenuView;->b:Lcom/sec/android/app/samsungapps/MenuItemList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/MenuItemList;->release()V

    .line 86
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/view/MenuView;->b:Lcom/sec/android/app/samsungapps/MenuItemList;

    .line 88
    :cond_1
    return-void
.end method

.method public selChanged(Lcom/sec/android/app/samsungapps/MenuItem;)V
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MenuView;->a:Lcom/sec/android/app/samsungapps/view/ar;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ar;->notifyDataSetChanged()V

    .line 172
    return-void
.end method

.method public setMenuItemList(Lcom/sec/android/app/samsungapps/MenuItemList;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MenuView;->a:Lcom/sec/android/app/samsungapps/view/ar;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ar;->clear()V

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MenuView;->b:Lcom/sec/android/app/samsungapps/MenuItemList;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MenuView;->b:Lcom/sec/android/app/samsungapps/MenuItemList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/MenuItemList;->release()V

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MenuView;->b:Lcom/sec/android/app/samsungapps/MenuItemList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/MenuItemList;->setObserver(Lcom/sec/android/app/samsungapps/MenuItemList$MenuItemListObserver;)V

    .line 61
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/view/MenuView;->b:Lcom/sec/android/app/samsungapps/MenuItemList;

    .line 63
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/MenuView;->b:Lcom/sec/android/app/samsungapps/MenuItemList;

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MenuView;->b:Lcom/sec/android/app/samsungapps/MenuItemList;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/MenuItemList;->setObserver(Lcom/sec/android/app/samsungapps/MenuItemList$MenuItemListObserver;)V

    .line 65
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/MenuItemList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/MenuItem;

    .line 67
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/MenuView;->a:Lcom/sec/android/app/samsungapps/view/ar;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/view/ar;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 70
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MenuView;->a:Lcom/sec/android/app/samsungapps/view/ar;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ar;->notifyDataSetChanged()V

    .line 72
    return-void
.end method
