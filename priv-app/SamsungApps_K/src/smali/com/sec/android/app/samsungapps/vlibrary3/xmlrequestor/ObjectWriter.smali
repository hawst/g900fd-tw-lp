.class public Lcom/sec/android/app/samsungapps/vlibrary3/xmlrequestor/ObjectWriter;
.super Ljava/lang/Object;
.source "ProGuard"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static setFieldValueFromMap(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;Ljava/lang/Object;Z)Z
    .locals 13

    .prologue
    const/4 v12, 0x1

    const-wide v10, -0x3f3c788000000000L    # -9999.0

    const/4 v0, 0x0

    const/16 v9, -0x270f

    .line 18
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 19
    invoke-virtual {v1}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v2

    .line 21
    array-length v3, v2

    :goto_0
    if-ge v0, v3, :cond_a

    aget-object v4, v2, v0

    .line 23
    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 25
    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    .line 29
    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v6

    .line 31
    if-ne v6, v1, :cond_1

    .line 33
    invoke-virtual {v4}, Ljava/lang/reflect/Field;->isAccessible()Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v4, v12}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 40
    :cond_0
    :try_start_0
    const-string v6, "int"

    invoke-virtual {v5, v6}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_3

    .line 42
    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v5

    const/16 v6, -0x270f

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 43
    if-ne v5, v9, :cond_2

    .line 45
    if-eqz p2, :cond_1

    .line 47
    const/4 v5, 0x0

    invoke-virtual {v4, p1, v5}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V

    .line 21
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 52
    :cond_2
    invoke-virtual {v4, p1, v5}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V

    goto :goto_1

    .line 113
    :catch_0
    move-exception v4

    goto :goto_1

    .line 56
    :cond_3
    const-string v6, "long"

    invoke-virtual {v5, v6}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_5

    .line 58
    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v5

    const/16 v6, -0x270f

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->getLong(Ljava/lang/String;I)J

    move-result-wide v5

    .line 59
    const-wide/16 v7, -0x270f

    cmp-long v7, v5, v7

    if-nez v7, :cond_4

    .line 61
    if-eqz p2, :cond_1

    .line 63
    const-wide/16 v5, 0x0

    invoke-virtual {v4, p1, v5, v6}, Ljava/lang/reflect/Field;->setLong(Ljava/lang/Object;J)V

    goto :goto_1

    :catch_1
    move-exception v4

    goto :goto_1

    .line 68
    :cond_4
    invoke-virtual {v4, p1, v5, v6}, Ljava/lang/reflect/Field;->setLong(Ljava/lang/Object;J)V

    goto :goto_1

    .line 72
    :cond_5
    const-string v6, "double"

    invoke-virtual {v5, v6}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_7

    .line 74
    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v5

    const-wide v6, -0x3f3c788000000000L    # -9999.0

    invoke-virtual {p0, v5, v6, v7}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->getDouble(Ljava/lang/String;D)D

    move-result-wide v5

    .line 75
    cmpl-double v7, v5, v10

    if-nez v7, :cond_6

    .line 77
    if-eqz p2, :cond_1

    .line 79
    const-wide/16 v5, 0x0

    invoke-virtual {v4, p1, v5, v6}, Ljava/lang/reflect/Field;->setDouble(Ljava/lang/Object;D)V

    goto :goto_1

    .line 84
    :cond_6
    invoke-virtual {v4, p1, v5, v6}, Ljava/lang/reflect/Field;->setDouble(Ljava/lang/Object;D)V

    goto :goto_1

    .line 87
    :cond_7
    const-string v6, "String"

    invoke-virtual {v5, v6}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_9

    .line 89
    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 90
    if-nez v5, :cond_8

    .line 92
    if-eqz p2, :cond_1

    .line 94
    const/4 v5, 0x0

    invoke-virtual {v4, p1, v5}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 99
    :cond_8
    invoke-virtual {v4, p1, v5}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 102
    :cond_9
    const-string v6, "boolean"

    invoke-virtual {v5, v6}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_1

    .line 104
    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->getBool(Ljava/lang/String;Z)Z

    move-result v5

    .line 105
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, p1, v5}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_1

    .line 115
    :cond_a
    return v12
.end method
