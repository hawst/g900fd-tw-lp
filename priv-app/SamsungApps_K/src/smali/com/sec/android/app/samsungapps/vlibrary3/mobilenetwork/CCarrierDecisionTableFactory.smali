.class public Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CCarrierDecisionTableFactory;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTableFactory;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    return-void
.end method


# virtual methods
.method public createCarrierTable(Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTableFactory$CARRIER;)Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTable;
    .locals 3

    .prologue
    .line 7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/a;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTableFactory$CARRIER;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 18
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 10
    :pswitch_0
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTable;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTable;-><init>()V

    const-string v1, "ATT"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTable;->setCarrierCSC(Ljava/lang/String;)V

    const-string v1, "450"

    const-string v2, "100"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTable;->addCarrierMCCMNC(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "250"

    const-string v2, "100"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTable;->addCarrierMCCMNC(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "350"

    const-string v2, "100"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTable;->addCarrierMCCMNC(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 12
    :pswitch_1
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTable;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTable;-><init>()V

    goto :goto_0

    .line 14
    :pswitch_2
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTable;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTable;-><init>()V

    goto :goto_0

    .line 16
    :pswitch_3
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTable;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTable;-><init>()V

    goto :goto_0

    .line 7
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
