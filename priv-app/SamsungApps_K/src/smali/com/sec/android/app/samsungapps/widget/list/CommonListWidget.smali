.class public abstract Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;
.super Lcom/sec/android/app/samsungapps/widget/CommonWidget;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;


# instance fields
.field protected final LIST_MORE_SCROLLING:I

.field protected final LIST_SCROLLING:I

.field a:Landroid/widget/AbsListView$OnScrollListener;

.field private b:Landroid/content/Context;

.field private d:I

.field private e:Lcom/sec/android/app/samsungapps/view/CommonAdapter;

.field private f:I

.field private final g:Ljava/lang/String;

.field protected mListView:Landroid/widget/AbsListView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 37
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;)V

    .line 26
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->LIST_SCROLLING:I

    .line 27
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->LIST_MORE_SCROLLING:I

    .line 28
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->d:I

    .line 32
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->f:I

    .line 34
    const-string v0, "CommonListWidget"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->g:Ljava/lang/String;

    .line 122
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/list/a;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/list/a;-><init>(Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->a:Landroid/widget/AbsListView$OnScrollListener;

    .line 38
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->b:Landroid/content/Context;

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 42
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->LIST_SCROLLING:I

    .line 27
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->LIST_MORE_SCROLLING:I

    .line 28
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->d:I

    .line 32
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->f:I

    .line 34
    const-string v0, "CommonListWidget"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->g:Ljava/lang/String;

    .line 122
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/list/a;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/list/a;-><init>(Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->a:Landroid/widget/AbsListView$OnScrollListener;

    .line 43
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->b:Landroid/content/Context;

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 47
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->LIST_SCROLLING:I

    .line 27
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->LIST_MORE_SCROLLING:I

    .line 28
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->d:I

    .line 32
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->f:I

    .line 34
    const-string v0, "CommonListWidget"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->g:Ljava/lang/String;

    .line 122
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/list/a;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/list/a;-><init>(Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->a:Landroid/widget/AbsListView$OnScrollListener;

    .line 48
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->b:Landroid/content/Context;

    .line 49
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;)Lcom/sec/android/app/samsungapps/view/CommonAdapter;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->e:Lcom/sec/android/app/samsungapps/view/CommonAdapter;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;)I
    .locals 1

    .prologue
    .line 18
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->d:I

    return v0
.end method


# virtual methods
.method public clearSavedPositon()V
    .locals 1

    .prologue
    .line 169
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->f:I

    .line 170
    return-void
.end method

.method public getAdapter()Lcom/sec/android/app/samsungapps/view/CommonAdapter;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->e:Lcom/sec/android/app/samsungapps/view/CommonAdapter;

    return-object v0
.end method

.method public getScrollState()I
    .locals 1

    .prologue
    .line 187
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->d:I

    return v0
.end method

.method public abstract loadMoreComplete()V
.end method

.method public loadOldY()I
    .locals 1

    .prologue
    .line 165
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->f:I

    return v0
.end method

.method public loadWidget()V
    .locals 3

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->e:Lcom/sec/android/app/samsungapps/view/CommonAdapter;

    if-nez v0, :cond_0

    .line 99
    :goto_0
    return-void

    .line 96
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->d:I

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->mListView:Landroid/widget/AbsListView;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->loadOldY()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setSelection(I)V

    .line 98
    const-string v0, "CommonListWidget"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "loadWidget() loadOldY = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->loadOldY()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public abstract loadingMore()V
.end method

.method public onWidgetMoreLoading(Z)V
    .locals 4

    .prologue
    const v3, 0x7f0c007b

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 112
    const v0, 0x7f0c007c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 113
    if-eqz p1, :cond_0

    .line 114
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 115
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->d:I

    .line 120
    :goto_0
    return-void

    .line 117
    :cond_0
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 118
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->d:I

    goto :goto_0
.end method

.method public refreshWidget()V
    .locals 0

    .prologue
    .line 109
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->e:Lcom/sec/android/app/samsungapps/view/CommonAdapter;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->e:Lcom/sec/android/app/samsungapps/view/CommonAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->release()V

    .line 81
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->e:Lcom/sec/android/app/samsungapps/view/CommonAdapter;

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->mListView:Landroid/widget/AbsListView;

    if-eqz v0, :cond_1

    .line 84
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->mListView:Landroid/widget/AbsListView;

    .line 86
    :cond_1
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->a:Landroid/widget/AbsListView$OnScrollListener;

    .line 87
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->release()V

    .line 88
    return-void
.end method

.method public saveOldY(I)V
    .locals 0

    .prologue
    .line 161
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->f:I

    .line 162
    return-void
.end method

.method public setAdapter(Lcom/sec/android/app/samsungapps/view/CommonAdapter;)V
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->mListView:Landroid/widget/AbsListView;

    instance-of v0, v0, Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->mListView:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 60
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->e:Lcom/sec/android/app/samsungapps/view/CommonAdapter;

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->mListView:Landroid/widget/AbsListView;

    instance-of v0, v0, Landroid/widget/ListView;

    if-eqz v0, :cond_1

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->mListView:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 68
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->mListView:Landroid/widget/AbsListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setFocusableInTouchMode(Z)V

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->mListView:Landroid/widget/AbsListView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->a:Landroid/widget/AbsListView$OnScrollListener;

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 71
    return-void

    .line 65
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->mListView:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/GridView;

    invoke-virtual {v0, p1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method

.method public setSelection(I)V
    .locals 2

    .prologue
    .line 173
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->mListView:Landroid/widget/AbsListView;

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->mListView:Landroid/widget/AbsListView;

    invoke-virtual {v0, p1}, Landroid/widget/AbsListView;->setSelection(I)V

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;->mListView:Landroid/widget/AbsListView;

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/list/b;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/widget/list/b;-><init>(Lcom/sec/android/app/samsungapps/widget/list/CommonListWidget;I)V

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->post(Ljava/lang/Runnable;)Z

    .line 184
    :cond_0
    return-void
.end method

.method public updateWidget()V
    .locals 0

    .prologue
    .line 104
    return-void
.end method
