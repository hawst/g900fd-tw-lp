.class public abstract Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AbstractWishItemCommandBuilder;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem$IRemoveWishItemData;


# instance fields
.field private _IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;

.field private _IContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AbstractWishItemCommandBuilder;->_IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;

    .line 22
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AbstractWishItemCommandBuilder;->_IContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;

    .line 23
    return-void
.end method


# virtual methods
.method public addToWishCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 2

    .prologue
    .line 47
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AddWishListCommand;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AbstractWishItemCommandBuilder;->createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AddWishListCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;)V

    return-object v0
.end method

.method protected abstract createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
.end method

.method public getButtonState()Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState;
    .locals 1

    .prologue
    .line 66
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;)V

    return-object v0
.end method

.method protected abstract getGoToWishViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
.end method

.method public getProductID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AbstractWishItemCommandBuilder;->_IContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;->getProductID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getWishListID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AbstractWishItemCommandBuilder;->_IContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->wishListId:Ljava/lang/String;

    return-object v0
.end method

.method public goToWishCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 52
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/a;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AbstractWishItemCommandBuilder;)V

    return-object v0
.end method

.method public hasOrderID()Z
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AbstractWishItemCommandBuilder;->_IContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;->hasOrderID()Z

    move-result v0

    return v0
.end method

.method public isAddedItem()Z
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AbstractWishItemCommandBuilder;->_IContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;->isAddedWishItem()Z

    move-result v0

    return v0
.end method

.method public isInstalledItem()Z
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AbstractWishItemCommandBuilder;->_IContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;->isInstalledItem()Z

    move-result v0

    return v0
.end method

.method public isLogedIn()Z
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AbstractWishItemCommandBuilder;->_IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;->isLogedIn()Z

    move-result v0

    return v0
.end method

.method public isPurchaseDetailItem()Z
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AbstractWishItemCommandBuilder;->_IContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;->isPurchasedDetailType()Z

    move-result v0

    return v0
.end method

.method public removeWishCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 2

    .prologue
    .line 97
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AbstractWishItemCommandBuilder;->createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem$IRemoveWishItemData;Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;)V

    return-object v0
.end method

.method public setAddedWishItem()V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AbstractWishItemCommandBuilder;->_IContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;->setAddedWishItem()V

    .line 79
    return-void
.end method

.method public setDeletedWishItem()V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AbstractWishItemCommandBuilder;->_IContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;->setDeletedWishItem()V

    .line 85
    return-void
.end method
