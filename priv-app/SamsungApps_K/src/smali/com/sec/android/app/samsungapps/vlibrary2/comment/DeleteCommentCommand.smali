.class public Lcom/sec/android/app/samsungapps/vlibrary2/comment/DeleteCommentCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _CommentID:Ljava/lang/String;

.field private _IDeleteCommentCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/DeleteCommentCommand$IDeleteCommentCommandData;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/comment/DeleteCommentCommand$IDeleteCommentCommandData;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 21
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/DeleteCommentCommand;->_CommentID:Ljava/lang/String;

    .line 22
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/DeleteCommentCommand;->_IDeleteCommentCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/DeleteCommentCommand$IDeleteCommentCommandData;

    .line 23
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/comment/DeleteCommentCommand;Z)V
    .locals 0

    .prologue
    .line 15
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/DeleteCommentCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/comment/DeleteCommentCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/DeleteCommentCommand$IDeleteCommentCommandData;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/DeleteCommentCommand;->_IDeleteCommentCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/DeleteCommentCommand$IDeleteCommentCommandData;

    return-object v0
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 4

    .prologue
    .line 27
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/DeleteCommentCommand;->_IDeleteCommentCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/DeleteCommentCommand$IDeleteCommentCommandData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/DeleteCommentCommand$IDeleteCommentCommandData;->getProductID()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/DeleteCommentCommand;->_CommentID:Ljava/lang/String;

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/comment/g;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/g;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/comment/DeleteCommentCommand;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->commentDelete(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 39
    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 40
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 41
    return-void
.end method
