.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryIDGetter;


# instance fields
.field private _CategorySelChangedObserver:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer$CategorySelChangedObserver;

.field mObserver:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

.field mRoot:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

.field mSelCategory:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

.field mSelCategoryStack:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->mRoot:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    .line 10
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->mSelCategory:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    .line 11
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->mSelCategoryStack:Ljava/util/ArrayList;

    .line 141
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    .line 161
    return-void
.end method


# virtual methods
.method public addCategory(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;)V
    .locals 2

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->mRoot:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    if-nez v0, :cond_1

    .line 31
    :cond_0
    :goto_0
    return-void

    .line 27
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->mRoot:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    iget-object v1, p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->parentID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->findCategory(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    move-result-object v0

    .line 28
    if-eqz v0, :cond_0

    .line 29
    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->setChild(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;)V

    goto :goto_0
.end method

.method public addObserver(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer$CategoryContainerObserver;)V
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;->addObserver(Ljava/lang/Object;)V

    .line 146
    return-void
.end method

.method public addSelectedItem(Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;)V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->mSelCategoryStack:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 98
    return-void
.end method

.method public clear()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->mRoot:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->mRoot:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->clear()V

    .line 42
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->mRoot:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    .line 44
    :cond_0
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->mSelCategory:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->mSelCategoryStack:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 46
    return-void
.end method

.method public clearSel()V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->mRoot:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->mRoot:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->clearSel()V

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->mSelCategoryStack:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 93
    return-void
.end method

.method public findCategoryByID(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->mRoot:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    if-nez v0, :cond_0

    .line 121
    const/4 v0, 0x0

    .line 123
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->mRoot:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->findCategory(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    move-result-object v0

    goto :goto_0
.end method

.method public findCategoryNeedingChild()Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->mRoot:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    if-nez v0, :cond_0

    .line 113
    const/4 v0, 0x0

    .line 115
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->mRoot:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->findCategoryNeedingChild()Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    move-result-object v0

    goto :goto_0
.end method

.method public getRoot()Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->mRoot:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    return-object v0
.end method

.method public getSelItemStack(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->mSelCategoryStack:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    return-object v0
.end method

.method public getSelectedCategory()Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->mSelCategory:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    return-object v0
.end method

.method public isLoaded()Z
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->mRoot:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->mRoot:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected notifyCategorySelChanged(Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;)V
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;->getCloneList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer$CategoryContainerObserver;

    .line 134
    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer$CategoryContainerObserver;->categorySelChanged(Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;)V

    goto :goto_0

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->_CategorySelChangedObserver:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer$CategorySelChangedObserver;

    if-eqz v0, :cond_1

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->_CategorySelChangedObserver:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer$CategorySelChangedObserver;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer$CategorySelChangedObserver;->onCategorySelChanged(Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;)V

    .line 139
    :cond_1
    return-void
.end method

.method public removeObserver(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer$CategoryContainerObserver;)V
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;->removeObserver(Ljava/lang/Object;)Z

    .line 150
    return-void
.end method

.method public selItemStackSize()I
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->mSelCategoryStack:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public setCategorySelChangedObserver(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer$CategorySelChangedObserver;)V
    .locals 0

    .prologue
    .line 158
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->_CategorySelChangedObserver:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer$CategorySelChangedObserver;

    .line 159
    return-void
.end method

.method public setRoot(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;)V
    .locals 0

    .prologue
    .line 14
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->mRoot:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    .line 15
    return-void
.end method

.method public setSel(Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 49
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->mRoot:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    if-nez v1, :cond_1

    .line 66
    :cond_0
    :goto_0
    return v0

    .line 53
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->clearSel()V

    .line 54
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->mRoot:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    invoke-virtual {v1, p1, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->selCategory(Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 56
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->mSelCategory:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    .line 57
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/k;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/k;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread;->add(Lcom/sec/android/app/samsungapps/vlibrary/doc/RequestResultNotificationThread$handlerInterface;)V

    .line 64
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setSel(Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;Z)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 70
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->mRoot:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    if-nez v2, :cond_1

    .line 83
    :cond_0
    :goto_0
    return v0

    .line 73
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->clearSel()V

    .line 74
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->mRoot:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    invoke-virtual {v2, p1, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->selCategory(Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 75
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->mSelCategory:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    .line 77
    if-ne p2, v1, :cond_2

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->mSelCategory:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->notifyCategorySelChanged(Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;)V

    :cond_2
    move v0, v1

    .line 81
    goto :goto_0
.end method
