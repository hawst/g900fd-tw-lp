.class public Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkPaymentActivity;
.super Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkActivity;
.source "ProGuard"


# instance fields
.field a:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkActivity;-><init>()V

    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;)Lcom/sec/android/app/billing/helper/UnifiedPaymentData;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 96
    new-instance v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;

    invoke-direct {v0}, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;-><init>()V

    .line 98
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkPaymentActivity;->uxVersion:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->uxVersion:Ljava/lang/String;

    .line 100
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->SAC_CLIENT_ID:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->appServiceID:Ljava/lang/String;

    .line 101
    iget-object v1, p2, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->storeReqeustID:Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->storeRequestID:Ljava/lang/String;

    .line 102
    new-instance v1, Lcom/sec/android/app/billing/helper/ProductInfo;

    invoke-direct {v1}, Lcom/sec/android/app/billing/helper/ProductInfo;-><init>()V

    iput-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->productInfo:Lcom/sec/android/app/billing/helper/ProductInfo;

    .line 104
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->productInfo:Lcom/sec/android/app/billing/helper/ProductInfo;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getDiscountPrice()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/ProductInfo;->totalAmount:Ljava/lang/String;

    .line 105
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->productInfo:Lcom/sec/android/app/billing/helper/ProductInfo;

    const-string v2, "0"

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/ProductInfo;->tax:Ljava/lang/String;

    .line 106
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->productInfo:Lcom/sec/android/app/billing/helper/ProductInfo;

    const-string v2, "N"

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/ProductInfo;->taxIncluded:Ljava/lang/String;

    .line 108
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->productInfo:Lcom/sec/android/app/billing/helper/ProductInfo;

    iget-object v2, p2, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->currency:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/ProductInfo;->currency:Ljava/lang/String;

    .line 109
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->productInfo:Lcom/sec/android/app/billing/helper/ProductInfo;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/sec/android/app/billing/helper/DetailProductInfos;

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/ProductInfo;->detailProductInfos:[Lcom/sec/android/app/billing/helper/DetailProductInfos;

    .line 111
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->productInfo:Lcom/sec/android/app/billing/helper/ProductInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/helper/ProductInfo;->detailProductInfos:[Lcom/sec/android/app/billing/helper/DetailProductInfos;

    new-instance v2, Lcom/sec/android/app/billing/helper/DetailProductInfos;

    invoke-direct {v2}, Lcom/sec/android/app/billing/helper/DetailProductInfos;-><init>()V

    aput-object v2, v1, v4

    .line 113
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->productInfo:Lcom/sec/android/app/billing/helper/ProductInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/helper/ProductInfo;->detailProductInfos:[Lcom/sec/android/app/billing/helper/DetailProductInfos;

    aget-object v1, v1, v4

    iget-object v2, p2, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->productID:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/DetailProductInfos;->productID:Ljava/lang/String;

    .line 114
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->productInfo:Lcom/sec/android/app/billing/helper/ProductInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/helper/ProductInfo;->detailProductInfos:[Lcom/sec/android/app/billing/helper/DetailProductInfos;

    aget-object v1, v1, v4

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/DetailProductInfos;->productName:Ljava/lang/String;

    .line 115
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->productInfo:Lcom/sec/android/app/billing/helper/ProductInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/helper/ProductInfo;->detailProductInfos:[Lcom/sec/android/app/billing/helper/DetailProductInfos;

    aget-object v1, v1, v4

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getDiscountPrice()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/DetailProductInfos;->amount:Ljava/lang/String;

    .line 116
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->productInfo:Lcom/sec/android/app/billing/helper/ProductInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/helper/ProductInfo;->detailProductInfos:[Lcom/sec/android/app/billing/helper/DetailProductInfos;

    aget-object v1, v1, v4

    const-string v2, "0"

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/DetailProductInfos;->tax:Ljava/lang/String;

    .line 117
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->productInfo:Lcom/sec/android/app/billing/helper/ProductInfo;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getDiscountPSMSPrice()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/ProductInfo;->phonebillAmount:Ljava/lang/String;

    .line 120
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->productImgUrl:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 122
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->productInfo:Lcom/sec/android/app/billing/helper/ProductInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/helper/ProductInfo;->detailProductInfos:[Lcom/sec/android/app/billing/helper/DetailProductInfos;

    aget-object v1, v1, v4

    new-instance v2, Lcom/sec/android/app/billing/helper/ThumbnailImageURL;

    invoke-direct {v2}, Lcom/sec/android/app/billing/helper/ThumbnailImageURL;-><init>()V

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/DetailProductInfos;->thumbnailImageURL:Lcom/sec/android/app/billing/helper/ThumbnailImageURL;

    .line 123
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->productInfo:Lcom/sec/android/app/billing/helper/ProductInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/helper/ProductInfo;->detailProductInfos:[Lcom/sec/android/app/billing/helper/DetailProductInfos;

    aget-object v1, v1, v4

    iget-object v1, v1, Lcom/sec/android/app/billing/helper/DetailProductInfos;->thumbnailImageURL:Lcom/sec/android/app/billing/helper/ThumbnailImageURL;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->productImgUrl:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/ThumbnailImageURL;->mdpi:Ljava/lang/String;

    .line 124
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->productInfo:Lcom/sec/android/app/billing/helper/ProductInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/helper/ProductInfo;->detailProductInfos:[Lcom/sec/android/app/billing/helper/DetailProductInfos;

    aget-object v1, v1, v4

    iget-object v1, v1, Lcom/sec/android/app/billing/helper/DetailProductInfos;->thumbnailImageURL:Lcom/sec/android/app/billing/helper/ThumbnailImageURL;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->productImgUrl:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/ThumbnailImageURL;->xhdpi:Ljava/lang/String;

    .line 125
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->productInfo:Lcom/sec/android/app/billing/helper/ProductInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/helper/ProductInfo;->detailProductInfos:[Lcom/sec/android/app/billing/helper/DetailProductInfos;

    aget-object v1, v1, v4

    iget-object v1, v1, Lcom/sec/android/app/billing/helper/DetailProductInfos;->thumbnailImageURL:Lcom/sec/android/app/billing/helper/ThumbnailImageURL;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->productImgUrl:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/ThumbnailImageURL;->xxhdpi:Ljava/lang/String;

    .line 134
    :cond_0
    new-instance v1, Lcom/sec/android/app/billing/helper/UserInfo;

    invoke-direct {v1}, Lcom/sec/android/app/billing/helper/UserInfo;-><init>()V

    iput-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->userInfo:Lcom/sec/android/app/billing/helper/UserInfo;

    .line 135
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->userInfo:Lcom/sec/android/app/billing/helper/UserInfo;

    iget-object v2, p2, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->userId:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/UserInfo;->userID:Ljava/lang/String;

    .line 136
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->userInfo:Lcom/sec/android/app/billing/helper/UserInfo;

    iget-object v2, p2, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->userName:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/UserInfo;->userName:Ljava/lang/String;

    .line 137
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->userInfo:Lcom/sec/android/app/billing/helper/UserInfo;

    iget-object v2, p2, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->userEmail:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/UserInfo;->userEmail:Ljava/lang/String;

    .line 138
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->userInfo:Lcom/sec/android/app/billing/helper/UserInfo;

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->SAC_CLIENT_ID:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/UserInfo;->authAppID:Ljava/lang/String;

    .line 139
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->userInfo:Lcom/sec/android/app/billing/helper/UserInfo;

    sget-object v2, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;->_Token:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/UserInfo;->accessToken:Ljava/lang/String;

    .line 142
    new-instance v1, Lcom/sec/android/app/billing/helper/BillingServerInfo;

    invoke-direct {v1}, Lcom/sec/android/app/billing/helper/BillingServerInfo;-><init>()V

    iput-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->billingServerInfo:Lcom/sec/android/app/billing/helper/BillingServerInfo;

    .line 143
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getSAConfig()Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    move-result-object v1

    .line 144
    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getBillingServerType()Ljava/lang/String;

    move-result-object v2

    .line 146
    iget-object v3, p2, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->serviceType:Ljava/lang/String;

    if-eqz v3, :cond_1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->isUsingStageURL()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 147
    :cond_1
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->billingServerInfo:Lcom/sec/android/app/billing/helper/BillingServerInfo;

    const-string v3, "STG2"

    iput-object v3, v1, Lcom/sec/android/app/billing/helper/BillingServerInfo;->serviceType:Ljava/lang/String;

    .line 148
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->billingServerInfo:Lcom/sec/android/app/billing/helper/BillingServerInfo;

    const-string v3, "https://stg-api.samsungosp.com"

    iput-object v3, v1, Lcom/sec/android/app/billing/helper/BillingServerInfo;->upServerURL:Ljava/lang/String;

    .line 150
    :cond_2
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 152
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->billingServerInfo:Lcom/sec/android/app/billing/helper/BillingServerInfo;

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/BillingServerInfo;->serviceType:Ljava/lang/String;

    .line 153
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->billingServerInfo:Lcom/sec/android/app/billing/helper/BillingServerInfo;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkPaymentActivity;->getUPServerURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/BillingServerInfo;->upServerURL:Ljava/lang/String;

    .line 170
    :cond_3
    :goto_0
    new-instance v1, Lcom/sec/android/app/billing/helper/ServiceStoreInfo;

    invoke-direct {v1}, Lcom/sec/android/app/billing/helper/ServiceStoreInfo;-><init>()V

    iput-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/helper/ServiceStoreInfo;

    .line 171
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/helper/ServiceStoreInfo;

    iget-object v2, p2, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->countryCode:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/ServiceStoreInfo;->country:Ljava/lang/String;

    .line 176
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/helper/ServiceStoreInfo;

    new-instance v2, Lcom/sec/android/app/billing/helper/BillingInterfaceURL;

    invoke-direct {v2}, Lcom/sec/android/app/billing/helper/BillingInterfaceURL;-><init>()V

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/helper/BillingInterfaceURL;

    .line 178
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/helper/ServiceStoreInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/helper/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/helper/BillingInterfaceURL;

    iget-object v2, p2, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->addGiftCardnCouponURL:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/BillingInterfaceURL;->addGiftCardnCouponURL:Ljava/lang/String;

    .line 179
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/helper/ServiceStoreInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/helper/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/helper/BillingInterfaceURL;

    iget-object v2, p2, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->getGiftCardnCouponURL:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/BillingInterfaceURL;->getGiftCardnCouponURL:Ljava/lang/String;

    .line 180
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/helper/ServiceStoreInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/helper/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/helper/BillingInterfaceURL;

    iget-object v2, p2, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->requestOrderURL:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/BillingInterfaceURL;->requestOrderURL:Ljava/lang/String;

    .line 181
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/helper/ServiceStoreInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/helper/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/helper/BillingInterfaceURL;

    iget-object v2, p2, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->notiPaymentResultURL:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/BillingInterfaceURL;->notiPaymentResultURL:Ljava/lang/String;

    .line 182
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->serviceStoreInfo:Lcom/sec/android/app/billing/helper/ServiceStoreInfo;

    iget-object v1, v1, Lcom/sec/android/app/billing/helper/ServiceStoreInfo;->billingInterfaceURL:Lcom/sec/android/app/billing/helper/BillingInterfaceURL;

    iget-object v2, p2, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->retrieveTaxURL:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/BillingInterfaceURL;->getTaxInfoURL:Ljava/lang/String;

    .line 187
    new-instance v1, Lcom/sec/android/app/billing/helper/DeviceInfo;

    invoke-direct {v1}, Lcom/sec/android/app/billing/helper/DeviceInfo;-><init>()V

    iput-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->deviceInfo:Lcom/sec/android/app/billing/helper/DeviceInfo;

    .line 188
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->deviceInfo:Lcom/sec/android/app/billing/helper/DeviceInfo;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getIMEI()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/DeviceInfo;->deviceUID:Ljava/lang/String;

    .line 189
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->deviceInfo:Lcom/sec/android/app/billing/helper/DeviceInfo;

    const-string v2, "M|640|480"

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/DeviceInfo;->displayType:Ljava/lang/String;

    .line 191
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->deviceInfo:Lcom/sec/android/app/billing/helper/DeviceInfo;

    const-string v2, ""

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/DeviceInfo;->shopID:Ljava/lang/String;

    .line 192
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->deviceInfo:Lcom/sec/android/app/billing/helper/DeviceInfo;

    const-string v2, ""

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/DeviceInfo;->channelID:Ljava/lang/String;

    .line 194
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    .line 196
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkPaymentActivity;->getFakeModel()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkPaymentActivity;->getFakeModel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_7

    .line 198
    iget-object v2, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->deviceInfo:Lcom/sec/android/app/billing/helper/DeviceInfo;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkPaymentActivity;->getFakeModel()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/billing/helper/DeviceInfo;->deviceID:Ljava/lang/String;

    .line 215
    :cond_4
    :goto_1
    iget-object v2, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->deviceInfo:Lcom/sec/android/app/billing/helper/DeviceInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMCC()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/billing/helper/DeviceInfo;->mcc:Ljava/lang/String;

    .line 216
    iget-object v2, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->deviceInfo:Lcom/sec/android/app/billing/helper/DeviceInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMNC()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/billing/helper/DeviceInfo;->mnc:Ljava/lang/String;

    .line 217
    iget-object v2, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->deviceInfo:Lcom/sec/android/app/billing/helper/DeviceInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getCSC()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/sec/android/app/billing/helper/DeviceInfo;->csc:Ljava/lang/String;

    .line 230
    new-instance v1, Lcom/sec/android/app/billing/helper/PaymentInfo;

    invoke-direct {v1}, Lcom/sec/android/app/billing/helper/PaymentInfo;-><init>()V

    iput-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->paymentInfo:Lcom/sec/android/app/billing/helper/PaymentInfo;

    .line 231
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->paymentInfo:Lcom/sec/android/app/billing/helper/PaymentInfo;

    iget-object v2, p2, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->paymentType:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/PaymentInfo;->paymentType:Ljava/lang/String;

    .line 232
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->paymentInfo:Lcom/sec/android/app/billing/helper/PaymentInfo;

    iget-object v2, p2, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->offerType:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/PaymentInfo;->offerType:Ljava/lang/String;

    .line 238
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->paymentInfo:Lcom/sec/android/app/billing/helper/PaymentInfo;

    const-string v2, "N"

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/PaymentInfo;->confirmPasswordYN:Ljava/lang/String;

    .line 239
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->paymentInfo:Lcom/sec/android/app/billing/helper/PaymentInfo;

    iget-object v2, p2, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->exceptionPaymentMethods:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/PaymentInfo;->exceptionPaymentMethods:Ljava/lang/String;

    .line 241
    new-instance v1, Lcom/sec/android/app/billing/helper/SignatureInfo;

    invoke-direct {v1}, Lcom/sec/android/app/billing/helper/SignatureInfo;-><init>()V

    iput-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->signatureInfo:Lcom/sec/android/app/billing/helper/SignatureInfo;

    .line 243
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->signatureInfo:Lcom/sec/android/app/billing/helper/SignatureInfo;

    iget-object v2, p2, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->timeStamp:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/SignatureInfo;->timeStamp:Ljava/lang/String;

    .line 244
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->signatureInfo:Lcom/sec/android/app/billing/helper/SignatureInfo;

    iget-object v2, p2, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->baseString:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/SignatureInfo;->baseString:Ljava/lang/String;

    .line 245
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->signatureInfo:Lcom/sec/android/app/billing/helper/SignatureInfo;

    iget-object v2, p2, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->signature:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/SignatureInfo;->signature:Ljava/lang/String;

    .line 247
    const-string v1, "+00:00"

    iput-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->timeOffset:Ljava/lang/String;

    .line 280
    return-object v0

    .line 155
    :cond_5
    iget-object v1, p2, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->serviceType:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 157
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->billingServerInfo:Lcom/sec/android/app/billing/helper/BillingServerInfo;

    iget-object v2, p2, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->serviceType:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/BillingServerInfo;->serviceType:Ljava/lang/String;

    .line 159
    iget-object v1, p2, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->serviceType:Ljava/lang/String;

    const-string v2, "PRD"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 161
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->billingServerInfo:Lcom/sec/android/app/billing/helper/BillingServerInfo;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkPaymentActivity;->getPRDServerURL()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/BillingServerInfo;->upServerURL:Ljava/lang/String;

    goto/16 :goto_0

    .line 164
    :cond_6
    iget-object v1, p2, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->serviceType:Ljava/lang/String;

    const-string v2, "STG2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 165
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->billingServerInfo:Lcom/sec/android/app/billing/helper/BillingServerInfo;

    const-string v2, "https://stg-api.samsungosp.com"

    iput-object v2, v1, Lcom/sec/android/app/billing/helper/BillingServerInfo;->upServerURL:Ljava/lang/String;

    goto/16 :goto_0

    .line 202
    :cond_7
    iget-object v2, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->deviceInfo:Lcom/sec/android/app/billing/helper/DeviceInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getModelName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/billing/helper/DeviceInfo;->deviceID:Ljava/lang/String;

    .line 204
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 205
    iget-object v2, v0, Lcom/sec/android/app/billing/helper/UnifiedPaymentData;->deviceInfo:Lcom/sec/android/app/billing/helper/DeviceInfo;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getModelName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_KNOX"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/billing/helper/DeviceInfo;->deviceID:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_1

    .line 212
    :catch_0
    move-exception v2

    goto/16 :goto_1

    :catch_1
    move-exception v2

    goto/16 :goto_1
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 285
    packed-switch p2, :pswitch_data_0

    .line 313
    :goto_0
    :pswitch_0
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 314
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkPaymentActivity;->finish()V

    .line 315
    return-void

    .line 289
    :pswitch_1
    const-string v0, "PAYMENT_RECEITE"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 290
    const-string v1, "SIGNATURE"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 292
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkPaymentActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager$UnifiedBillingResult;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager$UnifiedBillingResult;

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->onUnifiedBillingResult(Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager$UnifiedBillingResult;Ljava/lang/String;)V

    goto :goto_0

    .line 300
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkPaymentActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager$UnifiedBillingResult;->CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager$UnifiedBillingResult;

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->onUnifiedBillingResult(Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager$UnifiedBillingResult;Ljava/lang/String;)V

    goto :goto_0

    .line 304
    :pswitch_3
    const-string v0, "ERROR_ID"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 305
    const-string v1, "ERROR_MESSAGE"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 306
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkPaymentActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager$UnifiedBillingResult;->FAILURE:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager$UnifiedBillingResult;

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->onUnifiedBillingResult(Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager$UnifiedBillingResult;Ljava/lang/String;)V

    goto :goto_0

    .line 310
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkPaymentActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager$UnifiedBillingResult;->FAILURE:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager$UnifiedBillingResult;

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->onUnifiedBillingResult(Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager$UnifiedBillingResult;Ljava/lang/String;)V

    goto :goto_0

    .line 285
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 42
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkActivity;->onCreate(Landroid/os/Bundle;)V

    .line 45
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkPaymentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->readObject(Landroid/content/Intent;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkPaymentActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkPaymentActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;

    if-nez v0, :cond_0

    .line 55
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkPaymentActivity;->finish()V

    .line 71
    :goto_0
    return-void

    .line 49
    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkPaymentActivity;->finish()V

    goto :goto_0

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkPaymentActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->getInitResult()Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;

    move-result-object v0

    .line 60
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkPaymentActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v1

    .line 62
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkPaymentActivity;->mHandler:Landroid/os/Handler;

    new-instance v3, Lcom/sec/android/app/samsungapps/unfiedbilling/d;

    invoke-direct {v3, p0, v1, v0}, Lcom/sec/android/app/samsungapps/unfiedbilling/d;-><init>(Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkPaymentActivity;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;)V

    const-wide/16 v0, 0x64

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkPaymentActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->invokeCompleted()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkPaymentActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;

    if-eqz v0, :cond_0

    .line 321
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkPaymentActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->onDestroy()V

    .line 323
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkActivity;->onDestroy()V

    .line 324
    return-void
.end method

.method protected onExecute(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;)V
    .locals 8

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkPaymentActivity;->a(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;)Lcom/sec/android/app/billing/helper/UnifiedPaymentData;

    move-result-object v5

    .line 77
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 80
    :try_start_0
    invoke-static {v5, p0}, Lcom/sec/android/app/billing/helper/RequestParamValidator;->check(Lcom/sec/android/app/billing/helper/UnifiedPaymentData;Landroid/content/Context;)V

    .line 81
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkPaymentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/helper/UPHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/billing/helper/UPHelper;

    move-result-object v0

    const/16 v2, 0x4e30

    const-string v4, "PAYMENT"

    const-string v6, ""

    iget-object v7, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkPaymentActivity;->aidlHandler:Landroid/os/Handler;

    move-object v1, p0

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/app/billing/helper/UPHelper;->requestBilling(Landroid/app/Activity;IILjava/lang/String;Lcom/sec/android/app/billing/helper/UnifiedPaymentData;Ljava/lang/String;Landroid/os/Handler;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 92
    :goto_0
    return-void

    .line 82
    :catch_0
    move-exception v0

    .line 83
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UnifiedBillingApkPaymentActivity::RemoteException::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 84
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkPaymentActivity;->finish()V

    goto :goto_0

    .line 85
    :catch_1
    move-exception v0

    .line 86
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UnifiedBillingApkPaymentActivity::SendIntentException::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/IntentSender$SendIntentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 87
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkPaymentActivity;->finish()V

    goto :goto_0

    .line 88
    :catch_2
    move-exception v0

    .line 89
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UnifiedBillingApkPaymentActivity::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 90
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkPaymentActivity;->finish()V

    goto :goto_0
.end method
