.class public final enum Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum LAUNCHER:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

.field public static final enum SAMSUNG_BOOKS:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

.field public static final enum SAMSUNG_GAMES:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

.field public static final enum SAMSUNG_HUB:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

.field public static final enum SAMSUNG_LEARNING:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

.field public static final enum SAMSUNG_MUSIC:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

.field public static final enum SAMSUNG_MUSIC_OLD:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

.field public static final enum SAMSUNG_VIDEO:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

.field public static final enum SAMSUNG_VIDEO_LITE:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

.field public static final enum WIDGET:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

.field private static final synthetic c:[Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 5
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    const-string v1, "SAMSUNG_GAMES"

    const-string v2, "013"

    const-string v3, "com.sec.android.app.gamehub"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->SAMSUNG_GAMES:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    .line 6
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    const-string v1, "SAMSUNG_HUB"

    const-string v2, "014"

    const-string v3, "com.sec.everglades"

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->SAMSUNG_HUB:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    .line 7
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    const-string v1, "SAMSUNG_VIDEO"

    const-string v2, "018"

    const-string v3, "com.samsung.everglades.video"

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->SAMSUNG_VIDEO:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    .line 8
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    const-string v1, "SAMSUNG_VIDEO_LITE"

    const-string v2, "018"

    const-string v3, "com.samsung.videohub"

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->SAMSUNG_VIDEO_LITE:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    .line 9
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    const-string v1, "SAMSUNG_MUSIC"

    const-string v2, "019"

    const-string v3, "com.sec.android.app.music"

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->SAMSUNG_MUSIC:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    .line 10
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    const-string v1, "SAMSUNG_MUSIC_OLD"

    const/4 v2, 0x5

    const-string v3, "019"

    const-string v4, "com.samsung.music"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->SAMSUNG_MUSIC_OLD:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    .line 11
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    const-string v1, "SAMSUNG_BOOKS"

    const/4 v2, 0x6

    const-string v3, "020"

    const-string v4, "com.sec.readershub"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->SAMSUNG_BOOKS:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    .line 12
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    const-string v1, "SAMSUNG_LEARNING"

    const/4 v2, 0x7

    const-string v3, "017"

    const-string v4, "com.sec.msc.learninghub"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->SAMSUNG_LEARNING:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    .line 13
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    const-string v1, "LAUNCHER"

    const/16 v2, 0x8

    const-string v3, "999"

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->LAUNCHER:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    .line 14
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    const-string v1, "WIDGET"

    const/16 v2, 0x9

    const-string v3, "996"

    const-string v4, "com.sec.android.widget.samsungapps"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->WIDGET:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    .line 3
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->SAMSUNG_GAMES:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->SAMSUNG_HUB:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->SAMSUNG_VIDEO:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->SAMSUNG_VIDEO_LITE:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    aput-object v1, v0, v8

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->SAMSUNG_MUSIC:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->SAMSUNG_MUSIC_OLD:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->SAMSUNG_BOOKS:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->SAMSUNG_LEARNING:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->LAUNCHER:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->WIDGET:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->c:[Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 18
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->a:Ljava/lang/String;

    .line 19
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->b:Ljava/lang/String;

    .line 21
    return-void
.end method

.method public static fromName(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;
    .locals 4

    .prologue
    .line 36
    invoke-static {}, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->values()[Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    move-result-object v1

    .line 38
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 39
    aget-object v2, v1, v0

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 40
    aget-object v0, v1, v0

    .line 44
    :goto_1
    return-object v0

    .line 38
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 44
    :cond_1
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->LAUNCHER:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;
    .locals 1

    .prologue
    .line 3
    const-class v0, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->c:[Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    return-object v0
.end method


# virtual methods
.method public final code()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final packageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->b:Ljava/lang/String;

    return-object v0
.end method
