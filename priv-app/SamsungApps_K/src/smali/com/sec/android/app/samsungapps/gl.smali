.class final Lcom/sec/android/app/samsungapps/gl;
.super Landroid/webkit/WebViewClient;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;

.field private final b:Ljava/lang/String;

.field private c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 109
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/gl;->a:Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    .line 106
    const-string v0, "samsungappsniceresult://"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/gl;->b:Ljava/lang/String;

    .line 110
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/gl;->c:Landroid/content/Context;

    .line 111
    return-void
.end method


# virtual methods
.method public final onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gl;->a:Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;->a(Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;)V

    .line 115
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 116
    return-void
.end method

.method public final onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gl;->a:Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;->b(Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;)V

    .line 121
    const-string v0, "http://apps.samsung.com/mercury/nice/resultNice.as"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 123
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 126
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/gl;->a:Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;

    const-string v2, "resultCode"

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;->a(Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 127
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/gl;->a:Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;

    const-string v2, "age"

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v1, v0}, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;->a(Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;I)I
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_2

    .line 136
    :cond_0
    :goto_0
    return-void

    .line 128
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/UnsupportedOperationException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 130
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 132
    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 141
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 142
    return-void
.end method

.method public final onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V
    .locals 0

    .prologue
    .line 146
    invoke-virtual {p2}, Landroid/webkit/SslErrorHandler;->proceed()V

    .line 147
    return-void
.end method

.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 151
    const-string v1, "http://"

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eq v1, v0, :cond_0

    const-string v1, "https://"

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-ne v1, v0, :cond_1

    .line 157
    :cond_0
    const-string v1, "http://www.vno.co.kr"

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 158
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/util/BrowserUtil;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/gl;->c:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/BrowserUtil;-><init>(Landroid/content/Context;)V

    .line 159
    invoke-virtual {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/util/BrowserUtil;->openWebBrowser(Ljava/lang/String;)V

    .line 176
    :goto_0
    return v0

    .line 162
    :cond_1
    const-string v1, "samsungappsniceresult://"

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-ne v1, v0, :cond_3

    .line 164
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/gl;->a:Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;->c(Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 166
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/gl;->a:Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/gl;->a:Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;->d(Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;->done(I)V

    .line 172
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/gl;->a:Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;->finish()V

    goto :goto_0

    .line 170
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/gl;->a:Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/RealNameVerificationActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;->failed()V

    goto :goto_1

    .line 176
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
