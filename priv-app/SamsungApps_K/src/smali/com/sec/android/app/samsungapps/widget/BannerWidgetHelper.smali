.class public Lcom/sec/android/app/samsungapps/widget/BannerWidgetHelper;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;


# instance fields
.field a:Landroid/content/Context;

.field b:Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;

.field c:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/BannerWidgetHelper;->a:Landroid/content/Context;

    .line 24
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/BannerWidgetHelper;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;

    .line 25
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/BannerWidgetHelper;->c:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    .line 31
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/BannerWidgetHelper;->a:Landroid/content/Context;

    .line 32
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/BannerWidgetHelper;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;

    .line 33
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 40
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/BannerWidgetHelper;->a:Landroid/content/Context;

    .line 41
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/BannerWidgetHelper;->c:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/BannerWidgetHelper;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/BannerWidgetHelper;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;->clear()V

    .line 47
    :cond_0
    return-void
.end method

.method public getBanner(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;
    .locals 3

    .prologue
    .line 151
    const/4 v0, 0x0

    .line 153
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/BannerWidgetHelper;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/BannerWidgetHelper;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;->size()I

    move-result v1

    if-le v1, p1, :cond_0

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/BannerWidgetHelper;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;

    .line 162
    :goto_0
    return-object v0

    .line 159
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "BannerWidgetHelper::getBanner::BannerList is empty or less then index="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getBannerCount()I
    .locals 2

    .prologue
    .line 133
    const/4 v0, 0x0

    .line 135
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/BannerWidgetHelper;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;

    if-eqz v1, :cond_0

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/BannerWidgetHelper;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;->size()I

    move-result v0

    .line 140
    :cond_0
    return v0
.end method

.method public sendRequest(ILcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V
    .locals 3

    .prologue
    .line 57
    const/4 v0, 0x0

    .line 59
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/BannerWidgetHelper;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v1

    .line 60
    if-nez v1, :cond_0

    .line 62
    const-string v0, "BannerWidgetHelper::sendRequestProductSet::builder is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 87
    :goto_0
    return-void

    .line 66
    :cond_0
    const/4 v2, 0x1

    if-ne p1, v2, :cond_2

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/BannerWidgetHelper;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;

    invoke-virtual {v1, v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->bigBannerList(Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 86
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/BannerWidgetHelper;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    goto :goto_0

    .line 70
    :cond_2
    const/4 v2, 0x2

    if-ne p1, v2, :cond_1

    .line 75
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/BannerWidgetHelper;->c:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    if-nez v2, :cond_3

    .line 77
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/widget/BannerWidgetHelper;->c:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/BannerWidgetHelper;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;

    new-instance v2, Lcom/sec/android/app/samsungapps/widget/j;

    invoke-direct {v2, p0, p2}, Lcom/sec/android/app/samsungapps/widget/j;-><init>(Lcom/sec/android/app/samsungapps/widget/BannerWidgetHelper;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->promotionBanner(Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    goto :goto_1

    .line 82
    :cond_3
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/widget/BannerWidgetHelper;->c:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    goto :goto_1
.end method
