.class public Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AutoUpdateNotificationSetting;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SettingsProvider;


# instance fields
.field private _Key:Ljava/lang/String;

.field appSharedPreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AutoUpdateNotificationSetting;->appSharedPreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    .line 14
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AutoUpdateNotificationSetting;->appSharedPreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    .line 15
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AutoUpdateNotificationSetting;->_Key:Ljava/lang/String;

    .line 16
    return-void
.end method

.method private determineOn(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 60
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AutoUpdateNotificationSetting;->isChina()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 62
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 71
    :cond_0
    :goto_0
    return v0

    .line 67
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AutoUpdateNotificationSetting;->getOnValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 69
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private getConfigItem()Ljava/lang/String;
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AutoUpdateNotificationSetting;->appSharedPreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AutoUpdateNotificationSetting;->_Key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getSharedConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getOffValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    const-string v0, "1"

    return-object v0
.end method

.method private getOnValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    const-string v0, "0"

    return-object v0
.end method

.method private isChina()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 25
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->CHINA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->checkCountry(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 33
    :cond_0
    :goto_0
    return v0

    .line 29
    :cond_1
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->CHINA2:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->checkCountry(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 33
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setConfigItem(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AutoUpdateNotificationSetting;->appSharedPreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AutoUpdateNotificationSetting;->_Key:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setSharedConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public isOn()Z
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AutoUpdateNotificationSetting;->getConfigItem()Ljava/lang/String;

    move-result-object v0

    .line 50
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AutoUpdateNotificationSetting;->determineOn(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public setOff()Z
    .locals 1

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AutoUpdateNotificationSetting;->getOffValue()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AutoUpdateNotificationSetting;->setConfigItem(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public setOn()Z
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AutoUpdateNotificationSetting;->getOnValue()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AutoUpdateNotificationSetting;->setConfigItem(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
