.class public Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ProGuard"

# interfaces
.implements Landroid/widget/SectionIndexer;


# instance fields
.field private a:Landroid/widget/SectionIndexer;

.field protected final mContext:Landroid/content/Context;

.field protected mFilter:Landroid/widget/Filter;

.field protected mFilteredItems:Ljava/util/ArrayList;

.field protected final mInflater:Landroid/view/LayoutInflater;

.field protected final mLayoutId:I

.field protected mOrgItems:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;->mFilter:Landroid/widget/Filter;

    .line 41
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;->mContext:Landroid/content/Context;

    .line 43
    if-eqz p3, :cond_0

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;->mOrgItems:Ljava/util/ArrayList;

    .line 46
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;->mFilteredItems:Ljava/util/ArrayList;

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;->mContext:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 50
    iput p2, p0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;->mLayoutId:I

    .line 51
    return-void
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    return v0
.end method

.method public getFilter()Landroid/widget/Filter;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;->mFilter:Landroid/widget/Filter;

    if-nez v0, :cond_0

    .line 126
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter$ResultFilter;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter$ResultFilter;-><init>(Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;->mFilter:Landroid/widget/Filter;

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;->mFilter:Landroid/widget/Filter;

    return-object v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;->mFilteredItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SectionListItemView;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SectionListItemView;->isHeader()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    const/4 v0, 0x0

    .line 95
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getPositionForSection(I)I
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;->a:Landroid/widget/SectionIndexer;

    if-nez v0, :cond_0

    .line 101
    const/4 v0, 0x0

    .line 103
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;->a:Landroid/widget/SectionIndexer;

    invoke-interface {v0, p1}, Landroid/widget/SectionIndexer;->getPositionForSection(I)I

    move-result v0

    goto :goto_0
.end method

.method public getSectionForPosition(I)I
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;->a:Landroid/widget/SectionIndexer;

    if-nez v0, :cond_0

    .line 109
    const/4 v0, 0x0

    .line 112
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;->a:Landroid/widget/SectionIndexer;

    invoke-interface {v0, p1}, Landroid/widget/SectionIndexer;->getSectionForPosition(I)I

    move-result v0

    goto :goto_0
.end method

.method public getSectionPostion(Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v1, -0x1

    .line 133
    if-nez p1, :cond_0

    move v0, v1

    .line 146
    :goto_0
    return v0

    .line 137
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;->getCount()I

    move-result v3

    .line 139
    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_2

    .line 140
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SectionListItemView;

    .line 142
    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/widget/SectionListItemView;->hasItem(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    .line 143
    goto :goto_0

    .line 139
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 146
    goto :goto_0
.end method

.method public getSections()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;->a:Landroid/widget/SectionIndexer;

    if-nez v0, :cond_0

    .line 118
    const/4 v0, 0x0

    .line 120
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;->a:Landroid/widget/SectionIndexer;

    invoke-interface {v0}, Landroid/widget/SectionIndexer;->getSections()[Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 61
    if-nez p2, :cond_0

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;->mInflater:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;->mLayoutId:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 66
    :cond_0
    const v0, 0x7f0c004d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 71
    if-eqz v0, :cond_1

    .line 72
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;->mFilteredItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/widget/SectionListItemView;

    .line 73
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/SectionListItemView;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 76
    :cond_1
    return-object p2
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;->mFilteredItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SectionListItemView;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SectionListItemView;->isHeader()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setIndexer(Landroid/widget/SectionIndexer;)V
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;->a:Landroid/widget/SectionIndexer;

    .line 57
    return-void
.end method
