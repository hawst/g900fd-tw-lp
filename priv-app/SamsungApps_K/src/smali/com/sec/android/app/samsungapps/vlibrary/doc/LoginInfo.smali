.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;
.super Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;
.source "ProGuard"


# instance fields
.field public birthDay:Ljava/lang/String;

.field public birthMonth:Ljava/lang/String;

.field public birthYear:Ljava/lang/String;

.field public cardInfo:I

.field public currencyUnit:Ljava/lang/String;

.field public emailID:Ljava/lang/String;

.field public gender:Ljava/lang/String;

.field public giftCardIssueFlag:I

.field public giftCardIssuedPrice:D

.field public iranDebitUrl:Ljava/lang/String;

.field private mCouponList:Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponList;

.field public nameAuthYn:Z

.field public password:Ljava/lang/String;

.field public pushSvcYn:I

.field public realAge:Ljava/lang/String;

.field private receiveNewsLetter:Z

.field public smartClsf:Ljava/lang/String;

.field public userID:Ljava/lang/String;

.field public voucherIssueFlag:I

.field public voucherIssuedValue:D

.field public voucherType:Ljava/lang/String;

.field public writeHistory:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;-><init>()V

    .line 19
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponList;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->mCouponList:Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponList;

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->receiveNewsLetter:Z

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->iranDebitUrl:Ljava/lang/String;

    .line 32
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 35
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;-><init>()V

    .line 19
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponList;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->mCouponList:Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponList;

    .line 20
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->receiveNewsLetter:Z

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->iranDebitUrl:Ljava/lang/String;

    .line 36
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    invoke-static {p1, v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->mappingClass(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 37
    return-void
.end method


# virtual methods
.method public getCouponList()Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponList;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->mCouponList:Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponList;

    return-object v0
.end method

.method public getIranDebitUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->iranDebitUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getRealAge()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 52
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->realAge:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 59
    :goto_0
    return v0

    .line 56
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->realAge:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 59
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public hasCardInfo()Z
    .locals 1

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->isThereCardInfo()Z

    move-result v0

    return v0
.end method

.method public isThereCardInfo()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 70
    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->cardInfo:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public newsLetterReceive()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->receiveNewsLetter:Z

    return v0
.end method

.method public pushSVSAgreed()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 95
    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->pushSvcYn:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->pushSvcYn:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setCardInfo(Z)V
    .locals 1

    .prologue
    .line 85
    if-eqz p1, :cond_0

    .line 86
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->cardInfo:I

    .line 91
    :goto_0
    return-void

    .line 89
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->cardInfo:I

    goto :goto_0
.end method

.method public setJoinInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->emailID:Ljava/lang/String;

    .line 42
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->password:Ljava/lang/String;

    .line 44
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->birthYear:Ljava/lang/String;

    .line 45
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->birthMonth:Ljava/lang/String;

    .line 46
    iput-object p5, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->birthDay:Ljava/lang/String;

    .line 47
    iput-boolean p6, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->receiveNewsLetter:Z

    .line 48
    return-void
.end method
