.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field public static final transient BASE_EQUAL_VER:I = 0x0

.field public static final transient BASE_HIGH_VER:I = 0x1

.field public static final transient BASE_LOW_VER:I = 0x2


# instance fields
.field public GUID:Ljava/lang/String;

.field public UpgradeClsf:Z

.field public loadType:I

.field public version:Ljava/lang/String;

.field public versionCode:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->GUID:Ljava/lang/String;

    .line 22
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->loadType:I

    .line 43
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;

    const/4 v1, 0x1

    invoke-virtual {p1, v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->mappingClass(Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 44
    return-void
.end method

.method private getDecimalList(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 4

    .prologue
    .line 125
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 128
    if-eqz p1, :cond_1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    .line 129
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_1

    .line 133
    const/16 v0, 0x2e

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 135
    const/4 v0, -0x1

    if-ne v2, v0, :cond_0

    .line 137
    const-string v0, ""

    move-object v3, p1

    move-object p1, v0

    move-object v0, v3

    .line 143
    :goto_1
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 151
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 139
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 140
    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 147
    :catch_0
    move-exception v0

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 156
    :cond_1
    return-object v1
.end method

.method private getLong(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 94
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 99
    :goto_0
    return-wide v0

    :catch_0
    move-exception v0

    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method private getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->version:Ljava/lang/String;

    return-object v0
.end method

.method private getVersionCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->versionCode:Ljava/lang/String;

    return-object v0
.end method

.method private getWGTInstallVersion(Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 209
    :try_start_0
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;->getAPI()Lcom/samsung/android/aidl/ICheckAppInstallState;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->GUID:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/samsung/android/aidl/ICheckAppInstallState;->checkInstalledWGTVersion(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 215
    :goto_0
    return-object v0

    .line 211
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 215
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getWGTInstallVersionCode(Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 220
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->getWGTInstallVersion(Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 221
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 224
    :try_start_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 230
    :goto_0
    return-wide v0

    :catch_0
    move-exception v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method private isVercodeValid()Z
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->versionCode:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->versionCode:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isWGTInstalled(Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 202
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->getWGTInstallVersion(Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public compareVersion(Ljava/lang/String;Ljava/lang/String;)I
    .locals 7

    .prologue
    .line 160
    const/4 v1, 0x0

    .line 162
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->getDecimalList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 163
    invoke-direct {p0, p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->getDecimalList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 166
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-eq v0, v4, :cond_0

    .line 167
    const/4 v0, -0x1

    .line 187
    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 194
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 196
    return v0

    .line 171
    :cond_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 172
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 174
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 175
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 176
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 178
    if-eq v6, v0, :cond_1

    .line 179
    if-le v6, v0, :cond_2

    .line 183
    const/4 v0, 0x1

    .line 184
    goto :goto_0

    .line 186
    :cond_2
    const/4 v0, 0x2

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public isPostLoad()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 33
    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->loadType:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPreload()Z
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->loadType:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isStoreApp()Z
    .locals 2

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->loadType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isUpdatable(Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 53
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->isVercodeValid()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 55
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->GUID:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->getPackageVersionCode(Ljava/lang/String;)J

    move-result-wide v2

    .line 57
    const-wide/16 v4, -0x1

    cmp-long v4, v2, v4

    if-nez v4, :cond_1

    .line 83
    :cond_0
    :goto_0
    return v0

    .line 62
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->getVersionCode()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    cmp-long v4, v4, v2

    if-lez v4, :cond_0

    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "puchasedList:"

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->GUID:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " serverCode:"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->getVersionCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " installCode:"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    move v0, v1

    .line 65
    goto :goto_0

    .line 72
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->GUID:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->getPacakgeVersionName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 74
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->getVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->compareVersion(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 76
    if-ne v3, v1, :cond_0

    .line 78
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "puchasedList:"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->GUID:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " serverCode:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->getVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " installCode:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    move v0, v1

    .line 79
    goto :goto_0
.end method

.method public isUpdatable(Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 235
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->isUpdatable(Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 265
    :goto_0
    return v0

    .line 238
    :cond_0
    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;->isReady()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 240
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->GUID:Ljava/lang/String;

    invoke-direct {p0, p2, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->isWGTInstalled(Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->isPostLoad()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 242
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "puchasedList:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->GUID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "updatable because PostLoadApp not installed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 247
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->GUID:Ljava/lang/String;

    invoke-direct {p0, p2, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->getWGTInstallVersionCode(Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;Ljava/lang/String;)J

    move-result-wide v2

    .line 249
    const-wide/16 v4, -0x1

    cmp-long v4, v2, v4

    if-nez v4, :cond_2

    move v0, v1

    .line 250
    goto :goto_0

    .line 253
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->getVersionCode()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    cmp-long v4, v4, v2

    if-lez v4, :cond_3

    .line 254
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "puchasedList:"

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->GUID:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " serverCode:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->getVersionCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " installCode:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 261
    goto :goto_0

    :cond_4
    move v0, v1

    .line 265
    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 113
    const-string v2, ""

    .line 114
    const-string v3, "GUID:%s loadType:%d upgradeClsf:%d versionCode:%s version:%s"

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->GUID:Ljava/lang/String;

    aput-object v5, v4, v1

    iget v5, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->loadType:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v5, 0x2

    iget-boolean v6, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->UpgradeClsf:Z

    if-eqz v6, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->versionCode:Ljava/lang/String;

    aput-object v1, v4, v0

    const/4 v0, 0x4

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadedApp;->version:Ljava/lang/String;

    aput-object v1, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 115
    return-object v2

    :cond_0
    move v0, v1

    .line 114
    goto :goto_0
.end method
