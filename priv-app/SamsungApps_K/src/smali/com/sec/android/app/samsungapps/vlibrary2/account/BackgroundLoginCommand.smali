.class public Lcom/sec/android/app/samsungapps/vlibrary2/account/BackgroundLoginCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _IBackgroundLoginCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/account/BackgroundLoginCommand$IBackgroundLoginCommandData;

.field _ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/BackgroundLoginCommand$IBackgroundLoginCommandData;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/BackgroundLoginCommand;->_IBackgroundLoginCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/account/BackgroundLoginCommand$IBackgroundLoginCommandData;

    .line 20
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/account/BackgroundLoginCommand;Z)V
    .locals 0

    .prologue
    .line 14
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/BackgroundLoginCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/account/BackgroundLoginCommand;Z)V
    .locals 0

    .prologue
    .line 14
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/BackgroundLoginCommand;->onFinalResult(Z)V

    return-void
.end method

.method private requestLogin()V
    .locals 3

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/BackgroundLoginCommand;->_IBackgroundLoginCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/account/BackgroundLoginCommand$IBackgroundLoginCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/BackgroundLoginCommand$IBackgroundLoginCommandData;->createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/BackgroundLoginCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    .line 44
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/BackgroundLoginCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->startLoading()V

    .line 45
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    .line 46
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/BackgroundLoginCommand;->_IBackgroundLoginCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/account/BackgroundLoginCommand$IBackgroundLoginCommandData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/BackgroundLoginCommand$IBackgroundLoginCommandData;->getEmailID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->setEmail(Ljava/lang/String;)V

    .line 47
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/BackgroundLoginCommand;->_IBackgroundLoginCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/account/BackgroundLoginCommand$IBackgroundLoginCommandData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/BackgroundLoginCommand$IBackgroundLoginCommandData;->getPassword()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->setPass(Ljava/lang/String;)V

    .line 48
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/account/e;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/e;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/BackgroundLoginCommand;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->login(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;Z)V

    .line 58
    return-void
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 3

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/BackgroundLoginCommand;->_IBackgroundLoginCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/account/BackgroundLoginCommand$IBackgroundLoginCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/BackgroundLoginCommand$IBackgroundLoginCommandData;->isIDPasswordExist()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/BackgroundLoginCommand;->requestLogin()V

    .line 39
    :goto_0
    return-void

    .line 30
    :cond_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->setLoginInfo(Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;)V

    .line 31
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/BackgroundLoginCommand;->_IBackgroundLoginCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/account/BackgroundLoginCommand$IBackgroundLoginCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/BackgroundLoginCommand$IBackgroundLoginCommandData;->getNormalLoginCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/BackgroundLoginCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/account/d;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/BackgroundLoginCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0
.end method
