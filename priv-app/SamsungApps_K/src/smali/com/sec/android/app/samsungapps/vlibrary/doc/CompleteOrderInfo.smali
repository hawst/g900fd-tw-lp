.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private _Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

.field public appServiceID:Ljava/lang/String;

.field private bValid:Z

.field public couponAmount:Ljava/lang/String;

.field public currency:Ljava/lang/String;

.field public giftCardAmount:Ljava/lang/String;

.field public languageCode:Ljava/lang/String;

.field public orderDate:Ljava/lang/String;

.field public orderID:Ljava/lang/String;

.field public paymentAmount:Ljava/lang/String;

.field public paymentDate:Ljava/lang/String;

.field public paymentDetailType:Ljava/lang/String;

.field public paymentID:Ljava/lang/String;

.field public paymentMethod:Ljava/lang/String;

.field public paymentType:Ljava/lang/String;

.field public pgCode:Ljava/lang/String;

.field public pgMessage:Ljava/lang/String;

.field public productID:Ljava/lang/String;

.field public productName:Ljava/lang/String;

.field public requestID:Ljava/lang/String;

.field public resultCode:Ljava/lang/String;

.field public resultDetailMessage:Ljava/lang/String;

.field public resultMessage:Ljava/lang/String;

.field public retryCount:Ljava/lang/String;

.field public tax:Ljava/lang/String;

.field public taxIncluded:Ljava/lang/String;

.field public totalAmount:Ljava/lang/String;

.field public userID:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->appServiceID:Ljava/lang/String;

    .line 17
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->resultCode:Ljava/lang/String;

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->resultMessage:Ljava/lang/String;

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->resultDetailMessage:Ljava/lang/String;

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->requestID:Ljava/lang/String;

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->userID:Ljava/lang/String;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->pgCode:Ljava/lang/String;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->pgMessage:Ljava/lang/String;

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->languageCode:Ljava/lang/String;

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->orderID:Ljava/lang/String;

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->orderDate:Ljava/lang/String;

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->paymentID:Ljava/lang/String;

    .line 29
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->paymentDate:Ljava/lang/String;

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->tax:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->currency:Ljava/lang/String;

    .line 33
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->taxIncluded:Ljava/lang/String;

    .line 37
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->paymentMethod:Ljava/lang/String;

    .line 38
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->paymentType:Ljava/lang/String;

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->paymentDetailType:Ljava/lang/String;

    .line 40
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->productID:Ljava/lang/String;

    .line 41
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->productName:Ljava/lang/String;

    .line 42
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->_Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 44
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->bValid:Z

    .line 48
    const/4 v2, 0x0

    .line 50
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v2, v0

    .line 56
    :goto_0
    if-nez v2, :cond_1

    .line 143
    :cond_0
    :goto_1
    return-void

    .line 51
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 58
    :cond_1
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->_Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 59
    invoke-virtual {v2}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v3

    .line 60
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 62
    :try_start_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 63
    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 64
    iget-object v5, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->_Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {v5, v0, v4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 65
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2

    .line 71
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->_Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v0, v3, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;->mappingClass(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 104
    :try_start_2
    const-string v0, "productInfoList"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 105
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    move v0, v1

    .line 107
    :goto_3
    if-ge v0, v3, :cond_5

    .line 109
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    .line 110
    const-string v4, "productID"

    invoke-direct {p0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->readJSONObject(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 111
    if-eqz v4, :cond_3

    .line 113
    iput-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->productID:Ljava/lang/String;

    .line 115
    :cond_3
    const-string v4, "productName"

    invoke-direct {p0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->readJSONObject(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 116
    if-eqz v1, :cond_4

    .line 118
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->productName:Ljava/lang/String;

    .line 107
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 122
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->_Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    const-string v1, "productID"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->productID:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->_Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    const-string v1, "productName"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->productName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    .line 131
    :goto_4
    iput-boolean v6, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->bValid:Z

    .line 134
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->couponAmount:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-ne v0, v6, :cond_6

    .line 135
    const-string v0, "0.0"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->couponAmount:Ljava/lang/String;

    .line 139
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->giftCardAmount:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-ne v0, v6, :cond_0

    .line 140
    const-string v0, "0.0"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->giftCardAmount:Ljava/lang/String;

    goto/16 :goto_1

    .line 124
    :catch_2
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_4
.end method

.method private readJSONObject(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    :try_start_0
    invoke-virtual {p1, p2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 152
    :goto_0
    return-object v0

    .line 149
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 152
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getMap()Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->_Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    return-object v0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 158
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->bValid:Z

    return v0
.end method
