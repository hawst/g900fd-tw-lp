.class public Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLinkFactory;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static final DEEPLINK:Ljava/lang/String; = "DEEPLINK"

.field private static final DEEPLINKTYPE:Ljava/lang/String; = "DEEPLINKTYPE"


# instance fields
.field sharedPref:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLinkFactory;->sharedPref:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    .line 13
    return-void
.end method


# virtual methods
.method public clearDeepLink()V
    .locals 3

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLinkFactory;->sharedPref:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v1, "DEEPLINK"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 44
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLinkFactory;->sharedPref:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v1, "DEEPLINKTYPE"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 45
    return-void
.end method

.method public load()Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x0

    return-object v0
.end method

.method public save(Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;)V
    .locals 0

    .prologue
    .line 39
    return-void
.end method
