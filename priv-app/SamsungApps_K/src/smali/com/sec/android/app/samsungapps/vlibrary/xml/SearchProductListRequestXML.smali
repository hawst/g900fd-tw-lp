.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/SearchProductListRequestXML;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;
.source "ProGuard"


# instance fields
.field private _additionalString:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;I)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 14
    const-string v3, "searchProductListEx"

    const-string v4, "2040"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v5, p3

    move v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 10
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/SearchProductListRequestXML;->_additionalString:Ljava/lang/String;

    .line 16
    const-string v0, "keyword"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getKeyword()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SearchProductListRequestXML;->cleanInvalidXmlChars(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SearchProductListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    const-string v0, "imgWidth"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getWidth()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SearchProductListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    const-string v0, "imgHeight"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getHeight()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SearchProductListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    const-string v0, "bestselling"

    .line 22
    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getSortOrder()Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 24
    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getSortOrder()Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 27
    :cond_0
    const-string v1, "alignOrder"

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SearchProductListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    invoke-virtual {p0, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SearchProductListRequestXML;->addListStEdAsParam(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;)V

    .line 29
    const-string v1, "contentType"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->getContentType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SearchProductListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    const-string v1, "qlDomainCode"

    const-string v2, "sa"

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SearchProductListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDeviceType()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document$DeviceType;

    move-result-object v1

    .line 35
    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document$DeviceType;->none:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document$DeviceType;

    if-eq v1, v2, :cond_1

    .line 36
    const-string v2, "qlDeviceType"

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document$DeviceType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SearchProductListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    :cond_1
    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getQueryLogInputMethod()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    move-result-object v1

    .line 40
    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;->none:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    if-eq v1, v2, :cond_2

    .line 41
    const-string v2, "qlInputMethod"

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SearchProductListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    :cond_2
    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;->ac:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    if-ne v1, v2, :cond_3

    .line 48
    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getSearchClickURL()Ljava/lang/String;

    move-result-object v1

    .line 49
    const-string v2, "srchClickURL"

    invoke-virtual {p0, v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/SearchProductListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getKeyword()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "___"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/SearchProductListRequestXML;->_additionalString:Ljava/lang/String;

    .line 53
    return-void
.end method

.method private static cleanInvalidXmlChars(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 62
    :try_start_0
    const-string v0, "[^^\t\r\n -\ud7ff\ue000-\ufffd]"

    .line 63
    const-string v1, " "

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 66
    :goto_0
    return-object v0

    .line 64
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 66
    const-string v0, ""

    goto :goto_0
.end method


# virtual methods
.method protected additionalIdentifierName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/SearchProductListRequestXML;->_additionalString:Ljava/lang/String;

    return-object v0
.end method
