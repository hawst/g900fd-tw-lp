.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract checkAlipayInstallCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract complete()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
.end method

.method public abstract createPayment()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract getAlipayInitResult()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayIntResult;
.end method

.method public abstract init()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract installAlipay()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract onNotifyAlipay(Z)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract process()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method
