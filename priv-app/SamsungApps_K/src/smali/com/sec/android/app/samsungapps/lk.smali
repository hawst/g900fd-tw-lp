.class final Lcom/sec/android/app/samsungapps/lk;
.super Landroid/preference/Preference;
.source "ProGuard"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/SignUpActivity;

.field private b:Ljava/lang/String;

.field private c:Landroid/view/View;

.field private d:Lcom/sec/android/app/samsungapps/lj;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/SignUpActivity;Landroid/content/Context;Lcom/sec/android/app/samsungapps/lj;)V
    .locals 1

    .prologue
    .line 615
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/lk;->a:Lcom/sec/android/app/samsungapps/SignUpActivity;

    .line 616
    invoke-direct {p0, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 609
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/lk;->b:Ljava/lang/String;

    .line 617
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/lk;->d:Lcom/sec/android/app/samsungapps/lj;

    .line 618
    const v0, 0x7f040058

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/lk;->setLayoutResource(I)V

    invoke-virtual {p0, p0}, Lcom/sec/android/app/samsungapps/lk;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 619
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/SignUpActivity;Landroid/content/Context;Lcom/sec/android/app/samsungapps/lj;B)V
    .locals 1

    .prologue
    .line 621
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/lk;->a:Lcom/sec/android/app/samsungapps/SignUpActivity;

    .line 622
    invoke-direct {p0, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 609
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/lk;->b:Ljava/lang/String;

    .line 623
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/lk;->d:Lcom/sec/android/app/samsungapps/lj;

    .line 624
    const v0, 0x7f0400d5

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/lk;->setLayoutResource(I)V

    invoke-virtual {p0, p0}, Lcom/sec/android/app/samsungapps/lk;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 625
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/lk;)Lcom/sec/android/app/samsungapps/lj;
    .locals 1

    .prologue
    .line 608
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/lk;->d:Lcom/sec/android/app/samsungapps/lj;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 628
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/lk;->b:Ljava/lang/String;

    .line 629
    return-void
.end method

.method protected final onBindView(Landroid/view/View;)V
    .locals 4

    .prologue
    const v3, 0x7f0c017d

    .line 633
    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    .line 634
    const v0, 0x7f0c017c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 635
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/lk;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 638
    const v0, 0x7f0c02de

    :try_start_0
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 639
    if-eqz v0, :cond_0

    .line 640
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 641
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/lk;->a:Lcom/sec/android/app/samsungapps/SignUpActivity;

    const v2, 0x7f08011e

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/SignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 650
    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/lk;->c:Landroid/view/View;

    .line 651
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/lk;->c:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    .line 653
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/lk;->d:Lcom/sec/android/app/samsungapps/lj;

    iget-boolean v1, v1, Lcom/sec/android/app/samsungapps/lj;->a:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 654
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/lk;->c:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    new-instance v1, Lcom/sec/android/app/samsungapps/ll;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/ll;-><init>(Lcom/sec/android/app/samsungapps/lk;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 655
    return-void

    .line 643
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/lk;->a:Lcom/sec/android/app/samsungapps/SignUpActivity;

    const v2, 0x7f080291

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/SignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 646
    :catch_0
    move-exception v0

    .line 647
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SignUpActivity::NullpointerException::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 669
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/lk;->c:Landroid/view/View;

    const v1, 0x7f0c017d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    .line 671
    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 672
    return v2

    .line 671
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
