.class final Lcom/sec/android/app/samsungapps/vlibrary2/content/p;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqGUIDProductDetailExCommand;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqGUIDProductDetailExCommand;)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/p;->a:Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqGUIDProductDetailExCommand;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceiveResult(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 5

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/p;->a:Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqGUIDProductDetailExCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqGUIDProductDetailExCommand;->_ProductDetailParser:Lcom/sec/android/app/samsungapps/vlibrary2/unc/ProductDetailParser;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqGUIDProductDetailExCommand;->access$100(Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqGUIDProductDetailExCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/unc/ProductDetailParser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/unc/ProductDetailParser;->getMap()Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    move-result-object v1

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/p;->a:Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqGUIDProductDetailExCommand;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqGUIDProductDetailExCommand;->_IContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand$IReqDetailCommandData;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand$IReqDetailCommandData;->setProductDetailResultMap(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 52
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ProductDetailMainParser;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/p;->a:Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqGUIDProductDetailExCommand;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqGUIDProductDetailExCommand;->_IContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand$IReqDetailCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand$IReqDetailCommandData;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ProductDetailMainParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    .line 53
    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ProductDetailMainParser;->openMap()V

    .line 54
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 55
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 56
    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 57
    invoke-virtual {v2, v0, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ProductDetailMainParser;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 59
    :cond_0
    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ProductDetailMainParser;->closeMap()V

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/p;->a:Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqGUIDProductDetailExCommand;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqGUIDProductDetailExCommand;->onFinalResult(Z)V
    invoke-static {v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqGUIDProductDetailExCommand;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqGUIDProductDetailExCommand;Z)V

    .line 61
    return-void
.end method
