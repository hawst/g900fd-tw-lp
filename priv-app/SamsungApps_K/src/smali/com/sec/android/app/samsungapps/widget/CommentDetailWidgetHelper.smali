.class public Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/widget/interfaces/ICommentDetailWidgetData;


# instance fields
.field a:I

.field b:I

.field c:Ljava/lang/String;

.field d:Landroid/content/Context;

.field e:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

.field f:I

.field g:Ljava/lang/String;

.field h:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

.field private final i:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput v2, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->a:I

    .line 23
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->b:I

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->c:Ljava/lang/String;

    .line 25
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->d:Landroid/content/Context;

    .line 26
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->e:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 32
    iput v2, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->f:I

    .line 33
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->g:Ljava/lang/String;

    .line 38
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->h:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    .line 40
    const-string v0, "CommentDetailWidgetHelper"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->i:Ljava/lang/String;

    .line 44
    iput p2, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->a:I

    .line 45
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->d:Landroid/content/Context;

    .line 46
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->e:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 47
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 54
    iput v2, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->a:I

    .line 55
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->b:I

    .line 56
    iput v2, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->f:I

    .line 57
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->g:Ljava/lang/String;

    .line 58
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->c:Ljava/lang/String;

    .line 59
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->d:Landroid/content/Context;

    .line 60
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->e:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 61
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->h:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    .line 62
    return-void
.end method

.method public getComment()Ljava/lang/String;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getOldComment()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 114
    const-string v0, ""

    .line 116
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->e:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    if-nez v1, :cond_1

    .line 118
    const-string v1, "CommentDetailWidgetHelper::getOldComment::Command is null"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 144
    :cond_0
    :goto_0
    return-object v0

    .line 122
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->g:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->g:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eq v1, v2, :cond_2

    .line 124
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CommentDetailWidgetHelper::getOldComment::Restore review, length="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->g:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->g:Ljava/lang/String;

    .line 127
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->g:Ljava/lang/String;

    goto :goto_0

    .line 132
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->e:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    instance-of v1, v1, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;

    if-eqz v1, :cond_3

    .line 134
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->e:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->getCommentText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 138
    :cond_3
    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->a:I

    if-ne v1, v2, :cond_0

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->e:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->getOldCommentText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getOldRating()D
    .locals 4

    .prologue
    const/4 v3, -0x1

    const-wide/high16 v0, 0x4024000000000000L    # 10.0

    .line 154
    .line 156
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->e:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    if-nez v2, :cond_1

    .line 158
    const-string v2, "CommentDetailWidgetHelper::getOldRating::Command is null"

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 187
    :cond_0
    :goto_0
    return-wide v0

    .line 162
    :cond_1
    iget v2, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->f:I

    if-eq v2, v3, :cond_2

    .line 164
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CommentDetailWidgetHelper::getOldRating::Restore rating, rate="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    .line 166
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->f:I

    int-to-double v0, v0

    .line 167
    iput v3, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->f:I

    goto :goto_0

    .line 171
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->e:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    instance-of v2, v2, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;

    if-eqz v2, :cond_3

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->e:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->getRatingValue()I

    move-result v0

    int-to-double v0, v0

    goto :goto_0

    .line 177
    :cond_3
    iget v2, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->a:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->e:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->getRatingValue()I

    move-result v0

    int-to-double v0, v0

    goto :goto_0

    .line 181
    :cond_4
    iget v2, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->a:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->e:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->getOldRating()D

    move-result-wide v0

    goto :goto_0
.end method

.method public getProhibitWords()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 193
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    .line 194
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->e:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    instance-of v1, v1, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;

    if-eqz v1, :cond_1

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->e:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->getProhibitWords()[Ljava/lang/String;

    move-result-object v0

    .line 208
    :cond_0
    :goto_0
    return-object v0

    .line 200
    :cond_1
    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->a:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->e:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->getProhibitWords()[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 203
    :cond_2
    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->a:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 205
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->e:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->getProhibitWords()[Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getRating()I
    .locals 1

    .prologue
    .line 269
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->b:I

    return v0
.end method

.method public isModifyComment()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 231
    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->a:I

    if-ne v1, v0, :cond_0

    .line 233
    :goto_0
    return v0

    .line 231
    :cond_0
    const/4 v0, 0x0

    .line 233
    goto :goto_0
.end method

.method public isMyReviewDuplicated()Z
    .locals 2

    .prologue
    .line 214
    const/4 v0, 0x0

    .line 215
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->e:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    instance-of v1, v1, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;

    if-eqz v1, :cond_1

    .line 217
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->e:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->isMyReviewDuplicated()Z

    move-result v0

    .line 223
    :cond_0
    :goto_0
    return v0

    .line 219
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->e:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    instance-of v1, v1, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;

    if-eqz v1, :cond_0

    .line 221
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->e:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->isMyReviewDuplicated()Z

    move-result v0

    goto :goto_0
.end method

.method public onCommandResult(Z)V
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->h:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    if-eqz v0, :cond_0

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->h:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;->onCommandResult(Z)V

    .line 282
    :cond_0
    return-void
.end method

.method public sendRequest(Ljava/lang/String;ILcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->e:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    if-nez v0, :cond_1

    .line 69
    const-string v0, "CommentDetailWidgetHelper::sendRequest::Command is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 105
    :cond_0
    :goto_0
    return-void

    .line 73
    :cond_1
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->h:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    .line 74
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->c:Ljava/lang/String;

    .line 75
    iput p2, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->b:I

    .line 77
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->a:I

    packed-switch v0, :pswitch_data_0

    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CommentDetailWidgetHelper::sendRequest::Invalid Type="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 80
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->e:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    instance-of v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;

    if-eqz v0, :cond_2

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->e:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/AddCommentCommand;->addComment()V

    goto :goto_0

    .line 84
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->e:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    instance-of v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->e:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->addComment()V

    goto :goto_0

    .line 91
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->e:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    instance-of v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;

    if-eqz v0, :cond_3

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->e:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->modifyComment()V

    goto :goto_0

    .line 95
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->e:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    instance-of v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->e:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->modifyComment()V

    goto :goto_0

    .line 77
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setRestoreComment(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 242
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->g:Ljava/lang/String;

    .line 243
    return-void
.end method

.method public setRestoreRating(I)V
    .locals 0

    .prologue
    .line 251
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->f:I

    .line 252
    return-void
.end method
