.class public Lcom/sec/android/app/samsungapps/vlibrary/workprocess/SignUpInterface;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field mbStartWithOSPID:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/workprocess/SignUpInterface;->mbStartWithOSPID:Z

    .line 23
    return-void
.end method

.method private getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;
    .locals 1

    .prologue
    .line 87
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    return-object v0
.end method

.method private sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;)V
    .locals 1

    .prologue
    .line 92
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 93
    return-void
.end method


# virtual methods
.method public calcAge(III)I
    .locals 1

    .prologue
    .line 55
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AgeCalculator;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AgeCalculator;-><init>()V

    .line 56
    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AgeCalculator;->calcAge(III)I

    move-result v0

    return v0
.end method

.method public checkAgeLimitation(III)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 61
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/vlibrary/workprocess/SignUpInterface;->calcAge(III)I

    move-result v1

    .line 62
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v2

    .line 63
    if-nez v2, :cond_1

    .line 65
    const-string v1, "error"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->err(Ljava/lang/String;)V

    .line 71
    :cond_0
    :goto_0
    return v0

    .line 68
    :cond_1
    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getSingUpLimitationAge()I

    move-result v2

    if-gt v2, v1, :cond_0

    .line 69
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public checkIDDuplication(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ResultReceiver;)V
    .locals 3

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/workprocess/SignUpInterface;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/IDDuplicationCheckInfo;

    invoke-direct {v1, p1, p2, p3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IDDuplicationCheckInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/workprocess/b;

    invoke-direct {v2, p0, p4}, Lcom/sec/android/app/samsungapps/vlibrary/workprocess/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/workprocess/SignUpInterface;Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ResultReceiver;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->checkDuplication(Lcom/sec/android/app/samsungapps/vlibrary/doc/IDDuplicationCheckInfo;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 104
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/workprocess/SignUpInterface;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;)V

    .line 105
    return-void
.end method

.method public getAgeLimitation()I
    .locals 1

    .prologue
    .line 76
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    .line 77
    if-nez v0, :cond_0

    .line 79
    const-string v0, "error"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->err(Ljava/lang/String;)V

    .line 80
    const/4 v0, 0x0

    .line 82
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getSingUpLimitationAge()I

    move-result v0

    goto :goto_0
.end method

.method public isStartedWithOSPID()Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/workprocess/SignUpInterface;->mbStartWithOSPID:Z

    return v0
.end method

.method public join(Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ResultReceiver;)V
    .locals 3

    .prologue
    .line 109
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/workprocess/SignUpInterface;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/workprocess/SignUpInterface;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/workprocess/c;

    invoke-direct {v2, p0, p2}, Lcom/sec/android/app/samsungapps/vlibrary/workprocess/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/workprocess/SignUpInterface;Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ResultReceiver;)V

    invoke-virtual {v0, p1, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->joinRegister(Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 116
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/workprocess/SignUpInterface;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;)V

    .line 117
    return-void
.end method

.method public joinOld(Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ResultReceiver;)V
    .locals 3

    .prologue
    .line 121
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/workprocess/SignUpInterface;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/workprocess/SignUpInterface;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/workprocess/d;

    invoke-direct {v2, p0, p2}, Lcom/sec/android/app/samsungapps/vlibrary/workprocess/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/workprocess/SignUpInterface;Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ResultReceiver;)V

    invoke-virtual {v0, p1, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->joinRegisterOld(Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 128
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/workprocess/SignUpInterface;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;)V

    .line 129
    return-void
.end method

.method protected onHandleSignUpRequestResult(Z)V
    .locals 0

    .prologue
    .line 43
    return-void
.end method

.method public start(Z)V
    .locals 2

    .prologue
    .line 27
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/workprocess/SignUpInterface;->mbStartWithOSPID:Z

    .line 28
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getAccountEventManager()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEventManager;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/workprocess/a;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary/workprocess/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/workprocess/SignUpInterface;)V

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEventManager;->requestSignUp(Lcom/sec/android/app/samsungapps/vlibrary/workprocess/SignUpInterface;Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;)V

    .line 35
    return-void
.end method
