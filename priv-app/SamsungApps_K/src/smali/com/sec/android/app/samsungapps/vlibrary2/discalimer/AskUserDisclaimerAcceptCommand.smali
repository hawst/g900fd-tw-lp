.class public Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AskUserDisclaimerAcceptCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _IAskUserDisclaimerAcceptCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AskUserDisclaimerAcceptCommand$IAskUserDisclaimerAcceptCommandData;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AskUserDisclaimerAcceptCommand$IAskUserDisclaimerAcceptCommandData;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AskUserDisclaimerAcceptCommand;->_IAskUserDisclaimerAcceptCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AskUserDisclaimerAcceptCommand$IAskUserDisclaimerAcceptCommandData;

    .line 19
    return-void
.end method


# virtual methods
.method protected destroyApp()V
    .locals 1

    .prologue
    .line 68
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->exitSamsungApps()V

    .line 69
    return-void
.end method

.method public getDisclaimer()Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AskUserDisclaimerAcceptCommand;->_IAskUserDisclaimerAcceptCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AskUserDisclaimerAcceptCommand$IAskUserDisclaimerAcceptCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AskUserDisclaimerAcceptCommand$IAskUserDisclaimerAcceptCommandData;->getNewDisclaimer()Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;

    move-result-object v0

    return-object v0
.end method

.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 2

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AskUserDisclaimerAcceptCommand;->_IAskUserDisclaimerAcceptCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AskUserDisclaimerAcceptCommand$IAskUserDisclaimerAcceptCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AskUserDisclaimerAcceptCommand$IAskUserDisclaimerAcceptCommandData;->getDisclaimerViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AskUserDisclaimerAcceptCommand;->_Context:Landroid/content/Context;

    invoke-interface {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;->invoke(Landroid/content/Context;Ljava/lang/Object;)V

    .line 24
    return-void
.end method

.method protected onUserAgree()V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AskUserDisclaimerAcceptCommand;->_IAskUserDisclaimerAcceptCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AskUserDisclaimerAcceptCommand$IAskUserDisclaimerAcceptCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AskUserDisclaimerAcceptCommand$IAskUserDisclaimerAcceptCommandData;->setUserAgreeToDisclaimer()V

    .line 63
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AskUserDisclaimerAcceptCommand;->onFinalResult(Z)V

    .line 64
    return-void
.end method

.method public userAgree(Z)V
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AskUserDisclaimerAcceptCommand;->_Context:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 39
    const-string v0, "AskUserDisclaimerAcceptCommand::userAgree::Send Broadcast Message"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    .line 41
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.samsungapps.Disclaimer"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 45
    const-string v1, "Agreed"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 46
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AskUserDisclaimerAcceptCommand;->_Context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 49
    :cond_0
    if-nez p1, :cond_1

    .line 51
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AskUserDisclaimerAcceptCommand;->destroyApp()V

    .line 52
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AskUserDisclaimerAcceptCommand;->onFinalResult(Z)V

    .line 58
    :goto_0
    return-void

    .line 56
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/AskUserDisclaimerAcceptCommand;->onUserAgree()V

    goto :goto_0
.end method
