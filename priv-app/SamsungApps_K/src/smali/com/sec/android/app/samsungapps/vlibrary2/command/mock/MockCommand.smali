.class public Lcom/sec/android/app/samsungapps/vlibrary2/command/mock/MockCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field public bExecuted:Z

.field public bOnCancelCalled:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    return-void
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/mock/MockCommand;->bExecuted:Z

    .line 17
    return-void
.end method

.method protected onCancel()V
    .locals 1

    .prologue
    .line 21
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->onCancel()V

    .line 22
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/mock/MockCommand;->bOnCancelCalled:Z

    .line 23
    return-void
.end method

.method protected onFinalResult(Z)V
    .locals 0

    .prologue
    .line 28
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->onFinalResult(Z)V

    .line 29
    return-void
.end method

.method public result(Z)V
    .locals 0

    .prologue
    .line 33
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/mock/MockCommand;->onFinalResult(Z)V

    .line 34
    return-void
.end method
