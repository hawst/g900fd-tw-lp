.class public Lcom/sec/android/app/samsungapps/SecurityCodeHelpAmexActivity;
.super Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;
.source "ProGuard"


# instance fields
.field private a:Landroid/widget/LinearLayout;

.field private b:Landroid/widget/Button;

.field private c:I

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 15
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;-><init>()V

    .line 18
    iput v0, p0, Lcom/sec/android/app/samsungapps/SecurityCodeHelpAmexActivity;->c:I

    .line 19
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/SecurityCodeHelpAmexActivity;->d:Z

    return-void
.end method


# virtual methods
.method protected getFocus()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SecurityCodeHelpAmexActivity;->a:Landroid/widget/LinearLayout;

    const v1, 0x7f0c0096

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SecurityCodeHelpAmexActivity;->b:Landroid/widget/Button;

    .line 99
    iput v2, p0, Lcom/sec/android/app/samsungapps/SecurityCodeHelpAmexActivity;->c:I

    .line 100
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/SecurityCodeHelpAmexActivity;->d:Z

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SecurityCodeHelpAmexActivity;->b:Landroid/widget/Button;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SecurityCodeHelpAmexActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SecurityCodeHelpAmexActivity;->b:Landroid/widget/Button;

    if-ne v0, v1, :cond_0

    .line 103
    iput v3, p0, Lcom/sec/android/app/samsungapps/SecurityCodeHelpAmexActivity;->c:I

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SecurityCodeHelpAmexActivity;->b:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isPressed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    iput-boolean v3, p0, Lcom/sec/android/app/samsungapps/SecurityCodeHelpAmexActivity;->d:Z

    .line 109
    :cond_0
    return-void
.end method

.method protected getHeaderView()Landroid/view/View;
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getScrollView()Landroid/widget/ScrollView;
    .locals 1

    .prologue
    .line 54
    const v0, 0x7f0c0149

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SecurityCodeHelpAmexActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    return-object v0
.end method

.method protected getTailView()Landroid/view/View;
    .locals 1

    .prologue
    .line 59
    const v0, 0x7f0c0095

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SecurityCodeHelpAmexActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected getaddingResourceId()I
    .locals 2

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SecurityCodeHelpAmexActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 86
    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 87
    const v0, 0x7f0c0029

    .line 92
    :goto_0
    return v0

    .line 89
    :cond_0
    const v0, 0x7f0c0148

    goto :goto_0
.end method

.method protected getremovingResourceId()I
    .locals 2

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SecurityCodeHelpAmexActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 72
    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 73
    const v0, 0x7f0c0148

    .line 78
    :goto_0
    return v0

    .line 75
    :cond_0
    const v0, 0x7f0c0029

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 23
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;->onBackPressed()V

    .line 24
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 28
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;->onCreate(Landroid/os/Bundle;)V

    .line 29
    const v0, 0x7f040092

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SecurityCodeHelpAmexActivity;->setMainView(I)V

    const v0, 0x7f080151

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SecurityCodeHelpAmexActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const v3, 0xb0003

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/SecurityCodeHelpAmexActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;ZI)V

    const v0, 0x7f0c0095

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SecurityCodeHelpAmexActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SecurityCodeHelpAmexActivity;->a:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SecurityCodeHelpAmexActivity;->a:Landroid/widget/LinearLayout;

    const v1, 0x7f0c0096

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SecurityCodeHelpAmexActivity;->b:Landroid/widget/Button;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SecurityCodeHelpAmexActivity;->b:Landroid/widget/Button;

    const v1, 0x7f080236

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SecurityCodeHelpAmexActivity;->b:Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/app/samsungapps/im;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/im;-><init>(Lcom/sec/android/app/samsungapps/SecurityCodeHelpAmexActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 30
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 50
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;->onDestroy()V

    .line 51
    return-void
.end method

.method protected requestFocus()V
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SecurityCodeHelpAmexActivity;->a:Landroid/widget/LinearLayout;

    const v1, 0x7f0c0096

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SecurityCodeHelpAmexActivity;->b:Landroid/widget/Button;

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SecurityCodeHelpAmexActivity;->b:Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/samsungapps/SecurityCodeHelpAmexActivity;->c:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SecurityCodeHelpAmexActivity;->b:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    .line 118
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/SecurityCodeHelpAmexActivity;->d:Z

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SecurityCodeHelpAmexActivity;->b:Landroid/widget/Button;

    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/SecurityCodeHelpAmexActivity;->d:Z

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setPressed(Z)V

    .line 122
    :cond_0
    return-void
.end method
