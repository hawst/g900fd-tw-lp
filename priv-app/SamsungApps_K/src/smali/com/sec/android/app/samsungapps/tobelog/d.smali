.class final Lcom/sec/android/app/samsungapps/tobelog/d;
.super Landroid/os/Handler;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/tobelog/LogManager;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/tobelog/LogManager;)V
    .locals 0

    .prologue
    .line 310
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/tobelog/d;->a:Lcom/sec/android/app/samsungapps/tobelog/LogManager;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 315
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 334
    :goto_0
    return-void

    .line 319
    :pswitch_0
    iget v0, p1, Landroid/os/Message;->arg1:I

    .line 320
    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;->RESULT_SUCCESS:Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;->value()I

    move-result v1

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;->RESULT_ALREADY_REGISTERED:Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;->value()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 322
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/d;->a:Lcom/sec/android/app/samsungapps/tobelog/LogManager;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->a(Lcom/sec/android/app/samsungapps/tobelog/LogManager;)Lcom/sec/android/app/samsungapps/tobelog/Preferences;

    move-result-object v0

    const-string v1, "registered"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/tobelog/Preferences;->putBoolean(Ljava/lang/String;Z)V

    .line 323
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/d;->a:Lcom/sec/android/app/samsungapps/tobelog/LogManager;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->b(Lcom/sec/android/app/samsungapps/tobelog/LogManager;)V

    goto :goto_0

    .line 325
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/d;->a:Lcom/sec/android/app/samsungapps/tobelog/LogManager;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->a(Lcom/sec/android/app/samsungapps/tobelog/LogManager;)Lcom/sec/android/app/samsungapps/tobelog/Preferences;

    move-result-object v0

    const-string v1, "registered"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/tobelog/Preferences;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 332
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/d;->a:Lcom/sec/android/app/samsungapps/tobelog/LogManager;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->a(Lcom/sec/android/app/samsungapps/tobelog/LogManager;)Lcom/sec/android/app/samsungapps/tobelog/Preferences;

    move-result-object v0

    const-string v1, "registered"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/tobelog/Preferences;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 315
    nop

    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
