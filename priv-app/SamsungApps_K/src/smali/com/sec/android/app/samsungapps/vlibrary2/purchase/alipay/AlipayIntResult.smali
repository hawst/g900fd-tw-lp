.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayIntResult;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# instance fields
.field public orderid:Ljava/lang/String;

.field public orderinfo:Ljava/lang/String;

.field public paymentid:Ljava/lang/String;

.field public productid:Ljava/lang/String;

.field public responsetime:I

.field public retrycount:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayIntResult;

    invoke-static {p1, p2, v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;->mappingField(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Z

    .line 25
    return-void
.end method

.method public clearContainer()V
    .locals 0

    .prologue
    .line 30
    return-void
.end method

.method public closeMap()V
    .locals 0

    .prologue
    .line 21
    return-void
.end method

.method public findString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    return-object v0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayIntResult;->orderinfo:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayIntResult;->orderinfo:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    const/4 v0, 0x1

    .line 53
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public openMap()V
    .locals 0

    .prologue
    .line 17
    return-void
.end method

.method public setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 45
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    return v0
.end method
