.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

.field public static final enum CANCELING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

.field public static final enum DOWNLOAD:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

.field public static final enum DOWNLOAD_DONE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

.field public static final enum FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

.field public static final enum IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

.field public static final enum REQUEST_URL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

.field public static final enum RETRY:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 6
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    .line 7
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    const-string v1, "DOWNLOAD"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;->DOWNLOAD:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    .line 8
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    const-string v1, "DOWNLOAD_DONE"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;->DOWNLOAD_DONE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    .line 9
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    const-string v1, "RETRY"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;->RETRY:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    .line 10
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    const-string v1, "REQUEST_URL"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;->REQUEST_URL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    const-string v1, "CANCELING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;->CANCELING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    const-string v1, "CANCELED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;->CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    const-string v1, "FAILED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    .line 4
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;->DOWNLOAD:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;->DOWNLOAD_DONE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;->RETRY:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;->REQUEST_URL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;->CANCELING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;->CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;
    .locals 1

    .prologue
    .line 4
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;
    .locals 1

    .prologue
    .line 4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    return-object v0
.end method
