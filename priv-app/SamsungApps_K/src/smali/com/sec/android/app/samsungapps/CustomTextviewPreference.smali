.class public Lcom/sec/android/app/samsungapps/CustomTextviewPreference;
.super Landroid/preference/Preference;
.source "ProGuard"


# static fields
.field public static final CUSTOM_TYPE_ABOUT:I = 0x9

.field public static final CUSTOM_TYPE_RESET_LOCALE:I = 0x2

.field public static final CUSTOM_TYPE_SAMSUNG_ACCOUNT:I = 0x1

.field public static final CUSTOM_TYPE_SEARCH_SETTING:I = 0xd

.field public static final CUSTOM_TYPE_SETTINGS_AD_PREFERENCE:I = 0x5

.field public static final CUSTOM_TYPE_SETTINGS_AUTO_UPDATE:I = 0xa

.field public static final CUSTOM_TYPE_SETTINGS_CUSTOMER_SUPPORT:I = 0x6

.field public static final CUSTOM_TYPE_SETTINGS_NOTIFY_APP_UPDATES:I = 0xb

.field public static final CUSTOM_TYPE_SETTINGS_NOTIFY_STORE_ACTIVITIES:I = 0xc

.field public static final CUSTOM_TYPE_SETTINGS_PURCHASE_PROTECTION:I = 0xe

.field public static final CUSTOM_TYPE_SETTINGS_PUSH_NOTIFICATION_PREFERENCE:I = 0x4

.field public static final CUSTOM_TYPE_SETTINGS_SUGGEST_ALLSHARE_CONTENT_PREFERENCE:I = 0x8

.field public static final CUSTOM_TYPE_SETTINGS_SUPPORT_NOTICE:I = 0x7

.field public static final CUSTOM_TYPE_SETTINGS_UPDATE_NOTI:I = 0x3


# instance fields
.field private a:I

.field private b:I

.field private c:Landroid/view/View;

.field private d:Landroid/content/Context;

.field private e:Lcom/sec/android/app/samsungapps/SettingsListActivity;

.field private f:Landroid/widget/TextView;

.field private g:Z

.field private h:Landroid/widget/CheckBox;

.field protected final mRsrcs:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 52
    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 24
    iput v1, p0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->a:I

    .line 25
    iput v1, p0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->b:I

    .line 26
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->c:Landroid/view/View;

    .line 27
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->d:Landroid/content/Context;

    .line 44
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->e:Lcom/sec/android/app/samsungapps/SettingsListActivity;

    .line 45
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->f:Landroid/widget/TextView;

    .line 46
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->g:Z

    .line 47
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->h:Landroid/widget/CheckBox;

    .line 54
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->d:Landroid/content/Context;

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->d:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/samsungapps/SettingsListActivity;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->e:Lcom/sec/android/app/samsungapps/SettingsListActivity;

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->mRsrcs:Landroid/content/res/Resources;

    .line 58
    return-void
.end method


# virtual methods
.method public getPushServiceLoadingFlag()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->g:Z

    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->a:I

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->c:Landroid/view/View;

    return-object v0
.end method

.method protected onBindView(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 90
    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    .line 93
    iget v0, p0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->a:I

    packed-switch v0, :pswitch_data_0

    .line 130
    :cond_0
    :goto_0
    :pswitch_0
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->c:Landroid/view/View;

    .line 131
    return-void

    .line 96
    :pswitch_1
    const v0, 0x7f0c02b6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->e:Lcom/sec/android/app/samsungapps/SettingsListActivity;

    invoke-direct {v1, v2}, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;-><init>(Landroid/app/Activity;)V

    const-string v2, ""

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;->getSamsungAccount()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eq v1, v3, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->mRsrcs:Landroid/content/res/Resources;

    const v2, 0x7f08023b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->d:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->isLogedIn()Z

    move-result v1

    if-ne v1, v3, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->mRsrcs:Landroid/content/res/Resources;

    const v2, 0x7f0802b6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 99
    :pswitch_2
    const v0, 0x7f0c02b4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08033b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 102
    :pswitch_3
    const v0, 0x7f0c02e8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->e:Lcom/sec/android/app/samsungapps/SettingsListActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/SettingsListActivity;->getSubTitleUpdateNotification()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 105
    :pswitch_4
    const v0, 0x7f0c02b1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_2

    const v1, 0x7f080162

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_2
    const v0, 0x7f0c02b3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->f:Landroid/widget/TextView;

    const v0, 0x7f0c02b2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->h:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->h:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->f:Landroid/widget/TextView;

    const v1, 0x7f0801f4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->h:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->d:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->isPushSvcTurnedOn()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_0

    .line 111
    :pswitch_5
    const v0, 0x7f0c0307

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    const-string v1, "Customer Support"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 114
    :pswitch_6
    const v0, 0x7f0c0308

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    const-string v1, "Notice"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 117
    :pswitch_7
    const v0, 0x7f0c02b8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->h:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->h:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->h:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->d:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->isSuggestAllShareContentChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_0

    .line 120
    :pswitch_8
    const v0, 0x7f0c02a9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->e:Lcom/sec/android/app/samsungapps/SettingsListActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/SettingsListActivity;->getSubTitleAutoUpdate()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 126
    :pswitch_9
    const v0, 0x7f0c02ad

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v1

    if-eqz v1, :cond_3

    const v1, 0x7f080120

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    :cond_3
    const v1, 0x7f08019d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    .line 93
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_9
    .end packed-switch
.end method

.method public setPushServiceLoadingFlag(Z)V
    .locals 0

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->g:Z

    .line 66
    return-void
.end method

.method public setResourceId(I)V
    .locals 1

    .prologue
    .line 79
    iput p1, p0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->b:I

    .line 80
    iget v0, p0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->b:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->setLayoutResource(I)V

    .line 81
    return-void
.end method

.method public setType(I)V
    .locals 0

    .prologue
    .line 70
    iput p1, p0, Lcom/sec/android/app/samsungapps/CustomTextviewPreference;->a:I

    .line 71
    return-void
.end method
