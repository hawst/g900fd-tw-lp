.class public Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountNewVersionHandlerActivity;
.super Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerActivity;
.source "ProGuard"


# instance fields
.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerActivity;-><init>()V

    .line 11
    const v0, 0x8726

    iput v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountNewVersionHandlerActivity;->b:I

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountNewVersionHandlerActivity;->b:I

    if-ne p1, v0, :cond_0

    .line 49
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountNewVersionHandlerActivity;->endLoading()V

    .line 50
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 52
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountNewVersionHandlerActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginOldSamsungAccountCommand;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountNewVersionHandlerActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginOldSamsungAccountCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginOldSamsungAccountCommand;->onSamsungAccountLoginSuccess()V

    .line 68
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountNewVersionHandlerActivity;->finish()V

    .line 70
    return-void

    .line 58
    :cond_1
    if-eqz p3, :cond_2

    .line 61
    const-string v0, "error_message"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 63
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountNewVersionHandlerActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginOldSamsungAccountCommand;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountNewVersionHandlerActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginOldSamsungAccountCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginOldSamsungAccountCommand;->onSamsungAccountLoginFailed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 15
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerActivity;->onCreate(Landroid/os/Bundle;)V

    .line 16
    return-void
.end method

.method protected start()V
    .locals 5

    .prologue
    .line 20
    iget v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountNewVersionHandlerActivity;->b:I

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->getMyPackageInfo()Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.osp.app.signin.action.ADD_SAMSUNG_ACCOUNT"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "client_id"

    sget-object v4, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->SAC_CLIENT_ID:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "client_secret"

    sget-object v4, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->SAC_CLIENT_SECRET:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "mypackage"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "OSP_VER"

    const-string v3, "OSP_02"

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "MODE"

    const-string v3, "ADD_ACCOUNT"

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :try_start_0
    invoke-virtual {p0, v2, v0}, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountNewVersionHandlerActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountNewVersionHandlerActivity;->startLoading()V

    .line 21
    return-void

    .line 20
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountNewVersionHandlerActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginOldSamsungAccountCommand;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountNewVersionHandlerActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginOldSamsungAccountCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginOldSamsungAccountCommand;->onSamsungAccountLoginFailed()V

    goto :goto_0
.end method
