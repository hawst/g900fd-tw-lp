.class public Lcom/sec/android/app/samsungapps/vlibrary/util/SuggestManager;
.super Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;
.source "ProGuard"


# static fields
.field public static final SSUGGEST_DEFAULT_ACTIVITY:Ljava/lang/String; = "com.sec.ssuggest.activity.picks.PicksList"

.field public static final SSUGGEST_PKGNAME_HIDDEN:Ljava/lang/String; = "com.sec.ssuggest"


# instance fields
.field private mInstalledType:Lcom/sec/android/app/samsungapps/vlibrary/util/SuggestManager$SuggestInstalled;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/SuggestManager;->mInstalledType:Lcom/sec/android/app/samsungapps/vlibrary/util/SuggestManager$SuggestInstalled;

    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/SuggestManager;->_getInstalledType()Lcom/sec/android/app/samsungapps/vlibrary/util/SuggestManager$SuggestInstalled;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/SuggestManager;->mInstalledType:Lcom/sec/android/app/samsungapps/vlibrary/util/SuggestManager$SuggestInstalled;

    .line 31
    return-void
.end method

.method private _getInstalledType()Lcom/sec/android/app/samsungapps/vlibrary/util/SuggestManager$SuggestInstalled;
    .locals 3

    .prologue
    .line 38
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/util/SuggestManager$SuggestInstalled;->NOT_INSTALLED:Lcom/sec/android/app/samsungapps/vlibrary/util/SuggestManager$SuggestInstalled;

    .line 40
    const-string v1, "com.sec.ssuggest"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/SuggestManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 42
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/util/SuggestManager$SuggestInstalled;->HIDDEN_INSTALLED:Lcom/sec/android/app/samsungapps/vlibrary/util/SuggestManager$SuggestInstalled;

    .line 45
    :cond_0
    return-object v0
.end method


# virtual methods
.method public getInstalledType()Lcom/sec/android/app/samsungapps/vlibrary/util/SuggestManager$SuggestInstalled;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/SuggestManager;->mInstalledType:Lcom/sec/android/app/samsungapps/vlibrary/util/SuggestManager$SuggestInstalled;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 64
    const-string v0, ""

    .line 66
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/SuggestManager;->mInstalledType:Lcom/sec/android/app/samsungapps/vlibrary/util/SuggestManager$SuggestInstalled;

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/util/SuggestManager$SuggestInstalled;->HIDDEN_INSTALLED:Lcom/sec/android/app/samsungapps/vlibrary/util/SuggestManager$SuggestInstalled;

    if-ne v1, v2, :cond_0

    .line 68
    const-string v0, "com.sec.ssuggest"

    .line 71
    :cond_0
    return-object v0
.end method

.method public isInstalled()Z
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/SuggestManager;->mInstalledType:Lcom/sec/android/app/samsungapps/vlibrary/util/SuggestManager$SuggestInstalled;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/util/SuggestManager$SuggestInstalled;->NOT_INSTALLED:Lcom/sec/android/app/samsungapps/vlibrary/util/SuggestManager$SuggestInstalled;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public sendTermsAgreeMsgToSSuggest()V
    .locals 2

    .prologue
    .line 81
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.tgrape.android.radar.widget.TERMS_AGREE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 82
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/SuggestManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 83
    return-void
.end method
