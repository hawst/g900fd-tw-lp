.class public Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field _CommandArray:Ljava/util/ArrayList;

.field _CommandList:Ljava/util/LinkedList;

.field private curCommand:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 10
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;->_CommandArray:Ljava/util/ArrayList;

    .line 11
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;->_CommandList:Ljava/util/LinkedList;

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;->curCommand:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 16
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;->executeCommandDequeingFromList()V

    return-void
.end method

.method private createLinkedList()V
    .locals 3

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;->_CommandList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 31
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;->_CommandArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 33
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;->_CommandList:Ljava/util/LinkedList;

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    goto :goto_0

    .line 35
    :cond_0
    return-void
.end method

.method private executeCommandDequeingFromList()V
    .locals 3

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;->isCanceled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    :goto_0
    return-void

    .line 42
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;->_CommandList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;->curCommand:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;->curCommand:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    if-nez v0, :cond_1

    .line 45
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;->onFinalResult(Z)V

    goto :goto_0

    .line 49
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;->curCommand:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/command/a;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0
.end method


# virtual methods
.method public addNext(Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;->_CommandArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 21
    return-void
.end method

.method public cancel()V
    .locals 0

    .prologue
    .line 87
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->cancel()V

    .line 88
    return-void
.end method

.method public impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;->createLinkedList()V

    .line 25
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;->executeCommandDequeingFromList()V

    .line 26
    return-void
.end method

.method protected onCancel()V
    .locals 1

    .prologue
    .line 71
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->onCancel()V

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;->_FinalResult:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;->curCommand:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/command/CommandList;->curCommand:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->cancel()V

    .line 83
    :cond_0
    return-void
.end method
