.class public Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/UpdateAppEvent;
.super Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;
.source "ProGuard"


# instance fields
.field mCount:I

.field mUpdateAppEventType:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/UpdateAppEvent$UpdateAppEventType;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/UpdateAppEvent$UpdateAppEventType;I)V
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->UpdateAppEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;)V

    .line 12
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/UpdateAppEvent;->mUpdateAppEventType:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/UpdateAppEvent$UpdateAppEventType;

    .line 13
    iput p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/UpdateAppEvent;->mCount:I

    .line 14
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/UpdateAppEvent;->mCount:I

    return v0
.end method

.method public getUpdateAppEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/UpdateAppEvent$UpdateAppEventType;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/UpdateAppEvent;->mUpdateAppEventType:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/UpdateAppEvent$UpdateAppEventType;

    return-object v0
.end method
