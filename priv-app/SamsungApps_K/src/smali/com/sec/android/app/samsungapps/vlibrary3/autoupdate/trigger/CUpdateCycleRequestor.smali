.class public Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CUpdateCycleRequestor;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateCycleRequestor;


# static fields
.field public static final AUTOUPDATE_CYCLE_FIELD:Ljava/lang/String; = "normalUpdateCycle"

.field public static final PRELOAD_CYCLE_FIELD:Ljava/lang/String; = "emergencyUpdateCycle"


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CUpdateCycleRequestor;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;

    .line 19
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CUpdateCycleRequestor;->b:Ljava/lang/String;

    .line 20
    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CUpdateCycleRequestor;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;->findString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 77
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    .line 79
    :try_start_0
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 80
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;-><init>()V

    .line 81
    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;->setHours(I)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 88
    :goto_1
    return-object v0

    .line 77
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 83
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    move-object v0, v1

    .line 85
    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 88
    goto :goto_1
.end method

.method private static a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateCycleRequestor$IUpdateCycleReceiverResult;)V
    .locals 0

    .prologue
    .line 93
    if-eqz p0, :cond_0

    .line 95
    invoke-interface {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateCycleRequestor$IUpdateCycleReceiverResult;->onRecieveFailed()V

    .line 97
    :cond_0
    return-void
.end method


# virtual methods
.method protected onReceiveFailed(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateCycleRequestor$IUpdateCycleReceiverResult;)V
    .locals 0

    .prologue
    .line 60
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CUpdateCycleRequestor;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateCycleRequestor$IUpdateCycleReceiverResult;)V

    .line 61
    return-void
.end method

.method protected onReceiveResultSuccess(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateCycleRequestor$IUpdateCycleReceiverResult;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CUpdateCycleRequestor;->a(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;

    move-result-object v0

    .line 47
    if-eqz v0, :cond_1

    .line 49
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CUpdateCycleRequestor;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;

    .line 50
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CUpdateCycleRequestor;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;

    invoke-interface {p2, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateCycleRequestor$IUpdateCycleReceiverResult;->onReceivedInterval(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;)V

    .line 56
    :cond_0
    :goto_0
    return-void

    .line 54
    :cond_1
    invoke-static {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CUpdateCycleRequestor;->a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateCycleRequestor$IUpdateCycleReceiverResult;)V

    goto :goto_0
.end method

.method public requestInterval(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateCycleRequestor$IUpdateCycleReceiverResult;)V
    .locals 3

    .prologue
    .line 25
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;-><init>()V

    .line 26
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/e;

    invoke-direct {v2, p0, v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/e;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CUpdateCycleRequestor;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateCycleRequestor$IUpdateCycleReceiverResult;)V

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->checkUpdateCycle(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 40
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 41
    return-void
.end method
