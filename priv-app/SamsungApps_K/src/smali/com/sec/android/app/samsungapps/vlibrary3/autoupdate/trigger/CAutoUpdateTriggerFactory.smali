.class public Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CAutoUpdateTriggerFactory;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerFactory;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CAutoUpdateTriggerFactory;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    .line 15
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CAutoUpdateTriggerFactory;)Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;
    .locals 1

    .prologue
    .line 8
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CAutoUpdateTriggerFactory;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    return-object v0
.end method


# virtual methods
.method public createAutoUpdateChecker(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateTriggerChecker$IAutoUpdateTriggerManagerObserver;)Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateTriggerChecker;
    .locals 4

    .prologue
    .line 20
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CAutoUpdateCheckConfig;

    invoke-direct {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CAutoUpdateCheckConfig;-><init>(Landroid/content/Context;)V

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CUpdateCycleRequestor;

    const-string v3, "normalUpdateCycle"

    invoke-direct {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CUpdateCycleRequestor;-><init>(Ljava/lang/String;)V

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/c;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CAutoUpdateTriggerFactory;)V

    invoke-direct {v0, v1, p2, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateCheckConfig;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateTriggerChecker$IAutoUpdateTriggerManagerObserver;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateCycleRequestor;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateFakeInterval;)V

    .line 45
    return-object v0
.end method

.method public createPreloadAutoUpdateChecker(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateTriggerChecker$IAutoUpdateTriggerManagerObserver;)Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateTriggerChecker;
    .locals 4

    .prologue
    .line 51
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CPreloadAutoUpdateCheckConfig;

    invoke-direct {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CPreloadAutoUpdateCheckConfig;-><init>(Landroid/content/Context;)V

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CUpdateCycleRequestor;

    const-string v3, "emergencyUpdateCycle"

    invoke-direct {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CUpdateCycleRequestor;-><init>(Ljava/lang/String;)V

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/d;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CAutoUpdateTriggerFactory;)V

    invoke-direct {v0, v1, p2, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerManager;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateCheckConfig;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateTriggerChecker$IAutoUpdateTriggerManagerObserver;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateCycleRequestor;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/IAutoUpdateFakeInterval;)V

    .line 76
    return-object v0
.end method
