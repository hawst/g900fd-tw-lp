.class public Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field public static final OK_LEFT:I = 0x1

.field public static final OK_RIGHT:I = 0x2

.field private static a:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->a:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static _getAlertDialogButtonOrder(Landroid/content/Context;)I
    .locals 9

    .prologue
    const/4 v1, 0x2

    const/4 v0, 0x1

    .line 36
    :try_start_0
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-ge v2, v3, :cond_1

    .line 76
    :cond_0
    :goto_0
    return v0

    .line 41
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 43
    const-string v2, "tw_alert_dialog_holo"

    const-string v4, "layout"

    const-string v5, "android"

    invoke-virtual {v3, v2, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 44
    if-nez v2, :cond_2

    .line 46
    const-string v2, "alert_dialog_holo"

    const-string v4, "layout"

    const-string v5, "android"

    invoke-virtual {v3, v2, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 49
    :cond_2
    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getLayout(I)Landroid/content/res/XmlResourceParser;

    move-result-object v4

    .line 50
    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v2

    .line 51
    :goto_1
    if-eq v2, v0, :cond_5

    .line 53
    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->getAttributeCount()I

    move-result v5

    .line 54
    const/4 v2, 0x0

    :goto_2
    if-ge v2, v5, :cond_4

    .line 56
    const-string v6, "id"

    invoke-interface {v4, v2}, Landroid/content/res/XmlResourceParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 58
    invoke-interface {v4, v2}, Landroid/content/res/XmlResourceParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, "@"

    const-string v8, ""

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 59
    const-string v7, "android:id/button1"

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 63
    const-string v7, "android:id/button3"

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    move v0, v1

    .line 65
    goto :goto_0

    .line 54
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 69
    :cond_4
    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->next()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_1

    :catch_0
    move-exception v0

    :cond_5
    move v0, v1

    .line 76
    goto :goto_0
.end method

.method public static getAlertDialogButtonOrder(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 21
    sget v0, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->a:I

    if-nez v0, :cond_0

    .line 22
    const-class v1, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;

    monitor-enter v1

    .line 23
    :try_start_0
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->_getAlertDialogButtonOrder(Landroid/content/Context;)I

    move-result v0

    sput v0, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->a:I

    .line 24
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 26
    :cond_0
    sget v0, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->a:I

    return v0

    .line 24
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getNegativeButton(Landroid/app/Activity;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 130
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getNegativeButtonID(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 131
    return-object v0
.end method

.method public static getNegativeButtonID(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 90
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getAlertDialogButtonOrder(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 92
    const v0, 0x7f0c0147

    .line 94
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0c0141

    goto :goto_0
.end method

.method public static getPositiveButton(Landroid/app/Activity;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 124
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getPositiveButtonID(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 125
    return-object v0
.end method

.method public static getPositiveButtonID(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 81
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getAlertDialogButtonOrder(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 83
    const v0, 0x7f0c0145

    .line 85
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0c0143

    goto :goto_0
.end method

.method public static showPositiveButton(Landroid/app/Activity;I)V
    .locals 1

    .prologue
    .line 141
    invoke-virtual {p0, p1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->showPositiveButton(Landroid/app/Activity;Ljava/lang/String;)V

    .line 142
    return-void
.end method

.method public static showPositiveButton(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 135
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getPositiveButtonID(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 136
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 137
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 138
    return-void
.end method

.method public static showPositiveNegativeButton(Landroid/app/Activity;II)V
    .locals 2

    .prologue
    .line 119
    invoke-virtual {p0, p1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->showPositiveNegativeButton(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    return-void
.end method

.method public static showPositiveNegativeButton(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const v5, 0x7f0c0144

    const v4, 0x7f0c0140

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 99
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getAlertDialogButtonOrder(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 101
    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 102
    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 109
    :goto_0
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getPositiveButtonID(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 110
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getNegativeButtonID(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 111
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 112
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 113
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 114
    invoke-virtual {v1, p2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 115
    return-void

    .line 106
    :cond_0
    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 107
    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
