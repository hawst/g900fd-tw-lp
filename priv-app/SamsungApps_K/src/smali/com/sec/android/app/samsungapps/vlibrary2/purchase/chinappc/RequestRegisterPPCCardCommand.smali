.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _CardBalance:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PrepaidCardBalance;

.field private _IChinaPPCCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;

.field private _ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

.field private _PrepaidCardList:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCardInfo;

.field private _View:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand$IPPCRegisterView;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PrepaidCardBalance;Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;)V
    .locals 2

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;->_IChinaPPCCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;

    .line 24
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;->_CardBalance:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PrepaidCardBalance;

    .line 25
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    .line 26
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCardInfo;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;->getPurchaseInfo()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCardInfo;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;->_PrepaidCardList:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCardInfo;

    .line 27
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;Z)V
    .locals 0

    .prologue
    .line 14
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand$IPPCRegisterView;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;->_View:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand$IPPCRegisterView;

    return-object v0
.end method


# virtual methods
.method public getCardOperatorInfo()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IPPCCardOperatorInfo;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;->_PrepaidCardList:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCardInfo;

    return-object v0
.end method

.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 0

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;->invokeView()V

    .line 32
    return-void
.end method

.method public invokeCompleted(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand$IPPCRegisterView;)V
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;->_View:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand$IPPCRegisterView;

    .line 37
    return-void
.end method

.method protected invokeView()V
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;->_IChinaPPCCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;->getRegisterPPCViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;->_Context:Landroid/content/Context;

    invoke-interface {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;->invoke(Landroid/content/Context;Ljava/lang/Object;)V

    .line 65
    return-void
.end method

.method public register(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCRegisterParam;)V
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->startLoading()V

    .line 42
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/g;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/g;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;)V

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;->requestRegisterPrepaidCard(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCRegisterParam;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    .line 54
    return-void
.end method

.method protected requestRegisterPrepaidCard(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCRegisterParam;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V
    .locals 2

    .prologue
    .line 58
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;->_CardBalance:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PrepaidCardBalance;

    invoke-virtual {v0, p1, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->registerPrepaidCard(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCRegisterParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 59
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 60
    return-void
.end method
