.class final Lcom/sec/android/app/samsungapps/u;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/CommonActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/CommonActivity;)V
    .locals 0

    .prologue
    .line 636
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/u;->a:Lcom/sec/android/app/samsungapps/CommonActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCommandResult(Z)V
    .locals 3

    .prologue
    .line 639
    if-eqz p1, :cond_0

    .line 640
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 641
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/u;->a:Lcom/sec/android/app/samsungapps/CommonActivity;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/u;->a:Lcom/sec/android/app/samsungapps/CommonActivity;

    const v2, 0x7f08012d

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/uiutil/ToastUtil;->toastMessage(Landroid/content/Context;Ljava/lang/String;)Landroid/widget/Toast;

    .line 645
    :goto_0
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    const-class v2, Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 647
    const-string v1, "buttonType"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 648
    const/high16 v1, 0x20020000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 649
    sget-object v1, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    invoke-static {v1, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->commonStartActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 651
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/u;->a:Lcom/sec/android/app/samsungapps/CommonActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->a(Lcom/sec/android/app/samsungapps/CommonActivity;)Z

    .line 652
    return-void

    .line 643
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/u;->a:Lcom/sec/android/app/samsungapps/CommonActivity;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/u;->a:Lcom/sec/android/app/samsungapps/CommonActivity;

    const v2, 0x7f0801d5

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/uiutil/ToastUtil;->toastMessage(Landroid/content/Context;Ljava/lang/String;)Landroid/widget/Toast;

    goto :goto_0
.end method
