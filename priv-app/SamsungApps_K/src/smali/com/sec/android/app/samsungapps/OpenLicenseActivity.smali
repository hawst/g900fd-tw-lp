.class public Lcom/sec/android/app/samsungapps/OpenLicenseActivity;
.super Lcom/sec/android/app/samsungapps/ActionBarActivity;
.source "ProGuard"


# instance fields
.field private a:Landroid/webkit/WebView;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;-><init>()V

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/OpenLicenseActivity;->a:Landroid/webkit/WebView;

    .line 14
    const-string v0, "open_license.html"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/OpenLicenseActivity;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 62
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x6f

    if-ne v0, v1, :cond_0

    .line 63
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/OpenLicenseActivity;->onBackPressed()V

    .line 65
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 21
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 23
    const v0, 0x7f0400db

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/OpenLicenseActivity;->setMainView(I)V

    .line 25
    const v0, 0x7f0801c1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/OpenLicenseActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v4, v6, [I

    const v5, 0xb0001

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/OpenLicenseActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    .line 27
    const v0, 0x7f0c02ea

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/OpenLicenseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/OpenLicenseActivity;->a:Landroid/webkit/WebView;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/OpenLicenseActivity;->a:Landroid/webkit/WebView;

    if-nez v0, :cond_0

    const-string v0, "OpenLicenseActivity::View is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 28
    :goto_0
    return-void

    .line 27
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/OpenLicenseActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    invoke-virtual {v0, v6}, Landroid/webkit/WebSettings;->setSavePassword(Z)V

    invoke-virtual {v0, v6}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/OpenLicenseActivity;->a:Landroid/webkit/WebView;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "file:///android_asset/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/OpenLicenseActivity;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/OpenLicenseActivity;->a:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/OpenLicenseActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->clearHistory()V

    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/OpenLicenseActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->clearView()V

    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/OpenLicenseActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/OpenLicenseActivity;->a:Landroid/webkit/WebView;

    .line 41
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onDestroy()V

    .line 42
    return-void
.end method
