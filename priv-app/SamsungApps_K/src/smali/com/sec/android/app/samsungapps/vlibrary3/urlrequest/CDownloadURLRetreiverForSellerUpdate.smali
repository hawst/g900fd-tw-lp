.class public Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetreiverForSellerUpdate;
.super Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;
.source "ProGuard"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    .line 12
    return-void
.end method


# virtual methods
.method public onAction(Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Action;)V
    .locals 2

    .prologue
    .line 16
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/b;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Action;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 43
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetriever;->onAction(Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Action;)V

    .line 44
    :cond_1
    :goto_1
    return-void

    .line 19
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetreiverForSellerUpdate;->_IURLRequestorReceiver:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieveResult;

    if-eqz v0, :cond_1

    .line 21
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetreiverForSellerUpdate;->_IURLRequestorReceiver:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieveResult;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieveResult;->onURLFailed()V

    goto :goto_1

    .line 25
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetreiverForSellerUpdate;->_IURLRequestorReceiver:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieveResult;

    if-eqz v0, :cond_1

    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetreiverForSellerUpdate;->_IURLRequestorReceiver:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieveResult;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieveResult;->onURLFailed()V

    goto :goto_1

    .line 31
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetreiverForSellerUpdate;->_IURLRequestorReceiver:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieveResult;

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetreiverForSellerUpdate;->_IURLRequestorReceiver:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieveResult;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieveResult;->onURLFailed()V

    goto :goto_0

    .line 37
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetreiverForSellerUpdate;->_IURLRequestorReceiver:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieveResult;

    if-eqz v0, :cond_0

    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetreiverForSellerUpdate;->_IURLRequestorReceiver:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieveResult;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieveResult;->onURLSucceed()V

    goto :goto_0

    .line 16
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public bridge synthetic onAction(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Action;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetreiverForSellerUpdate;->onAction(Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Action;)V

    return-void
.end method
