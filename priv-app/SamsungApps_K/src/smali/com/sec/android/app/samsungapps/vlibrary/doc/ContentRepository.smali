.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentRepository;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field contentlist:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentRepository;->contentlist:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public add(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;)V
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentRepository;->contentlist:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 11
    return-void
.end method

.method public findByID(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;
    .locals 3

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentRepository;->contentlist:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 17
    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->compareID(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 21
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
