.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopup;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract askChinaDataWarningForBackgrounUpdate(Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupResponse;)V
.end method

.method public abstract askDeleteCreditCard(Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupResponse;)V
.end method

.method public abstract askEmergencyUpdatePopup(Ljava/util/ArrayList;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopup$INotiResponseOkCancel;)V
.end method

.method public abstract askRegistrationOfCreditCardIsRequiredToPayTax(Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupResponse;)V
.end method

.method public abstract registerEmergencyUpdateNotification(I)V
.end method

.method public abstract showAccountDisabled(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupConfirmResponse;)V
.end method

.method public abstract showAlreadyPurchasedContent(Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupResponse;)V
.end method

.method public abstract showGeoIPFailed(Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopup$INotiResponseOkCancel;)V
.end method

.method public abstract showIncompatibleOS(Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupConfirmResponse;)V
.end method
