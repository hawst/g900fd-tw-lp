.class final Lcom/sec/android/app/samsungapps/tobelog/f;
.super Ljava/lang/Thread;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder$ServiceConnectionListener;


# instance fields
.field private a:Z

.field private b:Lcom/sec/android/app/samsungapps/tobelog/e;

.field private c:Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;

.field private d:Ljava/lang/Thread;

.field private e:Landroid/content/Context;

.field private f:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/tobelog/e;ZZ)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/f;->d:Ljava/lang/Thread;

    .line 24
    iput-boolean p3, p0, Lcom/sec/android/app/samsungapps/tobelog/f;->a:Z

    .line 25
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/tobelog/f;->e:Landroid/content/Context;

    .line 26
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/tobelog/f;->b:Lcom/sec/android/app/samsungapps/tobelog/e;

    .line 27
    invoke-virtual {p0, p4}, Lcom/sec/android/app/samsungapps/tobelog/f;->setDaemon(Z)V

    .line 29
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;-><init>(Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder$ServiceConnectionListener;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/f;->c:Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;

    .line 30
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 163
    monitor-enter p0

    .line 164
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 165
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 79
    iput-wide p1, p0, Lcom/sec/android/app/samsungapps/tobelog/f;->f:J

    .line 80
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/f;->d:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    .line 173
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/tobelog/f;->a:Z

    .line 175
    monitor-enter p0

    .line 176
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 177
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 179
    const-string v0, "Stop send log"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V

    .line 182
    :cond_0
    return-void

    .line 177
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final onServiceConnected()V
    .locals 14

    .prologue
    const/4 v13, -0x4

    const/4 v12, -0x6

    .line 196
    const-string v0, "Send All log"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V

    new-instance v11, Ljava/util/LinkedList;

    invoke-direct {v11}, Ljava/util/LinkedList;-><init>()V

    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/f;->b:Lcom/sec/android/app/samsungapps/tobelog/e;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/tobelog/e;->size()I

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/sec/android/app/samsungapps/tobelog/h;->a()Lcom/sec/android/app/samsungapps/tobelog/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/tobelog/h;->c()Z

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/f;->b:Lcom/sec/android/app/samsungapps/tobelog/e;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/tobelog/e;->poll()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/sec/android/app/samsungapps/tobelog/LogData;

    const-string v0, "request send log"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V

    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/tobelog/f;->f:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/tobelog/f;->f:J

    invoke-virtual {v10}, Lcom/sec/android/app/samsungapps/tobelog/LogData;->getTimeStamp()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    invoke-virtual {v11, v10}, Ljava/util/LinkedList;->offer(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    :try_start_0
    invoke-virtual {v10}, Lcom/sec/android/app/samsungapps/tobelog/LogData;->getBodyString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/f;->c:Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;

    invoke-virtual {v10}, Lcom/sec/android/app/samsungapps/tobelog/LogData;->getLogType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10}, Lcom/sec/android/app/samsungapps/tobelog/LogData;->getSvcCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10}, Lcom/sec/android/app/samsungapps/tobelog/LogData;->getTimeStamp()J

    move-result-wide v3

    invoke-virtual {v10}, Lcom/sec/android/app/samsungapps/tobelog/LogData;->getActivity()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v10}, Lcom/sec/android/app/samsungapps/tobelog/LogData;->getUserId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v10}, Lcom/sec/android/app/samsungapps/tobelog/LogData;->getSpecVer()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {v0 .. v9}, Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;->sendLog(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x3

    if-eq v0, v1, :cond_1

    const/4 v1, -0x2

    if-eq v0, v1, :cond_1

    if-eq v0, v13, :cond_1

    if-ne v0, v12, :cond_5

    :cond_1
    if-ne v0, v13, :cond_2

    const-string v1, "ToBeLog"

    const-string v2, "TOKEN IS NOT MATCHED WITH SIGNED KEY."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x9

    if-lt v1, v2, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/f;->b:Lcom/sec/android/app/samsungapps/tobelog/e;

    invoke-virtual {v1, v10}, Lcom/sec/android/app/samsungapps/tobelog/e;->offerFirst(Ljava/lang/Object;)Z

    :goto_1
    if-ne v0, v12, :cond_3

    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/Preferences;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/f;->e:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/Preferences;-><init>(Landroid/content/Context;)V

    const-string v1, "registered"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/tobelog/Preferences;->putBoolean(Ljava/lang/String;Z)V

    :cond_3
    invoke-static {}, Lcom/sec/android/app/samsungapps/tobelog/h;->a()Lcom/sec/android/app/samsungapps/tobelog/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/tobelog/h;->d()V

    :goto_2
    invoke-virtual {v11}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/tobelog/LogData;

    if-eqz v0, :cond_6

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/f;->b:Lcom/sec/android/app/samsungapps/tobelog/e;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/tobelog/e;->offer(Ljava/lang/Object;)Z

    goto :goto_2

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Exception on LogSender.sendAll() "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/f;->b:Lcom/sec/android/app/samsungapps/tobelog/e;

    invoke-virtual {v1, v10}, Lcom/sec/android/app/samsungapps/tobelog/e;->offer(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    invoke-static {}, Lcom/sec/android/app/samsungapps/tobelog/h;->a()Lcom/sec/android/app/samsungapps/tobelog/h;

    move-result-object v0

    invoke-virtual {v10}, Lcom/sec/android/app/samsungapps/tobelog/LogData;->getDataSize()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/h;->a(I)V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/f;->c:Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/f;->e:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;->unbindService(Landroid/content/Context;)V

    .line 197
    return-void
.end method

.method public final onServiceDisconnected()V
    .locals 0

    .prologue
    .line 203
    return-void
.end method

.method public final run()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 35
    const-string v0, "LogSender thread has been started"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V

    .line 37
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/f;->d:Ljava/lang/Thread;

    .line 41
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 45
    :cond_0
    :try_start_1
    const-string v0, "LogSender thread is running"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/f;->b:Lcom/sec/android/app/samsungapps/tobelog/e;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/tobelog/e;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 48
    const-string v0, "mLogQueue size is zero"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V

    .line 54
    :goto_0
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/tobelog/f;->a:Z

    if-ne v0, v2, :cond_1

    .line 55
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V

    .line 58
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/tobelog/f;->a:Z

    if-eq v0, v2, :cond_0

    .line 60
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 66
    :goto_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/f;->d:Ljava/lang/Thread;

    .line 68
    const-string v0, "LogSender thread has been stopped"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V

    .line 70
    return-void

    .line 50
    :cond_2
    :try_start_2
    const-string v0, "Call bind service"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/f;->c:Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/f;->e:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;->bindService(Landroid/content/Context;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 60
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit p0

    throw v0
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0

    .line 64
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1
.end method
