.class public final enum Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;

.field public static final enum SAMSUNG:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;

.field public static final enum SINA:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;

.field public static final enum TENCENT:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 294
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;

    const-string v1, "SINA"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;->SINA:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;

    .line 295
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;

    const-string v1, "TENCENT"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;->TENCENT:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;

    .line 296
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;

    const-string v1, "SAMSUNG"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;->SAMSUNG:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;

    .line 292
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;->SINA:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;->TENCENT:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;->SAMSUNG:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 292
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;
    .locals 1

    .prologue
    .line 292
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;
    .locals 1

    .prologue
    .line 292
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$PoweredBy;

    return-object v0
.end method
