.class final Lcom/sec/android/app/samsungapps/fi;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/NoticeListViewActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/NoticeListViewActivity;)V
    .locals 0

    .prologue
    .line 143
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/fi;->a:Lcom/sec/android/app/samsungapps/NoticeListViewActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/fi;->a:Lcom/sec/android/app/samsungapps/NoticeListViewActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->a(Lcom/sec/android/app/samsungapps/NoticeListViewActivity;)Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/fi;->a:Lcom/sec/android/app/samsungapps/NoticeListViewActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->a(Lcom/sec/android/app/samsungapps/NoticeListViewActivity;)Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 156
    sub-int v0, p4, p3

    .line 157
    if-lt p2, v0, :cond_0

    if-eqz p4, :cond_0

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/fi;->a:Lcom/sec/android/app/samsungapps/NoticeListViewActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->b(Lcom/sec/android/app/samsungapps/NoticeListViewActivity;)Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->isCompleted()Z

    move-result v0

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/fi;->a:Lcom/sec/android/app/samsungapps/NoticeListViewActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->b(Lcom/sec/android/app/samsungapps/NoticeListViewActivity;)Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->isLoading()Z

    move-result v0

    if-ne v0, v1, :cond_1

    .line 168
    :cond_0
    :goto_0
    return-void

    .line 161
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/fi;->a:Lcom/sec/android/app/samsungapps/NoticeListViewActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->b(Lcom/sec/android/app/samsungapps/NoticeListViewActivity;)Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->isCompleted()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/fi;->a:Lcom/sec/android/app/samsungapps/NoticeListViewActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->c(Lcom/sec/android/app/samsungapps/NoticeListViewActivity;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/fi;->a:Lcom/sec/android/app/samsungapps/NoticeListViewActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->b(Lcom/sec/android/app/samsungapps/NoticeListViewActivity;)Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->getTotalCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/fi;->a:Lcom/sec/android/app/samsungapps/NoticeListViewActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->d(Lcom/sec/android/app/samsungapps/NoticeListViewActivity;)V

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/fi;->a:Lcom/sec/android/app/samsungapps/NoticeListViewActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->e(Lcom/sec/android/app/samsungapps/NoticeListViewActivity;)V

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/fi;->a:Lcom/sec/android/app/samsungapps/NoticeListViewActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->f(Lcom/sec/android/app/samsungapps/NoticeListViewActivity;)V

    goto :goto_0
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 149
    return-void
.end method
