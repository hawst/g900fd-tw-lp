.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListRequestXML;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;
.source "ProGuard"


# direct methods
.method protected constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 8

    .prologue
    const/4 v6, 0x1

    .line 11
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 12
    return-void
.end method

.method protected static purchaseList(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListRequestXML;
    .locals 6

    .prologue
    .line 17
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListRequestXML;

    const-string v3, "purchaseList"

    const-string v4, "2140"

    move-object v1, p0

    move-object v2, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;Ljava/lang/String;Ljava/lang/String;I)V

    .line 19
    invoke-virtual {v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListRequestXML;->addListStEdAsParam(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;)V

    .line 20
    const-string v1, "imgWidth"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getWidth()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    const-string v1, "imgHeight"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getHeight()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    const-string v1, "imei"

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getIMEI()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 23
    const-string v1, "contentType"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    const-string v1, "alignOrder"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getSortOrder()Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    return-object v0
.end method

.method public static purchaseListEx(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;ILjava/lang/String;ILjava/lang/String;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListRequestXML;
    .locals 6

    .prologue
    .line 31
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListRequestXML;

    const-string v3, "purchaseListExMulti"

    const-string v4, "2312"

    move-object v1, p0

    move-object v2, p2

    move v5, p7

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;Ljava/lang/String;Ljava/lang/String;I)V

    .line 33
    invoke-virtual {v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListRequestXML;->addListStEdAsParam(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;)V

    .line 34
    const-string v1, "imgWidth"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getWidth()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    const-string v1, "imgHeight"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getHeight()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    const-string v1, "imei"

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getIMEI()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 37
    const-string v1, "contentType"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    const-string v1, "alignOrder"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    const-string v1, "preloadCount"

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    const-string v1, "postloadCount"

    invoke-static {p5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    const-string v1, "preloadApp"

    invoke-virtual {v0, v1, p4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    const-string v1, "postloadApp"

    invoke-virtual {v0, v1, p6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    const-string v1, "filter"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isTestMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 48
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListRequestXML;->addPredeployed()V

    .line 51
    :cond_0
    return-object v0
.end method

.method public static upgradeListEx(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;ILjava/lang/String;ILjava/lang/String;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListRequestXML;
    .locals 2

    .prologue
    .line 56
    invoke-static/range {p0 .. p7}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListRequestXML;->purchaseListEx(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;ILjava/lang/String;ILjava/lang/String;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListRequestXML;

    move-result-object v0

    .line 57
    const-string v1, "2302"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListRequestXML;->setReqID(Ljava/lang/String;)V

    .line 58
    const-string v1, "upgradeListEx"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseListRequestXML;->setReqName(Ljava/lang/String;)V

    .line 59
    return-object v0
.end method
