.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract checkCardBalance()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract complete()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract createPayment()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract getCardBalance()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PrepaidCardBalance;
.end method

.method public abstract getConfirmViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
.end method

.method public abstract getExtraObject()Ljava/lang/Object;
.end method

.method public abstract getFinalMap()Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;
.end method

.method public abstract getPurchaseInfo()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;
.end method

.method public abstract getRegisterPPCViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
.end method

.method public abstract isCardBalanceEnough()Z
.end method

.method public abstract requestRegisterCard()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method
