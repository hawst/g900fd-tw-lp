.class public Lcom/sec/android/app/samsungapps/vlibrary2/util/ElapsedTimePrinter;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private _Label:Ljava/lang/String;

.field private _bPrint:Z

.field private dwStartTime:J


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/util/ElapsedTimePrinter;->_bPrint:Z

    .line 11
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/util/ElapsedTimePrinter;->_Label:Ljava/lang/String;

    .line 12
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/util/ElapsedTimePrinter;->dwStartTime:J

    .line 13
    return-void
.end method


# virtual methods
.method public off()V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/util/ElapsedTimePrinter;->_bPrint:Z

    .line 38
    return-void
.end method

.method public p(I)V
    .locals 4

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/util/ElapsedTimePrinter;->_bPrint:Z

    if-nez v0, :cond_0

    .line 33
    :goto_0
    return-void

    .line 30
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/util/ElapsedTimePrinter;->dwStartTime:J

    sub-long/2addr v0, v2

    long-to-double v0, v0

    .line 31
    invoke-static {v0, v1}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v0

    .line 32
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/util/ElapsedTimePrinter;->_Label:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public p(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 17
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/util/ElapsedTimePrinter;->_bPrint:Z

    if-nez v0, :cond_0

    .line 23
    :goto_0
    return-void

    .line 20
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/util/ElapsedTimePrinter;->dwStartTime:J

    sub-long/2addr v0, v2

    long-to-double v0, v0

    .line 21
    invoke-static {v0, v1}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v0

    .line 22
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/util/ElapsedTimePrinter;->_Label:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
