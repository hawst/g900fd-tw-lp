.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentMethodCheckInfo;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract isAlipayPurchaseSupported()Z
.end method

.method public abstract isCreditCardPurchaseAvailable()Z
.end method

.method public abstract isCyberCashSupported()Z
.end method

.method public abstract isDirectBillingPurchaseAvailable()Z
.end method

.method public abstract isPPCSupported()Z
.end method

.method public abstract isPSMSPurchaseAvailable()Z
.end method

.method public abstract isPhoneBillPurchaseAvailable()Z
.end method

.method public abstract isTestPSMSPurchaseSupported()Z
.end method
