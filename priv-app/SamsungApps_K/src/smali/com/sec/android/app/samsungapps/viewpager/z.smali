.class final Lcom/sec/android/app/samsungapps/viewpager/z;
.super Landroid/os/Handler;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;)V
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/viewpager/z;->a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 50
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 60
    :cond_0
    :goto_0
    return-void

    .line 53
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/z;->a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->isFakeDragging()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/z;->a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/z;->a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/z;->a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->beginFakeDrag()Z

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/z;->a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->fakeDragBy(F)V

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/z;->a:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsViewPager;->endFakeDrag()V

    goto :goto_0

    .line 50
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
