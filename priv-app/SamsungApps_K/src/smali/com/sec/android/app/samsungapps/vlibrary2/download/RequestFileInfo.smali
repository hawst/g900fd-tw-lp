.class public Lcom/sec/android/app/samsungapps/vlibrary2/download/RequestFileInfo;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/download/IRequestFileInfo;


# instance fields
.field private _Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

.field downloadSize:J

.field result:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseResultMapWrapper;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V
    .locals 2

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/RequestFileInfo;->downloadSize:J

    .line 16
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/RequestFileInfo;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 17
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseResultMapWrapper;

    invoke-direct {v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseResultMapWrapper;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/RequestFileInfo;->result:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseResultMapWrapper;

    .line 18
    return-void
.end method


# virtual methods
.method public downloadEnded(Z)V
    .locals 0

    .prologue
    .line 33
    return-void
.end method

.method public getDownloadURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/RequestFileInfo;->result:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseResultMapWrapper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseResultMapWrapper;->downloadUri()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRealContentSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;
    .locals 4

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/RequestFileInfo;->result:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseResultMapWrapper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseResultMapWrapper;->contentsSize()Ljava/lang/String;

    move-result-object v0

    .line 38
    if-nez v0, :cond_0

    .line 40
    const-string v0, "error"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->err(Ljava/lang/String;)V

    .line 41
    const-string v0, "0"

    .line 43
    :cond_0
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    int-to-long v2, v0

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;-><init>(J)V

    return-object v1
.end method

.method public getSaveFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/RequestFileInfo;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getSaveFileName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public updateDownloadedSize(J)V
    .locals 0

    .prologue
    .line 27
    iput-wide p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/RequestFileInfo;->downloadSize:J

    .line 28
    return-void
.end method
