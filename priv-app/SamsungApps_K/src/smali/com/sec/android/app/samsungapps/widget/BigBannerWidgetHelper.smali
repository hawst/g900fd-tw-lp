.class public Lcom/sec/android/app/samsungapps/widget/BigBannerWidgetHelper;
.super Lcom/sec/android/app/samsungapps/widget/BannerWidgetHelper;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetData;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/BannerWidgetHelper;-><init>(Landroid/content/Context;)V

    .line 28
    return-void
.end method

.method private a(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;
    .locals 2

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/BigBannerWidgetHelper;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/BigBannerWidgetHelper;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;->size()I

    move-result v0

    if-gt v0, p1, :cond_2

    .line 36
    :cond_0
    const-string v0, "BigBannerWidgetHelper::_getBanner::Banner is empty or less"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 37
    const/4 v0, 0x0

    .line 47
    :cond_1
    :goto_0
    return-object v0

    .line 40
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/BigBannerWidgetHelper;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;

    .line 41
    if-nez v0, :cond_1

    .line 43
    const-string v1, "BigBannerWidgetHelper::_getBanner::banner is null"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public getContent(II)Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 135
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/BigBannerWidgetHelper;->a(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;

    move-result-object v1

    .line 138
    if-nez v1, :cond_0

    .line 140
    const-string v1, "BigBannerWidgetHelper::getContent::banner is null"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 153
    :goto_0
    return-object v0

    .line 144
    :cond_0
    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->getLinkedContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v1

    .line 145
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->size()I

    move-result v2

    if-gt v2, p2, :cond_2

    .line 147
    :cond_1
    const-string v1, "BigBannerWidgetHelper::getContent::contentList is empty or less"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 151
    :cond_2
    invoke-virtual {v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    goto :goto_0
.end method

.method public getContentCount(I)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 105
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/BigBannerWidgetHelper;->a(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;

    move-result-object v1

    .line 108
    if-nez v1, :cond_0

    .line 110
    const-string v1, "BigBannerWidgetHelper::getContentCount::banner is null"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 123
    :goto_0
    return v0

    .line 114
    :cond_0
    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->getLinkedContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v1

    .line 115
    if-nez v1, :cond_1

    .line 117
    const-string v1, "BigBannerWidgetHelper::getContentCount::contentList is null"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 121
    :cond_1
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public hasProductSet(I)Z
    .locals 2

    .prologue
    .line 58
    const/4 v0, 0x0

    .line 60
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/BigBannerWidgetHelper;->a(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;

    move-result-object v1

    .line 61
    if-eqz v1, :cond_0

    .line 63
    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->isInteractionBanner()Z

    move-result v0

    .line 66
    :cond_0
    return v0
.end method

.method public sendRequestProductSet(ILcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V
    .locals 2

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/BigBannerWidgetHelper;->a(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;

    move-result-object v0

    .line 80
    if-nez v0, :cond_0

    .line 82
    const-string v0, "BigBannerWidgetHelper::sendRequestProductSet::banner is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 95
    :goto_0
    return-void

    .line 86
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/BigBannerWidgetHelper;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v1

    .line 87
    if-nez v1, :cond_1

    .line 89
    const-string v0, "BigBannerWidgetHelper::sendRequestProductSet::builder is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 93
    :cond_1
    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;

    invoke-virtual {v1, v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->getProductSetList(Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 94
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/BigBannerWidgetHelper;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    goto :goto_0
.end method
