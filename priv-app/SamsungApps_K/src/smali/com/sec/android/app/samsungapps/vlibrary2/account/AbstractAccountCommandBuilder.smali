.class public abstract Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/account/AutoLoginCommand$IAutoLoginCommandData;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/account/BackgroundLoginCommand$IBackgroundLoginCommandData;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin$ITokenBasedLoginData;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand$ILoginCommandData;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand$ILoginODCWithTokenCommandData;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand$ILogoutCommandData;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand$IPasswordConfirmCommandData;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$IRequestTokenCommandData;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand$ITokenBasedConfirmPasswordCommandData;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/account/ValidateConfirmPassword$IValidateConfirmPasswordData;


# static fields
.field private static _PasswordConfirmInfo:Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmInfo;

.field public static _Token:Ljava/lang/String;


# instance fields
.field protected _Context:Landroid/content/Context;

.field private _CreditCardStateContext:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;

.field _ICardInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/ICardInfo;

.field private _ILoadingDialogCreator:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;

.field private _TokenURL:Ljava/lang/String;

.field private _bHideNotificationAccountInstalled:Z

.field private _bNewInterfaceAccountInstalled:Z

.field private _bNewSamsungAccountInstalled:Z

.field private _bPopupConfirmPWAccountInstalled:Z

.field private _bSamsungAccountDisabled:Z

.field private _bSamsungAccountInstalled:Z

.field private _bTokenExpired:Z

.field protected bAutoLogin:Z

.field private mIGetDownloadListCheckerFactory:Lcom/sec/android/app/samsungapps/vlibrary2/update/IGetDownloadListCheckerFactory;

.field private mNotiPopupFactory:Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmInfo;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmInfo;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->_PasswordConfirmInfo:Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmInfo;

    .line 84
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->_Token:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;Lcom/sec/android/app/samsungapps/vlibrary2/update/IGetDownloadListCheckerFactory;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/a;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->_ICardInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/ICardInfo;

    .line 85
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->_TokenURL:Ljava/lang/String;

    .line 198
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->bAutoLogin:Z

    .line 41
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->_Context:Landroid/content/Context;

    .line 42
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->_CreditCardStateContext:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;

    .line 43
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isExistSamsungAccount(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->_bSamsungAccountInstalled:Z

    .line 44
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->_bSamsungAccountInstalled:Z

    if-eqz v0, :cond_0

    .line 46
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isDisabledSamsungAccount(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->_bSamsungAccountDisabled:Z

    .line 47
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isSAccountVersionHigherThan1_3_001(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->_bNewSamsungAccountInstalled:Z

    .line 48
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isSAccountVersionHigherThan1_4_0366(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->_bPopupConfirmPWAccountInstalled:Z

    .line 49
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isSAccountVersionHigherThan1_3_0032(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->_bHideNotificationAccountInstalled:Z

    .line 50
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isSAccountVersionHigherThan1_5_0200(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->_bNewInterfaceAccountInstalled:Z

    .line 56
    :cond_0
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->_bTokenExpired:Z

    .line 57
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->_ILoadingDialogCreator:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;

    .line 58
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->mNotiPopupFactory:Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;

    .line 59
    iput-object p5, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->mIGetDownloadListCheckerFactory:Lcom/sec/android/app/samsungapps/vlibrary2/update/IGetDownloadListCheckerFactory;

    .line 60
    return-void
.end method

.method private callGetDownloadList()V
    .locals 2

    .prologue
    .line 350
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->_Context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 352
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->mIGetDownloadListCheckerFactory:Lcom/sec/android/app/samsungapps/vlibrary2/update/IGetDownloadListCheckerFactory;

    invoke-interface {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/IGetDownloadListCheckerFactory;->createChecker(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/update/IGetDownloadListChecker;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/account/b;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;)V

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/update/IGetDownloadListChecker;->check(Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    .line 364
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    .line 363
    :catch_1
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public autoLogin()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 308
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AutoLoginCommand;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/AutoLoginCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/AutoLoginCommand$IAutoLoginCommandData;)V

    return-object v0
.end method

.method public backgroundLogin()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 276
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/BackgroundLoginCommand;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/BackgroundLoginCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/BackgroundLoginCommand$IBackgroundLoginCommandData;)V

    return-object v0
.end method

.method public clearLoginSession()V
    .locals 2

    .prologue
    .line 266
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->setLoginInfo(Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;)V

    .line 267
    return-void
.end method

.method public clearPasswordConfirmedTime()V
    .locals 1

    .prologue
    .line 244
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->_PasswordConfirmInfo:Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmInfo;->clearPasswordConfirmationTime()V

    .line 245
    return-void
.end method

.method public abstract createCancellableLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ICancellableLoadingDialog;
.end method

.method public createChangeCreditCardCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 2

    .prologue
    .line 99
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CreditCardRegisterCmd;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->_CreditCardStateContext:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CreditCardRegisterCmd;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;)V

    return-object v0
.end method

.method public createForceLogin()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 3

    .prologue
    .line 260
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/IForceLoginCommand;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->mNotiPopupFactory:Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;

    invoke-direct {v0, v1, p0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/account/IForceLoginCommand;-><init>(ZLcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;)V

    return-object v0
.end method

.method public abstract createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
.end method

.method public createLoginValidator(ZLcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 2

    .prologue
    .line 111
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->mNotiPopupFactory:Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;

    invoke-direct {v0, p1, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;-><init>(ZLcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;)V

    .line 112
    invoke-virtual {v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->setCommandToBeProcessedAfterLogin(Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V

    .line 113
    return-object v0
.end method

.method public createRegisterCreditCard(Z)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->_CreditCardStateContext:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->setNeedToDisplayDetail(Z)V

    .line 93
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CreditCardRegisterCmd;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->_CreditCardStateContext:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/CreditCardRegisterCmd;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;)V

    return-object v0
.end method

.method public abstract createRegisterVoucherCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public createValidateConfirmPassword()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 121
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ValidateConfirmPassword;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ValidateConfirmPassword;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/ValidateConfirmPassword$IValidateConfirmPasswordData;)V

    return-object v0
.end method

.method public doAfterLogin()V
    .locals 2

    .prologue
    .line 344
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/accounteventmanager/AccountEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/accounteventmanager/AccountEventManager;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/accounteventmanager/AccountEventManager$AccountEvent;->LOGEDIN:Lcom/sec/android/app/samsungapps/vlibrary2/accounteventmanager/AccountEventManager$AccountEvent;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/accounteventmanager/AccountEventManager;->notifyEvent(Lcom/sec/android/app/samsungapps/vlibrary2/accounteventmanager/AccountEventManager$AccountEvent;)V

    .line 345
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->callGetDownloadList()V

    .line 346
    return-void
.end method

.method public getAccountTokenURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->_TokenURL:Ljava/lang/String;

    return-object v0
.end method

.method public abstract getConfirmPasswordViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
.end method

.method public getEmailID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 298
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getEmailID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected abstract getIranDebitCardRegisterActivity()Ljava/lang/Class;
.end method

.method public getLoadingDialogCreator()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->_ILoadingDialogCreator:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;

    return-object v0
.end method

.method protected abstract getLoginViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
.end method

.method public getNormalLoginCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 2

    .prologue
    .line 303
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->createLoginValidator(ZLcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    return-object v0
.end method

.method protected abstract getOldSamsungAccountLoginInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
.end method

.method public getPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 293
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getPassword()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPasswordConfirmCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 271
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand$IPasswordConfirmCommandData;)V

    return-object v0
.end method

.method public abstract getPasswordConfirmViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
.end method

.method protected abstract getPushServiceRegCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method protected abstract getRegisterCreditCardInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
.end method

.method protected abstract getRequestTokenViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
.end method

.method protected abstract getSamsungAccountHandlerClass()Ljava/lang/Class;
.end method

.method protected abstract getSamsungAccountNewHandlerClass()Ljava/lang/Class;
.end method

.method protected abstract getSignInActivity()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
.end method

.method public getToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 212
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->_Token:Ljava/lang/String;

    return-object v0
.end method

.method public getTokenBasedPasswordConfirmCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 130
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand$ITokenBasedConfirmPasswordCommandData;)V

    return-object v0
.end method

.method public abstract getValidationViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
.end method

.method public hasToken()Z
    .locals 1

    .prologue
    .line 170
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->_Token:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->_Token:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isAutoLogin()Z
    .locals 1

    .prologue
    .line 207
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->bAutoLogin:Z

    return v0
.end method

.method public isHideNotificationSamsungAccountInstalled()Z
    .locals 1

    .prologue
    .line 160
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->_bHideNotificationAccountInstalled:Z

    return v0
.end method

.method public isIDPasswordExist()Z
    .locals 1

    .prologue
    .line 281
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->getPassword()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->getPassword()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 283
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->getEmailID()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->getEmailID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 285
    const/4 v0, 0x1

    .line 288
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLogedIn()Z
    .locals 1

    .prologue
    .line 135
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->_isLogedIn()Z

    move-result v0

    return v0
.end method

.method public isNewInterfaceSamsungAccountInstalled()Z
    .locals 1

    .prologue
    .line 165
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->_bNewInterfaceAccountInstalled:Z

    return v0
.end method

.method public isPasswordConfirmed()Z
    .locals 1

    .prologue
    .line 254
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->_PasswordConfirmInfo:Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmInfo;->isPasswordConfirmed()Z

    move-result v0

    return v0
.end method

.method public isPopupConfirmPWSamsungAccountInstalled()Z
    .locals 1

    .prologue
    .line 155
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->_bPopupConfirmPWAccountInstalled:Z

    return v0
.end method

.method public isSamsungAccountDisabled()Z
    .locals 1

    .prologue
    .line 145
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->_bSamsungAccountDisabled:Z

    return v0
.end method

.method public isSamsungAccountInstalled()Z
    .locals 1

    .prologue
    .line 140
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->_bSamsungAccountInstalled:Z

    return v0
.end method

.method public isTokenBasedSamsungAccountInstalled()Z
    .locals 1

    .prologue
    .line 150
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->_bNewSamsungAccountInstalled:Z

    return v0
.end method

.method public isTokenExpired()Z
    .locals 1

    .prologue
    .line 227
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->_bTokenExpired:Z

    return v0
.end method

.method public loginODCWithOldAccount()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 2

    .prologue
    .line 232
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->getLoginViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand$ILoginCommandData;Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;)V

    return-object v0
.end method

.method public loginODCWithToken()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 185
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand$ILoginODCWithTokenCommandData;)V

    return-object v0
.end method

.method public loginSamsungAccount()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 2

    .prologue
    .line 175
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginOldSamsungAccountCommand;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->getOldSamsungAccountLoginInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginOldSamsungAccountCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;)V

    return-object v0
.end method

.method public logout()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 318
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/LogoutCommand$ILogoutCommandData;)V

    return-object v0
.end method

.method public normalLogin()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 2

    .prologue
    .line 180
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->getLoginViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand$ILoginCommandData;Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;)V

    return-object v0
.end method

.method public requestToken()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 2

    .prologue
    .line 239
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->getRequestTokenViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$IRequestTokenCommandData;)V

    return-object v0
.end method

.method public setAutoLogin(Z)V
    .locals 0

    .prologue
    .line 201
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/status/SamsungAppsStatus;->setAutoLoginInProgress(Z)V

    .line 202
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->bAutoLogin:Z

    .line 203
    return-void
.end method

.method public setPasswordConfirmedTime()V
    .locals 1

    .prologue
    .line 249
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->_PasswordConfirmInfo:Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmInfo;->setPasswordConfirmationTimeNow()V

    .line 250
    return-void
.end method

.method public setTokenExpired()V
    .locals 1

    .prologue
    .line 222
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->_bTokenExpired:Z

    .line 223
    return-void
.end method

.method public setTokenResult(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 190
    sput-object p1, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->_Token:Ljava/lang/String;

    .line 191
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->_TokenURL:Ljava/lang/String;

    .line 192
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/AbstractAccountCommandBuilder;->_bTokenExpired:Z

    .line 196
    return-void
.end method

.method public tokenBasedLogin()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 313
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin$ITokenBasedLoginData;)V

    return-object v0
.end method
