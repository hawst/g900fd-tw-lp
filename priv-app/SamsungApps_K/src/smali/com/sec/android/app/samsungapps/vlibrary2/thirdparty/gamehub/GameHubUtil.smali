.class public Lcom/sec/android/app/samsungapps/vlibrary2/thirdparty/gamehub/GameHubUtil;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static map:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/thirdparty/gamehub/GameHubUtil;->map:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static add(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 75
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/thirdparty/gamehub/a;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/thirdparty/gamehub/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/thirdparty/gamehub/GameHubUtil;->map:Ljava/util/HashMap;

    invoke-virtual {v1, p0, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    return-void
.end method

.method private static get(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/thirdparty/gamehub/a;
    .locals 1

    .prologue
    .line 81
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/thirdparty/gamehub/GameHubUtil;->map:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/thirdparty/gamehub/a;

    .line 82
    return-object v0
.end method

.method public static notifyBroadcastIntentForLaunchApp(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 16
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isGameHubSupported()Z

    move-result v0

    if-nez v0, :cond_0

    .line 29
    :goto_0
    return-void

    .line 20
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.samsungapps.action.LAUNCH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 21
    const-string v1, "GUID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 24
    :try_start_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 25
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static notifyBroadcastIntentForPurchaseCompleted(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/thirdparty/gamehub/a;)V
    .locals 3

    .prologue
    .line 53
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isGameHubSupported()Z

    move-result v0

    if-nez v0, :cond_0

    .line 69
    :goto_0
    return-void

    .line 57
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.samsungapps.action.PURCHASECOMPLETED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 58
    const-string v1, "GUID"

    iget-object v2, p1, Lcom/sec/android/app/samsungapps/vlibrary2/thirdparty/gamehub/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 59
    const-string v1, "PRODUCTID"

    iget-object v2, p1, Lcom/sec/android/app/samsungapps/vlibrary2/thirdparty/gamehub/a;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 60
    const-string v1, "PRICE"

    iget-object v2, p1, Lcom/sec/android/app/samsungapps/vlibrary2/thirdparty/gamehub/a;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 61
    const-string v1, "TITLE"

    iget-object v2, p1, Lcom/sec/android/app/samsungapps/vlibrary2/thirdparty/gamehub/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 64
    :try_start_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 65
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static notifyBroadcastIntentForStartDownload(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 35
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isGameHubSupported()Z

    move-result v0

    if-nez v0, :cond_0

    .line 48
    :goto_0
    return-void

    .line 39
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.samsungapps.action.STARTDOWNLOADING"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 40
    const-string v1, "GUID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 43
    :try_start_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 44
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static purchaseComplete(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 92
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/thirdparty/gamehub/GameHubUtil;->get(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/thirdparty/gamehub/a;

    move-result-object v0

    .line 93
    if-eqz v0, :cond_0

    .line 95
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/thirdparty/gamehub/GameHubUtil;->remove(Ljava/lang/String;)V

    .line 96
    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/thirdparty/gamehub/GameHubUtil;->notifyBroadcastIntentForPurchaseCompleted(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/thirdparty/gamehub/a;)V

    .line 98
    :cond_0
    return-void
.end method

.method public static remove(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/thirdparty/gamehub/GameHubUtil;->map:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    return-void
.end method
