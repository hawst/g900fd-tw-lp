.class public abstract Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/AbsPPCCommandBuilder;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;


# instance fields
.field private _ExtraObject:Ljava/lang/Object;

.field private _FinalMap:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

.field private _PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

.field _cardBalance:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PrepaidCardBalance;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PrepaidCardBalance;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PrepaidCardBalance;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/AbsPPCCommandBuilder;->_cardBalance:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PrepaidCardBalance;

    .line 16
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/AbsPPCCommandBuilder;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    .line 17
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/AbsPPCCommandBuilder;->_FinalMap:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    .line 18
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/AbsPPCCommandBuilder;->_ExtraObject:Ljava/lang/Object;

    .line 19
    return-void
.end method


# virtual methods
.method public checkCardBalance()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 3

    .prologue
    .line 24
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCheckCardBalanceCommand;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/AbsPPCCommandBuilder;->_cardBalance:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PrepaidCardBalance;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/AbsPPCCommandBuilder;->createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCheckCardBalanceCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PrepaidCardBalance;Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;)V

    return-object v0
.end method

.method public complete()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 2

    .prologue
    .line 43
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/AbsPPCCommandBuilder;->createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;)V

    return-object v0
.end method

.method protected abstract createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
.end method

.method public createPayment()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 2

    .prologue
    .line 69
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCPayment;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/AbsPPCCommandBuilder;->_FinalMap:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCPayment;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    return-object v0
.end method

.method public getCardBalance()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PrepaidCardBalance;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/AbsPPCCommandBuilder;->_cardBalance:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PrepaidCardBalance;

    return-object v0
.end method

.method public getExtraObject()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/AbsPPCCommandBuilder;->_ExtraObject:Ljava/lang/Object;

    return-object v0
.end method

.method public getFinalMap()Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/AbsPPCCommandBuilder;->_FinalMap:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    return-object v0
.end method

.method public getPurchaseInfo()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/AbsPPCCommandBuilder;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    return-object v0
.end method

.method public isCardBalanceEnough()Z
    .locals 4

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/AbsPPCCommandBuilder;->_cardBalance:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PrepaidCardBalance;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PrepaidCardBalance;->getBalance()D

    move-result-wide v0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/AbsPPCCommandBuilder;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getBuyPrice()D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_0

    .line 31
    const/4 v0, 0x1

    .line 33
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public requestRegisterCard()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 3

    .prologue
    .line 38
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/AbsPPCCommandBuilder;->_cardBalance:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PrepaidCardBalance;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/AbsPPCCommandBuilder;->createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PrepaidCardBalance;Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;)V

    return-object v0
.end method
