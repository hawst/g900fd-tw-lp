.class public Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqGUIDProductDetailCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand;
.source "ProGuard"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand$IReqDetailCommandData;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand$IReqDetailCommandData;)V

    .line 17
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqGUIDProductDetailCommand;Z)V
    .locals 0

    .prologue
    .line 12
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqGUIDProductDetailCommand;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method protected createRequest()Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    .locals 4

    .prologue
    .line 21
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ProductDetailMainParser;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqGUIDProductDetailCommand;->_IContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand$IReqDetailCommandData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand$IReqDetailCommandData;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ProductDetailMainParser;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    .line 22
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqGUIDProductDetailCommand;->_IContentCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand$IReqDetailCommandData;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqDetailCommand$IReqDetailCommandData;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/content/n;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/n;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqGUIDProductDetailCommand;)V

    invoke-virtual {v1, v2, v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->guidProductDetail(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 29
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ReqGUIDProductDetailCommand;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 30
    return-object v0
.end method
