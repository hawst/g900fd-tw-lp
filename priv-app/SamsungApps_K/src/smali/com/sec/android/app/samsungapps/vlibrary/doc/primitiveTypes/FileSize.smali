.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private mSize:J

.field private oldMSize:J

.field private oldStr:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->oldStr:Ljava/lang/String;

    .line 47
    iput-wide v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->oldMSize:J

    .line 17
    iput-wide v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->mSize:J

    .line 18
    return-void
.end method

.method public constructor <init>(J)V
    .locals 2

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->oldStr:Ljava/lang/String;

    .line 47
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->oldMSize:J

    .line 13
    iput-wide p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->mSize:J

    .line 14
    return-void
.end method

.method private getByteFormat(Z)Ljava/lang/String;
    .locals 3

    .prologue
    .line 73
    :try_start_0
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "#,##0"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 74
    iget-wide v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->mSize:J

    invoke-virtual {v0, v1, v2}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    .line 75
    if-eqz p1, :cond_0

    .line 77
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " octets"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 85
    :goto_0
    return-object v0

    .line 81
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " bytes"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 85
    :catch_0
    move-exception v0

    const-string v0, ""

    goto :goto_0
.end method

.method private getKbFormat(Z)Ljava/lang/String;
    .locals 5

    .prologue
    .line 91
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->mSize:J

    long-to-double v0, v0

    .line 93
    :try_start_0
    new-instance v2, Ljava/text/DecimalFormat;

    const-string v3, "#,##0.00"

    invoke-direct {v2, v3}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 94
    const-wide/high16 v3, 0x4090000000000000L    # 1024.0

    div-double/2addr v0, v3

    invoke-virtual {v2, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    .line 95
    if-eqz p1, :cond_0

    .line 97
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Ko"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 105
    :goto_0
    return-object v0

    .line 101
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " KB"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 105
    :catch_0
    move-exception v0

    const-string v0, ""

    goto :goto_0
.end method

.method private getMbFormat(Z)Ljava/lang/String;
    .locals 6

    .prologue
    const-wide/high16 v4, 0x4090000000000000L    # 1024.0

    .line 111
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->mSize:J

    long-to-double v0, v0

    .line 113
    :try_start_0
    new-instance v2, Ljava/text/DecimalFormat;

    const-string v3, "#,##0.00"

    invoke-direct {v2, v3}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 114
    div-double/2addr v0, v4

    div-double/2addr v0, v4

    invoke-virtual {v2, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    .line 115
    if-eqz p1, :cond_0

    .line 117
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Mo"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 125
    :goto_0
    return-object v0

    .line 121
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " MB"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 125
    :catch_0
    move-exception v0

    const-string v0, ""

    goto :goto_0
.end method

.method private isFrench()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 24
    :try_start_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 25
    const-string v2, "fr"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    if-eqz v1, :cond_0

    .line 27
    const/4 v0, 0x1

    .line 42
    :cond_0
    :goto_0
    return v0

    .line 36
    :catch_0
    move-exception v1

    const-string v1, "FileSize::isFrench()::Locale information is empty or not correct"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 39
    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public getShortFormatString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 51
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->oldMSize:J

    iget-wide v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->mSize:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->oldStr:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->oldStr:Ljava/lang/String;

    .line 67
    :goto_0
    return-object v0

    .line 55
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->mSize:J

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->oldMSize:J

    .line 61
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->mSize:J

    const-wide/32 v2, 0x100000

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 63
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->isFrench()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->getKbFormat(Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->oldStr:Ljava/lang/String;

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->oldStr:Ljava/lang/String;

    goto :goto_0

    .line 66
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->isFrench()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->getMbFormat(Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->oldStr:Ljava/lang/String;

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->oldStr:Ljava/lang/String;

    goto :goto_0
.end method

.method public getSize()J
    .locals 2

    .prologue
    .line 131
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->mSize:J

    return-wide v0
.end method

.method public setSize(J)V
    .locals 0

    .prologue
    .line 136
    iput-wide p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->mSize:J

    .line 137
    return-void
.end method
