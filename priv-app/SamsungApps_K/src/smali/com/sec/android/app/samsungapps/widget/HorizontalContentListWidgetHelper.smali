.class public Lcom/sec/android/app/samsungapps/widget/HorizontalContentListWidgetHelper;
.super Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/widget/interfaces/IHorizontalContentListWidgetData;


# static fields
.field public static final CONTENT_LIST_PREMIUM:I = 0x1

.field public static final CONTENT_LIST_TOP_FREE:I = 0x4

.field public static final CONTENT_LIST_TOP_NEW:I = 0x8

.field public static final CONTENT_LIST_TOP_PAID:I = 0x2


# instance fields
.field a:Landroid/content/Context;

.field b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 37
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->FeaturedHot:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)V

    .line 25
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/HorizontalContentListWidgetHelper;->a:Landroid/content/Context;

    .line 26
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/HorizontalContentListWidgetHelper;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    .line 28
    const-string v0, "HorizontalContentListWidgetHelper"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/HorizontalContentListWidgetHelper;->c:Ljava/lang/String;

    .line 39
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/HorizontalContentListWidgetHelper;->a:Landroid/content/Context;

    .line 40
    return-void
.end method

.method private a(I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 147
    const-string v0, ""

    .line 151
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/HorizontalContentListWidgetHelper;->a:Landroid/content/Context;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "cache"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".txt"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/file/FilePath;->getAbsolutePath(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 158
    :goto_0
    return-object v0

    .line 153
    :catch_0
    move-exception v1

    .line 155
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "HorizontalContentListWidgetHelper_getCacheFilePath::"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/HorizontalContentListWidgetHelper;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/HorizontalContentListWidgetHelper;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getContent(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;
    .locals 2

    .prologue
    .line 199
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/HorizontalContentListWidgetHelper;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    if-eqz v0, :cond_1

    .line 204
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/HorizontalContentListWidgetHelper;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    .line 211
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->size()I

    move-result v1

    if-gt v1, p1, :cond_2

    .line 213
    :cond_0
    const-string v0, "HorizontalContentListWidgetHelper::getContent::contentList is empty or less"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 214
    const/4 v0, 0x0

    .line 219
    :goto_1
    return-object v0

    .line 208
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/HorizontalContentListWidgetHelper;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v0

    goto :goto_0

    .line 217
    :cond_2
    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    goto :goto_1
.end method

.method public getContentCount()I
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/HorizontalContentListWidgetHelper;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/HorizontalContentListWidgetHelper;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    .line 180
    :goto_0
    if-nez v0, :cond_1

    .line 182
    const-string v0, "HorizontalContentListWidgetHelper::getContentCount::content list is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 183
    const/4 v0, 0x0

    .line 188
    :goto_1
    return v0

    .line 177
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/HorizontalContentListWidgetHelper;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v0

    goto :goto_0

    .line 186
    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->size()I

    move-result v0

    goto :goto_1
.end method

.method public isFranceStore()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 229
    .line 231
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/HorizontalContentListWidgetHelper;->a:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 233
    const-string v1, "HorizontalContentListWidgetHelper::isFranceStore::Context is null"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 244
    :cond_0
    :goto_0
    return v0

    .line 237
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/HorizontalContentListWidgetHelper;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    .line 238
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    .line 239
    if-eqz v1, :cond_0

    .line 241
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isFrance()Z

    move-result v0

    goto :goto_0
.end method

.method public isGermanyStore()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 254
    .line 256
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/HorizontalContentListWidgetHelper;->a:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 258
    const-string v1, "HorizontalContentListWidgetHelper::isGermanyStore::Context is null"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 269
    :cond_0
    :goto_0
    return v0

    .line 262
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/HorizontalContentListWidgetHelper;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    .line 263
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    .line 264
    if-eqz v1, :cond_0

    .line 266
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isGermany()Z

    move-result v0

    goto :goto_0
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 47
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/HorizontalContentListWidgetHelper;->a:Landroid/content/Context;

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/HorizontalContentListWidgetHelper;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/HorizontalContentListWidgetHelper;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->clear()V

    .line 52
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/HorizontalContentListWidgetHelper;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    .line 54
    :cond_0
    return-void
.end method

.method public sendRequest(ILcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V
    .locals 3

    .prologue
    .line 64
    packed-switch p1, :pswitch_data_0

    .line 83
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "HorizontalContentListWidgetHelper::sendRequest::Invalid type="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 100
    :goto_0
    return-void

    .line 67
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->FeaturedHot:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/HorizontalContentListWidgetHelper;->setType(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)V

    .line 87
    :goto_1
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/HorizontalContentListWidgetHelper;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->load(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v0

    .line 88
    if-eqz v0, :cond_0

    .line 90
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HorizontalContentListWidgetHelpersendRequest::Existed Cache::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    .line 92
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/HorizontalContentListWidgetHelper;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    .line 93
    if-eqz p2, :cond_0

    .line 95
    const/4 v0, 0x0

    const/4 v1, 0x1

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

    invoke-direct {v2}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;-><init>()V

    invoke-interface {p2, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;->onReceiveResult(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V

    .line 99
    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/p;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/p;-><init>(Lcom/sec/android/app/samsungapps/widget/HorizontalContentListWidgetHelper;ILcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/HorizontalContentListWidgetHelper;->a:Landroid/content/Context;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/widget/HorizontalContentListWidgetHelper;->requestDataGet(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;Landroid/content/Context;)V

    goto :goto_0

    .line 71
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopPaid:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/HorizontalContentListWidgetHelper;->setType(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)V

    goto :goto_1

    .line 75
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopFree:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/HorizontalContentListWidgetHelper;->setType(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)V

    goto :goto_1

    .line 79
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopNew:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/HorizontalContentListWidgetHelper;->setType(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)V

    goto :goto_1

    .line 64
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method
