.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/BannerParser;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;
.source "ProGuard"


# instance fields
.field mBannerList:Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;)V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;-><init>()V

    .line 7
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/BannerParser;->mBannerList:Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;

    .line 10
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/BannerParser;->mBannerList:Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;

    .line 11
    return-void
.end method


# virtual methods
.method protected onCreateObject(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 2

    .prologue
    .line 14
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 15
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/BannerParser;->mBannerList:Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;->size()I

    move-result v1

    iput v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Banner;->bannerIndex:I

    .line 16
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/BannerParser;->mBannerList:Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;->add(Ljava/lang/Object;)Z

    .line 17
    return-void
.end method

.method protected onPostParseError()V
    .locals 0

    .prologue
    .line 22
    return-void
.end method

.method protected onPostParseResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/BannerParser;->mBannerList:Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;->clear()V

    .line 26
    return-void
.end method
