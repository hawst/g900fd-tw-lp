.class public final enum Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

.field public static final enum CHACHE_LOADING:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

.field public static final enum CHECK_CACHE:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

.field public static final enum IDLE:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

.field public static final enum LOADING:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

.field public static final enum LOAD_FAILED:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

.field public static final enum LOAD_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 29
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;->LOADING:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    const-string v1, "LOAD_FAILED"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;->LOAD_FAILED:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    const-string v1, "LOAD_SUCCESS"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;->LOAD_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    const-string v1, "CHECK_CACHE"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;->CHECK_CACHE:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    const-string v1, "CHACHE_LOADING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;->CHACHE_LOADING:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    .line 27
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;->LOADING:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;->LOAD_FAILED:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;->LOAD_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;->CHECK_CACHE:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;->CHACHE_LOADING:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    return-object v0
.end method
