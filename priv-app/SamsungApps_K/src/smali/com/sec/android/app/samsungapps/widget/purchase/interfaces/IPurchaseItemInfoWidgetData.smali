.class public interface abstract Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPurchaseItemInfoWidgetData;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract appTitle()Ljava/lang/String;
.end method

.method public abstract category()Ljava/lang/String;
.end method

.method public abstract count()I
.end method

.method public abstract discountFlag()Z
.end method

.method public abstract getDiscountPrice()Ljava/lang/String;
.end method

.method public abstract getNormalPrice()Ljava/lang/String;
.end method

.method public abstract isAlreadyPurchasedContent()Z
.end method

.method public abstract isWidgetContent()Z
.end method

.method public abstract productImageURL()Ljava/lang/String;
.end method

.method public abstract rating()I
.end method

.method public abstract release()V
.end method

.method public abstract sellerInfo()Ljava/lang/String;
.end method
