.class public Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;


# instance fields
.field private mListListMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator$IListListMapContainer;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator$IListListMapContainer;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator;->mListListMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator$IListListMapContainer;

    .line 16
    if-eqz p1, :cond_0

    .line 17
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator$IListListMapContainer;->clearContainer()V

    .line 19
    :cond_0
    return-void
.end method

.method private setMap(Ljava/util/ArrayList;)V
    .locals 6

    .prologue
    .line 37
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 39
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator;->mListListMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator$IListListMapContainer;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator$IListListMapContainer;->openMap()V

    .line 40
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->keySet()Ljava/util/Set;

    move-result-object v1

    .line 41
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 43
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator;->mListListMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator$IListListMapContainer;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator$IListListMapContainer;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator;->mListListMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator$IListListMapContainer;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator$IListListMapContainer;->closeMap()V

    goto :goto_0

    .line 47
    :cond_1
    return-void
.end method


# virtual methods
.method public onReceiveParsingResult(Lcom/sec/android/app/samsungapps/vlibrary/xml/result/IResponseParseResult;)V
    .locals 3

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator;->mListListMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator$IListListMapContainer;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/result/IResponseParseResult;->getHeaderMap()Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator$IListListMapContainer;->setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 25
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/result/IResponseParseResult;->getBodyListListMap()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 27
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator;->mListListMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator$IListListMapContainer;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator$IListListMapContainer;->openList()V

    .line 29
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator;->setMap(Ljava/util/ArrayList;)V

    .line 31
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator;->mListListMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator$IListListMapContainer;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator$IListListMapContainer;->closeList()V

    goto :goto_0

    .line 33
    :cond_0
    return-void
.end method
