.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/CustomerCouponDetailParser;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;
.source "ProGuard"


# instance fields
.field mCoupon:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;)V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;-><init>()V

    .line 8
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CustomerCouponDetailParser;->mCoupon:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    .line 11
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CustomerCouponDetailParser;->mCoupon:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    .line 12
    return-void
.end method


# virtual methods
.method protected onCreateObject(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 3

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CustomerCouponDetailParser;->mCoupon:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->getDetail()Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponDetail;

    move-result-object v0

    .line 23
    if-nez v0, :cond_0

    .line 25
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponDetail;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponDetail;-><init>()V

    .line 27
    :cond_0
    const-class v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponDetail;

    const/4 v2, 0x1

    invoke-virtual {p1, v1, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->mappingClass(Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 28
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/CustomerCouponDetailParser;->mCoupon:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    invoke-interface {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->setDetail(Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponDetail;)V

    .line 29
    return-void
.end method

.method protected onPostParseError()V
    .locals 0

    .prologue
    .line 17
    return-void
.end method

.method protected onPostParseResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 35
    return-void
.end method
