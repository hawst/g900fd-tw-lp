.class final Lcom/sec/android/app/samsungapps/ij;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/receiverinterface/OnResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/SearchResultActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/SearchResultActivity;)V
    .locals 0

    .prologue
    .line 1106
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/ij;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onResult(ZLjava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1109
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;

    .line 1110
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ij;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->c(Lcom/sec/android/app/samsungapps/SearchResultActivity;)Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 1112
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ij;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    invoke-static {v0, p2}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->a(Lcom/sec/android/app/samsungapps/SearchResultActivity;Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;)Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;

    .line 1115
    :cond_0
    if-eqz p2, :cond_1

    .line 1116
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Result: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->d(Ljava/lang/String;)V

    .line 1119
    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->isCanceled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1120
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ij;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->m(Lcom/sec/android/app/samsungapps/SearchResultActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1121
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ij;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->m(Lcom/sec/android/app/samsungapps/SearchResultActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 1122
    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1123
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ij;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->m(Lcom/sec/android/app/samsungapps/SearchResultActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1125
    :cond_2
    return-void
.end method
