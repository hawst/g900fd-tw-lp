.class public Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeStateMachine;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;
.source "ProGuard"


# static fields
.field private static instance:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeStateMachine;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;-><init>()V

    .line 15
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeStateMachine;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeStateMachine;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeStateMachine;

    if-nez v0, :cond_0

    .line 21
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeStateMachine;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeStateMachine;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeStateMachine;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeStateMachine;

    .line 23
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeStateMachine;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeStateMachine;

    return-object v0
.end method


# virtual methods
.method protected entry(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 2

    .prologue
    .line 69
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/d;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 81
    :goto_0
    :pswitch_0
    return-void

    .line 71
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Action;->SEND_SIG_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 76
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Action;->REQUEST_INIT:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 77
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Action;->START_CANCELLABLE_LOADING:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 80
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Action;->SEND_SIG_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 69
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;)Z
    .locals 2

    .prologue
    .line 30
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/d;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 59
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    return v0

    .line 35
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/d;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 38
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$State;->REQUEST_INIT:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 42
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$State;->REQUEST_INIT:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 47
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/d;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 50
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 53
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 56
    :pswitch_7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 30
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_4
    .end packed-switch

    .line 35
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 47
    :pswitch_data_2
    .packed-switch 0x3
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public bridge synthetic execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 9
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;)Z

    move-result v0

    return v0
.end method

.method protected exit(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 2

    .prologue
    .line 91
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/d;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 98
    :goto_0
    :pswitch_0
    return-void

    .line 97
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Action;->STOP_CANCELLABLE_LOADING:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 91
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
