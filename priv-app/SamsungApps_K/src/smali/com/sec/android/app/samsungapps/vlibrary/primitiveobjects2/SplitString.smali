.class public Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/SplitString;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private mResult:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 10
    const-string v0, ","

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/SplitString;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/SplitString;->mResult:Ljava/util/ArrayList;

    .line 15
    if-nez p1, :cond_1

    .line 24
    :cond_0
    return-void

    .line 18
    :cond_1
    invoke-virtual {p1, p2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 19
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/SplitString;->mResult:Ljava/util/ArrayList;

    .line 20
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 22
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/SplitString;->mResult:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 20
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public get(I)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 41
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/SplitString;->mResult:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 47
    :cond_0
    :goto_0
    return-object v0

    .line 44
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/SplitString;->mResult:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/SplitString;->mResult:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/SplitString;->mResult:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 29
    const/4 v0, 0x0

    .line 31
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/SplitString;->mResult:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method
