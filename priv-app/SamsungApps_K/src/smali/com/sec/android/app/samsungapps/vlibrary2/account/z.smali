.class final Lcom/sec/android/app/samsungapps/vlibrary2/account/z;
.super Lcom/msc/sa/aidl/ISACallback$Stub;
.source "ProGuard"


# instance fields
.field a:Ljava/lang/Boolean;

.field b:Ljava/lang/Boolean;

.field c:Ljava/lang/Boolean;

.field d:Ljava/lang/Boolean;

.field final synthetic e:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/z;->e:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;

    invoke-direct {p0}, Lcom/msc/sa/aidl/ISACallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceiveAccessToken(IZLandroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 178
    if-eqz p2, :cond_0

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/z;->e:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;

    const/4 v2, -0x1

    invoke-direct {v1, p3, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;-><init>(Landroid/os/Bundle;I)V

    # setter for: Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;->mBroadCastResult:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;
    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;->access$102(Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;)Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/z;->e:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;->onSuccess()V

    .line 208
    :goto_0
    return-void

    .line 184
    :cond_0
    const-string v0, "error_code"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 185
    const-string v1, "error_message"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 187
    const-string v2, "SAC_0204"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 189
    const-string v2, "tnc_acceptance_required"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/z;->a:Ljava/lang/Boolean;

    .line 192
    const-string v2, "name_verification _required"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/z;->b:Ljava/lang/Boolean;

    .line 195
    const-string v2, "email_validation_required"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/z;->c:Ljava/lang/Boolean;

    .line 198
    const-string v2, "mandatory_input_required"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/z;->d:Ljava/lang/Boolean;

    .line 202
    :cond_1
    const-string v2, "SAC"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error Code : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    const-string v0, "SAC"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error Message : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/z;->e:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;

    const/4 v2, 0x1

    invoke-direct {v1, p3, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;-><init>(Landroid/os/Bundle;I)V

    # setter for: Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;->mBroadCastResult:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;
    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;->access$102(Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;)Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;

    .line 206
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/z;->e:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;->onFail()V

    goto :goto_0
.end method

.method public final onReceiveAuthCode(IZLandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 172
    return-void
.end method

.method public final onReceiveChecklistValidation(IZLandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 165
    return-void
.end method

.method public final onReceiveDisclaimerAgreement(IZLandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 158
    return-void
.end method

.method public final onReceivePasswordConfirmation(IZLandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 215
    return-void
.end method

.method public final onReceiveSCloudAccessToken(IZLandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 151
    return-void
.end method
