.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/ReportAppDefectRequestXML;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
.source "ProGuard"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/AppDefect;I)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 9
    const-string v2, "reportAppDefect"

    const-string v3, "4080"

    move-object v0, p0

    move-object v1, p1

    move v4, p3

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 10
    const-string v0, "productID"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AppDefect;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ReportAppDefectRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    const-string v0, "comment"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AppDefect;->getComment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ReportAppDefectRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    const-string v0, "type"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AppDefect;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ReportAppDefectRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/xml/ReportAppDefectRequestXML$IReportProblemInfo;I)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 17
    const-string v2, "reportAppDefect"

    const-string v3, "4080"

    move-object v0, p0

    move-object v1, p1

    move v4, p3

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 18
    const-string v0, "productID"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ReportAppDefectRequestXML$IReportProblemInfo;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ReportAppDefectRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    const-string v0, "comment"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ReportAppDefectRequestXML$IReportProblemInfo;->getComment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ReportAppDefectRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    const-string v0, "type"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ReportAppDefectRequestXML$IReportProblemInfo;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/ReportAppDefectRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    return-void
.end method
