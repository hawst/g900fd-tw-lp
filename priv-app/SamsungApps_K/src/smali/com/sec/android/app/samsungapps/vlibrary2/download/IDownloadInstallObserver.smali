.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/download/IDownloadInstallObserver;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterListener;


# virtual methods
.method public abstract onDownloadCompleted(Lcom/sec/android/app/samsungapps/vlibrary2/download/IRequestFileInfo;Z)V
.end method

.method public abstract onInstallCompleted(Lcom/sec/android/app/samsungapps/vlibrary2/download/IRequestFileInfo;Z)V
.end method

.method public abstract onStartDownload(Lcom/sec/android/app/samsungapps/vlibrary2/download/IRequestFileInfo;)V
.end method

.method public abstract onStartInstall(Lcom/sec/android/app/samsungapps/vlibrary2/download/IRequestFileInfo;)V
.end method
