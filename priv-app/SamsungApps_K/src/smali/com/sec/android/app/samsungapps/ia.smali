.class final Lcom/sec/android/app/samsungapps/ia;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$ContentListQueryObserver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/SearchResultActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/SearchResultActivity;)V
    .locals 0

    .prologue
    .line 393
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/ia;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final contentListGetCompleted(Z)V
    .locals 1

    .prologue
    .line 398
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ia;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->hideLoadingEmptyView()V

    .line 401
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ia;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mContentListQuery:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v0

    .line 403
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 407
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ia;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->updateHotKeywordList()V

    .line 411
    :goto_0
    return-void

    .line 409
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ia;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->updateSearchResultCount()V

    goto :goto_0
.end method

.method public final contentListLoading()V
    .locals 1

    .prologue
    .line 418
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ia;->a:Lcom/sec/android/app/samsungapps/SearchResultActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->setLoadingView()V

    .line 421
    return-void
.end method

.method public final finishGettingMoreContent(Z)V
    .locals 0

    .prologue
    .line 428
    return-void
.end method

.method public final startGettingMoreContent()V
    .locals 0

    .prologue
    .line 433
    return-void
.end method
