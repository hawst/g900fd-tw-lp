.class public Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpecList;
.super Ljava/util/ArrayList;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/primitives/IPaymentMethodList;


# static fields
.field private static final SERIALVERSIONUID:J = 0x1L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    return-void
.end method


# virtual methods
.method public contains(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;)Z
    .locals 2

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpecList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    .line 15
    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 17
    const/4 v0, 0x1

    .line 20
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic get(I)Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;
    .locals 1

    .prologue
    .line 5
    invoke-super {p0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    return-object v0
.end method
