.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardDetail;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# instance fields
.field public avaliableServices:Ljava/lang/String;

.field public balanceAmount:D

.field public currencyCode:Ljava/lang/String;

.field public currencyUnit:Ljava/lang/String;

.field public giftCardAmount:D

.field public giftCardCode:Ljava/lang/String;

.field public giftCardDescription:Ljava/lang/String;

.field public giftCardImagePath:Ljava/lang/String;

.field public giftCardName:Ljava/lang/String;

.field public giftCardStatusCode:Ljava/lang/String;

.field public lastUsageDate:Ljava/lang/String;

.field map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

.field public userExpireDate:Ljava/lang/String;

.field public userRegisterDate:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->giftCardCode:Ljava/lang/String;

    .line 11
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->giftCardName:Ljava/lang/String;

    .line 12
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->currencyCode:Ljava/lang/String;

    .line 13
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->currencyUnit:Ljava/lang/String;

    .line 16
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->userRegisterDate:Ljava/lang/String;

    .line 17
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->userExpireDate:Ljava/lang/String;

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->lastUsageDate:Ljava/lang/String;

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->avaliableServices:Ljava/lang/String;

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->giftCardStatusCode:Ljava/lang/String;

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->giftCardDescription:Ljava/lang/String;

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->giftCardImagePath:Ljava/lang/String;

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    return-void
.end method


# virtual methods
.method public addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    if-eqz v0, :cond_0

    .line 40
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 42
    :cond_0
    return-void
.end method

.method public clearContainer()V
    .locals 0

    .prologue
    .line 29
    return-void
.end method

.method public closeMap()V
    .locals 3

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, p0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;->mappingClass(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 50
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 51
    return-void
.end method

.method public findString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    return-object v0
.end method

.method public getAvailableServices()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->avaliableServices:Ljava/lang/String;

    return-object v0
.end method

.method public getBalanceAmount()D
    .locals 2

    .prologue
    .line 95
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->balanceAmount:D

    return-wide v0
.end method

.method public getCurrencyCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->currencyCode:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrencyUnit()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->currencyUnit:Ljava/lang/String;

    return-object v0
.end method

.method public getGiftCardCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->giftCardCode:Ljava/lang/String;

    return-object v0
.end method

.method public getGiftCardDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->giftCardDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getGiftCardImagePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->giftCardImagePath:Ljava/lang/String;

    return-object v0
.end method

.method public getGiftCardName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->giftCardName:Ljava/lang/String;

    return-object v0
.end method

.method public getGiftCardStatusCode()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;
    .locals 2

    .prologue
    .line 120
    const-string v0, "A"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->giftCardStatusCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;->ACTIVE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;->EXPIRED:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;

    goto :goto_0
.end method

.method public getLastUsageDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->lastUsageDate:Ljava/lang/String;

    return-object v0
.end method

.method public getUserExpireDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->userExpireDate:Ljava/lang/String;

    return-object v0
.end method

.method public getUserRegisterDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->userRegisterDate:Ljava/lang/String;

    return-object v0
.end method

.method public giftCardAmount()D
    .locals 2

    .prologue
    .line 90
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->giftCardAmount:D

    return-wide v0
.end method

.method public openMap()V
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 34
    return-void
.end method

.method public setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 66
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x1

    return v0
.end method
