.class public Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/account/IPasswordConfirm;


# instance fields
.field _ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

.field private _ITokenBasedConfirmPasswordCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand$ITokenBasedConfirmPasswordCommandData;

.field private _View:Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand$PasswordConfirmView;

.field map:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand$ITokenBasedConfirmPasswordCommandData;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 20
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;->map:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;

    .line 24
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;->_ITokenBasedConfirmPasswordCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand$ITokenBasedConfirmPasswordCommandData;

    .line 25
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;->endLoading()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand$ITokenBasedConfirmPasswordCommandData;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;->_ITokenBasedConfirmPasswordCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand$ITokenBasedConfirmPasswordCommandData;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand$PasswordConfirmView;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;->_View:Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand$PasswordConfirmView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;Z)V
    .locals 0

    .prologue
    .line 17
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;->onFinalResult(Z)V

    return-void
.end method

.method private endLoading()V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->endLoading()V

    .line 48
    :cond_0
    return-void
.end method

.method private invokeView()V
    .locals 2

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;->_ITokenBasedConfirmPasswordCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand$ITokenBasedConfirmPasswordCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand$ITokenBasedConfirmPasswordCommandData;->getConfirmPasswordViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;->_Context:Landroid/content/Context;

    invoke-interface {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;->invoke(Landroid/content/Context;Ljava/lang/Object;)V

    .line 34
    return-void
.end method

.method private startLoading()V
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;->_ITokenBasedConfirmPasswordCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand$ITokenBasedConfirmPasswordCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand$ITokenBasedConfirmPasswordCommandData;->createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    .line 40
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->startLoading()V

    .line 41
    return-void
.end method


# virtual methods
.method public cancelConfirm()V
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;->onFinalResult(Z)V

    .line 89
    return-void
.end method

.method public confirmPassword(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 52
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getEmailID()Ljava/lang/String;

    move-result-object v0

    .line 53
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/account/ac;

    invoke-direct {v1, v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ac;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;->startLoading()V

    .line 55
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;->map:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;

    new-instance v4, Lcom/sec/android/app/samsungapps/vlibrary2/account/ab;

    invoke-direct {v4, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ab;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;)V

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->login(Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginParam;ZLcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 72
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 73
    return-void
.end method

.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;->invokeView()V

    .line 30
    return-void
.end method

.method public invokeCompleted(Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand$PasswordConfirmView;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;->_View:Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand$PasswordConfirmView;

    .line 94
    return-void
.end method

.method public onConfirmPasswordFailed()V
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;->onFinalResult(Z)V

    .line 84
    return-void
.end method

.method public onConfirmPasswordSuccess()V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;->_ITokenBasedConfirmPasswordCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand$ITokenBasedConfirmPasswordCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand$ITokenBasedConfirmPasswordCommandData;->setPasswordConfirmedTime()V

    .line 78
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/TokenBasedConfirmPasswordCommand;->onFinalResult(Z)V

    .line 79
    return-void
.end method
