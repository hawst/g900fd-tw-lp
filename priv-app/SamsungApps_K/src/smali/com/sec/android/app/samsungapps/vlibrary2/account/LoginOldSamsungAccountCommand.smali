.class public Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginOldSamsungAccountCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;

.field private _IViewInvoker:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 14
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginOldSamsungAccountCommand;->_IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;

    .line 15
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginOldSamsungAccountCommand;->_IViewInvoker:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    .line 16
    return-void
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 2

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginOldSamsungAccountCommand;->_IViewInvoker:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginOldSamsungAccountCommand;->_Context:Landroid/content/Context;

    invoke-interface {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;->invoke(Landroid/content/Context;Ljava/lang/Object;)V

    .line 20
    return-void
.end method

.method public onSamsungAccountLoginFailed()V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginOldSamsungAccountCommand;->onFinalResult(Z)V

    .line 28
    return-void
.end method

.method public onSamsungAccountLoginSuccess()V
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginOldSamsungAccountCommand;->onFinalResult(Z)V

    .line 24
    return-void
.end method
