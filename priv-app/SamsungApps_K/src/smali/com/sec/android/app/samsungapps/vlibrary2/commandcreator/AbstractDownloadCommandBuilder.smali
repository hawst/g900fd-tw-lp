.class public abstract Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/AbstractDownloadCommandBuilder;
.super Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/BaseCommandBuilder;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand$IPurchaseCommandData;


# instance fields
.field protected _Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field protected _Context:Landroid/content/Context;

.field protected _URLContainer:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

.field private _bDownloadingCanceled:Z

.field private _bNeedRetry:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/BaseCommandBuilder;-><init>()V

    .line 21
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/AbstractDownloadCommandBuilder;->_URLContainer:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

    .line 26
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/AbstractDownloadCommandBuilder;->_Context:Landroid/content/Context;

    .line 27
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/AbstractDownloadCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 28
    return-void
.end method

.method private isStoreType()Z
    .locals 2

    .prologue
    .line 148
    const-string v0, "2"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/AbstractDownloadCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getLoadType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public checkLoadTypeForDownloadEx()Z
    .locals 1

    .prologue
    .line 154
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/AbstractDownloadCommandBuilder;->isStoreType()Z

    move-result v0

    if-nez v0, :cond_0

    .line 156
    const/4 v0, 0x1

    .line 158
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected createEasyBuy(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 38
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/EasyBuyCommand;

    invoke-direct {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/EasyBuyCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    return-object v0
.end method

.method public doesContentHasOrderID()Z
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/AbstractDownloadCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->hasOrderID()Z

    move-result v0

    return v0
.end method

.method protected getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/AbstractDownloadCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    return-object v0
.end method

.method public getContentSize()J
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/AbstractDownloadCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getRealContentSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->getSize()J

    move-result-wide v0

    return-wide v0
.end method

.method public getGUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/AbstractDownloadCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIRequestFileInfo()Lcom/sec/android/app/samsungapps/vlibrary2/download/IRequestFileInfo;
    .locals 1

    .prologue
    .line 55
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/a;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/AbstractDownloadCommandBuilder;)V

    return-object v0
.end method

.method public getLoadType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/AbstractDownloadCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getLoadType()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNeedRetry()Z
    .locals 1

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/AbstractDownloadCommandBuilder;->_bNeedRetry:Z

    return v0
.end method

.method public getProductID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/AbstractDownloadCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProductName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/AbstractDownloadCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getURLContainer()Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/AbstractDownloadCommandBuilder;->_URLContainer:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

    return-object v0
.end method

.method public isDownloadingCanceled()Z
    .locals 1

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/AbstractDownloadCommandBuilder;->_bDownloadingCanceled:Z

    return v0
.end method

.method public isFreeContent()Z
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/AbstractDownloadCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-nez v0, :cond_0

    .line 141
    const/4 v0, 0x0

    .line 143
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/AbstractDownloadCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isFreeContent()Z

    move-result v0

    goto :goto_0
.end method

.method public setDownloadingCanceled()V
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/AbstractDownloadCommandBuilder;->_bDownloadingCanceled:Z

    .line 99
    return-void
.end method

.method public setNeedRetry(Z)V
    .locals 0

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/AbstractDownloadCommandBuilder;->_bNeedRetry:Z

    .line 44
    return-void
.end method

.method public showNoAvailablePaymentMethod()V
    .locals 0

    .prologue
    .line 169
    return-void
.end method
