.class public Lcom/sec/android/app/samsungapps/vlibrary2/account/SignUpCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _SignUpActivity:Ljava/lang/Class;

.field private _bStartWithOSPID:Z


# direct methods
.method public constructor <init>(Ljava/lang/Class;Z)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/SignUpCommand;->_SignUpActivity:Ljava/lang/Class;

    .line 26
    iput-boolean p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/SignUpCommand;->_bStartWithOSPID:Z

    .line 27
    return-void
.end method


# virtual methods
.method public calcAge(III)I
    .locals 1

    .prologue
    .line 41
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AgeCalculator;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AgeCalculator;-><init>()V

    .line 42
    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AgeCalculator;->calcAge(III)I

    move-result v0

    return v0
.end method

.method public checkAgeLimitation(III)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 47
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    .line 48
    if-nez v1, :cond_1

    .line 50
    const-string v1, "error"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->err(Ljava/lang/String;)V

    .line 57
    :cond_0
    :goto_0
    return v0

    .line 53
    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SignUpCommand;->calcAge(III)I

    move-result v2

    .line 54
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getSingUpLimitationAge()I

    move-result v1

    if-le v1, v2, :cond_0

    .line 57
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAgeLimitation()I
    .locals 1

    .prologue
    .line 67
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    .line 68
    if-nez v0, :cond_0

    .line 70
    const-string v0, "error"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->err(Ljava/lang/String;)V

    .line 71
    const/4 v0, 0x0

    .line 73
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getSingUpLimitationAge()I

    move-result v0

    goto :goto_0
.end method

.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 0

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SignUpCommand;->invokeUI()V

    .line 32
    return-void
.end method

.method protected invokeUI()V
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/SignUpCommand;->_Context:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/SignUpCommand;->_SignUpActivity:Ljava/lang/Class;

    invoke-static {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->startActivityWithObject(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/Object;)V

    .line 37
    return-void
.end method

.method public isStartedWithOSPID()Z
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/SignUpCommand;->_bStartWithOSPID:Z

    return v0
.end method

.method public joinOld(Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ResultReceiver;)V
    .locals 3

    .prologue
    .line 78
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/account/aa;

    invoke-direct {v2, p0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/account/aa;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/SignUpCommand;Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ResultReceiver;)V

    invoke-virtual {v0, p1, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->joinRegisterOld(Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 85
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 86
    return-void
.end method
