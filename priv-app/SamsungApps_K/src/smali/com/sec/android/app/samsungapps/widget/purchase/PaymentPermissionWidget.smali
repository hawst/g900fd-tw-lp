.class public Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;
.super Lcom/sec/android/app/samsungapps/widget/CommonWidget;
.source "ProGuard"


# instance fields
.field a:Ljava/util/HashMap;

.field b:Landroid/widget/TextView;

.field d:Landroid/widget/TextView;

.field e:Landroid/widget/TextView;

.field f:Z

.field g:[Ljava/lang/Object;

.field private h:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

.field private i:Ljava/util/ArrayList;

.field private j:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 39
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;)V

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->i:Ljava/util/ArrayList;

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->j:Ljava/util/ArrayList;

    .line 40
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->mContext:Landroid/content/Context;

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->f:Z

    .line 42
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->d:Landroid/widget/TextView;

    .line 43
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->e:Landroid/widget/TextView;

    .line 44
    const v0, 0x7f0400a5

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->initView(Landroid/content/Context;I)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->i:Ljava/util/ArrayList;

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->j:Ljava/util/ArrayList;

    .line 64
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->mContext:Landroid/content/Context;

    .line 65
    const v0, 0x7f0400a5

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->initView(Landroid/content/Context;I)V

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->i:Ljava/util/ArrayList;

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->j:Ljava/util/ArrayList;

    .line 54
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->mContext:Landroid/content/Context;

    .line 55
    const v0, 0x7f0400a5

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->initView(Landroid/content/Context;I)V

    .line 56
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;I)V
    .locals 12

    .prologue
    .line 26
    new-instance v6, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->mContext:Landroid/content/Context;

    invoke-direct {v6, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->f:Z

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->mContext:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f040096

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const v1, 0x7f0c0217

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->mContext:Landroid/content/Context;

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->j:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v7

    const/4 v4, 0x0

    const/4 v3, 0x0

    move v5, v3

    :goto_0
    if-ge v5, v7, :cond_5

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->j:Ljava/util/ArrayList;

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;->getGroupTitle()Ljava/lang/String;

    move-result-object v8

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->i:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;->getGroupTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    const v3, 0x7f040097

    const/4 v8, 0x0

    invoke-virtual {v2, v3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    const v3, 0x7f0c0219

    invoke-virtual {v8, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->d:Landroid/widget/TextView;

    const v3, 0x7f0c021a

    invoke-virtual {v8, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->e:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->d:Landroid/widget/TextView;

    const/4 v9, 0x0

    invoke-virtual {v3, v9}, Landroid/widget/TextView;->setSingleLine(Z)V

    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->j:Ljava/util/ArrayList;

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;->getLabel()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_3

    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->j:Ljava/util/ArrayList;

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;->getPermissionID()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    const-string v3, ""

    :cond_1
    :goto_1
    iget-object v9, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->d:Landroid/widget/TextView;

    invoke-virtual {v9, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->j:Ljava/util/ArrayList;

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;->getDescription()Ljava/lang/String;

    move-result-object v3

    iget-object v9, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->e:Landroid/widget/TextView;

    if-nez v3, :cond_2

    const-string v3, ""

    :cond_2
    invoke-virtual {v9, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    if-nez v4, :cond_4

    const v3, 0x7f0c0218

    invoke-virtual {v8, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/16 v9, 0x8

    invoke-virtual {v3, v9}, Landroid/view/View;->setVisibility(I)V

    :goto_3
    invoke-virtual {v1, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v3, v4, 0x1

    :goto_4
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move v4, v3

    goto/16 :goto_0

    :cond_3
    const/4 v9, 0x0

    :try_start_1
    invoke-virtual {v3, v9}, Ljava/lang/String;->charAt(I)C

    move-result v9

    invoke-static {v9}, Ljava/lang/Character;->isLowerCase(C)Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-static {v9}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v9}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v9

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v10, v9}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v3

    goto :goto_1

    :cond_4
    const v3, 0x7f0c0218

    invoke-virtual {v8, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/4 v9, 0x0

    invoke-virtual {v3, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->i:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;->getGroupTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    invoke-virtual {v6, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setCancelable(Z)V

    invoke-virtual {v6, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->mContext:Landroid/content/Context;

    const v1, 0x7f0802ef

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;)V

    invoke-virtual {v6}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->show()V

    return-void

    :catch_0
    move-exception v3

    goto :goto_2

    :cond_6
    move v3, v4

    goto :goto_4
.end method


# virtual methods
.method public loadWidget()V
    .locals 0

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->updateWidget()V

    .line 71
    return-void
.end method

.method public refreshWidget()V
    .locals 0

    .prologue
    .line 117
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 193
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->release()V

    .line 194
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->h:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->i:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 198
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->i:Ljava/util/ArrayList;

    .line 201
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->j:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 203
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->j:Ljava/util/ArrayList;

    .line 206
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->a:Ljava/util/HashMap;

    if-eqz v0, :cond_2

    .line 207
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 208
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->a:Ljava/util/HashMap;

    .line 211
    :cond_2
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->g:[Ljava/lang/Object;

    .line 212
    return-void
.end method

.method public setPermission(Ljava/util/HashMap;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 93
    invoke-virtual {p1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 94
    invoke-interface {v0}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->g:[Ljava/lang/Object;

    move v1, v2

    .line 96
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->g:[Ljava/lang/Object;

    array-length v0, v0

    if-ge v1, v0, :cond_0

    .line 97
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->i:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->g:[Ljava/lang/Object;

    aget-object v0, v0, v1

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 98
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->j:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->g:[Ljava/lang/Object;

    aget-object v0, v0, v1

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 96
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 100
    :cond_0
    return-void
.end method

.method public setPermissionData(Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;)V
    .locals 1

    .prologue
    .line 103
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->h:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->h:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;->getPermissionList()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->a:Ljava/util/HashMap;

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->h:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;->existPermission()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->a:Ljava/util/HashMap;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->setPermission(Ljava/util/HashMap;)V

    .line 111
    :goto_0
    return-void

    .line 109
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->setVisibility(I)V

    goto :goto_0
.end method

.method public setWidgetData(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 123
    return-void
.end method

.method public updateWidget()V
    .locals 5

    .prologue
    .line 75
    const v0, 0x7f0c01fa

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->b:Landroid/widget/TextView;

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->b:Landroid/widget/TextView;

    const v1, 0x7f080347

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 77
    const v0, 0x7f0c01fb

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 79
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFocusableInTouchMode(Z)V

    .line 80
    new-instance v1, Lcom/sec/android/app/samsungapps/view/PermissionArrayAdapter;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->mContext:Landroid/content/Context;

    const v3, 0x7f0400a6

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;->i:Ljava/util/ArrayList;

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/view/PermissionArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    .line 81
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 82
    new-instance v1, Lcom/sec/android/app/samsungapps/widget/purchase/v;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/v;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentPermissionWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 89
    return-void
.end method
