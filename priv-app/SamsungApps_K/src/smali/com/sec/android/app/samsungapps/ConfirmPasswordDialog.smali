.class public Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginParam;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand$ILoginView;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/account/PasswordConfirmCommand$PasswordConfirmView;


# instance fields
.field a:Landroid/view/View;

.field b:Z

.field c:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

.field d:Lcom/sec/android/app/samsungapps/LoadingDialog;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/EditText;

.field private g:Landroid/widget/Button;

.field private h:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

.field private i:Landroid/content/Context;

.field private j:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;

.field private k:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->e:Landroid/widget/TextView;

    .line 34
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->f:Landroid/widget/EditText;

    .line 35
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->g:Landroid/widget/Button;

    .line 36
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->h:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    .line 37
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->i:Landroid/content/Context;

    .line 38
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->a:Landroid/view/View;

    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->b:Z

    .line 45
    instance-of v0, p2, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;

    if-eqz v0, :cond_1

    .line 47
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;

    iput-object p2, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->j:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->j:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;

    new-instance v1, Lcom/sec/android/app/samsungapps/ab;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/ab;-><init>(Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->addObserver(Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager$IPwordConfirmObserver;)V

    .line 75
    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->i:Landroid/content/Context;

    .line 76
    return-void

    .line 70
    :cond_1
    instance-of v0, p2, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;

    if-eqz v0, :cond_0

    .line 72
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;

    iput-object p2, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->k:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->k:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;->invokeComplete(Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand$ILoginView;)V

    goto :goto_0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->c:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    if-eqz v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->c:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->dismiss()V

    .line 301
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->c:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    .line 302
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->a()V

    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->i:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->f:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;)Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->j:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;)Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->k:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginCommand;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;)Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->h:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->g:Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method public Create()Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;
    .locals 4

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->i:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 82
    const v1, 0x7f040026

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->a:Landroid/view/View;

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->a:Landroid/view/View;

    const v1, 0x7f0c009a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->i:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080105

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->a:Landroid/view/View;

    const v1, 0x7f0c009c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->i:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080288

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->a:Landroid/view/View;

    const v1, 0x7f0c009b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->e:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->e:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->getEmailID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->a:Landroid/view/View;

    const v1, 0x7f0c009d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->f:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->f:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->f:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/samsungapps/ai;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/ai;-><init>(Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->f:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocusFromTouch()Z

    .line 85
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->i:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->c:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->c:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    const v1, 0x7f08026f

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(I)V

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->c:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setView(Landroid/view/View;)V

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->c:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setCancelable(Z)V

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->a:Landroid/view/View;

    const v1, 0x7f0c00a0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 92
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "<u>"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->i:Landroid/content/Context;

    const v3, 0x7f0801cc

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</u>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    new-instance v1, Lcom/sec/android/app/samsungapps/ac;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/ac;-><init>(Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->a:Landroid/view/View;

    const v1, 0x7f0c009e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->a:Landroid/view/View;

    const v2, 0x7f0c009f

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 120
    new-instance v2, Lcom/sec/android/app/samsungapps/ad;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/app/samsungapps/ad;-><init>(Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;Landroid/widget/CheckBox;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    new-instance v1, Lcom/sec/android/app/samsungapps/ae;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/ae;-><init>(Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->c:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->g:Landroid/widget/Button;

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->c:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->i:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0802ef

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/af;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/af;-><init>(Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->c:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->i:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08023d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/ag;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/ag;-><init>(Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setNegativeButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 182
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->c:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->h:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->h:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    new-instance v1, Lcom/sec/android/app/samsungapps/ah;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/ah;-><init>(Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->h:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->dontDismissWhenClickPositive()V

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->h:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    return-object v0
.end method

.method public getEmailID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->i:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 307
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->i:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getEmailID()Ljava/lang/String;

    move-result-object v0

    .line 309
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->f:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onInvalidEmailID()V
    .locals 3

    .prologue
    .line 261
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->i:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->i:Landroid/content/Context;

    const v2, 0x7f0802c6

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->createInfoDialog(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    move-result-object v0

    .line 262
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    .line 263
    return-void
.end method

.method public onInvalidPassword()V
    .locals 3

    .prologue
    .line 267
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->i:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->i:Landroid/content/Context;

    const v2, 0x7f0802c7

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->createInfoDialog(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    move-result-object v0

    .line 268
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    .line 269
    return-void
.end method

.method public onLoadingEnd()V
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->d:Lcom/sec/android/app/samsungapps/LoadingDialog;

    if-eqz v0, :cond_0

    .line 283
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->d:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->end()V

    .line 285
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->d:Lcom/sec/android/app/samsungapps/LoadingDialog;

    .line 286
    return-void
.end method

.method public onLoadingStart()V
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->d:Lcom/sec/android/app/samsungapps/LoadingDialog;

    if-nez v0, :cond_0

    .line 275
    new-instance v0, Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->d:Lcom/sec/android/app/samsungapps/LoadingDialog;

    .line 277
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->d:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->start()V

    .line 278
    return-void
.end method

.method public onLoginCompleted(Z)V
    .locals 0

    .prologue
    .line 290
    if-eqz p1, :cond_0

    .line 292
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->a()V

    .line 294
    :cond_0
    return-void
.end method

.method public onPasswordConfirmed(Z)V
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->c:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 247
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->c:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->dismiss()V

    .line 249
    :cond_0
    if-nez p1, :cond_1

    .line 251
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->i:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isExistSamsungAccount(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->i:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isSAccountVersionHigherThan1_3_001(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 257
    :cond_1
    :goto_0
    return-void

    .line 255
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->onInvalidPassword()V

    goto :goto_0
.end method
