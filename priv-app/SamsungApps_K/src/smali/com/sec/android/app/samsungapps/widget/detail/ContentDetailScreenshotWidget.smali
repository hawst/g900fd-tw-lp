.class public Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;
.super Landroid/widget/HorizontalScrollView;
.source "ProGuard"


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Landroid/app/Activity;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

.field private d:Z

.field private e:Landroid/content/Context;

.field private f:Landroid/widget/LinearLayout;

.field private g:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailOverviewWidgetClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0, p1}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;)V

    .line 33
    const-string v0, "ContentDetailOverviewWidget"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->a:Ljava/lang/String;

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->d:Z

    .line 57
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->e:Landroid/content/Context;

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    const-string v0, "ContentDetailOverviewWidget"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->a:Ljava/lang/String;

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->d:Z

    .line 52
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->e:Landroid/content/Context;

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    const-string v0, "ContentDetailOverviewWidget"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->a:Ljava/lang/String;

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->d:Z

    .line 47
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->e:Landroid/content/Context;

    .line 48
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;)Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailOverviewWidgetClickListener;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->g:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailOverviewWidgetClickListener;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/youtube/player/YouTubeIntents;->isYouTubeInstalled(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v4, :cond_5

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->b:Landroid/app/Activity;

    invoke-static {v0, p1, v4, v4}, Lcom/google/android/youtube/player/YouTubeIntents;->createPlayVideoIntentWithOptions(Landroid/content/Context;Ljava/lang/String;ZZ)Landroid/content/Intent;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->e:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/high16 v3, 0x10000

    invoke-virtual {v2, v0, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move-object v0, v1

    :cond_1
    :goto_0
    if-nez v0, :cond_2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const-string v1, "force_fullscreen"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_2
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->b:Landroid/app/Activity;

    if-nez v1, :cond_3

    new-instance v0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget$ScreenShotException;

    const-string v1, "Activity is NULL :: startActivity "

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget$ScreenShotException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget$ScreenShotException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget$ScreenShotException;->showMsg()V

    :goto_1
    return-void

    :cond_3
    if-nez v0, :cond_4

    :try_start_1
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget$ScreenShotException;

    const-string v1, "intent passed is NULL :: startActivity "

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget$ScreenShotException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->b:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget$ScreenShotException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :cond_5
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public getParentView()Landroid/app/Activity;
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->b:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->b:Landroid/app/Activity;

    return-object v0

    .line 72
    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget$ScreenShotException;

    const-string v1, "mParentView is null"

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget$ScreenShotException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public recycle()V
    .locals 2

    .prologue
    .line 319
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->getParentView()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0c00e2

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 320
    if-eqz v0, :cond_0

    .line 322
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V
    :try_end_0
    .catch Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget$ScreenShotException; {:try_start_0 .. :try_end_0} :catch_0

    .line 327
    :cond_0
    :goto_0
    return-void

    .line 324
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget$ScreenShotException;->showMsg()V

    goto :goto_0
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 85
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->b:Landroid/app/Activity;

    .line 86
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->e:Landroid/content/Context;

    .line 87
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->c:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    .line 88
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->g:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailOverviewWidgetClickListener;

    .line 89
    const v0, 0x7f0c00e2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 90
    if-eqz v0, :cond_0

    .line 91
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViewsInLayout()V

    .line 92
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->f:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 96
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->f:Landroid/widget/LinearLayout;

    .line 98
    :cond_1
    return-void
.end method

.method public removeScreenshot()V
    .locals 2

    .prologue
    .line 101
    const v0, 0x7f0c00e2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 102
    if-eqz v0, :cond_0

    .line 103
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViewsInLayout()V

    .line 104
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->f:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_4

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->f:Landroid/widget/LinearLayout;

    const v1, 0x7f0c013e

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 109
    if-eqz v0, :cond_1

    .line 110
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 112
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->f:Landroid/widget/LinearLayout;

    const v1, 0x7f0c013c

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 113
    if-eqz v0, :cond_2

    .line 114
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 116
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->f:Landroid/widget/LinearLayout;

    const v1, 0x7f0c0139

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 117
    if-eqz v0, :cond_3

    .line 118
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 121
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 122
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->f:Landroid/widget/LinearLayout;

    .line 124
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->recycle()V

    .line 125
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->d:Z

    .line 126
    return-void
.end method

.method public setAllAttributes(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 313
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->b:Landroid/app/Activity;

    .line 314
    return-void
.end method

.method public setListener(Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailOverviewWidgetClickListener;)V
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->g:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailOverviewWidgetClickListener;

    .line 66
    return-void
.end method

.method public setRefresh()V
    .locals 1

    .prologue
    .line 369
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->d:Z

    .line 370
    return-void
.end method

.method public setScreenShotUIOnDetailPage()V
    .locals 13

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 78
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->d:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->getParentView()Landroid/app/Activity;

    move-result-object v0

    const v2, 0x7f0c00e2

    invoke-virtual {v0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->c:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    if-nez v2, :cond_1

    .line 82
    :cond_0
    :goto_0
    return-void

    .line 78
    :cond_1
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    const-string v2, "ContentDetailOverviewWidget"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "leeyj ContentDetailScreenshotWidget screenShotLayout="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->e:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getNetAPI()Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    move-result-object v8

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->c:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVyoutubeScreenShoutUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->c:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVyoutubeUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->e:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isYouTubeSupported()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->c:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVyoutubeUrl()Ljava/lang/String;

    move-result-object v2

    move-object v5, v2

    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->c:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVyoutubeScreenShoutUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->c:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVyoutubeRtspUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->e:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isYouTubeSupported()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->c:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVyoutubeRtspUrl()Ljava/lang/String;

    move-result-object v1

    move-object v4, v1

    :goto_2
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->c:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVyoutubeScreenShoutUrl()Ljava/lang/String;

    move-result-object v6

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->e:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const v2, 0x7f040044

    const/4 v9, 0x0

    invoke-virtual {v1, v2, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->f:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->f:Landroid/widget/LinearLayout;

    const v2, 0x7f0c013f

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->f:Landroid/widget/LinearLayout;

    const v9, 0x7f0c013e

    invoke-virtual {v2, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    iget-object v9, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->e:Landroid/content/Context;

    const-string v10, "isa_samsungapps_default"

    const-string v11, "drawable"

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/samsungapps/uiutil/KnoxResourceManager;->findResource(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v1, v9}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setImageResource(I)V

    invoke-virtual {v1, v8}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setNetAPI(Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;)V

    const v9, 0x7f0201c6

    invoke-virtual {v1, v9}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setOverlayResource(I)V

    invoke-virtual {v1, v6}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setURL(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->e:Landroid/content/Context;

    const v9, 0x7f080152

    invoke-virtual {v6, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    new-instance v6, Lcom/sec/android/app/samsungapps/widget/detail/l;

    invoke-direct {v6, p0, v1, v5, v4}, Lcom/sec/android/app/samsungapps/widget/detail/l;-><init>(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;Lcom/sec/android/app/samsungapps/view/CacheWebImageView;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v6}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    move v4, v7

    :goto_3
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->c:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVscreenShotCount()I

    move-result v9

    add-int v1, v9, v4

    if-nez v1, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->getParentView()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f0c00e0

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    move v5, v3

    move v3, v4

    :goto_4
    add-int v1, v9, v4

    if-ge v3, v1, :cond_8

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->c:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    sub-int v2, v3, v4

    invoke-interface {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVscreenShotImgURL(I)Ljava/lang/String;

    move-result-object v10

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->c:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    sub-int v2, v3, v4

    invoke-interface {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVscreenShotOrientation(I)I

    move-result v1

    if-ne v1, v7, :cond_7

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->e:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const v2, 0x7f040042

    const/4 v6, 0x0

    invoke-virtual {v1, v2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->f:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->f:Landroid/widget/LinearLayout;

    const v2, 0x7f0c013a

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->f:Landroid/widget/LinearLayout;

    const v6, 0x7f0c0139

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    move-object v6, v2

    :goto_5
    add-int v2, v9, v4

    add-int/lit8 v2, v2, -0x1

    if-ne v3, v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->f:Landroid/widget/LinearLayout;

    const v11, 0x7f0c013b

    invoke-virtual {v2, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_4

    const/16 v11, 0x8

    invoke-virtual {v2, v11}, Landroid/view/View;->setVisibility(I)V

    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->e:Landroid/content/Context;

    const-string v11, "isa_samsungapps_default"

    const-string v12, "drawable"

    invoke-static {v2, v11, v12}, Lcom/sec/android/app/samsungapps/uiutil/KnoxResourceManager;->findResource(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setImageResource(I)V

    invoke-virtual {v1, v8}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setNetAPI(Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;)V

    invoke-virtual {v1, v10}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setURL(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->e:Landroid/content/Context;

    const v10, 0x7f080152

    invoke-virtual {v2, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    add-int/lit8 v2, v5, 0x1

    new-instance v10, Lcom/sec/android/app/samsungapps/widget/detail/m;

    invoke-direct {v10, p0, v5, v1}, Lcom/sec/android/app/samsungapps/widget/detail/m;-><init>(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;ILcom/sec/android/app/samsungapps/view/CacheWebImageView;)V

    invoke-virtual {v6, v10}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v5, v2

    goto/16 :goto_4

    :cond_5
    move-object v5, v1

    goto/16 :goto_1

    :cond_6
    move-object v4, v1

    goto/16 :goto_2

    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->e:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const v2, 0x7f040043

    const/4 v6, 0x0

    invoke-virtual {v1, v2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->f:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->f:Landroid/widget/LinearLayout;

    const v2, 0x7f0c013d

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->f:Landroid/widget/LinearLayout;

    const v6, 0x7f0c013c

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    move-object v6, v2

    goto :goto_5

    :cond_8
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->d:Z
    :try_end_0
    .catch Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget$ScreenShotException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 79
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget$ScreenShotException;->showMsg()V

    goto/16 :goto_0

    :cond_9
    move v4, v3

    goto/16 :goto_3
.end method

.method public setWidgetData(Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailScreenshotWidget;->c:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    .line 62
    return-void
.end method
