.class final synthetic Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/b;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field static final synthetic a:[I

.field static final synthetic b:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 42
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$State;->values()[Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$State;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/b;->b:[I

    :try_start_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/b;->b:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$State;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$State;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_6

    :goto_0
    :try_start_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/b;->b:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$State;->CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$State;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$State;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_5

    :goto_1
    :try_start_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/b;->b:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$State;->DONE:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$State;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$State;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_4

    :goto_2
    :try_start_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/b;->b:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$State;->NOT_AGREED:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$State;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$State;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    .line 45
    :goto_3
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;->values()[Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/b;->a:[I

    :try_start_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/b;->a:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;->CHECK_AGREEMENT:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_2

    :goto_4
    :try_start_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/b;->a:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;->TnCAgreed:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_1

    :goto_5
    :try_start_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/b;->a:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;->TnCNotAgreed:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_0

    :goto_6
    return-void

    :catch_0
    move-exception v0

    goto :goto_6

    :catch_1
    move-exception v0

    goto :goto_5

    :catch_2
    move-exception v0

    goto :goto_4

    :catch_3
    move-exception v0

    goto :goto_3

    :catch_4
    move-exception v0

    goto :goto_2

    :catch_5
    move-exception v0

    goto :goto_1

    :catch_6
    move-exception v0

    goto :goto_0
.end method
