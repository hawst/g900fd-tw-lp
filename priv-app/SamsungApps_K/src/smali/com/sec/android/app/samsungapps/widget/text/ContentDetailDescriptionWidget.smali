.class public Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;
.super Lcom/sec/android/app/samsungapps/widget/CommonWidget;
.source "ProGuard"


# instance fields
.field a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView$ITextSingleLineChanged;

.field b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView$ITextSingleLineChanged;

.field private final d:Ljava/lang/String;

.field private e:Landroid/content/Context;

.field private f:I

.field private g:Z

.field private h:Z

.field private i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

.field private j:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailOverviewWidgetClickListener;

.field private final k:I

.field private l:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;

.field private m:Lcom/sec/android/app/samsungapps/CustomDialogBuilder;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 41
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;)V

    .line 30
    const-string v0, "ContentDetailDescriptionWidget"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->d:Ljava/lang/String;

    .line 33
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->g:Z

    .line 34
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->h:Z

    .line 37
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->k:I

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->m:Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    .line 601
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/text/d;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/text/d;-><init>(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView$ITextSingleLineChanged;

    .line 637
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/text/e;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/text/e;-><init>(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView$ITextSingleLineChanged;

    .line 42
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->e:Landroid/content/Context;

    .line 43
    const v0, 0x7f040030

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->f:I

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->e:Landroid/content/Context;

    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->f:I

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->initView(Landroid/content/Context;I)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    const-string v0, "ContentDetailDescriptionWidget"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->d:Ljava/lang/String;

    .line 33
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->g:Z

    .line 34
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->h:Z

    .line 37
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->k:I

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->m:Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    .line 601
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/text/d;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/text/d;-><init>(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView$ITextSingleLineChanged;

    .line 637
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/text/e;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/text/e;-><init>(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView$ITextSingleLineChanged;

    .line 50
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->e:Landroid/content/Context;

    .line 51
    const v0, 0x7f040030

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->f:I

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->e:Landroid/content/Context;

    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->f:I

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->initView(Landroid/content/Context;I)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 58
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    const-string v0, "ContentDetailDescriptionWidget"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->d:Ljava/lang/String;

    .line 33
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->g:Z

    .line 34
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->h:Z

    .line 37
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->k:I

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->m:Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    .line 601
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/text/d;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/text/d;-><init>(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView$ITextSingleLineChanged;

    .line 637
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/text/e;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/text/e;-><init>(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView$ITextSingleLineChanged;

    .line 59
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->e:Landroid/content/Context;

    .line 60
    const v0, 0x7f040030

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->f:I

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->e:Landroid/content/Context;

    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->f:I

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->initView(Landroid/content/Context;I)V

    .line 63
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->e:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;Lcom/sec/android/app/samsungapps/CustomDialogBuilder;)Lcom/sec/android/app/samsungapps/CustomDialogBuilder;
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->m:Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;)V
    .locals 3

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->m:Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->e:Landroid/content/Context;

    const v2, 0x7f0802ef

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/widget/text/h;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/app/samsungapps/widget/text/h;-><init>(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setPositiveButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->m:Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->e:Landroid/content/Context;

    const v2, 0x7f08023d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/widget/text/i;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/widget/text/i;-><init>(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setNegativeButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;Z)Z
    .locals 0

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->g:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;)Lcom/sec/android/app/samsungapps/CustomDialogBuilder;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->m:Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;Z)V
    .locals 6

    .prologue
    const v5, 0x7f0c00d0

    .line 29
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->isAlreadyPurchased()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x4

    move v2, v0

    :goto_0
    const v0, 0x7f0c00d1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    const v1, 0x7f0c007a

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setSelected(Z)V

    :cond_0
    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    invoke-virtual {v1, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->setExpandState(Z)V

    if-eqz p1, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->e:Landroid/content/Context;

    const v3, 0x7f0801b2

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    const/16 v0, 0x3e8

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->setMaxLines(I)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v0, 0x3

    move v2, v0

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->e:Landroid/content/Context;

    const v4, 0x7f0801b1

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->j:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailOverviewWidgetClickListener;

    const v3, 0x7f0c00cf

    invoke-interface {v0, v3, v5}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailOverviewWidgetClickListener;->onClickExpand(II)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->setMaxLines(I)V

    goto :goto_1
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;)Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->l:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;Z)Z
    .locals 0

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->h:Z

    return p1
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;)V
    .locals 3

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->m:Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->e:Landroid/content/Context;

    const v2, 0x7f0802ef

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/widget/text/f;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/widget/text/f;-><init>(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setPositiveButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->m:Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->e:Landroid/content/Context;

    const v2, 0x7f08023d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/widget/text/g;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/widget/text/g;-><init>(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setNegativeButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    return-void
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;Z)V
    .locals 5

    .prologue
    const v4, 0x7f0c007a

    .line 29
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->isAlreadyPurchased()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    move v3, v0

    :goto_0
    const v0, 0x7f0c00e5

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;

    const v1, 0x7f0c00e6

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    if-eqz v2, :cond_0

    invoke-virtual {v2, p1}, Landroid/widget/ImageView;->setSelected(Z)V

    :cond_0
    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/view/View;->setSelected(Z)V

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->setExpandState(Z)V

    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->e:Landroid/content/Context;

    const v3, 0x7f0801b2

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->setMaxLines(I)V

    :goto_1
    const-string v0, "ContentDetailDescriptionWidget"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "whatsnewButtonState isSelected="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    const/4 v0, 0x4

    move v3, v0

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->e:Landroid/content/Context;

    const v4, 0x7f0801b1

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->setMaxLines(I)V

    goto :goto_1
.end method

.method static synthetic e(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 29
    const v0, 0x7f0c00e3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    if-eqz p1, :cond_1

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/text/c;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/app/samsungapps/widget/text/c;-><init>(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;Landroid/widget/LinearLayout;)V

    move-object v3, v1

    :goto_0
    const v1, 0x7f0c00e5

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0c00e4

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->isAlreadyPurchased()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVupdateDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_1
    return-void

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVproductDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_1
    move-object v3, v4

    goto :goto_0
.end method

.method static synthetic e(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;)Z
    .locals 1

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->g:Z

    return v0
.end method

.method static synthetic f(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 29
    const v0, 0x7f0c00ce

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    if-eqz p1, :cond_1

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/text/b;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/app/samsungapps/widget/text/b;-><init>(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;Landroid/widget/LinearLayout;)V

    move-object v3, v1

    :goto_0
    const v1, 0x7f0c00d0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0c00cf

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->isAlreadyPurchased()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVupdateDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_1
    return-void

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVproductDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_1
    move-object v3, v4

    goto :goto_0
.end method

.method static synthetic f(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;)Z
    .locals 1

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->h:Z

    return v0
.end method

.method static synthetic g(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;Z)V
    .locals 1

    .prologue
    .line 29
    const v0, 0x7f0c00d6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->l:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->l:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;->setUpdateEnable()Z

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->m:Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->m:Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->getDialog()Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->m:Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->l:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;->setUpdateDisable()Z

    goto :goto_0
.end method


# virtual methods
.method public dispalyAutoUpdate()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 261
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    if-nez v0, :cond_0

    .line 340
    :goto_0
    return-void

    .line 265
    :cond_0
    const v0, 0x7f0c00d4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 266
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->e:Landroid/content/Context;

    invoke-direct {v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;-><init>(Landroid/content/Context;)V

    .line 268
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->isLinkApp()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->l:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;->checkAutoUpdSettingVisible()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->e:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isUncStore()Z

    move-result v1

    if-ne v1, v4, :cond_2

    .line 271
    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 274
    :cond_2
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 275
    const v1, 0x7f0c00d6

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    .line 276
    const v1, 0x7f0c00d5

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 277
    invoke-virtual {v0, v4}, Landroid/widget/CheckedTextView;->setFocusable(Z)V

    .line 278
    new-instance v3, Lcom/sec/android/app/samsungapps/widget/text/a;

    invoke-direct {v3, p0, v0, v2}, Lcom/sec/android/app/samsungapps/widget/text/a;-><init>(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;Landroid/widget/CheckedTextView;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;)V

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 328
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->l:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->l:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;->isDisabled()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;->getSetting()I

    move-result v1

    if-eqz v1, :cond_3

    .line 331
    invoke-virtual {v0, v4}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto :goto_0

    .line 335
    :cond_3
    invoke-virtual {v0, v5}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto :goto_0
.end method

.method public loadWidget()V
    .locals 0

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->updateWidget()V

    .line 103
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 670
    const v0, 0x7f0c00e5

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->clearOriginLineCount()V

    const v0, 0x7f0c00d0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->clearOriginLineCount()V

    .line 671
    return-void
.end method

.method public refreshAutoUpdate()V
    .locals 3

    .prologue
    .line 246
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->e:Landroid/content/Context;

    invoke-direct {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;-><init>(Landroid/content/Context;)V

    .line 247
    const v0, 0x7f0c00d6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    .line 249
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->l:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->l:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;->isDisabled()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;->getSetting()I

    move-result v1

    if-eqz v1, :cond_0

    .line 252
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 258
    :goto_0
    return-void

    .line 256
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto :goto_0
.end method

.method public refreshWidget()V
    .locals 0

    .prologue
    .line 684
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 90
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->e:Landroid/content/Context;

    .line 91
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    .line 92
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->j:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailOverviewWidgetClickListener;

    .line 93
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView$ITextSingleLineChanged;

    .line 94
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView$ITextSingleLineChanged;

    .line 95
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->l:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;

    .line 96
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->m:Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    .line 97
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->release()V

    .line 98
    return-void
.end method

.method public setListener(Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailOverviewWidgetClickListener;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->j:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailOverviewWidgetClickListener;

    .line 85
    return-void
.end method

.method public setWidgetData(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 75
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 76
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-nez v0, :cond_1

    .line 81
    :cond_0
    :goto_0
    return-void

    .line 79
    :cond_1
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->e:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->l:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateItemSetting;

    .line 80
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailOverview()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    goto :goto_0
.end method

.method public updateWidget()V
    .locals 11

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 107
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    if-nez v0, :cond_1

    .line 114
    :cond_0
    :goto_0
    return-void

    .line 110
    :cond_1
    const v0, 0x7f0c00da

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0c00db

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0c00d9

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v3, 0x7f0c00df

    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-object v6, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v6}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVrealContentsSize()Ljava/lang/String;

    move-result-object v6

    if-eqz v0, :cond_2

    if-eqz v6, :cond_2

    const-string v7, "%s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v6, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    if-eqz v0, :cond_3

    iget-object v6, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v6}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->isLinkApp()Z

    move-result v6

    if-eqz v6, :cond_3

    const/16 v6, 0x8

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVlastUpdateDate()Ljava/lang/String;

    move-result-object v0

    if-eqz v1, :cond_4

    if-eqz v0, :cond_4

    iget-object v6, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v6}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVversion()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_a

    iget-object v6, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v6}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVversion()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_a

    const-string v6, "%s:%s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->e:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f080294

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    iget-object v9, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v9}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVversion()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->e:Landroid/content/Context;

    invoke-static {v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/util/AppsDateFormat;->getSystemDateItem(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    :goto_1
    if-eqz v3, :cond_b

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->isIAPSupported()Z

    move-result v0

    if-eqz v0, :cond_b

    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->dispalyAutoUpdate()V

    const v0, 0x7f0c00dc

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_8

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVrestrictedAge()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->hasGradeImg()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->e:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v2

    if-ne v2, v4, :cond_c

    :cond_5
    if-eqz v1, :cond_8

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->isAllAge()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->e:Landroid/content/Context;

    const v2, 0x7f08027a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->e:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isKorea()Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v2, "18+"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v1, "19+"

    :cond_7
    const-string v2, "%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_8
    :goto_3
    const v0, 0x7f0c00cf

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0c00d0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;

    if-eqz v1, :cond_9

    const v2, 0x7f0c00d1

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->isAlreadyPurchased()Z

    move-result v6

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVupdateDescription()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_17

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVupdateDescription()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_17

    move v3, v4

    :goto_4
    if-eqz v6, :cond_18

    if-eqz v3, :cond_18

    const/4 v3, 0x4

    :goto_5
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->g:Z

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->clearData()V

    const v7, 0x7f0c007a

    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iget-boolean v7, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->g:Z

    invoke-virtual {v2, v7}, Landroid/view/View;->setSelected(Z)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVproductDescription()Ljava/lang/String;

    if-eqz v6, :cond_1a

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVupdateDescription()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_19

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVupdateDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_19

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVupdateDescription()Ljava/lang/String;

    move-result-object v2

    iget-object v6, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->e:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0801f2

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v0, v2

    :goto_6
    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1b

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView$ITextSingleLineChanged;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->setOnTextSingleLineChangedListener(Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView$ITextSingleLineChanged;)V

    invoke-virtual {v1, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->setMaxLineCount(I)V

    const v0, 0x7f0c00d7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_9
    :goto_7
    const v0, 0x7f0c00e5

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;

    if-eqz v0, :cond_0

    const v1, 0x7f0c00e4

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0c00e6

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->isAlreadyPurchased()Z

    move-result v6

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVupdateDescription()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1c

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVupdateDescription()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1c

    move v3, v4

    :goto_8
    if-eqz v6, :cond_1d

    if-eqz v3, :cond_1d

    const/4 v3, 0x3

    :goto_9
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->h:Z

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->clearData()V

    const v4, 0x7f0c007a

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iget-boolean v4, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->g:Z

    invoke-virtual {v2, v4}, Landroid/view/View;->setSelected(Z)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVupdateDescription()Ljava/lang/String;

    move-result-object v2

    if-eqz v6, :cond_1f

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v4}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVupdateDescription()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1e

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v4}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVupdateDescription()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_1e

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVproductDescription()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->e:Landroid/content/Context;

    const v5, 0x7f080272

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v1, v2

    :goto_a
    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_20

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView$ITextSingleLineChanged;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->setOnTextSingleLineChangedListener(Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView$ITextSingleLineChanged;)V

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->setMaxLineCount(I)V

    const v0, 0x7f0c00d8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V
    :try_end_0
    .catch Lcom/sec/android/app/samsungapps/widget/text/j; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 111
    :catch_0
    move-exception v0

    .line 112
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ContentDetailDescriptionWidget::DescriptionException::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/text/j;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 110
    :cond_a
    const/16 v6, 0x8

    :try_start_1
    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    const-string v2, "%s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->e:Landroid/content/Context;

    invoke-static {v8, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/util/AppsDateFormat;->getSystemDateItem(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v7

    invoke-static {v2, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_b
    const/16 v0, 0x8

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    :cond_c
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVnameAuth()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVcontentGradeImgUrl()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_d

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isBrazill()Z

    move-result v0

    if-eqz v0, :cond_d

    const v0, 0x7f0c00dc

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    const v0, 0x7f0c00de

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->e:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getNetAPI()Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setNetAPI(Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;)V

    if-eqz v3, :cond_8

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_8

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setVisibility(I)V

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setURL(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_d
    if-eqz v2, :cond_8

    if-eqz v1, :cond_8

    const v0, 0x7f0c00dc

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    const v0, 0x7f0c00de

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setVisibility(I)V

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->e:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isKorea()Z

    move-result v3

    if-eqz v3, :cond_8

    const-string v3, "V"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_12

    const-string v2, "12+"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    const v1, 0x7f020019

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setImageResource(I)V

    goto/16 :goto_3

    :cond_e
    const-string v2, "15+"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    const v1, 0x7f02001a

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setImageResource(I)V

    goto/16 :goto_3

    :cond_f
    const-string v2, "18+"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    const-string v2, "19+"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    :cond_10
    const v1, 0x7f02001b

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setImageResource(I)V

    goto/16 :goto_3

    :cond_11
    const v1, 0x7f02001c

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setImageResource(I)V

    goto/16 :goto_3

    :cond_12
    const-string v3, "Y"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v2, "12+"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    const v1, 0x7f020014

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setImageResource(I)V

    goto/16 :goto_3

    :cond_13
    const-string v2, "15+"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    const v1, 0x7f020015

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setImageResource(I)V

    goto/16 :goto_3

    :cond_14
    const-string v2, "18+"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    const-string v2, "19+"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    :cond_15
    const v1, 0x7f020016

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setImageResource(I)V

    goto/16 :goto_3

    :cond_16
    const v1, 0x7f020017

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setImageResource(I)V

    goto/16 :goto_3

    :cond_17
    move v3, v5

    goto/16 :goto_4

    :cond_18
    const/4 v3, 0x3

    goto/16 :goto_5

    :cond_19
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVproductDescription()Ljava/lang/String;

    move-result-object v2

    iget-object v6, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->e:Landroid/content/Context;

    const v7, 0x7f080272

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0c00d8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v6, 0x8

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    move-object v0, v2

    goto/16 :goto_6

    :cond_1a
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVproductDescription()Ljava/lang/String;

    move-result-object v2

    iget-object v6, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->e:Landroid/content/Context;

    const v7, 0x7f080272

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v0, v2

    goto/16 :goto_6

    :cond_1b
    const v0, 0x7f0c00d7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_7

    :cond_1c
    move v3, v5

    goto/16 :goto_8

    :cond_1d
    const/4 v3, 0x4

    goto/16 :goto_9

    :cond_1e
    const v1, 0x7f0c00d8

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v4, 0x8

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    move-object v1, v2

    goto/16 :goto_a

    :cond_1f
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVupdateDescription()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->e:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0801f2

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v1, v2

    goto/16 :goto_a

    :cond_20
    const v0, 0x7f0c00d8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailDescriptionWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V
    :try_end_1
    .catch Lcom/sec/android/app/samsungapps/widget/text/j; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
