.class public Lcom/sec/android/app/samsungapps/commands/PPCCommandBuilder;
.super Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/AbsPPCCommandBuilder;
.source "ProGuard"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/AbsPPCCommandBuilder;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Ljava/lang/Object;)V

    .line 26
    return-void
.end method

.method static synthetic a(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 20
    const-string v0, ""

    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;->getCardBalanceInfo()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PrepaidCardBalance;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PrepaidCardBalance;->getBalance()D

    move-result-wide v0

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;->getPurchaseInfo()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->currencyUnit:Ljava/lang/String;

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v3

    invoke-virtual {v3, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getFormattedPrice(DLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
    .locals 1

    .prologue
    .line 77
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/bv;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/bv;-><init>(Lcom/sec/android/app/samsungapps/commands/PPCCommandBuilder;)V

    return-object v0
.end method

.method public getConfirmViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
    .locals 1

    .prologue
    .line 41
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/bs;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/bs;-><init>(Lcom/sec/android/app/samsungapps/commands/PPCCommandBuilder;)V

    return-object v0
.end method

.method public getRegisterPPCViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/br;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/br;-><init>(Lcom/sec/android/app/samsungapps/commands/PPCCommandBuilder;)V

    return-object v0
.end method
