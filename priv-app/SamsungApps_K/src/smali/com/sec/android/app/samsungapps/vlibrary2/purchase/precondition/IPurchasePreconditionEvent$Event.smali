.class public final enum Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;

.field public static final enum CHECK_NOT_USA:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;

.field public static final enum CHECK_USA_CARDEXIST:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;

.field public static final enum CHECK_USA_CARD_NOT_EXIST:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;

.field public static final enum REG_CARD_FAILURE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;

.field public static final enum REG_CARD_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;

.field public static final enum USER_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;

.field public static final enum USER_OK:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 6
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;

    const-string v1, "CHECK_USA_CARDEXIST"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;->CHECK_USA_CARDEXIST:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;

    .line 7
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;

    const-string v1, "CHECK_USA_CARD_NOT_EXIST"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;->CHECK_USA_CARD_NOT_EXIST:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;

    .line 8
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;

    const-string v1, "CHECK_NOT_USA"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;->CHECK_NOT_USA:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;

    .line 9
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;

    const-string v1, "USER_CANCEL"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;->USER_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;

    .line 10
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;

    const-string v1, "USER_OK"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;->USER_OK:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;

    .line 11
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;

    const-string v1, "REG_CARD_FAILURE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;->REG_CARD_FAILURE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;

    .line 12
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;

    const-string v1, "REG_CARD_SUCCESS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;->REG_CARD_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;

    .line 4
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;->CHECK_USA_CARDEXIST:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;->CHECK_USA_CARD_NOT_EXIST:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;->CHECK_NOT_USA:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;->USER_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;->USER_OK:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;->REG_CARD_FAILURE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;->REG_CARD_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;
    .locals 1

    .prologue
    .line 4
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;
    .locals 1

    .prologue
    .line 4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;

    return-object v0
.end method
