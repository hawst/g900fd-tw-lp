.class final Lcom/sec/android/app/samsungapps/widget/detail/e;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage$IDeletePackageObserver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;)V
    .locals 0

    .prologue
    .line 685
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/e;->a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDeleteFailed()V
    .locals 3

    .prologue
    .line 706
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/e;->a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;)Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/e;->a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 708
    :cond_0
    const-string v0, "context is null or activity is finishing"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 719
    :cond_1
    :goto_0
    return-void

    .line 712
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/e;->a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    const v1, 0x7f0c00b1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 713
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/e;->a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->c(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;)Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 715
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/e;->a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/e;->a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;)Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVProductName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/e;->a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->b(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;)Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;->getContentDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVGUID()Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;Ljava/lang/String;)V

    .line 717
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/e;->a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->c(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;)Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->onDetailLoaded()V

    goto :goto_0
.end method

.method public final onDeleteSuccess()V
    .locals 2

    .prologue
    .line 690
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/e;->a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;)Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/e;->a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->a(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 692
    :cond_0
    const-string v0, "context is null or activity is finishing"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 701
    :cond_1
    :goto_0
    return-void

    .line 696
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/e;->a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    const v1, 0x7f0c00b1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 697
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/e;->a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->c(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;)Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 699
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/e;->a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->c(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;)Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->onDetailLoaded()V

    goto :goto_0
.end method
