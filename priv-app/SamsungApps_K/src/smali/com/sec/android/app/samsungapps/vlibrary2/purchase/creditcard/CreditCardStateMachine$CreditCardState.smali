.class public final enum Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

.field public static final enum BEGIN:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

.field public static final enum INVOKE_VIEW_STATE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

.field public static final enum READY:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

.field public static final enum RECEIVED_CARDSORT_STATE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

.field public static final enum RECIVED_STATELIST_STATE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

.field public static final enum REGISTERFAIL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

.field public static final enum REGISTERING:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

.field public static final enum SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

.field public static final enum WAITING_USER_SUBMIT:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 182
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    const-string v1, "BEGIN"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;->BEGIN:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    .line 183
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    const-string v1, "READY"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;->READY:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    .line 184
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    const-string v1, "REGISTERFAIL"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;->REGISTERFAIL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    .line 185
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    const-string v1, "RECEIVED_CARDSORT_STATE"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;->RECEIVED_CARDSORT_STATE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    .line 186
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    const-string v1, "RECIVED_STATELIST_STATE"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;->RECIVED_STATELIST_STATE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    .line 187
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    const-string v1, "INVOKE_VIEW_STATE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;->INVOKE_VIEW_STATE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    const-string v1, "WAITING_USER_SUBMIT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;->WAITING_USER_SUBMIT:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    const-string v1, "REGISTERING"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;->REGISTERING:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    const-string v1, "SUCCESS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    .line 180
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;->BEGIN:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;->READY:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;->REGISTERFAIL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;->RECEIVED_CARDSORT_STATE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;->RECIVED_STATELIST_STATE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;->INVOKE_VIEW_STATE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;->WAITING_USER_SUBMIT:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;->REGISTERING:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 180
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;
    .locals 1

    .prologue
    .line 180
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;
    .locals 1

    .prologue
    .line 180
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    return-object v0
.end method
