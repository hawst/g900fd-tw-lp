.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/childs/PurchaseListXml;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;
.source "ProGuard"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;I)V
    .locals 8

    .prologue
    const/4 v6, 0x1

    .line 11
    const-string v3, "purchaseList"

    const-string v4, "2140"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v5, p3

    move v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/samsungapps/vlibrary/xml/IListRequestXml;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 13
    invoke-virtual {p0, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/childs/PurchaseListXml;->addListStEdAsParam(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;)V

    .line 14
    invoke-virtual {p0, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/childs/PurchaseListXml;->addImageSizeAsParam(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IProductListParam;)V

    .line 15
    const-string v0, "imei"

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getIMEI()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/childs/PurchaseListXml;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    const-string v0, "contentType"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->getContentType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/childs/PurchaseListXml;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getSortOrder()Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 19
    const-string v0, "alignOrder"

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getSortOrder()Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/childs/PurchaseListXml;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    :goto_0
    return-void

    .line 23
    :cond_0
    const-string v0, "alignOrder"

    const-string v1, "recent"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/childs/PurchaseListXml;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
