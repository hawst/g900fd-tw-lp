.class public Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/RequestDisclaimerCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field _Disclaimer:Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;

.field private _IRequestDisclaimerCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/RequestDisclaimerCommand$IRequestDisclaimerCommandData;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/RequestDisclaimerCommand$IRequestDisclaimerCommandData;)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 16
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/RequestDisclaimerCommand;->_Disclaimer:Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;

    .line 19
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/RequestDisclaimerCommand;->_IRequestDisclaimerCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/RequestDisclaimerCommand$IRequestDisclaimerCommandData;

    .line 20
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/RequestDisclaimerCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/RequestDisclaimerCommand$IRequestDisclaimerCommandData;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/RequestDisclaimerCommand;->_IRequestDisclaimerCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/RequestDisclaimerCommand$IRequestDisclaimerCommandData;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/RequestDisclaimerCommand;Z)V
    .locals 0

    .prologue
    .line 14
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/RequestDisclaimerCommand;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 0

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/RequestDisclaimerCommand;->requestDisclaimer()V

    .line 24
    return-void
.end method

.method public requestDisclaimer()V
    .locals 3

    .prologue
    .line 28
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/RequestDisclaimerCommand;->_Disclaimer:Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/a;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/RequestDisclaimerCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->termInformation_TNC(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 36
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 37
    return-void
.end method
