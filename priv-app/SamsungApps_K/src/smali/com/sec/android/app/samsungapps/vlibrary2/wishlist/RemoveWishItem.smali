.class public Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

.field private _IRemoveWishItemData:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem$IRemoveWishItemData;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem$IRemoveWishItemData;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;->_IRemoveWishItemData:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem$IRemoveWishItemData;

    .line 24
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem$IRemoveWishItemData;Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;->_IRemoveWishItemData:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem$IRemoveWishItemData;

    .line 30
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    .line 31
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;)Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;)Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem$IRemoveWishItemData;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;->_IRemoveWishItemData:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem$IRemoveWishItemData;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;Z)V
    .locals 0

    .prologue
    .line 17
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;Z)V
    .locals 0

    .prologue
    .line 17
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 3

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    if-eqz v0, :cond_0

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->startLoading()V

    .line 40
    :cond_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;->_IRemoveWishItemData:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem$IRemoveWishItemData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem$IRemoveWishItemData;->getWishListID()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/d;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->deleteWishList(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 63
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 65
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 66
    return-void
.end method
