.class final Lcom/sec/android/app/samsungapps/magazinehome/a;
.super Landroid/os/Handler;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/magazinehome/a;->a:Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 68
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/a;->a:Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->a(Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v0

    if-nez v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/a;->a:Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->stopService()V

    .line 114
    :goto_0
    return-void

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/a;->a:Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->a(Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/magazinehome/b;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/magazinehome/b;-><init>(Lcom/sec/android/app/samsungapps/magazinehome/a;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/magazinehome/a;->a:Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->b(Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->requestDataGet(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;Landroid/content/Context;)V

    goto :goto_0

    .line 102
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/a;->a:Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->a(Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v0

    .line 105
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 106
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/a;->a:Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->stopService()V

    goto :goto_0

    .line 109
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/a;->a:Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->sendStoryItems()V

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/a;->a:Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->stopService()V

    goto :goto_0

    .line 113
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/a;->a:Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->stopService()V

    goto :goto_0
.end method
