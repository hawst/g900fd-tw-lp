.class public Lcom/sec/android/app/samsungapps/view/SearchArrayAdapter;
.super Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;
.source "ProGuard"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;)V
    .locals 2

    .prologue
    .line 14
    const v0, 0x7f040010

    const/16 v1, 0x63

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;-><init>(Landroid/content/Context;II)V

    .line 15
    invoke-virtual {p0, p2}, Lcom/sec/android/app/samsungapps/view/SearchArrayAdapter;->setQuery(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;)V

    .line 16
    return-void
.end method


# virtual methods
.method protected createViewHolder(Landroid/view/View;)Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;
    .locals 9

    .prologue
    .line 21
    new-instance v0, Lcom/sec/android/app/samsungapps/viewholder/SearchContentListViewHolder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/SearchArrayAdapter;->_ListViewInfo:Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;

    sget-object v2, Lcom/sec/android/app/samsungapps/view/SearchArrayAdapter;->mContext:Landroid/content/Context;

    sget-object v4, Lcom/sec/android/app/samsungapps/view/SearchArrayAdapter;->defaultImage:Landroid/graphics/drawable/Drawable;

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/view/SearchArrayAdapter;->defaultPrice:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/SearchArrayAdapter;->isSingleColumn()Z

    move-result v6

    iget v7, p0, Lcom/sec/android/app/samsungapps/view/SearchArrayAdapter;->mListType:I

    move-object v3, p1

    move-object v8, p0

    invoke-direct/range {v0 .. v8}, Lcom/sec/android/app/samsungapps/viewholder/SearchContentListViewHolder;-><init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;Landroid/content/Context;Landroid/view/View;Landroid/graphics/drawable/Drawable;Ljava/lang/String;ZILcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V

    return-object v0
.end method

.method public isSearchList()Z
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x1

    return v0
.end method

.method protected isSingleColumn()Z
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x1

    return v0
.end method
