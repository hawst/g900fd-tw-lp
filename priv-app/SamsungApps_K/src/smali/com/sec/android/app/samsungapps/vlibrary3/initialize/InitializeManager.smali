.class public Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$State;

.field private b:Landroid/content/Context;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager$InitializeManagerResultListener;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$State;

    .line 27
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;->b:Landroid/content/Context;

    .line 28
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    .line 29
    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;)V
    .locals 1

    .prologue
    .line 33
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManagerStateMachine;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManagerStateMachine;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManagerStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;)Z

    .line 34
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;)V

    return-void
.end method


# virtual methods
.method public execute(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager$InitializeManagerResultListener;)V
    .locals 1

    .prologue
    .line 38
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager$InitializeManagerResultListener;

    .line 39
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;)V

    .line 40
    return-void
.end method

.method public getState()Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$State;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$State;

    return-object v0
.end method

.method public bridge synthetic getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;->getState()Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$State;

    move-result-object v0

    return-object v0
.end method

.method public onAction(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;)V
    .locals 2

    .prologue
    .line 54
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/f;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 72
    :cond_0
    :goto_0
    return-void

    .line 57
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->Is3GAvailable(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->IsWifiAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;->NETWORK_STATE_OK:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;)V

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;->NETWORK_STATE_NOT_OK:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;)V

    goto :goto_0

    .line 60
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->needUpdate()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;->NEED_COUNTRY_SEARCH:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;)V

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;->COUNTRY_INFO_OK:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;)V

    goto :goto_0

    .line 63
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager$InitializeManagerResultListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager$InitializeManagerResultListener;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager$InitializeManagerResultListener;->onInitializeFailed()V

    goto :goto_0

    .line 66
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager$InitializeManagerResultListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager$InitializeManagerResultListener;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager$InitializeManagerResultListener;->onInitializeSuccess()V

    goto :goto_0

    .line 69
    :pswitch_4
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    if-nez v0, :cond_4

    const-string v0, "error"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->err(Ljava/lang/String;)V

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;->COUNTRY_SEARCH_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;)V

    goto :goto_0

    :cond_4
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/e;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/e;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->requestContryInfo(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    goto :goto_0

    .line 54
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public bridge synthetic onAction(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;->onAction(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;)V

    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$State;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$State;

    .line 45
    return-void
.end method

.method public bridge synthetic setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$State;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$State;)V

    return-void
.end method
