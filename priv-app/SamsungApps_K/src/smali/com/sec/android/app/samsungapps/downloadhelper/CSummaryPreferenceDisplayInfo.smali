.class public Lcom/sec/android/app/samsungapps/downloadhelper/CSummaryPreferenceDisplayInfo;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/ISummaryPreferenceDisplayInfo;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_Normal:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/downloadhelper/CSummaryPreferenceDisplayInfo;->b:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    .line 18
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/downloadhelper/CSummaryPreferenceDisplayInfo;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 19
    return-void
.end method


# virtual methods
.method public averageRating()I
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadhelper/CSummaryPreferenceDisplayInfo;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    iget v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->averageRating:I

    return v0
.end method

.method public getCurrencyUnit()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadhelper/CSummaryPreferenceDisplayInfo;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getVCurrencyUnit()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPaymentPrice()D
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadhelper/CSummaryPreferenceDisplayInfo;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getPaymentPrice()D

    move-result-wide v0

    return-wide v0
.end method

.method public productImageURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadhelper/CSummaryPreferenceDisplayInfo;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getVProductImgUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public productName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadhelper/CSummaryPreferenceDisplayInfo;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getVProductName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public sellerName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadhelper/CSummaryPreferenceDisplayInfo;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getVSellerName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setPaymentMethodType(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;)V
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/downloadhelper/CSummaryPreferenceDisplayInfo;->b:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    .line 29
    return-void
.end method
