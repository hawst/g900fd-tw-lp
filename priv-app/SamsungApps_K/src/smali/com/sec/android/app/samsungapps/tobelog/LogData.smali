.class public Lcom/sec/android/app/samsungapps/tobelog/LogData;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:J

.field private d:Lcom/sec/android/app/samsungapps/tobelog/ILogPage;

.field private e:Lcom/sec/android/app/samsungapps/tobelog/ILogEvent;

.field private f:Ljava/lang/String;

.field private g:Lcom/sec/android/app/samsungapps/tobelog/ILogBody;

.field private h:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/tobelog/ILogPage;Lcom/sec/android/app/samsungapps/tobelog/ILogEvent;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/tobelog/ILogBody;)V
    .locals 2

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogData;->a:Ljava/lang/String;

    .line 22
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/tobelog/LogData;->b:Ljava/lang/String;

    .line 23
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/tobelog/LogData;->d:Lcom/sec/android/app/samsungapps/tobelog/ILogPage;

    .line 24
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/tobelog/LogData;->e:Lcom/sec/android/app/samsungapps/tobelog/ILogEvent;

    .line 25
    iput-object p5, p0, Lcom/sec/android/app/samsungapps/tobelog/LogData;->f:Ljava/lang/String;

    .line 26
    iput-object p7, p0, Lcom/sec/android/app/samsungapps/tobelog/LogData;->g:Lcom/sec/android/app/samsungapps/tobelog/ILogBody;

    .line 27
    iput-object p6, p0, Lcom/sec/android/app/samsungapps/tobelog/LogData;->h:Ljava/lang/String;

    .line 28
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogData;->c:J

    .line 30
    return-void
.end method


# virtual methods
.method final a()V
    .locals 4

    .prologue
    .line 102
    const-string v0, "SVC : %s, Type : %s, Activity: %s, UserId : %s, SpecVersion : %s"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogData;->getSvcCode()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogData;->getLogType()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogData;->getActivity()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogData;->getUserId()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogData;->getSpecVer()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V

    .line 105
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Body : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogData;->getBodyString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/sec/android/app/samsungapps/tobelog/LogBodyDataException; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public getActivity()Ljava/lang/String;
    .locals 2

    .prologue
    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogData;->d:Lcom/sec/android/app/samsungapps/tobelog/ILogPage;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/tobelog/ILogPage;->value()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogData;->e:Lcom/sec/android/app/samsungapps/tobelog/ILogEvent;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/tobelog/ILogEvent;->value()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBody()Lcom/sec/android/app/samsungapps/tobelog/ILogBody;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogData;->g:Lcom/sec/android/app/samsungapps/tobelog/ILogBody;

    return-object v0
.end method

.method public getBodyString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogData;->g:Lcom/sec/android/app/samsungapps/tobelog/ILogBody;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogData;->e:Lcom/sec/android/app/samsungapps/tobelog/ILogEvent;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/ILogBody;->toString(Lcom/sec/android/app/samsungapps/tobelog/ILogEvent;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDataSize()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 77
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogData;->getSvcCode()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit8 v2, v0, 0x0

    .line 81
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogData;->getLogType()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    .line 82
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogData;->getActivity()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v2, v0

    .line 83
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogData;->getUserId()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    .line 84
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogData;->getSpecVer()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 86
    add-int/lit8 v0, v0, 0x8

    .line 88
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogData;->getBodyString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    array-length v1, v2
    :try_end_1
    .catch Lcom/sec/android/app/samsungapps/tobelog/LogBodyDataException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    add-int/2addr v1, v0

    .line 98
    :goto_5
    return v1

    .line 77
    :cond_0
    :try_start_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogData;->getSvcCode()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    array-length v0, v0

    goto :goto_0

    .line 81
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogData;->getLogType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    array-length v0, v0

    goto :goto_1

    .line 82
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogData;->getActivity()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    array-length v0, v0

    goto :goto_2

    .line 83
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogData;->getUserId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    array-length v0, v0

    goto :goto_3

    .line 84
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogData;->getSpecVer()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    array-length v0, v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_4

    :catch_0
    move-exception v1

    move v1, v0

    .line 96
    goto :goto_5

    .line 92
    :catch_1
    move-exception v0

    .line 93
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception on LogData.getDataSize() "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V

    .line 94
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5
.end method

.method public getEventId()Lcom/sec/android/app/samsungapps/tobelog/ILogEvent;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogData;->e:Lcom/sec/android/app/samsungapps/tobelog/ILogEvent;

    return-object v0
.end method

.method public getLogType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogData;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getPageId()Lcom/sec/android/app/samsungapps/tobelog/ILogPage;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogData;->d:Lcom/sec/android/app/samsungapps/tobelog/ILogPage;

    return-object v0
.end method

.method public getSpecVer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogData;->h:Ljava/lang/String;

    return-object v0
.end method

.method public getSvcCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogData;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getTimeStamp()J
    .locals 2

    .prologue
    .line 41
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogData;->c:J

    return-wide v0
.end method

.method public getUserId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogData;->f:Ljava/lang/String;

    return-object v0
.end method

.method public setBody(Lcom/sec/android/app/samsungapps/tobelog/ILogBody;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogData;->g:Lcom/sec/android/app/samsungapps/tobelog/ILogBody;

    .line 62
    return-void
.end method
