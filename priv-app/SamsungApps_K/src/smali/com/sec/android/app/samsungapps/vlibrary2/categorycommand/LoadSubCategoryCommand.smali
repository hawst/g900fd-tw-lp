.class public Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/LoadSubCategoryCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _Category:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/LoadSubCategoryCommand;->_Category:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    .line 21
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/LoadSubCategoryCommand;Z)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/LoadSubCategoryCommand;->onReceiveCategoryList(Z)V

    return-void
.end method

.method private onReceiveCategoryList(Z)V
    .locals 0

    .prologue
    .line 48
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/LoadSubCategoryCommand;->onFinalResult(Z)V

    .line 49
    return-void
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 25
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/LoadSubCategoryCommand;->_Category:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->isComplete()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 27
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/LoadSubCategoryCommand;->onFinalResult(Z)V

    .line 44
    :goto_0
    return-void

    .line 30
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/LoadSubCategoryCommand;->_Category:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->getChildCount()I

    move-result v0

    if-eqz v0, :cond_1

    .line 32
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/LoadSubCategoryCommand;->onFinalResult(Z)V

    goto :goto_0

    .line 35
    :cond_1
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/LoadSubCategoryCommand;->_Category:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/d;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/LoadSubCategoryCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->categoryListSearch(Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 42
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/LoadSubCategoryCommand;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 43
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    goto :goto_0
.end method
