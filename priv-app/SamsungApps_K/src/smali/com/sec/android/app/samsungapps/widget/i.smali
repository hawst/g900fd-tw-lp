.class final Lcom/sec/android/app/samsungapps/widget/i;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

.field final synthetic b:Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/i;->b:Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;

    iput-object p2, p0, Lcom/sec/android/app/samsungapps/widget/i;->a:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCommandResult(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/i;->a:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/i;->b:Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->a:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 126
    :cond_0
    const-string v0, "AboutWidgetHelper::_onUpdateCheckCommandReceiver::Receiver is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 141
    :goto_0
    return-void

    .line 130
    :cond_1
    if-eq p1, v3, :cond_2

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/i;->b:Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    .line 133
    if-eqz v0, :cond_2

    .line 135
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeSamsungAppsUpdate(Z)V

    .line 136
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v0

    const-string v1, ""

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->setLatestSamsungAppsVersion(Ljava/lang/String;)V

    .line 140
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/i;->a:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    const/4 v1, 0x0

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

    invoke-direct {v2}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;-><init>()V

    invoke-interface {v0, v1, v3, v2}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;->onReceiveResult(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V

    goto :goto_0
.end method
