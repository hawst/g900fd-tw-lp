.class public Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;
.super Ljava/util/ArrayList;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/baselist/CalcListStEd$ICalcListInfo;
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x6b221b8451805edeL


# instance fields
.field protected _CalcListStEd:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/CalcListStEd;

.field protected _ListHeaderResponse:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 19
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/CalcListStEd;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/CalcListStEd;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->_CalcListStEd:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/CalcListStEd;

    .line 20
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->_ListHeaderResponse:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;

    .line 21
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 25
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/CalcListStEd;

    invoke-direct {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/CalcListStEd;-><init>(II)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->_CalcListStEd:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/CalcListStEd;

    .line 26
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->_ListHeaderResponse:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;

    .line 27
    return-void
.end method


# virtual methods
.method public append(Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;)V
    .locals 2

    .prologue
    .line 87
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->isIntersect(Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 101
    :cond_0
    :goto_0
    return-void

    .line 91
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->_ListHeaderResponse:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->getEndNumber()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->setEndNumber(I)V

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->_ListHeaderResponse:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;

    iget-object v1, p1, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->_ListHeaderResponse:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->append(Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;)V

    .line 93
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 95
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 97
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->_ListHeaderResponse:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->clear()V

    .line 111
    invoke-super {p0}, Ljava/util/ArrayList;->clear()V

    .line 112
    return-void
.end method

.method public getEndNumber()I
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->_ListHeaderResponse:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->getEndNumber()I

    move-result v0

    return v0
.end method

.method public getListHeaderResponse()Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->_ListHeaderResponse:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;

    return-object v0
.end method

.method public getNextEndNumber()I
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->_CalcListStEd:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/CalcListStEd;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/CalcListStEd;->getNextEndNumber(Lcom/sec/android/app/samsungapps/vlibrary2/baselist/CalcListStEd$ICalcListInfo;)I

    move-result v0

    return v0
.end method

.method public getNextStartNumber()I
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->_CalcListStEd:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/CalcListStEd;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/CalcListStEd;->getNextStartNumber(Lcom/sec/android/app/samsungapps/vlibrary2/baselist/CalcListStEd$ICalcListInfo;)I

    move-result v0

    return v0
.end method

.method public getStartNumber()I
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->_ListHeaderResponse:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->getStartNumber()I

    move-result v0

    return v0
.end method

.method public getTotalCount()I
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->_ListHeaderResponse:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->getTotalCount()I

    move-result v0

    return v0
.end method

.method public getTotalCount2()I
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->_ListHeaderResponse:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->getTotalCount2()I

    move-result v0

    return v0
.end method

.method public isEOF()Z
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->_ListHeaderResponse:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->isEof()Z

    move-result v0

    return v0
.end method

.method protected isIntersect(Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;)Z
    .locals 2

    .prologue
    .line 104
    iget-object v0, p1, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->_ListHeaderResponse:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->_ListHeaderResponse:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->isIntersect(Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;)Z

    move-result v0

    return v0
.end method

.method public isTotalCount2Valid()Z
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->_ListHeaderResponse:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->isTotalCount2Valid()Z

    move-result v0

    return v0
.end method

.method public isTotalCountValid()Z
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->_ListHeaderResponse:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->isTotalCountValid()Z

    move-result v0

    return v0
.end method

.method public setHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->_ListHeaderResponse:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->setHeaderResponse(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 37
    return-void
.end method
