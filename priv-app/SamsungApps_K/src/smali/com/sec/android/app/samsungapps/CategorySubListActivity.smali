.class public Lcom/sec/android/app/samsungapps/CategorySubListActivity;
.super Lcom/sec/android/app/samsungapps/ActionBarActivity;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/view/CategoryItemListView$ICategoryData;


# static fields
.field public static final EXTRA_DEEPLINK_CATEGORYID:Ljava/lang/String; = "SubCategoryContentId"

.field public static final EXTRA_TITLETEXT:Ljava/lang/String; = "SubCategoryTitle"


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/view/CategoryItemListView;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->c:Ljava/lang/String;

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->a:Lcom/sec/android/app/samsungapps/view/CategoryItemListView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->c:Ljava/lang/String;

    new-instance v2, Lcom/sec/android/app/samsungapps/p;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/p;-><init>(Lcom/sec/android/app/samsungapps/CategorySubListActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->load(Ljava/lang/String;Lcom/sec/android/app/samsungapps/view/CategoryItemListView$ICallback;)V

    .line 116
    :goto_0
    return-void

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->a:Lcom/sec/android/app/samsungapps/view/CategoryItemListView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->load(Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/CategorySubListActivity;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->a()V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/CategorySubListActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 90
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 91
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v0, 0x2

    new-array v4, v0, [I

    fill-array-data v4, :array_0

    const v5, 0xb0001

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 97
    :goto_0
    return-void

    .line 94
    :catch_0
    move-exception v0

    const-string v0, "mCategory name is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    .line 95
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->finish()V

    goto :goto_0

    .line 91
    :array_0
    .array-data 4
        0xa0005
        0xa0008
    .end array-data
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const v3, 0x7f040050

    .line 37
    const-string v1, "Samsung Apps"

    .line 38
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->readObject(Landroid/content/Intent;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    if-eqz v0, :cond_4

    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->getName()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    :goto_0
    move-object v1, v0

    .line 50
    :goto_1
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 53
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    if-nez v0, :cond_1

    .line 54
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "SubCategoryContentId"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->c:Ljava/lang/String;

    .line 55
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "SubCategoryTitle"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 57
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->c:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 58
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->finish()V

    .line 61
    :cond_0
    if-eqz v0, :cond_2

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Error; {:try_start_1 .. :try_end_1} :catch_1

    move-result v2

    if-nez v2, :cond_2

    move-object v1, v0

    .line 73
    :cond_1
    :goto_2
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 74
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->setMainViewAndEmptyView(I)V

    .line 79
    :goto_3
    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->a(Ljava/lang/String;)V

    .line 81
    const v0, 0x7f0c0161

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 82
    new-instance v1, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;-><init>(Landroid/content/Context;Landroid/widget/ListView;)V

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->a:Lcom/sec/android/app/samsungapps/view/CategoryItemListView;

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->a:Lcom/sec/android/app/samsungapps/view/CategoryItemListView;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->setData(Lcom/sec/android/app/samsungapps/view/CategoryItemListView$ICategoryData;)V

    .line 84
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->a()V

    .line 85
    return-void

    .line 64
    :cond_2
    const v0, 0x7f0800b1

    :try_start_2
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->getString(I)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Error; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v1

    goto :goto_2

    .line 68
    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->finish()V

    goto :goto_2

    .line 70
    :catch_1
    move-exception v0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->finish()V

    goto :goto_2

    .line 76
    :cond_3
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->setMainView(I)V

    goto :goto_3

    :catch_2
    move-exception v0

    goto :goto_1

    .line 48
    :catch_3
    move-exception v0

    goto :goto_1

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->a:Lcom/sec/android/app/samsungapps/view/CategoryItemListView;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->a:Lcom/sec/android/app/samsungapps/view/CategoryItemListView;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->release()V

    .line 122
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->a:Lcom/sec/android/app/samsungapps/view/CategoryItemListView;

    .line 123
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onDestroy()V

    .line 124
    return-void
.end method

.method public retry()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 135
    const v0, 0x7f0c007f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0c0080

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0c0084

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0c007e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0c0086

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/app/samsungapps/q;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/q;-><init>(Lcom/sec/android/app/samsungapps/CategorySubListActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 136
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 129
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CategorySubListActivity;->setVisibleEmpty(Z)Z

    .line 130
    return-void
.end method
