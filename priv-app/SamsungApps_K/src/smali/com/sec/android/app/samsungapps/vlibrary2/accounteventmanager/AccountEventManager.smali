.class public Lcom/sec/android/app/samsungapps/vlibrary2/accounteventmanager/AccountEventManager;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static instance:Lcom/sec/android/app/samsungapps/vlibrary2/accounteventmanager/AccountEventManager;


# instance fields
.field _arrayList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/accounteventmanager/AccountEventManager;->_arrayList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    .line 23
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/accounteventmanager/AccountEventManager;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/accounteventmanager/AccountEventManager;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/accounteventmanager/AccountEventManager;

    if-nez v0, :cond_0

    .line 15
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/accounteventmanager/AccountEventManager;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/accounteventmanager/AccountEventManager;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/accounteventmanager/AccountEventManager;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/accounteventmanager/AccountEventManager;

    .line 17
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/accounteventmanager/AccountEventManager;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/accounteventmanager/AccountEventManager;

    return-object v0
.end method


# virtual methods
.method public addSubscriber(Lcom/sec/android/app/samsungapps/vlibrary2/accounteventmanager/AccountEventManager$IAccountEventSubscriber;)V
    .locals 1

    .prologue
    .line 43
    monitor-enter p0

    .line 45
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/accounteventmanager/AccountEventManager;->_arrayList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;->add(Ljava/lang/Object;)Z

    .line 46
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public notifyEvent(Lcom/sec/android/app/samsungapps/vlibrary2/accounteventmanager/AccountEventManager$AccountEvent;)V
    .locals 2

    .prologue
    .line 27
    monitor-enter p0

    .line 28
    :try_start_0
    const-string v0, "AccountEventManager"

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/accounteventmanager/AccountEventManager$AccountEvent;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 29
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/accounteventmanager/AccountEventManager;->_arrayList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;->clone()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/accounteventmanager/AccountEventManager$IAccountEventSubscriber;

    .line 31
    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/accounteventmanager/AccountEventManager$IAccountEventSubscriber;->onAccountEvent(Lcom/sec/android/app/samsungapps/vlibrary2/accounteventmanager/AccountEventManager$AccountEvent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 33
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public removeSubscriber(Lcom/sec/android/app/samsungapps/vlibrary2/accounteventmanager/AccountEventManager$IAccountEventSubscriber;)Z
    .locals 1

    .prologue
    .line 51
    monitor-enter p0

    .line 53
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/accounteventmanager/AccountEventManager;->_arrayList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 54
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
