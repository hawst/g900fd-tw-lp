.class public Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;
.source "ProGuard"


# static fields
.field private static instance:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;-><init>()V

    .line 40
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine;

    if-nez v0, :cond_0

    .line 46
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine;

    .line 48
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine;

    return-object v0
.end method


# virtual methods
.method protected entry(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 3

    .prologue
    .line 127
    const-string v0, "GetDownloadListStateMachine"

    const-string v1, "entry"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 128
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/update/e;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 145
    :goto_0
    return-void

    .line 131
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;->SEND_REQUEST:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 134
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;->CHECK_LOGINSTATE:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 137
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;->NOTIFY_SIG_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 138
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 141
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;->CALCULATE_UPDATE_AND_SIGSUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 144
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;->REQUEST_LOGIN:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 128
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;)Z
    .locals 3

    .prologue
    .line 54
    const-string v0, "GetDownloadListStateMachine"

    const-string v1, "execute"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 55
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/update/e;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 118
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    return v0

    .line 58
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/e;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 61
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$State;->REQUESTING:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 66
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$State;->CHECKING:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 75
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/e;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 78
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$State;->REQUESTING:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 81
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$State;->REQUEST_LOGIN:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 86
    :pswitch_7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/e;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 90
    :sswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$State;->REQUESTING:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 93
    :sswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 98
    :pswitch_8
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/e;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    sparse-switch v0, :sswitch_data_1

    goto :goto_0

    .line 104
    :sswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 101
    :sswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$State;->RESPONSE_RECEIVED:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 109
    :pswitch_9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/e;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    goto :goto_0

    .line 115
    :pswitch_a
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 112
    :pswitch_b
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;->NOTIFY_SIG_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 55
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_4
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch

    .line 58
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
    .end packed-switch

    .line 75
    :pswitch_data_2
    .packed-switch 0x5
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 86
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x7 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch

    .line 98
    :sswitch_data_1
    .sparse-switch
        0x4 -> :sswitch_2
        0x9 -> :sswitch_3
    .end sparse-switch

    .line 109
    :pswitch_data_3
    .packed-switch 0x2
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public bridge synthetic execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 9
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Event;)Z

    move-result v0

    return v0
.end method

.method protected exit(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 0

    .prologue
    .line 155
    return-void
.end method
