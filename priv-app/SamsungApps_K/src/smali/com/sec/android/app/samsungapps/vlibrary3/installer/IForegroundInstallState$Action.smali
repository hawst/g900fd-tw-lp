.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum DELETE_FILE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;

.field public static final enum REGISTER_PACKAGEMANAGER_OBSERVER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;

.field public static final enum REMOVE_PACKAGEMANAGER_OBSERVER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;

.field public static final enum REQUEST_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;

.field public static final enum SIG_INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;

.field public static final enum SIG_INSTALL_COMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 13
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;

    const-string v1, "DELETE_FILE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;->DELETE_FILE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;

    const-string v1, "SIG_INSTALL_COMPLETED"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;->SIG_INSTALL_COMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;

    const-string v1, "REQUEST_INSTALL"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;->REQUEST_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;

    const-string v1, "SIG_INSTALLING"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;->SIG_INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;

    const-string v1, "REMOVE_PACKAGEMANAGER_OBSERVER"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;->REMOVE_PACKAGEMANAGER_OBSERVER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;

    const-string v1, "REGISTER_PACKAGEMANAGER_OBSERVER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;->REGISTER_PACKAGEMANAGER_OBSERVER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;

    .line 11
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;->DELETE_FILE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;->SIG_INSTALL_COMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;->REQUEST_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;->SIG_INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;->REMOVE_PACKAGEMANAGER_OBSERVER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;->REGISTER_PACKAGEMANAGER_OBSERVER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;

    return-object v0
.end method
