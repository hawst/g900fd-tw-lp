.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICouponListGetter;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract addListener(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICouponListListener;)V
.end method

.method public abstract getSelCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;
.end method

.method public abstract removeListener(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICouponListListener;)V
.end method

.method public abstract requestPurchaseCouponList(Lcom/sec/android/app/samsungapps/vlibrary2/requestbuilder/IPurchaseCouponQueryCondition;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V
.end method

.method public abstract setSelCoupon(Ljava/lang/String;)V
.end method
