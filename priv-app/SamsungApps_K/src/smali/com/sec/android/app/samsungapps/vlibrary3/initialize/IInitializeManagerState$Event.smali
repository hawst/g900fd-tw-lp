.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum COUNTRY_INFO_OK:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

.field public static final enum COUNTRY_SEARCH_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

.field public static final enum COUNTRY_SEARCH_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

.field public static final enum EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

.field public static final enum NEED_COUNTRY_SEARCH:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

.field public static final enum NETWORK_STATE_NOT_OK:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

.field public static final enum NETWORK_STATE_OK:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 18
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

    const-string v1, "EXECUTE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

    const-string v1, "NETWORK_STATE_OK"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;->NETWORK_STATE_OK:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

    const-string v1, "NETWORK_STATE_NOT_OK"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;->NETWORK_STATE_NOT_OK:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

    const-string v1, "NEED_COUNTRY_SEARCH"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;->NEED_COUNTRY_SEARCH:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

    const-string v1, "COUNTRY_INFO_OK"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;->COUNTRY_INFO_OK:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

    const-string v1, "COUNTRY_SEARCH_FAILED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;->COUNTRY_SEARCH_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

    const-string v1, "COUNTRY_SEARCH_SUCCESS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;->COUNTRY_SEARCH_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

    .line 16
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;->NETWORK_STATE_OK:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;->NETWORK_STATE_NOT_OK:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;->NEED_COUNTRY_SEARCH:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;->COUNTRY_INFO_OK:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;->COUNTRY_SEARCH_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;->COUNTRY_SEARCH_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

    return-object v0
.end method
