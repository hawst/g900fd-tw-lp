.class public Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;
.source "ProGuard"


# static fields
.field private static a:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;-><init>()V

    .line 36
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine;

    return-object v0
.end method


# virtual methods
.method protected entry(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 3

    .prologue
    .line 107
    const-string v0, "EmergencyUpdateRunnerStateMachine"

    const-string v1, "entry"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 108
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/f;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 130
    :goto_0
    :pswitch_0
    return-void

    .line 111
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;->SHOW_EMERGENCY_ASKING_POPUP:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 114
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 115
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 118
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;->NOTIFY_FINISHED:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 119
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 124
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;->SEND_REQUEST:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 127
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;->RUN_UPDATE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 108
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Event;)Z
    .locals 3

    .prologue
    .line 46
    const-string v0, "EmergencyUpdateRunnerStateMachine"

    const-string v1, "execute"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 48
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/f;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 102
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    return v0

    .line 51
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/f;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 54
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;->UPDATE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 57
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;->FINISH:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 66
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/f;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 69
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;->REQUEST:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 74
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/f;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    goto :goto_0

    .line 77
    :pswitch_7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;->FINISH:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 80
    :pswitch_8
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 83
    :pswitch_9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 86
    :pswitch_a
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;->ASK_UPDATE_BY_POPUP:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 91
    :pswitch_b
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/f;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_4

    goto :goto_0

    .line 94
    :pswitch_c
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;->FINISH:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 97
    :pswitch_d
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 48
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_6
        :pswitch_b
    .end packed-switch

    .line 51
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 66
    :pswitch_data_2
    .packed-switch 0x3
        :pswitch_5
    .end packed-switch

    .line 74
    :pswitch_data_3
    .packed-switch 0x4
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch

    .line 91
    :pswitch_data_4
    .packed-switch 0x8
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method public bridge synthetic execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 8
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Event;

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Event;)Z

    move-result v0

    return v0
.end method

.method protected exit(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 3

    .prologue
    .line 135
    const-string v0, "EmergencyUpdateRunnerStateMachine"

    const-string v1, "exit"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 136
    return-void
.end method
