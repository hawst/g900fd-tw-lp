.class public Lcom/sec/android/app/samsungapps/vlibrary3/networkstatereceiver/WaitQueueForNetworkActivate;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static a:Lcom/sec/android/app/samsungapps/vlibrary3/networkstatereceiver/WaitQueueForNetworkActivate;


# instance fields
.field private b:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/networkstatereceiver/WaitQueueForNetworkActivate;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/networkstatereceiver/WaitQueueForNetworkActivate;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/networkstatereceiver/WaitQueueForNetworkActivate;->a:Lcom/sec/android/app/samsungapps/vlibrary3/networkstatereceiver/WaitQueueForNetworkActivate;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/networkstatereceiver/WaitQueueForNetworkActivate;->b:Ljava/util/ArrayList;

    .line 37
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/networkstatereceiver/WaitQueueForNetworkActivate;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/networkstatereceiver/WaitQueueForNetworkActivate;->a:Lcom/sec/android/app/samsungapps/vlibrary3/networkstatereceiver/WaitQueueForNetworkActivate;

    return-object v0
.end method


# virtual methods
.method public add(Lcom/sec/android/app/samsungapps/vlibrary3/networkstatereceiver/WaitQueueForNetworkActivate$IWaitNetworkActive;)V
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/networkstatereceiver/WaitQueueForNetworkActivate;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 18
    return-void
.end method

.method public notifyNetworkActivated()V
    .locals 2

    .prologue
    .line 27
    const-string v0, "WaitQueueForNetworkActivate"

    const-string v1, "notifyNetworkActivated"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 28
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/networkstatereceiver/WaitQueueForNetworkActivate;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/networkstatereceiver/WaitQueueForNetworkActivate$IWaitNetworkActive;

    .line 30
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/networkstatereceiver/WaitQueueForNetworkActivate$IWaitNetworkActive;->onNetworActivated()V

    goto :goto_0

    .line 32
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/networkstatereceiver/WaitQueueForNetworkActivate;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 33
    return-void
.end method

.method public remove(Lcom/sec/android/app/samsungapps/vlibrary3/networkstatereceiver/WaitQueueForNetworkActivate$IWaitNetworkActive;)V
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/networkstatereceiver/WaitQueueForNetworkActivate;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 23
    return-void
.end method
