.class public Lcom/sec/android/app/samsungapps/InicisPaymentActivity;
.super Lcom/sec/android/app/samsungapps/CommonActivity;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/iniciswebview/IWebViewContainerView;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisInitParam;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisView;


# static fields
.field private static f:Ljava/lang/String;


# instance fields
.field a:Lcom/sec/android/app/samsungapps/ActivityHelper;

.field private b:Landroid/webkit/WebView;

.field private c:Lcom/sec/android/app/samsungapps/eq;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private g:Z

.field private h:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;-><init>()V

    .line 50
    const-string v0, "samsungappsinicisresult://"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->d:Ljava/lang/String;

    .line 51
    const-string v0, "samsungappsiniciscancelresult://"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->e:Ljava/lang/String;

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->g:Z

    .line 403
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/InicisPaymentActivity;)Lcom/sec/android/app/samsungapps/eq;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->c:Lcom/sec/android/app/samsungapps/eq;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/InicisPaymentActivity;Lcom/sec/android/app/samsungapps/eq;)Lcom/sec/android/app/samsungapps/eq;
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->c:Lcom/sec/android/app/samsungapps/eq;

    return-object p1
.end method

.method private a(Z)V
    .locals 2

    .prologue
    const v1, 0x7f0c007f

    .line 195
    if-eqz p1, :cond_0

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->a:Lcom/sec/android/app/samsungapps/ActivityHelper;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->show(I)V

    .line 201
    :goto_0
    return-void

    .line 199
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->a:Lcom/sec/android/app/samsungapps/ActivityHelper;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->hide(I)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/InicisPaymentActivity;)Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->b:Landroid/webkit/WebView;

    return-object v0
.end method

.method private b(Z)V
    .locals 2

    .prologue
    const v1, 0x7f0c0080

    .line 205
    if-eqz p1, :cond_0

    .line 206
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->a:Lcom/sec/android/app/samsungapps/ActivityHelper;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->show(I)V

    .line 211
    :goto_0
    return-void

    .line 209
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->a:Lcom/sec/android/app/samsungapps/ActivityHelper;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->hide(I)V

    goto :goto_0
.end method


# virtual methods
.method public endLoading()V
    .locals 0

    .prologue
    .line 273
    return-void
.end method

.method public getCouponSeq()Ljava/lang/String;
    .locals 1

    .prologue
    .line 358
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->h:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->getCouponSeq()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGiftCardCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 667
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->h:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->getGiftCardCode()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProductInfo()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;
    .locals 1

    .prologue
    .line 350
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->h:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->getProductToBuy()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    move-result-object v0

    return-object v0
.end method

.method public isMobileWallet()Z
    .locals 1

    .prologue
    .line 368
    const/4 v0, 0x0

    return v0
.end method

.method public isPhoneBill()Z
    .locals 1

    .prologue
    .line 373
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->h:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->isPhoneBill()Z

    move-result v0

    return v0
.end method

.method public isUPoint()Z
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->h:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->isUpoint()Z

    move-result v0

    return v0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->h:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->h:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->cancel()V

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->h:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;

    .line 63
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->onBackPressed()V

    .line 64
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 87
    invoke-static {}, Lcom/sec/android/app/samsungapps/ThemeInfo;->getInstance()Lcom/sec/android/app/samsungapps/ThemeInfo;

    move-result-object v0

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ThemeInfo;->getTheme(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->mTheme:I

    .line 88
    iget v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->mTheme:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->setTheme(I)V

    .line 90
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/CommonActivity;->onCreate(Landroid/os/Bundle;)V

    .line 98
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 100
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 102
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 103
    const-string v1, "samsungappsinicisresult://"

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 105
    const-string v0, "samsungappsinicisresult://"

    sput-object v0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->f:Ljava/lang/String;

    .line 111
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->finish()V

    .line 130
    :cond_0
    :goto_1
    return-void

    .line 109
    :cond_1
    const-string v0, "samsungappsiniciscancelresult://"

    sput-object v0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->f:Ljava/lang/String;

    goto :goto_0

    .line 115
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->readObject(Landroid/content/Intent;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->h:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->h:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->h:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->invokeCompleted(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisView;)V

    .line 121
    const v0, 0x7f0400a1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->setContentView(I)V

    .line 122
    const v0, 0x7f0c024f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 123
    if-eqz v0, :cond_3

    .line 124
    const-string v1, "isa_samsungapps_icon_dummy"

    const-string v2, "drawable"

    invoke-static {p0, v1, v2}, Lcom/sec/android/app/samsungapps/uiutil/KnoxResourceManager;->findResource(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 126
    :cond_3
    new-instance v0, Lcom/sec/android/app/samsungapps/ActivityHelper;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/ActivityHelper;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->a:Lcom/sec/android/app/samsungapps/ActivityHelper;

    .line 128
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->startLoading()V

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->h:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->requestInitInicis(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisInitParam;)V

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->b:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->b:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->clearHistory()V

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->b:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->clearView()V

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->b:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V

    .line 173
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->b:Landroid/webkit/WebView;

    .line 176
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->onDestroy()V

    .line 177
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 69
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-eq v1, v0, :cond_0

    .line 71
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/CommonActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 82
    :goto_0
    return v0

    .line 74
    :cond_0
    const/4 v1, 0x4

    if-ne p1, v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->c:Lcom/sec/android/app/samsungapps/eq;

    if-eqz v1, :cond_1

    .line 77
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->c:Lcom/sec/android/app/samsungapps/eq;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/eq;->cancel(Z)Z

    .line 78
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->c:Lcom/sec/android/app/samsungapps/eq;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/eq;->a()V

    goto :goto_0

    .line 82
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/CommonActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 182
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/CommonActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 184
    const-string v0, "android.intent.action.VIEW"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 187
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 188
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/inicispay/InicisClientResult;

    invoke-direct {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/inicispay/InicisClientResult;-><init>(Landroid/net/Uri;)V

    .line 189
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->processInicisClientResult(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisWebClientResult;)V

    .line 191
    :cond_0
    return-void
.end method

.method public onRequestCompleteResult(Z)V
    .locals 0

    .prologue
    .line 290
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->endLoading()V

    .line 291
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->finish()V

    .line 292
    return-void
.end method

.method public onRequestInitResult(Z)V
    .locals 1

    .prologue
    .line 277
    if-eqz p1, :cond_0

    .line 279
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->endLoading()V

    .line 286
    :goto_0
    return-void

    .line 283
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->webViewNoData(Landroid/webkit/WebViewClient;)V

    .line 284
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->endLoading()V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 134
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->onResume()V

    .line 135
    sget-object v0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 137
    const-string v0, "samsungappsinicisresult://"

    sget-object v1, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v3, :cond_2

    .line 141
    const-string v0, "samsungappsinicisresult://"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 148
    :goto_0
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/inicispay/InicisClientResult;

    invoke-direct {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/inicispay/InicisClientResult;-><init>(Landroid/net/Uri;)V

    .line 149
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->processInicisClientResult(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisWebClientResult;)V

    .line 150
    const-string v0, ""

    sput-object v0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->f:Ljava/lang/String;

    .line 151
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->g:Z

    .line 154
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->g:Z

    if-ne v0, v3, :cond_1

    .line 156
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->finish()V

    .line 157
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->g:Z

    .line 159
    :cond_1
    return-void

    .line 145
    :cond_2
    const-string v0, "samsungappsiniciscancelresult://"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public openURL(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 232
    const v0, 0x7f0c024d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->b:Landroid/webkit/WebView;

    .line 233
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->b:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 234
    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 235
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setSavePassword(Z)V

    .line 236
    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 238
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->b:Landroid/webkit/WebView;

    new-instance v1, Lcom/sec/android/app/samsungapps/er;

    invoke-direct {v1, v2}, Lcom/sec/android/app/samsungapps/er;-><init>(B)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 239
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->b:Landroid/webkit/WebView;

    new-instance v1, Lcom/sec/android/app/samsungapps/iniciswebview/InicisWebClient;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->b:Landroid/webkit/WebView;

    invoke-direct {v1, p0, p0, v2}, Lcom/sec/android/app/samsungapps/iniciswebview/InicisWebClient;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/iniciswebview/IWebViewContainerView;Landroid/webkit/WebView;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 240
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->b:Landroid/webkit/WebView;

    new-instance v1, Lcom/sec/android/app/samsungapps/ep;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/ep;-><init>(Lcom/sec/android/app/samsungapps/InicisPaymentActivity;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setDownloadListener(Landroid/webkit/DownloadListener;)V

    .line 241
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->b:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->requestFocus()Z

    .line 242
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->b:Landroid/webkit/WebView;

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 243
    return-void
.end method

.method public openWebURL(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 363
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->openURL(Ljava/lang/String;)V

    .line 364
    return-void
.end method

.method public processInicisClientResult(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisWebClientResult;)V
    .locals 2

    .prologue
    .line 327
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisWebClientResult;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 329
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->h:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisPayment;->requestConfirmInicis(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisWebClientResult;Z)V

    .line 330
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->startLoading()V

    .line 347
    :goto_0
    return-void

    .line 338
    :cond_0
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisWebClientResult;->userCanceled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 340
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->finish()V

    goto :goto_0

    .line 344
    :cond_1
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisWebClientResult;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->showMessageWebView(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public showMessageWebView(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 215
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 227
    :goto_0
    return-void

    .line 221
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->a:Lcom/sec/android/app/samsungapps/ActivityHelper;

    const v1, 0x7f0c024d

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->hide(I)V

    .line 222
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->a:Lcom/sec/android/app/samsungapps/ActivityHelper;

    const v1, 0x7f0c024e

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->show(I)V

    .line 223
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->a:Lcom/sec/android/app/samsungapps/ActivityHelper;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->a(Ljava/lang/String;)V

    .line 225
    invoke-direct {p0, v2}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->a(Z)V

    .line 226
    invoke-direct {p0, v2}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->b(Z)V

    goto :goto_0
.end method

.method public startLoading()V
    .locals 0

    .prologue
    .line 266
    return-void
.end method

.method public startedWithISPURL(Landroid/webkit/WebViewClient;)V
    .locals 1

    .prologue
    .line 322
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->g:Z

    .line 323
    return-void
.end method

.method public webViewCanceled()V
    .locals 0

    .prologue
    .line 316
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->finish()V

    .line 317
    return-void
.end method

.method public webViewEnded(Landroid/webkit/WebViewClient;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 304
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->a(Z)V

    .line 305
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->b(Z)V

    .line 306
    return-void
.end method

.method public webViewNoData(Landroid/webkit/WebViewClient;)V
    .locals 1

    .prologue
    .line 310
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->a(Z)V

    .line 311
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->b(Z)V

    .line 312
    return-void
.end method

.method public webViewStarted(Landroid/webkit/WebViewClient;)V
    .locals 1

    .prologue
    .line 297
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->a(Z)V

    .line 298
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/InicisPaymentActivity;->b(Z)V

    .line 299
    return-void
.end method
