.class final Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/a;
.super Landroid/os/Handler;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;)V
    .locals 0

    .prologue
    .line 409
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/a;->a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 411
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onInstalledPackaged.packageInstalled("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/a;->a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->pkgname:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/a;->a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;

    iget v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->returncode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ");"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 412
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/a;->a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->onInstalledPackaged:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/OnInstalledPackaged;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->access$000(Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;)Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/OnInstalledPackaged;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/a;->a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->pkgname:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/a;->a:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;

    iget v2, v2, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->returncode:I

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/OnInstalledPackaged;->packageInstalled(Ljava/lang/String;I)V

    .line 413
    return-void
.end method
