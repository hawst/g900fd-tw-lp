.class public final enum Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;

.field public static final enum ExtraState:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;

.field public static final enum Finished:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;

.field public static final enum ForceClose:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;

.field public static final enum Init:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;

.field public static final enum LoadComplete:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;

.field public static final enum Loading:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;

.field public static final enum NoData:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 66
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;

    const-string v1, "Loading"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;->Loading:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;

    .line 67
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;

    const-string v1, "NoData"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;->NoData:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;

    .line 68
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;

    const-string v1, "LoadComplete"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;->LoadComplete:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;

    const-string v1, "Init"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;->Init:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;

    const-string v1, "ForceClose"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;->ForceClose:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;

    .line 69
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;

    const-string v1, "ExtraState"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;->ExtraState:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;

    const-string v1, "Finished"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;->Finished:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;

    .line 64
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;->Loading:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;->NoData:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;->LoadComplete:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;->Init:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;->ForceClose:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;->ExtraState:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;->Finished:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;
    .locals 1

    .prologue
    .line 64
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView$ViewState;

    return-object v0
.end method
