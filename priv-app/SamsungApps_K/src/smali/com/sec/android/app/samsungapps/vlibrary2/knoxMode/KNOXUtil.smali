.class public Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static instance:Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    if-nez v0, :cond_0

    .line 17
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    .line 20
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    return-object v0
.end method

.method private isKnox2FakeMode()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 53
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getSAConfig()Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->isKnox2Mode()Ljava/lang/String;

    move-result-object v1

    .line 54
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-ne v1, v0, :cond_0

    .line 65
    :goto_0
    return v0

    .line 61
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 65
    :cond_0
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 63
    :catch_1
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public getKnox2FakeHomeType()I
    .locals 4

    .prologue
    const/4 v1, 0x2

    const/4 v0, 0x1

    .line 71
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getSAConfig()Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getKnox2HomeType()Ljava/lang/String;

    move-result-object v2

    .line 72
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    if-ne v3, v0, :cond_0

    .line 87
    :goto_0
    return v0

    .line 76
    :cond_0
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    .line 78
    goto :goto_0

    .line 83
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 87
    :cond_1
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 85
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public isKnox2Mode()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 25
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2FakeMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 47
    :cond_0
    :goto_0
    return v0

    .line 30
    :cond_1
    :try_start_0
    const-string v1, "sys.knox.store"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 34
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 36
    const/16 v2, 0x64

    if-ge v1, v2, :cond_0

    .line 47
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 43
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 45
    :catch_1
    move-exception v0

    goto :goto_1
.end method
