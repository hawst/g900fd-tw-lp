.class public Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private a:J

.field private b:Z

.field private c:Landroid/content/Context;

.field private d:Ljava/io/FileOutputStream;

.field private e:J

.field private f:Z

.field private g:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;J)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->b:Z

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->d:Ljava/io/FileOutputStream;

    .line 20
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->c:Landroid/content/Context;

    .line 21
    iput-wide p3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->a:J

    .line 22
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->g:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;

    .line 23
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->b:Z

    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->f:Z

    .line 60
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 147
    if-eqz p1, :cond_0

    .line 150
    const v0, 0x8000

    .line 157
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->g:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;->getFileName()Ljava/lang/String;

    move-result-object v2

    or-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->d:Ljava/io/FileOutputStream;

    .line 158
    return-void

    .line 154
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()V
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->g:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;->deleteFile()Z

    .line 120
    return-void
.end method

.method private c()Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter$HFileWriteOpenResult;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 124
    const/4 v0, 0x1

    :try_start_0
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->a(Z)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 129
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->b:Z

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->g:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;->length()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->e:J

    .line 131
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter$HFileWriteOpenResult;->OPEN_EXISTFILE:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter$HFileWriteOpenResult;

    :goto_0
    return-object v0

    .line 125
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 127
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter$HFileWriteOpenResult;->OPEN_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter$HFileWriteOpenResult;

    goto :goto_0
.end method

.method private d()Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter$HFileWriteOpenResult;
    .locals 2

    .prologue
    .line 136
    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->a(Z)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 141
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->b:Z

    .line 142
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->e:J

    .line 143
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter$HFileWriteOpenResult;->OPEN_NEWFILE:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter$HFileWriteOpenResult;

    :goto_0
    return-object v0

    .line 137
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 139
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter$HFileWriteOpenResult;->OPEN_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter$HFileWriteOpenResult;

    goto :goto_0
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 162
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->b:Z

    if-eqz v0, :cond_1

    .line 164
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->b:Z

    .line 165
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->d:Ljava/io/FileOutputStream;

    if-eqz v0, :cond_0

    .line 168
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->d:Ljava/io/FileOutputStream;

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 174
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->b()V

    .line 176
    :cond_1
    return-void

    .line 169
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public close()V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->d:Ljava/io/FileOutputStream;

    if-eqz v0, :cond_0

    .line 100
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->d:Ljava/io/FileOutputStream;

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 105
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->d:Ljava/io/FileOutputStream;

    .line 107
    :cond_0
    return-void

    .line 101
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public getDownloadedSize()J
    .locals 2

    .prologue
    .line 27
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->e:J

    return-wide v0
.end method

.method public isDone()Z
    .locals 1

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->f:Z

    return v0
.end method

.method public open()Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter$HFileWriteOpenResult;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 32
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->f:Z

    .line 34
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->g:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;->exists()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 36
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->g:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;->length()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->a:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->b()V

    .line 39
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->d()Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter$HFileWriteOpenResult;

    move-result-object v0

    .line 53
    :goto_1
    return-object v0

    :cond_0
    move v2, v1

    .line 36
    goto :goto_0

    .line 41
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->g:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;->length()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->a:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    :goto_2
    if-eqz v0, :cond_3

    .line 43
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->a()V

    .line 44
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter$HFileWriteOpenResult;->OPEN_COMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter$HFileWriteOpenResult;

    goto :goto_1

    :cond_2
    move v0, v1

    .line 41
    goto :goto_2

    .line 48
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->c()Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter$HFileWriteOpenResult;

    move-result-object v0

    goto :goto_1

    .line 53
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->d()Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter$HFileWriteOpenResult;

    move-result-object v0

    goto :goto_1
.end method

.method public writeBuffer(I[B)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 74
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->b:Z

    if-nez v1, :cond_0

    .line 92
    :goto_0
    return v0

    .line 80
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->d:Ljava/io/FileOutputStream;

    const/4 v2, 0x0

    invoke-virtual {v1, p2, v2, p1}, Ljava/io/FileOutputStream;->write([BII)V

    .line 81
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->d:Ljava/io/FileOutputStream;

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->flush()V

    .line 82
    iget-wide v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->e:J

    int-to-long v3, p1

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->e:J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 88
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->e:J

    iget-wide v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->a:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    .line 90
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->a()V

    .line 92
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 83
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 85
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter;->b:Z

    goto :goto_0
.end method
