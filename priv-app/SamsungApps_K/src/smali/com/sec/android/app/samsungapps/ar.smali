.class final Lcom/sec/android/app/samsungapps/ar;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailOverviewWidgetClickListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V
    .locals 0

    .prologue
    .line 1803
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/ar;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClickExpand(II)V
    .locals 4

    .prologue
    .line 1814
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ar;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    const v1, 0x7f0c00a4

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 1815
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ar;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    const v2, 0x7f0c00cf

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1816
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/ar;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    const v3, 0x7f0c00d0

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;

    .line 1817
    if-eqz v0, :cond_0

    if-eqz v0, :cond_0

    if-nez v0, :cond_1

    .line 1823
    :cond_0
    :goto_0
    return-void

    .line 1820
    :cond_1
    invoke-virtual {v0, v2, v1}, Landroid/widget/ScrollView;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    .line 1821
    invoke-virtual {v0}, Landroid/widget/ScrollView;->requestFocus()Z

    goto :goto_0
.end method

.method public final onClickScreenshot(I)V
    .locals 1

    .prologue
    .line 1807
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ar;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ar;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailOverview()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1808
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ar;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailOverview()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->openScreenShot(I)V

    .line 1810
    :cond_0
    return-void
.end method
