.class public Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo$HelpInfoObserver;
.implements Lcom/sec/android/app/samsungapps/widget/interfaces/IAboutWidgetData;


# static fields
.field public static final REQUEST_TYPE_ABOUT:I = 0x2

.field public static final REQUEST_TYPE_UPDATE:I = 0x1


# instance fields
.field a:Landroid/content/Context;

.field b:Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo;

.field c:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

.field d:Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->a:Landroid/content/Context;

    .line 25
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo;

    .line 26
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->c:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    .line 27
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->d:Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;

    .line 29
    const-string v0, "AboutWidgetHelper"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->e:Ljava/lang/String;

    .line 36
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->a:Landroid/content/Context;

    .line 37
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo;

    .line 38
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->d:Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;

    .line 39
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 43
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->a:Landroid/content/Context;

    .line 44
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->c:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    .line 45
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->d:Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo;->removeObserver(Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo$HelpInfoObserver;)V

    .line 50
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo;

    .line 52
    :cond_0
    return-void
.end method

.method public getCurrentVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo;-><init>()V

    .line 169
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo;->getSamsungAppsVersion()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->convertVersionNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 171
    return-object v0
.end method

.method public getHelpText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 148
    const-string v0, ""

    .line 150
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo;

    if-eqz v1, :cond_0

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo;->value:Ljava/lang/String;

    .line 155
    :cond_0
    return-object v0
.end method

.method public getLatestVersion()Ljava/lang/String;
    .locals 2

    .prologue
    .line 183
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->isUpdateAvailable()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 190
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    .line 191
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->getLatestSamsungAppsVersion()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->convertVersionNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 198
    :goto_0
    return-object v0

    .line 195
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->getCurrentVersion()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public helpLoadCompleted(Z)V
    .locals 3

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo;->removeObserver(Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo$HelpInfoObserver;)V

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->c:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    if-eqz v0, :cond_1

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->c:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    const/4 v1, 0x0

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

    invoke-direct {v2}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;-><init>()V

    invoke-interface {v0, v1, p1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;->onReceiveResult(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V

    .line 82
    :cond_1
    return-void
.end method

.method public helpLoading()V
    .locals 0

    .prologue
    .line 61
    return-void
.end method

.method public isUpdateAvailable()Z
    .locals 3

    .prologue
    .line 208
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    .line 209
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->needSamsungAppsUpdate()Z

    move-result v0

    .line 211
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AboutWidgetHelper::isUpdateAvailable::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    .line 213
    return v0
.end method

.method public sendRequest(ILcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V
    .locals 3

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->d:Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo;

    if-eqz v0, :cond_0

    if-nez p2, :cond_1

    .line 92
    :cond_0
    const-string v0, "AboutWidgetHelpersendRequest::Help Info is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 109
    :goto_0
    return-void

    .line 96
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 99
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->d:Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->setNeedAutoDownload(Z)V

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->d:Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->a:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/widget/i;

    invoke-direct {v2, p0, p2}, Lcom/sec/android/app/samsungapps/widget/i;-><init>(Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/update/UpdateUtilCommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0

    .line 104
    :pswitch_1
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->c:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo;->addObserver(Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo$HelpInfoObserver;)V

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/AboutWidgetHelper;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo;->requestLoadHelp()V

    goto :goto_0

    .line 96
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
