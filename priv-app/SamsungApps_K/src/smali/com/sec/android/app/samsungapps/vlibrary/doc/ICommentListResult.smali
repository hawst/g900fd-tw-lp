.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract compareUserID(Ljava/lang/String;)Z
.end method

.method public abstract getComment()Ljava/lang/String;
.end method

.method public abstract getCommentDetail()Lcom/sec/android/app/samsungapps/vlibrary/doc/CommentDetail;
.end method

.method public abstract getDate()Ljava/lang/String;
.end method

.method public abstract getExpertSource()Ljava/lang/String;
.end method

.method public abstract getExpertTitle()Ljava/lang/String;
.end method

.method public abstract getExpertUpdateDate()Ljava/lang/String;
.end method

.method public abstract getExpertUrl()Ljava/lang/String;
.end method

.method public abstract getID()Ljava/lang/String;
.end method

.method public abstract getLoginID()Ljava/lang/String;
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getRating()I
.end method

.method public abstract getUserID()Ljava/lang/String;
.end method

.method public abstract isExpertComment()Z
.end method

.method public abstract isSeller()Z
.end method

.method public abstract setCommentDetail(Lcom/sec/android/app/samsungapps/vlibrary/doc/CommentDetail;)V
.end method
