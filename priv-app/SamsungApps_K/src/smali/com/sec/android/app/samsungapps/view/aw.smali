.class Lcom/sec/android/app/samsungapps/view/aw;
.super Landroid/text/style/ClickableSpan;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;

.field private b:Ljava/lang/String;

.field private c:I

.field private d:Landroid/text/TextPaint;

.field private e:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 171
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/aw;->a:Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    .line 168
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/aw;->e:Z

    .line 172
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/view/aw;->b:Ljava/lang/String;

    .line 173
    iput p3, p0, Lcom/sec/android/app/samsungapps/view/aw;->c:I

    .line 174
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Z)V
    .locals 1

    .prologue
    .line 190
    iput-boolean p2, p0, Lcom/sec/android/app/samsungapps/view/aw;->e:Z

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/aw;->d:Landroid/text/TextPaint;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/aw;->updateDrawState(Landroid/text/TextPaint;)V

    .line 192
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 193
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 197
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/view/aw;->a(Landroid/view/View;Z)V

    .line 198
    iget v0, p0, Lcom/sec/android/app/samsungapps/view/aw;->c:I

    packed-switch v0, :pswitch_data_0

    .line 232
    :goto_0
    return-void

    .line 200
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/aw;->a:Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;

    new-instance v1, Lcom/sec/android/app/samsungapps/view/ax;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/view/ax;-><init>(Lcom/sec/android/app/samsungapps/view/aw;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 209
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/aw;->a:Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;

    new-instance v1, Lcom/sec/android/app/samsungapps/view/ay;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/view/ay;-><init>(Lcom/sec/android/app/samsungapps/view/aw;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 217
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/aw;->a:Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;

    new-instance v1, Lcom/sec/android/app/samsungapps/view/az;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/view/az;-><init>(Lcom/sec/android/app/samsungapps/view/aw;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 226
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/aw;->a:Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;

    new-instance v1, Lcom/sec/android/app/samsungapps/view/ba;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/view/ba;-><init>(Lcom/sec/android/app/samsungapps/view/aw;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/SamsungAppsClickableTextView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 198
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 178
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/aw;->d:Landroid/text/TextPaint;

    .line 179
    const v0, -0xcd6924

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 180
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 187
    return-void
.end method
