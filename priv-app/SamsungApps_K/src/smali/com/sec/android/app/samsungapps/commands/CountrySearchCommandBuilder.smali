.class public Lcom/sec/android/app/samsungapps/commands/CountrySearchCommandBuilder;
.super Lcom/sec/android/app/samsungapps/vlibrary2/country/AbstractCountrySearchCommandBuilder;
.source "ProGuard"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/IDisclaimerCommandBuilder;)V
    .locals 2

    .prologue
    .line 25
    new-instance v0, Lcom/sec/android/app/samsungapps/notipopup/NotiPopupFactory;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/notipopup/NotiPopupFactory;-><init>()V

    new-instance v1, Lcom/sec/android/app/samsungapps/networkerrorpopup/NetworkErrorPopup;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/networkerrorpopup/NetworkErrorPopup;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/country/AbstractCountrySearchCommandBuilder;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/IDisclaimerCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;)V

    .line 26
    return-void
.end method


# virtual methods
.method public clearCache()V
    .locals 5

    .prologue
    .line 77
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 79
    new-instance v1, Ljava/io/File;

    sget-object v2, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "cache"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".txt"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/file/FilePath;->getAbsolutePath(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 80
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 77
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 82
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/AbstractCountrySearchCommandBuilder;->clearCache()V

    .line 83
    return-void
.end method

.method public createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
    .locals 1

    .prologue
    .line 56
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/ax;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/ax;-><init>(Lcom/sec/android/app/samsungapps/commands/CountrySearchCommandBuilder;)V

    return-object v0
.end method

.method public getCQPasswordViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
    .locals 1

    .prologue
    .line 44
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/aw;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/aw;-><init>(Lcom/sec/android/app/samsungapps/commands/CountrySearchCommandBuilder;)V

    return-object v0
.end method

.method public invokeSelectCountryListView()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/av;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/av;-><init>(Lcom/sec/android/app/samsungapps/commands/CountrySearchCommandBuilder;)V

    return-object v0
.end method
