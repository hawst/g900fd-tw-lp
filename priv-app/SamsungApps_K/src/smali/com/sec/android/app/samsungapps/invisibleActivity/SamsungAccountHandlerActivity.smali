.class public Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerActivity;
.super Lcom/sec/android/app/samsungapps/base/BaseActivity;
.source "ProGuard"


# static fields
.field protected static final REQUEST_CODE_SAC_SIGN_IN:I = 0x2001

.field protected static final RESULT_CANCELED:I = 0x0

.field protected static final RESULT_FAILED:I = 0x1

.field protected static final RESULT_INCORRECT_CLIENT_SECRET:I = 0x5

.field protected static final RESULT_INVALID_CLIENT_ID:I = 0x4

.field protected static final RESULT_INVALID_COUNTRYCODE:I = 0x6

.field protected static final RESULT_NETWORK_ERROR:I = 0x3

.field protected static final RESULT_OK:I = -0x1

.field protected static final RESULT_SIM_NOT_READY:I = 0x2

.field protected static final RESULT_SKIP:I = 0x7

.field public static final SAC_INIT_VERSION_CODE:I = 0x2ee1

.field public static final SAC_PKGNAME:Ljava/lang/String; = "com.osp.app.signin"


# instance fields
.field a:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginOldSamsungAccountCommand;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

.field private c:Lcom/sec/android/app/samsungapps/LoadingDialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/base/BaseActivity;-><init>()V

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerActivity;->c:Lcom/sec/android/app/samsungapps/LoadingDialog;

    return-void
.end method


# virtual methods
.method protected endLoading()V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerActivity;->c:Lcom/sec/android/app/samsungapps/LoadingDialog;

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerActivity;->c:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->end()V

    .line 78
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerActivity;->c:Lcom/sec/android/app/samsungapps/LoadingDialog;

    .line 80
    :cond_0
    return-void
.end method

.method public intentCheck(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 137
    if-eqz p1, :cond_1

    .line 141
    :try_start_0
    const-string v0, ""

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const-string v2, "com.osp.app.signin"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    array-length v2, v1

    if-lez v2, :cond_0

    const/4 v0, 0x0

    aget-object v0, v1, v0

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 143
    :cond_0
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->setEmail(Ljava/lang/String;)V

    .line 144
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->setPass(Ljava/lang/String;)V

    .line 146
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v2, 0x10

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->setDbData(ILjava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 152
    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 100
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 101
    const-string v1, "com.osp.app.signin"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "com.osp.app.signin"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->getPackageVersionCode(Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, 0x2ee1

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 103
    const/16 v0, 0x2001

    if-ne p1, v0, :cond_0

    .line 105
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerActivity;->onActivityResultSacFirstCheck(IILandroid/content/Intent;)V

    .line 106
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/base/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 109
    :cond_0
    return-void
.end method

.method protected onActivityResultSacFirstCheck(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 156
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerActivity;->endLoading()V

    .line 158
    const/16 v0, 0x2001

    if-ne p1, v0, :cond_1

    .line 160
    packed-switch p2, :pswitch_data_0

    .line 183
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginOldSamsungAccountCommand;

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginOldSamsungAccountCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginOldSamsungAccountCommand;->onSamsungAccountLoginFailed()V

    .line 188
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerActivity;->finish()V

    .line 198
    :cond_1
    :goto_0
    return-void

    .line 163
    :pswitch_0
    invoke-virtual {p0, p3}, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerActivity;->intentCheck(Landroid/content/Intent;)V

    .line 164
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    .line 165
    if-nez v0, :cond_3

    .line 167
    const-string v0, "SamsungAccountHandlerActivity::onActivityResultSacFirstCheck::AccountInfo is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 178
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerActivity;->finish()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 195
    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerActivity;->finish()V

    .line 196
    const-string v0, "SamsungApps"

    const-string v1, "onActivityResultSacFirstCheck abnormal state"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 171
    :cond_3
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->getDbData(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->setEmail(Ljava/lang/String;)V

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginOldSamsungAccountCommand;

    if-eqz v0, :cond_2

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginOldSamsungAccountCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginOldSamsungAccountCommand;->onSamsungAccountLoginSuccess()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 160
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 35
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 37
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->readObject(Landroid/content/Intent;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginOldSamsungAccountCommand;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginOldSamsungAccountCommand;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 44
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginOldSamsungAccountCommand;

    if-nez v0, :cond_0

    .line 46
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerActivity;->finish()V

    .line 51
    :goto_0
    return-void

    .line 38
    :catch_0
    move-exception v0

    .line 40
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SamsungAccountHandlerActivity::ClassCastException::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/ClassCastException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 41
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerActivity;->finish()V

    goto :goto_0

    .line 49
    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    .line 50
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerActivity;->start()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 60
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/base/BaseActivity;->onDestroy()V

    .line 61
    return-void
.end method

.method protected start()V
    .locals 3

    .prologue
    .line 55
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.AccountView"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_id"

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->SAC_CLIENT_ID:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "client_secret"

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->SAC_CLIENT_SECRET:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "service_name"

    const-string v2, "SamsungApps"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "OSP_VER"

    const-string v2, "OSP_02"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/16 v1, 0x2001

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerActivity;->startActivityForResult(Landroid/content/Intent;I)V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerActivity;->startLoading()V

    .line 56
    return-void
.end method

.method protected startLoading()V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerActivity;->c:Lcom/sec/android/app/samsungapps/LoadingDialog;

    if-nez v0, :cond_0

    .line 68
    new-instance v0, Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/LoadingDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerActivity;->c:Lcom/sec/android/app/samsungapps/LoadingDialog;

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerActivity;->c:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->start()V

    .line 71
    :cond_0
    return-void
.end method
