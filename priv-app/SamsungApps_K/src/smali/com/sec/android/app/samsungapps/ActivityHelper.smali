.class public Lcom/sec/android/app/samsungapps/ActivityHelper;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private a:Landroid/app/Activity;

.field private b:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ActivityHelper;->a:Landroid/app/Activity;

    .line 17
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ActivityHelper;->b:Landroid/view/View;

    .line 21
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/ActivityHelper;->a:Landroid/app/Activity;

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ActivityHelper;->a:Landroid/app/Activity;

    .line 17
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ActivityHelper;->b:Landroid/view/View;

    .line 25
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/ActivityHelper;->b:Landroid/view/View;

    .line 26
    return-void
.end method


# virtual methods
.method final a(I)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->find(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 50
    const v0, 0x7f0c024e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ActivityHelper;->a(I)Landroid/widget/TextView;

    move-result-object v0

    .line 51
    if-eqz v0, :cond_0

    .line 53
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    :cond_0
    return-void
.end method

.method final b(I)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 59
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->find(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    return-object v0
.end method

.method public createStringID(Ljava/lang/String;I)Lcom/sec/android/app/samsungapps/ActivityHelper$StringID;
    .locals 1

    .prologue
    .line 137
    new-instance v0, Lcom/sec/android/app/samsungapps/ActivityHelper$StringID;

    invoke-direct {v0, p1, p2}, Lcom/sec/android/app/samsungapps/ActivityHelper$StringID;-><init>(Ljava/lang/String;I)V

    return-object v0
.end method

.method public find(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActivityHelper;->a:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActivityHelper;->a:Landroid/app/Activity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 71
    :goto_0
    return-object v0

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActivityHelper;->b:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActivityHelper;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 71
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public findButton(I)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->find(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method public findCheckBox(I)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 75
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->find(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    return-object v0
.end method

.method public findSpinner(I)Landroid/widget/Spinner;
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->find(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    return-object v0
.end method

.method public getCheckBoxValue(I)Z
    .locals 1

    .prologue
    .line 103
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->find(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 104
    if-nez v0, :cond_0

    .line 106
    const/4 v0, 0x0

    .line 108
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    goto :goto_0
.end method

.method public getEditTextString(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->b(I)Landroid/widget/EditText;

    move-result-object v0

    .line 96
    if-eqz v0, :cond_0

    .line 98
    invoke-virtual {v0}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 100
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public hide(I)V
    .locals 2

    .prologue
    .line 85
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->find(I)Landroid/view/View;

    move-result-object v0

    .line 86
    if-eqz v0, :cond_0

    .line 88
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 90
    :cond_0
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 140
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ActivityHelper;->a:Landroid/app/Activity;

    .line 141
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ActivityHelper;->b:Landroid/view/View;

    .line 142
    return-void
.end method

.method public setSpinnerStringArrayAdapter(Landroid/content/Context;Landroid/widget/Spinner;)Landroid/widget/ArrayAdapter;
    .locals 2

    .prologue
    .line 130
    new-instance v0, Landroid/widget/ArrayAdapter;

    const v1, 0x7f040021

    invoke-direct {v0, p1, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 132
    const v1, 0x7f040022

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 133
    invoke-virtual {p2, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 134
    return-object v0
.end method

.method public show(I)V
    .locals 2

    .prologue
    .line 78
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->find(I)Landroid/view/View;

    move-result-object v0

    .line 79
    if-eqz v0, :cond_0

    .line 81
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 83
    :cond_0
    return-void
.end method
