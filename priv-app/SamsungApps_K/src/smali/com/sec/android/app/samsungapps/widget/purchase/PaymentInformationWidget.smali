.class public Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;
.super Lcom/sec/android/app/samsungapps/widget/CommonWidget;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPriceChangeListener;


# static fields
.field public static final PAYMENT_INFO_TYPE_PERMISSION:I = 0x1

.field public static final PAYMENT_INFO_TYPE_PURCHASE:I = 0x2


# instance fields
.field private a:Landroid/content/Context;

.field private b:Landroid/widget/TextView;

.field private d:Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPaymentInformationWidgetData;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 20
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;)V

    .line 13
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;->a:Landroid/content/Context;

    .line 14
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;->b:Landroid/widget/TextView;

    .line 21
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;->a:Landroid/content/Context;

    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;->a:Landroid/content/Context;

    const v1, 0x7f0400a4

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;->initView(Landroid/content/Context;I)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 26
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 13
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;->a:Landroid/content/Context;

    .line 14
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;->b:Landroid/widget/TextView;

    .line 27
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;->a:Landroid/content/Context;

    .line 28
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;->a:Landroid/content/Context;

    const v1, 0x7f0400a4

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;->initView(Landroid/content/Context;I)V

    .line 29
    return-void
.end method


# virtual methods
.method public loadWidget()V
    .locals 1

    .prologue
    .line 35
    const v0, 0x7f0c0257

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;->b:Landroid/widget/TextView;

    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;->d:Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPaymentInformationWidgetData;

    if-eqz v0, :cond_0

    .line 37
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;->updateWidget()V

    .line 39
    :cond_0
    return-void
.end method

.method public onPriceChanged()V
    .locals 0

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;->updateWidget()V

    .line 71
    return-void
.end method

.method public refreshWidget()V
    .locals 0

    .prologue
    .line 61
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;->d:Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPaymentInformationWidgetData;

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;->d:Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPaymentInformationWidgetData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPaymentInformationWidgetData;->release()V

    .line 78
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;->d:Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPaymentInformationWidgetData;

    .line 80
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->release()V

    .line 81
    return-void
.end method

.method public setWidgetData(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 64
    check-cast p1, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPaymentInformationWidgetData;

    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;->d:Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPaymentInformationWidgetData;

    .line 65
    return-void
.end method

.method public updateWidget()V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;->b:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;->d:Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPaymentInformationWidgetData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPaymentInformationWidgetData;->infomationExists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;->d:Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPaymentInformationWidgetData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPaymentInformationWidgetData;->getInformationString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 48
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;->setVisibility(I)V

    .line 55
    :cond_0
    :goto_0
    return-void

    .line 52
    :cond_1
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentInformationWidget;->setVisibility(I)V

    goto :goto_0
.end method
