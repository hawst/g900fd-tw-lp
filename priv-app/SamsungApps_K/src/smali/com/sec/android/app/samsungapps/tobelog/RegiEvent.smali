.class public final enum Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum RESULT_ALREADY_REGISTERED:Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

.field public static final enum RESULT_BLOCKED_APP:Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

.field public static final enum RESULT_FAIL:Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

.field public static final enum RESULT_HTTP_FAIL:Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

.field public static final enum RESULT_INTERNAL_DB_ERROR:Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

.field public static final enum RESULT_INTERNAL_ERROR:Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

.field public static final enum RESULT_INVALID_PARAMS:Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

.field public static final enum RESULT_SUCCESS:Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

.field public static final enum RESULT_TIMEOUT:Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

.field public static final enum RESUlT_PACKAGE_NOT_FOUND:Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

.field public static final enum UNKNOWN:Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

.field private static final b:I

.field private static c:[Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

.field private static final synthetic d:[Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;


# instance fields
.field private final a:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 8
    new-instance v1, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    const-string v2, "RESULT_SUCCESS"

    const/16 v3, 0x64

    invoke-direct {v1, v2, v0, v3}, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;->RESULT_SUCCESS:Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    new-instance v1, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    const-string v2, "RESULT_ALREADY_REGISTERED"

    const/16 v3, 0xc8

    invoke-direct {v1, v2, v5, v3}, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;->RESULT_ALREADY_REGISTERED:Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    .line 11
    new-instance v1, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    const-string v2, "RESULT_FAIL"

    const/4 v3, -0x1

    invoke-direct {v1, v2, v6, v3}, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;->RESULT_FAIL:Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    new-instance v1, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    const-string v2, "RESULT_INVALID_PARAMS"

    const/4 v3, -0x2

    invoke-direct {v1, v2, v7, v3}, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;->RESULT_INVALID_PARAMS:Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    new-instance v1, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    const-string v2, "RESULT_INTERNAL_ERROR"

    const/4 v3, -0x3

    invoke-direct {v1, v2, v8, v3}, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;->RESULT_INTERNAL_ERROR:Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    new-instance v1, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    const-string v2, "RESULT_INTERNAL_DB_ERROR"

    const/4 v3, 0x5

    const/4 v4, -0x4

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;->RESULT_INTERNAL_DB_ERROR:Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    new-instance v1, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    const-string v2, "RESULT_HTTP_FAIL"

    const/4 v3, 0x6

    const/4 v4, -0x5

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;->RESULT_HTTP_FAIL:Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    .line 12
    new-instance v1, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    const-string v2, "RESULT_TIMEOUT"

    const/4 v3, 0x7

    const/4 v4, -0x6

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;->RESULT_TIMEOUT:Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    new-instance v1, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    const-string v2, "RESULT_BLOCKED_APP"

    const/16 v3, 0x8

    const/4 v4, -0x7

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;->RESULT_BLOCKED_APP:Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    new-instance v1, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    const-string v2, "RESUlT_PACKAGE_NOT_FOUND"

    const/16 v3, 0x9

    const/4 v4, -0x8

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;->RESUlT_PACKAGE_NOT_FOUND:Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    .line 14
    new-instance v1, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    const-string v2, "UNKNOWN"

    const/16 v3, 0xa

    const/16 v4, -0x3e8

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;->UNKNOWN:Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    .line 5
    const/16 v1, 0xb

    new-array v1, v1, [Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;->RESULT_SUCCESS:Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    aput-object v2, v1, v0

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;->RESULT_ALREADY_REGISTERED:Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    aput-object v2, v1, v5

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;->RESULT_FAIL:Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    aput-object v2, v1, v6

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;->RESULT_INVALID_PARAMS:Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    aput-object v2, v1, v7

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;->RESULT_INTERNAL_ERROR:Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    aput-object v2, v1, v8

    const/4 v2, 0x5

    sget-object v3, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;->RESULT_INTERNAL_DB_ERROR:Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;->RESULT_HTTP_FAIL:Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    sget-object v3, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;->RESULT_TIMEOUT:Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    sget-object v3, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;->RESULT_BLOCKED_APP:Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    sget-object v3, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;->RESUlT_PACKAGE_NOT_FOUND:Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    sget-object v3, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;->UNKNOWN:Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    aput-object v3, v1, v2

    sput-object v1, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;->d:[Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    .line 30
    const-class v1, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    invoke-static {v1}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/EnumSet;->size()I

    move-result v1

    .line 32
    sput v1, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;->b:I

    new-array v1, v1, [Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    sput-object v1, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;->c:[Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    .line 38
    const-class v1, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    invoke-static {v1}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    .line 40
    sget-object v4, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;->c:[Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    add-int/lit8 v2, v1, 0x1

    aput-object v0, v4, v1

    move v1, v2

    goto :goto_0

    .line 44
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 18
    iput p3, p0, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;->a:I

    .line 20
    return-void
.end method

.method public static fromInt(I)Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;
    .locals 3

    .prologue
    .line 48
    const-class v0, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    .line 50
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;->value()I

    move-result v2

    if-ne v2, p0, :cond_0

    .line 58
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;->UNKNOWN:Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;
    .locals 1

    .prologue
    .line 5
    const-class v0, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;
    .locals 1

    .prologue
    .line 5
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;->d:[Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;

    return-object v0
.end method


# virtual methods
.method public final value()I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lcom/sec/android/app/samsungapps/tobelog/RegiEvent;->a:I

    return v0
.end method
