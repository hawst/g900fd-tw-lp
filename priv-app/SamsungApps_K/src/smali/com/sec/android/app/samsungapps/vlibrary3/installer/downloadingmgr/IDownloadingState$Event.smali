.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum CANCELE_DONE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

.field public static final enum DOWNLOAD_DONE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

.field public static final enum DOWNLOAD_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

.field public static final enum EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

.field public static final enum RETRY_COUNT_OVER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

.field public static final enum TIMEOUT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

.field public static final enum URL_DONE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

.field public static final enum URL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

.field public static final enum USER_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 15
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

    const-string v1, "EXECUTE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

    const-string v1, "DOWNLOAD_DONE"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;->DOWNLOAD_DONE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

    const-string v1, "DOWNLOAD_FAILED"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;->DOWNLOAD_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

    const-string v1, "USER_CANCEL"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;->USER_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

    const-string v1, "CANCELE_DONE"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;->CANCELE_DONE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

    const-string v1, "RETRY_COUNT_OVER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;->RETRY_COUNT_OVER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

    const-string v1, "TIMEOUT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;->TIMEOUT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

    const-string v1, "URL_DONE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;->URL_DONE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

    const-string v1, "URL_FAILED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;->URL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

    .line 13
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;->DOWNLOAD_DONE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;->DOWNLOAD_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;->USER_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;->CANCELE_DONE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;->RETRY_COUNT_OVER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;->TIMEOUT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;->URL_DONE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;->URL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

    return-object v0
.end method
