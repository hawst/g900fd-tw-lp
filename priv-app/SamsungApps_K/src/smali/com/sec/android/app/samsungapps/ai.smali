.class final Lcom/sec/android/app/samsungapps/ai;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;)V
    .locals 0

    .prologue
    .line 211
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/ai;->a:Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 217
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, -0x1

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ai;->a:Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;

    iget-boolean v0, v0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->b:Z

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ai;->a:Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->f(Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;)Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 222
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ai;->a:Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->f(Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;)Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setFocusable(Z)V

    .line 224
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ai;->a:Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->b:Z

    .line 225
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 228
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ai;->a:Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->c(Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_0

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ai;->a:Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->g(Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 231
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ai;->a:Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->g(Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    .line 238
    :goto_0
    return-void

    .line 235
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ai;->a:Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->g(Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 236
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ai;->a:Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;->g(Lcom/sec/android/app/samsungapps/ConfirmPasswordDialog;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setFocusable(Z)V

    goto :goto_0
.end method
