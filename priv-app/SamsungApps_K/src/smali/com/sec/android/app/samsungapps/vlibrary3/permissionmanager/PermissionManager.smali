.class public Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/IPermissionManager;
.implements Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;


# instance fields
.field a:Landroid/os/Handler;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$State;

.field private c:Landroid/content/Context;

.field private d:Landroid/os/CountDownTimer;

.field private e:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

.field private f:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager$IPermissionManagerObserver;

.field private g:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field private h:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionLoader;

.field private i:Z

.field private j:[Ljava/lang/String;

.field private k:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;ZZ)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->a:Landroid/os/Handler;

    .line 29
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$State;

    .line 135
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->k:Z

    .line 40
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->c:Landroid/content/Context;

    .line 41
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 42
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->e:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    .line 43
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionLoader;

    invoke-direct {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionLoader;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->h:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionLoader;

    .line 44
    iput-boolean p4, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->k:Z

    .line 45
    iput-boolean p5, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->i:Z

    .line 46
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;)V

    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;)V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->a:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/b;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 68
    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;)Z
    .locals 2

    .prologue
    .line 27
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isUnifiedBillingSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isSAccountVersionHigherThan1_3_001(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public execute()V
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;)V

    .line 84
    return-void
.end method

.method public getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    return-object v0
.end method

.method public getState()Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$State;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$State;

    return-object v0
.end method

.method public bridge synthetic getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->getState()Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$State;

    move-result-object v0

    return-object v0
.end method

.method public invokeCompleted()V
    .locals 1

    .prologue
    .line 269
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->INVOKECOMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;)V

    .line 270
    return-void
.end method

.method public isForUpdate()Z
    .locals 1

    .prologue
    .line 288
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->i:Z

    return v0
.end method

.method public onAction(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 88
    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/e;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 132
    :cond_0
    :goto_0
    return-void

    .line 91
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->h:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionLoader;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/d;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionLoader;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionLoader$IPermissionLoaderObserver;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->h:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionLoader;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionLoader;->execute()V

    goto :goto_0

    .line 94
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->f:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager$IPermissionManagerObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->f:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager$IPermissionManagerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager$IPermissionManagerObserver;->onPermissionFailed()V

    goto :goto_0

    .line 97
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->f:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager$IPermissionManagerObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->f:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager$IPermissionManagerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager$IPermissionManagerObserver;->onPermissionSuccess()V

    goto :goto_0

    .line 100
    :pswitch_3
    iget-boolean v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->k:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getPermission()Lcom/sec/android/app/samsungapps/vlibrary3/doc/Permission;

    move-result-object v2

    if-nez v2, :cond_2

    move v0, v1

    :cond_1
    :goto_1
    if-eqz v0, :cond_3

    const-string v0, "PermissionManagerStateMachine"

    const-string v1, "permission Same"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->INVOKECOMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;)V

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->USER_AGREE:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getPermission()Lcom/sec/android/app/samsungapps/vlibrary3/doc/Permission;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/doc/Permission;->hasNewPermission()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getPermission()Lcom/sec/android/app/samsungapps/vlibrary3/doc/Permission;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/doc/Permission;->hasCreatedPermission()Z

    move-result v2

    if-nez v2, :cond_1

    move v0, v1

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->e:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->c:Landroid/content/Context;

    invoke-interface {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;->invoke(Landroid/content/Context;Ljava/lang/Object;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->e:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->c:Landroid/content/Context;

    invoke-interface {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;->invoke(Landroid/content/Context;Ljava/lang/Object;)V

    goto :goto_0

    .line 103
    :pswitch_4
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/c;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->d:Landroid/os/CountDownTimer;

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->d:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    goto :goto_0

    .line 119
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->d:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->d:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 122
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->d:Landroid/os/CountDownTimer;

    goto/16 :goto_0

    .line 126
    :pswitch_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->c:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->isPackageInstalled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->PACKAGE_ALREADY_INSTALLED:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;)V

    goto/16 :goto_0

    :cond_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->PACKAGE_NOT_INSTALLED:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;)V

    goto/16 :goto_0

    .line 129
    :pswitch_7
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->c:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->getPackagePermission(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->j:[Ljava/lang/String;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->j:[Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->j:[Ljava/lang/String;

    array-length v3, v2

    :goto_2
    if-ge v0, v3, :cond_6

    aget-object v4, v2, v0

    invoke-virtual {v1, v4, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getPermission()Lcom/sec/android/app/samsungapps/vlibrary3/doc/Permission;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/doc/Permission;->getPermissionMap()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;->getPermissionID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    sget-object v4, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo$EnumPermissionType;->DERIVE:Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo$EnumPermissionType;

    invoke-interface {v0, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;->setPermissionType(Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo$EnumPermissionType;)V

    goto :goto_3

    :cond_8
    sget-object v4, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo$EnumPermissionType;->NEW:Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo$EnumPermissionType;

    invoke-interface {v0, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;->setPermissionType(Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo$EnumPermissionType;)V

    goto :goto_3

    :cond_9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->APK_PERMISSION_COMPARE_COMPLETE:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;)V

    goto/16 :goto_0

    .line 88
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public bridge synthetic onAction(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 27
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->onAction(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;)V

    return-void
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager$IPermissionManagerObserver;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->f:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager$IPermissionManagerObserver;

    .line 52
    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$State;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$State;

    .line 73
    return-void
.end method

.method public bridge synthetic setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 27
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$State;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$State;)V

    return-void
.end method

.method public userAgree(Z)V
    .locals 1

    .prologue
    .line 226
    if-eqz p1, :cond_0

    .line 228
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->USER_AGREE:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;)V

    .line 234
    :goto_0
    return-void

    .line 232
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->USER_DISAGREE:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;)V

    goto :goto_0
.end method
