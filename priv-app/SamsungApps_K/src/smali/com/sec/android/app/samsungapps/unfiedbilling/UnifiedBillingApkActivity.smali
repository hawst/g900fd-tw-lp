.class public Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkActivity;
.super Landroid/app/Activity;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/doc/IBaseContext;


# static fields
.field protected static final AIDL_SENDING_RESULT_ERROR:I = 0x3

.field protected static final AIDL_SENDING_RESULT_FAIL:I = 0x2

.field protected static final AIDL_SENDING_RESULT_OK:I = 0x1

.field protected static final apiVersion:I = 0x4e30


# instance fields
.field protected aidlHandler:Landroid/os/Handler;

.field protected mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

.field protected mHandler:Landroid/os/Handler;

.field protected uxVersion:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    .line 34
    new-instance v0, Lcom/sec/android/app/samsungapps/unfiedbilling/a;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/unfiedbilling/a;-><init>(Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkActivity;->aidlHandler:Landroid/os/Handler;

    .line 57
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkActivity;->mHandler:Landroid/os/Handler;

    .line 59
    const-string v0, "v2"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkActivity;->uxVersion:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getFakeModel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getFakeModelName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getFakeModelName()Ljava/lang/String;

    move-result-object v0

    .line 25
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getGearPlatformVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getGearOSVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getPRDServerURL()Ljava/lang/String;
    .locals 2

    .prologue
    .line 94
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    .line 95
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->CHINA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->checkCountry(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->CHINA2:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->checkCountry(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 97
    :cond_0
    const-string v0, "https://cn-mop.samsungosp.com"

    .line 99
    :goto_0
    return-object v0

    :cond_1
    const-string v0, "https://mop.samsungosp.com"

    goto :goto_0
.end method

.method protected getUPServerURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 63
    if-eqz p1, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 65
    :cond_0
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    .line 66
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->CHINA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->checkCountry(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->CHINA2:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->checkCountry(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 68
    :cond_1
    const-string v0, "https://cn-mop.samsungosp.com"

    .line 89
    :goto_0
    return-object v0

    .line 70
    :cond_2
    const-string v0, "https://mop.samsungosp.com"

    goto :goto_0

    .line 72
    :cond_3
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 73
    const-string v1, "PRD"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 75
    const-string v0, "https://mop.samsungosp.com"

    goto :goto_0

    .line 77
    :cond_4
    const-string v1, "STG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 79
    const-string v0, "https://stg-api.samsungosp.com"

    goto :goto_0

    .line 81
    :cond_5
    const-string v1, "CHINA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 83
    const-string v0, "https://cn-mop.samsungosp.com"

    goto :goto_0

    .line 85
    :cond_6
    const-string v1, "PRT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 87
    const-string v0, "https://stg-mop.samsungosp.com"

    goto :goto_0

    .line 89
    :cond_7
    const-string v0, "https://mop.samsungosp.com"

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 52
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 54
    new-instance v0, Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/base/BaseHandle;-><init>(Landroid/content/Intent;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    .line 55
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 105
    invoke-static {p0}, Lcom/sec/android/app/billing/helper/UPHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/billing/helper/UPHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/billing/helper/UPHelper;->dispose()V

    .line 106
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 107
    return-void
.end method

.method protected onStart()V
    .locals 0

    .prologue
    .line 30
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 32
    return-void
.end method
