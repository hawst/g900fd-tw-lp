.class public Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventQueue;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field mSystemEventList:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventQueue;->mSystemEventList:Ljava/util/ArrayList;

    .line 13
    return-void
.end method


# virtual methods
.method public add(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventQueue;->mSystemEventList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 18
    return-void
.end method

.method public findEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;)Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;
    .locals 3

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventQueue;->mSystemEventList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

    .line 29
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v2

    if-ne v2, p1, :cond_0

    .line 34
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getList()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventQueue;->mSystemEventList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public remove(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)Z
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventQueue;->mSystemEventList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
