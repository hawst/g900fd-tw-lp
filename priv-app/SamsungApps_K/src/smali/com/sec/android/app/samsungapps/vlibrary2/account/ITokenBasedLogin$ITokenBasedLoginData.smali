.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/account/ITokenBasedLogin$ITokenBasedLoginData;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract hasToken()Z
.end method

.method public abstract isSamsungAccountDisabled()Z
.end method

.method public abstract isTokenExpired()Z
.end method

.method public abstract loginODCWithToken()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract loginSamsungAccount()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract requestToken()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method
