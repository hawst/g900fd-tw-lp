.class public Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/initialize/ICheckAppResultObserver;


# static fields
.field private static a:Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;


# instance fields
.field private b:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

.field private c:Landroid/content/Context;

.field private d:Z

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;->a:Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;->b:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 20
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;->c:Landroid/content/Context;

    .line 21
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;->b:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    if-nez v0, :cond_0

    .line 70
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/CommandCreator;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/CommandCreator;-><init>()V

    .line 71
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;->c:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->createInstallerFactory()Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/commandcreator/CommandCreator;->createUpdateUtilCommand(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;->b:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;->b:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;->c:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 74
    :cond_0
    return-void
.end method

.method private static a(Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;)Z
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    .line 45
    :try_start_0
    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->upgrade:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->upgrade:I

    if-ne v1, v2, :cond_2

    .line 46
    :cond_0
    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->ForceUpdate:I

    if-eq v1, v0, :cond_1

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->ForceUpdate:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v1, v2, :cond_2

    .line 55
    :cond_1
    :goto_0
    return v0

    :catch_0
    move-exception v0

    :cond_2
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 54
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;->a:Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;

    if-nez v0, :cond_0

    .line 25
    new-instance v0, Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;->a:Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;

    .line 27
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;->a:Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;

    return-object v0
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;->b:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;->b:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->cancel()V

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;->b:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 81
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;->e:Z

    .line 82
    return-void
.end method

.method public onReceiveResult()V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCheckAppUpgradeResult()Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    move-result-object v0

    .line 33
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;->a(Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;->d:Z

    .line 41
    :goto_0
    return-void

    .line 36
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;->e:Z

    if-eqz v0, :cond_1

    .line 37
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;->a()V

    .line 39
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;->d:Z

    goto :goto_0
.end method

.method public updateCheck()V
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;->d:Z

    if-eqz v0, :cond_0

    .line 60
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;->a()V

    .line 62
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;->e:Z

    .line 63
    return-void
.end method
