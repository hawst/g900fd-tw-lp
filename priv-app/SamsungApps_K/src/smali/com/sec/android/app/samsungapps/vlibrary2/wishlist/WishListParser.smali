.class public Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishListParser;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# instance fields
.field _Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

.field _Result:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishArray;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishListParser;->_Result:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishArray;

    .line 12
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishArray;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishArray;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishListParser;->_Result:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishArray;

    .line 13
    return-void
.end method


# virtual methods
.method public addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishListParser;->_Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 30
    return-void
.end method

.method public clearContainer()V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishListParser;->_Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    if-eqz v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishListParser;->_Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->clear()V

    .line 37
    :cond_0
    return-void
.end method

.method public closeMap()V
    .locals 3

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishListParser;->_Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    if-eqz v0, :cond_0

    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishListParser;->_Result:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishArray;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishListParser;->_Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishArray;->add(Ljava/lang/Object;)Z

    .line 25
    :cond_0
    return-void
.end method

.method public findString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    return-object v0
.end method

.method public getResult()Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishArray;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishListParser;->_Result:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishArray;

    return-object v0
.end method

.method public openMap()V
    .locals 1

    .prologue
    .line 16
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishListParser;->_Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 17
    return-void
.end method

.method public setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 57
    const-string v0, "endOfList"

    invoke-virtual {p1, v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->getBool(Ljava/lang/String;Z)Z

    move-result v0

    .line 58
    const-string v1, "endNum"

    invoke-virtual {p1, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 59
    const-string v2, "startNum"

    invoke-virtual {p1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 60
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishListParser;->_Result:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishArray;

    invoke-virtual {v3, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishArray;->setEOF(Z)V

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishListParser;->_Result:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishArray;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishArray;->setStartNumber(I)V

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishListParser;->_Result:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishArray;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishArray;->setEndNumber(I)V

    .line 63
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishListParser;->_Result:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishArray;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishArray;->size()I

    move-result v0

    return v0
.end method
