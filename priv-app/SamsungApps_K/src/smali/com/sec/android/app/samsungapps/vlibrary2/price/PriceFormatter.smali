.class public Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatter;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceFormatter;


# instance fields
.field private _CurrencyUnitDivision:Z

.field private _CurrencyUnitHasPenny:Z

.field private _CurrencyUnitPrecedes:Z

.field private _FreeString:Ljava/lang/String;

.field private _currencyUnit:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/price/ICurrencyFormatInfo;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    if-nez p1, :cond_0

    .line 18
    const-string v0, "error"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->err(Ljava/lang/String;)V

    .line 26
    :goto_0
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatter;->_currencyUnit:Ljava/lang/String;

    .line 27
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatter;->_FreeString:Ljava/lang/String;

    .line 28
    return-void

    .line 22
    :cond_0
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/price/ICurrencyFormatInfo;->getCurrencyUnitHasPenny()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatter;->_CurrencyUnitHasPenny:Z

    .line 23
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/price/ICurrencyFormatInfo;->getCurrencyUnitDivision()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatter;->_CurrencyUnitDivision:Z

    .line 24
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/price/ICurrencyFormatInfo;->getCurrencyUnitPrecedes()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatter;->_CurrencyUnitPrecedes:Z

    goto :goto_0
.end method


# virtual methods
.method public getFormattedPrice(D)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 33
    const-wide/16 v0, 0x0

    cmpl-double v0, p1, v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatter;->_FreeString:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatter;->_FreeString:Ljava/lang/String;

    .line 75
    :goto_0
    return-object v0

    .line 37
    :cond_0
    double-to-int v0, p1

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 39
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatter;->_CurrencyUnitHasPenny:Z

    if-eqz v1, :cond_1

    .line 44
    :try_start_0
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "0.00"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 45
    invoke-virtual {v0, p1, p2}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 51
    :cond_1
    :goto_1
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatter;->_CurrencyUnitDivision:Z

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x3

    if-le v1, v2, :cond_3

    .line 55
    :try_start_1
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "#,###"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 56
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatter;->_CurrencyUnitHasPenny:Z

    if-eqz v1, :cond_2

    .line 58
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "#,###.00"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 60
    :cond_2
    invoke-virtual {v0, p1, p2}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 67
    :cond_3
    :goto_2
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatter;->_CurrencyUnitPrecedes:Z

    if-eqz v1, :cond_4

    .line 69
    const-string v1, "%s%s"

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatter;->_currencyUnit:Ljava/lang/String;

    aput-object v3, v2, v4

    aput-object v0, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 48
    :catch_0
    move-exception v0

    const-string v0, " "

    goto :goto_1

    .line 64
    :catch_1
    move-exception v0

    const-string v0, " "

    goto :goto_2

    .line 73
    :cond_4
    const-string v1, "%s%s"

    new-array v2, v3, [Ljava/lang/Object;

    aput-object v0, v2, v4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/PriceFormatter;->_currencyUnit:Ljava/lang/String;

    aput-object v0, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
