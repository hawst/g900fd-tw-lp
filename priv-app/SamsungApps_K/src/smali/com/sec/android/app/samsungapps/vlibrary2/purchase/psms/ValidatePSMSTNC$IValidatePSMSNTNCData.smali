.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC$IValidatePSMSNTNCData;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
.end method

.method public abstract createSMSSender()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ISMSSender;
.end method

.method public abstract existTNC()Z
.end method

.method public abstract getFormattedSellingPrice()Ljava/lang/String;
.end method

.method public abstract getNotiTNCSendFailCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract getProductName()Ljava/lang/String;
.end method

.method public abstract getTNCDisplayMessage()Ljava/lang/String;
.end method

.method public abstract getTNCSMSMessage()Ljava/lang/String;
.end method

.method public abstract getTNCSMSNo()Ljava/lang/String;
.end method

.method public abstract getTNCViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
.end method
