.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/AliPayXMLRequest;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
.source "ProGuard"


# direct methods
.method protected constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V
    .locals 0

    .prologue
    .line 15
    invoke-direct/range {p0 .. p6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 17
    return-void
.end method

.method public static completeAlipayPurchase(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCompParam;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/AliPayXMLRequest;
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 98
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/AliPayXMLRequest;

    const-string v2, "completeAlipayPurchase"

    const-string v3, "3220"

    move-object v1, p0

    move v4, p2

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/AliPayXMLRequest;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 99
    const-string v1, "productID"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCompParam;->getProductInfo()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;->getProductID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/AliPayXMLRequest;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    const-string v1, "orderID"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCompParam;->getOrderID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/AliPayXMLRequest;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    const-string v1, "paymentID"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCompParam;->getPaymentID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/AliPayXMLRequest;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 102
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCompParam;->getCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 104
    const-string v1, "couponIssuedSEQ"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCompParam;->getCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->getCouponSeq()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/AliPayXMLRequest;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    :goto_0
    const-string v2, "lastReqYn"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCompParam;->lastReq()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "Y"

    :goto_1
    invoke-virtual {v0, v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/AliPayXMLRequest;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    return-object v0

    .line 108
    :cond_0
    const-string v1, "couponIssuedSEQ"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/AliPayXMLRequest;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 110
    :cond_1
    const-string v1, "N"

    goto :goto_1
.end method

.method private static getImei()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v0

    .line 45
    if-nez v0, :cond_0

    .line 47
    const-string v0, "000000000"

    .line 49
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getIMEI()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static initAlipayPurchase(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayInitParam;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/AliPayXMLRequest;
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 54
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/AliPayXMLRequest;

    const-string v2, "initAlipayPurchase"

    const-string v3, "3210"

    move-object v1, p0

    move v4, p2

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/AliPayXMLRequest;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 55
    const-string v1, "productID"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayInitParam;->getProductInfo()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;->getProductID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/AliPayXMLRequest;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    const-string v1, "imei"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/xml/AliPayXMLRequest;->getImei()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/AliPayXMLRequest;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 57
    const-string v1, "guid"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayInitParam;->getProductInfo()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;->getGUID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/AliPayXMLRequest;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 59
    const-string v1, "mcc"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMCC()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/AliPayXMLRequest;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    const-string v1, "mnc"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMNC()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/AliPayXMLRequest;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    :cond_0
    const-string v1, "reserved01"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/AliPayXMLRequest;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    const-string v1, "reserved02"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/AliPayXMLRequest;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    const-string v1, "reserved03"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/AliPayXMLRequest;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    const-string v1, "reserved04"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/AliPayXMLRequest;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    const-string v1, "reserved05"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/AliPayXMLRequest;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    const-string v1, "rentalTerm"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/AliPayXMLRequest;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayInitParam;->getCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 70
    const-string v1, "couponIssuedSEQ"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayInitParam;->getCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->getCouponSeq()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/AliPayXMLRequest;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    :goto_0
    const-string v1, "paymentMethodID"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayInitParam;->getPaymentMethod()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/AliPayXMLRequest;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 78
    return-object v0

    .line 74
    :cond_1
    const-string v1, "couponIssuedSEQ"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/AliPayXMLRequest;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
