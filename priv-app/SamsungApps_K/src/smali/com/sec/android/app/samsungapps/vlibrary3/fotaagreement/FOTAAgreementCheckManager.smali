.class public Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckManager;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$State;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckManager$IFOTAAgreementCheckManagerObserver;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/PreloadTnCAgreementChecker;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckManager$IFOTAAgreementCheckManagerObserver;Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/PreloadTnCAgreementChecker;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$State;

    .line 23
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckManager$IFOTAAgreementCheckManagerObserver;

    .line 24
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/PreloadTnCAgreementChecker;

    .line 25
    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;)V
    .locals 1

    .prologue
    .line 18
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;)Z

    .line 19
    return-void
.end method


# virtual methods
.method public getState()Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$State;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$State;

    return-object v0
.end method

.method public bridge synthetic getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckManager;->getState()Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$State;

    move-result-object v0

    return-object v0
.end method

.method public onAction(Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Action;)V
    .locals 2

    .prologue
    .line 39
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/a;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Action;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 55
    :cond_0
    :goto_0
    return-void

    .line 42
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/PreloadTnCAgreementChecker;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/PreloadTnCAgreementChecker;->isTncAgreed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 44
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;->TnCAgreed:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;)V

    goto :goto_0

    .line 48
    :cond_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;->TnCNotAgreed:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Event;)V

    goto :goto_0

    .line 52
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckManager$IFOTAAgreementCheckManagerObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckManager$IFOTAAgreementCheckManagerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckManager$IFOTAAgreementCheckManagerObserver;->onTNCAgreed()V

    goto :goto_0

    .line 39
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic onAction(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 10
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Action;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckManager;->onAction(Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$Action;)V

    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$State;)V
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$State;

    .line 30
    return-void
.end method

.method public bridge synthetic setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 10
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$State;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckManager;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckSTM$State;)V

    return-void
.end method
