.class public Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailRelated;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# instance fields
.field _DetailMain:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

.field _Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

.field private _bValid:Z

.field private categoryID:Ljava/lang/String;

.field private categoryID2:Ljava/lang/String;

.field private categoryName:Ljava/lang/String;

.field private categoryName2:Ljava/lang/String;

.field private reportNum:Ljava/lang/String;

.field private representation:Ljava/lang/String;

.field private sellerDescription:Ljava/lang/String;

.field private sellerEmail:Ljava/lang/String;

.field private sellerID:Ljava/lang/String;

.field private sellerLocation:Ljava/lang/String;

.field private sellerName:Ljava/lang/String;

.field private sellerNum:Ljava/lang/String;

.field private sellerRegisterNum:Ljava/lang/String;

.field private sellerTag:Ljava/lang/String;

.field private sellerTagFeatured1:Ljava/lang/String;

.field private sellerTagFeatured2:Ljava/lang/String;

.field private sellerTradeName:Ljava/lang/String;

.field private sellerUrl:Ljava/lang/String;

.field private supportEmail:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->_bValid:Z

    .line 208
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->_Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 39
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->_DetailMain:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    .line 40
    return-void
.end method


# virtual methods
.method public addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->_Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->_Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 227
    :cond_0
    return-void
.end method

.method public clearContainer()V
    .locals 0

    .prologue
    .line 231
    return-void
.end method

.method public closeMap()V
    .locals 3

    .prologue
    .line 217
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->_Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, p0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;->mappingClass(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 218
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->_Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 219
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->_bValid:Z

    .line 220
    return-void
.end method

.method public findString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 240
    const/4 v0, 0x0

    return-object v0
.end method

.method public getVcategoryID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->categoryID:Ljava/lang/String;

    return-object v0
.end method

.method public getVcategoryID2()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->categoryID2:Ljava/lang/String;

    return-object v0
.end method

.method public getVcategoryName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->categoryName:Ljava/lang/String;

    return-object v0
.end method

.method public getVcategoryName2()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->categoryName2:Ljava/lang/String;

    return-object v0
.end method

.method public getVreportNum()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->reportNum:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 132
    const-string v0, ""

    .line 134
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->reportNum:Ljava/lang/String;

    goto :goto_0
.end method

.method public getVrepresentation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->representation:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 116
    const-string v0, ""

    .line 118
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->representation:Ljava/lang/String;

    goto :goto_0
.end method

.method public getVsellerDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->sellerDescription:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 84
    const-string v0, ""

    .line 86
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->sellerDescription:Ljava/lang/String;

    goto :goto_0
.end method

.method public getVsellerEmail()Ljava/lang/String;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->sellerEmail:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 180
    const-string v0, ""

    .line 182
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->sellerEmail:Ljava/lang/String;

    goto :goto_0
.end method

.method public getVsellerID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->sellerID:Ljava/lang/String;

    return-object v0
.end method

.method public getVsellerLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->sellerLocation:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 148
    const-string v0, ""

    .line 150
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->sellerLocation:Ljava/lang/String;

    goto :goto_0
.end method

.method public getVsellerName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->sellerName:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 75
    const-string v0, ""

    .line 77
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->sellerName:Ljava/lang/String;

    goto :goto_0
.end method

.method public getVsellerNum()Ljava/lang/String;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->sellerNum:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 140
    const-string v0, ""

    .line 142
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->sellerNum:Ljava/lang/String;

    goto :goto_0
.end method

.method public getVsellerRegisterNum()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->sellerRegisterNum:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 124
    const-string v0, ""

    .line 126
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->sellerRegisterNum:Ljava/lang/String;

    goto :goto_0
.end method

.method public getVsellerTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->sellerTag:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 172
    const-string v0, ""

    .line 174
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->sellerTag:Ljava/lang/String;

    goto :goto_0
.end method

.method public getVsellerTagFeatured1()Ljava/lang/String;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->sellerTagFeatured1:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 156
    const-string v0, ""

    .line 158
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->sellerTagFeatured1:Ljava/lang/String;

    goto :goto_0
.end method

.method public getVsellerTagFeatured2()Ljava/lang/String;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->sellerTagFeatured2:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 164
    const-string v0, ""

    .line 166
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->sellerTagFeatured2:Ljava/lang/String;

    goto :goto_0
.end method

.method public getVsellerTradeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->sellerTradeName:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 108
    const-string v0, ""

    .line 110
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->sellerTradeName:Ljava/lang/String;

    goto :goto_0
.end method

.method public getVsellerUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->sellerUrl:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 100
    const-string v0, ""

    .line 102
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->sellerUrl:Ljava/lang/String;

    goto :goto_0
.end method

.method public getVsupportEmail()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->supportEmail:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 92
    const-string v0, ""

    .line 94
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->supportEmail:Ljava/lang/String;

    goto :goto_0
.end method

.method public isDisplaySellerInfo()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 198
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    .line 200
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isKorea()Z

    move-result v1

    if-ne v1, v0, :cond_0

    .line 204
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDisplaySellerLocation()Z
    .locals 2

    .prologue
    .line 187
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->sellerRegisterNum:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->sellerRegisterNum:Ljava/lang/String;

    const-string v1, ""

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->_DetailMain:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isFreeContent()Z

    move-result v0

    if-nez v0, :cond_2

    .line 191
    :cond_1
    const/4 v0, 0x1

    .line 193
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->_bValid:Z

    return v0
.end method

.method public openMap()V
    .locals 1

    .prologue
    .line 211
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->_Map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 213
    return-void
.end method

.method public setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 245
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 235
    const/4 v0, 0x0

    return v0
.end method
