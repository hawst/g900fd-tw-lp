.class public Lcom/sec/android/app/samsungapps/viewholder/DetailContentListViewHolder;
.super Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;
.source "ProGuard"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;Landroid/content/Context;Landroid/view/View;Landroid/graphics/drawable/Drawable;Ljava/lang/String;ZILcom/sec/android/app/samsungapps/view/IContentArrayAdapter;I)V
    .locals 0

    .prologue
    .line 13
    invoke-direct/range {p0 .. p9}, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;-><init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;Landroid/content/Context;Landroid/view/View;Landroid/graphics/drawable/Drawable;Ljava/lang/String;ZILcom/sec/android/app/samsungapps/view/IContentArrayAdapter;I)V

    .line 14
    return-void
.end method


# virtual methods
.method public displayNormalItem()V
    .locals 2

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/DetailContentListViewHolder;->divider:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/DetailContentListViewHolder;->divider:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 19
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/DetailContentListViewHolder;->showProductRating()V

    .line 20
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->displayNormalItem()V

    .line 21
    return-void
.end method
