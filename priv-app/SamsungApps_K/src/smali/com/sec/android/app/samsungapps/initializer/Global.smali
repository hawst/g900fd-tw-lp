.class public Lcom/sec/android/app/samsungapps/initializer/Global;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field static b:I

.field private static e:Lcom/sec/android/app/samsungapps/initializer/Global;

.field private static i:Z


# instance fields
.field a:Z

.field c:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ISettingsProviderCreator;

.field d:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;

.field private f:Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/CategoryCommandBuilder;

.field private g:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeCommandBuilder;

.field private h:Lcom/sec/android/app/samsungapps/initializer/k;

.field private j:Lcom/samsung/minizip/SignatureChecker;

.field private k:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;

.field private l:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;

.field private m:Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;

.field private n:Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager;

.field private o:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/PreloadAppUpdaterFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/samsungapps/initializer/Global;->i:Z

    .line 336
    const/16 v0, 0x384

    sput v0, Lcom/sec/android/app/samsungapps/initializer/Global;->b:I

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 17

    .prologue
    .line 119
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 96
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/samsungapps/initializer/Global;->f:Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/CategoryCommandBuilder;

    .line 97
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/samsungapps/initializer/Global;->g:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeCommandBuilder;

    .line 98
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/samsungapps/initializer/Global;->h:Lcom/sec/android/app/samsungapps/initializer/k;

    .line 101
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/samsungapps/initializer/Global;->j:Lcom/samsung/minizip/SignatureChecker;

    .line 161
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/samsungapps/initializer/Global;->a:Z

    .line 163
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/samsungapps/initializer/Global;->k:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;

    .line 164
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/samsungapps/initializer/Global;->l:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;

    .line 191
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/samsungapps/initializer/Global;->m:Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;

    .line 192
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/samsungapps/initializer/Global;->n:Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager;

    .line 479
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/samsungapps/initializer/Global;->c:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ISettingsProviderCreator;

    .line 120
    monitor-enter p0

    .line 121
    if-nez p1, :cond_0

    .line 122
    monitor-exit p0

    .line 158
    :goto_0
    return-void

    .line 125
    :cond_0
    :try_start_0
    new-instance v2, Lcom/samsung/minizip/SignatureChecker;

    move-object/from16 v0, p1

    invoke-direct {v2, v0}, Lcom/samsung/minizip/SignatureChecker;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/samsungapps/initializer/Global;->j:Lcom/samsung/minizip/SignatureChecker;

    .line 126
    sget-object v2, Lcom/sec/android/app/samsungapps/Main;->contentNotificationManager:Lcom/sec/android/app/samsungapps/ContentNotificationManager;

    if-nez v2, :cond_1

    .line 127
    new-instance v2, Lcom/sec/android/app/samsungapps/ContentNotificationManager;

    new-instance v3, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;

    move-object/from16 v0, p1

    invoke-direct {v3, v0}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p1

    invoke-direct {v2, v0, v3}, Lcom/sec/android/app/samsungapps/ContentNotificationManager;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationManager;)V

    sput-object v2, Lcom/sec/android/app/samsungapps/Main;->contentNotificationManager:Lcom/sec/android/app/samsungapps/ContentNotificationManager;

    .line 131
    :cond_1
    new-instance v6, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;

    invoke-direct {v6}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;-><init>()V

    .line 132
    new-instance v4, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;

    move-object/from16 v0, p1

    invoke-direct {v4, v0, v6}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;)V

    .line 133
    new-instance v13, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;

    move-object/from16 v0, p1

    invoke-direct {v13, v0, v6}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;)V

    .line 134
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    .line 135
    new-instance v5, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;

    move-object/from16 v0, p1

    invoke-direct {v5, v0, v6}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;)V

    new-instance v7, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;

    move-object/from16 v0, p1

    invoke-direct {v7, v0}, Lcom/sec/android/app/samsungapps/SamsungAccountInteractor;-><init>(Landroid/content/Context;)V

    new-instance v8, Lcom/sec/android/app/samsungapps/MigrationProceeder;

    invoke-direct {v8}, Lcom/sec/android/app/samsungapps/MigrationProceeder;-><init>()V

    new-instance v9, Lcom/sec/android/app/samsungapps/initializer/g;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v9, v0, v1}, Lcom/sec/android/app/samsungapps/initializer/g;-><init>(Lcom/sec/android/app/samsungapps/initializer/Global;Landroid/content/Context;)V

    const/4 v10, 0x0

    new-instance v11, Lcom/sec/android/app/samsungapps/initializer/Config;

    invoke-direct {v11}, Lcom/sec/android/app/samsungapps/initializer/Config;-><init>()V

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    new-instance v12, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilderFactory;

    invoke-static {}, Lcom/sec/android/app/samsungapps/ErrorHandler;->getInstance()Lcom/sec/android/app/samsungapps/ErrorHandler;

    move-result-object v14

    const/4 v15, 0x0

    new-instance v16, Lcom/sec/android/app/samsungapps/vlibrary/net/CRequestPOSTFactory;

    move-object/from16 v0, v16

    invoke-direct {v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/net/CRequestPOSTFactory;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v16

    invoke-direct {v12, v3, v14, v15, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilderFactory;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;Lcom/sec/android/app/samsungapps/vlibrary/net/RequestSendingProgressObserver;Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOSTFactory;)V

    move-object/from16 v3, p1

    invoke-virtual/range {v2 .. v12}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->init(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SamsungAccountExistChecker;Lcom/sec/android/app/samsungapps/vlibrary/viewInteractor/InfCheckMigrationRequired;Lcom/sec/android/app/samsungapps/vlibrary/doc/IVersionProvider;ZLcom/sec/android/app/samsungapps/vlibrary/util/IConfig;Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilderFactory;)V

    .line 144
    new-instance v3, Lcom/sec/android/app/samsungapps/ConcreteViewCaller;

    invoke-direct {v3}, Lcom/sec/android/app/samsungapps/ConcreteViewCaller;-><init>()V

    .line 145
    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->setViewCaller(Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ViewCaller;)V

    .line 146
    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getNetAPI()Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    move-result-object v3

    .line 148
    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->init()V

    .line 149
    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getNetAPI()Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->increaseStageNo()V

    .line 150
    const/16 v3, 0x1e

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->setItemCountPerPage(I)V

    .line 152
    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/CategoryCommandBuilder;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCategoryContainer()Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

    move-result-object v2

    invoke-direct {v3, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/CategoryCommandBuilder;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/app/samsungapps/initializer/Global;->f:Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/CategoryCommandBuilder;

    .line 153
    new-instance v2, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;

    move-object/from16 v0, p1

    invoke-direct {v2, v0, v13}, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/samsungapps/initializer/Global;->g:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeCommandBuilder;

    .line 155
    new-instance v5, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;

    new-instance v3, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;

    move-object/from16 v0, p1

    invoke-direct {v3, v0}, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p1

    invoke-direct {v2, v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;)V

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverFactoryForSellerAppAutoUpdate;

    invoke-direct {v3}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverFactoryForSellerAppAutoUpdate;-><init>()V

    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v3, v4}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDownloadSingleItemCreator(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieverFactory;Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManagerCreater;)Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-direct {v5, v0, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/IDownloaderCreator;)V

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverFactoryForPreloadAppAutoUpdate;

    invoke-direct {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverFactoryForPreloadAppAutoUpdate;-><init>()V

    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDownloadSingleItemCreator(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieverFactory;Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManagerCreater;)Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/samsungapps/initializer/Global;->o:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/PreloadAppUpdaterFactory;

    if-nez v3, :cond_2

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterFactory;

    invoke-direct {v3, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterFactory;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/IDownloaderCreator;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/app/samsungapps/initializer/Global;->o:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/PreloadAppUpdaterFactory;

    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/samsungapps/initializer/Global;->o:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/PreloadAppUpdaterFactory;

    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getSettingsProviderCreator(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ISettingsProviderCreator;

    move-result-object v3

    new-instance v7, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;

    const/4 v9, 0x0

    invoke-static {v6}, Lcom/sec/android/app/samsungapps/initializer/Global;->a(Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;)Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerFactory;

    move-result-object v10

    new-instance v11, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckManagerFactory;

    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getPreloadTncAgreementChecker(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/PreloadTnCAgreementChecker;

    move-result-object v2

    invoke-direct {v11, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckManagerFactory;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/PreloadTnCAgreementChecker;)V

    new-instance v12, Lcom/sec/android/app/samsungapps/notipopup/NotiPopupFactory;

    invoke-direct {v12}, Lcom/sec/android/app/samsungapps/notipopup/NotiPopupFactory;-><init>()V

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ISettingsProviderCreator;->createAutoUpdateNotification()Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SettingsProvider;

    move-result-object v13

    move-object/from16 v8, p1

    invoke-direct/range {v7 .. v13}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerFactory;Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckManagerFactory;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SettingsProvider;)V

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/sec/android/app/samsungapps/initializer/Global;->l:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    new-instance v4, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;

    move-object/from16 v0, p1

    invoke-direct {v4, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;)V

    new-instance v2, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;

    move-object/from16 v0, p1

    invoke-direct {v2, v0}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;-><init>(Landroid/content/Context;)V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->createInstallerFactory()Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

    move-result-object v7

    new-instance v8, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManagerFactory;

    new-instance v9, Lcom/sec/android/app/samsungapps/initializer/i;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v9, v0, v1}, Lcom/sec/android/app/samsungapps/initializer/i;-><init>(Lcom/sec/android/app/samsungapps/initializer/Global;Landroid/content/Context;)V

    move-object/from16 v0, p1

    invoke-direct {v8, v0, v2, v7, v9}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateManagerFactory;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationManager;Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/ISetForegroundProcess;)V

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ISettingsProviderCreator;->createAutoUpdateNotification()Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SettingsProvider;

    move-result-object v9

    invoke-static {v6}, Lcom/sec/android/app/samsungapps/initializer/Global;->a(Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;)Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerFactory;

    move-result-object v7

    move-object/from16 v3, p1

    move-object v6, v9

    invoke-direct/range {v2 .. v8}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerAppAutoUpdateManager;Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SettingsProvider;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerFactory;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/ISelfUpdateManagerFactory;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/samsungapps/initializer/Global;->k:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;

    .line 157
    new-instance v2, Lcom/sec/android/app/samsungapps/initializer/k;

    move-object/from16 v0, p1

    invoke-direct {v2, v0}, Lcom/sec/android/app/samsungapps/initializer/k;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/samsungapps/initializer/Global;->h:Lcom/sec/android/app/samsungapps/initializer/k;

    .line 158
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method private a(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorCheckerFactory;
    .locals 3

    .prologue
    .line 504
    new-instance v0, Lcom/sec/android/app/samsungapps/helper/TrukeyErrorPopup;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/helper/TrukeyErrorPopup;-><init>(Landroid/content/Context;)V

    .line 505
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/initializer/Global;->createNetworkErrorPopup(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;

    move-result-object v1

    .line 506
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorCheckerFactory;

    invoke-direct {v2, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorCheckerFactory;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;)V

    .line 507
    return-object v2
.end method

.method private static a(Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;)Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerFactory;
    .locals 1

    .prologue
    .line 252
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CAutoUpdateTriggerFactory;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CAutoUpdateTriggerFactory;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;)V

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/initializer/Global;)V
    .locals 3

    .prologue
    .line 93
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/Global;->n:Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager;->stopForeground()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Global::onStopForeground()::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/initializer/Global;)V
    .locals 3

    .prologue
    .line 93
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/Global;->n:Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager;->startForeground()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Global::onStartForeground()::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static clearBadgeCount(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 374
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/BadgeNotification;->setBadgeNotification(Landroid/content/Context;I)Z

    .line 375
    return-void
.end method

.method public static clearInitialized()V
    .locals 1

    .prologue
    .line 445
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/samsungapps/initializer/Global;->i:Z

    .line 446
    return-void
.end method

.method public static clearUnaSetting(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 382
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 383
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;-><init>(Landroid/content/Context;)V

    .line 384
    const/16 v2, 0x48

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->setDbData(ILjava/lang/String;)Z

    .line 386
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;-><init>(Landroid/content/Context;)V

    .line 387
    const-string v2, "una_setting"

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setSharedConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 392
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_0

    .line 393
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "una_setting"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 395
    :cond_0
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;
    .locals 1

    .prologue
    .line 104
    sget-object v0, Lcom/sec/android/app/samsungapps/initializer/Global;->e:Lcom/sec/android/app/samsungapps/initializer/Global;

    if-nez v0, :cond_0

    .line 105
    new-instance v0, Lcom/sec/android/app/samsungapps/initializer/Global;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/initializer/Global;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/initializer/Global;->e:Lcom/sec/android/app/samsungapps/initializer/Global;

    .line 107
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/initializer/Global;->e:Lcom/sec/android/app/samsungapps/initializer/Global;

    return-object v0
.end method

.method public static isInitialized()Z
    .locals 1

    .prologue
    .line 430
    sget-boolean v0, Lcom/sec/android/app/samsungapps/initializer/Global;->i:Z

    return v0
.end method

.method public static reInit(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 111
    new-instance v0, Lcom/sec/android/app/samsungapps/initializer/Global;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/initializer/Global;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/samsungapps/initializer/Global;->e:Lcom/sec/android/app/samsungapps/initializer/Global;

    .line 112
    return-void
.end method

.method public static setInitialized(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 434
    sput-boolean v1, Lcom/sec/android/app/samsungapps/initializer/Global;->i:Z

    .line 437
    if-eqz p0, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/TencentUtil;->isAvailableTencent(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v1, :cond_0

    .line 438
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.tencent.android.action.InitData"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 439
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 440
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 442
    :cond_0
    return-void
.end method


# virtual methods
.method public OTAdownloadContent(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieverFactory;)V
    .locals 1

    .prologue
    .line 363
    new-instance v0, Lcom/sec/android/app/samsungapps/downloadhelper/CPurchaseManagerCreater;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/downloadhelper/CPurchaseManagerCreater;-><init>()V

    invoke-virtual {p0, p1, p3, v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDownloadSingleItemCreator(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieverFactory;Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManagerCreater;)Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->downloadContent(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;)V

    .line 365
    return-void
.end method

.method public cancelDownload(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 349
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->getItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    move-result-object v0

    .line 350
    if-eqz v0, :cond_0

    .line 351
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->userCancel()V

    .line 352
    const/4 v0, 0x1

    .line 355
    :goto_0
    return v0

    .line 354
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "cancelDownload::  "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 355
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public createDownloadURLWithoutLoginFactory()Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieverFactory;
    .locals 1

    .prologue
    .line 296
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverWithoutLoginFactory;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverWithoutLoginFactory;-><init>()V

    return-object v0
.end method

.method public createInstallerFactory()Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;
    .locals 2

    .prologue
    .line 495
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/CInstallerFactory;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/initializer/Global;->j:Lcom/samsung/minizip/SignatureChecker;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/CInstallerFactory;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigChecker$ISignatureMethod;)V

    .line 496
    return-object v0
.end method

.method public createLogin()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 398
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->createLogin(Z)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    return-object v0
.end method

.method public createLogin(Z)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 2

    .prologue
    .line 402
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;-><init>()V

    .line 403
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;->createLoginValidator(ZLcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    return-object v0
.end method

.method public createLoginAfterSessionExpired()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 3

    .prologue
    .line 407
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;-><init>()V

    .line 408
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;->clearLoginSession()V

    .line 409
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;->createLoginValidator(ZLcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    return-object v0
.end method

.method public createNetworkErrorPopup(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup;
    .locals 1

    .prologue
    .line 511
    new-instance v0, Lcom/sec/android/app/samsungapps/networkerrorpopup/NetworkErrorPopup;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/networkerrorpopup/NetworkErrorPopup;-><init>()V

    .line 512
    return-object v0
.end method

.method public createWishItemCommandBuilder(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;)Lcom/sec/android/app/samsungapps/commands/WishItemCommandBuilder;
    .locals 2

    .prologue
    .line 421
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/WishItemCommandBuilder;

    new-instance v1, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;-><init>()V

    invoke-direct {v0, p1, v1}, Lcom/sec/android/app/samsungapps/commands/WishItemCommandBuilder;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;)V

    return-object v0
.end method

.method public downloadContent(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;)V
    .locals 1

    .prologue
    .line 332
    const/4 v0, 0x0

    invoke-virtual {p3, p1, p2, v0}, Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;->createDownloader(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Z)Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    move-result-object v0

    .line 333
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->execute()V

    .line 334
    return-void
.end method

.method public getAutoUpdateManager()Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/Global;->k:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;

    return-object v0
.end method

.method public getAutoUpdateTriggerFactory()Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerFactory;
    .locals 2

    .prologue
    .line 256
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;-><init>()V

    .line 257
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CAutoUpdateTriggerFactory;

    invoke-direct {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CAutoUpdateTriggerFactory;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;)V

    return-object v1
.end method

.method public getCategoryCommandBuilder()Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/CategoryCommandBuilder;
    .locals 1

    .prologue
    .line 413
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/Global;->f:Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/CategoryCommandBuilder;

    return-object v0
.end method

.method public getCheckAppUpgradeCommandBuilder()Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeCommandBuilder;
    .locals 1

    .prologue
    .line 417
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/Global;->g:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeCommandBuilder;

    return-object v0
.end method

.method public getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;
    .locals 1

    .prologue
    .line 327
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    return-object v0
.end method

.method public getDownloadSingleItemCreator(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieverFactory;Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManagerCreater;)Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;
    .locals 6

    .prologue
    .line 186
    new-instance v0, Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->createInstallerFactory()Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

    move-result-object v2

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/initializer/Global;->m:Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;

    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/initializer/Global;->m:Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;

    :goto_0
    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieverFactory;Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManagerCreater;)V

    .line 188
    return-object v0

    .line 186
    :cond_0
    new-instance v1, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager;

    invoke-direct {v1, p1}, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/initializer/Global;->n:Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;

    new-instance v3, Lcom/sec/android/app/samsungapps/initializer/h;

    invoke-direct {v3, p0, p1}, Lcom/sec/android/app/samsungapps/initializer/h;-><init>(Lcom/sec/android/app/samsungapps/initializer/Global;Landroid/content/Context;)V

    invoke-direct {v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager$IResourceStateReceiver;)V

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/initializer/Global;->m:Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/initializer/Global;->m:Lcom/sec/android/app/samsungapps/vlibrary3/installer/reslockmanager/ResourceLockManager;

    goto :goto_0
.end method

.method public getEmergencyUpdateRunner(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;
    .locals 3

    .prologue
    .line 534
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/Global;->d:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;

    if-eqz v0, :cond_0

    .line 535
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/Global;->d:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;

    .line 538
    :goto_0
    return-object v0

    .line 537
    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/initializer/Global;->o:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/PreloadAppUpdaterFactory;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->notiPopup()Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;

    move-result-object v2

    invoke-direct {v0, p1, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/PreloadAppUpdaterFactory;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/Global;->d:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;

    .line 538
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/Global;->d:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;

    goto :goto_0
.end method

.method public getInstallChecker(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;
    .locals 2

    .prologue
    .line 553
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IBaseContextUtil;->getFakeModel(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IBaseContextUtil;->getGearOSVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 554
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;-><init>(Landroid/content/Context;)V

    .line 555
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;->connect()V

    .line 556
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WGTInstallChecker;

    invoke-direct {v0, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WGTInstallChecker;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;)V

    .line 559
    :goto_1
    return-object v0

    .line 553
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 559
    :cond_1
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;-><init>(Landroid/content/Context;)V

    goto :goto_1
.end method

.method public getPreloadAppUpdaterFactory()Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/PreloadAppUpdaterFactory;
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/Global;->o:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/PreloadAppUpdaterFactory;

    return-object v0
.end method

.method public getPreloadTncAgreementChecker(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/PreloadTnCAgreementChecker;
    .locals 2

    .prologue
    .line 489
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/EulaAgreementFactory;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/EulaAgreementFactory;-><init>()V

    .line 490
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CCarrierDecisionTableFactory;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CCarrierDecisionTableFactory;-><init>()V

    .line 491
    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/EulaAgreementFactory;->createTncAgreementChecker(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/mobilenetwork/CarrierDecisionTableFactory;)Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/PreloadTnCAgreementChecker;

    move-result-object v0

    return-object v0
.end method

.method public getPreloadUpdateManager()Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/Global;->l:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;

    return-object v0
.end method

.method public getSettingsProviderCreator(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ISettingsProviderCreator;
    .locals 1

    .prologue
    .line 482
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/Global;->c:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ISettingsProviderCreator;

    if-nez v0, :cond_0

    .line 483
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SettingsProviderCreator;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SettingsProviderCreator;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/Global;->c:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ISettingsProviderCreator;

    .line 485
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/Global;->c:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ISettingsProviderCreator;

    return-object v0
.end method

.method public initializer(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializerFactory;
    .locals 4

    .prologue
    .line 516
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/CInitializerFactory;

    new-instance v1, Lcom/sec/android/app/samsungapps/commands/InitializeCommandBuilder;

    invoke-direct {v1, p1}, Lcom/sec/android/app/samsungapps/commands/InitializeCommandBuilder;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/initializer/Global;->a(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorCheckerFactory;

    move-result-object v2

    invoke-static {p1}, Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/CInitializerFactory;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializeCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorCheckerFactory;Lcom/sec/android/app/samsungapps/vlibrary2/initialize/ICheckAppResultObserver;)V

    return-object v0
.end method

.method public initializerWithOutLogin(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializerFactory;
    .locals 4

    .prologue
    .line 521
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/CInitializerFactory;

    new-instance v1, Lcom/sec/android/app/samsungapps/initializer/j;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/initializer/j;-><init>(Lcom/sec/android/app/samsungapps/initializer/Global;Landroid/content/Context;)V

    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/initializer/Global;->a(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/NetworkErrorCheckerFactory;

    move-result-object v2

    invoke-static {p1}, Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/viewpager/IAPUpdateChecker;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/CInitializerFactory;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializeCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorCheckerFactory;Lcom/sec/android/app/samsungapps/vlibrary2/initialize/ICheckAppResultObserver;)V

    return-object v0
.end method

.method public isCancellable(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 339
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->getItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    move-result-object v0

    .line 340
    if-eqz v0, :cond_0

    .line 341
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->isCancellable()Z

    move-result v0

    .line 344
    :goto_0
    return v0

    .line 343
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "isCancellable::  error"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 344
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLogedIn()Z
    .locals 1

    .prologue
    .line 425
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;-><init>()V

    .line 426
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;->isLogedIn()Z

    move-result v0

    return v0
.end method

.method public isTalkbackEnabled(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 449
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/Global;->h:Lcom/sec/android/app/samsungapps/initializer/k;

    if-nez v0, :cond_0

    .line 450
    const/4 v0, 0x0

    .line 452
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/Global;->h:Lcom/sec/android/app/samsungapps/initializer/k;

    iget-boolean v0, v0, Lcom/sec/android/app/samsungapps/initializer/k;->a:Z

    goto :goto_0
.end method

.method public notiPopup()Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;
    .locals 1

    .prologue
    .line 500
    new-instance v0, Lcom/sec/android/app/samsungapps/notipopup/NotiPopupFactory;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/notipopup/NotiPopupFactory;-><init>()V

    return-object v0
.end method

.method public readUpdateCount(Landroid/content/Context;)I
    .locals 1

    .prologue
    .line 378
    const/4 v0, 0x0

    return v0
.end method

.method public refreshTalkbackEnabled(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/Global;->h:Lcom/sec/android/app/samsungapps/initializer/k;

    if-nez v0, :cond_0

    .line 461
    :goto_0
    return-void

    .line 459
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/Global;->h:Lcom/sec/android/app/samsungapps/initializer/k;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/initializer/k;->a(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public setAutoUpdateManager(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;)V
    .locals 3

    .prologue
    .line 172
    if-eqz p1, :cond_0

    .line 173
    :try_start_0
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/initializer/Global;->k:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 178
    :cond_0
    :goto_0
    return-void

    .line 175
    :catch_0
    move-exception v0

    .line 176
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Global::setAutoUpdateManager()::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public updateContent(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieverFactory;)V
    .locals 1

    .prologue
    .line 369
    new-instance v0, Lcom/sec/android/app/samsungapps/downloadhelper/CPurchaseManagerCreater;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/downloadhelper/CPurchaseManagerCreater;-><init>()V

    invoke-virtual {p0, p1, p3, v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDownloadSingleItemCreator(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieverFactory;Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseManagerCreater;)Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->downloadContent(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;)V

    .line 371
    return-void
.end method
