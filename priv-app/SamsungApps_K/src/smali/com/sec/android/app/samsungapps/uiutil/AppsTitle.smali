.class public Lcom/sec/android/app/samsungapps/uiutil/AppsTitle;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static a:Z

.field private static b:Z

.field private static c:Z

.field private static d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    sput-boolean v0, Lcom/sec/android/app/samsungapps/uiutil/AppsTitle;->a:Z

    .line 11
    sput-boolean v0, Lcom/sec/android/app/samsungapps/uiutil/AppsTitle;->b:Z

    .line 13
    sput-boolean v0, Lcom/sec/android/app/samsungapps/uiutil/AppsTitle;->c:Z

    .line 14
    sput-boolean v0, Lcom/sec/android/app/samsungapps/uiutil/AppsTitle;->d:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getString(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 35
    if-nez p0, :cond_0

    .line 37
    const-string v0, ""

    .line 45
    :goto_0
    return-object v0

    .line 40
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/samsungapps/uiutil/AppsTitle;->a:Z

    if-nez v0, :cond_1

    sput-boolean v1, Lcom/sec/android/app/samsungapps/uiutil/AppsTitle;->a:Z

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/samsungapps/uiutil/AppsTitle;->b:Z

    :cond_1
    sget-boolean v0, Lcom/sec/android/app/samsungapps/uiutil/AppsTitle;->b:Z

    if-eqz v0, :cond_2

    .line 41
    const v0, 0x7f080133

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 42
    :cond_2
    sget-boolean v0, Lcom/sec/android/app/samsungapps/uiutil/AppsTitle;->c:Z

    if-nez v0, :cond_3

    sput-boolean v1, Lcom/sec/android/app/samsungapps/uiutil/AppsTitle;->c:Z

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IBaseContextUtil;->isGearMode(Landroid/content/Context;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/samsungapps/uiutil/AppsTitle;->d:Z

    :cond_3
    sget-boolean v0, Lcom/sec/android/app/samsungapps/uiutil/AppsTitle;->d:Z

    if-eqz v0, :cond_4

    .line 43
    const-string v0, "GearApps"

    goto :goto_0

    .line 45
    :cond_4
    const v0, 0x7f0802fa

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
