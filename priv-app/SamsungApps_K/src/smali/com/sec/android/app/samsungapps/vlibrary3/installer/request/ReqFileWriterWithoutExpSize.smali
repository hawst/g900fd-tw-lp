.class public Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriterWithoutExpSize;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/IFileWriter;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;

.field private b:Landroid/content/Context;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter$IReqFileWriterObserver;

.field private d:Z

.field private e:Ljava/io/FileOutputStream;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriterWithoutExpSize;->b:Landroid/content/Context;

    .line 30
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;

    invoke-static {p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/FileCreator;->internalStorage(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriterWithoutExpSize;->a:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;

    .line 31
    return-void
.end method

.method private a()Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter$HFileWriteOpenResult;
    .locals 3

    .prologue
    .line 65
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriterWithoutExpSize;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriterWithoutExpSize;->a:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;->getFileName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriterWithoutExpSize;->e:Ljava/io/FileOutputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter$HFileWriteOpenResult;->OPEN_NEWFILE:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter$HFileWriteOpenResult;

    :goto_0
    return-object v0

    .line 66
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 68
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter$HFileWriteOpenResult;->OPEN_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter$HFileWriteOpenResult;

    goto :goto_0
.end method

.method private a(J)V
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriterWithoutExpSize;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter$IReqFileWriterObserver;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriterWithoutExpSize;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter$IReqFileWriterObserver;

    invoke-interface {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter$IReqFileWriterObserver;->onProgress(J)V

    .line 145
    :cond_0
    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriterWithoutExpSize;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter$IReqFileWriterObserver;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriterWithoutExpSize;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter$IReqFileWriterObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter$IReqFileWriterObserver;->onCancelCompleted()V

    .line 153
    :cond_0
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 174
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriterWithoutExpSize;->d:Z

    .line 175
    return-void
.end method

.method public close()V
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriterWithoutExpSize;->e:Ljava/io/FileOutputStream;

    if-eqz v0, :cond_0

    .line 49
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriterWithoutExpSize;->e:Ljava/io/FileOutputStream;

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 55
    :cond_0
    :goto_0
    return-void

    .line 50
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public deleteFile()V
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriterWithoutExpSize;->a:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;->deleteFile()Z

    .line 42
    return-void
.end method

.method public download(Ljava/io/InputStream;)Z
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 94
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriterWithoutExpSize;->d:Z

    .line 96
    :try_start_0
    new-instance v4, Ljava/io/BufferedInputStream;

    const/16 v1, 0x2000

    invoke-direct {v4, p1, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 100
    const/16 v1, 0x2000

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 101
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    .line 102
    const-wide/16 v1, 0x0

    move v3, v0

    .line 105
    :cond_0
    const/4 v6, 0x0

    const/16 v7, 0x2000

    invoke-virtual {v4, v5, v6, v7}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_3

    .line 106
    iget-object v7, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriterWithoutExpSize;->e:Ljava/io/FileOutputStream;

    const/4 v8, 0x0

    invoke-virtual {v7, v5, v8, v6}, Ljava/io/FileOutputStream;->write([BII)V

    .line 107
    iget-object v7, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriterWithoutExpSize;->e:Ljava/io/FileOutputStream;

    invoke-virtual {v7}, Ljava/io/FileOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 109
    const-wide/16 v7, 0x1

    :try_start_1
    invoke-static {v7, v8}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 113
    :goto_0
    add-int/2addr v3, v6

    .line 114
    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    long-to-double v6, v6

    sub-double/2addr v6, v1

    const-wide v8, 0x408f400000000000L    # 1000.0

    cmpl-double v6, v6, v8

    if-lez v6, :cond_1

    .line 115
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    long-to-double v1, v1

    .line 116
    int-to-long v6, v3

    invoke-direct {p0, v6, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriterWithoutExpSize;->a(J)V

    .line 118
    :cond_1
    iget-boolean v6, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriterWithoutExpSize;->d:Z

    if-eqz v6, :cond_0

    .line 119
    const-string v1, "FileWriter:download canceled"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 120
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriterWithoutExpSize;->b()V

    .line 135
    :cond_2
    :goto_1
    return v0

    .line 125
    :cond_3
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriterWithoutExpSize;->d:Z

    if-eqz v1, :cond_4

    .line 126
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriterWithoutExpSize;->b()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 134
    :catch_0
    move-exception v1

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriterWithoutExpSize;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter$IReqFileWriterObserver;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriterWithoutExpSize;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter$IReqFileWriterObserver;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter$IReqFileWriterObserver;->onWriteFailed()V

    goto :goto_1

    .line 130
    :cond_4
    int-to-long v1, v3

    :try_start_3
    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriterWithoutExpSize;->a(J)V

    .line 131
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriterWithoutExpSize;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter$IReqFileWriterObserver;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriterWithoutExpSize;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter$IReqFileWriterObserver;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter$IReqFileWriterObserver;->onWriteCompleted()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 132
    :cond_5
    const/4 v0, 0x1

    goto :goto_1

    :catch_1
    move-exception v7

    goto :goto_0
.end method

.method public getFileSize()J
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriterWithoutExpSize;->a:Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileUtil;->length()J

    move-result-wide v0

    return-wide v0
.end method

.method public open()Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter$HFileWriteOpenResult;
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriterWithoutExpSize;->a()Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/HFileWriter$HFileWriteOpenResult;

    move-result-object v0

    return-object v0
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter$IReqFileWriterObserver;)V
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriterWithoutExpSize;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/ReqFileWriter$IReqFileWriterObserver;

    .line 37
    return-void
.end method
