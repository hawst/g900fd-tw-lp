.class final Lcom/sec/android/app/samsungapps/el;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;)V
    .locals 0

    .prologue
    .line 182
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/el;->a:Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 186
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/el;->a:Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->helper:Lcom/sec/android/app/samsungapps/ActivityHelper;

    const v1, 0x7f0c01ce

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->getCheckBoxValue(I)Z

    move-result v0

    .line 187
    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/el;->a:Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->mNotification:Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->setDontDisplayAgain()V

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/el;->a:Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->_Context:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v0

    iget v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->giftCardIssueFlag:I

    if-eq v0, v2, :cond_1

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/el;->a:Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->_Context:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v0

    iget v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->voucherIssueFlag:I

    if-ne v0, v2, :cond_1

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/el;->a:Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->_Context:Landroid/content/Context;

    const/16 v1, 0x22

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;I)V

    .line 204
    :goto_0
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->dismiss()V

    .line 205
    return-void

    .line 201
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/el;->a:Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->_Context:Landroid/content/Context;

    const/16 v1, 0x21

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;I)V

    goto :goto_0
.end method
