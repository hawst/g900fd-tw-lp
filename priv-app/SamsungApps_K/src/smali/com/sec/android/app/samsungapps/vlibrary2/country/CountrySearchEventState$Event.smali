.class public final enum Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

.field public static final enum CHINA_POPUP_FAIL:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

.field public static final enum CHINA_POPUP_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

.field public static final enum COP2:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

.field public static final enum DISCALIMER_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

.field public static final enum DISCLAIMER_FAIL:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

.field public static final enum ENP2:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

.field public static final enum EXECUTE_C_SEARCH_NOT_REQ:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

.field public static final enum EXECUTE_C_SEARCH_REQ_MCCO:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

.field public static final enum EXECUTE_C_SEARCH_REQ_MCCX_GEOIPO:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

.field public static final enum EXECUTE_C_SEARCH_REQ_MCCX_GEOIPX:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

.field public static final enum EXECUTE_TESTMODE:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

.field public static final enum EXECUTE_TESTMODE_COUNTRY_LIST:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

.field public static final enum GEOIP_COUNTRY_SEARCH_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

.field public static final enum GEOIP_COUNTRY_SEARCH_FAILED_BY_IP:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

.field public static final enum GEOIP_COUNTRY_SEARCH_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

.field public static final enum IVP2:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

.field public static final enum MCC_COUNTRY_SEARCH_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

.field public static final enum MCC_COUNTRY_SEARCH_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

.field public static final enum P2USERCANCEL:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

.field public static final enum USER_GEOIP_SEARCH_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

.field public static final enum USER_GEOIP_SEARCH_RETRY:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 6
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    const-string v1, "EXECUTE_TESTMODE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->EXECUTE_TESTMODE:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    .line 7
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    const-string v1, "EXECUTE_C_SEARCH_NOT_REQ"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->EXECUTE_C_SEARCH_NOT_REQ:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    .line 8
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    const-string v1, "EXECUTE_C_SEARCH_REQ_MCCO"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->EXECUTE_C_SEARCH_REQ_MCCO:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    .line 9
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    const-string v1, "EXECUTE_C_SEARCH_REQ_MCCX_GEOIPO"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->EXECUTE_C_SEARCH_REQ_MCCX_GEOIPO:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    .line 10
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    const-string v1, "EXECUTE_C_SEARCH_REQ_MCCX_GEOIPX"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->EXECUTE_C_SEARCH_REQ_MCCX_GEOIPX:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    .line 11
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    const-string v1, "EXECUTE_TESTMODE_COUNTRY_LIST"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->EXECUTE_TESTMODE_COUNTRY_LIST:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    .line 12
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    const-string v1, "ENP2"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->ENP2:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    const-string v1, "COP2"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->COP2:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    const-string v1, "IVP2"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->IVP2:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    .line 13
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    const-string v1, "P2USERCANCEL"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->P2USERCANCEL:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    .line 14
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    const-string v1, "MCC_COUNTRY_SEARCH_SUCCESS"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->MCC_COUNTRY_SEARCH_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    .line 15
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    const-string v1, "MCC_COUNTRY_SEARCH_FAILED"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->MCC_COUNTRY_SEARCH_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    .line 16
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    const-string v1, "GEOIP_COUNTRY_SEARCH_SUCCESS"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->GEOIP_COUNTRY_SEARCH_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    .line 17
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    const-string v1, "GEOIP_COUNTRY_SEARCH_FAILED"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->GEOIP_COUNTRY_SEARCH_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    .line 18
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    const-string v1, "GEOIP_COUNTRY_SEARCH_FAILED_BY_IP"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->GEOIP_COUNTRY_SEARCH_FAILED_BY_IP:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    .line 19
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    const-string v1, "DISCALIMER_SUCCESS"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->DISCALIMER_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    .line 20
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    const-string v1, "DISCLAIMER_FAIL"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->DISCLAIMER_FAIL:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    const-string v1, "USER_GEOIP_SEARCH_RETRY"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->USER_GEOIP_SEARCH_RETRY:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    const-string v1, "USER_GEOIP_SEARCH_CANCEL"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->USER_GEOIP_SEARCH_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    .line 21
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    const-string v1, "CHINA_POPUP_SUCCESS"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->CHINA_POPUP_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    const-string v1, "CHINA_POPUP_FAIL"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->CHINA_POPUP_FAIL:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    .line 4
    const/16 v0, 0x15

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->EXECUTE_TESTMODE:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->EXECUTE_C_SEARCH_NOT_REQ:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->EXECUTE_C_SEARCH_REQ_MCCO:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->EXECUTE_C_SEARCH_REQ_MCCX_GEOIPO:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->EXECUTE_C_SEARCH_REQ_MCCX_GEOIPX:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->EXECUTE_TESTMODE_COUNTRY_LIST:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->ENP2:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->COP2:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->IVP2:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->P2USERCANCEL:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->MCC_COUNTRY_SEARCH_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->MCC_COUNTRY_SEARCH_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->GEOIP_COUNTRY_SEARCH_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->GEOIP_COUNTRY_SEARCH_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->GEOIP_COUNTRY_SEARCH_FAILED_BY_IP:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->DISCALIMER_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->DISCLAIMER_FAIL:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->USER_GEOIP_SEARCH_RETRY:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->USER_GEOIP_SEARCH_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->CHINA_POPUP_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->CHINA_POPUP_FAIL:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;
    .locals 1

    .prologue
    .line 4
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;
    .locals 1

    .prologue
    .line 4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary2/country/CountrySearchEventState$Event;

    return-object v0
.end method
