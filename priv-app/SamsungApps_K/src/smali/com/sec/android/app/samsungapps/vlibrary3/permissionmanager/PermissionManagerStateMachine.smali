.class public Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;
.source "ProGuard"


# static fields
.field private static a:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;-><init>()V

    .line 21
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine;

    if-nez v0, :cond_0

    .line 33
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine;

    .line 35
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine;

    return-object v0
.end method


# virtual methods
.method protected entry(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 3

    .prologue
    .line 113
    const-string v0, "PermissionManagerStateMachine"

    const-string v1, "entry"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 114
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/f;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 136
    :goto_0
    :pswitch_0
    return-void

    .line 117
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;->REQUEST_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 120
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;->NOTIFY_FAILURE:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 125
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;->START_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 126
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;->SHOW_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 129
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;->NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 132
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;->CHECK_PACKAGE_EXIST:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 135
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;->COMPARE_PERMISSION_WITH_APK:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 114
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method public execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;)Z
    .locals 3

    .prologue
    .line 41
    const-string v0, "PermissionManagerStateMachine"

    const-string v1, "execute"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 42
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/f;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 108
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 45
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/f;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 48
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$State;->REQUEST_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 53
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/f;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 56
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$State;->FAILURE:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 59
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$State;->CHECK_PACKAGE_EXIST:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 62
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 67
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/f;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    goto :goto_0

    .line 70
    :pswitch_7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$State;->FAILURE:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 73
    :pswitch_8
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$State;->WAITING_USERRESPONSE:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 78
    :pswitch_9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/f;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_4

    goto :goto_0

    .line 81
    :pswitch_a
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 84
    :pswitch_b
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$State;->FAILURE:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 89
    :pswitch_c
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/f;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_5

    goto :goto_0

    .line 92
    :pswitch_d
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$State;->COMPARE_PACKAGE_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 95
    :pswitch_e
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$State;->SHOW_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 100
    :pswitch_f
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/f;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_6

    goto/16 :goto_0

    .line 103
    :pswitch_10
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$State;->SHOW_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 42
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_6
        :pswitch_9
        :pswitch_c
        :pswitch_f
    .end packed-switch

    .line 45
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch

    .line 53
    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 67
    :pswitch_data_3
    .packed-switch 0x5
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 78
    :pswitch_data_4
    .packed-switch 0x7
        :pswitch_a
        :pswitch_b
    .end packed-switch

    .line 89
    :pswitch_data_5
    .packed-switch 0x9
        :pswitch_d
        :pswitch_e
    .end packed-switch

    .line 100
    :pswitch_data_6
    .packed-switch 0xb
        :pswitch_10
    .end packed-switch
.end method

.method public bridge synthetic execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 9
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;)Z

    move-result v0

    return v0
.end method

.method protected exit(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 3

    .prologue
    .line 144
    const-string v0, "PermissionManagerStateMachine"

    const-string v1, "exit"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 145
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/f;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 151
    :goto_0
    return-void

    .line 148
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;->STOP_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 145
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method
