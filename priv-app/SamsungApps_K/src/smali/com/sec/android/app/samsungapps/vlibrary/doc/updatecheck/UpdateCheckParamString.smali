.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/updatecheck/UpdateCheckParamString;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private mAppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

.field private mContext:Landroid/content/Context;

.field private mDefaultVersion:Ljava/lang/String;

.field private mPackageName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/updatecheck/UpdateCheckParamString;->mContext:Landroid/content/Context;

    .line 20
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/updatecheck/UpdateCheckParamString;->mAppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    .line 21
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/updatecheck/UpdateCheckParamString;->mPackageName:Ljava/lang/String;

    .line 22
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/updatecheck/UpdateCheckParamString;->mDefaultVersion:Ljava/lang/String;

    .line 23
    return-void
.end method


# virtual methods
.method public getVersionCodeList()Ljava/lang/String;
    .locals 4

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/updatecheck/UpdateCheckParamString;->mAppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/updatecheck/UpdateCheckParamString;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/updatecheck/UpdateCheckParamString;->mAppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/updatecheck/UpdateCheckParamString;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->getPackageVersionCode(Ljava/lang/String;)J

    move-result-wide v0

    .line 62
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/updatecheck/UpdateCheckParamString;->mPackageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "@"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 64
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 27
    const-string v0, ""

    .line 30
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/updatecheck/UpdateCheckParamString;->mAppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/updatecheck/UpdateCheckParamString;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 34
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/updatecheck/UpdateCheckParamString;->mPackageName:Ljava/lang/String;

    const-string v2, "com.sec.android.app.samsungapps"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v3, :cond_0

    .line 35
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/updatecheck/UpdateCheckParamString;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/tool/pcheck/PlatformChecker;->getInstance(Landroid/content/Context;)Lcom/sec/android/tool/pcheck/PlatformChecker;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/updatecheck/UpdateCheckParamString;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/tool/pcheck/PlatformChecker;->getAppsType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 38
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo;-><init>()V

    .line 39
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo;->getSamsungAppsVersion()Ljava/lang/String;

    move-result-object v0

    .line 50
    :goto_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v1, ""

    .line 52
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/updatecheck/UpdateCheckParamString;->mPackageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "@"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "@0"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 54
    :goto_2
    return-object v0

    .line 40
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/updatecheck/UpdateCheckParamString;->mPackageName:Ljava/lang/String;

    const-string v2, "com.sec.android.app.samsungapps.una2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v3, :cond_1

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/updatecheck/UpdateCheckParamString;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/tool/pcheck/PlatformChecker;->getInstance(Landroid/content/Context;)Lcom/sec/android/tool/pcheck/PlatformChecker;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/updatecheck/UpdateCheckParamString;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/tool/pcheck/PlatformChecker;->getAppsType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 45
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/updatecheck/UpdateCheckParamString;->mAppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/updatecheck/UpdateCheckParamString;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->getPacakgeVersionName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_0

    .line 50
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "@"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 54
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/updatecheck/UpdateCheckParamString;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "@"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/updatecheck/UpdateCheckParamString;->mDefaultVersion:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "@0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method
