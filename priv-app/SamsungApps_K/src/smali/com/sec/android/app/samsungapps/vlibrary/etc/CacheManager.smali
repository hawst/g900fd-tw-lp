.class public Lcom/sec/android/app/samsungapps/vlibrary/etc/CacheManager;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static CACHE_LIMIT_SIZE:I

.field private static CACHE_LIMIT_SIZE_FOR_HHP:I


# instance fields
.field _Context:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const/16 v0, 0x28

    sput v0, Lcom/sec/android/app/samsungapps/vlibrary/etc/CacheManager;->CACHE_LIMIT_SIZE:I

    .line 11
    const/16 v0, 0x14

    sput v0, Lcom/sec/android/app/samsungapps/vlibrary/etc/CacheManager;->CACHE_LIMIT_SIZE_FOR_HHP:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/CacheManager;->_Context:Landroid/content/Context;

    .line 15
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/CacheManager;->_Context:Landroid/content/Context;

    .line 16
    return-void
.end method

.method private getFolderSize(Ljava/io/File;)J
    .locals 6

    .prologue
    .line 55
    const-wide/16 v1, 0x0

    .line 57
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 58
    if-eqz v3, :cond_1

    .line 59
    const/4 v0, 0x0

    :goto_0
    array-length v4, v3

    if-ge v0, v4, :cond_1

    .line 60
    aget-object v4, v3, v0

    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 61
    aget-object v4, v3, v0

    invoke-direct {p0, v4}, Lcom/sec/android/app/samsungapps/vlibrary/etc/CacheManager;->getFolderSize(Ljava/io/File;)J

    move-result-wide v4

    add-long/2addr v1, v4

    .line 59
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 64
    :cond_0
    aget-object v4, v3, v0

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v4

    add-long/2addr v1, v4

    goto :goto_1

    .line 69
    :cond_1
    return-wide v1
.end method


# virtual methods
.method public ClearFileCache()V
    .locals 6

    .prologue
    .line 19
    sget v0, Lcom/sec/android/app/samsungapps/vlibrary/etc/CacheManager;->CACHE_LIMIT_SIZE_FOR_HHP:I

    .line 20
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isTablet()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 22
    sget v0, Lcom/sec/android/app/samsungapps/vlibrary/etc/CacheManager;->CACHE_LIMIT_SIZE:I

    .line 24
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/CacheManager;->_Context:Landroid/content/Context;

    if-nez v1, :cond_2

    .line 51
    :cond_1
    return-void

    .line 28
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/etc/CacheManager;->_Context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    .line 29
    if-eqz v1, :cond_1

    .line 31
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 33
    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/CacheManager;->getFolderSize(Ljava/io/File;)J

    move-result-wide v2

    const-wide/32 v4, 0x100000

    div-long/2addr v2, v4

    long-to-double v2, v2

    .line 34
    int-to-double v4, v0

    cmpl-double v0, v2, v4

    if-lez v0, :cond_1

    .line 36
    invoke-virtual {v1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    .line 37
    if-eqz v2, :cond_1

    .line 38
    const/4 v0, 0x0

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_1

    .line 39
    new-instance v3, Ljava/io/File;

    aget-object v4, v2, v0

    invoke-direct {v3, v1, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 41
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
