.class public Lcom/sec/android/app/samsungapps/permission/CPermissionManagerFactory;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/IPermissionManagerFactory;


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/permission/CPermissionManagerFactory;->a:Z

    .line 22
    return-void
.end method

.method static synthetic a(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 17
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isUnifiedBillingSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xa

    if-lt v0, v1, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isSAccountVersionHigherThan1_3_001(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public create(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/IPermissionManager;
    .locals 6

    .prologue
    .line 27
    iget-boolean v4, p0, Lcom/sec/android/app/samsungapps/permission/CPermissionManagerFactory;->a:Z

    iget-boolean v5, p0, Lcom/sec/android/app/samsungapps/permission/CPermissionManagerFactory;->a:Z

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    new-instance v3, Lcom/sec/android/app/samsungapps/permission/a;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/permission/a;-><init>(Lcom/sec/android/app/samsungapps/permission/CPermissionManagerFactory;)V

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;ZZ)V

    return-object v0
.end method
