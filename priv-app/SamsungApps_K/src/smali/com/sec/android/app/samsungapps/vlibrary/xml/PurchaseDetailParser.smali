.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseDetailParser;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;
.source "ProGuard"


# instance fields
.field private mContent:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseDetailParser;->mContent:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 13
    return-void
.end method


# virtual methods
.method protected onCreateObject(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 2

    .prologue
    .line 24
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseDetail;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/PurchaseDetail;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 26
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseDetailParser;->mContent:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->setDetailMain(Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;)V

    .line 27
    return-void
.end method

.method protected onPostParseError()V
    .locals 0

    .prologue
    .line 19
    return-void
.end method

.method protected onPostParseResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 33
    return-void
.end method
