.class final Lcom/sec/android/app/samsungapps/aw;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;

.field final synthetic b:Lcom/sec/android/app/samsungapps/ContentDetailActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;)V
    .locals 0

    .prologue
    .line 727
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/aw;->b:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    iput-object p2, p0, Lcom/sec/android/app/samsungapps/aw;->a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCommandResult(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 730
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/aw;->b:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 751
    :cond_0
    :goto_0
    return-void

    .line 734
    :cond_1
    if-eqz p1, :cond_3

    .line 736
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/aw;->b:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->b(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 737
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/aw;->b:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->onWidgetSetViewState(I)V

    .line 738
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/aw;->b:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a(Lcom/sec/android/app/samsungapps/ContentDetailActivity;Z)Z

    .line 741
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/aw;->a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/aw;->b:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->setWidgetData(Ljava/lang/Object;)V

    .line 742
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/aw;->a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->setWidgetState(I)V

    .line 743
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/aw;->a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/aw;->b:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->g:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailOverviewWidgetClickListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->setScreenshotListner(Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailOverviewWidgetClickListener;)V

    .line 744
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/aw;->a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->updateWidget()V

    .line 748
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/aw;->b:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v0, v2}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a(Lcom/sec/android/app/samsungapps/ContentDetailActivity;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 749
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/aw;->a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->onWidgetSetViewState()V

    goto :goto_0

    .line 746
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/aw;->a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->setWidgetState(I)V

    goto :goto_1
.end method
