.class public final enum Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;

.field public static final enum CALCULATE_UPDATE_AND_SIGSUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;

.field public static final enum CHECK_LOGINSTATE:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;

.field public static final enum NOTIFY_SIG_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;

.field public static final enum NOTIFY_SIG_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;

.field public static final enum REQUEST_LOGIN:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;

.field public static final enum SEND_REQUEST:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 29
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;

    const-string v1, "SEND_REQUEST"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;->SEND_REQUEST:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;

    .line 30
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;

    const-string v1, "CALCULATE_UPDATE_AND_SIGSUCCESS"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;->CALCULATE_UPDATE_AND_SIGSUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;

    .line 31
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;

    const-string v1, "NOTIFY_SIG_SUCCESS"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;->NOTIFY_SIG_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;

    .line 32
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;

    const-string v1, "NOTIFY_SIG_FAILED"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;->NOTIFY_SIG_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;

    const-string v1, "CHECK_LOGINSTATE"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;->CHECK_LOGINSTATE:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;

    const-string v1, "REQUEST_LOGIN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;->REQUEST_LOGIN:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;

    .line 27
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;->SEND_REQUEST:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;->CALCULATE_UPDATE_AND_SIGSUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;->NOTIFY_SIG_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;->NOTIFY_SIG_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;->CHECK_LOGINSTATE:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;->REQUEST_LOGIN:Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListStateMachine$Action;

    return-object v0
.end method
