.class public Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;
.super Lcom/sec/android/app/samsungapps/widget/CommonWidget;
.source "ProGuard"


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

.field private b:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

.field private d:Landroid/widget/CheckedTextView;

.field private e:I

.field private f:I

.field private g:Landroid/view/View;

.field private h:Landroid/view/View;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 28
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;)V

    .line 17
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->a:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    .line 18
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    .line 19
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->d:Landroid/widget/CheckedTextView;

    .line 20
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->e:I

    .line 21
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->f:I

    .line 29
    const v0, 0x7f0400b6

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->initView(Landroid/content/Context;I)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 33
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 17
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->a:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    .line 18
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    .line 19
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->d:Landroid/widget/CheckedTextView;

    .line 20
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->e:I

    .line 21
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->f:I

    .line 34
    const v0, 0x7f0400b6

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->initView(Landroid/content/Context;I)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 38
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 17
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->a:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    .line 18
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    .line 19
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->d:Landroid/widget/CheckedTextView;

    .line 20
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->e:I

    .line 21
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->f:I

    .line 39
    const v0, 0x7f0400b6

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->initView(Landroid/content/Context;I)V

    .line 40
    return-void
.end method

.method private a(Landroid/content/res/Configuration;)I
    .locals 2

    .prologue
    .line 311
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 314
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->e:I

    div-int/lit8 v0, v0, 0x2

    .line 319
    :goto_0
    return v0

    .line 316
    :cond_0
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->f:I

    div-int/lit8 v0, v0, 0x2

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;)Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->a:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    return-object v0
.end method

.method private a(I)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 248
    const v0, 0x7f0c028d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 249
    const v1, 0x7f0c028a

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 251
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->h:Landroid/view/View;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->g:Landroid/view/View;

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 269
    :cond_0
    :goto_0
    return-void

    .line 254
    :cond_1
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 255
    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 257
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 264
    :pswitch_0
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 265
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->h:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setSelected(Z)V

    .line 266
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->g:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setSelected(Z)V

    goto :goto_0

    .line 259
    :pswitch_1
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 260
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->g:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setSelected(Z)V

    .line 261
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->h:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setSelected(Z)V

    goto :goto_0

    .line 257
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;)Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;)Landroid/widget/CheckedTextView;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->d:Landroid/widget/CheckedTextView;

    return-object v0
.end method


# virtual methods
.method public isCheckAllButtonChecked()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->d:Landroid/widget/CheckedTextView;

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public loadWidget()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x2

    .line 48
    const v0, 0x7f0c028c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->h:Landroid/view/View;

    .line 49
    const v0, 0x7f0c0289

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->g:Landroid/view/View;

    .line 50
    const v0, 0x7f0c028b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->i:Landroid/widget/TextView;

    .line 51
    const v0, 0x7f0c028e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->j:Landroid/widget/TextView;

    .line 52
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0802f3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0802b3

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0800de

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->a:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    if-eqz v0, :cond_0

    .line 56
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->setLayoutTopButton(I)V

    .line 57
    const v0, 0x7f0c0286

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->d:Landroid/widget/CheckedTextView;

    .line 58
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->updateWidget()V

    .line 62
    :goto_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->setTabTextSize(Landroid/content/res/Configuration;)V

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->g:Landroid/view/View;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->h:Landroid/view/View;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v8

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v7

    invoke-static {v3, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 67
    return-void

    .line 60
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public refreshWidget()V
    .locals 0

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->updateWidget()V

    .line 88
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 97
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->release()V

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->a:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->a:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->clear()V

    .line 102
    :cond_0
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->a:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    .line 103
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    .line 104
    return-void
.end method

.method public setCheckAllButton(Ljava/lang/Boolean;)V
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->d:Landroid/widget/CheckedTextView;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 113
    return-void
.end method

.method public setClickListener(Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    .line 44
    return-void
.end method

.method public setEnableButton(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->h:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->g:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->j:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->i:Landroid/widget/TextView;

    if-nez v0, :cond_1

    .line 202
    :cond_0
    :goto_0
    return-void

    .line 184
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->a(I)V

    .line 186
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 195
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->g:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->g:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    invoke-interface {v0, v2, v2}, Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;->onChangeActionItem(IZ)V

    .line 198
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->j:Landroid/widget/TextView;

    invoke-static {v0, v3}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->setTitleColor(Landroid/widget/TextView;Z)V

    .line 199
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->i:Landroid/widget/TextView;

    invoke-static {v0, v2}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->setTitleColor(Landroid/widget/TextView;Z)V

    goto :goto_0

    .line 188
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->h:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->h:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    .line 190
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    const/4 v1, 0x2

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;->onChangeActionItem(IZ)V

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->j:Landroid/widget/TextView;

    invoke-static {v0, v2}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->setTitleColor(Landroid/widget/TextView;Z)V

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->i:Landroid/widget/TextView;

    invoke-static {v0, v3}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->setTitleColor(Landroid/widget/TextView;Z)V

    goto :goto_0

    .line 186
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setEnableButton(IZ)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->h:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->g:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->j:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->i:Landroid/widget/TextView;

    if-nez v0, :cond_1

    .line 235
    :cond_0
    :goto_0
    return-void

    .line 214
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 226
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->g:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->setEnabled(Z)V

    .line 227
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->g:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->setFocusable(Z)V

    .line 228
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    invoke-interface {v0, v1, p2}, Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;->onChangeActionItem(IZ)V

    .line 229
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->a:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->getCommandType()I

    move-result v0

    if-ne v0, v2, :cond_3

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->j:Landroid/widget/TextView;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->setTitleColor(Landroid/widget/TextView;Z)V

    goto :goto_0

    .line 216
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->h:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->setEnabled(Z)V

    .line 217
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->h:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->setFocusable(Z)V

    .line 218
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;

    invoke-interface {v0, v2, p2}, Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;->onChangeActionItem(IZ)V

    .line 219
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->a:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->getCommandType()I

    move-result v0

    if-ne v0, v2, :cond_2

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->j:Landroid/widget/TextView;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->setTitleColor(Landroid/widget/TextView;Z)V

    goto :goto_0

    .line 222
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->j:Landroid/widget/TextView;

    invoke-static {v0, v3}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->setTitleColor(Landroid/widget/TextView;Z)V

    goto :goto_0

    .line 232
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->j:Landroid/widget/TextView;

    invoke-static {v0, v3}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->setTitleColor(Landroid/widget/TextView;Z)V

    goto :goto_0

    .line 214
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setEnableCheckAllTab(Ljava/lang/Boolean;)V
    .locals 2

    .prologue
    .line 136
    const v0, 0x7f0c0286

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 137
    return-void
.end method

.method public setHideButtonLayout(Z)V
    .locals 1

    .prologue
    .line 144
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 145
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->setVisibility(I)V

    .line 148
    :goto_0
    return-void

    .line 147
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public setHideCheckAllButton()V
    .locals 2

    .prologue
    .line 128
    const v0, 0x7f0c0285

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 129
    return-void
.end method

.method public setLayoutTopButton(I)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 156
    const v0, 0x7f0c0288

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 157
    const v1, 0x7f0c0285

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 159
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 173
    :cond_0
    :goto_0
    return-void

    .line 163
    :cond_1
    const/16 v2, 0x12

    if-ne p1, v2, :cond_2

    .line 164
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 165
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 167
    :cond_2
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 168
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->d:Landroid/widget/CheckedTextView;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->d:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v3}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto :goto_0
.end method

.method public setTabTextSize(Landroid/content/res/Configuration;)V
    .locals 6

    .prologue
    const/16 v5, 0x11

    const/16 v4, 0xf

    const/4 v3, 0x1

    .line 323
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 350
    :cond_0
    :goto_0
    return-void

    .line 329
    :cond_1
    if-nez p1, :cond_2

    .line 330
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p1

    .line 332
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 333
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->e:I

    .line 334
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->f:I

    .line 341
    :cond_2
    :goto_1
    const v0, 0x7f0c028b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 342
    if-eqz v0, :cond_3

    .line 343
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->mContext:Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->a(Landroid/content/res/Configuration;)I

    move-result v2

    invoke-static {v1, v0, v5, v4, v2}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->tabTextResize(Landroid/content/Context;Landroid/widget/TextView;III)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 346
    :cond_3
    const v0, 0x7f0c028e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 347
    if-eqz v0, :cond_0

    .line 348
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->mContext:Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->a(Landroid/content/res/Configuration;)I

    move-result v2

    invoke-static {v1, v0, v5, v4, v2}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->tabTextResize(Landroid/content/Context;Landroid/widget/TextView;III)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0

    .line 336
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->f:I

    .line 337
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->e:I

    goto :goto_1
.end method

.method public setWidgetData(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 92
    check-cast p1, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->a:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    .line 93
    return-void
.end method

.method public updateWidget()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->a:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    if-eqz v0, :cond_3

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->a:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->getCommandType()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->a(I)V

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->a:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->getCommandType()I

    move-result v0

    invoke-virtual {p0, v0, v2}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->setEnableButton(IZ)V

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->a:Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->getCommandType()I

    move-result v0

    if-ne v0, v3, :cond_1

    .line 75
    invoke-virtual {p0, v2, v1}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->setEnableButton(IZ)V

    .line 79
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->h:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->g:Landroid/view/View;

    if-nez v0, :cond_2

    .line 83
    :cond_0
    :goto_1
    return-void

    .line 77
    :cond_1
    invoke-virtual {p0, v3, v1}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->setEnableButton(IZ)V

    goto :goto_0

    .line 79
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->h:Landroid/view/View;

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/s;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/s;-><init>(Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->g:Landroid/view/View;

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/t;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/t;-><init>(Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0c0286

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/u;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/u;-><init>(Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 81
    :cond_3
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->setVisibility(I)V

    goto :goto_1
.end method
