.class public Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/MockNOXAPI;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;


# instance fields
.field public bKnoxMode:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/MockNOXAPI;->bKnoxMode:Z

    return-void
.end method


# virtual methods
.method public getLaunchIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    return-object v0
.end method

.method public installPackage(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)Z
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x0

    return v0
.end method

.method public isExecutable(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x1

    return v0
.end method

.method public isKnoxMode()Z
    .locals 1

    .prologue
    .line 12
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/MockNOXAPI;->bKnoxMode:Z

    return v0
.end method

.method public launch(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    return v0
.end method

.method public release()V
    .locals 0

    .prologue
    .line 35
    return-void
.end method
