.class public Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private a:Lcom/sec/spp/push/dlc/api/IDlcService;

.field private b:Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder$ServiceConnectionListener;

.field private c:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder$ServiceConnectionListener;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;->a:Lcom/sec/spp/push/dlc/api/IDlcService;

    .line 109
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/b;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/tobelog/b;-><init>(Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;->c:Landroid/content/ServiceConnection;

    .line 35
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;->b:Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder$ServiceConnectionListener;

    .line 37
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;)Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder$ServiceConnectionListener;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;->b:Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder$ServiceConnectionListener;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;Lcom/sec/spp/push/dlc/api/IDlcService;)Lcom/sec/spp/push/dlc/api/IDlcService;
    .locals 0

    .prologue
    .line 21
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;->a:Lcom/sec/spp/push/dlc/api/IDlcService;

    return-object p1
.end method


# virtual methods
.method public bindService(Landroid/content/Context;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 41
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 42
    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "com.sec.spp.push"

    const-string v4, "com.sec.spp.push.dlc.writer.WriterService"

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 44
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;->c:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {p1, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    :goto_0
    return v0

    .line 45
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 47
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isServiceConnected()Z
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;->a:Lcom/sec/spp/push/dlc/api/IDlcService;

    if-nez v0, :cond_0

    .line 73
    const/4 v0, 0x0

    .line 75
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onDestroy(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 30
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;->unbindService(Landroid/content/Context;)V

    .line 31
    return-void
.end method

.method public sendLog(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 10

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;->isServiceConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;->a:Lcom/sec/spp/push/dlc/api/IDlcService;

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-interface/range {v0 .. v9}, Lcom/sec/spp/push/dlc/api/IDlcService;->requestSend(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    .line 99
    :goto_0
    return v0

    .line 89
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 99
    :cond_0
    :goto_1
    const/16 v0, -0x3e8

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public unbindService(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;->a:Lcom/sec/spp/push/dlc/api/IDlcService;

    if-eqz v0, :cond_0

    .line 58
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;->c:Landroid/content/ServiceConnection;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogCollectorBinder;->a:Lcom/sec/spp/push/dlc/api/IDlcService;

    .line 64
    const-string v0, "LogCollectorService is disconnected"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V

    .line 68
    :cond_0
    return-void

    .line 59
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method
