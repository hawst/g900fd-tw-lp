.class public Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginStateMachine;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;
.source "ProGuard"


# static fields
.field private static a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginStateMachine;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;-><init>()V

    .line 32
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginStateMachine;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginStateMachine;

    if-nez v0, :cond_0

    .line 16
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginStateMachine;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginStateMachine;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginStateMachine;

    .line 18
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginStateMachine;

    return-object v0
.end method


# virtual methods
.method protected entry(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 3

    .prologue
    .line 75
    const-string v0, "AutoUpdLoginStateMachine"

    const-string v1, "entry"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 76
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/d;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 95
    :goto_0
    :pswitch_0
    return-void

    .line 79
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;->IS_LOGED_IN:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 84
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;->NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 85
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 88
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;->REQUEST_LOGIN:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 91
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 92
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 76
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method public execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Event;)Z
    .locals 3

    .prologue
    .line 24
    const-string v0, "AutoUpdLoginStateMachine"

    const-string v1, "execute"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 25
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/d;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 70
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 28
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/d;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 31
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$State;->CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 36
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/d;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 39
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 42
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;->CHECK_INSTALL_AND_VERSION:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 45
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;->CHECK_EMAIL_ACCOUNT:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 48
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 51
    :pswitch_7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 54
    :pswitch_8
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$State;->REQUEST_LOGIN:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 59
    :pswitch_9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/d;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    goto :goto_0

    .line 62
    :pswitch_a
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 65
    :pswitch_b
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 25
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_9
    .end packed-switch

    .line 28
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch

    .line 36
    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 59
    :pswitch_data_3
    .packed-switch 0x8
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public bridge synthetic execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 9
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Event;

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/IAutoUpdLoginState$Event;)Z

    move-result v0

    return v0
.end method

.method protected exit(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 0

    .prologue
    .line 101
    return-void
.end method
