.class public Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/ApkSaveFileName;
.super Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/SaveFileName;
.source "ProGuard"


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/SaveFileNameInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/SaveFileName;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/SaveFileNameInfo;)V

    .line 11
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/ApkSaveFileName;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 12
    return-void
.end method

.method public static fromContent(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/SaveFileName;
    .locals 2

    .prologue
    .line 16
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/a;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    .line 33
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/ApkSaveFileName;

    invoke-direct {v1, v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/ApkSaveFileName;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/SaveFileNameInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    return-object v1
.end method


# virtual methods
.method protected fileExtension()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/ApkSaveFileName;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->isWGTOnly()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    const-string v0, ".wgt"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 45
    :goto_0
    return-object v0

    .line 42
    :cond_0
    const-string v0, ".apk"

    goto :goto_0

    .line 45
    :catch_0
    move-exception v0

    const-string v0, ".apk"

    goto :goto_0
.end method
