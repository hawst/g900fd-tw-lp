.class public abstract Lcom/sec/android/app/samsungapps/OrientationActivity;
.super Lcom/sec/android/app/samsungapps/ActionBarActivity;
.source "ProGuard"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;-><init>()V

    .line 27
    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/CustomDialogBuilder;Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent;)V
    .locals 2

    .prologue
    .line 33
    const v0, 0x7f08023d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/OrientationActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/fq;

    invoke-direct {v1, p0, p2}, Lcom/sec/android/app/samsungapps/fq;-><init>(Lcom/sec/android/app/samsungapps/OrientationActivity;Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent;)V

    invoke-virtual {p1, v0, v1}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setNegativeButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 39
    const v0, 0x7f0802ef

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/OrientationActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/fr;

    invoke-direct {v1, p0, p2}, Lcom/sec/android/app/samsungapps/fr;-><init>(Lcom/sec/android/app/samsungapps/OrientationActivity;Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent;)V

    invoke-virtual {p1, v0, v1}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setPositiveButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 45
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    .line 46
    return-void
.end method


# virtual methods
.method public handleYesOrNoEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const v2, 0xff40

    .line 23
    sget-object v0, Lcom/sec/android/app/samsungapps/fs;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent;->getYesOrNoEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoEventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoEventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 29
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 26
    :pswitch_0
    if-nez p2, :cond_0

    invoke-static {p0, v2}, Lcom/sec/android/app/samsungapps/NotiDialog;->getMessage(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->createInfoDialog(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/samsungapps/OrientationActivity;->a(Lcom/sec/android/app/samsungapps/CustomDialogBuilder;Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent;)V

    .line 27
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 26
    :cond_0
    check-cast p2, Ljava/lang/String;

    invoke-static {p0, v2}, Lcom/sec/android/app/samsungapps/NotiDialog;->getMessage(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p2, v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->createInfoDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/samsungapps/OrientationActivity;->a(Lcom/sec/android/app/samsungapps/CustomDialogBuilder;Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent;)V

    goto :goto_1

    .line 23
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 13
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 19
    return-void
.end method
