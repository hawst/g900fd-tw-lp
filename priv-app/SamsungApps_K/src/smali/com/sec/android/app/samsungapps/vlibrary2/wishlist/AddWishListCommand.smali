.class public Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AddWishListCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

.field private _IWishListItemCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AddWishListCommand;->_IWishListItemCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;

    .line 22
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AddWishListCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    .line 23
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AddWishListCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AddWishListCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AddWishListCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AddWishListCommand;->_IWishListItemCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AddWishListCommand;Z)V
    .locals 0

    .prologue
    .line 15
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AddWishListCommand;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 3

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AddWishListCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->startLoading()V

    .line 28
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AddWishListCommand;->_IWishListItemCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;->getProductID()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/b;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AddWishListCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->createWishList(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 44
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/AddWishListCommand;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 46
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 47
    return-void
.end method
