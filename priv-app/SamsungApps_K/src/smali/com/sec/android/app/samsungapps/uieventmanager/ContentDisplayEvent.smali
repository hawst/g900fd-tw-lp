.class public Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;
.super Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;
.source "ProGuard"


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;)V
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;->ContentDisplayEvent:Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;-><init>(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;)V

    .line 24
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->a:Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    .line 25
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;-><init>(Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;)V

    .line 29
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 30
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;-><init>(Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;)V

    .line 35
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->c:Ljava/lang/String;

    .line 36
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;-><init>(Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;)V

    .line 41
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->c:Ljava/lang/String;

    .line 42
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->d:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;-><init>(Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;)V

    .line 49
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->c:Ljava/lang/String;

    .line 50
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->d:Ljava/lang/String;

    .line 51
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->e:Ljava/lang/String;

    .line 52
    return-void
.end method


# virtual methods
.method public getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    return-object v0
.end method

.method public getContentDisplayEventType()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->a:Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->d:Ljava/lang/String;

    return-object v0
.end method
