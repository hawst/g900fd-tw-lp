.class public final enum Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

.field public static final enum ac:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

.field public static final enum hstr:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

.field public static final enum iqry:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

.field public static final enum jump:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

.field public static final enum more:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

.field public static final enum none:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

.field public static final enum pop:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

.field public static final enum tag:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 53
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    const-string v1, "none"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;->none:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    .line 54
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    const-string v1, "iqry"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;->iqry:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    .line 55
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    const-string v1, "tag"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;->tag:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    .line 56
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    const-string v1, "pop"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;->pop:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    .line 57
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    const-string v1, "ac"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;->ac:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    .line 58
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    const-string v1, "hstr"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;->hstr:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    .line 59
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    const-string v1, "more"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;->more:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    .line 60
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    const-string v1, "jump"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;->jump:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    .line 51
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;->none:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;->iqry:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;->tag:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;->pop:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;->ac:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;->hstr:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;->more:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;->jump:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    return-object v0
.end method
