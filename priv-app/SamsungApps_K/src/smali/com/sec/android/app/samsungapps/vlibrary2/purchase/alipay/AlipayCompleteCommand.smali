.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _FinalMap:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

.field private _IAlipayCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;

.field private _IPurchaseCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseCommandBuilder;

.field private _TimeoutRetrier:Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 28
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;->_FinalMap:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    .line 29
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;->_IPurchaseCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseCommandBuilder;

    .line 30
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;->_IAlipayCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;

    .line 31
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;Z)V
    .locals 0

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;)Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;->_TimeoutRetrier:Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;Z)V
    .locals 0

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseCommandBuilder;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;->_IPurchaseCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseCommandBuilder;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;->_IAlipayCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;

    return-object v0
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 5

    .prologue
    .line 34
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;->_IAlipayCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;->getAlipayInitResult()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayIntResult;

    move-result-object v1

    iget v1, v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayIntResult;->responsetime:I

    int-to-long v1, v1

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;->_IAlipayCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;->getAlipayInitResult()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayIntResult;

    move-result-object v3

    iget v3, v3, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayIntResult;->retrycount:I

    new-instance v4, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/a;

    invoke-direct {v4, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;)V

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;-><init>(JILcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier$RetryAction;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;->_TimeoutRetrier:Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;

    .line 41
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;->requestComplete(Z)V

    .line 42
    return-void
.end method

.method protected isSuccess()Z
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;->_FinalMap:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    const-string v1, "downloadUri"

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;->findString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 70
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    const/4 v0, 0x1

    .line 74
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public requestComplete(Z)V
    .locals 4

    .prologue
    .line 46
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/c;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;Z)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;->_FinalMap:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/b;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->completeAlipayPurchase(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCompParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 62
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 64
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 65
    return-void
.end method
