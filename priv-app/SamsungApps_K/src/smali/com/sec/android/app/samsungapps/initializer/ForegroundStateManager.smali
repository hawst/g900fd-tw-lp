.class public Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/SimpleFSM;
.source "ProGuard"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Landroid/os/CountDownTimer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/SimpleFSM;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager;->a:Landroid/content/Context;

    .line 62
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 132
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/samsungapps/AppsService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 133
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 134
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 136
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager;)V
    .locals 2

    .prologue
    .line 11
    sget-object v1, Lcom/sec/android/app/samsungapps/initializer/f;->a:[I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager$State;->STOP_FOREGROUND:Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager$State;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager;->setState(Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected entry()V
    .locals 3

    .prologue
    .line 43
    sget-object v1, Lcom/sec/android/app/samsungapps/initializer/f;->a:[I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 57
    :goto_0
    :pswitch_0
    return-void

    .line 51
    :pswitch_1
    const-string v0, "hcjo"

    const-string v1, "opStopForeground"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/samsungapps/AppsService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    goto :goto_0

    .line 54
    :pswitch_2
    new-instance v0, Lcom/sec/android/app/samsungapps/initializer/e;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/initializer/e;-><init>(Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager;->b:Landroid/os/CountDownTimer;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager;->b:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    const-string v0, "hcjo"

    const-string v1, "startTimer"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 43
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected exit()V
    .locals 2

    .prologue
    .line 25
    sget-object v1, Lcom/sec/android/app/samsungapps/initializer/f;->a:[I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 39
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 30
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager;->a()V

    goto :goto_0

    .line 33
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager;->a()V

    goto :goto_0

    .line 36
    :pswitch_3
    const-string v0, "hcjo"

    const-string v1, "opStopTimer"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager;->b:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager;->b:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager;->b:Landroid/os/CountDownTimer;

    goto :goto_0

    .line 25
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected getIdleState()Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager$State;
    .locals 1

    .prologue
    .line 163
    sget-object v0, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager$State;->IDLE:Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager$State;

    return-object v0
.end method

.method protected bridge synthetic getIdleState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager;->getIdleState()Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager$State;

    move-result-object v0

    return-object v0
.end method

.method public startForeground()V
    .locals 2

    .prologue
    .line 113
    sget-object v1, Lcom/sec/android/app/samsungapps/initializer/f;->a:[I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 127
    :goto_0
    :pswitch_0
    return-void

    .line 118
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager$State;->FOREGROUND:Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager$State;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 121
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager$State;->FOREGROUND:Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager$State;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 124
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager$State;->FOREGROUND:Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager$State;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 113
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public stopForeground()V
    .locals 2

    .prologue
    .line 140
    sget-object v1, Lcom/sec/android/app/samsungapps/initializer/f;->a:[I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 144
    :goto_0
    return-void

    .line 143
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager$State;->WAITING_STOP_FOREGROUND:Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager$State;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/initializer/ForegroundStateManager;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 140
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
