.class public Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;


# static fields
.field private static final CSC_FILE_PATH:Ljava/lang/String; = "/system/csc/sales_code.dat"

.field private static final DEFAULTIMEI:Ljava/lang/String; = "000000000"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mSappsConfig:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

.field private mStrChinaModels:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;)V
    .locals 3

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 467
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "SM-T805C"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "SM-T705C"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "GT-I9508V"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "SM-G7508Q"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "SM-G9006W"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "SM-G5308W"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mStrChinaModels:[Ljava/lang/String;

    .line 41
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mContext:Landroid/content/Context;

    .line 42
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mSappsConfig:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    .line 43
    return-void
.end method

.method public static SHA1(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 47
    :try_start_0
    const-string v0, "SHA-1"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    .line 49
    const-string v1, "iso-8859-1"

    invoke-virtual {p0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Ljava/security/MessageDigest;->update([BII)V

    .line 50
    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    .line 51
    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 54
    :goto_0
    return-object v0

    .line 52
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 54
    const-string v0, "000000000"

    goto :goto_0
.end method

.method private getCSCVersion()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 573
    new-instance v0, Ljava/io/File;

    const-string v2, "/system/csc/sales_code.dat"

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 574
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    :cond_0
    move-object v0, v1

    .line 607
    :goto_0
    return-object v0

    .line 582
    :cond_1
    const/16 v2, 0x14

    :try_start_0
    new-array v3, v2, [B

    .line 583
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 585
    :try_start_1
    invoke-virtual {v2, v3}, Ljava/io/InputStream;->read([B)I

    move-result v0

    if-eqz v0, :cond_2

    .line 587
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 588
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>([B)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_0

    .line 596
    :catch_0
    move-exception v0

    move-object v0, v2

    :goto_1
    move-object v2, v0

    .line 599
    :cond_2
    :goto_2
    if-eqz v2, :cond_3

    .line 601
    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :cond_3
    :goto_3
    move-object v0, v1

    .line 607
    goto :goto_0

    .line 603
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    :catch_2
    move-exception v0

    move-object v2, v1

    goto :goto_2

    :catch_3
    move-exception v0

    goto :goto_2

    .line 596
    :catch_4
    move-exception v0

    move-object v0, v1

    goto :goto_1
.end method

.method private isChinaDeviceModel()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 475
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->getModelName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    .line 476
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mStrChinaModels:[Ljava/lang/String;

    array-length v4, v3

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    .line 477
    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-eqz v5, :cond_1

    .line 478
    const/4 v0, 0x1

    .line 485
    :cond_0
    :goto_1
    return v0

    .line 476
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_1

    .line 484
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method private isValidString(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 709
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private validString(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 519
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 520
    const/4 v0, 0x1

    .line 522
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public doesSupportPhoneFeature()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mContext:Landroid/content/Context;

    const-string v2, "phone"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 184
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v0

    if-nez v0, :cond_0

    .line 186
    const/4 v0, 0x0

    .line 193
    :goto_0
    return v0

    .line 188
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->isPhoneNumberReadable()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 190
    goto :goto_0

    :cond_1
    move v0, v1

    .line 193
    goto :goto_0
.end method

.method public getConnectedNetworkType()I
    .locals 3

    .prologue
    .line 297
    const/4 v1, -0x1

    .line 299
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mContext:Landroid/content/Context;

    const-string v2, "connectivity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 303
    if-nez v0, :cond_0

    .line 305
    const-string v0, "ConcreteDeviceInfoLoader::isConnectedNetwork::conMgr is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->d(Ljava/lang/String;)V

    move v0, v1

    .line 320
    :goto_0
    return v0

    .line 309
    :cond_0
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 310
    if-nez v0, :cond_1

    .line 312
    const-string v0, "ConcreteDeviceInfoLoader::isConnectedNetwork::info is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->d(Ljava/lang/String;)V

    move v0, v1

    .line 313
    goto :goto_0

    .line 316
    :cond_1
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    goto :goto_0
.end method

.method public getDeiviceWidth()I
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    return v0
.end method

.method public getDeviceDensity()F
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    return v0
.end method

.method public getDeviceHeight()I
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    return v0
.end method

.method public getDeviceLCDSize()I
    .locals 7

    .prologue
    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    .line 128
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->getDeviceDensity()F

    move-result v0

    .line 129
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->getDeiviceWidth()I

    move-result v1

    .line 130
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->getDeviceHeight()I

    move-result v2

    .line 132
    const/16 v3, 0x400

    if-ne v1, v3, :cond_0

    const/16 v3, 0x228

    if-ne v2, v3, :cond_0

    float-to-double v3, v0

    cmpl-double v3, v3, v5

    if-eqz v3, :cond_1

    :cond_0
    const/16 v3, 0x258

    if-ne v1, v3, :cond_2

    const/16 v1, 0x3d0

    if-ne v2, v1, :cond_2

    float-to-double v0, v0

    cmpl-double v0, v0, v5

    if-nez v0, :cond_2

    .line 135
    :cond_1
    const/4 v0, 0x0

    .line 137
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getExtraPhoneType()I
    .locals 2

    .prologue
    .line 216
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mContext:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 218
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 233
    const/4 v0, 0x0

    .line 236
    :goto_0
    return v0

    .line 221
    :pswitch_0
    const/4 v0, 0x1

    .line 222
    goto :goto_0

    .line 225
    :pswitch_1
    const/4 v0, 0x2

    .line 226
    goto :goto_0

    .line 229
    :pswitch_2
    const/4 v0, 0x3

    .line 230
    goto :goto_0

    .line 218
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getIMEI()Ljava/lang/String;
    .locals 4

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mSappsConfig:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->isExistSaconfig()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mSappsConfig:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getIMEI()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->isValidString(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mSappsConfig:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getIMEI()Ljava/lang/String;

    move-result-object v0

    .line 108
    :cond_0
    :goto_0
    return-object v0

    .line 68
    :cond_1
    const-string v1, "000000000"

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mContext:Landroid/content/Context;

    const-string v2, "phone"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 71
    if-nez v0, :cond_2

    .line 73
    const-string v0, "000000000"

    goto :goto_0

    .line 77
    :cond_2
    :try_start_0
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 93
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 97
    :cond_3
    :goto_1
    if-eqz v0, :cond_4

    :try_start_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 98
    :cond_4
    const-string v0, "000000000"
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 80
    :pswitch_0
    :try_start_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mContext:Landroid/content/Context;

    const-string v2, "ro.serialno"

    const-string v3, "Unknown"

    invoke-static {v0, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SystemPropertiesProxy;->get(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v0

    .line 81
    :try_start_3
    const-string v1, "Unknown"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 83
    const-string v0, "000000000"
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 87
    :pswitch_1
    :try_start_4
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 90
    :pswitch_2
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    move-result-object v0

    goto :goto_1

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0

    .line 77
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getIMSI()Ljava/lang/String;
    .locals 3

    .prologue
    .line 682
    const-string v1, ""

    .line 686
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mContext:Landroid/content/Context;

    const-string v2, "phone"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 687
    if-eqz v0, :cond_0

    .line 690
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    move-result-object v0

    .line 694
    if-eqz v0, :cond_0

    .line 704
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public getMSISDN()Ljava/lang/String;
    .locals 3

    .prologue
    .line 654
    const-string v1, ""

    .line 658
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mContext:Landroid/content/Context;

    const-string v2, "phone"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 659
    if-eqz v0, :cond_0

    .line 662
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v0

    .line 666
    if-eqz v0, :cond_0

    .line 676
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public getModelName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 325
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    .line 327
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isReleaseMode()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->getModel()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->validString(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 329
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->getModel()Ljava/lang/String;

    move-result-object v0

    .line 351
    :cond_0
    :goto_0
    return-object v0

    .line 331
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mSappsConfig:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->isExistSaconfig()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 333
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mSappsConfig:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getModelName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->isValidString(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 334
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mSappsConfig:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getModelName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 337
    :cond_2
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 343
    const-string v1, "OMAP_SS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 349
    const-string v1, "SAMSUNG-"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getNetwrokType()I
    .locals 7

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 241
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mContext:Landroid/content/Context;

    const-string v5, "phone"

    invoke-virtual {v0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 244
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 269
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mContext:Landroid/content/Context;

    const-string v5, "connectivity"

    invoke-virtual {v0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 270
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 271
    if-nez v0, :cond_0

    .line 292
    :goto_0
    return v4

    .line 249
    :pswitch_1
    const/4 v0, 0x0

    :goto_1
    move v4, v0

    .line 292
    goto :goto_0

    :pswitch_2
    move v0, v1

    .line 253
    goto :goto_1

    :pswitch_3
    move v0, v2

    .line 261
    goto :goto_1

    :pswitch_4
    move v0, v3

    .line 266
    goto :goto_1

    .line 275
    :cond_0
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v5

    const/4 v6, 0x6

    if-ne v5, v6, :cond_1

    move v0, v3

    .line 278
    goto :goto_1

    .line 280
    :cond_1
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v4

    .line 283
    goto :goto_1

    :cond_2
    move v0, v2

    .line 287
    goto :goto_1

    .line 244
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_3
        :pswitch_3
        :pswitch_1
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public getOpenApiVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 398
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mSappsConfig:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->isExistSaconfig()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 401
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mSappsConfig:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getOpenAPIVersion()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->isValidString(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 403
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mSappsConfig:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getOpenAPIVersion()Ljava/lang/String;

    move-result-object v0

    .line 409
    :goto_0
    return-object v0

    .line 407
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public isAirplaneMode()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 743
    .line 744
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 749
    if-eqz v2, :cond_0

    .line 750
    const-string v3, "airplane_mode_on"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 754
    if-ne v2, v0, :cond_0

    .line 761
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public isBlackTheme()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 153
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->getModelName()Ljava/lang/String;

    move-result-object v1

    .line 154
    if-nez v1, :cond_1

    .line 178
    :cond_0
    :goto_0
    return v0

    .line 158
    :cond_1
    const-string v2, "GT-P6800"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "GT-P6810"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "GT-P6810D1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "GT-P6811"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "GT-P6801"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "SHV-E150S"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_0

    .line 176
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isConnectedDataNetwork()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 714
    const/4 v2, 0x0

    .line 715
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mContext:Landroid/content/Context;

    const-string v3, "connectivity"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 719
    if-eqz v0, :cond_0

    .line 721
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 725
    if-eqz v0, :cond_0

    .line 728
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    .line 738
    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method public isPhoneNumberReadable()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 199
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mContext:Landroid/content/Context;

    const-string v2, "phone"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 200
    if-nez v0, :cond_0

    move v0, v1

    .line 210
    :goto_0
    return v0

    .line 205
    :cond_0
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v0

    .line 206
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    move v0, v1

    .line 208
    goto :goto_0

    .line 210
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isViaNetwork()Z
    .locals 3

    .prologue
    .line 832
    const/4 v1, 0x0

    .line 834
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mContext:Landroid/content/Context;

    const-string v2, "phone"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 838
    if-eqz v0, :cond_0

    .line 840
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v0

    const/4 v2, 0x5

    if-ne v0, v2, :cond_0

    .line 844
    const/4 v0, 0x1

    .line 849
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public isWIFIConnected()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 767
    const/4 v2, 0x0

    .line 769
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mContext:Landroid/content/Context;

    const-string v3, "connectivity"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 773
    if-eqz v0, :cond_0

    .line 775
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 779
    if-eqz v0, :cond_0

    .line 782
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    .line 791
    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method public isWibroConnected()Z
    .locals 4

    .prologue
    .line 797
    const/4 v1, 0x0

    .line 799
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mContext:Landroid/content/Context;

    const-string v2, "connectivity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 803
    if-eqz v0, :cond_0

    .line 806
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x8

    if-lt v2, v3, :cond_0

    .line 811
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 815
    if-eqz v0, :cond_0

    .line 817
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    const/4 v2, 0x6

    if-ne v0, v2, :cond_0

    .line 821
    const/4 v0, 0x1

    .line 826
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public loadODCVersion()Ljava/lang/String;
    .locals 2

    .prologue
    .line 611
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mSappsConfig:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->isExistSaconfig()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 613
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mSappsConfig:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getODCVersion()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->isValidString(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 614
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mSappsConfig:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getODCVersion()Ljava/lang/String;

    move-result-object v0

    .line 624
    :goto_0
    return-object v0

    .line 617
    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 618
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->getMyPackageInfo()Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 619
    if-eqz v0, :cond_1

    .line 621
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    goto :goto_0

    .line 623
    :cond_1
    const-string v0, "error"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->err(Ljava/lang/String;)V

    .line 624
    const-string v0, "0"

    goto :goto_0
.end method

.method public loadODCVersionCode()I
    .locals 2

    .prologue
    .line 629
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mSappsConfig:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->isExistSaconfig()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 631
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mSappsConfig:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getODCVersionCode()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->isValidString(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 633
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mSappsConfig:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getODCVersionCode()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 648
    :goto_0
    return v0

    .line 634
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 641
    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 642
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->getMyPackageInfo()Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 643
    if-eqz v0, :cond_1

    .line 645
    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    goto :goto_0

    .line 647
    :cond_1
    const-string v0, "error"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->err(Ljava/lang/String;)V

    .line 648
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public readCSC()Ljava/lang/String;
    .locals 4

    .prologue
    .line 527
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    .line 528
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isReleaseMode()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->getCSC()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->validString(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 530
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->getCSC()Ljava/lang/String;

    move-result-object v0

    .line 566
    :cond_0
    :goto_0
    return-object v0

    .line 532
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mSappsConfig:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->isExistSaconfig()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 534
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mSappsConfig:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getCSC()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->isValidString(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 536
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mSappsConfig:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getCSC()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 539
    :cond_2
    const-string v0, "WIFI"

    .line 541
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->getCSCVersion()Ljava/lang/String;

    move-result-object v1

    .line 543
    if-eqz v1, :cond_0

    .line 548
    const/4 v2, 0x0

    const/4 v3, 0x3

    :try_start_0
    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 558
    if-eqz v1, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    move-object v0, v1

    .line 566
    goto :goto_0

    .line 555
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public readMCC()Ljava/lang/String;
    .locals 3

    .prologue
    .line 414
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    .line 416
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isReleaseMode()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->getMCC()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->validString(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 418
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->getMCC()Ljava/lang/String;

    move-result-object v0

    .line 464
    :cond_0
    :goto_0
    return-object v0

    .line 420
    :cond_1
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isTestMode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 422
    const-string v0, "000"

    goto :goto_0

    .line 424
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mSappsConfig:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->isExistSaconfig()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 426
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mSappsConfig:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getMCC()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->isValidString(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 428
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mSappsConfig:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getMCC()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 431
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mContext:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 432
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    .line 433
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v1

    .line 452
    const-string v0, ""

    .line 454
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 455
    const/4 v0, 0x0

    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 459
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->isChinaDeviceModel()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 460
    const-string v0, "460"

    goto :goto_0
.end method

.method public readMNC()Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/MNCInfo;
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 491
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v1

    .line 493
    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isReleaseMode()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->getMNC()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->validString(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 495
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/MNCInfo;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->getMNC()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/MNCInfo;-><init>(Ljava/lang/String;Z)V

    .line 514
    :goto_0
    return-object v0

    .line 497
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mSappsConfig:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->isExistSaconfig()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 499
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mSappsConfig:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getMNC()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->isValidString(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 500
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/MNCInfo;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mSappsConfig:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getMNC()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/MNCInfo;-><init>(Ljava/lang/String;Z)V

    goto :goto_0

    .line 503
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDeviceInfoLoader;->mContext:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 504
    if-nez v0, :cond_2

    .line 506
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/MNCInfo;

    const-string v1, "00"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/MNCInfo;-><init>(Ljava/lang/String;Z)V

    goto :goto_0

    .line 509
    :cond_2
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v1

    .line 510
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_3

    .line 512
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/MNCInfo;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/MNCInfo;-><init>(Ljava/lang/String;Z)V

    goto :goto_0

    .line 514
    :cond_3
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/MNCInfo;

    const-string v1, "00"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/MNCInfo;-><init>(Ljava/lang/String;Z)V

    goto :goto_0
.end method
