.class public Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterFactory;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/PreloadAppUpdaterFactory;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/IDownloaderCreator;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/IDownloaderCreator;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterFactory;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/IDownloaderCreator;

    .line 15
    return-void
.end method


# virtual methods
.method public createUpdater(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/PreloadUpdateInfo;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/PreloadAppUpdater;
    .locals 3

    .prologue
    .line 21
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;-><init>()V

    .line 22
    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/PreloadUpdateInfo;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->GUID:Ljava/lang/String;

    .line 23
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterFactory;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/IDownloaderCreator;

    const/4 v2, 0x1

    invoke-interface {v1, p1, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/IDownloaderCreator;->createDownloader(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Z)Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/Downloader;

    move-result-object v0

    .line 24
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdater;

    invoke-direct {v1, p1, v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdater;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/Downloader;Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/PreloadUpdateInfo;)V

    return-object v1
.end method
