.class public Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _IWishListItemCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;)V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState;->_IWishListItemCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;

    .line 14
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState;Z)V
    .locals 0

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method public getState()Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState;->_IWishListItemCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;->isPurchaseDetailItem()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState;->_IWishListItemCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;->isInstalledItem()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 19
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;->DISABLED:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;

    .line 33
    :goto_0
    return-object v0

    .line 21
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState;->_IWishListItemCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;->isLogedIn()Z

    move-result v0

    if-nez v0, :cond_2

    .line 22
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;->ADD_TO_WISH:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;

    goto :goto_0

    .line 24
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState;->_IWishListItemCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;->hasOrderID()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 25
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;->DISABLED:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;

    goto :goto_0

    .line 27
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState;->_IWishListItemCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;->isAddedItem()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 30
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;->DELETE_WISH:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;

    goto :goto_0

    .line 33
    :cond_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;->ADD_TO_WISH:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;

    goto :goto_0
.end method

.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 3

    .prologue
    .line 46
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->dummyCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    .line 47
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/f;->a:[I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState;->getState()Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 61
    :goto_0
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/e;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/e;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 68
    return-void

    .line 50
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState;->_IWishListItemCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;->addToWishCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    goto :goto_0

    .line 55
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState;->_IWishListItemCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;->removeWishCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    goto :goto_0

    .line 58
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState;->_IWishListItemCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;->goToWishCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    goto :goto_0

    .line 47
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
