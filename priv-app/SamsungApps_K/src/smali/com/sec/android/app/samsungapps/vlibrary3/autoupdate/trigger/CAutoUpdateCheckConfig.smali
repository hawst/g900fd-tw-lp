.class public Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/CAutoUpdateCheckConfig;
.super Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AbsAutoUpdateCheckConfig;
.source "ProGuard"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AbsAutoUpdateCheckConfig;-><init>(Landroid/content/Context;)V

    .line 15
    return-void
.end method


# virtual methods
.method protected getClassNameForAlarmRegister()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    const-string v0, "com.sec.android.app.samsungapps.AlarmReceiver"

    return-object v0
.end method

.method protected getDefaultIntervalSec()J
    .locals 2

    .prologue
    .line 38
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->getAutoUpdateDefaultInterval()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method protected getIntervalSharedPrefName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    const-string v0, "AppsSharedPreference.SP_UPDATE_INTERVAL"

    return-object v0
.end method

.method protected getLastUpdateCheckStr()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    const-string v0, "LASTUPDATECHECK"

    return-object v0
.end method

.method protected getUpdateCycleServerResponseFieldName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    const-string v0, "normalUpdateCycle"

    return-object v0
.end method
