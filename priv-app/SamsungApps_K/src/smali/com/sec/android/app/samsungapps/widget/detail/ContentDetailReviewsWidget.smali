.class public Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;
.super Lcom/sec/android/app/samsungapps/widget/CommonWidget;
.source "ProGuard"


# static fields
.field public static final WIDGET_EXPERT_REVIEWS:I = 0x0

.field public static final WIDGET_USER_REVIEWS:I = 0x1


# instance fields
.field a:Landroid/view/View$OnClickListener;

.field private final b:Ljava/lang/String;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

.field private e:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailReviewsWidgetClickListener;

.field public expertReviewWidget:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;

.field private f:Lcom/sec/android/app/samsungapps/widget/interfaces/IRetryContentDetail;

.field public isExpertReviewLoaded:Z

.field public isExpertReviewReqFinished:Z

.field public isUserReviewLoaded:Z

.field public isUserReviewReqFinished:Z

.field public userReviewWidget:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 41
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;)V

    .line 24
    const-string v0, "ContentDetailReviewsWidget"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->b:Ljava/lang/String;

    .line 32
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->isExpertReviewLoaded:Z

    .line 33
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->isUserReviewLoaded:Z

    .line 34
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->isExpertReviewReqFinished:Z

    .line 35
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->isUserReviewReqFinished:Z

    .line 178
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/detail/k;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/detail/k;-><init>(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->a:Landroid/view/View$OnClickListener;

    .line 42
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->a(Landroid/content/Context;)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 46
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    const-string v0, "ContentDetailReviewsWidget"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->b:Ljava/lang/String;

    .line 32
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->isExpertReviewLoaded:Z

    .line 33
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->isUserReviewLoaded:Z

    .line 34
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->isExpertReviewReqFinished:Z

    .line 35
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->isUserReviewReqFinished:Z

    .line 178
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/detail/k;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/detail/k;-><init>(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->a:Landroid/view/View$OnClickListener;

    .line 47
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->a(Landroid/content/Context;)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 52
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    const-string v0, "ContentDetailReviewsWidget"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->b:Ljava/lang/String;

    .line 32
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->isExpertReviewLoaded:Z

    .line 33
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->isUserReviewLoaded:Z

    .line 34
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->isExpertReviewReqFinished:Z

    .line 35
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->isUserReviewReqFinished:Z

    .line 178
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/detail/k;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/detail/k;-><init>(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->a:Landroid/view/View$OnClickListener;

    .line 53
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->a(Landroid/content/Context;)V

    .line 54
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;)Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailReviewsWidgetClickListener;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->e:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailReviewsWidgetClickListener;

    return-object v0
.end method

.method private a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->mContext:Landroid/content/Context;

    .line 58
    const v0, 0x7f040040

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->initView(Landroid/content/Context;I)V

    .line 60
    const v0, 0x7f0c0136

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->expertReviewWidget:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;

    .line 61
    const v0, 0x7f0c0137

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->userReviewWidget:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;

    .line 63
    return-void
.end method


# virtual methods
.method public initReviewState(I)V
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->expertReviewWidget:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->userReviewWidget:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;

    if-nez v0, :cond_1

    .line 205
    :cond_0
    :goto_0
    return-void

    .line 202
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->setWidgetState(I)V

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->expertReviewWidget:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->setWidgetState(I)V

    .line 204
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->userReviewWidget:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->setWidgetState(I)V

    goto :goto_0
.end method

.method public loadWidget()V
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->f:Lcom/sec/android/app/samsungapps/widget/interfaces/IRetryContentDetail;

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->f:Lcom/sec/android/app/samsungapps/widget/interfaces/IRetryContentDetail;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/widget/interfaces/IRetryContentDetail;->onClickRetryBtn(I)V

    .line 161
    :cond_0
    return-void
.end method

.method public loadWidget(I)V
    .locals 2

    .prologue
    const v1, 0x7f0c0136

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->expertReviewWidget:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->userReviewWidget:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;

    if-nez v0, :cond_1

    .line 153
    :cond_0
    :goto_0
    return-void

    .line 139
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 149
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->isLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->getWidgetState()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->expertReviewWidget:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->isEmptyReview()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 151
    :cond_2
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->updateWidget()V

    goto :goto_0

    .line 141
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->expertReviewWidget:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->loadWidget()V

    goto :goto_1

    .line 145
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->userReviewWidget:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->loadWidget()V

    goto :goto_1

    .line 150
    :cond_3
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 139
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public refreshWidget()V
    .locals 0

    .prologue
    .line 166
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 109
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->mContext:Landroid/content/Context;

    .line 110
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    .line 111
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->e:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailReviewsWidgetClickListener;

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->expertReviewWidget:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->expertReviewWidget:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->release()V

    .line 115
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->expertReviewWidget:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->userReviewWidget:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;

    if-eqz v0, :cond_1

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->userReviewWidget:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->release()V

    .line 119
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->userReviewWidget:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;

    .line 121
    :cond_1
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->f:Lcom/sec/android/app/samsungapps/widget/interfaces/IRetryContentDetail;

    .line 122
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->release()V

    .line 123
    return-void
.end method

.method public setIRetryContentDetail(Lcom/sec/android/app/samsungapps/widget/interfaces/IRetryContentDetail;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->f:Lcom/sec/android/app/samsungapps/widget/interfaces/IRetryContentDetail;

    .line 67
    return-void
.end method

.method public setNeedLoad(Z)V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->expertReviewWidget:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->userReviewWidget:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->expertReviewWidget:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->setNeedLoad(Z)V

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->userReviewWidget:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->setNeedLoad(Z)V

    .line 174
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->setNeedLoad(Z)V

    .line 175
    return-void
.end method

.method public setReviewsWidgetListener(Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailReviewsWidgetClickListener;)V
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->e:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailReviewsWidgetClickListener;

    .line 105
    return-void
.end method

.method public setWidgetData(ILjava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x0

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->expertReviewWidget:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->userReviewWidget:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;

    if-nez v0, :cond_1

    .line 93
    :cond_0
    :goto_0
    return-void

    .line 74
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 86
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->expertReviewWidget:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->isLoaded()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->userReviewWidget:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->isLoaded()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 87
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->setWidgetState(I)V

    goto :goto_0

    .line 76
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->expertReviewWidget:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->setWidgetData(Ljava/lang/Object;)V

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->expertReviewWidget:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->setWidgetState(I)V

    goto :goto_1

    .line 81
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->userReviewWidget:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->setWidgetData(Ljava/lang/Object;)V

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->userReviewWidget:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->setWidgetState(I)V

    goto :goto_1

    .line 88
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->expertReviewWidget:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->getWidgetState()I

    move-result v0

    if-eq v0, v2, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->userReviewWidget:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->getWidgetState()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 90
    :cond_3
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->setWidgetState(I)V

    goto :goto_0

    .line 74
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setWidgetData(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 97
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->hasMyComment()Z

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->d:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->hasMyRating()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->updateWriteBtnState(ZZ)V

    .line 100
    :cond_0
    return-void
.end method

.method public updateWidget()V
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->expertReviewWidget:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->userReviewWidget:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;

    if-nez v0, :cond_1

    .line 133
    :cond_0
    :goto_0
    return-void

    .line 130
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->expertReviewWidget:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->updateWidget()V

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->userReviewWidget:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->updateWidget()V

    .line 132
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->setNeedLoad(Z)V

    goto :goto_0
.end method

.method public updateWriteBtnState(ZZ)V
    .locals 2

    .prologue
    .line 208
    const v0, 0x7f0c0135

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 209
    if-nez v0, :cond_0

    .line 219
    :goto_0
    return-void

    .line 212
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 213
    if-nez p1, :cond_1

    if-nez p2, :cond_1

    .line 214
    const v1, 0x7f080220

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 217
    :cond_1
    const v1, 0x7f08010d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method
