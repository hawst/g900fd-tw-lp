.class public Lcom/sec/android/app/samsungapps/SaccountReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ProGuard"


# static fields
.field private static a:Ljava/lang/String;


# instance fields
.field private b:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

.field private d:Landroid/content/Context;

.field private e:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-string v0, ""

    sput-object v0, Lcom/sec/android/app/samsungapps/SaccountReceiver;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 68
    new-instance v0, Lcom/sec/android/app/samsungapps/hs;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/hs;-><init>(Lcom/sec/android/app/samsungapps/SaccountReceiver;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SaccountReceiver;->e:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/SaccountReceiver;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 21
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SaccountReceiver;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;-><init>()V

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SaccountReceiver;->d:Landroid/content/Context;

    invoke-direct {v0, v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;)V

    :cond_0
    const-string v1, ""

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeLastMCC(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->setDisclaimerAgreed(Z)V

    invoke-interface {v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->setDisclaimerVersion(Ljava/lang/String;)V

    invoke-interface {v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeURL(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/SaccountReceiver;)V
    .locals 3

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SaccountReceiver;->c:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->getDbData(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.app.samsungapps.res.mcc"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "SAMSUNGAPPS_MCC"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SaccountReceiver;->d:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/SaccountReceiver;)V
    .locals 3

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SaccountReceiver;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SaccountReceiver;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->isLogedIn()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;->logout()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SaccountReceiver;->d:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/ht;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/ht;-><init>(Lcom/sec/android/app/samsungapps/SaccountReceiver;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SaccountReceiver;->c:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v1, 0x10

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->setDbData(ILjava/lang/String;)Z

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SaccountReceiver;->c:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v1, 0x43

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->setDbData(ILjava/lang/String;)Z

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SaccountReceiver;->c:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v1, 0x44

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->setDbData(ILjava/lang/String;)Z

    return-void
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/SaccountReceiver;)V
    .locals 3

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SaccountReceiver;->b:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v1, "DF_SKIP_SACCOUNT"

    const-string v2, "N"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    return-void
.end method

.method static synthetic e(Lcom/sec/android/app/samsungapps/SaccountReceiver;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 21
    const-string v0, "1"

    sget-object v1, Lcom/sec/android/app/samsungapps/SaccountReceiver;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eq v0, v2, :cond_0

    const-string v0, "0"

    sget-object v1, Lcom/sec/android/app/samsungapps/SaccountReceiver;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v2, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SaccountReceiver;->b:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v1, "DF_AD_SETTING"

    sget-object v2, Lcom/sec/android/app/samsungapps/SaccountReceiver;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    :cond_1
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 38
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/SaccountReceiver;->d:Landroid/content/Context;

    .line 39
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SaccountReceiver;->b:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    .line 40
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SaccountReceiver;->c:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    .line 42
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 44
    const-string v1, "com.sec.android.app.samsungapps.reset.virtualMCC"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v2, :cond_1

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SaccountReceiver;->e:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 66
    :cond_0
    :goto_0
    return-void

    .line 48
    :cond_1
    const-string v1, "com.sec.android.app.samsungapps.req.mcc"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v2, :cond_2

    .line 50
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SaccountReceiver;->e:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 52
    :cond_2
    const-string v1, "android.intent.action.SAMSUNGACCOUNT_SIGNOUT_COMPLETED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v2, :cond_3

    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isRegisteredSamsungAccount(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SaccountReceiver;->e:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 56
    :cond_3
    const-string v1, "android.intent.action.SAMSUNGACCOUNT_CHANGED_PASSWORD_COMPLETED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v2, :cond_4

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SaccountReceiver;->e:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 60
    :cond_4
    const-string v1, "com.sec.android.app.ad.onoff.setting.fromAPK"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v2, :cond_0

    .line 62
    const-string v0, ""

    sput-object v0, Lcom/sec/android/app/samsungapps/SaccountReceiver;->a:Ljava/lang/String;

    .line 63
    const-string v0, "onoff"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/samsungapps/SaccountReceiver;->a:Ljava/lang/String;

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SaccountReceiver;->e:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method
