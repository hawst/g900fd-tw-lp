.class final Lcom/sec/android/app/samsungapps/pushclient/a;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/pushclient/PushInterface;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/pushclient/PushInterface;)V
    .locals 0

    .prologue
    .line 19
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/pushclient/a;->a:Lcom/sec/android/app/samsungapps/pushclient/PushInterface;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    .prologue
    .line 24
    const-string v0, "[ServiceConnection] onServiceConnected"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/a;->a:Lcom/sec/android/app/samsungapps/pushclient/PushInterface;

    invoke-static {p2}, Lcom/sec/spp/push/IPushClientService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/spp/push/IPushClientService;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/pushclient/PushInterface;->a(Lcom/sec/android/app/samsungapps/pushclient/PushInterface;Lcom/sec/spp/push/IPushClientService;)Lcom/sec/spp/push/IPushClientService;

    .line 27
    return-void
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 32
    const-string v0, "[ServiceConnection]onServiceDisconnected"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/a;->a:Lcom/sec/android/app/samsungapps/pushclient/PushInterface;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/pushclient/PushInterface;->a(Lcom/sec/android/app/samsungapps/pushclient/PushInterface;Lcom/sec/spp/push/IPushClientService;)Lcom/sec/spp/push/IPushClientService;

    .line 34
    return-void
.end method
