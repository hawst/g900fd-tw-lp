.class final synthetic Lcom/sec/android/app/samsungapps/cn;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field static final synthetic a:[I

.field static final synthetic b:[I

.field static final synthetic c:[I

.field static final synthetic d:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 349
    invoke-static {}, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->values()[Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/sec/android/app/samsungapps/cn;->d:[I

    :try_start_0
    sget-object v0, Lcom/sec/android/app/samsungapps/cn;->d:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ShowChangeCreditCardView:Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_a

    .line 333
    :goto_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;->values()[Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/sec/android/app/samsungapps/cn;->c:[I

    :try_start_1
    sget-object v0, Lcom/sec/android/app/samsungapps/cn;->c:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;->DisplayBannerContentList:Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_9

    .line 292
    :goto_1
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->values()[Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/sec/android/app/samsungapps/cn;->b:[I

    :try_start_2
    sget-object v0, Lcom/sec/android/app/samsungapps/cn;->b:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->AccountEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_8

    :goto_2
    :try_start_3
    sget-object v0, Lcom/sec/android/app/samsungapps/cn;->b:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->CreditCardRemoved:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_7

    .line 199
    :goto_3
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->values()[Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/sec/android/app/samsungapps/cn;->a:[I

    :try_start_4
    sget-object v0, Lcom/sec/android/app/samsungapps/cn;->a:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->AmericanExpress:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_6

    :goto_4
    :try_start_5
    sget-object v0, Lcom/sec/android/app/samsungapps/cn;->a:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->Discover:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :goto_5
    :try_start_6
    sget-object v0, Lcom/sec/android/app/samsungapps/cn;->a:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->Etc:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_4

    :goto_6
    :try_start_7
    sget-object v0, Lcom/sec/android/app/samsungapps/cn;->a:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->Invalid:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_3

    :goto_7
    :try_start_8
    sget-object v0, Lcom/sec/android/app/samsungapps/cn;->a:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->JCB:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_2

    :goto_8
    :try_start_9
    sget-object v0, Lcom/sec/android/app/samsungapps/cn;->a:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->MasterCard:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_1

    :goto_9
    :try_start_a
    sget-object v0, Lcom/sec/android/app/samsungapps/cn;->a:[I

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->VISA:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_0

    :goto_a
    return-void

    :catch_0
    move-exception v0

    goto :goto_a

    :catch_1
    move-exception v0

    goto :goto_9

    :catch_2
    move-exception v0

    goto :goto_8

    :catch_3
    move-exception v0

    goto :goto_7

    :catch_4
    move-exception v0

    goto :goto_6

    :catch_5
    move-exception v0

    goto :goto_5

    :catch_6
    move-exception v0

    goto :goto_4

    :catch_7
    move-exception v0

    goto :goto_3

    :catch_8
    move-exception v0

    goto :goto_2

    :catch_9
    move-exception v0

    goto/16 :goto_1

    :catch_a
    move-exception v0

    goto/16 :goto_0
.end method
