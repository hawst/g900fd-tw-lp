.class public Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedStateMachine;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;
.source "ProGuard"


# static fields
.field private static instance:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedStateMachine;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;-><init>()V

    .line 15
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedStateMachine;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedStateMachine;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedStateMachine;

    if-nez v0, :cond_0

    .line 21
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedStateMachine;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedStateMachine;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedStateMachine;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedStateMachine;

    .line 23
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedStateMachine;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedStateMachine;

    return-object v0
.end method


# virtual methods
.method protected entry(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 2

    .prologue
    .line 69
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/c;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 83
    :goto_0
    return-void

    .line 72
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;->GET_APK_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 73
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;->REQUEST_PERMISSION_FROM_SERVER:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 76
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;->COMPARE_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 79
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;->SEND_SIG_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 80
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 69
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;)Z
    .locals 2

    .prologue
    .line 29
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/c;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 64
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 32
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/c;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 35
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$State;->ASKPERMISSION:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 40
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/c;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 43
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$State;->COMPARE:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 46
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 51
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/c;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    goto :goto_0

    .line 54
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;->SIG_SAME_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 55
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 58
    :pswitch_7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;->SIG_CHANGED_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 59
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 29
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_5
    .end packed-switch

    .line 32
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch

    .line 40
    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 51
    :pswitch_data_3
    .packed-switch 0x4
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public bridge synthetic execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 9
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/PermissionChangedStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Event;)Z

    move-result v0

    return v0
.end method

.method protected exit(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 0

    .prologue
    .line 89
    return-void
.end method
