.class public Lcom/sec/android/app/samsungapps/PSMSRandomKeyConfirmActivity;
.super Lcom/sec/android/app/samsungapps/ActionBarActivity;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey$IConfirmPasswordUI;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/ActivityHelper;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey;

.field private c:Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 24
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;-><init>()V

    .line 26
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PSMSRandomKeyConfirmActivity;->a:Lcom/sec/android/app/samsungapps/ActivityHelper;

    .line 27
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PSMSRandomKeyConfirmActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey;

    .line 28
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PSMSRandomKeyConfirmActivity;->c:Landroid/text/TextWatcher;

    .line 24
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/PSMSRandomKeyConfirmActivity;)Lcom/sec/android/app/samsungapps/ActivityHelper;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PSMSRandomKeyConfirmActivity;->a:Lcom/sec/android/app/samsungapps/ActivityHelper;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/PSMSRandomKeyConfirmActivity;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PSMSRandomKeyConfirmActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey;

    return-object v0
.end method


# virtual methods
.method public getTextWatcher()Landroid/text/TextWatcher;
    .locals 1

    .prologue
    .line 152
    new-instance v0, Lcom/sec/android/app/samsungapps/fv;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/fv;-><init>(Lcom/sec/android/app/samsungapps/PSMSRandomKeyConfirmActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PSMSRandomKeyConfirmActivity;->c:Landroid/text/TextWatcher;

    return-object v0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PSMSRandomKeyConfirmActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey;->cancel()V

    .line 195
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onBackPressed()V

    .line 196
    return-void
.end method

.method public onConfirmPassworCompleted(Z)V
    .locals 1

    .prologue
    .line 185
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 187
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PSMSRandomKeyConfirmActivity;->finish()V

    .line 189
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 33
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 38
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PSMSRandomKeyConfirmActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->readObject(Landroid/content/Intent;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PSMSRandomKeyConfirmActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey;

    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PSMSRandomKeyConfirmActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey;

    if-nez v0, :cond_0

    .line 41
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PSMSRandomKeyConfirmActivity;->finish()V

    .line 57
    :goto_0
    return-void

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PSMSRandomKeyConfirmActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey;->invokeCompleted(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey$IConfirmPasswordUI;)V

    .line 52
    const v0, 0x7f0400ad

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/PSMSRandomKeyConfirmActivity;->setContentView(I)V

    .line 53
    const v0, 0x7f0c0039

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/PSMSRandomKeyConfirmActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PSMSRandomKeyConfirmActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    .line 54
    const v1, 0x7f08026f

    const v5, 0xb0003

    move-object v0, p0

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/PSMSRandomKeyConfirmActivity;->setActionBarConfiguration(ILcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PSMSRandomKeyConfirmActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey;->getProductName()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PSMSRandomKeyConfirmActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey;->getFormattedSellingPrice()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PSMSRandomKeyConfirmActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f08031b

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v0, 0x7f0c026b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/PSMSRandomKeyConfirmActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v1, v5, v6

    aput-object v2, v5, v3

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Lcom/sec/android/app/samsungapps/ActivityHelper;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/ActivityHelper;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PSMSRandomKeyConfirmActivity;->a:Lcom/sec/android/app/samsungapps/ActivityHelper;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PSMSRandomKeyConfirmActivity;->a:Lcom/sec/android/app/samsungapps/ActivityHelper;

    const v1, 0x7f0c0269

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->a(I)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PSMSRandomKeyConfirmActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey;->getRandomKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PSMSRandomKeyConfirmActivity;->a:Lcom/sec/android/app/samsungapps/ActivityHelper;

    const v1, 0x7f0c0268

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->b(I)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PSMSRandomKeyConfirmActivity;->getTextWatcher()Landroid/text/TextWatcher;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    const v0, 0x7f0802ef

    const v1, 0x7f08023d

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->showPositiveNegativeButton(Landroid/app/Activity;II)V

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getPositiveButton(Landroid/app/Activity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setEnabled(Z)V

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getPositiveButton(Landroid/app/Activity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PSMSRandomKeyConfirmActivity;->onPositive()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getNegativeButton(Landroid/app/Activity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PSMSRandomKeyConfirmActivity;->onNegative()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PSMSRandomKeyConfirmActivity;->c:Landroid/text/TextWatcher;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PSMSRandomKeyConfirmActivity;->a:Lcom/sec/android/app/samsungapps/ActivityHelper;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PSMSRandomKeyConfirmActivity;->a:Lcom/sec/android/app/samsungapps/ActivityHelper;

    const v1, 0x7f0c0268

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->b(I)Landroid/widget/EditText;

    move-result-object v0

    .line 71
    if-eqz v0, :cond_0

    .line 73
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PSMSRandomKeyConfirmActivity;->c:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 77
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onDestroy()V

    .line 78
    return-void
.end method

.method public onEnteredWrongPassword()V
    .locals 0

    .prologue
    .line 202
    return-void
.end method

.method public onNegative()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 135
    new-instance v0, Lcom/sec/android/app/samsungapps/fu;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/fu;-><init>(Lcom/sec/android/app/samsungapps/PSMSRandomKeyConfirmActivity;)V

    return-object v0
.end method

.method public onPositive()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 116
    new-instance v0, Lcom/sec/android/app/samsungapps/ft;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/ft;-><init>(Lcom/sec/android/app/samsungapps/PSMSRandomKeyConfirmActivity;)V

    return-object v0
.end method

.method protected setCurActivity()V
    .locals 0

    .prologue
    .line 63
    return-void
.end method
