.class public Lcom/sec/android/app/samsungapps/UpdateCheckSVC;
.super Landroid/app/Service;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/doc/IBaseContext;


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field c:Landroid/os/Handler;

.field d:Lcom/sec/android/app/samsungapps/updateCheck/aidl/IUpdateCheck$Stub;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/UpdateCheckSVC;->a:Ljava/lang/String;

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/UpdateCheckSVC;->b:Ljava/lang/String;

    .line 28
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/UpdateCheckSVC;->c:Landroid/os/Handler;

    .line 38
    new-instance v0, Lcom/sec/android/app/samsungapps/lp;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/lp;-><init>(Lcom/sec/android/app/samsungapps/UpdateCheckSVC;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/UpdateCheckSVC;->d:Lcom/sec/android/app/samsungapps/updateCheck/aidl/IUpdateCheck$Stub;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/UpdateCheckSVC;)V
    .locals 2

    .prologue
    .line 25
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;

    new-instance v1, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;)V

    new-instance v1, Lcom/sec/android/app/samsungapps/lw;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/lw;-><init>(Lcom/sec/android/app/samsungapps/UpdateCheckSVC;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr$IAutoUpdLoginObserver;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;->execute()V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/UpdateCheckSVC;Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager$IUpdateCheckSVCManagerObserver;)V
    .locals 5

    .prologue
    .line 25
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;)V

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;-><init>(Landroid/content/Context;)V

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;

    new-instance v4, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;

    invoke-direct {v4, p0}, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;-><init>(Landroid/content/Context;)V

    invoke-direct {v3, p0, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;)V

    new-instance v4, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListCheckerFactory;

    invoke-direct {v4, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListCheckerFactory;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;)V

    invoke-direct {v2, p0, v1, v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/autoupdlogin/AutoUpdLoginMgr;Lcom/sec/android/app/samsungapps/vlibrary2/update/IGetDownloadListCheckerFactory;)V

    invoke-virtual {v2, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager$IUpdateCheckSVCManagerObserver;)V

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager;->execute()V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/UpdateCheckSVC;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/UpdateCheckSVC;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/sec/android/app/samsungapps/UpdateCheckSVC;->b:Ljava/lang/String;

    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/UpdateCheckSVC;)V
    .locals 3

    .prologue
    .line 25
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "fakeModel"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/UpdateCheckSVC;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "GOSVERSION"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/UpdateCheckSVC;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/UpdateCheckSVC;->startActivity(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public getFakeModel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/UpdateCheckSVC;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getGearPlatformVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/UpdateCheckSVC;->b:Ljava/lang/String;

    return-object v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/UpdateCheckSVC;->d:Lcom/sec/android/app/samsungapps/updateCheck/aidl/IUpdateCheck$Stub;

    return-object v0
.end method
