.class public final enum Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

.field public static final enum ASK_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

.field public static final enum COMPLETING:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

.field public static final enum FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

.field public static final enum IDLE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

.field public static final enum INIT:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

.field public static final enum INSTALL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

.field public static final enum PROCESS:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

.field public static final enum SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 22
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    .line 23
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    const-string v1, "ASK_INSTALL"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;->ASK_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    .line 24
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    const-string v1, "INSTALL"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;->INSTALL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    .line 25
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    .line 26
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    const-string v1, "INIT"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;->INIT:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    .line 27
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    const-string v1, "PROCESS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;->PROCESS:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    .line 28
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    const-string v1, "COMPLETING"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;->COMPLETING:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    .line 29
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    const-string v1, "SUCCESS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    .line 20
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;->ASK_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;->INSTALL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;->INIT:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;->PROCESS:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;->COMPLETING:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$State;

    return-object v0
.end method
