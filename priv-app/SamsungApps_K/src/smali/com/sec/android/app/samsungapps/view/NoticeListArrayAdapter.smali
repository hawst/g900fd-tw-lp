.class public Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ProGuard"


# instance fields
.field private a:Landroid/content/Context;

.field private b:I

.field private c:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 29
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;->b:I

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;->c:Landroid/view/LayoutInflater;

    .line 35
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;->a:Landroid/content/Context;

    .line 36
    iput p2, p0, Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;->b:I

    .line 37
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;->c:Landroid/view/LayoutInflater;

    .line 38
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;->a:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 42
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notice;

    .line 47
    if-nez p2, :cond_0

    .line 48
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;->c:Landroid/view/LayoutInflater;

    iget v2, p0, Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;->b:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 49
    new-instance v1, Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter$NoticeListViewHolder;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;->a:Landroid/content/Context;

    invoke-direct {v1, p0, v2, p2}, Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter$NoticeListViewHolder;-><init>(Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;Landroid/content/Context;Landroid/view/View;)V

    .line 51
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 56
    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter$NoticeListViewHolder;->setNotice(Lcom/sec/android/app/samsungapps/vlibrary/doc/Notice;)V

    .line 58
    return-object p2

    .line 53
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter$NoticeListViewHolder;

    goto :goto_0
.end method
