.class public Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field public static final DF_25G_LIMIT_SIZE:I = 0x8

.field public static final DF_2G_LIMIT_SIZE:I = 0x7

.field public static final DF_3G_LIMIT_SIZE:I = 0x9

.field public static final DF_4G_LIMIT_SIZE:I = 0xa

.field public static final DF_ANDROMEDA_MCC:I = 0x3

.field public static final DF_BIRTH_DAY:I = 0x43

.field public static final DF_CAPP_FOLDER:I = 0x19

.field public static final DF_CAUTO_LOGIN:I = 0x2d

.field public static final DF_CBANNER_TYPE:I = 0x28

.field public static final DF_CCACHE_SIZE:I = 0x30

.field public static final DF_CCONFIG_ID:I = 0x40

.field public static final DF_CCURRENCY_UNIT_DIVISION:I = 0x3c

.field public static final DF_CCURRENCY_UNIT_HAS_PENNY:I = 0x36

.field public static final DF_CCURRENCY_UNIT_PRECEDES:I = 0x34

.field public static final DF_CDATA_FOLDER:I = 0x1a

.field public static final DF_CDEBUG_MODE:I = 0x33

.field public static final DF_CDISCLAIMER_VERSION:I = 0x2c

.field public static final DF_CFAKE_CSC:I = 0x20

.field public static final DF_CFAKE_HEADER_HOST:I = 0x23

.field public static final DF_CFAKE_IMEI:I = 0x1c

.field public static final DF_CFAKE_MCC:I = 0x1d

.field public static final DF_CFAKE_MNC:I = 0x1e

.field public static final DF_CFAKE_MODEL:I = 0x1f

.field public static final DF_CFAKE_ODC_VERSION:I = 0x21

.field public static final DF_CFAKE_OPEN_API_VERSION:I = 0x22

.field public static final DF_CFREE_TAB_PRECEDES:I = 0x3a

.field public static final DF_CHEADER_INFINITY_VERSION:I = 0x25

.field public static final DF_CHUB_URL:I = 0x13

.field public static final DF_CIMG_FOLDER:I = 0x18

.field public static final DF_CIS_STAGING:I = 0x14

.field public static final DF_CLAST_BANNER_IMG_URL:I = 0x27

.field public static final DF_CLAST_BANNER_PRODUCT_ID:I = 0x26

.field public static final DF_CLAST_COUNTRY_CODE:I = 0x35

.field public static final DF_CLAST_COUNTRY_FREE_STORE:I = 0x24

.field public static final DF_CLAST_CSC:I = 0x2b

.field public static final DF_CLAST_MCC:I = 0x29

.field public static final DF_CLAST_MNC:I = 0x2a

.field public static final DF_CLAST_MODEL:I = 0x4e

.field public static final DF_CLICENSE_URL:I = 0x37

.field public static final DF_CNOTIFICATION_POPUP_SHOW_SET:I = 0x3f

.field public static final DF_COUNTRY_URL:I = 0x4

.field public static final DF_CPLATFORM_KEY:I = 0x38

.field public static final DF_CPROXY_ADDRESS:I = 0x2f

.field public static final DF_CPSMS_TEST_MODE:I = 0x3e

.field public static final DF_CSAMSUNGAPPS_NOTI_SHOW:I = 0x3d

.field public static final DF_CSNS_CAPA_MASK:I = 0x3b

.field public static final DF_CSTAGING_APP_HOST_URL:I = 0x16

.field public static final DF_CSTAGING_DATA_HOST_URL:I = 0x17

.field public static final DF_CSTAGING_FREE_DATA_URL:I = 0x31

.field public static final DF_CSTAGING_IMG_HOST_URL:I = 0x15

.field public static final DF_CSTAGING_UNC_DATA_URL:I = 0x32

.field public static final DF_CTEMP_FOLDER:I = 0x1b

.field public static final DF_CTRANSFER_CONFIG:I = 0x39

.field public static final DF_CUNC_MODE:I = 0x2e

.field public static final DF_DISCLAIMER_SKIP:I = 0x5

.field public static final DF_DISCLAIMER_VERSION:I = 0x6

.field public static final DF_EMAIL_ID:I = 0x10

.field public static final DF_ENCRYPTION:I = 0x12

.field public static final DF_FAKE_CSC_CKECK:I = 0xc

.field public static final DF_FLEXIBLE_TAB_NAME:I = 0x4a

.field public static final DF_FLEXIBLE_TAB_SHOW:I = 0x49

.field public static final DF_ID:I = 0x0

.field public static final DF_INFO:I = 0x44

.field public static final DF_IRAN_SHETAB_CARD_URL:I = 0x4c

.field public static final DF_MCC:I = 0x2

.field public static final DF_NETWORK_OPERATOR:I = 0x42

.field public static final DF_NOTI_POPUP:I = 0xf

.field public static final DF_OPTION_12:I = 0x50

.field public static final DF_OPTION_13:I = 0x51

.field public static final DF_OPTION_14:I = 0x52

.field public static final DF_OPTION_15:I = 0x53

.field public static final DF_OPTION_16:I = 0x54

.field public static final DF_OPTION_17:I = 0x55

.field public static final DF_OPTION_18:I = 0x56

.field public static final DF_OPTION_19:I = 0x57

.field public static final DF_OPTION_20:I = 0x58

.field public static final DF_OPTION_21:I = 0x59

.field public static final DF_OPTION_22:I = 0x5a

.field public static final DF_OPTION_23:I = 0x5b

.field public static final DF_OPTION_24:I = 0x5c

.field public static final DF_OPTION_25:I = 0x5d

.field public static final DF_OPTION_26:I = 0x5e

.field public static final DF_OPTION_27:I = 0x5f

.field public static final DF_OPTION_28:I = 0x60

.field public static final DF_OPTION_29:I = 0x61

.field public static final DF_OPTION_30:I = 0x62

.field public static final DF_OPTION_31:I = 0x63

.field public static final DF_OPTION_32:I = 0x64

.field public static final DF_PHONE_NUM:I = 0x41

.field public static final DF_PROTOCOL_CACHING_TIME:I = 0x4b

.field public static final DF_PUSH_NOTI_REGISTRATION_SUCCESS:I = 0x4d

.field public static final DF_PUSH_SERVICE_ONOFF:I = 0x45

.field public static final DF_PUSH_SERVICE_REG_ID:I = 0x47

.field public static final DF_RESERVED:I = 0x11

.field public static final DF_SAMSUNGAPPS_MODE:I = 0x1

.field public static final DF_SECURE_TIME:I = 0x46

.field public static final DF_SERVER_LOAD_LEVEL:I = 0x4f

.field public static final DF_SKIP_SIGN_IN:I = 0xe

.field public static final DF_UNA_SETTING:I = 0x48

.field public static final DF_USER_ID:I = 0xd

.field public static final DF_WIFI_LIMIT_SIZE:I = 0xb

.field public static DbDataFieldString:[[Ljava/lang/String;

.field private static final PROJECTION2:[Ljava/lang/String;

.field private static _DbCache:Lcom/sec/android/app/samsungapps/vlibrary2/db/DbCache;


# instance fields
.field private mContentResolver:Landroid/content/ContentResolver;

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 122
    const/16 v0, 0x65

    new-array v0, v0, [[Ljava/lang/String;

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "DF_ID"

    aput-object v2, v1, v4

    const-string v2, "0"

    aput-object v2, v1, v5

    aput-object v1, v0, v4

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "DF_SAMSUNGAPPS_MODE"

    aput-object v2, v1, v4

    const-string v2, "1"

    aput-object v2, v1, v5

    aput-object v1, v0, v5

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "DF_MCC"

    aput-object v2, v1, v4

    const-string v2, "2"

    aput-object v2, v1, v5

    aput-object v1, v0, v6

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "DF_ANDROMEDA_MCC"

    aput-object v2, v1, v4

    const-string v2, "3"

    aput-object v2, v1, v5

    aput-object v1, v0, v7

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "DF_COUNTRY_URL"

    aput-object v2, v1, v4

    const-string v2, "4"

    aput-object v2, v1, v5

    aput-object v1, v0, v8

    const/4 v1, 0x5

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_DISCLAIMER_SKIP"

    aput-object v3, v2, v4

    const-string v3, "5"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_DISCLAIMER_VERSION"

    aput-object v3, v2, v4

    const-string v3, "6"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_2G_LIMIT_SIZE"

    aput-object v3, v2, v4

    const-string v3, "7"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_25G_LIMIT_SIZE"

    aput-object v3, v2, v4

    const-string v3, "8"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_3G_LIMIT_SIZE"

    aput-object v3, v2, v4

    const-string v3, "9"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_4G_LIMIT_SIZE"

    aput-object v3, v2, v4

    const-string v3, "10"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_WIFI_LIMIT_SIZE"

    aput-object v3, v2, v4

    const-string v3, "11"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_FAKE_CSC_CKECK"

    aput-object v3, v2, v4

    const-string v3, "12"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_USER_ID"

    aput-object v3, v2, v4

    const-string v3, "13"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xe

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_SKIP_SIGN_IN"

    aput-object v3, v2, v4

    const-string v3, "14"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xf

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_NOTI_POPUP"

    aput-object v3, v2, v4

    const-string v3, "15"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x10

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_EMAIL_ID"

    aput-object v3, v2, v4

    const-string v3, "16"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x11

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_RESERVED"

    aput-object v3, v2, v4

    const-string v3, "17"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x12

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_ENCRYPTION"

    aput-object v3, v2, v4

    const-string v3, "18"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x13

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CHUB_URL"

    aput-object v3, v2, v4

    const-string v3, "19"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x14

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CIS_STAGING"

    aput-object v3, v2, v4

    const-string v3, "20"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x15

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CSTAGING_IMG_HOST_URL"

    aput-object v3, v2, v4

    const-string v3, "21"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x16

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CSTAGING_APP_HOST_URL"

    aput-object v3, v2, v4

    const-string v3, "22"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x17

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CSTAGING_DATA_HOST_URL"

    aput-object v3, v2, v4

    const-string v3, "23"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x18

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CIMG_FOLDER"

    aput-object v3, v2, v4

    const-string v3, "24"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x19

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CAPP_FOLDER"

    aput-object v3, v2, v4

    const-string v3, "25"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CDATA_FOLDER"

    aput-object v3, v2, v4

    const-string v3, "26"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CTEMP_FOLDER"

    aput-object v3, v2, v4

    const-string v3, "27"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CFAKE_IMEI"

    aput-object v3, v2, v4

    const-string v3, "28"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CFAKE_MCC"

    aput-object v3, v2, v4

    const-string v3, "29"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CFAKE_MNC"

    aput-object v3, v2, v4

    const-string v3, "30"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CFAKE_MODEL"

    aput-object v3, v2, v4

    const-string v3, "31"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x20

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CFAKE_CSC"

    aput-object v3, v2, v4

    const-string v3, "32"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x21

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CFAKE_ODC_VERSION"

    aput-object v3, v2, v4

    const-string v3, "33"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x22

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CFAKE_OPEN_API_VERSION"

    aput-object v3, v2, v4

    const-string v3, "34"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x23

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CFAKE_HEADER_HOST"

    aput-object v3, v2, v4

    const-string v3, "35"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x24

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CLAST_COUNTRY_FREE_STORE"

    aput-object v3, v2, v4

    const-string v3, "36"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x25

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CHEADER_INFINITY_VERSION"

    aput-object v3, v2, v4

    const-string v3, "37"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x26

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CLAST_BANNER_PRODUCT_ID"

    aput-object v3, v2, v4

    const-string v3, "38"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x27

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CLAST_BANNER_IMG_URL"

    aput-object v3, v2, v4

    const-string v3, "39"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x28

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CBANNER_TYPE"

    aput-object v3, v2, v4

    const-string v3, "40"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x29

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CLAST_MCC"

    aput-object v3, v2, v4

    const-string v3, "41"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CLAST_MNC"

    aput-object v3, v2, v4

    const-string v3, "42"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CLAST_CSC"

    aput-object v3, v2, v4

    const-string v3, "43"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CDISCLAIMER_VERSION"

    aput-object v3, v2, v4

    const-string v3, "44"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CAUTO_LOGIN"

    aput-object v3, v2, v4

    const-string v3, "45"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CUNC_MODE"

    aput-object v3, v2, v4

    const-string v3, "46"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CPROXY_ADDRESS"

    aput-object v3, v2, v4

    const-string v3, "47"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x30

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CCACHE_SIZE"

    aput-object v3, v2, v4

    const-string v3, "48"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x31

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CSTAGING_FREE_DATA_URL"

    aput-object v3, v2, v4

    const-string v3, "49"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x32

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CSTAGING_UNC_DATA_URL"

    aput-object v3, v2, v4

    const-string v3, "50"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x33

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CDEBUG_MODE"

    aput-object v3, v2, v4

    const-string v3, "51"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x34

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CCURRENCY_UNIT_PRECEDES"

    aput-object v3, v2, v4

    const-string v3, "52"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x35

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CLAST_COUNTRY_CODE"

    aput-object v3, v2, v4

    const-string v3, "53"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x36

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CCURRENCY_UNIT_HAS_PENNY"

    aput-object v3, v2, v4

    const-string v3, "54"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x37

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CLICENSE_URL"

    aput-object v3, v2, v4

    const-string v3, "55"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x38

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CPLATFORM_KEY"

    aput-object v3, v2, v4

    const-string v3, "56"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x39

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CTRANSFER_CONFIG"

    aput-object v3, v2, v4

    const-string v3, "57"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CFREE_TAB_PRECEDES"

    aput-object v3, v2, v4

    const-string v3, "58"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CSNS_CAPA_MASK"

    aput-object v3, v2, v4

    const-string v3, "59"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CCURRENCY_UNIT_DIVISION"

    aput-object v3, v2, v4

    const-string v3, "60"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CSAMSUNGAPPS_NOTI_SHOW"

    aput-object v3, v2, v4

    const-string v3, "61"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CPSMS_TEST_MODE"

    aput-object v3, v2, v4

    const-string v3, "62"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CNOTIFICATION_POPUP_SHOW_SET"

    aput-object v3, v2, v4

    const-string v3, "63"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x40

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CCONFIG_ID"

    aput-object v3, v2, v4

    const-string v3, "64"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x41

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_PHONE_NUM"

    aput-object v3, v2, v4

    const-string v3, "65"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x42

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_NETWORK_OPERATOR"

    aput-object v3, v2, v4

    const-string v3, "66"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x43

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_BIRTH_DAY"

    aput-object v3, v2, v4

    const-string v3, "67"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x44

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_INFO"

    aput-object v3, v2, v4

    const-string v3, "68"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x45

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_PUSH_SERVICE_ONOFF"

    aput-object v3, v2, v4

    const-string v3, "69"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x46

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_SECURE_TIME"

    aput-object v3, v2, v4

    const-string v3, "70"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x47

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_PUSH_SERVICE_REG_ID"

    aput-object v3, v2, v4

    const-string v3, "71"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x48

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_UNA_SETTING"

    aput-object v3, v2, v4

    const-string v3, "72"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x49

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_FLEXIBLE_TAB_SHOW"

    aput-object v3, v2, v4

    const-string v3, "73"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_FLEXIBLE_TAB_NAME"

    aput-object v3, v2, v4

    const-string v3, "74"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_PROTOCOL_CACHING_TIME"

    aput-object v3, v2, v4

    const-string v3, "75"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_IRAN_SHETAB_CARD_URL"

    aput-object v3, v2, v4

    const-string v3, "76"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_PUSH_NOTI_REGISTRATION_SUCCESS"

    aput-object v3, v2, v4

    const-string v3, "77"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_CLAST_MODEL"

    aput-object v3, v2, v4

    const-string v3, "78"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_SERVER_LOAD_LEVEL"

    aput-object v3, v2, v4

    const-string v3, "79"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x50

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_OPTION_12"

    aput-object v3, v2, v4

    const-string v3, "80"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x51

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_OPTION_13"

    aput-object v3, v2, v4

    const-string v3, "81"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x52

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_OPTION_14"

    aput-object v3, v2, v4

    const-string v3, "82"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x53

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_OPTION_15"

    aput-object v3, v2, v4

    const-string v3, "83"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x54

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_OPTION_16"

    aput-object v3, v2, v4

    const-string v3, "84"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x55

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_OPTION_17"

    aput-object v3, v2, v4

    const-string v3, "85"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x56

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_OPTION_18"

    aput-object v3, v2, v4

    const-string v3, "86"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x57

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_OPTION_19"

    aput-object v3, v2, v4

    const-string v3, "87"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x58

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_OPTION_20"

    aput-object v3, v2, v4

    const-string v3, "88"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x59

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_OPTION_21"

    aput-object v3, v2, v4

    const-string v3, "89"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_OPTION_22"

    aput-object v3, v2, v4

    const-string v3, "90"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_OPTION_23"

    aput-object v3, v2, v4

    const-string v3, "91"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_OPTION_24"

    aput-object v3, v2, v4

    const-string v3, "92"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_OPTION_25"

    aput-object v3, v2, v4

    const-string v3, "93"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_OPTION_26"

    aput-object v3, v2, v4

    const-string v3, "94"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_OPTION_27"

    aput-object v3, v2, v4

    const-string v3, "95"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x60

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_OPTION_28"

    aput-object v3, v2, v4

    const-string v3, "96"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x61

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_OPTION_29"

    aput-object v3, v2, v4

    const-string v3, "97"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x62

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_OPTION_30"

    aput-object v3, v2, v4

    const-string v3, "98"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x63

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_OPTION_31"

    aput-object v3, v2, v4

    const-string v3, "99"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x64

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "DF_OPTION_32"

    aput-object v3, v2, v4

    const-string v3, "100"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->DbDataFieldString:[[Ljava/lang/String;

    .line 226
    const/16 v0, 0x65

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "SamsungAppsMode"

    aput-object v1, v0, v5

    const-string v1, "LatestMCC"

    aput-object v1, v0, v6

    const-string v1, "AndromedaMCC"

    aput-object v1, v0, v7

    const-string v1, "CountryURL"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string v2, "DisclaimerSkip"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "DisclaimerVersion"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "TwoGLimitSize"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "TwofiveGLimitSize"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "ThreeGLimitSize"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "FourGLimitSize"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "WifiLimitSize"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "FakeCSCCheck"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "userID"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "skipSignIn"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "NotiPopup"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "emailID"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "reserved"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "Encryption"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "CHubUrl"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "CIsStaging"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "CStagingImgHostUrl"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "CStagingAppHostUrl"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "CStagingDataHostUrl"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "CImgFolder"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "CAppFolder"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "CDataFolder"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "CTempFolder"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "CFakeImei"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "CFakeMcc"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "CFakeMnc"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "CFakeModel"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "CFakeCsc"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "CFakeOdcVer"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "CFakeOpenApiVer"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "CFakeHeaderHost"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "CLastCountryFreeStore"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "CHeaderInfinityVersion"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "CLastBannerProductId"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "CLastBannerImgUrl"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "CLastBannerType"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "CLastMcc"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "CLastMnc"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "CLastCsc"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "CDisclaimerVer"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, "CAutoLogin"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "CUncMode"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, "CProxyAddress"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, "CCacheSize"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, "CFreeDataUrl"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, "CUncDataUrl"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, "CDebugMode"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, "CCurrencyUnitPrecedes"

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, "CLastCountryCode"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string v2, "CCurrencyUnitHasPenny"

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, "CLicenseUrl"

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string v2, "CPlatformKey"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, "CTransferConfig"

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string v2, "CFreeTabPrecedes"

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string v2, "CSnsCapaMask"

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string v2, "CCurrencyUnitDivision"

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string v2, "CSamsungNotiShow"

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string v2, "CPsmsTestMode"

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string v2, "CNotificationPopupShowSet"

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string v2, "CConfigId"

    aput-object v2, v0, v1

    const/16 v1, 0x41

    const-string v2, "PhoneNumber"

    aput-object v2, v0, v1

    const/16 v1, 0x42

    const-string v2, "NetworkOperator"

    aput-object v2, v0, v1

    const/16 v1, 0x43

    const-string v2, "Birthday"

    aput-object v2, v0, v1

    const/16 v1, 0x44

    const-string v2, "Info"

    aput-object v2, v0, v1

    const/16 v1, 0x45

    const-string v2, "push_service_onoff"

    aput-object v2, v0, v1

    const/16 v1, 0x46

    const-string v2, "SecureTime"

    aput-object v2, v0, v1

    const/16 v1, 0x47

    const-string v2, "push_service_reg_id"

    aput-object v2, v0, v1

    const/16 v1, 0x48

    const-string v2, "una_setting"

    aput-object v2, v0, v1

    const/16 v1, 0x49

    const-string v2, "FlexibleTabShow"

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    const-string v2, "FlexibleTabName"

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    const-string v2, "ProtocolCachingTime"

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    const-string v2, "IranShetabCardUrl"

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    const-string v2, "PushNotiRegistrationSuccess"

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    const-string v2, "CLastModel"

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    const-string v2, "ServerLoadLevel"

    aput-object v2, v0, v1

    const/16 v1, 0x50

    const-string v2, "Option_12"

    aput-object v2, v0, v1

    const/16 v1, 0x51

    const-string v2, "Option_13"

    aput-object v2, v0, v1

    const/16 v1, 0x52

    const-string v2, "Option_14"

    aput-object v2, v0, v1

    const/16 v1, 0x53

    const-string v2, "Option_15"

    aput-object v2, v0, v1

    const/16 v1, 0x54

    const-string v2, "Option_16"

    aput-object v2, v0, v1

    const/16 v1, 0x55

    const-string v2, "Option_17"

    aput-object v2, v0, v1

    const/16 v1, 0x56

    const-string v2, "Option_18"

    aput-object v2, v0, v1

    const/16 v1, 0x57

    const-string v2, "Option_19"

    aput-object v2, v0, v1

    const/16 v1, 0x58

    const-string v2, "Option_20"

    aput-object v2, v0, v1

    const/16 v1, 0x59

    const-string v2, "Option_21"

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    const-string v2, "Option_22"

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    const-string v2, "Option_23"

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    const-string v2, "Option_24"

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    const-string v2, "Option_25"

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    const-string v2, "Option_26"

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    const-string v2, "Option_27"

    aput-object v2, v0, v1

    const/16 v1, 0x60

    const-string v2, "Option_28"

    aput-object v2, v0, v1

    const/16 v1, 0x61

    const-string v2, "Option_29"

    aput-object v2, v0, v1

    const/16 v1, 0x62

    const-string v2, "Option_30"

    aput-object v2, v0, v1

    const/16 v1, 0x63

    const-string v2, "Option_31"

    aput-object v2, v0, v1

    const/16 v1, 0x64

    const-string v2, "Option_32"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->PROJECTION2:[Ljava/lang/String;

    .line 330
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->_DbCache:Lcom/sec/android/app/samsungapps/vlibrary2/db/DbCache;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 336
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 332
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->mContentResolver:Landroid/content/ContentResolver;

    .line 333
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->mContext:Landroid/content/Context;

    .line 337
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->mContext:Landroid/content/Context;

    .line 338
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->mContentResolver:Landroid/content/ContentResolver;

    .line 339
    return-void
.end method

.method private loadDb()Lcom/sec/android/app/samsungapps/vlibrary2/db/DbCache;
    .locals 6

    .prologue
    .line 498
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/db/DbCache;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->PROJECTION2:[Ljava/lang/String;

    array-length v1, v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/db/DbCache;-><init>(I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->_DbCache:Lcom/sec/android/app/samsungapps/vlibrary2/db/DbCache;

    .line 501
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->mContentResolver:Landroid/content/ContentResolver;

    if-nez v0, :cond_0

    .line 505
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->mContentResolver:Landroid/content/ContentResolver;

    .line 507
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->CONTENT_URI2:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->PROJECTION2:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 513
    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 515
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 517
    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->getColumnCount()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 519
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 520
    if-eqz v2, :cond_1

    .line 522
    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->_DbCache:Lcom/sec/android/app/samsungapps/vlibrary2/db/DbCache;

    invoke-virtual {v3, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/db/DbCache;->set(ILjava/lang/String;)V

    .line 523
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->PROJECTION2:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->i(Ljava/lang/String;)V

    .line 517
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 527
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 539
    :cond_3
    :goto_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->_DbCache:Lcom/sec/android/app/samsungapps/vlibrary2/db/DbCache;

    return-object v0

    .line 532
    :catch_0
    move-exception v0

    const-string v0, "DbCommon::queryAllDB::SQLiteException"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    goto :goto_1

    .line 534
    :catch_1
    move-exception v0

    .line 536
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DbCommon::queryAllDB::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public clearAllDB()Z
    .locals 5

    .prologue
    .line 475
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->_DbCache:Lcom/sec/android/app/samsungapps/vlibrary2/db/DbCache;

    if-eqz v0, :cond_0

    .line 477
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->_DbCache:Lcom/sec/android/app/samsungapps/vlibrary2/db/DbCache;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/db/DbCache;->clear()V

    .line 479
    :cond_0
    const/4 v0, 0x0

    .line 482
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->mContentResolver:Landroid/content/ContentResolver;

    if-eqz v1, :cond_1

    .line 484
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->CONTENT_URI2:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 486
    :cond_1
    const/4 v0, 0x1

    .line 492
    :goto_0
    return v0

    .line 487
    :catch_0
    move-exception v1

    .line 489
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DbCommon::clearAllDB::"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getDbData(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 395
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->_DbCache:Lcom/sec/android/app/samsungapps/vlibrary2/db/DbCache;

    if-nez v0, :cond_0

    .line 397
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->loadDb()Lcom/sec/android/app/samsungapps/vlibrary2/db/DbCache;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->_DbCache:Lcom/sec/android/app/samsungapps/vlibrary2/db/DbCache;

    .line 399
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->_DbCache:Lcom/sec/android/app/samsungapps/vlibrary2/db/DbCache;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/db/DbCache;->get(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public queryAllDB()Ljava/util/Vector;
    .locals 7

    .prologue
    .line 547
    new-instance v6, Ljava/util/Vector;

    invoke-direct {v6}, Ljava/util/Vector;-><init>()V

    .line 551
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->mContentResolver:Landroid/content/ContentResolver;

    if-nez v0, :cond_0

    .line 555
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->mContentResolver:Landroid/content/ContentResolver;

    .line 557
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->CONTENT_URI2:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->PROJECTION2:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 563
    if-eqz v1, :cond_4

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_4

    .line 565
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 568
    :cond_1
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 570
    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->getColumnCount()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 572
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 573
    if-eqz v3, :cond_2

    .line 575
    sget-object v4, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->PROJECTION2:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 576
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->PROJECTION2:[Ljava/lang/String;

    aget-object v5, v5, v0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 570
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 580
    :cond_3
    invoke-virtual {v6, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 582
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 583
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 595
    :cond_4
    :goto_1
    return-object v6

    .line 588
    :catch_0
    move-exception v0

    const-string v0, "DbCommon::queryAllDB::SQLiteException"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    goto :goto_1

    .line 590
    :catch_1
    move-exception v0

    .line 592
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DbCommon::queryAllDB::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public resetDbData(I)Z
    .locals 6

    .prologue
    .line 444
    const/4 v0, 0x0

    .line 445
    :try_start_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 452
    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->PROJECTION2:[Ljava/lang/String;

    aget-object v2, v2, p1

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->CONTENT_URI2:Landroid/net/Uri;

    const-string v4, "_id=0"

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 460
    const/4 v0, 0x1

    .line 467
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public setDbData(ILjava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 343
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->_DbCache:Lcom/sec/android/app/samsungapps/vlibrary2/db/DbCache;

    if-eqz v1, :cond_0

    .line 345
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->_DbCache:Lcom/sec/android/app/samsungapps/vlibrary2/db/DbCache;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/db/DbCache;->set(ILjava/lang/String;)V

    .line 348
    :cond_0
    if-nez p2, :cond_1

    .line 390
    :goto_0
    return v0

    .line 371
    :cond_1
    :try_start_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 380
    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->PROJECTION2:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->CONTENT_URI2:Landroid/net/Uri;

    const-string v4, "_id=0"

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 383
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DbCommon::setDbData::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->PROJECTION2:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->i(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 384
    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method
