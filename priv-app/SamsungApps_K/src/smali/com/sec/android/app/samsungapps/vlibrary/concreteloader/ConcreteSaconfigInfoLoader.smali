.class public Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;


# static fields
.field static MAX_CONFIG_COUNT:I


# instance fields
.field private mAPILevel:Ljava/lang/String;

.field private mBannerImgUrl:Ljava/lang/String;

.field private mBannerProductID:Ljava/lang/String;

.field private mBannerType:Ljava/lang/String;

.field private mBillingServerType:Ljava/lang/String;

.field private mCSC:Ljava/lang/String;

.field private mCachedAppPath:Ljava/lang/String;

.field private mCachedDataPath:Ljava/lang/String;

.field private mCachedImgPath:Ljava/lang/String;

.field private mCachedTempPath:Ljava/lang/String;

.field private mConfigID:Ljava/lang/String;

.field private mCountryCode:Ljava/lang/String;

.field private mCurrencyUnitDivision:Ljava/lang/String;

.field private mCurrencyUnitHasPenny:Ljava/lang/String;

.field private mCurrencyUnitPrecedes:Ljava/lang/String;

.field private mDebugMode:Ljava/lang/String;

.field private mDisclaimerVersion:Ljava/lang/String;

.field private mFreeTabPrecedes:Ljava/lang/String;

.field private mHeaderHostUrl:Ljava/lang/String;

.field private mHeaderInfinityVersion:Ljava/lang/String;

.field private mHubUrl:Ljava/lang/String;

.field private mIMEI:Ljava/lang/String;

.field private mIsAutoLogin:Ljava/lang/String;

.field private mIsKnox2Mode:Ljava/lang/String;

.field private mIsSaconfig:Z

.field private mIsStaging:Ljava/lang/String;

.field private mIsUNC:Ljava/lang/String;

.field private mKnox2HomeType:Ljava/lang/String;

.field private mLastCSC:Ljava/lang/String;

.field private mLastMCC:Ljava/lang/String;

.field private mLastMNC:Ljava/lang/String;

.field private mLicenseUrl:Ljava/lang/String;

.field private mLimitCachedImgSize:Ljava/lang/String;

.field private mMCC:Ljava/lang/String;

.field private mMNC:Ljava/lang/String;

.field private mModel:Ljava/lang/String;

.field private mNetworkProxyAddress:Ljava/lang/String;

.field private mNotificationSet:Ljava/lang/String;

.field private mODCVersion:Ljava/lang/String;

.field private mODCVersionCode:Ljava/lang/String;

.field private mPlatformKey:Ljava/lang/String;

.field private mSamsungHubHost:Ljava/lang/String;

.field private mShowAoCNotiification:Ljava/lang/String;

.field private mSnsSupport:Ljava/lang/String;

.field private mStagingAppHostUrl:Ljava/lang/String;

.field private mStagingDataHostUrl:Ljava/lang/String;

.field private mStagingFreeDataUrl:Ljava/lang/String;

.field private mStagingImgHostUrl:Ljava/lang/String;

.field private mStagingUncDataUrl:Ljava/lang/String;

.field private mStoreType:Ljava/lang/String;

.field private mSupportPsmsTest:Ljava/lang/String;

.field private mTransferConfig:Ljava/lang/String;

.field private mUpdateInterval:Ljava/lang/String;

.field private mUseAPKVersionBilling:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    const/16 v0, 0x34

    sput v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->MAX_CONFIG_COUNT:I

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mIsSaconfig:Z

    .line 17
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mHubUrl:Ljava/lang/String;

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mIsStaging:Ljava/lang/String;

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mStagingImgHostUrl:Ljava/lang/String;

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mStagingAppHostUrl:Ljava/lang/String;

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mStagingDataHostUrl:Ljava/lang/String;

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mCachedImgPath:Ljava/lang/String;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mCachedAppPath:Ljava/lang/String;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mCachedDataPath:Ljava/lang/String;

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mCachedTempPath:Ljava/lang/String;

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mIMEI:Ljava/lang/String;

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mMCC:Ljava/lang/String;

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mMNC:Ljava/lang/String;

    .line 29
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mModel:Ljava/lang/String;

    .line 30
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mCSC:Ljava/lang/String;

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mODCVersion:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mAPILevel:Ljava/lang/String;

    .line 33
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mHeaderHostUrl:Ljava/lang/String;

    .line 34
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mStoreType:Ljava/lang/String;

    .line 35
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mHeaderInfinityVersion:Ljava/lang/String;

    .line 36
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mBannerProductID:Ljava/lang/String;

    .line 37
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mBannerImgUrl:Ljava/lang/String;

    .line 38
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mBannerType:Ljava/lang/String;

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mLastMCC:Ljava/lang/String;

    .line 40
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mLastMNC:Ljava/lang/String;

    .line 41
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mLastCSC:Ljava/lang/String;

    .line 42
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mDisclaimerVersion:Ljava/lang/String;

    .line 43
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mIsAutoLogin:Ljava/lang/String;

    .line 44
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mIsUNC:Ljava/lang/String;

    .line 45
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mNetworkProxyAddress:Ljava/lang/String;

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mLimitCachedImgSize:Ljava/lang/String;

    .line 47
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mStagingFreeDataUrl:Ljava/lang/String;

    .line 48
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mStagingUncDataUrl:Ljava/lang/String;

    .line 49
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mDebugMode:Ljava/lang/String;

    .line 50
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mCurrencyUnitPrecedes:Ljava/lang/String;

    .line 51
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mCountryCode:Ljava/lang/String;

    .line 52
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mCurrencyUnitHasPenny:Ljava/lang/String;

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mLicenseUrl:Ljava/lang/String;

    .line 54
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mPlatformKey:Ljava/lang/String;

    .line 55
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mTransferConfig:Ljava/lang/String;

    .line 56
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mFreeTabPrecedes:Ljava/lang/String;

    .line 57
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mSnsSupport:Ljava/lang/String;

    .line 58
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mCurrencyUnitDivision:Ljava/lang/String;

    .line 59
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mShowAoCNotiification:Ljava/lang/String;

    .line 60
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mSupportPsmsTest:Ljava/lang/String;

    .line 61
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mNotificationSet:Ljava/lang/String;

    .line 62
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mConfigID:Ljava/lang/String;

    .line 63
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mUpdateInterval:Ljava/lang/String;

    .line 64
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mBillingServerType:Ljava/lang/String;

    .line 65
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mSamsungHubHost:Ljava/lang/String;

    .line 66
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mIsKnox2Mode:Ljava/lang/String;

    .line 67
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mKnox2HomeType:Ljava/lang/String;

    .line 68
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mUseAPKVersionBilling:Ljava/lang/String;

    .line 69
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mODCVersionCode:Ljava/lang/String;

    .line 75
    const-string v0, "78,66,68,74,73,6b,6e,6c,33,6e,73,6e,"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->coverLang(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 79
    :try_start_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 85
    :goto_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 88
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mIsSaconfig:Z

    .line 92
    :try_start_1
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/FileReader;

    invoke-direct {v0, v1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    move v1, v2

    .line 94
    :goto_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    .line 100
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-ltz v2, :cond_0

    .line 102
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 106
    const-string v2, "="

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    .line 107
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v0, v2, v4}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 109
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 110
    packed-switch v1, :pswitch_data_0

    .line 199
    :goto_2
    :pswitch_0
    add-int/lit8 v0, v1, 0x1

    .line 201
    sget v1, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->MAX_CONFIG_COUNT:I

    if-le v0, v1, :cond_2

    .line 203
    :cond_0
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    .line 214
    :cond_1
    :goto_3
    return-void

    .line 80
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 81
    const-string v0, "/sdcard"

    .line 82
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 113
    :pswitch_1
    :try_start_2
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mHubUrl:Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_2

    .line 205
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_3

    .line 117
    :pswitch_2
    :try_start_3
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mIsStaging:Ljava/lang/String;
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_2

    .line 207
    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 120
    :pswitch_3
    :try_start_4
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mStagingImgHostUrl:Ljava/lang/String;
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_2

    .line 209
    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 123
    :pswitch_4
    :try_start_5
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mStagingAppHostUrl:Ljava/lang/String;

    goto :goto_2

    .line 126
    :pswitch_5
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mStagingDataHostUrl:Ljava/lang/String;

    goto :goto_2

    .line 130
    :pswitch_6
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mIMEI:Ljava/lang/String;

    goto :goto_2

    .line 134
    :pswitch_7
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mMCC:Ljava/lang/String;

    goto :goto_2

    .line 138
    :pswitch_8
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mMNC:Ljava/lang/String;

    goto :goto_2

    .line 142
    :pswitch_9
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mModel:Ljava/lang/String;

    goto :goto_2

    .line 146
    :pswitch_a
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mCSC:Ljava/lang/String;

    goto :goto_2

    .line 150
    :pswitch_b
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mODCVersion:Ljava/lang/String;

    goto :goto_2

    .line 154
    :pswitch_c
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mAPILevel:Ljava/lang/String;

    goto :goto_2

    .line 158
    :pswitch_d
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mHeaderHostUrl:Ljava/lang/String;

    goto :goto_2

    .line 162
    :pswitch_e
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mHeaderInfinityVersion:Ljava/lang/String;

    goto :goto_2

    .line 166
    :pswitch_f
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mDisclaimerVersion:Ljava/lang/String;

    goto :goto_2

    .line 170
    :pswitch_10
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mPlatformKey:Ljava/lang/String;

    goto :goto_2

    .line 174
    :pswitch_11
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mSupportPsmsTest:Ljava/lang/String;

    goto :goto_2

    .line 177
    :pswitch_12
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mUpdateInterval:Ljava/lang/String;

    goto :goto_2

    .line 180
    :pswitch_13
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mBillingServerType:Ljava/lang/String;

    goto :goto_2

    .line 183
    :pswitch_14
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mSamsungHubHost:Ljava/lang/String;

    goto :goto_2

    .line 186
    :pswitch_15
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mIsKnox2Mode:Ljava/lang/String;

    goto :goto_2

    .line 189
    :pswitch_16
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mKnox2HomeType:Ljava/lang/String;

    goto :goto_2

    .line 192
    :pswitch_17
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mUseAPKVersionBilling:Ljava/lang/String;

    goto :goto_2

    .line 195
    :pswitch_18
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mODCVersionCode:Ljava/lang/String;
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_2

    :cond_2
    move v1, v0

    goto/16 :goto_1

    .line 110
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_0
        :pswitch_e
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_f
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_10
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_11
        :pswitch_0
        :pswitch_0
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
    .end packed-switch
.end method


# virtual methods
.method public getBillingServerType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 386
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mBillingServerType:Ljava/lang/String;

    return-object v0
.end method

.method public getCSC()Ljava/lang/String;
    .locals 2

    .prologue
    .line 276
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mCSC:Ljava/lang/String;

    const-string v1, ""

    if-eq v0, v1, :cond_0

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mCSC:Ljava/lang/String;

    .line 279
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDisclaimerVersion()Ljava/lang/String;
    .locals 2

    .prologue
    .line 316
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mDisclaimerVersion:Ljava/lang/String;

    const-string v1, ""

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mDisclaimerVersion:Ljava/lang/String;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 317
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mDisclaimerVersion:Ljava/lang/String;

    .line 319
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getHeaderHost()Ljava/lang/String;
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mHeaderHostUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getHubHost()Ljava/lang/String;
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mSamsungHubHost:Ljava/lang/String;

    return-object v0
.end method

.method public getHubUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 223
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mHubUrl:Ljava/lang/String;

    const-string v1, ""

    if-eq v0, v1, :cond_0

    .line 224
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mHubUrl:Ljava/lang/String;

    .line 226
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIMEI()Ljava/lang/String;
    .locals 2

    .prologue
    .line 231
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mIMEI:Ljava/lang/String;

    const-string v1, ""

    if-eq v0, v1, :cond_0

    .line 232
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mIMEI:Ljava/lang/String;

    .line 234
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getInfinityVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 369
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mHeaderInfinityVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getKnox2HomeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mKnox2HomeType:Ljava/lang/String;

    return-object v0
.end method

.method public getMCC()Ljava/lang/String;
    .locals 2

    .prologue
    .line 239
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mMCC:Ljava/lang/String;

    const-string v1, ""

    if-eq v0, v1, :cond_0

    .line 240
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mMCC:Ljava/lang/String;

    .line 242
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMNC()Ljava/lang/String;
    .locals 2

    .prologue
    .line 247
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mMNC:Ljava/lang/String;

    const-string v1, ""

    if-eq v0, v1, :cond_0

    .line 248
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mMNC:Ljava/lang/String;

    .line 250
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getModelName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 255
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mModel:Ljava/lang/String;

    const-string v1, ""

    if-eq v0, v1, :cond_0

    .line 256
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mModel:Ljava/lang/String;

    .line 258
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getODCVersion()Ljava/lang/String;
    .locals 2

    .prologue
    .line 284
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mODCVersion:Ljava/lang/String;

    const-string v1, ""

    if-eq v0, v1, :cond_0

    .line 285
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mODCVersion:Ljava/lang/String;

    .line 287
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getODCVersionCode()Ljava/lang/String;
    .locals 2

    .prologue
    .line 292
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mODCVersionCode:Ljava/lang/String;

    const-string v1, ""

    if-eq v0, v1, :cond_0

    .line 293
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mODCVersionCode:Ljava/lang/String;

    .line 295
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getOpenAPIVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mAPILevel:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 302
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mAPILevel:Ljava/lang/String;

    .line 304
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPlatformKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mPlatformKey:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mPlatformKey:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 362
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mPlatformKey:Ljava/lang/String;

    .line 364
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSizeLimitation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 311
    const/4 v0, 0x0

    return-object v0
.end method

.method public getStageDataHostURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 350
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mStagingDataHostUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getStagingAppHostUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 381
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mStagingAppHostUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getStagingImgHostUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 375
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mStagingImgHostUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getUpdateInterval()I
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mUpdateInterval:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mUpdateInterval:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 265
    :cond_0
    const/4 v0, 0x0

    .line 270
    :goto_0
    return v0

    .line 269
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mUpdateInterval:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public isExistSaconfig()Z
    .locals 1

    .prologue
    .line 218
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mIsSaconfig:Z

    return v0
.end method

.method public isKnox2Mode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 401
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mIsKnox2Mode:Ljava/lang/String;

    return-object v0
.end method

.method public isPSMSMoNeededSkipping()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 333
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mSupportPsmsTest:Ljava/lang/String;

    const-string v2, ""

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mSupportPsmsTest:Ljava/lang/String;

    const-string v2, "2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v0, :cond_0

    .line 336
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTestPurchaseSupported()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 324
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mSupportPsmsTest:Ljava/lang/String;

    const-string v2, ""

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mSupportPsmsTest:Ljava/lang/String;

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eq v1, v0, :cond_0

    .line 327
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isUsingAPKVersionBilling()Z
    .locals 2

    .prologue
    .line 406
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mUseAPKVersionBilling:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, "1"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mUseAPKVersionBilling:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 407
    const/4 v0, 0x1

    .line 409
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isUsingStageURL()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 341
    const-string v1, "1"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteSaconfigInfoLoader;->mIsStaging:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v0, :cond_0

    .line 344
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
