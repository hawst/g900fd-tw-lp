.class public interface abstract Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract onAllCheckClick(Z)V
.end method

.method public abstract onChangeActionBar(I)V
.end method

.method public abstract onChangeActionItem(IZ)V
.end method

.method public abstract onClickTab(I)V
.end method

.method public abstract onDataLoadCompleted()V
.end method

.method public abstract onDataLoadingMore()V
.end method

.method public abstract refreshActionBar(Z)V
.end method

.method public abstract setPurchasedMode(I)V
.end method

.method public abstract setTopButtonLayoutVisible(Z)V
.end method

.method public abstract updatePopupButtonText(II)V
.end method
