.class final Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/d;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionLoader$IPermissionLoaderObserver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;)V
    .locals 0

    .prologue
    .line 193
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/d;->a:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onResult(Z)V
    .locals 2

    .prologue
    .line 197
    if-eqz p1, :cond_2

    .line 199
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/d;->a:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getPermission()Lcom/sec/android/app/samsungapps/vlibrary3/doc/Permission;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/d;->a:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->isFreeContent()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/d;->a:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->b(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/d;->a:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->SUCCESS_BUT_EMPTY_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;)V

    .line 219
    :goto_0
    return-void

    .line 207
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/d;->a:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->SUCCESS_EXIST_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;)V

    goto :goto_0

    .line 212
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/d;->a:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->SUCCESS_BUT_EMPTY_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;)V

    goto :goto_0

    .line 217
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/d;->a:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;->FAIL_RECEIVE_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager;Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Event;)V

    goto :goto_0
.end method
