.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum CHECK_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;

.field public static final enum INVOKE_POPUP:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;

.field public static final enum NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;

.field public static final enum NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;

.field public static final enum START_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;

.field public static final enum STOP_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 22
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;

    const-string v1, "CHECK_CONDITION"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;->CHECK_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;

    const-string v1, "INVOKE_POPUP"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;->INVOKE_POPUP:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;

    const-string v1, "START_TIMER"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;->START_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;

    const-string v1, "STOP_TIMER"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;->STOP_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;

    const-string v1, "NOTIFY_FAILED"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;

    const-string v1, "NOTIFY_SUCCESS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;->NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;

    .line 20
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;->CHECK_CONDITION:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;->INVOKE_POPUP:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;->START_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;->STOP_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;->NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopupStateMachine$Action;

    return-object v0
.end method
