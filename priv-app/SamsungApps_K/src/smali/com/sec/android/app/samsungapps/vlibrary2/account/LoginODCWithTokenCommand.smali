.class public Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field _IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand$ILoginODCWithTokenCommandData;

.field private _ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

.field _LoginExResultMap:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginExResultMap;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand$ILoginODCWithTokenCommandData;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 17
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginExResultMap;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginExResultMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;->_LoginExResultMap:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginExResultMap;

    .line 21
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;->_IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand$ILoginODCWithTokenCommandData;

    .line 22
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;Z)V
    .locals 0

    .prologue
    .line 15
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;Z)V
    .locals 0

    .prologue
    .line 15
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;Z)V
    .locals 0

    .prologue
    .line 15
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;->_IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand$ILoginODCWithTokenCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand$ILoginODCWithTokenCommandData;->createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->startLoading()V

    .line 27
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/o;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/o;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;->request(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    .line 53
    return-void
.end method

.method protected request(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V
    .locals 4

    .prologue
    .line 57
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UPBillingConditionChecker;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;->_Context:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UPBillingConditionChecker;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;Landroid/content/Context;)V

    .line 58
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UPBillingConditionChecker;->isUPBillingCondition()Z

    move-result v0

    .line 59
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/account/p;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/p;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;)V

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;->_LoginExResultMap:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginExResultMap;

    invoke-virtual {v1, v2, v3, v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->loginEx(Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginExParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 61
    const-string v1, "3010"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->addDontDisplayErrorCode(Ljava/lang/String;)V

    .line 62
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;->_IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand$ILoginODCWithTokenCommandData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand$ILoginODCWithTokenCommandData;->isAutoLogin()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 64
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->dontDisplayErrorPopup()V

    .line 66
    :cond_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 67
    return-void
.end method
