.class public final enum Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

.field public static final enum CANCELED:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

.field public static final enum DOWNLOADCOMPLETED:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

.field public static final enum DOWNLOADING:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

.field public static final enum DOWNLOADINGFAILED:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

.field public static final enum GETTINGURL:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

.field public static final enum INSTALLCOMPLETED:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

.field public static final enum INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

.field public static final enum URLGETCOMPLETED:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

.field public static final enum WAITING:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 135
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    const-string v1, "WAITING"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;->WAITING:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    .line 136
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    const-string v1, "DOWNLOADING"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;->DOWNLOADING:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    .line 137
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    const-string v1, "INSTALLING"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;->INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    .line 138
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    const-string v1, "INSTALLCOMPLETED"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;->INSTALLCOMPLETED:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    .line 139
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    const-string v1, "CANCELED"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;->CANCELED:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    const-string v1, "DOWNLOADINGFAILED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;->DOWNLOADINGFAILED:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    const-string v1, "URLGETCOMPLETED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;->URLGETCOMPLETED:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    const-string v1, "GETTINGURL"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;->GETTINGURL:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    const-string v1, "DOWNLOADCOMPLETED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;->DOWNLOADCOMPLETED:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    .line 133
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;->WAITING:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;->DOWNLOADING:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;->INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;->INSTALLCOMPLETED:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;->CANCELED:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;->DOWNLOADINGFAILED:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;->URLGETCOMPLETED:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;->GETTINGURL:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;->DOWNLOADCOMPLETED:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 133
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;
    .locals 1

    .prologue
    .line 133
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;
    .locals 1

    .prologue
    .line 133
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    return-object v0
.end method
