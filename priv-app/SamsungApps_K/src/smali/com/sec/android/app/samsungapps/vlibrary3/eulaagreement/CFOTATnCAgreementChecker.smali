.class public Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/CFOTATnCAgreementChecker;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/FOTATnCAgreementChecker;


# static fields
.field public static final STATE_AGREE:I = 0x1

.field public static final STATE_NONE:I

.field private static a:Ljava/lang/String;

.field private static c:Ljava/lang/String;


# instance fields
.field private b:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const-string v0, "samsung_eula_agree"

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/CFOTATnCAgreementChecker;->a:Ljava/lang/String;

    .line 29
    const-string v0, "FOTA_CLIENT_REGISTRATION"

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/CFOTATnCAgreementChecker;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/CFOTATnCAgreementChecker;->b:Landroid/content/Context;

    .line 16
    return-void
.end method


# virtual methods
.method public eulaAgreed()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 21
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary3/systemsettings/SystemSettingsReader;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/CFOTATnCAgreementChecker;->b:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/systemsettings/SystemSettingsReader;-><init>(Landroid/content/Context;)V

    .line 22
    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/CFOTATnCAgreementChecker;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/systemsettings/SystemSettingsReader;->readInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 26
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public fotaAgreed()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 35
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary3/systemsettings/SystemSettingsReader;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/CFOTATnCAgreementChecker;->b:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/systemsettings/SystemSettingsReader;-><init>(Landroid/content/Context;)V

    .line 36
    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary3/eulaagreement/CFOTATnCAgreementChecker;->c:Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/systemsettings/SystemSettingsReader;->readInt(Ljava/lang/String;I)I

    move-result v2

    .line 37
    if-ne v2, v0, :cond_0

    .line 41
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
