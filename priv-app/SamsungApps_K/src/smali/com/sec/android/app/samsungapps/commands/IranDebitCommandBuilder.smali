.class public Lcom/sec/android/app/samsungapps/commands/IranDebitCommandBuilder;
.super Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/AbsIranPaymentCommandBuilder;
.source "ProGuard"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/AbsIranPaymentCommandBuilder;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V

    .line 24
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/commands/IranDebitCommandBuilder;Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IranPin2Command;)V
    .locals 3

    .prologue
    .line 20
    const v0, 0x7f0400b4

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-direct {v1, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0802a9

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(I)V

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setView(Landroid/view/View;)V

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setCancelable(Z)V

    const v0, 0x7f0802e4

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/sec/android/app/samsungapps/commands/bj;

    invoke-direct {v2, p0, v1, p2}, Lcom/sec/android/app/samsungapps/commands/bj;-><init>(Lcom/sec/android/app/samsungapps/commands/IranDebitCommandBuilder;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IranPin2Command;)V

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    const v0, 0x7f08023d

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/sec/android/app/samsungapps/commands/bk;

    invoke-direct {v2, p0, p2}, Lcom/sec/android/app/samsungapps/commands/bk;-><init>(Lcom/sec/android/app/samsungapps/commands/IranDebitCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/IranPin2Command;)V

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setNegativeButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->show()V

    return-void
.end method


# virtual methods
.method public createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
    .locals 1

    .prologue
    .line 39
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/bh;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/bh;-><init>(Lcom/sec/android/app/samsungapps/commands/IranDebitCommandBuilder;)V

    return-object v0
.end method

.method public getIranCreditCardViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/bg;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/bg;-><init>(Lcom/sec/android/app/samsungapps/commands/IranDebitCommandBuilder;)V

    return-object v0
.end method

.method public getPin2VieInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
    .locals 1

    .prologue
    .line 55
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/bi;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/bi;-><init>(Lcom/sec/android/app/samsungapps/commands/IranDebitCommandBuilder;)V

    return-object v0
.end method
