.class public Lcom/sec/android/app/samsungapps/SearchResultActivity;
.super Lcom/sec/android/app/samsungapps/ContentListActivity;
.source "ProGuard"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/widget/TextView$OnEditorActionListener;


# static fields
.field public static final DEFAULT_STRING_FOR_SEARCH:Ljava/lang/String; = "DEFAULT_STRING_FOR_SEARCH"

.field public static final GESTURE_PAD_QUERY_KEYWORD:Ljava/lang/String; = "query"

.field static e:Ljava/lang/String;


# instance fields
.field private A:Ljava/lang/String;

.field private B:Ljava/lang/String;

.field private C:Ljava/lang/String;

.field private D:Z

.field private final E:Landroid/os/Handler;

.field private F:Landroid/widget/AdapterView$OnItemClickListener;

.field final a:I

.field final b:Ljava/lang/String;

.field c:Lcom/sec/android/app/samsungapps/ik;

.field d:Landroid/database/sqlite/SQLiteDatabase;

.field f:Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

.field private g:Landroid/widget/ListView;

.field private h:Landroid/widget/LinearLayout;

.field private j:Landroid/view/View;

.field private k:Landroid/view/View;

.field private l:Landroid/view/View;

.field private m:Landroid/view/View;

.field protected mAdapter:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

.field protected mContentListQuery:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

.field protected mKeyword:Ljava/lang/String;

.field protected mSearchListQueryObserver:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$ContentListQueryObserver;

.field protected mSearchMode:I

.field private n:Landroid/view/View;

.field private o:Landroid/widget/ScrollView;

.field private p:Z

.field private q:I

.field private r:I

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/Boolean;

.field private v:Landroid/widget/EditText;

.field private w:Landroid/view/inputmethod/InputMethodManager;

.field private x:Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;

.field private final y:I

.field private final z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 181
    const-string v0, "personalSearchResult"

    sput-object v0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 72
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ContentListActivity;-><init>()V

    .line 74
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->a:I

    .line 75
    const-string v0, "show_popup_state"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->b:Ljava/lang/String;

    .line 92
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->p:Z

    .line 98
    iput v2, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->q:I

    .line 99
    iput v2, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->r:I

    .line 104
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mKeyword:Ljava/lang/String;

    .line 109
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->s:Ljava/lang/String;

    .line 114
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->t:Ljava/lang/String;

    .line 117
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->u:Ljava/lang/Boolean;

    .line 119
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->v:Landroid/widget/EditText;

    .line 123
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->w:Landroid/view/inputmethod/InputMethodManager;

    .line 125
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->x:Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;

    .line 130
    iput v3, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->y:I

    .line 131
    iput v3, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->z:I

    .line 133
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->A:Ljava/lang/String;

    .line 135
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->B:Ljava/lang/String;

    .line 136
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->C:Ljava/lang/String;

    .line 179
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->c:Lcom/sec/android/app/samsungapps/ik;

    .line 185
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mSearchMode:I

    .line 186
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->D:Z

    .line 188
    new-instance v0, Lcom/sec/android/app/samsungapps/hw;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/hw;-><init>(Lcom/sec/android/app/samsungapps/SearchResultActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->E:Landroid/os/Handler;

    .line 766
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->f:Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    .line 1189
    new-instance v0, Lcom/sec/android/app/samsungapps/hx;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/hx;-><init>(Lcom/sec/android/app/samsungapps/SearchResultActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->F:Landroid/widget/AdapterView$OnItemClickListener;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/SearchResultActivity;Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;)Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->x:Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;

    return-object p1
.end method

.method private a()V
    .locals 3

    .prologue
    const v2, 0x7f0802ea

    .line 773
    const v0, 0x7f0801dd

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 774
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->f:Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    if-nez v1, :cond_0

    .line 775
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->createInfoDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->f:Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    .line 778
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->f:Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    if-nez v0, :cond_1

    .line 818
    :goto_0
    return-void

    .line 783
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->f:Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/if;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/if;-><init>(Lcom/sec/android/app/samsungapps/SearchResultActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setPositiveButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 800
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->f:Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    const v1, 0x7f08023d

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/ih;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/ih;-><init>(Lcom/sec/android/app/samsungapps/SearchResultActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setNegativeButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 815
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->f:Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->getDialog()Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    move-result-object v0

    const v1, 0x7f0c015c

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 816
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->f:Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    .line 817
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->p:Z

    goto :goto_0
.end method

.method private declared-synchronized a(Landroid/database/Cursor;)V
    .locals 4

    .prologue
    .line 1346
    monitor-enter p0

    const/4 v0, 0x0

    .line 1348
    :try_start_0
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mKeyword:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;-><init>(Ljava/lang/String;)V

    .line 1349
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1351
    if-nez p1, :cond_1

    .line 1386
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1355
    :cond_1
    :goto_1
    :try_start_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1357
    const/16 v2, 0xa

    if-eq v0, v2, :cond_2

    .line 1359
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResult;

    invoke-direct {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResult;-><init>()V

    .line 1363
    const-string v3, "searchString"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResult;->keyword:Ljava/lang/String;

    .line 1364
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResult;->isUserSearchHistory:Ljava/lang/Boolean;

    .line 1365
    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->add(Ljava/lang/Object;)Z

    .line 1368
    add-int/lit8 v0, v0, 0x1

    .line 1372
    goto :goto_1

    .line 1377
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->x:Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;

    if-eqz v0, :cond_3

    .line 1379
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->x:Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->addAll(Ljava/util/Collection;)Z

    .line 1380
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->x:Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;

    .line 1383
    :cond_3
    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1384
    invoke-interface {p1}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1346
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1553
    if-eqz p1, :cond_0

    invoke-static {p2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->isValidString(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1554
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f0800dd

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1556
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/SearchResultActivity;)V
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->c(Z)V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/SearchResultActivity;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->x:Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->x:Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->cancel()V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->x:Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->clear()V

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mSearchMode:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->D:Z

    const-string v0, "false"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->x:Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->x:Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;

    new-instance v1, Lcom/sec/android/app/samsungapps/ij;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/ij;-><init>(Lcom/sec/android/app/samsungapps/SearchResultActivity;)V

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->requestAutoSearch(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/receiverinterface/OnResultReceiver;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 2

    .prologue
    .line 1390
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->g:Landroid/widget/ListView;

    if-eqz v0, :cond_1

    .line 1392
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1394
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->g:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 1395
    new-instance v0, Lcom/sec/android/app/samsungapps/view/AutoCompleteSearchListAdapter;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/android/app/samsungapps/view/AutoCompleteSearchListAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1396
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->g:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1397
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/AutoCompleteSearchListAdapter;->notifyDataSetChanged()V

    .line 1398
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->g:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1399
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->g:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->F:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1414
    :cond_0
    :goto_0
    return-void

    .line 1404
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->f()V

    goto :goto_0
.end method

.method private a(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 881
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->v:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 882
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->v:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 883
    if-nez p1, :cond_1

    .line 884
    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 885
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->v:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 892
    :cond_0
    :goto_0
    return-void

    .line 887
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07003b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 888
    invoke-virtual {v0, v3, v3, v1, v3}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 889
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->v:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private b()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 1130
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->E:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 1131
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->E:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1132
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->E:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1134
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/SearchResultActivity;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->e()V

    return-void
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 895
    const v0, 0x7f0c008f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 897
    if-eqz v1, :cond_0

    .line 898
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 900
    :cond_0
    return-void

    .line 898
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private c()Landroid/database/Cursor;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1302
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->c:Lcom/sec/android/app/samsungapps/ik;

    if-nez v0, :cond_0

    move-object v0, v1

    .line 1330
    :goto_0
    return-object v0

    .line 1309
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->c:Lcom/sec/android/app/samsungapps/ik;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/ik;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->d:Landroid/database/sqlite/SQLiteDatabase;

    .line 1310
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mKeyword:Ljava/lang/String;

    const-string v2, "\'"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1312
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mKeyword:Ljava/lang/String;

    const-string v2, "\'"

    const-string v3, "\'\'"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1327
    :goto_1
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 1329
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->d:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SELECT DISTINCT searchString FROM "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v4, Lcom/sec/android/app/samsungapps/SearchResultActivity;->e:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " WHERE searchString LIKE \'%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "%\' ORDER BY _id DESC"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 1314
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mKeyword:Ljava/lang/String;

    const-string v2, "%"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1316
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mKeyword:Ljava/lang/String;

    const-string v2, "%"

    const-string v3, "\\%"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1318
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mKeyword:Ljava/lang/String;

    const-string v2, "_"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1320
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mKeyword:Ljava/lang/String;

    const-string v2, "_"

    const-string v3, "\\_"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1324
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mKeyword:Ljava/lang/String;

    goto :goto_1
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/SearchResultActivity;)Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->x:Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;

    return-object v0
.end method

.method private c(Z)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 1466
    const v0, 0x7f0c0182

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 1467
    const v0, 0x7f0c008b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1468
    const v1, 0x7f0c008c

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1470
    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mAdapter:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    if-nez v0, :cond_1

    .line 1490
    :cond_0
    :goto_0
    return-void

    .line 1474
    :cond_1
    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mAdapter:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mAdapter:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mAdapter:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->correctedKeyword:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->isValidString(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1476
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1477
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mAdapter:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->correctedKeyword:Ljava/lang/String;

    .line 1478
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "<i>"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "</i>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1479
    new-instance v1, Lcom/sec/android/app/samsungapps/hy;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/app/samsungapps/hy;-><init>(Lcom/sec/android/app/samsungapps/SearchResultActivity;Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 1488
    :cond_2
    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/SearchResultActivity;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->v:Landroid/widget/EditText;

    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1335
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->c:Lcom/sec/android/app/samsungapps/ik;

    if-nez v0, :cond_0

    .line 1342
    :goto_0
    return-void

    .line 1340
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->c:Lcom/sec/android/app/samsungapps/ik;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/ik;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->d:Landroid/database/sqlite/SQLiteDatabase;

    .line 1341
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->c:Lcom/sec/android/app/samsungapps/ik;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->d:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, v1, v2, v2}, Lcom/sec/android/app/samsungapps/ik;->onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V

    goto :goto_0
.end method

.method private declared-synchronized e()V
    .locals 7

    .prologue
    .line 1418
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->x:Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->size()I

    move-result v0

    if-gtz v0, :cond_2

    :cond_0
    move-object v0, v2

    :goto_0
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->x:Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;

    .line 1419
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->c:Lcom/sec/android/app/samsungapps/ik;

    if-eqz v0, :cond_1

    .line 1421
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->c()Landroid/database/Cursor;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->a(Landroid/database/Cursor;)V

    .line 1424
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mKeyword:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->x:Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->a(Ljava/lang/String;Ljava/util/ArrayList;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1426
    monitor-exit p0

    return-void

    .line 1418
    :cond_2
    :try_start_1
    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->size()I

    move-result v0

    const/16 v1, 0xa

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v4

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->getKeyword()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getKnoxAPI()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;->isKnoxMode()Z

    move-result v5

    const/4 v0, 0x0

    move v3, v0

    :goto_1
    if-ge v3, v4, :cond_5

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResult;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResult;->keyword:Ljava/lang/String;

    if-nez v5, :cond_3

    const-string v6, "Samsung KNOX"

    invoke-virtual {v0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    const-string v6, "for KNOX"

    invoke-virtual {v0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_5
    move-object v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic e(Lcom/sec/android/app/samsungapps/SearchResultActivity;)Z
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->D:Z

    return v0
.end method

.method private f()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1618
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->k:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 1620
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->hideLoadingEmptyView()V

    .line 1622
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->g:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 1624
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->g:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 1625
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1628
    :cond_0
    const v0, 0x7f0c0083

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1629
    if-eqz v0, :cond_1

    .line 1631
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->j:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1632
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->o:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 1633
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->o:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setFocusable(Z)V

    .line 1634
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->o:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setFocusableInTouchMode(Z)V

    .line 1637
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->k:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1639
    :cond_2
    return-void
.end method

.method static synthetic f(Lcom/sec/android/app/samsungapps/SearchResultActivity;)V
    .locals 3

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->w:Landroid/view/inputmethod/InputMethodManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->v:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->w:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->v:Landroid/widget/EditText;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    :cond_0
    return-void
.end method

.method static synthetic g(Lcom/sec/android/app/samsungapps/SearchResultActivity;)V
    .locals 3

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->w:Landroid/view/inputmethod/InputMethodManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->v:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->w:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->v:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    :cond_0
    return-void
.end method

.method static synthetic h(Lcom/sec/android/app/samsungapps/SearchResultActivity;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->a()V

    return-void
.end method

.method static synthetic i(Lcom/sec/android/app/samsungapps/SearchResultActivity;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 72
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mSearchMode:I

    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->D:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->s:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->v:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mKeyword:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->clearList()V

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->f()V

    const-string v0, ""

    invoke-virtual {p0, v0, v2, v2, v2}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->onTextChanged(Ljava/lang/CharSequence;III)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->E:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    invoke-direct {p0, v2}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->a(Z)V

    invoke-direct {p0, v2}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->b(Z)V

    invoke-direct {p0, v2}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->c(Z)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->v:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mAdapter:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mAdapter:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->clear()V

    :cond_0
    return-void
.end method

.method static synthetic j(Lcom/sec/android/app/samsungapps/SearchResultActivity;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->d()V

    return-void
.end method

.method static synthetic k(Lcom/sec/android/app/samsungapps/SearchResultActivity;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->f()V

    return-void
.end method

.method static synthetic l(Lcom/sec/android/app/samsungapps/SearchResultActivity;)Z
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->p:Z

    return v0
.end method

.method static synthetic m(Lcom/sec/android/app/samsungapps/SearchResultActivity;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->E:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic n(Lcom/sec/android/app/samsungapps/SearchResultActivity;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->b()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 935
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 940
    return-void
.end method

.method protected clearList()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/16 v3, 0x8

    .line 1063
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->x:Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;

    if-eqz v0, :cond_0

    .line 1064
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->x:Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;

    .line 1067
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->g:Landroid/widget/ListView;

    if-eqz v0, :cond_1

    .line 1068
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->g:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1071
    :cond_1
    const v0, 0x7f0c0097

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1072
    const v1, 0x7f0c0183

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1073
    const v2, 0x7f0c0184

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 1074
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    if-eqz v2, :cond_2

    .line 1076
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1077
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1078
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1080
    :cond_2
    return-void
.end method

.method protected createContentArrayAdapter()Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;
    .locals 2

    .prologue
    .line 1186
    new-instance v0, Lcom/sec/android/app/samsungapps/view/SearchArrayAdapter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mContentListQuery:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/samsungapps/view/SearchArrayAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;)V

    return-object v0
.end method

.method protected createContentListQuery()V
    .locals 2

    .prologue
    .line 439
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->createSearch()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mContentListQuery:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    .line 441
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mContentListQuery:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-eqz v0, :cond_0

    .line 442
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mContentListQuery:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mSearchListQueryObserver:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$ContentListQueryObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->addObserver(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$ContentListQueryObserver;)V

    .line 444
    :cond_0
    return-void
.end method

.method protected createContentListQueryObserver()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$ContentListQueryObserver;
    .locals 1

    .prologue
    .line 393
    new-instance v0, Lcom/sec/android/app/samsungapps/ia;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/ia;-><init>(Lcom/sec/android/app/samsungapps/SearchResultActivity;)V

    return-object v0
.end method

.method protected createHotKeywordListAdapter()Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;
    .locals 2

    .prologue
    .line 1589
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mContentListQuery:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SearchContentQuery;

    .line 1590
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SearchContentQuery;->getSearchContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/SearchContentList;

    move-result-object v0

    .line 1591
    new-instance v1, Lcom/sec/android/app/samsungapps/view/HotKeyWordArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SearchContentList;->getRecommendKeywordList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Lcom/sec/android/app/samsungapps/view/HotKeyWordArrayAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    return-object v1
.end method

.method protected displayHotKeywordList()V
    .locals 6

    .prologue
    const v5, 0x7f080192

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1560
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mContentListQuery:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SearchContentQuery;

    .line 1561
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SearchContentQuery;->getSearchContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/SearchContentList;

    move-result-object v1

    .line 1562
    iput-boolean v3, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->D:Z

    .line 1565
    const v0, 0x7f0c0097

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 1566
    const v0, 0x1020016

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1567
    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    .line 1568
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1569
    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1570
    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1574
    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SearchContentList;->getRecommendKeywordList()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SearchContentList;->getRecommendKeywordList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 1578
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->createHotKeywordListAdapter()Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mAdapter:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    .line 1579
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mAdapter:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->setLogicalView(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;Ljava/lang/Boolean;)V

    .line 1582
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mAdapter:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->setInfLoadingState(Lcom/sec/android/app/samsungapps/view/InfLoadingState;)V

    .line 1584
    :cond_1
    invoke-direct {p0, v4}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->c(Z)V

    .line 1585
    return-void
.end method

.method protected displaySearchResultCount()V
    .locals 11

    .prologue
    const v10, 0x7f0801df

    const v9, 0x7f0800ec

    const/16 v8, 0x8

    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 1493
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->D:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mSearchMode:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mSearchMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 1495
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mContentListQuery:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mAdapter:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    if-nez v0, :cond_2

    .line 1543
    :cond_1
    :goto_0
    return-void

    .line 1499
    :cond_2
    const v0, 0x7f0c0097

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 1500
    const v0, 0x1020016

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1501
    const v1, 0x7f0c0183

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 1502
    const v1, 0x7f0c0184

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1504
    if-eqz v4, :cond_1

    if-eqz v5, :cond_1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 1508
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mContentListQuery:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/SearchContentList;

    .line 1509
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SearchContentList;->getTotalCount()I

    move-result v2

    .line 1511
    :goto_1
    if-lez v2, :cond_5

    iget-object v6, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mAdapter:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v6}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->getCount()I

    move-result v6

    if-lez v6, :cond_5

    .line 1512
    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1513
    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1514
    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1515
    if-ne v2, v7, :cond_4

    .line 1516
    invoke-virtual {p0, v10}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1517
    invoke-virtual {p0, v10}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1522
    :goto_2
    invoke-direct {p0, v7}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->c(Z)V

    goto :goto_0

    :cond_3
    move v2, v3

    .line 1509
    goto :goto_1

    .line 1519
    :cond_4
    invoke-virtual {p0, v9}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v3

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1520
    invoke-virtual {p0, v9}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v3

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    goto :goto_2

    .line 1526
    :cond_5
    invoke-virtual {v5, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1527
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1528
    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setSelected(Z)V

    .line 1529
    const v0, 0x7f0801e1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1530
    invoke-direct {p0, v3}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->c(Z)V

    goto/16 :goto_0
.end method

.method protected empty()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1600
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mContentListQuery:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-nez v1, :cond_1

    .line 1608
    :cond_0
    :goto_0
    return v0

    .line 1604
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mContentListQuery:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v1

    .line 1605
    if-eqz v1, :cond_0

    .line 1606
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->size()I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z
    .locals 2

    .prologue
    .line 1259
    sget-object v0, Lcom/sec/android/app/samsungapps/hz;->b:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1268
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/ContentListActivity;->handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z

    move-result v0

    :goto_0
    return v0

    .line 1261
    :pswitch_0
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getYesOrNoEvent()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getExtraData()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->handleYesOrNoEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 1263
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mAdapter:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    if-eqz v0, :cond_0

    .line 1264
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mAdapter:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->notifyDataSetChanged()V

    .line 1266
    :cond_0
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;

    invoke-static {p1}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->handleAccountEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;)Z

    move-result v0

    goto :goto_0

    .line 1259
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public handleUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V
    .locals 2

    .prologue
    .line 1224
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getEventType()Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;->ContentDisplayEvent:Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    if-ne v0, v1, :cond_0

    .line 1225
    sget-object v0, Lcom/sec/android/app/samsungapps/hz;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getContentDisplayEvent()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->getContentDisplayEventType()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1254
    :cond_0
    :goto_0
    return-void

    .line 1228
    :pswitch_0
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getContentDisplayEvent()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    const/16 v1, 0x8

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;I)V

    goto :goto_0

    .line 1231
    :pswitch_1
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getContentDisplayEvent()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    .line 1233
    if-eqz v0, :cond_0

    .line 1234
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mKeyword:Ljava/lang/String;

    .line 1236
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->v:Landroid/widget/EditText;

    if-eqz v0, :cond_1

    .line 1238
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->v:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mKeyword:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1239
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->v:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mKeyword:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 1242
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mContentListQuery:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-eqz v0, :cond_2

    .line 1245
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mContentListQuery:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;->pop:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->setQueryLogInputMethod(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;)V

    .line 1248
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->b()V

    goto :goto_0

    .line 1225
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected hideLoadingEmptyView()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 1675
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->l:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 1676
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->l:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1677
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->m:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1678
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->m:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1680
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->n:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 1682
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->n:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1684
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->j:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 1685
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->j:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1687
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->o:Landroid/widget/ScrollView;

    if-eqz v0, :cond_3

    .line 1688
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->o:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 1691
    :cond_3
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 477
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ContentListActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 478
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->p:Z

    if-eqz v0, :cond_0

    .line 479
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->f:Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->hide()V

    .line 480
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->f:Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    .line 481
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->a()V

    .line 483
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 272
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v0

    if-ne v0, v6, :cond_0

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CSC;->isVZW()Z

    move-result v0

    if-nez v0, :cond_0

    .line 275
    const-string v0, "SearchResultActivity::onCreate::Not Supported"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    .line 276
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->finish()V

    .line 348
    :goto_0
    return-void

    .line 280
    :cond_0
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getKnoxAPI()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;->isKnoxMode()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 282
    const-string v0, "personalSearchResultForKnox"

    sput-object v0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->e:Ljava/lang/String;

    .line 289
    :goto_1
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;-><init>(Landroid/content/Context;)V

    .line 290
    const-string v1, "user_keywords_search_setting"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getSharedConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->B:Ljava/lang/String;

    .line 291
    const-string v1, "auto_complete_search_setting"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getSharedConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->C:Ljava/lang/String;

    .line 293
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->B:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->B:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 295
    :cond_1
    const-string v1, "true"

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->B:Ljava/lang/String;

    .line 296
    const-string v1, "user_keywords_search_setting"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->B:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setSharedConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 298
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->C:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->C:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 300
    :cond_3
    const-string v1, "true"

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->C:Ljava/lang/String;

    .line 301
    const-string v1, "auto_complete_search_setting"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->C:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setSharedConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 306
    :cond_4
    :try_start_0
    const-string v0, "true"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->B:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 308
    new-instance v0, Lcom/sec/android/app/samsungapps/ik;

    sget-object v1, Lcom/sec/android/app/samsungapps/SearchResultActivity;->e:Ljava/lang/String;

    invoke-direct {v0, p0, p0, v1}, Lcom/sec/android/app/samsungapps/ik;-><init>(Lcom/sec/android/app/samsungapps/SearchResultActivity;Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->c:Lcom/sec/android/app/samsungapps/ik;

    .line 318
    :goto_2
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ContentListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 320
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 321
    const-string v1, "DEFAULT_STRING_FOR_SEARCH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->A:Ljava/lang/String;

    .line 323
    const-string v1, "query"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mKeyword:Ljava/lang/String;

    .line 325
    if-eqz p1, :cond_5

    .line 326
    const-string v0, "show_popup_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->p:Z

    .line 328
    :cond_5
    const v0, 0x7f04005b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->setMainView(I)V

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v0, 0x1

    new-array v4, v0, [I

    const/4 v0, 0x0

    const v5, 0xa000b

    aput v5, v4, v0

    const v5, 0xb0001

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    const v0, 0x7f0c007e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->j:Landroid/view/View;

    const v0, 0x7f0c02a1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->o:Landroid/widget/ScrollView;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->o:Landroid/widget/ScrollView;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->o:Landroid/widget/ScrollView;

    new-instance v1, Lcom/sec/android/app/samsungapps/ib;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/ib;-><init>(Lcom/sec/android/app/samsungapps/SearchResultActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    :cond_6
    const v0, 0x7f0c0169

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->h:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->h:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->h:Landroid/widget/LinearLayout;

    const v1, 0x7f0c0096

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f080216

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    new-instance v1, Lcom/sec/android/app/samsungapps/ic;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/ic;-><init>(Lcom/sec/android/app/samsungapps/SearchResultActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->p:Z

    if-ne v0, v6, :cond_7

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->a()V

    :cond_7
    const v0, 0x7f0c0080

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->k:Landroid/view/View;

    const v0, 0x7f0c007f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->l:Landroid/view/View;

    const v0, 0x7f0c02a2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->m:Landroid/view/View;

    const v0, 0x7f0c02a3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->n:Landroid/view/View;

    const v0, 0x7f0c0078

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->g:Landroid/widget/ListView;

    const v0, 0x7f0c008e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->v:Landroid/widget/EditText;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08011f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_3
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->v:Landroid/widget/EditText;

    new-instance v2, Landroid/text/SpannableStringBuilder;

    const-string v3, "   "

    invoke-direct {v2, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0201b0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->v:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getTextSize()F

    move-result v3

    float-to-double v3, v3

    const-wide/high16 v5, 0x3ff4000000000000L    # 1.25

    mul-double/2addr v3, v5

    double-to-int v3, v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5, v3, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    new-instance v3, Landroid/text/style/ImageSpan;

    invoke-direct {v3, v0}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;)V

    const/4 v0, 0x1

    const/4 v4, 0x2

    const/16 v5, 0x21

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->v:Landroid/widget/EditText;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f0802f1

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f0800ca

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->v:Landroid/widget/EditText;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mKeyword:Ljava/lang/String;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mKeyword:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_10

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->a(Z)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->b(Z)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->v:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mKeyword:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_8
    :goto_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->v:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->v:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->v:Landroid/widget/EditText;

    iget v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->q:I

    iget v2, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->r:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/EditText;->setSelection(II)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_5
    :try_start_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->v:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/samsungapps/id;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/id;-><init>(Lcom/sec/android/app/samsungapps/SearchResultActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_9
    const v0, 0x7f0c008f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f0801b4

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/sec/android/app/samsungapps/ie;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/ie;-><init>(Lcom/sec/android/app/samsungapps/SearchResultActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    const v1, 0x7f0c0078

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->setNextFocusItems(I)V

    .line 330
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mKeyword:Ljava/lang/String;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mKeyword:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_a

    .line 332
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->E:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 335
    :cond_a
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->createContentListQueryObserver()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$ContentListQueryObserver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mSearchListQueryObserver:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$ContentListQueryObserver;

    .line 336
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->createContentListQuery()V

    .line 337
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mKeyword:Ljava/lang/String;

    if-eqz v0, :cond_b

    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mKeyword:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 339
    :cond_b
    const-string v0, ""

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 342
    :cond_c
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->w:Landroid/view/inputmethod/InputMethodManager;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 347
    :goto_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->SEARCH_RESULT:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->setCurrentPage(Lcom/sec/android/app/samsungapps/tobelog/LogPage;)V

    goto/16 :goto_0

    .line 286
    :cond_d
    const-string v0, "personalSearchResult"

    sput-object v0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->e:Ljava/lang/String;

    goto/16 :goto_1

    .line 312
    :cond_e
    :try_start_3
    new-instance v0, Lcom/sec/android/app/samsungapps/ik;

    sget-object v1, Lcom/sec/android/app/samsungapps/SearchResultActivity;->e:Ljava/lang/String;

    invoke-direct {v0, p0, p0, v1}, Lcom/sec/android/app/samsungapps/ik;-><init>(Lcom/sec/android/app/samsungapps/SearchResultActivity;Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->c:Lcom/sec/android/app/samsungapps/ik;

    .line 313
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->d()V

    .line 314
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->c:Lcom/sec/android/app/samsungapps/ik;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/ik;->close()V

    .line 315
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->c:Lcom/sec/android/app/samsungapps/ik;

    goto/16 :goto_2

    :catch_0
    move-exception v0

    goto :goto_6

    .line 328
    :cond_f
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0802f1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    :cond_10
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->A:Ljava/lang/String;

    if-nez v0, :cond_11

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->a(Z)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->b(Z)V

    goto/16 :goto_4

    :cond_11
    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->a(Z)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->b(Z)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->A:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mKeyword:Ljava/lang/String;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->u:Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->v:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mKeyword:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mKeyword:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->q:I

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mKeyword:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->r:I

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->E:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->A:Ljava/lang/String;

    goto/16 :goto_4

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SearchResultActivity::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_5
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 455
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->removeContentListQuery()V

    .line 457
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->j:Landroid/view/View;

    .line 458
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->k:Landroid/view/View;

    .line 459
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->l:Landroid/view/View;

    .line 460
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->m:Landroid/view/View;

    .line 461
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->n:Landroid/view/View;

    .line 462
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->c:Lcom/sec/android/app/samsungapps/ik;

    if-eqz v0, :cond_0

    .line 463
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->c:Lcom/sec/android/app/samsungapps/ik;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/ik;->close()V

    .line 464
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->c:Lcom/sec/android/app/samsungapps/ik;

    .line 466
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mAdapter:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    if-eqz v0, :cond_1

    .line 467
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mAdapter:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->setAdapterObserver(Lcom/sec/android/app/samsungapps/widget/interfaces/IAdapterObserver;)V

    .line 468
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mAdapter:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->release()V

    .line 471
    :cond_1
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ContentListActivity;->onDestroy()V

    .line 472
    return-void
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 822
    if-eqz p3, :cond_4

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x42

    if-ne v1, v2, :cond_4

    .line 824
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 825
    if-eqz v1, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-ne v2, v0, :cond_1

    .line 827
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->E:Landroid/os/Handler;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 845
    :goto_0
    return v0

    .line 831
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mContentListQuery:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-eqz v2, :cond_2

    .line 834
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mContentListQuery:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;->iqry:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->setQueryLogInputMethod(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;)V

    .line 837
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v2, :cond_3

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-ne v2, v0, :cond_3

    .line 838
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getPreviousPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_SEARCH_WITH_KEYWORD:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-static {v2, v3, v1}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendSearchLog(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;Ljava/lang/String;)V

    .line 841
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->b()V

    goto :goto_0

    .line 845
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 362
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getKnoxAPI()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;->isKnoxMode()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 364
    const-string v0, "personalSearchResultForKnox"

    sput-object v0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->e:Ljava/lang/String;

    .line 371
    :goto_0
    const-string v0, "true"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->B:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 373
    new-instance v0, Lcom/sec/android/app/samsungapps/ik;

    sget-object v1, Lcom/sec/android/app/samsungapps/SearchResultActivity;->e:Ljava/lang/String;

    invoke-direct {v0, p0, p0, v1}, Lcom/sec/android/app/samsungapps/ik;-><init>(Lcom/sec/android/app/samsungapps/SearchResultActivity;Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->c:Lcom/sec/android/app/samsungapps/ik;

    .line 383
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mAdapter:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    if-eqz v0, :cond_0

    .line 385
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mAdapter:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->notifyDataSetChanged()V

    .line 387
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mKeyword:Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mKeyword:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const-string v0, ""

    invoke-virtual {p0, v0, v2, v2, v2}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 388
    :cond_2
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ContentListActivity;->onResume()V

    .line 389
    return-void

    .line 368
    :cond_3
    const-string v0, "personalSearchResult"

    sput-object v0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->e:Ljava/lang/String;

    goto :goto_0

    .line 377
    :cond_4
    new-instance v0, Lcom/sec/android/app/samsungapps/ik;

    sget-object v1, Lcom/sec/android/app/samsungapps/SearchResultActivity;->e:Ljava/lang/String;

    invoke-direct {v0, p0, p0, v1}, Lcom/sec/android/app/samsungapps/ik;-><init>(Lcom/sec/android/app/samsungapps/SearchResultActivity;Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->c:Lcom/sec/android/app/samsungapps/ik;

    .line 378
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->d()V

    .line 379
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->c:Lcom/sec/android/app/samsungapps/ik;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/ik;->close()V

    .line 380
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->c:Lcom/sec/android/app/samsungapps/ik;

    goto :goto_1
.end method

.method public onSamsungAppsCreateOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;)Z
    .locals 1

    .prologue
    .line 259
    const/4 v0, 0x0

    return v0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1705
    const-string v0, "show_popup_state"

    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->p:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1706
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ContentListActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1707
    return-void
.end method

.method protected onSelectionChanged(II)V
    .locals 0

    .prologue
    .line 930
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 989
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->a(Z)V

    .line 990
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->b(Z)V

    .line 991
    invoke-direct {p0, v2}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->c(Z)V

    .line 993
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mAdapter:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    if-eqz v0, :cond_0

    .line 995
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mAdapter:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->clear()V

    .line 996
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mAdapter:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->notifyDataSetChanged()V

    .line 1003
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mKeyword:Ljava/lang/String;

    .line 1004
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mKeyword:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_7

    .line 1006
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->s:Ljava/lang/String;

    .line 1007
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mSearchMode:I

    .line 1008
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->clearList()V

    .line 1009
    const-string v0, "true"

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->B:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1011
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->c()Landroid/database/Cursor;

    move-result-object v0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    if-eqz v0, :cond_4

    :goto_2
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_3

    new-instance v4, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResult;

    invoke-direct {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResult;-><init>()V

    const-string v5, "searchString"

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResult;->keyword:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResult;->isUserSearchHistory:Ljava/lang/Boolean;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_1
    move v0, v2

    .line 989
    goto :goto_0

    :cond_2
    move v0, v2

    .line 990
    goto :goto_1

    .line 1011
    :cond_3
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->f()V

    :goto_3
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1057
    :cond_4
    :goto_4
    return-void

    .line 1011
    :cond_5
    const/4 v1, 0x0

    invoke-direct {p0, v1, v3}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_3

    .line 1015
    :cond_6
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->f()V

    goto :goto_4

    .line 1024
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mKeyword:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->s:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eq v0, v1, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mKeyword:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->t:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eq v0, v1, :cond_4

    .line 1032
    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 1033
    invoke-static {v0}, Ljava/lang/Character$UnicodeBlock;->of(C)Ljava/lang/Character$UnicodeBlock;

    move-result-object v0

    .line 1034
    sget-object v3, Ljava/lang/Character$UnicodeBlock;->HANGUL_SYLLABLES:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {v3, v0}, Ljava/lang/Character$UnicodeBlock;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eq v3, v1, :cond_8

    sget-object v3, Ljava/lang/Character$UnicodeBlock;->HANGUL_COMPATIBILITY_JAMO:Ljava/lang/Character$UnicodeBlock;

    invoke-virtual {v3, v0}, Ljava/lang/Character$UnicodeBlock;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1037
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->clearList()V

    .line 1040
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->f()V

    .line 1042
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->A:Ljava/lang/String;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->A:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-ne v0, v1, :cond_a

    .line 1044
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->E:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_4

    .line 1048
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mContentListQuery:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-eqz v0, :cond_b

    .line 1051
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mContentListQuery:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;->tag:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->setQueryLogInputMethod(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;)V

    .line 1054
    :cond_b
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->b()V

    goto :goto_4
.end method

.method protected removeContentListQuery()V
    .locals 2

    .prologue
    .line 448
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mContentListQuery:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-eqz v0, :cond_0

    .line 449
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mContentListQuery:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mSearchListQueryObserver:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$ContentListQueryObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->removeObserver(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$ContentListQueryObserver;)Z

    .line 451
    :cond_0
    return-void
.end method

.method protected search(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1138
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->c:Lcom/sec/android/app/samsungapps/ik;

    if-eqz v0, :cond_0

    .line 1140
    const-string v0, "\'"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1143
    const-string v0, "\'"

    const-string v1, "\'\'"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1158
    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 1160
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->c:Lcom/sec/android/app/samsungapps/ik;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/ik;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "INSERT INTO "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/sec/android/app/samsungapps/SearchResultActivity;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " VALUES(null, \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\');"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1163
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->E:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1164
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->clearList()V

    .line 1166
    iput v4, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mSearchMode:I

    .line 1167
    iput-boolean v4, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->D:Z

    .line 1169
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mContentListQuery:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-eqz v0, :cond_2

    .line 1170
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mContentListQuery:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->clear()V

    .line 1171
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->u:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1173
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mContentListQuery:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;->tag:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->setQueryLogInputMethod(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryLogInputMethod;)V

    .line 1175
    :cond_1
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->u:Ljava/lang/Boolean;

    .line 1176
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mContentListQuery:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->setKeyword(Ljava/lang/String;)V

    .line 1177
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->createContentArrayAdapter()Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mAdapter:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    .line 1178
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mAdapter:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->showInstalledMark()V

    .line 1179
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mAdapter:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->setLogicalView(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V

    .line 1180
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->mAdapter:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->setInfLoadingState(Lcom/sec/android/app/samsungapps/view/InfLoadingState;)V

    .line 1182
    :cond_2
    return-void

    .line 1145
    :cond_3
    const-string v0, "%"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1147
    const-string v0, "%"

    const-string v1, "\\%"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1149
    :cond_4
    const-string v0, "_"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1151
    const-string v0, "_"

    const-string v1, "\\_"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_5
    move-object v0, p1

    .line 1155
    goto/16 :goto_0
.end method

.method protected setLoadingView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1647
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->l:Landroid/view/View;

    if-eqz v0, :cond_4

    .line 1649
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->k:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->k:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1650
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->empty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1651
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->j:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 1652
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->j:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1654
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->o:Landroid/widget/ScrollView;

    if-eqz v0, :cond_2

    .line 1655
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->o:Landroid/widget/ScrollView;

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 1657
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->l:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1658
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->m:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 1659
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->m:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1661
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->n:Landroid/view/View;

    if-eqz v0, :cond_4

    .line 1663
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->n:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1667
    :cond_4
    return-void
.end method

.method public setVisibleNodata(I)Z
    .locals 5

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 851
    const/4 v3, 0x1

    .line 853
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ContentListActivity;->setVisibleNodata(I)Z

    .line 855
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->g:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 857
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->g:Landroid/widget/ListView;

    if-nez p1, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Landroid/widget/ListView;->setVisibility(I)V

    .line 858
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->h:Landroid/widget/LinearLayout;

    if-nez p1, :cond_3

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 861
    :cond_0
    if-nez p1, :cond_4

    .line 864
    const v0, 0x7f0c0083

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SearchResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 865
    if-eqz v0, :cond_1

    .line 867
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->j:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 868
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->o:Landroid/widget/ScrollView;

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 869
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->o:Landroid/widget/ScrollView;

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->setFocusable(Z)V

    .line 870
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->o:Landroid/widget/ScrollView;

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->setFocusableInTouchMode(Z)V

    move v2, v3

    .line 877
    :cond_1
    :goto_2
    return v2

    :cond_2
    move v0, v2

    .line 857
    goto :goto_0

    :cond_3
    move v1, v2

    .line 858
    goto :goto_1

    :cond_4
    move v2, v3

    goto :goto_2
.end method

.method protected updateHotKeywordList()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 1280
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->E:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 1281
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->E:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1282
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->E:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1284
    :cond_0
    return-void
.end method

.method protected updateSearchResultCount()V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 1273
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->E:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 1274
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->E:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1275
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SearchResultActivity;->E:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1277
    :cond_0
    return-void
.end method
