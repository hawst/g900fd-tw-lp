.class public Lcom/sec/android/app/samsungapps/widget/SectionListItemView;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 14
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/SectionListItemView;-><init>(Ljava/lang/String;Z)V

    .line 15
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/SectionListItemView;->a:Ljava/lang/String;

    .line 19
    iput-boolean p2, p0, Lcom/sec/android/app/samsungapps/widget/SectionListItemView;->b:Z

    .line 20
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/sec/android/app/samsungapps/widget/SectionListItemView;)I
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListItemView;->a:Ljava/lang/String;

    iget-object v1, p1, Lcom/sec/android/app/samsungapps/widget/SectionListItemView;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    .line 37
    if-gez v0, :cond_0

    .line 38
    const/4 v0, -0x1

    .line 42
    :goto_0
    return v0

    .line 39
    :cond_0
    if-lez v0, :cond_1

    .line 40
    const/4 v0, 0x1

    goto :goto_0

    .line 42
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 9
    check-cast p1, Lcom/sec/android/app/samsungapps/widget/SectionListItemView;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/widget/SectionListItemView;->compareTo(Lcom/sec/android/app/samsungapps/widget/SectionListItemView;)I

    move-result v0

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListItemView;->a:Ljava/lang/String;

    return-object v0
.end method

.method public hasItem(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListItemView;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isHeader()Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListItemView;->b:Z

    return v0
.end method
