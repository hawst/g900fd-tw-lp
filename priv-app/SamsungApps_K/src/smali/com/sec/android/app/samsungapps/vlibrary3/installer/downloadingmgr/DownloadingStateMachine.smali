.class public Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/DownloadingStateMachine;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;
.source "ProGuard"


# static fields
.field private static a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/DownloadingStateMachine;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;-><init>()V

    .line 33
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/DownloadingStateMachine;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/DownloadingStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/DownloadingStateMachine;

    if-nez v0, :cond_0

    .line 17
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/DownloadingStateMachine;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/DownloadingStateMachine;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/DownloadingStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/DownloadingStateMachine;

    .line 19
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/DownloadingStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/DownloadingStateMachine;

    return-object v0
.end method


# virtual methods
.method protected entry(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 3

    .prologue
    .line 91
    const-string v0, "DownloadingStateMachine"

    const-string v1, "entry"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/DownloadingStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 92
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/a;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 119
    :goto_0
    return-void

    .line 94
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;->CLEAR_RETRYCOUNT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 95
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;->REQUEST_DOWNLOAD:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 98
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;->REQUEST_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 101
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;->NOTIFY_CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 104
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;->NOTIFY_DOWNLOADDONE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 107
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;->INCREASE_RETRYCOUNT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 108
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;->START_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 111
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;->STOP_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 112
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 115
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;->STOP_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 116
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;->REQUEST_URL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 92
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_6
        :pswitch_2
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method

.method public execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;)Z
    .locals 3

    .prologue
    .line 25
    const-string v0, "DownloadingStateMachine"

    const-string v1, "execute"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/DownloadingStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 27
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/a;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 86
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 29
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/a;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 32
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;->DOWNLOAD:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/DownloadingStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 37
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/a;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 40
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;->CANCELING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/DownloadingStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 43
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;->DOWNLOAD_DONE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/DownloadingStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 46
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/DownloadingStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 51
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/a;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    :pswitch_7
    goto :goto_0

    .line 57
    :pswitch_8
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;->CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/DownloadingStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 54
    :pswitch_9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;->CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/DownloadingStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 62
    :pswitch_a
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/a;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_4

    goto :goto_0

    .line 65
    :pswitch_b
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;->REQUEST_URL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/DownloadingStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 68
    :pswitch_c
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/DownloadingStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 72
    :pswitch_d
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/a;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 81
    :sswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/DownloadingStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 75
    :sswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;->DOWNLOAD:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/DownloadingStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 78
    :sswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/DownloadingStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 27
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_6
        :pswitch_a
        :pswitch_d
    .end packed-switch

    .line 29
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch

    .line 37
    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 51
    :pswitch_data_3
    .packed-switch 0x3
        :pswitch_8
        :pswitch_7
        :pswitch_9
    .end packed-switch

    .line 62
    :pswitch_data_4
    .packed-switch 0x6
        :pswitch_b
        :pswitch_c
    .end packed-switch

    .line 72
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x8 -> :sswitch_1
        0x9 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 10
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/DownloadingStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Event;)Z

    move-result v0

    return v0
.end method

.method protected exit(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 3

    .prologue
    .line 124
    const-string v0, "DownloadingStateMachine"

    const-string v1, "exit"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/DownloadingStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 125
    return-void
.end method
