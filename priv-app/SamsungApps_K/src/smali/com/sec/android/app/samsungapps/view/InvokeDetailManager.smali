.class public Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

.field b:Z

.field private c:Landroid/content/Context;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field private e:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->b:Z

    .line 31
    if-eqz p1, :cond_0

    .line 33
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->c:Landroid/content/Context;

    .line 34
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    .line 35
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/realname/CRealNameAgeCheckerFactory;->createRealNameAgeConfirm(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->e:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;

    .line 38
    :cond_0
    if-eqz p2, :cond_1

    .line 40
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 42
    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;)Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->e:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->c:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public requestAdultCheck()V
    .locals 3

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->_isLogedIn()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 82
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->isNameAgeAuthorized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    :try_start_0
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->c:Landroid/content/Context;

    const v2, 0x7f080142

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setCancelable(Z)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->c:Landroid/content/Context;

    const v2, 0x7f080144

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->c:Landroid/content/Context;

    const v2, 0x7f080145

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/view/x;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/view/x;-><init>(Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->c:Landroid/content/Context;

    const v2, 0x7f08023d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/view/z;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/view/z;-><init>(Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setNegativeButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 99
    :goto_0
    return-void

    .line 84
    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "InvokeDetailManager::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->isAdult()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    const/16 v2, 0x25

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;I)V

    goto :goto_0

    .line 92
    :cond_1
    :try_start_1
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->c:Landroid/content/Context;

    const v2, 0x7f0801ba

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setCancelable(Z)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->c:Landroid/content/Context;

    const v2, 0x7f080149

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->c:Landroid/content/Context;

    const v2, 0x7f0802ef

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/view/ad;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/view/ad;-><init>(Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->show()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "InvokeDetailManager::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 97
    :cond_2
    :try_start_2
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->c:Landroid/content/Context;

    const v2, 0x7f0802b5

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setCancelable(Z)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->c:Landroid/content/Context;

    const v2, 0x7f08014a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->c:Landroid/content/Context;

    const v2, 0x7f08023d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/view/aa;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/view/aa;-><init>(Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setNegativeButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->c:Landroid/content/Context;

    const v2, 0x7f0802b5

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/view/ab;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/view/ab;-><init>(Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->show()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    :catch_2
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "InvokeDetailManager::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public requestInvokeDetail()V
    .locals 3

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->c:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    if-nez v0, :cond_1

    .line 49
    :cond_0
    const-string v0, "InvokeDetailManager::aContext or aContent or document is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 62
    :goto_0
    return-void

    .line 54
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isKorea()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getRestrictedAge()I

    move-result v0

    const/16 v1, 0x12

    if-lt v0, v1, :cond_2

    .line 56
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->requestAdultCheck()V

    goto :goto_0

    .line 60
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/InvokeDetailManager;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    const/16 v2, 0x25

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;I)V

    goto :goto_0
.end method
