.class public Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;
.super Landroid/widget/Gallery;
.source "ProGuard"


# instance fields
.field a:Landroid/view/MotionEvent;

.field b:I

.field c:Lcom/sec/android/app/samsungapps/widget/banner/a;

.field private d:F

.field private e:F

.field private f:F

.field private g:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    .line 15
    invoke-direct {p0, p1}, Landroid/widget/Gallery;-><init>(Landroid/content/Context;)V

    .line 34
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->d:F

    .line 35
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->e:F

    .line 36
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->f:F

    .line 37
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->g:F

    .line 38
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->a:Landroid/view/MotionEvent;

    .line 39
    const/16 v0, 0xff

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->b:I

    .line 145
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->c:Lcom/sec/android/app/samsungapps/widget/banner/a;

    .line 16
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    .line 19
    invoke-direct {p0, p1, p2}, Landroid/widget/Gallery;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->d:F

    .line 35
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->e:F

    .line 36
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->f:F

    .line 37
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->g:F

    .line 38
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->a:Landroid/view/MotionEvent;

    .line 39
    const/16 v0, 0xff

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->b:I

    .line 145
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->c:Lcom/sec/android/app/samsungapps/widget/banner/a;

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    .line 23
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Gallery;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->d:F

    .line 35
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->e:F

    .line 36
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->f:F

    .line 37
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->g:F

    .line 38
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->a:Landroid/view/MotionEvent;

    .line 39
    const/16 v0, 0xff

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->b:I

    .line 145
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->c:Lcom/sec/android/app/samsungapps/widget/banner/a;

    .line 24
    return-void
.end method


# virtual methods
.method public moveLeft()V
    .locals 4

    .prologue
    .line 31
    const/16 v0, 0x15

    new-instance v1, Landroid/view/KeyEvent;

    const/4 v2, 0x3

    const/16 v3, 0x14

    invoke-direct {v1, v2, v3}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->onKeyDown(ILandroid/view/KeyEvent;)Z

    .line 32
    return-void
.end method

.method public moveRight()V
    .locals 4

    .prologue
    .line 27
    const/16 v0, 0x16

    new-instance v1, Landroid/view/KeyEvent;

    const/4 v2, 0x3

    const/16 v3, 0x14

    invoke-direct {v1, v2, v3}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->onKeyDown(ILandroid/view/KeyEvent;)Z

    .line 28
    return-void
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 2

    .prologue
    .line 131
    const/4 v0, 0x0

    cmpl-float v0, p3, v0

    if-lez v0, :cond_0

    .line 133
    const/16 v0, 0x15

    .line 140
    :goto_0
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->onKeyDown(ILandroid/view/KeyEvent;)Z

    .line 142
    const/4 v0, 0x1

    return v0

    .line 137
    :cond_0
    const/16 v0, 0x16

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v5, 0x0

    const v4, 0x7f7fffff    # Float.MAX_VALUE

    .line 43
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;

    .line 45
    const/16 v0, 0xff

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 46
    const/4 v0, 0x0

    .line 100
    :goto_0
    return v0

    .line 48
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->b:I

    .line 50
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 100
    :cond_1
    :goto_1
    :pswitch_0
    invoke-super {p0, p1}, Landroid/widget/Gallery;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 52
    :pswitch_1
    iput v5, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->d:F

    .line 53
    iput v5, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->e:F

    .line 54
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->f:F

    .line 55
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->g:F

    .line 57
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getMetaState()I

    move-result v7

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->a:Landroid/view/MotionEvent;

    .line 59
    if-eqz v8, :cond_1

    .line 60
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->getSelectedItemPosition()I

    move-result v0

    invoke-virtual {v8, v0}, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->calcPosition(I)I

    move-result v0

    invoke-virtual {v8, v0}, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->getReadView(I)Landroid/view/View;

    move-result-object v0

    .line 63
    if-eqz v0, :cond_1

    .line 64
    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->f:F

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v2

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    add-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->f:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_3

    :cond_2
    move v0, v9

    .line 66
    goto :goto_0

    .line 68
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->c:Lcom/sec/android/app/samsungapps/widget/banner/a;

    if-eqz v0, :cond_1

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->c:Lcom/sec/android/app/samsungapps/widget/banner/a;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/banner/a;->a()V

    goto :goto_1

    .line 77
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 78
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 80
    iget v2, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->d:F

    iget v3, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->f:F

    sub-float v3, v0, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    add-float/2addr v2, v3

    iput v2, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->d:F

    .line 81
    iget v2, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->e:F

    iget v3, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->g:F

    sub-float v3, v1, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    add-float/2addr v2, v3

    iput v2, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->e:F

    .line 82
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->f:F

    .line 83
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->g:F

    .line 85
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->d:F

    cmpl-float v0, v0, v5

    if-lez v0, :cond_1

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->a:Landroid/view/MotionEvent;

    if-eqz v0, :cond_4

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->a:Landroid/view/MotionEvent;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 88
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->a:Landroid/view/MotionEvent;

    .line 91
    :cond_4
    iput v4, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->d:F

    .line 92
    iput v4, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->e:F

    .line 93
    iput v4, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->f:F

    .line 94
    iput v4, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->g:F

    move v0, v9

    .line 96
    goto/16 :goto_0

    .line 50
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 105
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 123
    :cond_0
    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Landroid/widget/Gallery;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 107
    :pswitch_1
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->d:F

    .line 108
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->e:F

    .line 109
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->f:F

    .line 110
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->g:F

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->c:Lcom/sec/android/app/samsungapps/widget/banner/a;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->c:Lcom/sec/android/app/samsungapps/widget/banner/a;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/banner/a;->a()V

    goto :goto_0

    .line 118
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->c:Lcom/sec/android/app/samsungapps/widget/banner/a;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->c:Lcom/sec/android/app/samsungapps/widget/banner/a;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/banner/a;->b()V

    goto :goto_0

    .line 105
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public setOnTouchObserver(Lcom/sec/android/app/samsungapps/widget/banner/a;)V
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerGallery;->c:Lcom/sec/android/app/samsungapps/widget/banner/a;

    .line 149
    return-void
.end method
