.class public Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequest;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field mOnSystemRequestResult:Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;

.field mRequestType:Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequest$SystemRequestType;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequest$SystemRequestType;Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequest;->mRequestType:Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequest$SystemRequestType;

    .line 13
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequest;->mOnSystemRequestResult:Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;

    .line 14
    return-void
.end method


# virtual methods
.method public failed()V
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequest;->mOnSystemRequestResult:Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;

    if-eqz v0, :cond_0

    .line 31
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequest;->mOnSystemRequestResult:Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;->onSystemRequestResult(Z)V

    .line 33
    :cond_0
    return-void
.end method

.method public getRequestType()Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequest$SystemRequestType;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequest;->mRequestType:Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequest$SystemRequestType;

    return-object v0
.end method

.method public processed()V
    .locals 1

    .prologue
    .line 37
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestManager;->requestProcessed(Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequest;)V

    .line 38
    return-void
.end method

.method public success()V
    .locals 2

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequest;->mOnSystemRequestResult:Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;

    if-eqz v0, :cond_0

    .line 24
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequest;->mOnSystemRequestResult:Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/systemService/OnSystemRequestResult;->onSystemRequestResult(Z)V

    .line 26
    :cond_0
    return-void
.end method
