.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmResult;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# instance fields
.field public contentsSize:J

.field public downloadUri:Ljava/lang/String;

.field private mResponseMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

.field private mUrlResultContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

.field public orderID:Ljava/lang/String;

.field public paymentStatusCD:Ljava/lang/String;

.field public successYn:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmResult;->contentsSize:J

    .line 14
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmResult;->orderID:Ljava/lang/String;

    .line 15
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmResult;->downloadUri:Ljava/lang/String;

    .line 16
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmResult;->paymentStatusCD:Ljava/lang/String;

    .line 17
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmResult;->successYn:Z

    .line 19
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmResult;->mResponseMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 20
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmResult;->mUrlResultContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    .line 24
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmResult;->mUrlResultContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    .line 25
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmResult;->mResponseMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 26
    return-void
.end method


# virtual methods
.method public addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmResult;->mUrlResultContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmResult;->mUrlResultContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    invoke-interface {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmResult;->mResponseMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 64
    return-void
.end method

.method public clearContainer()V
    .locals 0

    .prologue
    .line 38
    return-void
.end method

.method public closeMap()V
    .locals 3

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmResult;->mResponseMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    const-class v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmResult;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->mappingClass(Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 50
    return-void
.end method

.method public findString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmResult;->mResponseMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getContentSize()J
    .locals 2

    .prologue
    .line 84
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmResult;->contentsSize:J

    return-wide v0
.end method

.method public getDownloadUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmResult;->downloadUri:Ljava/lang/String;

    return-object v0
.end method

.method public getOrderID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmResult;->orderID:Ljava/lang/String;

    return-object v0
.end method

.method public getResponseMap()Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmResult;->mResponseMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    return-object v0
.end method

.method public isSuccess()Z
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmResult;->successYn:Z

    return v0
.end method

.method public openMap()V
    .locals 0

    .prologue
    .line 32
    return-void
.end method

.method public setOrderID(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmResult;->orderID:Ljava/lang/String;

    .line 100
    return-void
.end method

.method public setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 111
    return-void
.end method

.method public setResponseMap(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmResult;->mResponseMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 105
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x1

    return v0
.end method
