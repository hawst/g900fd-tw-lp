.class public Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;
.super Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;
.source "ProGuard"


# instance fields
.field b:Ljava/lang/String;

.field c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;)V
    .locals 4

    .prologue
    const v3, 0x7f07003a

    .line 36
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;)V

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->b:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->c:Ljava/lang/String;

    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->_Context:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->mNotification:Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    if-nez v0, :cond_1

    .line 46
    :cond_0
    :goto_0
    return-void

    .line 42
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->getDialog()Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    move-result-object v2

    .line 43
    if-eqz v2, :cond_0

    .line 44
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/DeviceResolution;->getDisplayMetrics(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, "SamsungAppsGiftCardNotiBuilder::resizeDialog::metrics is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    if-le v0, v1, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    mul-int/lit8 v0, v0, 0x2

    sub-int v0, v1, v0

    move v1, v0

    :goto_1
    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    check-cast v0, Landroid/view/WindowManager$LayoutParams;

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    move v1, v0

    goto :goto_1
.end method


# virtual methods
.method public populateUI()V
    .locals 10

    .prologue
    const v3, 0x7f0c01ce

    const/16 v9, 0x8

    const/4 v8, 0x6

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->_Context:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 74
    const v1, 0x7f04007d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 75
    new-instance v0, Lcom/sec/android/app/samsungapps/ActivityHelper;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/ActivityHelper;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->helper:Lcom/sec/android/app/samsungapps/ActivityHelper;

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->helper:Lcom/sec/android/app/samsungapps/ActivityHelper;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/ActivityHelper;->show(I)V

    .line 80
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 81
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->mNotification:Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->getNotificationType()I

    move-result v2

    if-ne v2, v8, :cond_3

    .line 83
    invoke-virtual {v0, v9}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 98
    :goto_0
    const v0, 0x7f0c01cd

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 99
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->mNotification:Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->moreDetailBtnVal:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->mNotification:Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->moreDetailBtnVal:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->mNotification:Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->moreDetailBtnLink:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->mNotification:Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->moreDetailBtnLink:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 102
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->mNotification:Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->moreDetailBtnVal:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 103
    new-instance v2, Lcom/sec/android/app/samsungapps/eh;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/eh;-><init>(Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    invoke-virtual {v0, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 118
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->_Context:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->getAlertDialogButtonOrder(Landroid/content/Context;)I

    move-result v0

    .line 121
    if-ne v0, v7, :cond_6

    .line 123
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->_Context:Landroid/content/Context;

    const v3, 0x7f04004c

    invoke-direct {v0, v2, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    .line 130
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->mNotification:Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    iget v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->eventType:I

    if-ne v0, v7, :cond_7

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->b:Ljava/lang/String;

    new-instance v3, Lcom/sec/android/app/samsungapps/ei;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/ei;-><init>(Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;)V

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 225
    :cond_0
    :goto_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setView(Landroid/view/View;)V

    .line 226
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->mNotification:Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->notificationHeaderVal:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->mNotification:Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->notificationHeaderVal:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 228
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "<i>"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->_Context:Landroid/content/Context;

    const v4, 0x7f0802fa

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "</i>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 229
    const-string v0, "mNotification.notificationHeaderVal is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    .line 235
    :goto_4
    const v0, 0x7f0c01cb

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 236
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->mNotification:Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->notificationVal:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 238
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->mNotification:Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->promotionImgUrl:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->mNotification:Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->promotionImgUrl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_2

    .line 240
    const v0, 0x7f0c01cc

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    .line 241
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->_Context:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getNetAPI()Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setNetAPI(Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;)V

    .line 242
    invoke-virtual {v0, v6}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setVisibility(I)V

    .line 243
    new-instance v1, Lcom/sec/android/app/samsungapps/en;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/app/samsungapps/en;-><init>(Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;Lcom/sec/android/app/samsungapps/view/CacheWebImageView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setConverter(Lcom/sec/android/app/samsungapps/view/CacheWebImageView$BitmapConverter;)V

    .line 265
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->mNotification:Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->promotionImgUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setURL(Ljava/lang/String;)V

    .line 267
    :cond_2
    return-void

    .line 87
    :cond_3
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v2, v3, :cond_4

    .line 88
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->_Context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    .line 89
    invoke-virtual {v0}, Landroid/widget/CheckBox;->getPaddingLeft()I

    move-result v3

    const/high16 v4, 0x41200000    # 10.0f

    mul-float/2addr v2, v4

    const/high16 v4, 0x3f000000    # 0.5f

    add-float/2addr v2, v4

    float-to-int v2, v2

    add-int/2addr v2, v3

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getPaddingTop()I

    move-result v3

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getPaddingRight()I

    move-result v4

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getPaddingBottom()I

    move-result v5

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/widget/CheckBox;->setPadding(IIII)V

    .line 94
    :cond_4
    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 95
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->onClickListener()Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 115
    :cond_5
    invoke-virtual {v0, v9}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_1

    .line 127
    :cond_6
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->_Context:Landroid/content/Context;

    const v3, 0x7f04004b

    invoke-direct {v0, v2, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    goto/16 :goto_2

    .line 141
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->mNotification:Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    iget v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->eventType:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->mNotification:Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->shortcutBtnVal:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->b:Ljava/lang/String;

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->mNotification:Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->getNotificationType()I

    move-result v0

    const/4 v2, 0x5

    if-ne v0, v2, :cond_b

    .line 147
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->b:Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 149
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->_Context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0802b5

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->b:Ljava/lang/String;

    .line 151
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->b:Ljava/lang/String;

    new-instance v3, Lcom/sec/android/app/samsungapps/ej;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/ej;-><init>(Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;)V

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 209
    :cond_a
    :goto_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->_Context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0801e9

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->c:Ljava/lang/String;

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->c:Ljava/lang/String;

    new-instance v3, Lcom/sec/android/app/samsungapps/em;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/em;-><init>(Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;)V

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setNegativeButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    goto/16 :goto_3

    .line 176
    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->mNotification:Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->getNotificationType()I

    move-result v0

    if-ne v0, v8, :cond_a

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->b:Ljava/lang/String;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 180
    :cond_c
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->_Context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0800f0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->b:Ljava/lang/String;

    .line 182
    :cond_d
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->b:Ljava/lang/String;

    new-instance v3, Lcom/sec/android/app/samsungapps/el;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/el;-><init>(Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;)V

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    goto :goto_5

    .line 233
    :cond_e
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "<i>"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;->mNotification:Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    iget-object v3, v3, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->notificationHeaderVal:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "</i>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(Ljava/lang/CharSequence;)V

    goto/16 :goto_4
.end method
