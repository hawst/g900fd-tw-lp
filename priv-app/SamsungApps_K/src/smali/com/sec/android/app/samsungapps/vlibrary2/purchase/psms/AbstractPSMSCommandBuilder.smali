.class public abstract Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/AbstractPSMSCommandBuilder;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommandBuilder;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSConfirmMSGCommand$IIPSMSConfirmMSGData;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm$IPSMSConfirmData;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitCommand$IPSMSInitCommandData;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSMoSend$IPSMSMoSendData;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/SendPSMS$ISendPSMSData;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey$IValidatePSMSRandomKey;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC$IValidatePSMSNTNCData;


# instance fields
.field private _Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field private _FinalMap:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

.field private _InitMap:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;

.field private _PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

.field private bSMSSendSuccess:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/AbstractPSMSCommandBuilder;->_InitMap:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;

    .line 32
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/AbstractPSMSCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 33
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/AbstractPSMSCommandBuilder;->_FinalMap:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

    .line 34
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/AbstractPSMSCommandBuilder;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    .line 35
    return-void
.end method


# virtual methods
.method public checkConfirmMSG()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 52
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSConfirmMSGCommand;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSConfirmMSGCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSConfirmMSGCommand$IIPSMSConfirmMSGData;)V

    return-object v0
.end method

.method public checkRandomKey()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 48
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSRandomKey$IValidatePSMSRandomKey;)V

    return-object v0
.end method

.method public checkTNC()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 44
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC$IValidatePSMSNTNCData;)V

    return-object v0
.end method

.method public complete()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 60
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSConfirm$IPSMSConfirmData;)V

    return-object v0
.end method

.method public abstract createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
.end method

.method public abstract createSMSSender()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ISMSSender;
.end method

.method public existConfirmMSG()Z
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/AbstractPSMSCommandBuilder;->_InitMap:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->existConfirmMsg()Z

    move-result v0

    return v0
.end method

.method public existRandomKey()Z
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/AbstractPSMSCommandBuilder;->_InitMap:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->existRandomKey()Z

    move-result v0

    return v0
.end method

.method public existTNC()Z
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/AbstractPSMSCommandBuilder;->_InitMap:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->existTNC()Z

    move-result v0

    return v0
.end method

.method public abstract getAskConfirmMSGViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
.end method

.method public getConfirmMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/AbstractPSMSCommandBuilder;->_InitMap:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->getConfirmMsg()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getConfirmResult()Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/AbstractPSMSCommandBuilder;->_FinalMap:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

    return-object v0
.end method

.method public getFormattedSellingPrice()Ljava/lang/String;
    .locals 4

    .prologue
    .line 169
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    .line 170
    if-nez v0, :cond_0

    .line 172
    const-string v0, "error"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->err(Ljava/lang/String;)V

    .line 173
    const-string v0, ""

    .line 175
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/AbstractPSMSCommandBuilder;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getDisplayBuyPrice()D

    move-result-wide v1

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/AbstractPSMSCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v3

    iget-object v3, v3, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->currencyUnit:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getFormattedPrice(DLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getInitResultMap()Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/AbstractPSMSCommandBuilder;->_InitMap:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;

    return-object v0
.end method

.method public abstract getNotiTNCSendFailCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public getOrderID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/AbstractPSMSCommandBuilder;->_InitMap:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->getOrderID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPSMSDestNo()Ljava/lang/String;
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/AbstractPSMSCommandBuilder;->_InitMap:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->getShortCode(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract getPSMSRandomkeyViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
.end method

.method public getPSMSSendingMessage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/AbstractPSMSCommandBuilder;->_InitMap:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->getMessage(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPaymentID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/AbstractPSMSCommandBuilder;->_InitMap:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->getPaymentID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProduct()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/AbstractPSMSCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    return-object v0
.end method

.method public getProductName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/AbstractPSMSCommandBuilder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRandomKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/AbstractPSMSCommandBuilder;->_InitMap:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->getRandomKey()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getResponseTime()J
    .locals 2

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/AbstractPSMSCommandBuilder;->_InitMap:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->getResponseTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public getRetryCount()I
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/AbstractPSMSCommandBuilder;->_InitMap:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->getRetryCount()I

    move-result v0

    return v0
.end method

.method public getSMSSendSuccess()Z
    .locals 1

    .prologue
    .line 109
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/AbstractPSMSCommandBuilder;->bSMSSendSuccess:Z

    return v0
.end method

.method public getTNCDisplayMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/AbstractPSMSCommandBuilder;->_InitMap:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->getTNCMsg()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTNCSMSMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/AbstractPSMSCommandBuilder;->_InitMap:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->getTNCSMSMsg()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTNCSMSNo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/AbstractPSMSCommandBuilder;->_InitMap:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->getTNCSMSNumber()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract getTNCViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;
.end method

.method public init()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 39
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitCommand;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitCommand$IPSMSInitCommandData;)V

    return-object v0
.end method

.method public isFinalMapSuccess()Z
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/AbstractPSMSCommandBuilder;->_FinalMap:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->downLoadURI:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/AbstractPSMSCommandBuilder;->_FinalMap:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->downLoadURI:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    const/4 v0, 0x1

    .line 142
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public moDelivery()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 56
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSMoSend;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSMoSend;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSMoSend$IPSMSMoSendData;)V

    return-object v0
.end method

.method public needSendSMS()Z
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/AbstractPSMSCommandBuilder;->_InitMap:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitResult;->getSendSMS()Z

    move-result v0

    return v0
.end method

.method public psmsPayment()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 164
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSCommandBuilder;)V

    return-object v0
.end method

.method public sendSMS()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 113
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/SendPSMS;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/SendPSMS;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/SendPSMS$ISendPSMSData;)V

    return-object v0
.end method

.method public setSMSSendSuccess(Z)V
    .locals 0

    .prologue
    .line 125
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/AbstractPSMSCommandBuilder;->bSMSSendSuccess:Z

    .line 126
    return-void
.end method
