.class final Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/d;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

.field final synthetic b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 0

    .prologue
    .line 183
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/d;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;

    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/d;->a:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceiveResult(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 2

    .prologue
    .line 187
    if-eqz p2, :cond_0

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/d;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;->REGISTERCARD_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;)V

    .line 190
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/d;->a:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;->onCommandResult(Z)V

    .line 197
    :goto_0
    return-void

    .line 194
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/d;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;->REGISTERCARD_FAIL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;)V

    .line 195
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/d;->a:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;->onCommandResult(Z)V

    goto :goto_0
.end method
