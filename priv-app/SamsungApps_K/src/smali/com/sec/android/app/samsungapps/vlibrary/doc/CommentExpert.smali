.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/CommentExpert;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;


# instance fields
.field public commentID:Ljava/lang/String;

.field public commentTitle:Ljava/lang/String;

.field public date:Ljava/lang/String;

.field public expertComment:Ljava/lang/String;

.field public expertCommentSource:Ljava/lang/String;

.field public expertCommentSourceUrl:Ljava/lang/String;

.field public expertName:Ljava/lang/String;

.field public updateDate:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 2

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CommentExpert;

    const/4 v1, 0x1

    invoke-virtual {p1, v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->mappingClass(Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 24
    return-void
.end method


# virtual methods
.method public compareUserID(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    return v0
.end method

.method public getComment()Ljava/lang/String;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CommentExpert;->expertComment:Ljava/lang/String;

    return-object v0
.end method

.method public getCommentDetail()Lcom/sec/android/app/samsungapps/vlibrary/doc/CommentDetail;
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CommentExpert;->date:Ljava/lang/String;

    return-object v0
.end method

.method public getExpertSource()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CommentExpert;->expertCommentSource:Ljava/lang/String;

    return-object v0
.end method

.method public getExpertTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CommentExpert;->commentTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getExpertUpdateDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CommentExpert;->updateDate:Ljava/lang/String;

    return-object v0
.end method

.method public getExpertUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CommentExpert;->expertCommentSourceUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CommentExpert;->commentID:Ljava/lang/String;

    return-object v0
.end method

.method public getLoginID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CommentExpert;->expertName:Ljava/lang/String;

    return-object v0
.end method

.method public getRating()I
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    return v0
.end method

.method public getUserID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    return-object v0
.end method

.method public isExpertComment()Z
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x1

    return v0
.end method

.method public isSeller()Z
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    return v0
.end method

.method public setCommentDetail(Lcom/sec/android/app/samsungapps/vlibrary/doc/CommentDetail;)V
    .locals 0

    .prologue
    .line 116
    return-void
.end method
