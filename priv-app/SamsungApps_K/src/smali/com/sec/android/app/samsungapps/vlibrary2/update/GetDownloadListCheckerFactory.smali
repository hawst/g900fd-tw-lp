.class public Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListCheckerFactory;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/update/IGetDownloadListCheckerFactory;


# instance fields
.field private mGear2Api:Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListCheckerFactory;->mGear2Api:Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;

    .line 15
    return-void
.end method


# virtual methods
.method public createChecker(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/update/IGetDownloadListChecker;
    .locals 2

    .prologue
    .line 19
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IBaseContextUtil;->isGear2(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 20
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListForGear2;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListCheckerFactory;->mGear2Api:Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;

    invoke-direct {v0, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListForGear2;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;)V

    .line 22
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;->instance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListContext;

    move-result-object v0

    goto :goto_0
.end method
