.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponList;
.super Ljava/util/ArrayList;
.source "ProGuard"


# static fields
.field private static final serialVersionUID:J = 0x3bb69e15d451de08L


# instance fields
.field bCompleted:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 13
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponList;->bCompleted:Z

    return-void
.end method


# virtual methods
.method public dump()V
    .locals 2

    .prologue
    .line 20
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;

    .line 22
    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->couponName:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 24
    :cond_0
    return-void
.end method

.method public isCompleted()Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponList;->bCompleted:Z

    return v0
.end method

.method public requestVoucher(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V
    .locals 3

    .prologue
    .line 37
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    .line 38
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 40
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->getCouponList()Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponList;

    move-result-object v0

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/v;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/v;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponList;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->customerCouponList(Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponList;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 47
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 49
    :cond_0
    return-void
.end method

.method public setCompleted(Z)V
    .locals 0

    .prologue
    .line 27
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponList;->bCompleted:Z

    .line 28
    return-void
.end method
