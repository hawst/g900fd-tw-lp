.class public Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field _CommentList:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

.field private _ICommentLoadCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand$ICommentLoadCommandData;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand$ICommentLoadCommandData;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand;->_ICommentLoadCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand$ICommentLoadCommandData;

    .line 21
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand$ICommentLoadCommandData;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand;->_ICommentLoadCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand$ICommentLoadCommandData;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand;Z)V
    .locals 0

    .prologue
    .line 14
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand;->onFinalResult(Z)V

    return-void
.end method

.method private getNetResultReceiver()Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;
    .locals 1

    .prologue
    .line 50
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/f;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/f;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand;)V

    return-object v0
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 0

    .prologue
    .line 25
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand;->request(Landroid/content/Context;)V

    .line 26
    return-void
.end method

.method protected request(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand;->_ICommentLoadCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand$ICommentLoadCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand$ICommentLoadCommandData;->getCommentType()I

    move-result v1

    .line 31
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand;->_ICommentLoadCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand$ICommentLoadCommandData;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand$ICommentLoadCommandData;->getProductID()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand;->_ICommentLoadCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand$ICommentLoadCommandData;

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand$ICommentLoadCommandData;->isTablet()Z

    move-result v3

    invoke-direct {v0, v2, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;-><init>(Ljava/lang/String;IZ)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand;->_CommentList:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    .line 33
    const/4 v0, 0x0

    .line 34
    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 36
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand;->_ICommentLoadCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand$ICommentLoadCommandData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand$ICommentLoadCommandData;->getCommentList()Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand;->_CommentList:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand;->getNetResultReceiver()Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->commentList(Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListRequestParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 43
    :cond_0
    :goto_0
    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 45
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 46
    return-void

    .line 38
    :cond_1
    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 40
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand;->_ICommentLoadCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand$ICommentLoadCommandData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand$ICommentLoadCommandData;->getCommentList()Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand;->_CommentList:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentLoadCommand;->getNetResultReceiver()Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->commentListExpert(Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListRequestParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    goto :goto_0
.end method
