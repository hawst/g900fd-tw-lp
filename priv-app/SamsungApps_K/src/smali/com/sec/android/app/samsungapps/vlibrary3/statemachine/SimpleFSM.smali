.class public abstract Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/SimpleFSM;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field protected mState:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/SimpleFSM;->getIdleState()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/SimpleFSM;->mState:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method protected abstract entry()V
.end method

.method protected abstract exit()V
.end method

.method protected abstract getIdleState()Ljava/lang/Object;
.end method

.method public getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/SimpleFSM;->mState:Ljava/lang/Object;

    return-object v0
.end method

.method protected final setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 17
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/SimpleFSM;->exit()V

    .line 18
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/SimpleFSM;->mState:Ljava/lang/Object;

    .line 19
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/SimpleFSM;->entry()V

    .line 20
    return-void
.end method
