.class public Lcom/sec/android/app/samsungapps/DeepLinker;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->a:Landroid/content/Context;

    .line 26
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->b:Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    .line 27
    return-void
.end method


# virtual methods
.method public needRunDeepLink()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 281
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->b:Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    if-eqz v1, :cond_2

    .line 282
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->b:Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    .line 284
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isBannerList()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 344
    :cond_0
    :goto_0
    return v0

    .line 288
    :cond_1
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isCategoryList()Z

    move-result v2

    if-nez v2, :cond_0

    .line 292
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isDetailPage()Z

    move-result v2

    if-nez v2, :cond_0

    .line 296
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isSellerPage()Z

    move-result v2

    if-nez v2, :cond_0

    .line 300
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isMainView()Z

    .line 303
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isSearchResult()Z

    move-result v2

    if-nez v2, :cond_0

    .line 307
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isMainCategoryList()Z

    move-result v2

    if-nez v2, :cond_0

    .line 311
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isPaidProductList()Z

    move-result v2

    if-nez v2, :cond_0

    .line 315
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isFreeProductList()Z

    move-result v2

    if-nez v2, :cond_0

    .line 319
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isNewProductList()Z

    move-result v2

    if-nez v2, :cond_0

    .line 323
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isPaymentMehtodsList()Z

    move-result v2

    if-nez v2, :cond_0

    .line 327
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isGiftCardList()Z

    move-result v2

    if-nez v2, :cond_0

    .line 331
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isGoToDownloads()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isGoToUpdates()Z

    move-result v2

    if-nez v2, :cond_0

    .line 335
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isInterimPage()Z

    move-result v2

    if-nez v2, :cond_0

    .line 339
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isSubCategoryList()Z

    move-result v1

    if-nez v1, :cond_0

    .line 344
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public runDeepLink()Z
    .locals 8

    .prologue
    const/high16 v7, 0x24000000

    const v4, 0x7f0802f6

    const/high16 v6, 0x20000000

    const/high16 v5, 0x4000000

    const/4 v0, 0x1

    .line 348
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->b:Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    if-eqz v1, :cond_17

    .line 349
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->b:Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    .line 351
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isBannerList()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 352
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->getBannerListID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->getCategoryTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->a:Landroid/content/Context;

    const-class v5, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    if-eqz v1, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "_titleText"

    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    const-string v1, "_deeplink_categoryId"

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "_productSetListId"

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "_buttonState"

    sget-object v2, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->NONE:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v1, "_listType"

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->ProductSetList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v1, "isDeepLink"

    invoke-virtual {v3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "amIS2I"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->b:Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isS2I()Z

    move-result v2

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {v3, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->a:Landroid/content/Context;

    invoke-virtual {v1, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 430
    :cond_1
    :goto_0
    return v0

    .line 356
    :cond_2
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isCategoryList()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 357
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->getCategoryID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->getCategoryTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->getAppName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v2, v3, v1}, Lcom/sec/android/app/samsungapps/DeepLinker;->showDeepLinkContentList(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 362
    :cond_3
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isDetailPage()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 363
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->getDetailID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->getAppName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->getFakeModelName()Ljava/lang/String;

    move-result-object v1

    new-instance v4, Landroid/content/Intent;

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->a:Landroid/content/Context;

    const-class v6, Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-direct {v4, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v5, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    const-string v6, "GUID"

    invoke-virtual {v5, v6, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    const-string v2, "cdcontainer"

    new-instance v6, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-direct {v6, v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    invoke-virtual {v4, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "isDeepLink"

    invoke-virtual {v4, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v2, "deepLinkAppName"

    invoke-virtual {v4, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "fakeModel"

    invoke-virtual {v4, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/Main;->getCacheCleanManager(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManager;->toBMode()V

    :cond_4
    const-string v1, "amIS2I"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->b:Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isS2I()Z

    move-result v2

    invoke-virtual {v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "NOACCOUNT"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->b:Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isNoAccount()Z

    move-result v2

    invoke-virtual {v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {v4, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->a:Landroid/content/Context;

    invoke-virtual {v1, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 367
    :cond_5
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isMainView()Z

    .line 370
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isSearchResult()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 371
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->getSearchKeyword()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->a:Landroid/content/Context;

    const-class v4, Lcom/sec/android/app/samsungapps/SearchResultActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "isDeepLink"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v3, "amIS2I"

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->b:Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isS2I()Z

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    if-eqz v1, :cond_6

    const-string v3, "DEFAULT_STRING_FOR_SEARCH"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_6
    invoke-virtual {v2, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->a:Landroid/content/Context;

    invoke-virtual {v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 375
    :cond_7
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isMainCategoryList()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 376
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->getFakeModelName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->a:Landroid/content/Context;

    const-class v4, Lcom/sec/android/app/samsungapps/CategoryListActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "isDeepLink"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    const-string v3, "fakeModel"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/Main;->getCacheCleanManager(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManager;->toBMode()V

    :cond_8
    const-string v1, "amIS2I"

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->b:Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isS2I()Z

    move-result v3

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {v2, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->a:Landroid/content/Context;

    invoke-virtual {v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 380
    :cond_9
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isPaidProductList()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 381
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->a:Landroid/content/Context;

    const-class v3, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "_titleText"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isFreeStore()Z

    move-result v2

    if-eqz v2, :cond_a

    const-string v2, "_buttonState"

    sget-object v3, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->ALL_NEW:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :goto_1
    const-string v2, "isDeepLink"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v2, "amIS2I"

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->b:Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isS2I()Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v2, "_listType"

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopPaid:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-virtual {v1, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->a:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_a
    const-string v2, "_buttonState"

    sget-object v3, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->ALL_PAID_FREE_NEW:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_1

    .line 385
    :cond_b
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isFreeProductList()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 386
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->a:Landroid/content/Context;

    const-class v3, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "_titleText"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isFreeStore()Z

    move-result v2

    if-eqz v2, :cond_c

    const-string v2, "_buttonState"

    sget-object v3, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->ALL_NEW:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :goto_2
    const-string v2, "isDeepLink"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v2, "amIS2I"

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->b:Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isS2I()Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v2, "_listType"

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopFree:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-virtual {v1, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->a:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_c
    const-string v2, "_buttonState"

    sget-object v3, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->ALL_PAID_FREE_NEW:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_2

    .line 390
    :cond_d
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isNewProductList()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 391
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->a:Landroid/content/Context;

    const-class v3, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "_titleText"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isFreeStore()Z

    move-result v2

    if-eqz v2, :cond_e

    const-string v2, "_buttonState"

    sget-object v3, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->ALL_NEW:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :goto_3
    const-string v2, "isDeepLink"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v2, "amIS2I"

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->b:Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isS2I()Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v2, "_listType"

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopNew:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-virtual {v1, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->a:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_e
    const-string v2, "_buttonState"

    sget-object v3, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->ALL_PAID_FREE_NEW:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_3

    .line 395
    :cond_f
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isGoToDownloads()Z

    move-result v2

    if-eqz v2, :cond_10

    .line 396
    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->a:Landroid/content/Context;

    const-class v4, Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->a:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "fakeModel"

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->getFakeModelName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "buttonType"

    const/4 v3, 0x2

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "isDeepLink"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "amIS2I"

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->b:Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isS2I()Z

    move-result v3

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {v2, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->a:Landroid/content/Context;

    invoke-virtual {v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 400
    :cond_10
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isGoToUpdates()Z

    move-result v2

    if-eqz v2, :cond_11

    .line 401
    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->a:Landroid/content/Context;

    const-class v4, Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->a:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "fakeModel"

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->getFakeModelName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "buttonType"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "isDeepLink"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "amIS2I"

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->b:Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isS2I()Z

    move-result v3

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {v2, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->a:Landroid/content/Context;

    invoke-virtual {v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 405
    :cond_11
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isSellerPage()Z

    move-result v2

    if-eqz v2, :cond_12

    .line 406
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->getSellerID()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->a:Landroid/content/Context;

    const-class v4, Lcom/sec/android/app/samsungapps/RelatedAppProductListActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "_sellerID"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "isDeepLink"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "amIS2I"

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->b:Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isS2I()Z

    move-result v3

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {v2, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->a:Landroid/content/Context;

    invoke-virtual {v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 410
    :cond_12
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isPaymentMehtodsList()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 411
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->a:Landroid/content/Context;

    const-class v3, Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "isDeepLink"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v2, "amIS2I"

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->b:Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isS2I()Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {v1, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->a:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 415
    :cond_13
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isGiftCardList()Z

    move-result v2

    if-eqz v2, :cond_14

    .line 416
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->a:Landroid/content/Context;

    const-class v3, Lcom/sec/android/app/samsungapps/GiftCardListActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "isDeepLink"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v2, "amIS2I"

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->b:Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isS2I()Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {v1, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->a:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 420
    :cond_14
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isInterimPage()Z

    move-result v2

    if-eqz v2, :cond_15

    .line 421
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->a:Landroid/content/Context;

    const-class v3, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "isDeepLink"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v2, "amIS2I"

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->b:Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isS2I()Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {v1, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->a:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "showInterimPage:: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 425
    :cond_15
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isSubCategoryList()Z

    move-result v2

    if-eqz v2, :cond_17

    .line 426
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->getCategoryID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->getCategoryTitle()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->a:Landroid/content/Context;

    const-class v5, Lcom/sec/android/app/samsungapps/CategorySubListActivity;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    if-eqz v1, :cond_16

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_16

    const-string v4, "SubCategoryTitle"

    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_16
    const-string v1, "SubCategoryContentId"

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "isDeepLink"

    invoke-virtual {v3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "amIS2I"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->b:Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isS2I()Z

    move-result v2

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {v3, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->a:Landroid/content/Context;

    invoke-virtual {v1, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 430
    :cond_17
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public showDeepLinkContentList(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 78
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->a:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 82
    const-string v1, "defalutTitle"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 83
    const-string v1, "_titleText"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 86
    :cond_0
    const-string v1, "_deeplink_categoryId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 88
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isFreeStore()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 89
    const-string v1, "_buttonState"

    sget-object v2, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->ALL_NEW:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 94
    :goto_0
    const-string v1, "isDeepLink"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 95
    const-string v1, "deepLinkAppName"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 96
    const-string v1, "amIS2I"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->b:Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isS2I()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 97
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 98
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/DeepLinker;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 99
    return-void

    .line 91
    :cond_1
    const-string v1, "_buttonState"

    sget-object v2, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->ALL_PAID_FREE_NEW:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_0
.end method
