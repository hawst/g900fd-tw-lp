.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum EXCUTE_WITH_FREE_APP:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;

.field public static final enum EXCUTE_WITH_ORDERID:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;

.field public static final enum EXCUTE_WITH_PAID_APP:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;

.field public static final enum EXCUTE_WITH_PREPOSTAPP:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;

.field public static final enum RECEIVE_FAILURE:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;

.field public static final enum RECEIVE_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 23
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;

    const-string v1, "EXCUTE_WITH_ORDERID"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;->EXCUTE_WITH_ORDERID:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;

    const-string v1, "EXCUTE_WITH_PREPOSTAPP"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;->EXCUTE_WITH_PREPOSTAPP:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;

    const-string v1, "EXCUTE_WITH_FREE_APP"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;->EXCUTE_WITH_FREE_APP:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;

    const-string v1, "EXCUTE_WITH_PAID_APP"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;->EXCUTE_WITH_PAID_APP:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;

    const-string v1, "RECEIVE_SUCCESS"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;->RECEIVE_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;

    const-string v1, "RECEIVE_FAILURE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;->RECEIVE_FAILURE:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;

    .line 21
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;->EXCUTE_WITH_ORDERID:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;->EXCUTE_WITH_PREPOSTAPP:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;->EXCUTE_WITH_FREE_APP:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;->EXCUTE_WITH_PAID_APP:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;->RECEIVE_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;->RECEIVE_FAILURE:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiverStateMachine$Event;

    return-object v0
.end method
