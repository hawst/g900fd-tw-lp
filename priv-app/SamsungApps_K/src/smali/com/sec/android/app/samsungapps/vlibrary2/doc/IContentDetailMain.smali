.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract getCategoryProductList()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;
.end method

.method public abstract getSellerProductList()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;
.end method

.method public abstract getVAverageRating()F
.end method

.method public abstract getVCategoryID()Ljava/lang/String;
.end method

.method public abstract getVCategoryName()Ljava/lang/String;
.end method

.method public abstract getVContentType()Ljava/lang/String;
.end method

.method public abstract getVContentURL()Ljava/lang/String;
.end method

.method public abstract getVCurrencyUnit()Ljava/lang/String;
.end method

.method public abstract getVGUID()Ljava/lang/String;
.end method

.method public abstract getVLikeCount()I
.end method

.method public abstract getVLinkStoreName()Ljava/lang/String;
.end method

.method public abstract getVProductID()Ljava/lang/String;
.end method

.method public abstract getVProductImgUrl()Ljava/lang/String;
.end method

.method public abstract getVProductName()Ljava/lang/String;
.end method

.method public abstract getVRatingParticipants()I
.end method

.method public abstract getVReducePrice()D
.end method

.method public abstract getVSellerId()Ljava/lang/String;
.end method

.method public abstract getVSellerName()Ljava/lang/String;
.end method

.method public abstract getVSellingPrice()D
.end method

.method public abstract hasLike()Z
.end method

.method public abstract hasMyComment()Z
.end method

.method public abstract hasMyRating()Z
.end method

.method public abstract hasSellerID()Z
.end method

.method public abstract hasWishList()Z
.end method

.method public abstract hasWishListID()Z
.end method

.method public abstract isAlreadyPurchased()Z
.end method

.method public abstract isDiscount()Z
.end method

.method public abstract isLinkApp()Z
.end method

.method public abstract isMobileWalletSupportYn()Z
.end method

.method public abstract isPackageInstalled(Landroid/content/Context;)Z
.end method

.method public abstract isPrePostApp()Z
.end method

.method public abstract isSINAContent()Z
.end method

.method public abstract isUpdatable()Z
.end method

.method public abstract isWidgetContent()Z
.end method

.method public abstract setAlreadyPurchased()V
.end method

.method public abstract setCategoryProductList(Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;)V
.end method

.method public abstract setSellerProductList(Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;)V
.end method
