.class final Lcom/sec/android/app/samsungapps/downloadableapps/b;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider$IButtonDataGetter;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)V
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/downloadableapps/b;->a:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getInstallingCount()I
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/b;->a:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->c(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->getInstallingCount()I

    move-result v0

    return v0
.end method

.method public final getItemCount()I
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/b;->a:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->c(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->getDownloadableItemCount()I

    move-result v0

    return v0
.end method

.method public final isAllPage()Z
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/b;->a:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->c(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/DownloadableListWidget;->isAllPage()Z

    move-result v0

    return v0
.end method

.method public final isVzwAtSamsungUpdates()Z
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadableapps/b;->a:Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;->e(Lcom/sec/android/app/samsungapps/downloadableapps/DownloadableAppsActivity;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CSC;->isVZW()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
