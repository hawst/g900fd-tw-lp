.class public Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _After:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

.field private _IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;

.field private _bClearPasswordConfirmedTime:Z

.field private mNotiPopupFactory:Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;


# direct methods
.method public constructor <init>(ZLcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 16
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->_bClearPasswordConfirmedTime:Z

    .line 21
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->_bClearPasswordConfirmedTime:Z

    .line 22
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->_IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;

    .line 23
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->mNotiPopupFactory:Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;

    .line 24
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;Z)V
    .locals 0

    .prologue
    .line 13
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;Z)V
    .locals 0

    .prologue
    .line 13
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->_Context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;Z)V
    .locals 0

    .prologue
    .line 13
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->_IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;Z)V
    .locals 0

    .prologue
    .line 13
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;Z)V
    .locals 0

    .prologue
    .line 13
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->onFinalResult(Z)V

    return-void
.end method

.method private isUncStore()Z
    .locals 1

    .prologue
    .line 47
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    .line 48
    if-nez v0, :cond_0

    .line 49
    const/4 v0, 0x0

    .line 51
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isUncStore()Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->isLoginRequired()Z

    move-result v0

    if-nez v0, :cond_0

    .line 30
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->onFinalResult(Z)V

    .line 43
    :goto_0
    return-void

    .line 34
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->_IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;->isSamsungAccountInstalled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 36
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->onSamsungAccountLogin()V

    goto :goto_0

    .line 40
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->onNormalLogin()V

    goto :goto_0
.end method

.method protected isLoginRequired()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->isUncStore()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 59
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->_IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;->isLogedIn()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isTestMode()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onLoginSuccess()V
    .locals 3

    .prologue
    .line 131
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->_bClearPasswordConfirmedTime:Z

    if-nez v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->_IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;->setPasswordConfirmedTime()V

    .line 139
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->_After:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    if-eqz v0, :cond_1

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->_After:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->_Context:Landroid/content/Context;

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->Nothing:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 142
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->onFinalResult(Z)V

    .line 143
    return-void
.end method

.method protected onNormalLogin()V
    .locals 3

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->_IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;->normalLogin()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/account/j;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/j;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 160
    return-void
.end method

.method protected onOldSamsungAccountLogin()V
    .locals 3

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->_IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;->loginSamsungAccount()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/account/h;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/h;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 127
    return-void
.end method

.method protected onSamsungAccountLogin()V
    .locals 3

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->_IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;->isSamsungAccountDisabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->mNotiPopupFactory:Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->_Context:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;->createNotiPopup(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopup;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/account/f;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/f;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;)V

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopup;->showAccountDisabled(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupConfirmResponse;)V

    .line 98
    :goto_0
    return-void

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->_IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;->isTokenBasedSamsungAccountInstalled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->_IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;->tokenBasedLogin()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/account/g;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/g;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0

    .line 95
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->onOldSamsungAccountLogin()V

    goto :goto_0
.end method

.method public setCommandToBeProcessedAfterLogin(Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V
    .locals 0

    .prologue
    .line 164
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginValidateCommand;->_After:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 165
    return-void
.end method
