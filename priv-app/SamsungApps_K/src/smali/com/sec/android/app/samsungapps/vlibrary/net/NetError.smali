.class public Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field e:Ljava/lang/Exception;

.field private errorcode:Ljava/lang/String;

.field private errormsg:Ljava/lang/String;

.field mErrorChecker:Lcom/sec/android/app/samsungapps/vlibrary/net/NetErrorChecker;

.field mErrorType:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError$ErrorType;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->errorcode:Ljava/lang/String;

    .line 8
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->errormsg:Ljava/lang/String;

    .line 10
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->e:Ljava/lang/Exception;

    .line 23
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetErrorChecker;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetErrorChecker;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->mErrorChecker:Lcom/sec/android/app/samsungapps/vlibrary/net/NetErrorChecker;

    .line 24
    return-void
.end method


# virtual methods
.method public checkServerError(Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;)V
    .locals 1

    .prologue
    .line 83
    if-nez p1, :cond_0

    .line 92
    :goto_0
    return-void

    .line 86
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;->isError()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 88
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError$ErrorType;->SVRError:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError$ErrorType;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->mErrorType:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError$ErrorType;

    .line 90
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;->getErrorMsg()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->errormsg:Ljava/lang/String;

    .line 91
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;->getErrorCode()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->errorcode:Ljava/lang/String;

    goto :goto_0
.end method

.method public clearErrorCondition()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 104
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->errormsg:Ljava/lang/String;

    .line 105
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->errorcode:Ljava/lang/String;

    .line 106
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->mErrorType:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError$ErrorType;

    .line 107
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->e:Ljava/lang/Exception;

    .line 108
    return-void
.end method

.method public getErrorChecker()Lcom/sec/android/app/samsungapps/vlibrary/net/NetErrorChecker;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->mErrorChecker:Lcom/sec/android/app/samsungapps/vlibrary/net/NetErrorChecker;

    return-object v0
.end method

.method public getErrorCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->errorcode:Ljava/lang/String;

    return-object v0
.end method

.method public getErrorMsg()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->errormsg:Ljava/lang/String;

    return-object v0
.end method

.method public getException()Ljava/lang/Exception;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->e:Ljava/lang/Exception;

    return-object v0
.end method

.method public getType()Lcom/sec/android/app/samsungapps/vlibrary/net/NetError$ErrorType;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->mErrorType:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError$ErrorType;

    return-object v0
.end method

.method public isError()Z
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->mErrorChecker:Lcom/sec/android/app/samsungapps/vlibrary/net/NetErrorChecker;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetErrorChecker;->isError(Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;)Z

    move-result v0

    return v0
.end method

.method public setConnectionTimeoutException(Lorg/apache/http/conn/ConnectTimeoutException;)V
    .locals 1

    .prologue
    .line 99
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError$ErrorType;->CONNECTIONTIMEOUT:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError$ErrorType;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->mErrorType:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError$ErrorType;

    .line 100
    return-void
.end method

.method public setErrorCode(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->errorcode:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public setErrorMsg(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->errormsg:Ljava/lang/String;

    .line 34
    return-void
.end method

.method public setException(Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError$ErrorType;->EXCEPTION:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError$ErrorType;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->mErrorType:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError$ErrorType;

    .line 53
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->e:Ljava/lang/Exception;

    .line 54
    return-void
.end method

.method public setHttpError()V
    .locals 1

    .prologue
    .line 95
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError$ErrorType;->HTTPERROR:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError$ErrorType;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->mErrorType:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError$ErrorType;

    .line 96
    return-void
.end method

.method public setParseError()V
    .locals 1

    .prologue
    .line 74
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError$ErrorType;->PARSINGERROR:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError$ErrorType;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->mErrorType:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError$ErrorType;

    .line 75
    return-void
.end method

.method public setTimeoutException(Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError$ErrorType;->SOCKETTIMEOUT:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError$ErrorType;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->mErrorType:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError$ErrorType;

    .line 59
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->e:Ljava/lang/Exception;

    .line 60
    return-void
.end method
