.class final Lcom/sec/android/app/samsungapps/widget/detail/r;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)V
    .locals 0

    .prologue
    .line 183
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/r;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 235
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 230
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 4

    .prologue
    .line 189
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/r;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->e(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/140"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 191
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/16 v1, 0x8c

    if-ne v0, v1, :cond_3

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/r;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->e(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/r;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->b(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0079

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 200
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/r;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->a(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;Ljava/lang/String;)V

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/r;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->b(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;Ljava/lang/String;)Ljava/lang/String;

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/r;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->g(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/r;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->f(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/r;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->h(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/r;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->c(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;Ljava/lang/String;)I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 204
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/r;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/r;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->a(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->a(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;I)I

    .line 205
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/r;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/r;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->g(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->d(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;Ljava/lang/String;)Ljava/lang/String;

    .line 206
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/r;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->a(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/widget/EditText;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/detail/s;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/detail/s;-><init>(Lcom/sec/android/app/samsungapps/widget/detail/r;)V

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 214
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/r;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->j(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 215
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/r;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->k(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Z

    .line 216
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/r;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->l(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/r;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->g(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-le v0, v1, :cond_1

    .line 217
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/r;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/r;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->g(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->a(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;I)I

    .line 219
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/r;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->f(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/r;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->g(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-le v0, v1, :cond_4

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/r;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->a(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/r;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->l(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 225
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/r;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/r;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->g(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->d(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;Ljava/lang/String;)Ljava/lang/String;

    .line 226
    return-void

    .line 197
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/r;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->e(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/r;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->b(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b004e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    .line 222
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/r;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->a(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/r;->a:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->l(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    goto :goto_1
.end method
