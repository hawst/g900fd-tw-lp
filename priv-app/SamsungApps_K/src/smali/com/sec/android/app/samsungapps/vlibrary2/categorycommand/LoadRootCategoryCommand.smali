.class public Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/LoadRootCategoryCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field _CategoryContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/LoadRootCategoryCommand;->_CategoryContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

    .line 20
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/LoadRootCategoryCommand;Z)V
    .locals 0

    .prologue
    .line 15
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/LoadRootCategoryCommand;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 3

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/LoadRootCategoryCommand;->_CategoryContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->isLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 25
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/LoadRootCategoryCommand;->onFinalResult(Z)V

    .line 37
    :goto_0
    return-void

    .line 28
    :cond_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/LoadRootCategoryCommand;->_CategoryContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/c;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/LoadRootCategoryCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->categoryListSearch(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 35
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/LoadRootCategoryCommand;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 36
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    goto :goto_0
.end method
