.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/CheckAppUpgradeValidateCommand$ICheckAppUpgradeValidateCommandData;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract getCheckAppUpgradeResult()Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;
.end method

.method public abstract getOldFreeStoreType()I
.end method

.method public abstract getSavedClientType()Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;
.end method

.method public abstract isDontSelftUpdateCondition()Z
.end method

.method public abstract notifyChangedStoreTypeAndRestart()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract notifyCheckAppUpgradeFinished()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract notifyLimitedStoreTypeAndRestart()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract odcUpdateCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract requestCheckAppUpgradeCommand(Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract saveClientType(Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult$ClientType;)V
.end method

.method public abstract saveStoreType(I)V
.end method

.method public abstract unaUpdateCommand(Z)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract validateDisclaimerCommand(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method
