.class public Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerCustomView;
.super Landroid/widget/TextView;
.source "ProGuard"


# instance fields
.field private a:Z

.field private b:F

.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 16
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerCustomView;->a:Z

    .line 17
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerCustomView;->b:F

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 16
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerCustomView;->a:Z

    .line 17
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerCustomView;->b:F

    .line 39
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerCustomView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 16
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerCustomView;->a:Z

    .line 17
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerCustomView;->b:F

    .line 28
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerCustomView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 52
    sget-object v0, Lcom/sec/android/app/samsungapps/R$styleable;->isa_ChartBannerCustomTextStyle:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 54
    invoke-virtual {v0, v1, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerCustomView;->a:Z

    .line 55
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerCustomView;->b:F

    .line 56
    const/4 v1, 0x2

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerCustomView;->c:I

    .line 57
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerCustomView;->a:Z

    if-eqz v0, :cond_0

    .line 63
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerCustomView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    .line 64
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerCustomView;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 65
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerCustomView;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerCustomView;->b:F

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setStrokeWidth(F)V

    .line 66
    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerCustomView;->c:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerCustomView;->setTextColor(I)V

    .line 67
    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 68
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerCustomView;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 69
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerCustomView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 71
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 72
    return-void
.end method
