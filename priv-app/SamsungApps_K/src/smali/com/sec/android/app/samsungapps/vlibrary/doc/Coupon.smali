.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;


# static fields
.field public static final EXCHANGE_VOUCHER:I = 0x3

.field public static final PRICE_DISCOUNT_VOUCHER:I = 0x2

.field public static final RATE_DISCOUNT_VOUCHER:I = 0x1

.field private static final VOUCHER_RATE_LOW:D = 0.30000001192092896

.field private static final VOUCHER_RATE_MEDIUM:D = 0.6000000238418579


# instance fields
.field public availablePeriodEndDate:Ljava/lang/String;

.field public availablePeriodStartDate:Ljava/lang/String;

.field public couponID:Ljava/lang/String;

.field public couponIssuedSEQ:Ljava/lang/String;

.field public couponName:Ljava/lang/String;

.field public currencyUnit:Ljava/lang/String;

.field public discountPrice:D

.field public discountRate:D

.field public discountType:I

.field private mCouponDetail:Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponDetail;

.field private mIsEnabled:Z

.field public maxPrice:D

.field public minPrice:D

.field public usageCount:I

.field public usageCountRemain:I


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 2

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->mCouponDetail:Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponDetail;

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->mIsEnabled:Z

    .line 44
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;

    const/4 v1, 0x1

    invoke-static {p1, v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;->mappingClass(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 45
    return-void
.end method

.method private checkAvailabilityByRate(D)Z
    .locals 5

    .prologue
    const-wide/16 v3, 0x0

    const/4 v0, 0x1

    .line 257
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->getDiscountType()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 259
    iget-wide v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->minPrice:D

    cmpl-double v1, v1, v3

    if-nez v1, :cond_1

    iget-wide v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->maxPrice:D

    cmpl-double v1, v1, v3

    if-nez v1, :cond_1

    .line 277
    :cond_0
    :goto_0
    return v0

    .line 264
    :cond_1
    iget-wide v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->minPrice:D

    cmpl-double v1, p1, v1

    if-ltz v1, :cond_2

    iget-wide v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->maxPrice:D

    cmpg-double v1, p1, v1

    if-lez v1, :cond_0

    .line 274
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public calcPaymentPrice(D)D
    .locals 7

    .prologue
    const/4 v3, 0x1

    const-wide/16 v0, 0x0

    .line 197
    .line 204
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->getDiscountType()I

    move-result v2

    if-ne v3, v2, :cond_1

    .line 206
    new-instance v0, Ljava/math/BigDecimal;

    const-string v1, "1"

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/math/BigDecimal;

    iget-wide v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->discountRate:D

    invoke-direct {v1, v2, v3}, Ljava/math/BigDecimal;-><init>(D)V

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 207
    new-instance v1, Ljava/math/BigDecimal;

    invoke-direct {v1, p1, p2}, Ljava/math/BigDecimal;-><init>(D)V

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 208
    invoke-virtual {v0}, Ljava/math/BigDecimal;->doubleValue()D

    move-result-wide v0

    .line 226
    :cond_0
    :goto_0
    :try_start_0
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%.2f"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 227
    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 234
    :goto_1
    return-wide v0

    .line 210
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->getDiscountType()I

    move-result v3

    if-ne v2, v3, :cond_2

    .line 212
    new-instance v2, Ljava/math/BigDecimal;

    invoke-direct {v2, p1, p2}, Ljava/math/BigDecimal;-><init>(D)V

    .line 213
    new-instance v3, Ljava/math/BigDecimal;

    iget-wide v4, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->discountPrice:D

    invoke-direct {v3, v4, v5}, Ljava/math/BigDecimal;-><init>(D)V

    .line 214
    invoke-virtual {v2, v3}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigDecimal;->doubleValue()D

    move-result-wide p1

    .line 215
    cmpg-double v2, p1, v0

    if-ltz v2, :cond_0

    :cond_2
    move-wide v0, p1

    goto :goto_0

    .line 229
    :catch_0
    move-exception v2

    .line 231
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Coupon::calcPaymentPrice::"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public checkAvailablity(D)V
    .locals 1

    .prologue
    .line 286
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->checkAvailabilityByRate(D)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 288
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->mIsEnabled:Z

    .line 294
    :goto_0
    return-void

    .line 292
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->mIsEnabled:Z

    goto :goto_0
.end method

.method public enable(Z)V
    .locals 0

    .prologue
    .line 179
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->mIsEnabled:Z

    .line 180
    return-void
.end method

.method public getAvailablePeriodEndDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->availablePeriodEndDate:Ljava/lang/String;

    return-object v0
.end method

.method public getAvailablePeriodStartDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->availablePeriodStartDate:Ljava/lang/String;

    return-object v0
.end method

.method public getCouponID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->couponID:Ljava/lang/String;

    return-object v0
.end method

.method public getCouponIssuedSEQ()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->couponIssuedSEQ:Ljava/lang/String;

    return-object v0
.end method

.method public getCouponName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->couponName:Ljava/lang/String;

    return-object v0
.end method

.method public getCouponSeq()Ljava/lang/String;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->couponIssuedSEQ:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrencyUnit()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->currencyUnit:Ljava/lang/String;

    return-object v0
.end method

.method public getDetail()Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponDetail;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->mCouponDetail:Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponDetail;

    return-object v0
.end method

.method public getDiscountPrice()D
    .locals 2

    .prologue
    .line 62
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->discountPrice:D

    return-wide v0
.end method

.method public getDiscountRate()D
    .locals 2

    .prologue
    .line 74
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->discountRate:D

    return-wide v0
.end method

.method public getDiscountType()I
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->discountType:I

    return v0
.end method

.method public getMaxPrice()D
    .locals 2

    .prologue
    .line 303
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->maxPrice:D

    return-wide v0
.end method

.method public getMinPrice()D
    .locals 2

    .prologue
    .line 298
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->minPrice:D

    return-wide v0
.end method

.method public getUsageCount()I
    .locals 1

    .prologue
    .line 114
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->usageCount:I

    return v0
.end method

.method public getUsageCountRemain()I
    .locals 1

    .prologue
    .line 119
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->usageCountRemain:I

    return v0
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 184
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->mIsEnabled:Z

    return v0
.end method

.method public isHigh()Z
    .locals 4

    .prologue
    .line 144
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->discountRate:D

    const-wide v2, 0x3fe3333340000000L    # 0.6000000238418579

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    .line 146
    const/4 v0, 0x1

    .line 150
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLow()Z
    .locals 4

    .prologue
    .line 128
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->discountRate:D

    const-wide v2, 0x3fd3333340000000L    # 0.30000001192092896

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->discountRate:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    .line 130
    const/4 v0, 0x1

    .line 134
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMedium()Z
    .locals 4

    .prologue
    .line 160
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->discountRate:D

    const-wide v2, 0x3fe3333340000000L    # 0.6000000238418579

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->discountRate:D

    const-wide v2, 0x3fd3333340000000L    # 0.30000001192092896

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    .line 162
    const/4 v0, 0x1

    .line 166
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRemainPriceLost(D)Z
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 243
    .line 245
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->discountPrice:D

    cmpl-double v0, v0, v4

    if-eqz v0, :cond_0

    .line 247
    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, p1, p2}, Ljava/math/BigDecimal;-><init>(D)V

    .line 248
    new-instance v1, Ljava/math/BigDecimal;

    iget-wide v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->discountPrice:D

    invoke-direct {v1, v2, v3}, Ljava/math/BigDecimal;-><init>(D)V

    .line 249
    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->doubleValue()D

    move-result-wide p1

    .line 252
    :cond_0
    cmpg-double v0, p1, v4

    if-gez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDetail(Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponDetail;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Coupon;->mCouponDetail:Lcom/sec/android/app/samsungapps/vlibrary/doc/CouponDetail;

    .line 81
    return-void
.end method
