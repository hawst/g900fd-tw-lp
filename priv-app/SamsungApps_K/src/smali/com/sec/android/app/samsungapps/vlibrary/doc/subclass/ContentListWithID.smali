.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/subclass/ContentListWithID;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field id:I

.field mCategoryID:Ljava/lang/String;

.field mContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

.field paidType:I


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;II)V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/subclass/ContentListWithID;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;Ljava/lang/String;II)V

    .line 22
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput p3, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/subclass/ContentListWithID;->id:I

    .line 14
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/subclass/ContentListWithID;->mContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    .line 15
    iput p4, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/subclass/ContentListWithID;->paidType:I

    .line 16
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/subclass/ContentListWithID;->mCategoryID:Ljava/lang/String;

    .line 17
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/subclass/ContentListWithID;->mContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->clear()V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/subclass/ContentListWithID;->mContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    .line 27
    return-void
.end method

.method public compare(II)Z
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/subclass/ContentListWithID;->id:I

    if-ne v0, p1, :cond_0

    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/subclass/ContentListWithID;->paidType:I

    if-ne v0, p2, :cond_0

    .line 32
    const/4 v0, 0x1

    .line 34
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public compareID(Ljava/lang/String;II)Z
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/subclass/ContentListWithID;->mCategoryID:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 41
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/subclass/ContentListWithID;->id:I

    if-ne v0, p2, :cond_0

    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/subclass/ContentListWithID;->paidType:I

    if-ne v0, p3, :cond_0

    .line 42
    const/4 v0, 0x1

    .line 45
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/subclass/ContentListWithID;->mContentList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    return-object v0
.end method
