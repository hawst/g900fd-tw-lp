.class public Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;
.super Lcom/sec/android/app/samsungapps/ActionBarActivity;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;


# static fields
.field public static final TYPE_CHANGE_SHETAB_MYPAGE:I = 0x3

.field public static final TYPE_REGISTER_SHETAB_MYPAGE:I = 0x2

.field public static final TYPE_REGISTER_SHETAB_PURCHASE:I = 0x1

.field public static final TYPE_VIEW_SHETAB:Ljava/lang/String; = "TYPE_VIEW_SHETAB"


# instance fields
.field final a:Ljava/lang/String;

.field final b:Ljava/lang/String;

.field private c:Landroid/webkit/WebView;

.field private d:Landroid/content/Context;

.field private e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/RegisterIranCreditCardCommand;

.field private f:Z

.field private g:Ljava/util/HashMap;

.field protected mView:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;-><init>()V

    .line 40
    const-string v0, "FAIL"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->a:Ljava/lang/String;

    .line 41
    const-string v0, "AUTHORIZED"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->b:Ljava/lang/String;

    .line 189
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->g:Ljava/util/HashMap;

    .line 55
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->g:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;Ljava/util/HashMap;)Ljava/util/HashMap;
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->g:Ljava/util/HashMap;

    return-object p1
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->d:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/RegisterIranCreditCardCommand;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/RegisterIranCreditCardCommand;

    return-object v0
.end method


# virtual methods
.method public handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z
    .locals 5

    .prologue
    .line 295
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->CreditCardRemoved:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    if-ne v0, v1, :cond_0

    .line 297
    new-instance v0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08027d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->d:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080329

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    .line 301
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z

    move-result v0

    return v0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 306
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->f:Z

    if-nez v0, :cond_0

    .line 308
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->f:Z

    .line 309
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/RegisterIranCreditCardCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/RegisterIranCreditCardCommand;->cancel()V

    .line 311
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onBackPressed()V

    .line 312
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const v7, 0x7f0400ed

    const/4 v2, 0x0

    const v5, 0xb0003

    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 69
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 70
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->readObject(Landroid/content/Intent;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/RegisterIranCreditCardCommand;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/RegisterIranCreditCardCommand;

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/RegisterIranCreditCardCommand;

    if-nez v0, :cond_0

    .line 73
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->finish()V

    .line 127
    :goto_0
    return-void

    .line 76
    :cond_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->addSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V

    .line 77
    iput-object p0, p0, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->d:Landroid/content/Context;

    .line 79
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 80
    const-string v1, "TYPE_VIEW_SHETAB"

    const/4 v4, -0x1

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 105
    const v1, 0x7f080322

    new-array v4, v6, [I

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->setActionBarConfiguration(ILcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    .line 110
    :goto_1
    :pswitch_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 112
    invoke-virtual {p0, v7}, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->setMainViewAndEmptyView(I)V

    .line 117
    :goto_2
    const v0, 0x7f0c024d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->c:Landroid/webkit/WebView;

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->c:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 119
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 120
    invoke-virtual {v0, v6}, Landroid/webkit/WebSettings;->setSavePassword(Z)V

    .line 121
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->c:Landroid/webkit/WebView;

    new-instance v1, Lcom/sec/android/app/samsungapps/es;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/es;-><init>(Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/RegisterIranCreditCardCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/iran/RegisterIranCreditCardCommand;->getIranDebitURL()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->onBackPressed()V

    .line 125
    :goto_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->c:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->requestFocus()Z

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->c:Landroid/webkit/WebView;

    invoke-virtual {v0, v6}, Landroid/webkit/WebView;->setVisibility(I)V

    goto :goto_0

    .line 94
    :pswitch_1
    const v1, 0x7f080322

    new-array v4, v6, [I

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->setActionBarConfiguration(ILcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    goto :goto_1

    .line 100
    :pswitch_2
    const v1, 0x7f080323

    new-array v4, v6, [I

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->setActionBarConfiguration(ILcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    goto :goto_1

    .line 114
    :cond_1
    invoke-virtual {p0, v7}, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->setMainView(I)V

    goto :goto_2

    .line 124
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->c:Landroid/webkit/WebView;

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_3

    .line 80
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 60
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->removeSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V

    .line 61
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onDestroy()V

    .line 62
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 143
    return-void
.end method

.method public toastMessage(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 167
    if-eqz p1, :cond_0

    .line 169
    const-string v0, "layout_inflater"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/IranDebitCardRegisterActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 170
    if-eqz v0, :cond_0

    .line 172
    const v1, 0x7f040024

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 173
    if-nez v1, :cond_1

    .line 187
    :cond_0
    :goto_0
    return-void

    .line 178
    :cond_1
    const v0, 0x7f0c0099

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 179
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 181
    new-instance v0, Landroid/widget/Toast;

    invoke-direct {v0, p0}, Landroid/widget/Toast;-><init>(Landroid/content/Context;)V

    .line 182
    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    .line 183
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setDuration(I)V

    .line 184
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
