.class final Lcom/sec/android/app/samsungapps/jp;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;


# instance fields
.field a:I

.field final synthetic b:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;

.field final synthetic c:Lcom/sec/android/app/samsungapps/widget/AutoUpdateOptionArrayAdapter;

.field final synthetic d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;Lcom/sec/android/app/samsungapps/widget/AutoUpdateOptionArrayAdapter;)V
    .locals 1

    .prologue
    .line 655
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/jp;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    iput-object p2, p0, Lcom/sec/android/app/samsungapps/jp;->b:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;

    iput-object p3, p0, Lcom/sec/android/app/samsungapps/jp;->c:Lcom/sec/android/app/samsungapps/widget/AutoUpdateOptionArrayAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 656
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jp;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->b(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/jp;->a:I

    return-void
.end method


# virtual methods
.method public final onClick(Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;I)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const-wide/16 v2, 0x1

    const/4 v1, 0x0

    .line 661
    packed-switch p2, :pswitch_data_0

    .line 865
    :cond_0
    :goto_0
    return-void

    .line 663
    :pswitch_0
    iget v0, p0, Lcom/sec/android/app/samsungapps/jp;->a:I

    if-nez v0, :cond_1

    .line 664
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jp;->b:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;

    invoke-virtual {v0, p2, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;->setSetting(ILcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting$IAutoUpdateMainSettingResultListener;)Z

    .line 665
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jp;->c:Lcom/sec/android/app/samsungapps/widget/AutoUpdateOptionArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/AutoUpdateOptionArrayAdapter;->notifyDataSetChanged()V

    .line 666
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 667
    new-instance v1, Lcom/sec/android/app/samsungapps/jq;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/jq;-><init>(Lcom/sec/android/app/samsungapps/jp;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 673
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jp;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->d(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->onViewUpdate()V

    goto :goto_0

    .line 675
    :cond_1
    iget v0, p0, Lcom/sec/android/app/samsungapps/jp;->a:I

    if-eq v0, v5, :cond_2

    iget v0, p0, Lcom/sec/android/app/samsungapps/jp;->a:I

    if-ne v0, v6, :cond_0

    .line 676
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jp;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getAutoUpdateManager()Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->isIdle()Z

    move-result v0

    if-nez v0, :cond_3

    .line 677
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/jp;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;)V

    .line 678
    invoke-virtual {v0, v4}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setCanceledOnTouchOutside(Z)V

    .line 679
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/jp;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0801a2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 680
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/jp;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0801a3

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 682
    new-instance v1, Lcom/sec/android/app/samsungapps/jt;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/jt;-><init>(Lcom/sec/android/app/samsungapps/jp;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setBackKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 703
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/jp;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0802ef

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/jv;

    invoke-direct {v2, p0, p2}, Lcom/sec/android/app/samsungapps/jv;-><init>(Lcom/sec/android/app/samsungapps/jp;I)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 720
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/jp;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f08023d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/jx;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/jx;-><init>(Lcom/sec/android/app/samsungapps/jp;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setNegativeButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 736
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->show()V

    goto/16 :goto_0

    .line 739
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jp;->b:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;

    invoke-virtual {v0, p2, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;->setSetting(ILcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting$IAutoUpdateMainSettingResultListener;)Z

    .line 740
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jp;->c:Lcom/sec/android/app/samsungapps/widget/AutoUpdateOptionArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/AutoUpdateOptionArrayAdapter;->notifyDataSetChanged()V

    .line 741
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 742
    new-instance v1, Lcom/sec/android/app/samsungapps/jz;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/jz;-><init>(Lcom/sec/android/app/samsungapps/jp;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 748
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jp;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->d(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->onViewUpdate()V

    goto/16 :goto_0

    .line 754
    :pswitch_1
    iget v0, p0, Lcom/sec/android/app/samsungapps/jp;->a:I

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/sec/android/app/samsungapps/jp;->a:I

    if-ne v0, v5, :cond_5

    .line 755
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jp;->b:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;

    invoke-virtual {v0, p2, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;->setSetting(ILcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting$IAutoUpdateMainSettingResultListener;)Z

    .line 756
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jp;->c:Lcom/sec/android/app/samsungapps/widget/AutoUpdateOptionArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/AutoUpdateOptionArrayAdapter;->notifyDataSetChanged()V

    .line 757
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 758
    new-instance v1, Lcom/sec/android/app/samsungapps/ka;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/ka;-><init>(Lcom/sec/android/app/samsungapps/jp;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 764
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jp;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->d(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->onViewUpdate()V

    goto/16 :goto_0

    .line 766
    :cond_5
    iget v0, p0, Lcom/sec/android/app/samsungapps/jp;->a:I

    if-ne v0, v6, :cond_0

    .line 767
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jp;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getAutoUpdateManager()Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->isIdle()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jp;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->Is3GAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 768
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/jp;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;)V

    .line 769
    invoke-virtual {v0, v4}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setCanceledOnTouchOutside(Z)V

    .line 770
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/jp;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0801a2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 771
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/jp;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/jp;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f080153

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/samsungapps/uiutil/StringUtil;->replaceChineseString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 772
    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 774
    new-instance v1, Lcom/sec/android/app/samsungapps/kb;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/kb;-><init>(Lcom/sec/android/app/samsungapps/jp;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setBackKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 796
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/jp;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0802ef

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/kd;

    invoke-direct {v2, p0, p2}, Lcom/sec/android/app/samsungapps/kd;-><init>(Lcom/sec/android/app/samsungapps/jp;I)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 813
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/jp;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f08023d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/kf;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/kf;-><init>(Lcom/sec/android/app/samsungapps/jp;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setNegativeButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 829
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->show()V

    goto/16 :goto_0

    .line 831
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jp;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getAutoUpdateManager()Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->isIdle()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jp;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->IsWifiAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 832
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jp;->b:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;

    invoke-virtual {v0, p2, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;->setSetting(ILcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting$IAutoUpdateMainSettingResultListener;)Z

    .line 833
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jp;->c:Lcom/sec/android/app/samsungapps/widget/AutoUpdateOptionArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/AutoUpdateOptionArrayAdapter;->notifyDataSetChanged()V

    .line 834
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 835
    new-instance v1, Lcom/sec/android/app/samsungapps/jr;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/jr;-><init>(Lcom/sec/android/app/samsungapps/jp;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 841
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jp;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->d(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->onViewUpdate()V

    goto/16 :goto_0

    .line 844
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jp;->b:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;

    invoke-virtual {v0, p2, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;->setSetting(ILcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting$IAutoUpdateMainSettingResultListener;)Z

    .line 845
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jp;->c:Lcom/sec/android/app/samsungapps/widget/AutoUpdateOptionArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/AutoUpdateOptionArrayAdapter;->notifyDataSetChanged()V

    .line 846
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 847
    new-instance v1, Lcom/sec/android/app/samsungapps/js;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/js;-><init>(Lcom/sec/android/app/samsungapps/jp;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 853
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jp;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->d(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->onViewUpdate()V

    goto/16 :goto_0

    .line 859
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jp;->b:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;

    invoke-virtual {v0, p2, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting;->setSetting(ILcom/sec/android/app/samsungapps/vlibrary3/autoupdate/setting/AutoUpdateMainSetting$IAutoUpdateMainSettingResultListener;)Z

    .line 860
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jp;->c:Lcom/sec/android/app/samsungapps/widget/AutoUpdateOptionArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/AutoUpdateOptionArrayAdapter;->notifyDataSetChanged()V

    .line 861
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jp;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/jp;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->c(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;)V

    .line 862
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jp;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->d(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/SettingsListWidget;->onViewUpdate()V

    goto/16 :goto_0

    .line 661
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
