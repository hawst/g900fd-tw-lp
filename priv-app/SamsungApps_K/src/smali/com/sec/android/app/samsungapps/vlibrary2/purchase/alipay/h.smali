.class final Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/h;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayInitParam;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseCommandBuilder;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseCommandBuilder;)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/h;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseCommandBuilder;

    .line 66
    return-void
.end method


# virtual methods
.method public final getCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/h;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseCommandBuilder;->getSelectedCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    move-result-object v0

    return-object v0
.end method

.method public final getPaymentMethod()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/h;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseCommandBuilder;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 81
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getAlipayPaymentMethod()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getProductInfo()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/h;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseCommandBuilder;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    move-result-object v0

    return-object v0
.end method
