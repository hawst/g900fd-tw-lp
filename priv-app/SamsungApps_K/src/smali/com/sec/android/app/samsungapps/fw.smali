.class final Lcom/sec/android/app/samsungapps/fw;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;)V
    .locals 0

    .prologue
    .line 126
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/fw;->a:Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCommandResult(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 130
    if-eqz p1, :cond_2

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/fw;->a:Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isTestMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/fw;->a:Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;->b:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->createMenuItem(I)V

    .line 149
    :goto_0
    return-void

    .line 137
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/fw;->a:Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->ableToUseGlobalCreditCard()Z

    move-result v0

    if-ne v0, v2, :cond_1

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/fw;->a:Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;->b:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->setType(I)V

    .line 143
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/fw;->a:Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;->b:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->loadWidget()V

    goto :goto_0

    .line 140
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/fw;->a:Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;->b:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->setType(I)V

    goto :goto_1

    .line 147
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/fw;->a:Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;->finish()V

    goto :goto_0
.end method
