.class public Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;
.super Lcom/sec/android/app/samsungapps/ActionBarActivity;
.source "ProGuard"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCRegisterParam;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand$IPPCRegisterView;


# instance fields
.field private a:Z

.field private b:Landroid/widget/Spinner;

.field private c:Landroid/widget/CheckBox;

.field private d:Lcom/sec/android/app/samsungapps/ActivityHelper;

.field private e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 27
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->a:Z

    .line 30
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->b:Landroid/widget/Spinner;

    .line 31
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->c:Landroid/widget/CheckBox;

    .line 32
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->d:Lcom/sec/android/app/samsungapps/ActivityHelper;

    .line 33
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;

    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->d:Lcom/sec/android/app/samsungapps/ActivityHelper;

    const v1, 0x7f0c0261

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->getEditTextString(I)Ljava/lang/String;

    move-result-object v0

    .line 203
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->d:Lcom/sec/android/app/samsungapps/ActivityHelper;

    const v2, 0x7f0c0263

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/ActivityHelper;->getEditTextString(I)Ljava/lang/String;

    move-result-object v1

    .line 204
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->d:Lcom/sec/android/app/samsungapps/ActivityHelper;

    const v3, 0x7f0c0265

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/ActivityHelper;->getEditTextString(I)Ljava/lang/String;

    move-result-object v2

    .line 205
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->d:Lcom/sec/android/app/samsungapps/ActivityHelper;

    const v4, 0x7f0c0267

    invoke-virtual {v3, v4}, Lcom/sec/android/app/samsungapps/ActivityHelper;->getCheckBoxValue(I)Z

    move-result v3

    .line 206
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eq v0, v5, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eq v0, v5, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eq v0, v5, :cond_0

    if-ne v3, v5, :cond_0

    .line 209
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getPositiveButton(Landroid/app/Activity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 215
    :goto_0
    return-void

    .line 213
    :cond_0
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getPositiveButton(Landroid/app/Activity;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->a()V

    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->c:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;)Landroid/widget/Spinner;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->b:Landroid/widget/Spinner;

    return-object v0
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 161
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 167
    return-void
.end method

.method public getCardCorpSeq()Ljava/lang/String;
    .locals 2

    .prologue
    .line 292
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->b:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    .line 293
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;->getCardOperatorInfo()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IPPCCardOperatorInfo;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IPPCCardOperatorInfo;->getCardOperatorSeq(I)Ljava/lang/String;

    move-result-object v0

    .line 294
    return-object v0
.end method

.method public getCardNumber()Ljava/lang/String;
    .locals 2

    .prologue
    .line 314
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->d:Lcom/sec/android/app/samsungapps/ActivityHelper;

    const v1, 0x7f0c0263

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->getEditTextString(I)Ljava/lang/String;

    move-result-object v0

    .line 315
    return-object v0
.end method

.method public getCardPassword()Ljava/lang/String;
    .locals 2

    .prologue
    .line 307
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->d:Lcom/sec/android/app/samsungapps/ActivityHelper;

    const v1, 0x7f0c0265

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->getEditTextString(I)Ljava/lang/String;

    move-result-object v0

    .line 308
    return-object v0
.end method

.method public getCardValue()Ljava/lang/String;
    .locals 2

    .prologue
    .line 300
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->d:Lcom/sec/android/app/samsungapps/ActivityHelper;

    const v1, 0x7f0c0261

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->getEditTextString(I)Ljava/lang/String;

    move-result-object v0

    .line 301
    return-object v0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->a:Z

    if-nez v0, :cond_0

    .line 77
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->a:Z

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;->cancel()V

    .line 82
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;

    .line 86
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onBackPressed()V

    .line 87
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 38
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 40
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->readObject(Landroid/content/Intent;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;

    if-nez v0, :cond_0

    .line 43
    const-string v0, "RegisterPrepaidCardActivity::onCreate::Not Ready Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 44
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->finish()V

    .line 52
    :goto_0
    return-void

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;->invokeCompleted(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand$IPPCRegisterView;)V

    .line 51
    const v0, 0x7f0400ac

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->setContentView(I)V

    const v0, 0x7f0c0039

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    const v1, 0x7f0802ab

    const/4 v3, 0x1

    const v5, 0xb0003

    move-object v0, p0

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->setActionBarConfiguration(ILcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    new-instance v0, Lcom/sec/android/app/samsungapps/ActivityHelper;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/ActivityHelper;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->d:Lcom/sec/android/app/samsungapps/ActivityHelper;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->d:Lcom/sec/android/app/samsungapps/ActivityHelper;

    const v1, 0x7f0c0267

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->findCheckBox(I)Landroid/widget/CheckBox;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->c:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->d:Lcom/sec/android/app/samsungapps/ActivityHelper;

    const v1, 0x7f0c025e

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->findSpinner(I)Landroid/widget/Spinner;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->b:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->d:Lcom/sec/android/app/samsungapps/ActivityHelper;

    const v1, 0x7f0c0261

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->b(I)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->d:Lcom/sec/android/app/samsungapps/ActivityHelper;

    const v2, 0x7f0c0263

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/ActivityHelper;->b(I)Landroid/widget/EditText;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->d:Lcom/sec/android/app/samsungapps/ActivityHelper;

    const v3, 0x7f0c0265

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/ActivityHelper;->b(I)Landroid/widget/EditText;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->c:Landroid/widget/CheckBox;

    if-eqz v3, :cond_1

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    if-eqz v2, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->c:Landroid/widget/CheckBox;

    new-instance v4, Lcom/sec/android/app/samsungapps/gz;

    invoke-direct {v4, p0}, Lcom/sec/android/app/samsungapps/gz;-><init>(Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    invoke-virtual {v1, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    invoke-virtual {v2, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->d:Lcom/sec/android/app/samsungapps/ActivityHelper;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->b:Landroid/widget/Spinner;

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->setSpinnerStringArrayAdapter(Landroid/content/Context;Landroid/widget/Spinner;)Landroid/widget/ArrayAdapter;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;->getCardOperatorInfo()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IPPCCardOperatorInfo;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IPPCCardOperatorInfo;->getCardOperatorCount()I

    move-result v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/RequestRegisterPPCCardCommand;->getCardOperatorInfo()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IPPCCardOperatorInfo;

    move-result-object v3

    invoke-interface {v3, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IPPCCardOperatorInfo;->getCardOperatorName(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->d:Lcom/sec/android/app/samsungapps/ActivityHelper;

    invoke-virtual {v4, v3, v0}, Lcom/sec/android/app/samsungapps/ActivityHelper;->createStringID(Ljava/lang/String;I)Lcom/sec/android/app/samsungapps/ActivityHelper$StringID;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const v0, 0x7f0802e7

    const v1, 0x7f08023d

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->showPositiveNegativeButton(Landroid/app/Activity;II)V

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getPositiveButton(Landroid/app/Activity;)Landroid/widget/Button;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/ha;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/ha;-><init>(Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getNegativeButton(Landroid/app/Activity;)Landroid/widget/Button;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/hb;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/hb;-><init>(Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->a()V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->d:Lcom/sec/android/app/samsungapps/ActivityHelper;

    const v1, 0x7f0c0266

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->findButton(I)Landroid/widget/Button;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/hc;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/hc;-><init>(Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0802d0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->createInfoDialog(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    move-result-object v0

    const v1, 0x7f0802ef

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/gy;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/gy;-><init>(Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setPositiveButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->d:Lcom/sec/android/app/samsungapps/ActivityHelper;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->d:Lcom/sec/android/app/samsungapps/ActivityHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/ActivityHelper;->release()V

    .line 60
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->d:Lcom/sec/android/app/samsungapps/ActivityHelper;

    .line 63
    :cond_0
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->b:Landroid/widget/Spinner;

    .line 64
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->c:Landroid/widget/CheckBox;

    .line 66
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onDestroy()V

    .line 67
    return-void
.end method

.method public onRegisteredCard(Z)V
    .locals 1

    .prologue
    .line 283
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 285
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->finish()V

    .line 287
    :cond_0
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->d:Lcom/sec/android/app/samsungapps/ActivityHelper;

    if-nez v0, :cond_0

    .line 174
    const-string v0, "RegisterPrepaidCardActivity::onTextChanged::Not Ready Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 179
    :goto_0
    return-void

    .line 178
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/RegisterPrepaidCardActivity;->a()V

    goto :goto_0
.end method
