.class public Lcom/sec/android/app/samsungapps/vlibrary/net/HttpTextResponse;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static final BUFF_SIZE:I = 0x800


# instance fields
.field private mResponse:Lorg/apache/http/HttpResponse;

.field private resultString:Ljava/lang/String;

.field private statusCode:I


# direct methods
.method public constructor <init>(Lorg/apache/http/HttpResponse;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/HttpTextResponse;->mResponse:Lorg/apache/http/HttpResponse;

    .line 16
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/HttpTextResponse;->resultString:Ljava/lang/String;

    .line 21
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/HttpTextResponse;->mResponse:Lorg/apache/http/HttpResponse;

    .line 22
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/HttpTextResponse;->getResponseData()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/HttpTextResponse;->resultString:Ljava/lang/String;

    .line 23
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/HttpTextResponse;->statusCode:I

    .line 24
    if-eqz p1, :cond_0

    .line 26
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    .line 27
    if-eqz v0, :cond_0

    .line 29
    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/HttpTextResponse;->statusCode:I

    .line 32
    :cond_0
    return-void
.end method

.method private getResponseData()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/HttpTextResponse;->mResponse:Lorg/apache/http/HttpResponse;

    if-nez v0, :cond_0

    .line 43
    const-string v0, ""

    .line 93
    :goto_0
    return-object v0

    .line 46
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/HttpTextResponse;->mResponse:Lorg/apache/http/HttpResponse;

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    .line 52
    new-instance v1, Ljava/io/BufferedInputStream;

    const/16 v2, 0x800

    invoke-direct {v1, v0, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 53
    new-instance v2, Lorg/apache/http/util/ByteArrayBuffer;

    const/16 v0, 0x800

    invoke-direct {v2, v0}, Lorg/apache/http/util/ByteArrayBuffer;-><init>(I)V

    .line 54
    const/16 v0, 0x800

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 61
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    .line 67
    :goto_1
    const/4 v3, 0x0

    const/16 v4, 0x800

    :try_start_1
    invoke-virtual {v1, v0, v3, v4}, Ljava/io/BufferedInputStream;->read([BII)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v3

    .line 74
    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    .line 76
    invoke-virtual {v2, v0, v5, v3}, Lorg/apache/http/util/ByteArrayBuffer;->append([BII)V

    goto :goto_1

    .line 58
    :catch_0
    move-exception v0

    const-string v0, ""

    goto :goto_0

    .line 71
    :catch_1
    move-exception v0

    .line 82
    :cond_1
    invoke-virtual {v2}, Lorg/apache/http/util/ByteArrayBuffer;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 84
    const-string v0, ""

    goto :goto_0

    .line 89
    :cond_2
    :try_start_2
    new-instance v0, Ljava/lang/String;

    invoke-virtual {v2}, Lorg/apache/http/util/ByteArrayBuffer;->toByteArray()[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 93
    :catch_2
    move-exception v0

    const-string v0, ""

    goto :goto_0
.end method


# virtual methods
.method public getStatusCode()I
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/HttpTextResponse;->statusCode:I

    return v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/HttpTextResponse;->resultString:Ljava/lang/String;

    return-object v0
.end method

.method public isStatusCode200OK()Z
    .locals 2

    .prologue
    .line 104
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/HttpTextResponse;->statusCode:I

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
