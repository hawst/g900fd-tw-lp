.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListCreator;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator$IListListMapContainer;


# instance fields
.field private mCuratedProductList:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;

.field private mCuratedProductListInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;

.field private mListItemCount:I

.field private map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListCreator;->mCuratedProductList:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;

    .line 9
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListCreator;->mCuratedProductListInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;

    .line 10
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListCreator;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 11
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListCreator;->mListItemCount:I

    .line 15
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListCreator;->mCuratedProductListInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;

    .line 16
    return-void
.end method

.method private setCuratedProductInfoData(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 3

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListCreator;->mCuratedProductList:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;

    const-string v1, "listTitle"

    invoke-virtual {p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;->setListTitle(Ljava/lang/String;)V

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListCreator;->mCuratedProductList:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;

    const-string v1, "listDescription"

    invoke-virtual {p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;->setListDescription(Ljava/lang/String;)V

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListCreator;->mCuratedProductList:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;

    const-string v1, "index"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;->setIndex(I)V

    .line 80
    return-void
.end method


# virtual methods
.method public addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListCreator;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListCreator;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 49
    :cond_0
    return-void
.end method

.method public clearContainer()V
    .locals 0

    .prologue
    .line 21
    return-void
.end method

.method public closeList()V
    .locals 2

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListCreator;->mCuratedProductListInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;

    if-eqz v0, :cond_0

    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListCreator;->mCuratedProductList:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListCreator;->mCuratedProductList:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 29
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListCreator;->mCuratedProductListInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListCreator;->mCuratedProductList:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;->add(Ljava/lang/Object;)Z

    .line 32
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListCreator;->mCuratedProductList:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;

    .line 33
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListCreator;->mListItemCount:I

    .line 34
    return-void
.end method

.method public closeMap()V
    .locals 2

    .prologue
    .line 53
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListCreator;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 54
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListCreator;->mCuratedProductList:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;

    if-eqz v1, :cond_0

    .line 56
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListCreator;->mCuratedProductList:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;->add(Ljava/lang/Object;)Z

    .line 57
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListCreator;->mListItemCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListCreator;->mListItemCount:I

    .line 58
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListCreator;->mListItemCount:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListCreator;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListCreator;->setCuratedProductInfoData(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 63
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListCreator;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 64
    return-void
.end method

.method public openList()V
    .locals 1

    .prologue
    .line 38
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListCreator;->mCuratedProductList:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductList;

    .line 39
    return-void
.end method

.method public openMap()V
    .locals 1

    .prologue
    .line 68
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListCreator;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 69
    return-void
.end method

.method public setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 73
    return-void
.end method
