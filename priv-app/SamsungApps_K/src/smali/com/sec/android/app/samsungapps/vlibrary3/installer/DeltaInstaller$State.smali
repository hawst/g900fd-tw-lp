.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum DELTACHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;

.field public static final enum DELTAPATCH:Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;

.field public static final enum IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;

.field public static final enum NORMAL_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 25
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;

    const-string v1, "DELTACHECK"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;->DELTACHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;

    const-string v1, "NORMAL_INSTALL"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;->NORMAL_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;

    const-string v1, "DELTAPATCH"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;->DELTAPATCH:Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;

    .line 24
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;->DELTACHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;->NORMAL_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;->DELTAPATCH:Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;

    return-object v0
.end method
