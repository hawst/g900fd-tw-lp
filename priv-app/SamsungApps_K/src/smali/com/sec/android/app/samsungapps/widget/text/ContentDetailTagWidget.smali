.class public Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;
.super Lcom/sec/android/app/samsungapps/widget/CommonWidget;
.source "ProGuard"


# instance fields
.field private a:Ljava/util/ArrayList;

.field private b:Ljava/util/ArrayList;

.field private d:Landroid/widget/ImageView;

.field private e:Z

.field private f:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;

.field private g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;)V

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->e:Z

    .line 32
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->mContext:Landroid/content/Context;

    .line 33
    const v0, 0x7f040038

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->initView(Landroid/content/Context;I)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->e:Z

    .line 38
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->mContext:Landroid/content/Context;

    .line 39
    const v0, 0x7f040038

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->initView(Landroid/content/Context;I)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->e:Z

    .line 45
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->mContext:Landroid/content/Context;

    .line 46
    const v0, 0x7f040038

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->initView(Landroid/content/Context;I)V

    .line 47
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private a()V
    .locals 13

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->f:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;

    if-nez v0, :cond_1

    .line 242
    :cond_0
    :goto_0
    return-void

    .line 113
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->f:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->getVsellerTag()Ljava/lang/String;

    move-result-object v0

    .line 114
    const-string v1, "\uff0c"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "\u3001"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 115
    :cond_2
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->convertOtherDivideComma(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 118
    :cond_3
    if-nez v0, :cond_4

    .line 120
    const v0, 0x7f0c0103

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 124
    :cond_4
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->f:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->getVsellerTag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_7

    array-length v0, v7

    if-lez v0, :cond_7

    .line 127
    const v0, 0x7f0c0107

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 128
    if-eqz v0, :cond_0

    .line 132
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViewsInLayout()V

    .line 133
    new-instance v4, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->mContext:Landroid/content/Context;

    invoke-direct {v4, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 134
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->a:Ljava/util/ArrayList;

    .line 135
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->b:Ljava/util/ArrayList;

    .line 138
    const/4 v1, 0x2

    new-array v3, v1, [I

    .line 139
    const v1, 0x7f0c0103

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 140
    if-eqz v1, :cond_0

    .line 144
    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->getLocationOnScreen([I)V

    .line 146
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    const/4 v2, 0x0

    aget v2, v3, v2

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->g:I

    .line 148
    const/4 v2, 0x0

    .line 149
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    .line 150
    const/high16 v5, 0x41800000    # 16.0f

    mul-float/2addr v5, v1

    float-to-int v5, v5

    .line 151
    const/high16 v6, 0x40e00000    # 7.0f

    mul-float/2addr v1, v6

    float-to-int v8, v1

    .line 152
    const/4 v1, 0x0

    .line 154
    iget-object v6, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    iget v6, v6, Landroid/util/DisplayMetrics;->widthPixels:I

    const/4 v9, 0x0

    aget v3, v3, v9

    sub-int v3, v6, v3

    mul-int/lit8 v5, v5, 0x2

    sub-int/2addr v3, v5

    iput v3, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->g:I

    .line 156
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x1

    const/4 v6, -0x2

    invoke-direct {v3, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 158
    invoke-virtual {v4, v3}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 159
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 160
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 162
    const/4 v3, 0x0

    move v5, v1

    move v1, v2

    move-object v2, v4

    :goto_1
    array-length v4, v7

    if-ge v3, v4, :cond_8

    .line 163
    aget-object v4, v7, v3

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    .line 165
    const/4 v4, 0x1

    if-ne v1, v4, :cond_a

    .line 166
    const/4 v4, 0x0

    .line 167
    new-instance v6, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->mContext:Landroid/content/Context;

    invoke-direct {v6, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 169
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v10, -0x2

    invoke-direct {v1, v2, v10}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 171
    const/4 v2, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v1, v2, v8, v10, v11}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 172
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 173
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 174
    invoke-virtual {v6, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 175
    const/16 v1, 0x8

    invoke-virtual {v6, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 178
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 179
    const v2, 0x7f040037

    const/4 v10, 0x0

    invoke-virtual {v1, v2, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Landroid/widget/LinearLayout;

    .line 180
    const v1, 0x7f0c0102

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 181
    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f080097

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 184
    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getPaddingRight()I

    move-result v10

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v11

    add-int/2addr v10, v11

    .line 186
    invoke-virtual {v1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v11

    .line 187
    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v12

    invoke-virtual {v11, v12}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 189
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v1

    float-to-int v1, v1

    .line 190
    iget v11, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->g:I

    sub-int/2addr v11, v10

    if-lt v1, v11, :cond_5

    .line 191
    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->g:I

    sub-int/2addr v1, v10

    .line 193
    :cond_5
    add-int/2addr v1, v5

    .line 194
    add-int/2addr v1, v10

    .line 195
    iget v5, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->g:I

    if-le v1, v5, :cond_6

    .line 197
    const/4 v4, 0x1

    .line 198
    const/4 v1, 0x0

    .line 199
    add-int/lit8 v2, v3, -0x1

    move v3, v4

    .line 162
    :goto_3
    add-int/lit8 v2, v2, 0x1

    move v5, v1

    move v1, v3

    move v3, v2

    move-object v2, v6

    goto/16 :goto_1

    .line 202
    :cond_6
    new-instance v5, Lcom/sec/android/app/samsungapps/widget/text/n;

    invoke-direct {v5, p0, v9}, Lcom/sec/android/app/samsungapps/widget/text/n;-><init>(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 208
    invoke-virtual {v6, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 209
    iget-object v5, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->a:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 212
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x1

    invoke-direct {v2, v8, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 214
    new-instance v5, Landroid/view/View;

    iget-object v9, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->mContext:Landroid/content/Context;

    invoke-direct {v5, v9}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 215
    invoke-virtual {v5, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 216
    invoke-virtual {v6, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 217
    add-int/2addr v1, v8

    move v2, v3

    move v3, v4

    goto :goto_3

    .line 220
    :cond_7
    const v0, 0x7f0c0103

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 224
    :cond_8
    const v0, 0x7f0c0109

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 225
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->b:Ljava/util/ArrayList;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_9

    .line 226
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 227
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08027b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 228
    const v1, 0x7f0c0108

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 229
    const v1, 0x7f0c007a

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->d:Landroid/widget/ImageView;

    .line 230
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->d:Landroid/widget/ImageView;

    iget-boolean v2, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->e:Z

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 231
    new-instance v1, Lcom/sec/android/app/samsungapps/widget/text/o;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/text/o;-><init>(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 239
    :cond_9
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 240
    const v0, 0x7f0c0108

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    :cond_a
    move v4, v1

    move-object v6, v2

    goto/16 :goto_2
.end method

.method private a(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 245
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->d:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    .line 260
    :cond_0
    :goto_0
    return-void

    .line 248
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 250
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 251
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 252
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1

    .line 255
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 256
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_2

    .line 258
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;Z)Z
    .locals 0

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->e:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;Z)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->a(Z)V

    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;)Z
    .locals 1

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->e:Z

    return v0
.end method


# virtual methods
.method public convertOtherDivideComma(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 93
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 94
    const-string v2, ","

    .line 95
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 96
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const v4, 0xff0c

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eq v3, v4, :cond_0

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/16 v4, 0x2c

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eq v3, v4, :cond_0

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/16 v4, 0x3001

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ne v3, v4, :cond_1

    .line 99
    :cond_0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 95
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 101
    :cond_1
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 105
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public loadWidget()V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->f:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->f:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->getVsellerTag()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->f:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;->getVsellerTag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 74
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->updateWidget()V

    .line 78
    :goto_0
    return-void

    .line 76
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 264
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 266
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->a:Ljava/util/ArrayList;

    .line 268
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 269
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 270
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->b:Ljava/util/ArrayList;

    .line 272
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->a()V

    .line 273
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->e:Z

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->a(Z)V

    .line 274
    return-void
.end method

.method public refreshWidget()V
    .locals 0

    .prologue
    .line 90
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 59
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->a:Ljava/util/ArrayList;

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 63
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->b:Ljava/util/ArrayList;

    .line 65
    :cond_1
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->d:Landroid/widget/ImageView;

    .line 66
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->f:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;

    .line 67
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->release()V

    .line 68
    return-void
.end method

.method public setWidgetData(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 51
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 52
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailRelated()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailRelated;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->f:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;

    .line 53
    return-void
.end method

.method public updateWidget()V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->a()V

    .line 84
    return-void
.end method
