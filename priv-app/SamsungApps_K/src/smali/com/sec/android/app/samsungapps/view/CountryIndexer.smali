.class public final Lcom/sec/android/app/samsungapps/view/CountryIndexer;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/widget/SectionIndexer;


# instance fields
.field private a:[Ljava/lang/String;

.field private b:[I


# direct methods
.method public constructor <init>(Ljava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/samsungapps/view/CountryIndexer;-><init>(Ljava/util/ArrayList;Z)V

    .line 24
    return-void
.end method

.method public constructor <init>(Ljava/util/ArrayList;Z)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    if-nez p1, :cond_0

    .line 55
    :goto_0
    return-void

    .line 31
    :cond_0
    if-ne p2, v7, :cond_1

    .line 32
    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 35
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 37
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CountryIndexer;->b:[I

    .line 39
    const/4 v1, -0x1

    .line 41
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v2, v3

    .line 42
    :goto_1
    if-ge v2, v5, :cond_2

    .line 43
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SectionListItemView;

    .line 44
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SectionListItemView;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v3, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 46
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 47
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 48
    add-int/lit8 v0, v1, 0x1

    .line 50
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/CountryIndexer;->b:[I

    aput v0, v1, v2

    .line 42
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    .line 53
    :cond_2
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CountryIndexer;->a:[Ljava/lang/String;

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CountryIndexer;->a:[Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_2
.end method


# virtual methods
.method public final getPositionForSection(I)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 59
    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/CountryIndexer;->b:[I

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 60
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/CountryIndexer;->b:[I

    aget v2, v2, v0

    if-ne v2, p1, :cond_0

    .line 64
    :goto_1
    return v0

    .line 59
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 64
    goto :goto_1
.end method

.method public final getSectionForPosition(I)I
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CountryIndexer;->b:[I

    aget v0, v0, p1

    return v0
.end method

.method public final getSections()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CountryIndexer;->a:[Ljava/lang/String;

    return-object v0
.end method
