.class public Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider$IButtonDataGetter;

.field private b:Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider$IButtonDeciderObserver;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider$IButtonDataGetter;Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider$IButtonDeciderObserver;)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider;->a:Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider$IButtonDataGetter;

    .line 10
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider;->b:Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider$IButtonDeciderObserver;

    .line 11
    return-void
.end method


# virtual methods
.method public checkButtonState()V
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider;->a:Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider$IButtonDataGetter;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider$IButtonDataGetter;->isAllPage()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider;->a:Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider$IButtonDataGetter;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider$IButtonDataGetter;->getItemCount()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider;->a:Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider$IButtonDataGetter;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider$IButtonDataGetter;->isVzwAtSamsungUpdates()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 17
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider;->b:Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider$IButtonDeciderObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider$IButtonDeciderObserver;->hideButton()V

    .line 28
    :goto_0
    return-void

    .line 20
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider;->a:Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider$IButtonDataGetter;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider$IButtonDataGetter;->getInstallingCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider;->b:Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider$IButtonDeciderObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider$IButtonDeciderObserver;->showCancelAllButton()V

    goto :goto_0

    .line 26
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider;->b:Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider$IButtonDeciderObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/view/downloadable/ButtonDecider$IButtonDeciderObserver;->showInstallAllButton()V

    goto :goto_0
.end method
