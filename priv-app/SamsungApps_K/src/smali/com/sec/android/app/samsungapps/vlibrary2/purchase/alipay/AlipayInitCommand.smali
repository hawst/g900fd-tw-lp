.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInitCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _AlipayIntResult:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayIntResult;

.field private _CIAlipayInitParam:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/h;

.field private _IPurchaseCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseCommandBuilder;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayIntResult;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseCommandBuilder;)V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInitCommand;->_AlipayIntResult:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayIntResult;

    .line 29
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInitCommand;->_IPurchaseCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseCommandBuilder;

    .line 30
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/h;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInitCommand;->_IPurchaseCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseCommandBuilder;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/h;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseCommandBuilder;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInitCommand;->_CIAlipayInitParam:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/h;

    .line 31
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInitCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayIntResult;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInitCommand;->_AlipayIntResult:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayIntResult;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInitCommand;Z)V
    .locals 0

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInitCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInitCommand;Z)V
    .locals 0

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInitCommand;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 3

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInitCommand;->_CIAlipayInitParam:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/h;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInitCommand;->_AlipayIntResult:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayIntResult;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/d;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInitCommand;)V

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInitCommand;->requestInit(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayInitParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    .line 47
    return-void
.end method

.method protected requestInit(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayInitParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V
    .locals 2

    .prologue
    .line 50
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->initAlipayPurchase(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayInitParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 52
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInitCommand;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 54
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 55
    return-void
.end method
