.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field public confirmMsg:Ljava/lang/String;

.field private mPSMSConfirmResult:Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSConfirmResult;

.field public message:Ljava/lang/String;

.field messages:Ljava/util/ArrayList;

.field public orderID:Ljava/lang/String;

.field public paymentID:Ljava/lang/String;

.field public productID:Ljava/lang/String;

.field public randomKey:Ljava/lang/String;

.field public responseTime:I

.field public retryCount:I

.field public sendSMS:I

.field public shortCode:Ljava/lang/String;

.field shortCodes:Ljava/util/ArrayList;

.field public tncMsg:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;->shortCodes:Ljava/util/ArrayList;

    .line 18
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;->messages:Ljava/util/ArrayList;

    return-void
.end method

.method private createMessages()V
    .locals 5

    .prologue
    .line 87
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;->messages:Ljava/util/ArrayList;

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;->message:Ljava/lang/String;

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 89
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 91
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;->messages:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 89
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 93
    :cond_0
    return-void
.end method

.method private createShortCodes()V
    .locals 5

    .prologue
    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;->shortCodes:Ljava/util/ArrayList;

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;->shortCode:Ljava/lang/String;

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 56
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 58
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;->shortCodes:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 56
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 60
    :cond_0
    return-void
.end method


# virtual methods
.method public existConfirmMsg()Z
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;->confirmMsg:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;->confirmMsg:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public existTnc()Z
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;->tncMsg:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;->tncMsg:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getConfirmMsg()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;->confirmMsg:Ljava/lang/String;

    return-object v0
.end method

.method public getConfirmResult()Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSConfirmResult;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;->mPSMSConfirmResult:Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSConfirmResult;

    return-object v0
.end method

.method public getMessage(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;->messages:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 80
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;->createMessages()V

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;->messages:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getMessageCount()I
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;->message:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;->message:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 65
    :cond_0
    const/4 v0, 0x0

    .line 73
    :goto_0
    return v0

    .line 68
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;->messages:Ljava/util/ArrayList;

    if-nez v0, :cond_2

    .line 70
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;->createMessages()V

    .line 73
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;->messages:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getShortCode(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;->shortCodes:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 46
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;->createShortCodes()V

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;->shortCodes:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getShortCodeCount()I
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;->shortCode:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;->shortCode:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 31
    :cond_0
    const/4 v0, 0x0

    .line 39
    :goto_0
    return v0

    .line 34
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;->shortCodes:Ljava/util/ArrayList;

    if-nez v0, :cond_2

    .line 36
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;->createShortCodes()V

    .line 39
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;->shortCodes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getTnc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;->tncMsg:Ljava/lang/String;

    return-object v0
.end method

.method public setConfirmResult(Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSConfirmResult;)V
    .locals 0

    .prologue
    .line 22
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;->mPSMSConfirmResult:Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSConfirmResult;

    .line 23
    return-void
.end method
