.class public Lcom/sec/android/app/samsungapps/MenuItemList;
.super Ljava/util/ArrayList;
.source "ProGuard"


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/MenuItemList$MenuItemListObserver;

.field private b:Lcom/sec/android/app/samsungapps/MenuItem;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 133
    return-void
.end method


# virtual methods
.method public add(ILcom/sec/android/app/samsungapps/MenuItem;)V
    .locals 1

    .prologue
    .line 27
    if-nez p2, :cond_1

    .line 34
    :cond_0
    :goto_0
    return-void

    .line 30
    :cond_1
    invoke-super {p0, p1, p2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 31
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/MenuItemList;->a:Lcom/sec/android/app/samsungapps/MenuItemList$MenuItemListObserver;

    if-eqz v0, :cond_0

    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/MenuItemList;->a:Lcom/sec/android/app/samsungapps/MenuItemList$MenuItemListObserver;

    invoke-interface {v0, p2}, Lcom/sec/android/app/samsungapps/MenuItemList$MenuItemListObserver;->menuItemAdded(Lcom/sec/android/app/samsungapps/MenuItem;)V

    goto :goto_0
.end method

.method public bridge synthetic add(ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 5
    check-cast p2, Lcom/sec/android/app/samsungapps/MenuItem;

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/MenuItemList;->add(ILcom/sec/android/app/samsungapps/MenuItem;)V

    return-void
.end method

.method public add(Lcom/sec/android/app/samsungapps/MenuItem;)Z
    .locals 2

    .prologue
    .line 39
    if-nez p1, :cond_1

    .line 40
    const/4 v0, 0x0

    .line 46
    :cond_0
    :goto_0
    return v0

    .line 42
    :cond_1
    invoke-super {p0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result v0

    .line 43
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/MenuItemList;->a:Lcom/sec/android/app/samsungapps/MenuItemList$MenuItemListObserver;

    if-eqz v1, :cond_0

    .line 44
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/MenuItemList;->a:Lcom/sec/android/app/samsungapps/MenuItemList$MenuItemListObserver;

    invoke-interface {v1, p1}, Lcom/sec/android/app/samsungapps/MenuItemList$MenuItemListObserver;->menuItemAdded(Lcom/sec/android/app/samsungapps/MenuItem;)V

    goto :goto_0
.end method

.method public bridge synthetic add(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 5
    check-cast p1, Lcom/sec/android/app/samsungapps/MenuItem;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/MenuItemList;->add(Lcom/sec/android/app/samsungapps/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public findItem(Lcom/sec/android/app/samsungapps/MenuItem;)Lcom/sec/android/app/samsungapps/MenuItem;
    .locals 2

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/MenuItemList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/MenuItem;

    .line 115
    if-ne v0, p1, :cond_0

    .line 119
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public findItemById(I)Lcom/sec/android/app/samsungapps/MenuItem;
    .locals 3

    .prologue
    .line 124
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/MenuItemList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/MenuItem;

    .line 126
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/MenuItem;->getID()I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 130
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSel()Lcom/sec/android/app/samsungapps/MenuItem;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/MenuItemList;->b:Lcom/sec/android/app/samsungapps/MenuItem;

    if-nez v0, :cond_1

    .line 103
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/MenuItemList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 104
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/MenuItemList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/MenuItem;

    .line 108
    :goto_0
    return-object v0

    .line 106
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 108
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/MenuItemList;->b:Lcom/sec/android/app/samsungapps/MenuItem;

    goto :goto_0
.end method

.method public getSelId()I
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/MenuItemList;->getSel()Lcom/sec/android/app/samsungapps/MenuItem;

    move-result-object v0

    .line 93
    if-nez v0, :cond_0

    .line 94
    const/4 v0, 0x0

    .line 96
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/MenuItem;->getID()I

    move-result v0

    goto :goto_0
.end method

.method public release()V
    .locals 0

    .prologue
    .line 16
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/MenuItemList;->clear()V

    .line 17
    return-void
.end method

.method public remove(I)Lcom/sec/android/app/samsungapps/MenuItem;
    .locals 2

    .prologue
    .line 52
    invoke-super {p0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/MenuItem;

    .line 53
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/MenuItemList;->a:Lcom/sec/android/app/samsungapps/MenuItemList$MenuItemListObserver;

    if-eqz v1, :cond_0

    .line 54
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/MenuItemList;->a:Lcom/sec/android/app/samsungapps/MenuItemList$MenuItemListObserver;

    invoke-interface {v1, v0}, Lcom/sec/android/app/samsungapps/MenuItemList$MenuItemListObserver;->menuItemRemoved(Lcom/sec/android/app/samsungapps/MenuItem;)V

    .line 56
    :cond_0
    return-object v0
.end method

.method public bridge synthetic remove(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/MenuItemList;->remove(I)Lcom/sec/android/app/samsungapps/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 62
    invoke-super {p0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v1

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/MenuItemList;->a:Lcom/sec/android/app/samsungapps/MenuItemList$MenuItemListObserver;

    if-eqz v0, :cond_0

    .line 64
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/MenuItemList;->a:Lcom/sec/android/app/samsungapps/MenuItemList$MenuItemListObserver;

    move-object v0, p1

    check-cast v0, Lcom/sec/android/app/samsungapps/MenuItem;

    invoke-interface {v2, v0}, Lcom/sec/android/app/samsungapps/MenuItemList$MenuItemListObserver;->menuItemRemoved(Lcom/sec/android/app/samsungapps/MenuItem;)V

    .line 66
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/MenuItemList;->getSel()Lcom/sec/android/app/samsungapps/MenuItem;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 68
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/MenuItemList;->setSel(Lcom/sec/android/app/samsungapps/MenuItem;)Z

    .line 70
    :cond_1
    return v1
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/MenuItemList$MenuItemListObserver;)V
    .locals 0

    .prologue
    .line 21
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/MenuItemList;->a:Lcom/sec/android/app/samsungapps/MenuItemList$MenuItemListObserver;

    .line 22
    return-void
.end method

.method public setSel(Lcom/sec/android/app/samsungapps/MenuItem;)Z
    .locals 1

    .prologue
    .line 75
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/MenuItemList;->findItem(Lcom/sec/android/app/samsungapps/MenuItem;)Lcom/sec/android/app/samsungapps/MenuItem;

    move-result-object v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 76
    const/4 v0, 0x0

    .line 82
    :goto_0
    return v0

    .line 78
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/MenuItemList;->b:Lcom/sec/android/app/samsungapps/MenuItem;

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/MenuItemList;->a:Lcom/sec/android/app/samsungapps/MenuItemList$MenuItemListObserver;

    if-eqz v0, :cond_1

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/MenuItemList;->a:Lcom/sec/android/app/samsungapps/MenuItemList$MenuItemListObserver;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/MenuItemList$MenuItemListObserver;->selChanged(Lcom/sec/android/app/samsungapps/MenuItem;)V

    .line 82
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setSelById(I)V
    .locals 1

    .prologue
    .line 87
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/MenuItemList;->findItemById(I)Lcom/sec/android/app/samsungapps/MenuItem;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/MenuItemList;->setSel(Lcom/sec/android/app/samsungapps/MenuItem;)Z

    .line 88
    return-void
.end method
