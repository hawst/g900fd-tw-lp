.class public Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/IDownloadState;


# instance fields
.field private _DLSize:I

.field private _FakeModel:Ljava/lang/String;

.field private _GUID:Ljava/lang/String;

.field private _LoadType:Ljava/lang/String;

.field private _Name:Ljava/lang/String;

.field private _Object:Ljava/lang/Object;

.field private _Observers:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

.field private _ProductID:Ljava/lang/String;

.field private _State:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

.field private _TotalSize:I

.field private _bCancellable:Z

.field private _bDontOpenDetailPage:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->_Observers:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    .line 17
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->_bDontOpenDetailPage:Z

    .line 19
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->_bCancellable:Z

    .line 28
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->_ProductID:Ljava/lang/String;

    .line 29
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->_GUID:Ljava/lang/String;

    .line 30
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->_Name:Ljava/lang/String;

    .line 31
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->_LoadType:Ljava/lang/String;

    .line 32
    iput-boolean p6, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->_bDontOpenDetailPage:Z

    .line 33
    iput-object p5, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->_FakeModel:Ljava/lang/String;

    .line 34
    return-void
.end method


# virtual methods
.method public addObserver(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateObserver;)V
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->_Observers:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;->clone()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateObserver;

    .line 102
    if-ne v0, p1, :cond_0

    .line 108
    :goto_0
    return-void

    .line 107
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->_Observers:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getDontOpenDetailPage()Z
    .locals 1

    .prologue
    .line 171
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->_bDontOpenDetailPage:Z

    return v0
.end method

.method public getDownloadedSize()I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->_DLSize:I

    return v0
.end method

.method public getFakeModel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->_FakeModel:Ljava/lang/String;

    return-object v0
.end method

.method public getGUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->_GUID:Ljava/lang/String;

    return-object v0
.end method

.method public getLoadType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->_LoadType:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->_Name:Ljava/lang/String;

    return-object v0
.end method

.method public getProductID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->_ProductID:Ljava/lang/String;

    return-object v0
.end method

.method public getState()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->_State:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    return-object v0
.end method

.method public getTagStr()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->_Object:Ljava/lang/Object;

    return-object v0
.end method

.method public getTotalSize()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->_TotalSize:I

    return v0
.end method

.method public isCancellable()Z
    .locals 1

    .prologue
    .line 182
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->_bCancellable:Z

    return v0
.end method

.method protected notifyChanged()V
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->_Observers:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;->clone()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateObserver;

    .line 119
    invoke-interface {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateObserver;->onDLStateChanged(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V

    goto :goto_0

    .line 121
    :cond_0
    return-void
.end method

.method public removeAllObserver()V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->_Observers:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;->clear()V

    .line 126
    return-void
.end method

.method public removeObserver(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateObserver;)V
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->_Observers:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;->remove(Ljava/lang/Object;)Z

    .line 113
    return-void
.end method

.method public setCancellable(Z)V
    .locals 0

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->_bCancellable:Z

    .line 39
    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->_State:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    .line 73
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->notifyChanged()V

    .line 74
    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;I)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->_State:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    .line 85
    iput p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->_DLSize:I

    .line 86
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->notifyChanged()V

    .line 87
    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;II)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->_State:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    .line 78
    iput p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->_DLSize:I

    .line 79
    iput p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->_TotalSize:I

    .line 80
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->notifyChanged()V

    .line 81
    return-void
.end method

.method public setTag(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 154
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->_Object:Ljava/lang/Object;

    .line 155
    return-void
.end method

.method public setTotalSize(I)V
    .locals 0

    .prologue
    .line 91
    iput p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->_TotalSize:I

    .line 92
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 164
    const-string v0, "ProductID:%s GUID:%s Name:%s State:%s"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->_ProductID:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->_GUID:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->_Name:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->_State:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 166
    return-object v0
.end method
