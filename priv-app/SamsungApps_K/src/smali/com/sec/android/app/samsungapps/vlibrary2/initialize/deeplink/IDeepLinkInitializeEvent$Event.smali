.class public final enum Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;

.field public static final enum CHECK_URL_NOT_VALID:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;

.field public static final enum CHECK_URL_VALID:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;

.field public static final enum REQUEST_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;

.field public static final enum REQUEST_SUCCEED:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;

.field public static final enum USER_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 6
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;

    const-string v1, "CHECK_URL_NOT_VALID"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;->CHECK_URL_NOT_VALID:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;

    .line 7
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;

    const-string v1, "CHECK_URL_VALID"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;->CHECK_URL_VALID:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;

    .line 8
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;

    const-string v1, "REQUEST_FAILED"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;->REQUEST_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;

    .line 9
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;

    const-string v1, "REQUEST_SUCCEED"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;->REQUEST_SUCCEED:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;

    .line 10
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;

    const-string v1, "USER_CANCEL"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;->USER_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;

    .line 4
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;->CHECK_URL_NOT_VALID:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;->CHECK_URL_VALID:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;->REQUEST_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;->REQUEST_SUCCEED:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;->USER_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;
    .locals 1

    .prologue
    .line 4
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;
    .locals 1

    .prologue
    .line 4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/IDeepLinkInitializeEvent$Event;

    return-object v0
.end method
