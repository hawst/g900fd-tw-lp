.class public Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;
.super Lcom/sec/android/app/samsungapps/widget/CommonWidget;
.source "ProGuard"


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;)V

    .line 31
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->mContext:Landroid/content/Context;

    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->mContext:Landroid/content/Context;

    const v1, 0x7f040036

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->initView(Landroid/content/Context;I)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->mContext:Landroid/content/Context;

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->mContext:Landroid/content/Context;

    const v1, 0x7f040036

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->initView(Landroid/content/Context;I)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->mContext:Landroid/content/Context;

    .line 44
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->mContext:Landroid/content/Context;

    const v1, 0x7f040036

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->initView(Landroid/content/Context;I)V

    .line 45
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private a(Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 255
    invoke-virtual {p0, p2}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 256
    if-nez v0, :cond_1

    .line 257
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "::TextView is null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->v(Ljava/lang/String;)V

    .line 277
    :cond_0
    :goto_0
    return-void

    .line 261
    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 262
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "::string is empty"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 266
    :cond_2
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 267
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 268
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "<u>"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</u>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 269
    const v1, 0x7f0c00fc

    if-ne p2, v1, :cond_3

    .line 271
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0800cf

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 273
    :cond_3
    const v1, 0x7f0c00fe

    if-ne p2, v1, :cond_0

    .line 275
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0800c3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method private a(Ljava/lang/String;II)V
    .locals 3

    .prologue
    .line 215
    invoke-virtual {p0, p2}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 216
    if-nez v0, :cond_0

    .line 217
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "::TextView is null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->v(Ljava/lang/String;)V

    .line 230
    :goto_0
    return-void

    .line 221
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 222
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 223
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "::string is empty"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 227
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 228
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 229
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;)Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public loadWidget()V
    .locals 0

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->updateWidget()V

    .line 63
    return-void
.end method

.method public refreshWidget()V
    .locals 0

    .prologue
    .line 78
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    .line 57
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->release()V

    .line 58
    return-void
.end method

.method public setWidgetData(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 49
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 50
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->b:Ljava/lang/String;

    .line 51
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailOverview()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    .line 52
    return-void
.end method

.method public updateWidget()V
    .locals 7

    .prologue
    const v6, 0x7f0c00f7

    const/4 v5, 0x1

    const/16 v4, 0x8

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    if-nez v0, :cond_0

    .line 74
    :goto_0
    return-void

    .line 70
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->isLinkApp()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->isDisplaySellerInfo()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isUncStore()Z

    move-result v0

    if-ne v0, v5, :cond_5

    :cond_1
    const v0, 0x7f0c00ef

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 71
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVsupportEmail()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVsupportEmail()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_9

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVsellerUrl()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVsellerUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_9

    :cond_3
    const v0, 0x7f0c00f9

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 72
    :goto_2
    const v0, 0x7f0c00ff

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isUncStore()Z

    move-result v1

    if-eq v1, v5, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v1

    if-ne v1, v5, :cond_e

    :cond_4
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 70
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVsellerName()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0c00f1

    const v2, 0x7f0802fd

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->a(Ljava/lang/String;II)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVsellerTradeName()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVrepresentation()Ljava/lang/String;

    move-result-object v2

    const v0, 0x7f0c00f2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "::TextView is null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->v(Ljava/lang/String;)V

    :goto_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVsellerRegisterNum()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0c00f3

    const v2, 0x7f080342

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->a(Ljava/lang/String;II)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVreportNum()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0c00f4

    const v2, 0x7f080343

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->a(Ljava/lang/String;II)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVsellerNum()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0c00f5

    const v2, 0x7f080345

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->a(Ljava/lang/String;II)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVsellerEmail()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0c00f6

    const v2, 0x7f080275

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->a(Ljava/lang/String;II)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->isDisplaySellerLocation()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVsellerLocation()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f080346

    invoke-direct {p0, v0, v6, v1}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->a(Ljava/lang/String;II)V

    goto/16 :goto_1

    :cond_6
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-ne v3, v5, :cond_7

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-ne v3, v5, :cond_7

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "::both values are empty"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->v(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_7
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080341

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_8
    invoke-virtual {p0, v6}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 71
    :cond_9
    const v0, 0x7f0c00fd

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVsupportEmail()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVsupportEmail()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_c

    :cond_a
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_4
    const v0, 0x7f0c00fb

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVsellerUrl()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVsellerUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_d

    :cond_b
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_2

    :cond_c
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVsupportEmail()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0c00fe

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->a(Ljava/lang/String;I)V

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/text/l;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/text/l;-><init>(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_4

    :cond_d
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;->getVsellerUrl()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0c00fc

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->a(Ljava/lang/String;I)V

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/text/m;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/text/m;-><init>(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_2

    .line 72
    :cond_e
    const v0, 0x7f0c0100

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/text/k;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/text/k;-><init>(Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0c0101

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailSellerInfoWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0800a5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method
