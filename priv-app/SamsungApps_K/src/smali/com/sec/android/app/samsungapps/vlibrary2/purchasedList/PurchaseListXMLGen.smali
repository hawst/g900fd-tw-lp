.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseListXMLGen;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseListXMLGen;->mContext:Landroid/content/Context;

    .line 17
    return-void
.end method


# virtual methods
.method public purchaseHistHide(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 101
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getNetHeaderInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    move-result-object v1

    const-string v2, "purchaseHistHide"

    const-string v3, "2313"

    const/4 v5, 0x1

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 102
    const-string v1, "orderID"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    const-string v1, "productID"

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    return-object v0
.end method

.method public purchaseListExMulti(II)Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
    .locals 8

    .prologue
    const/4 v5, 0x1

    .line 61
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParamCreator;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParamCreator;-><init>()V

    .line 62
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseListXMLGen;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParamCreator;->create(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;

    move-result-object v7

    .line 64
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getNetHeaderInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    move-result-object v1

    const-string v2, "purchaseListExMulti2Notc"

    const-string v3, "2312"

    const/4 v4, 0x0

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 65
    const-string v1, "startNum"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    const-string v1, "endNum"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    const-string v1, "imgWidth"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getWidth()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const-string v1, "imgHeight"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getHeight()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 70
    const-string v1, "imei"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getIMEI()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    :goto_0
    const-string v1, "contentType"

    const-string v2, "all"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    const-string v1, "alignOrder"

    const-string v2, "recent"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    const-string v1, "preloadCount"

    iget v2, v7, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;->preLoadCount:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    const-string v1, "postloadCount"

    iget v2, v7, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;->postLoadCount:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    const-string v1, "preloadApp"

    iget-object v2, v7, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;->preLoadApps:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    const-string v1, "postloadApp"

    iget-object v2, v7, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;->postLoadApps:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isTestMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 84
    const-string v1, "predeployed"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    :cond_0
    return-object v0

    .line 72
    :cond_1
    const-string v1, "imei"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public purchaseListExMulti(IILcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;)Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
    .locals 8

    .prologue
    const/4 v5, 0x1

    .line 21
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParamCreator;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParamCreator;-><init>()V

    .line 22
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseListXMLGen;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p3, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParamCreator;->createWithWGT(Lcom/sec/android/app/samsungapps/vlibrary3/installer/gearapi/Gear2APIConnectionManager;Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;

    move-result-object v7

    .line 24
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getNetHeaderInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    move-result-object v1

    const-string v2, "purchaseListExMulti2Notc"

    const-string v3, "2312"

    const/4 v4, 0x0

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 25
    const-string v1, "startNum"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    const-string v1, "endNum"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    const-string v1, "imgWidth"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getWidth()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    const-string v1, "imgHeight"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;->getHeight()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 30
    const-string v1, "imei"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getIMEI()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    :goto_0
    const-string v1, "contentType"

    const-string v2, "all"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    const-string v1, "alignOrder"

    const-string v2, "recent"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    const-string v1, "preloadCount"

    iget v2, v7, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;->preLoadCount:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    const-string v1, "postloadCount"

    iget v2, v7, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;->postLoadCount:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    const-string v1, "preloadApp"

    iget-object v2, v7, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;->preLoadApps:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    const-string v1, "postloadApp"

    iget-object v2, v7, Lcom/sec/android/app/samsungapps/vlibrary2/update/GetDownloadListParam;->postLoadApps:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isTestMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 44
    const-string v1, "predeployed"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    :cond_0
    return-object v0

    .line 32
    :cond_1
    const-string v1, "imei"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
