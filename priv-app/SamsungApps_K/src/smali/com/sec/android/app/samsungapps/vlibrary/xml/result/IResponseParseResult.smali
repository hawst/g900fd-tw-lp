.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary/xml/result/IResponseParseResult;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract addBodyListMap(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
.end method

.method public abstract getBodyListListMap()Ljava/util/ArrayList;
.end method

.method public abstract getBodyListMap()Ljava/util/ArrayList;
.end method

.method public abstract getHeaderMap()Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;
.end method

.method public abstract getServerErrorInfo()Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;
.end method

.method public abstract setHeaderMap(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
.end method

.method public abstract setServerErrorCode(Ljava/lang/String;)V
.end method

.method public abstract setServerErrorInfo(Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;)V
.end method

.method public abstract setServerErrorMsg(Ljava/lang/String;)V
.end method
