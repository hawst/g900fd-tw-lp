.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentButtonWidgetData;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;


# instance fields
.field private _IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

.field private _IPriceCalculator:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;

.field private _IPriceFormatter:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceFormatter;

.field private _PurchaseCommand:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

.field private _PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceFormatter;Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentButtonWidgetData;->_IPriceFormatter:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceFormatter;

    .line 24
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentButtonWidgetData;->_IPriceCalculator:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;

    .line 25
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentButtonWidgetData;->_IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

    .line 26
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentButtonWidgetData;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    .line 27
    iput-object p5, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentButtonWidgetData;->_PurchaseCommand:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    .line 28
    return-void
.end method

.method private getCouponAppliedPrice()D
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentButtonWidgetData;->_IPriceCalculator:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;->getBuyPrice()D

    move-result-wide v0

    return-wide v0
.end method

.method private getDefaultPrice()D
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentButtonWidgetData;->_IPriceCalculator:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;->isDiscounted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentButtonWidgetData;->_IPriceCalculator:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;->getDiscountedPrice()D

    move-result-wide v0

    .line 59
    :goto_0
    return-wide v0

    .line 57
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentButtonWidgetData;->_IPriceCalculator:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;->getNormalPrice()D

    move-result-wide v0

    goto :goto_0
.end method


# virtual methods
.method public cancel()Z
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentButtonWidgetData;->_PurchaseCommand:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->cancelPurchase()V

    .line 40
    const/4 v0, 0x0

    return v0
.end method

.method public getDiscountPrice()Ljava/lang/String;
    .locals 5

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentButtonWidgetData;->_IPriceFormatter:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceFormatter;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentButtonWidgetData;->getDefaultPrice()D

    move-result-wide v1

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentButtonWidgetData;->getCouponAppliedPrice()D

    move-result-wide v3

    sub-double/2addr v1, v3

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceFormatter;->getFormattedPrice(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNormalPrice()Ljava/lang/String;
    .locals 3

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentButtonWidgetData;->isDiscounted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentButtonWidgetData;->_IPriceFormatter:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceFormatter;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentButtonWidgetData;->getCouponAppliedPrice()D

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceFormatter;->getFormattedPrice(D)Ljava/lang/String;

    move-result-object v0

    .line 73
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentButtonWidgetData;->_IPriceFormatter:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceFormatter;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentButtonWidgetData;->getDefaultPrice()D

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceFormatter;->getFormattedPrice(D)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public isDiscounted()Z
    .locals 4

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentButtonWidgetData;->getCouponAppliedPrice()D

    move-result-wide v0

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentButtonWidgetData;->getDefaultPrice()D

    move-result-wide v2

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    .line 85
    const/4 v0, 0x1

    .line 88
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFreeProduct()Z
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x0

    return v0
.end method

.method public isKorea()Z
    .locals 2

    .prologue
    .line 45
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->KOREA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->checkCountry(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;)Z

    move-result v0

    return v0
.end method

.method public isPaymentAbleCondition()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 116
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentButtonWidgetData;->_IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;->getSelPaymentMethod()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    move-result-object v2

    if-nez v2, :cond_1

    .line 131
    :cond_0
    :goto_0
    return v0

    .line 121
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentButtonWidgetData;->_IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;->getSelPaymentMethod()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_Normal:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    if-eq v2, v3, :cond_2

    move v0, v1

    .line 123
    goto :goto_0

    .line 126
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentButtonWidgetData;->getCouponAppliedPrice()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    .line 128
    goto :goto_0
.end method

.method public payment()Z
    .locals 2

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentButtonWidgetData;->_IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentButtonWidgetData;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;->createRealPurchaseInfo(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentButtonWidgetData;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentButtonWidgetData;->_PurchaseCommand:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentButtonWidgetData;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->startPayment(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;)V

    .line 34
    const/4 v0, 0x0

    return v0
.end method

.method public release()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 105
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentButtonWidgetData;->_IPriceFormatter:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceFormatter;

    .line 106
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentButtonWidgetData;->_IPriceCalculator:Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceCalculator;

    .line 107
    return-void
.end method
