.class public final enum Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;

.field public static final enum COMPARE_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;

.field public static final enum GET_APK_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;

.field public static final enum REQUEST_PERMISSION_FROM_SERVER:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;

.field public static final enum SEND_SIG_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;

.field public static final enum SIG_CHANGED_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;

.field public static final enum SIG_SAME_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 16
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;

    const-string v1, "GET_APK_PERMISSION"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;->GET_APK_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;

    const-string v1, "REQUEST_PERMISSION_FROM_SERVER"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;->REQUEST_PERMISSION_FROM_SERVER:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;

    const-string v1, "SEND_SIG_FAILED"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;->SEND_SIG_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;

    const-string v1, "COMPARE_PERMISSION"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;->COMPARE_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;

    const-string v1, "SIG_SAME_PERMISSION"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;->SIG_SAME_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;

    const-string v1, "SIG_CHANGED_PERMISSION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;->SIG_CHANGED_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;

    .line 14
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;->GET_APK_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;->REQUEST_PERMISSION_FROM_SERVER:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;->SEND_SIG_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;->COMPARE_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;->SIG_SAME_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;->SIG_CHANGED_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary2/permission/autoupdate/IPermissionChangedState$Action;

    return-object v0
.end method
