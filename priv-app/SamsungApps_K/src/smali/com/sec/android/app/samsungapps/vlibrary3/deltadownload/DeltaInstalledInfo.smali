.class public Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaInstalledInfo;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaInstalledInfo;->a:Landroid/content/Context;

    .line 15
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaInstalledInfo;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 16
    return-void
.end method


# virtual methods
.method public getVersionCode()J
    .locals 4

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaInstalledInfo;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaInstalledInfo;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 22
    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaException;

    const-string v1, "Package info Error"

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 24
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaInstalledInfo;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 26
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaException;

    const-string v1, "Package info Error"

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 28
    :cond_2
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaInstalledInfo;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 29
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaInstalledInfo;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->getPackageVersionCode(Ljava/lang/String;)J

    move-result-wide v0

    .line 30
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_3

    .line 32
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaException;

    const-string v1, "Version Code Error"

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 34
    :cond_3
    return-wide v0
.end method

.method public getVersionCodeString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaInstalledInfo;->getVersionCode()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
