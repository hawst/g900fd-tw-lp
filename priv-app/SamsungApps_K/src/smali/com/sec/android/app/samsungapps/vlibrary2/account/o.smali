.class final Lcom/sec/android/app/samsungapps/vlibrary2/account/o;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;)V
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/o;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceiveResult(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 31
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/o;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->endLoading()V

    .line 32
    if-eqz p2, :cond_0

    .line 34
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/o;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;->onFinalResult(Z)V
    invoke-static {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;->access$100(Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;Z)V

    .line 35
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/o;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;->_IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand$ILoginODCWithTokenCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand$ILoginODCWithTokenCommandData;->doAfterLogin()V

    .line 51
    :goto_0
    return-void

    .line 39
    :cond_0
    if-eqz p3, :cond_1

    const-string v0, "3010"

    invoke-virtual {p3}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/o;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;->onFinalResult(Z)V
    invoke-static {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;Z)V

    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/o;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;->_IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand$ILoginODCWithTokenCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand$ILoginODCWithTokenCommandData;->doAfterLogin()V

    goto :goto_0

    .line 45
    :cond_1
    if-eqz p3, :cond_2

    const-string v0, "3015"

    invoke-virtual {p3}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getErrorCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/o;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;->_IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand$ILoginODCWithTokenCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand$ILoginODCWithTokenCommandData;->setTokenExpired()V

    .line 49
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/o;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;->onFinalResult(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;->access$300(Lcom/sec/android/app/samsungapps/vlibrary2/account/LoginODCWithTokenCommand;Z)V

    goto :goto_0
.end method
