.class public Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/SimpleFSM;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;
.implements Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;


# instance fields
.field a:Landroid/os/Handler;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

.field private d:Ljava/io/File;

.field private e:Ljava/lang/String;

.field private f:Landroid/content/Context;

.field private g:Ljava/io/File;

.field private h:Ljava/io/File;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;Ljava/io/File;Ljava/lang/String;Ljava/io/File;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/SimpleFSM;-><init>()V

    .line 83
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;->a:Landroid/os/Handler;

    .line 30
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;->f:Landroid/content/Context;

    .line 31
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

    .line 32
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;->d:Ljava/io/File;

    .line 33
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;->e:Ljava/lang/String;

    .line 34
    iput-object p5, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;->h:Ljava/io/File;

    .line 35
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

    invoke-interface {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;)V

    .line 36
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;)V
    .locals 4

    .prologue
    .line 15
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/GDiffPatcher;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/GDiffPatcher;-><init>()V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;->g:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;->d:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;->h:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/GDiffPatcher;->patch(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;->a:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/l;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/l;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;)V
    .locals 2

    .prologue
    .line 15
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/m;->a:[I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;->NORMAL_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;->setState(Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method protected entry()V
    .locals 2

    .prologue
    .line 64
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/m;->a:[I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 77
    :goto_0
    :pswitch_0
    return-void

    .line 66
    :pswitch_1
    :try_start_0
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;->f:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->getApkSourceDir(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;->g:Ljava/io/File;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;->DELTAPATCH:Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;->setState(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;->NORMAL_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;->setState(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    :try_start_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;->NORMAL_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;->setState(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 69
    :pswitch_2
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/k;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/k;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 74
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer;->install()V

    goto :goto_0

    .line 64
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected exit()V
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/m;->a:[I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;->ordinal()I

    .line 60
    return-void
.end method

.method protected getIdleState()Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;
    .locals 1

    .prologue
    .line 143
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;

    return-object v0
.end method

.method protected bridge synthetic getIdleState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;->getIdleState()Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;

    move-result-object v0

    return-object v0
.end method

.method public install()V
    .locals 2

    .prologue
    .line 40
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/m;->a:[I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 45
    :goto_0
    return-void

    .line 42
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;->DELTACHECK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 40
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onForegroundInstalling()V
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;->onForegroundInstalling()V

    .line 165
    :cond_0
    return-void
.end method

.method public onInstallFailed()V
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;->onInstallFailed()V

    .line 173
    :cond_0
    return-void
.end method

.method public onInstallFailed(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;->onInstallFailed(Ljava/lang/String;)V

    .line 157
    :cond_0
    return-void
.end method

.method public onInstallSuccess()V
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;->onInstallSuccess()V

    .line 181
    :cond_0
    return-void
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;)V
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    .line 149
    return-void
.end method

.method public userCancel()V
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/m;->a:[I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/DeltaInstaller$State;->ordinal()I

    .line 53
    return-void
.end method
