.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadItemCommand;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;


# virtual methods
.method public abstract cancel()V
.end method

.method public abstract cancelPreProcess()V
.end method

.method public abstract execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
.end method

.method public abstract isCanceled()Z
.end method

.method public abstract runPreProcess(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
.end method

.method public abstract setWaiting()V
.end method
