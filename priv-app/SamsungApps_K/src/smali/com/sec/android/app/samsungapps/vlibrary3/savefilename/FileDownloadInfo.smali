.class public Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/FileDownloadInfo;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/SaveFileName;

.field private b:J

.field private c:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/SaveFileName;Ljava/lang/String;J)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/FileDownloadInfo;->a:Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/SaveFileName;

    .line 13
    iput-wide p3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/FileDownloadInfo;->b:J

    .line 14
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/FileDownloadInfo;->c:Ljava/lang/String;

    .line 15
    return-void
.end method

.method public static forApk(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;)Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/FileDownloadInfo;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 42
    :try_start_0
    iget-object v1, p1, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->contentsSize:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v1

    .line 49
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/ApkSaveFileName;->fromContent(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/SaveFileName;

    move-result-object v3

    .line 50
    if-nez v3, :cond_1

    .line 60
    :cond_0
    :goto_0
    return-object v0

    .line 55
    :cond_1
    iget-object v4, p1, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->downLoadURI:Ljava/lang/String;

    .line 56
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_0

    .line 60
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/FileDownloadInfo;

    invoke-direct {v0, v3, v4, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/FileDownloadInfo;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/SaveFileName;Ljava/lang/String;J)V

    goto :goto_0

    .line 47
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static forDelta(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;)Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/FileDownloadInfo;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 19
    :try_start_0
    iget-object v1, p1, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->deltaContentsSize:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v1

    .line 26
    invoke-static {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/DeltaSaveFileName;->fromURLResult(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;)Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/DeltaSaveFileName;

    move-result-object v3

    .line 27
    if-nez v3, :cond_1

    .line 37
    :cond_0
    :goto_0
    return-object v0

    .line 32
    :cond_1
    iget-object v4, p1, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;->deltaDownloadURL:Ljava/lang/String;

    .line 33
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_0

    .line 37
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/FileDownloadInfo;

    invoke-direct {v0, v3, v4, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/FileDownloadInfo;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/SaveFileName;Ljava/lang/String;J)V

    goto :goto_0

    .line 24
    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public getDownloadURI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/FileDownloadInfo;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getExpectedSize()J
    .locals 2

    .prologue
    .line 70
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/FileDownloadInfo;->b:J

    return-wide v0
.end method

.method public getSaveFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/FileDownloadInfo;->a:Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/SaveFileName;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/SaveFileName;->getFileName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
