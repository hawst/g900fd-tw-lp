.class public Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IButtonStaticStateManager;


# instance fields
.field protected _Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field private a:Landroid/content/Context;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;)V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->a:Landroid/content/Context;

    .line 23
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 24
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->b:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    .line 25
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;

    .line 26
    return-void
.end method

.method private a()Z
    .locals 2

    .prologue
    .line 208
    const-string v0, "0"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getLoadType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "1"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getLoadType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getButtonState()Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-nez v0, :cond_0

    .line 33
    const/4 v0, 0x0

    .line 127
    :goto_0
    return-object v0

    .line 37
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->isInstalled()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 39
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->isUpdatable()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->hasOrderID()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->a()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "3"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getLoadType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    instance-of v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    if-eqz v0, :cond_2

    .line 43
    :cond_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->UPDATABLE:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    goto :goto_0

    .line 47
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->isFreeContent()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 49
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->GET:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    goto :goto_0

    .line 53
    :cond_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->BUY:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    goto :goto_0

    .line 59
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->isOldVersionInstalled()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->isFreeContent()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 63
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->UPDATABLE:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    goto :goto_0

    .line 67
    :cond_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->BUY:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    goto :goto_0

    .line 71
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isExecutable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 73
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->LAUNCHABLE:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    goto :goto_0

    .line 77
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IBaseContextUtil;->getFakeModel(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 79
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->SEE_THIS_APP_IN_GEAR_MANAGER:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    goto :goto_0

    .line 83
    :cond_8
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->LAUNCH_DISABLED:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    goto :goto_0

    .line 90
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isLinkApp()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->isFreeContent()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 94
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->GOOGLE_GET:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    goto/16 :goto_0

    .line 98
    :cond_a
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->GOOGLE_BUY:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    goto/16 :goto_0

    .line 103
    :cond_b
    const-string v0, "1"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getLoadType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 105
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    goto/16 :goto_0

    .line 109
    :cond_c
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->isCompletelyDownloaded(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 111
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->DOWNLOAD_COMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    goto/16 :goto_0

    .line 115
    :cond_d
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->hasOrderID()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 117
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    goto/16 :goto_0

    .line 121
    :cond_e
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->isFreeContent()Z

    move-result v0

    if-nez v0, :cond_f

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->a()Z

    move-result v0

    if-nez v0, :cond_f

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->hasOrderID()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 123
    :cond_f
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->GET:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    goto/16 :goto_0

    .line 127
    :cond_10
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->BUY:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    goto/16 :goto_0
.end method

.method public getDeleteButtonState()Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DeleteButtonState;
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IBaseContextUtil;->getFakeModel(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 154
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DeleteButtonState;->HIDE_DELETEBUTTON:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DeleteButtonState;

    .line 187
    :goto_0
    return-object v0

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->isKNOXApp()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 159
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DeleteButtonState;->HIDE_DELETEBUTTON:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DeleteButtonState;

    goto :goto_0

    .line 162
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-nez v0, :cond_2

    .line 164
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DeleteButtonState;->HIDE_DELETEBUTTON:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DeleteButtonState;

    goto :goto_0

    .line 167
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;->isInstalled(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->b:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isDeletableApp(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 171
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DeleteButtonState;->SHOW_DELETEBUTTON:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DeleteButtonState;

    goto :goto_0

    .line 175
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->b:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isSystemApp(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 177
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DeleteButtonState;->HIDE_DELETEBUTTON:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DeleteButtonState;

    goto :goto_0

    .line 181
    :cond_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DeleteButtonState;->SHOW_DELETEBUTTON_DISABLED:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DeleteButtonState;

    goto :goto_0

    .line 187
    :cond_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DeleteButtonState;->HIDE_DELETEBUTTON:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DeleteButtonState;

    goto :goto_0
.end method

.method protected isInstalled()Z
    .locals 2

    .prologue
    .line 225
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;->isInstalled(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v0

    return v0
.end method

.method protected isOldVersionInstalled()Z
    .locals 2

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;->isOldVersionInstalled(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v0

    return v0
.end method

.method protected isUpdatable()Z
    .locals 2

    .prologue
    .line 221
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;->isUpdatable(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v0

    return v0
.end method
