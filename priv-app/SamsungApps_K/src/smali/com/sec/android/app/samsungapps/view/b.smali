.class final Lcom/sec/android/app/samsungapps/view/b;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/view/BannerListView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/view/BannerListView;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/b;->a:Lcom/sec/android/app/samsungapps/view/BannerListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/b;->a:Lcom/sec/android/app/samsungapps/view/BannerListView;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/view/BannerListView;->mArrayAdapter:Landroid/widget/ArrayAdapter;

    if-eqz v0, :cond_1

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/b;->a:Lcom/sec/android/app/samsungapps/view/BannerListView;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/view/BannerListView;->mArrayAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, p3}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;

    .line 82
    if-eqz v0, :cond_1

    .line 85
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/b;->a:Lcom/sec/android/app/samsungapps/view/BannerListView;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/view/BannerListView;->c:Lcom/sec/android/app/samsungapps/view/GoogleAnalytics;

    if-eqz v1, :cond_0

    .line 87
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/b;->a:Lcom/sec/android/app/samsungapps/view/BannerListView;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/view/BannerListView;->c:Lcom/sec/android/app/samsungapps/view/GoogleAnalytics;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/view/GoogleAnalytics;->trackCountOfClickedBanner(Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;)V

    .line 89
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/b;->a:Lcom/sec/android/app/samsungapps/view/BannerListView;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/view/BannerListView;->runBanner(Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;)V

    .line 92
    :cond_1
    return-void
.end method
