.class public abstract Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/view/IContentArrayAdapter;


# static fields
.field public static final SIGNED_PLATFORM_NEW:I = 0x3

.field public static final SIGNED_PLATFORM_OLD:I = 0x1

.field public static final SIGNED_PLATFORM_UNKNOWN:I

.field protected static defaultImage:Landroid/graphics/drawable/Drawable;

.field protected static mContext:Landroid/content/Context;


# instance fields
.field protected _ListViewInfo:Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;

.field private a:Lcom/sec/android/app/samsungapps/view/InfLoadingState;

.field private b:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter$OnDisplayHandler;

.field private c:Z

.field private d:Landroid/view/LayoutInflater;

.field protected defaultPrice:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field protected mAppMgr:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

.field protected mListType:I

.field protected mTextViewResourceId:I

.field protected showInstalledMark:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 54
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 28
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->c:Z

    .line 34
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->e:Ljava/lang/String;

    .line 38
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->showInstalledMark:Z

    .line 55
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->a(Landroid/content/Context;II)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/util/List;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 48
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 28
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->c:Z

    .line 34
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->e:Ljava/lang/String;

    .line 38
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->showInstalledMark:Z

    .line 49
    invoke-direct {p0, p1, p2, p4}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->a(Landroid/content/Context;II)V

    .line 50
    return-void
.end method

.method private a(Landroid/content/Context;II)V
    .locals 5

    .prologue
    .line 131
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->d:Landroid/view/LayoutInflater;

    .line 132
    iput p2, p0, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->mTextViewResourceId:I

    .line 133
    sput-object p1, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->mContext:Landroid/content/Context;

    .line 134
    iput p3, p0, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->mListType:I

    .line 135
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->mAppMgr:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    .line 137
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->setItemCountPerPage()V

    .line 139
    const v0, 0x7f080274

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->e:Ljava/lang/String;

    .line 140
    sget-object v0, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08029b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->defaultPrice:Ljava/lang/String;

    .line 141
    sget-object v0, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->defaultImage:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 142
    const-class v1, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    monitor-enter v1

    .line 143
    :try_start_0
    sget-object v0, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget-object v2, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->mContext:Landroid/content/Context;

    const-string v3, "isa_samsungapps_default"

    const-string v4, "drawable"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/samsungapps/uiutil/KnoxResourceManager;->findResource(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->defaultImage:Landroid/graphics/drawable/Drawable;

    .line 144
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 146
    :cond_0
    return-void

    .line 144
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected createViewHolder(Landroid/view/View;)Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;
    .locals 9

    .prologue
    .line 199
    new-instance v0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->_ListViewInfo:Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;

    sget-object v2, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->mContext:Landroid/content/Context;

    sget-object v4, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->defaultImage:Landroid/graphics/drawable/Drawable;

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->defaultPrice:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->isSingleColumn()Z

    move-result v6

    iget v7, p0, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->mListType:I

    move-object v3, p1

    move-object v8, p0

    invoke-direct/range {v0 .. v8}, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;-><init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;Landroid/content/Context;Landroid/view/View;Landroid/graphics/drawable/Drawable;Ljava/lang/String;ZILcom/sec/android/app/samsungapps/view/IContentArrayAdapter;)V

    return-object v0
.end method

.method protected getDownloadingString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->e:Ljava/lang/String;

    return-object v0
.end method

.method public abstract getIndexOfFirstItem()I
.end method

.method public bridge synthetic getItem(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0, p1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 151
    .line 155
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v0

    .line 163
    :goto_0
    if-nez p2, :cond_2

    .line 165
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->d:Landroid/view/LayoutInflater;

    iget v3, p0, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->mTextViewResourceId:I

    invoke-virtual {v0, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 166
    invoke-virtual {p0, p2}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->createViewHolder(Landroid/view/View;)Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;

    move-result-object v0

    .line 168
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 175
    :goto_1
    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->setPosition(I)V

    .line 177
    iget-boolean v2, p0, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->showInstalledMark:Z

    if-eqz v2, :cond_0

    .line 179
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->setIsShowInstalledMark(Z)V

    .line 182
    :cond_0
    if-eqz v1, :cond_1

    .line 183
    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->setContent(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;)V

    .line 185
    :cond_1
    return-object p2

    .line 156
    :catch_0
    move-exception v0

    .line 157
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "BaseContentArrayAdapter::Exception::"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move-object v1, v2

    goto :goto_0

    .line 172
    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;

    goto :goto_1
.end method

.method public hasMoreItems()Z
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->c:Z

    return v0
.end method

.method public abstract isCompleteEmptyList()Z
.end method

.method public abstract isLoading()Z
.end method

.method public abstract isSearchList()Z
.end method

.method protected isSingleColumn()Z
    .locals 1

    .prologue
    .line 194
    const/4 v0, 0x0

    return v0
.end method

.method protected loadCompleted(Z)V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/view/InfLoadingState;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/view/InfLoadingState;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/view/InfLoadingState;->setNoDataEmptyView(Z)V

    .line 74
    :cond_0
    return-void
.end method

.method protected loading()V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/view/InfLoadingState;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/view/InfLoadingState;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/view/InfLoadingState;->setLoadingEmptyView()V

    .line 82
    :cond_0
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    .prologue
    .line 203
    invoke-static {}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->getInstance()Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;

    move-result-object v1

    invoke-virtual {p0, p3}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->showContentDetailActivity(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;)V

    .line 204
    return-void
.end method

.method protected onUpdateView()V
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->b:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter$OnDisplayHandler;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->b:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter$OnDisplayHandler;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->getIndexOfFirstItem()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter$OnDisplayHandler;->setFirstVisibleItem(I)V

    .line 110
    :cond_0
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/view/InfLoadingState;

    .line 86
    return-void
.end method

.method public abstract requestMoreData()V
.end method

.method public setDisplayHandler(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter$OnDisplayHandler;)V
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->b:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter$OnDisplayHandler;

    .line 102
    return-void
.end method

.method public setHasLoading(Z)V
    .locals 0

    .prologue
    .line 118
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->c:Z

    .line 119
    return-void
.end method

.method public abstract setIndexOfFirstItem(I)V
.end method

.method public setInfLoadingState(Lcom/sec/android/app/samsungapps/view/InfLoadingState;)V
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/view/InfLoadingState;

    .line 66
    return-void
.end method

.method protected setItemCountPerPage()V
    .locals 3

    .prologue
    .line 122
    sget-object v0, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->isSingleColumn()Z

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    const/16 v0, 0xf

    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->setItemCountPerPage(I)V

    .line 123
    return-void

    .line 122
    :cond_0
    const/16 v0, 0x1e

    goto :goto_0
.end method

.method public setNoDataEmptyView()V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/view/InfLoadingState;

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->a:Lcom/sec/android/app/samsungapps/view/InfLoadingState;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/view/InfLoadingState;->setNoDataEmptyView()V

    .line 98
    :cond_0
    return-void
.end method

.method public showInstalledMark()V
    .locals 1

    .prologue
    .line 190
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->showInstalledMark:Z

    .line 191
    return-void
.end method
