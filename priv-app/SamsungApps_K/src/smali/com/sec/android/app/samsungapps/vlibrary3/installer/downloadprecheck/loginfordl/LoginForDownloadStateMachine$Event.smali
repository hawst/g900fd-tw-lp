.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum AGREE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

.field public static final enum ALREADY_LOGED_IN:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

.field public static final enum DISAGREE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

.field public static final enum EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

.field public static final enum LOAD_DETAIL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

.field public static final enum LOAD_DETAIL_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

.field public static final enum LOGIN_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

.field public static final enum LOGIN_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

.field public static final enum NOT_LOGED_IN:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

.field public static final enum PAID_TYPE_CHANGED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

.field public static final enum PAID_TYPE_NOT_CHANGED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 17
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    const-string v1, "EXECUTE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    const-string v1, "ALREADY_LOGED_IN"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;->ALREADY_LOGED_IN:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    const-string v1, "NOT_LOGED_IN"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;->NOT_LOGED_IN:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    const-string v1, "LOGIN_SUCCESS"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;->LOGIN_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    const-string v1, "LOGIN_FAILED"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;->LOGIN_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    const-string v1, "LOAD_DETAIL_SUCCESS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;->LOAD_DETAIL_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    const-string v1, "LOAD_DETAIL_FAILED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;->LOAD_DETAIL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    const-string v1, "PAID_TYPE_CHANGED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;->PAID_TYPE_CHANGED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    const-string v1, "PAID_TYPE_NOT_CHANGED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;->PAID_TYPE_NOT_CHANGED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    const-string v1, "AGREE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;->AGREE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    const-string v1, "DISAGREE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;->DISAGREE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    .line 15
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;->ALREADY_LOGED_IN:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;->NOT_LOGED_IN:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;->LOGIN_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;->LOGIN_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;->LOAD_DETAIL_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;->LOAD_DETAIL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;->PAID_TYPE_CHANGED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;->PAID_TYPE_NOT_CHANGED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;->AGREE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;->DISAGREE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/LoginForDownloadStateMachine$Event;

    return-object v0
.end method
