.class public Lcom/sec/android/app/samsungapps/view/TransparentActivity;
.super Lcom/sec/android/app/samsungapps/base/BaseActivity;
.source "ProGuard"


# static fields
.field public static mTransparentActivity:Landroid/app/Activity;


# instance fields
.field a:Landroid/os/PowerManager;

.field b:Landroid/os/PowerManager$WakeLock;

.field public mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/samsungapps/view/TransparentActivity;->mTransparentActivity:Landroid/app/Activity;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 12
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/base/BaseActivity;-><init>()V

    .line 14
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/TransparentActivity;->mContext:Landroid/content/Context;

    .line 16
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/TransparentActivity;->a:Landroid/os/PowerManager;

    .line 17
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/TransparentActivity;->b:Landroid/os/PowerManager$WakeLock;

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 21
    invoke-static {}, Lcom/sec/android/app/samsungapps/ThemeInfo;->getInstance()Lcom/sec/android/app/samsungapps/ThemeInfo;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ThemeInfo;->getTheme(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/TransparentActivity;->setTheme(I)V

    .line 23
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 25
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/TransparentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x680080

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 30
    iput-object p0, p0, Lcom/sec/android/app/samsungapps/view/TransparentActivity;->mContext:Landroid/content/Context;

    .line 31
    sput-object p0, Lcom/sec/android/app/samsungapps/view/TransparentActivity;->mTransparentActivity:Landroid/app/Activity;

    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/TransparentActivity;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/TransparentActivity;->mContext:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/TransparentActivity;->a:Landroid/os/PowerManager;

    .line 34
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/TransparentActivity;->a:Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/TransparentActivity;->b:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/TransparentActivity;->a:Landroid/os/PowerManager;

    const v1, 0x3000000a

    const-string v2, "PushNotification WakeLock"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/TransparentActivity;->b:Landroid/os/PowerManager$WakeLock;

    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/TransparentActivity;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 40
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/base/BaseActivity;->onBackPressed()V

    .line 41
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/TransparentActivity;->b:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/TransparentActivity;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/TransparentActivity;->b:Landroid/os/PowerManager$WakeLock;

    .line 51
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/base/BaseActivity;->onDestroy()V

    .line 52
    return-void
.end method
