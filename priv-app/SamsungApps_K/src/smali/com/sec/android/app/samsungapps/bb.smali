.class final Lcom/sec/android/app/samsungapps/bb;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V
    .locals 0

    .prologue
    .line 2569
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/bb;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createNdefMessage(Landroid/nfc/NfcEvent;)Landroid/nfc/NdefMessage;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2578
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/bb;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2600
    :cond_0
    :goto_0
    return-object v0

    .line 2586
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/bb;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v1

    .line 2588
    if-eqz v1, :cond_0

    .line 2594
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "samsungapps://ProductDetail/"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2595
    new-instance v0, Landroid/nfc/NdefMessage;

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/nfc/NdefRecord;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/bb;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    const-string v5, "application/com.sec.android.app.samsungapps.detail"

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v4, v5, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->_createMimeRecord(Ljava/lang/String;[B)Landroid/nfc/NdefRecord;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-direct {v0, v2}, Landroid/nfc/NdefMessage;-><init>([Landroid/nfc/NdefRecord;)V

    goto :goto_0
.end method
