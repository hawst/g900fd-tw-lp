.class public abstract Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerBase;
.super Landroid/app/Activity;
.source "ProGuard"


# static fields
.field protected static final REQUEST_CODE_SAC_FIRST_CHECK:I = 0x2002

.field protected static final RESULT_CANCELED:I = 0x0

.field protected static final RESULT_FAILED:I = 0x1

.field protected static final RESULT_INCORRECT_CLIENT_SECRET:I = 0x5

.field protected static final RESULT_INVALID_CLIENT_ID:I = 0x4

.field protected static final RESULT_INVALID_COUNTRYCODE:I = 0x6

.field protected static final RESULT_NETWORK_ERROR:I = 0x3

.field protected static final RESULT_OK:I = -0x1

.field protected static final RESULT_SIM_NOT_READY:I = 0x2

.field protected static final RESULT_SKIP:I = 0x7

.field public static final SAC_INIT_VERSION_CODE:I = 0x2ee1

.field public static final SAC_PKGNAME:Ljava/lang/String; = "com.osp.app.signin"

.field public static final SAC_VERSION_1_3001:I = 0x32c9


# instance fields
.field protected _SamsungAccountValidator:Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;

.field private a:Lcom/sec/android/app/samsungapps/LoadingDialog;

.field protected db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

.field protected mAppPreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerBase;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    return-void
.end method


# virtual methods
.method protected endLoading()V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerBase;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerBase;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->end()V

    .line 81
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerBase;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    .line 83
    :cond_0
    return-void
.end method

.method protected getCommand()Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerBase;->_SamsungAccountValidator:Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 37
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 39
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerBase;->mAppPreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    .line 40
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerBase;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    .line 42
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerBase;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->readObject(Landroid/content/Intent;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerBase;->_SamsungAccountValidator:Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerBase;->_SamsungAccountValidator:Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;

    if-nez v0, :cond_0

    .line 45
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerBase;->finish()V

    .line 50
    :goto_0
    return-void

    .line 49
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerBase;->start()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 60
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 61
    return-void
.end method

.method protected abstract start()V
.end method

.method protected startLoading()V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerBase;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    if-nez v0, :cond_0

    .line 71
    new-instance v0, Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerBase;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerBase;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->start()V

    .line 74
    :cond_0
    return-void
.end method
