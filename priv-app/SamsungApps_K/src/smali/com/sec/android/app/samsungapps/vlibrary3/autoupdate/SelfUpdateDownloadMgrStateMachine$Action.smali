.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;

.field public static final enum INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;

.field public static final enum NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;

.field public static final enum NOTIFY_INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;

.field public static final enum NOTIFY_SILENCE_INSTALLFAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;

.field public static final enum NOTIFY_UPDATE_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;

.field public static final enum REQ_DOWNLOADING:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;

.field public static final enum REQ_RESOURCE_LOCK:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 20
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;

    const-string v1, "REQ_RESOURCE_LOCK"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;->REQ_RESOURCE_LOCK:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;

    const-string v1, "REQ_DOWNLOADING"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;->REQ_DOWNLOADING:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;

    const-string v1, "NOTIFY_FAILED"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;

    const-string v1, "INSTALL"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;->INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;

    const-string v1, "NOTIFY_UPDATE_SUCCESS"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;->NOTIFY_UPDATE_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;

    const-string v1, "NOTIFY_SILENCE_INSTALLFAILED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;->NOTIFY_SILENCE_INSTALLFAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;

    const-string v1, "NOTIFY_INSTALLING"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;->NOTIFY_INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;

    const-string v1, "CANCEL"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;->CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;

    .line 19
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;->REQ_RESOURCE_LOCK:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;->REQ_DOWNLOADING:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;->INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;->NOTIFY_UPDATE_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;->NOTIFY_SILENCE_INSTALLFAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;->NOTIFY_INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;->CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateDownloadMgrStateMachine$Action;

    return-object v0
.end method
