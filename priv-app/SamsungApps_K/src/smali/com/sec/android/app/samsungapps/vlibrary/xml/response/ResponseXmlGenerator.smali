.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseXmlGenerator;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private mResponseData:Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseData;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseData;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseXmlGenerator;->mResponseData:Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseData;

    .line 17
    return-void
.end method

.method private writeCloseTag(Ljava/lang/String;Lorg/xmlpull/v1/XmlSerializer;)V
    .locals 1

    .prologue
    .line 70
    const-string v0, ""

    invoke-interface {p2, v0, p1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 71
    return-void
.end method

.method private writeObject(Lorg/xmlpull/v1/XmlSerializer;Ljava/util/HashMap;)V
    .locals 5

    .prologue
    .line 76
    const-string v0, ""

    const-string v1, "list"

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 78
    invoke-virtual {p2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 79
    const-string v1, ""

    const-string v3, "value"

    invoke-interface {p1, v1, v3}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 80
    const-string v3, ""

    const-string v4, "name"

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {p1, v3, v4, v1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 81
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {p1, v0}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 82
    const-string v0, ""

    const-string v1, "param"

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0

    .line 85
    :cond_0
    const-string v0, ""

    const-string v1, "list"

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 86
    return-void
.end method

.method private writeOpenTagWithMapAttribute(Ljava/lang/String;Lorg/xmlpull/v1/XmlSerializer;Ljava/util/HashMap;)V
    .locals 4

    .prologue
    .line 61
    const-string v0, ""

    invoke-interface {p2, v0, p1}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 63
    invoke-virtual {p3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/util/Map$Entry;

    .line 64
    const-string v3, ""

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {p2, v3, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0

    .line 66
    :cond_0
    return-void
.end method


# virtual methods
.method public genXml()Ljava/lang/String;
    .locals 5

    .prologue
    .line 21
    :try_start_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    .line 22
    invoke-static {}, Landroid/util/Xml;->newSerializer()Lorg/xmlpull/v1/XmlSerializer;

    move-result-object v1

    .line 23
    new-instance v2, Ljava/io/StringWriter;

    invoke-direct {v2}, Ljava/io/StringWriter;-><init>()V

    .line 25
    invoke-interface {v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/Writer;)V

    .line 27
    const-string v0, "SamsungProtocol"

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseXmlGenerator;->mResponseData:Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseData;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseData;->getSamsungProtocolMap()Ljava/util/HashMap;

    move-result-object v3

    invoke-direct {p0, v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseXmlGenerator;->writeOpenTagWithMapAttribute(Ljava/lang/String;Lorg/xmlpull/v1/XmlSerializer;Ljava/util/HashMap;)V

    .line 29
    const-string v0, "response"

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseXmlGenerator;->mResponseData:Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseData;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseData;->getResponseHeaderMap()Ljava/util/HashMap;

    move-result-object v3

    invoke-direct {p0, v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseXmlGenerator;->writeOpenTagWithMapAttribute(Ljava/lang/String;Lorg/xmlpull/v1/XmlSerializer;Ljava/util/HashMap;)V

    .line 32
    const-string v0, ""

    const-string v3, "errorInfo"

    invoke-interface {v1, v0, v3}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 33
    const-string v0, ""

    const-string v3, "errorString"

    invoke-interface {v1, v0, v3}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 34
    const-string v0, ""

    const-string v3, "errorCode"

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseXmlGenerator;->mResponseData:Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseData;

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseData;->getErrorCode()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v0, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 35
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseXmlGenerator;->mResponseData:Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseData;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseData;->getErrorString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseXmlGenerator;->mResponseData:Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseData;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseData;->getErrorString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 38
    :cond_0
    const-string v0, ""

    const-string v3, "errorString"

    invoke-interface {v1, v0, v3}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 39
    const-string v0, ""

    const-string v3, "errorInfo"

    invoke-interface {v1, v0, v3}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 40
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseXmlGenerator;->mResponseData:Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseData;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseData;->getObjectMapList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 42
    invoke-direct {p0, v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseXmlGenerator;->writeObject(Lorg/xmlpull/v1/XmlSerializer;Ljava/util/HashMap;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 52
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 54
    const-string v0, ""

    :goto_1
    return-object v0

    .line 45
    :cond_1
    :try_start_1
    const-string v0, "response"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseXmlGenerator;->writeCloseTag(Ljava/lang/String;Lorg/xmlpull/v1/XmlSerializer;)V

    .line 46
    const-string v0, "SamsungProtocol"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/response/ResponseXmlGenerator;->writeCloseTag(Ljava/lang/String;Lorg/xmlpull/v1/XmlSerializer;)V

    .line 48
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_1
.end method
