.class final Lcom/sec/android/app/samsungapps/vlibrary2/account/s;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/s;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCommandResult(Z)V
    .locals 3

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/s;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->mRequestTokenForServiceCommand:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;->getResultData()Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;

    move-result-object v0

    .line 53
    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/s;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/s;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->mRequestTokenForServiceCommand:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;
    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenForServiceCommand;->getResultData()Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->onResultToGetToken(Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand$BroadCastResult;)V

    .line 59
    :goto_0
    return-void

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/account/s;->a:Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;

    const/4 v1, 0x1

    const-string v2, "\r\nNULL"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/account/RequestTokenCommand;->onFailGetAccessToken(ILjava/lang/String;)V

    goto :goto_0
.end method
