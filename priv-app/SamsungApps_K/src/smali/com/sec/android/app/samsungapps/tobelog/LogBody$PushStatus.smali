.class public final enum Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum OFF:Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;

.field public static final enum ON:Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 14
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;

    const-string v1, "ON"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;->ON:Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;

    .line 15
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;

    const-string v1, "OFF"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;->OFF:Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;

    .line 13
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;->ON:Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;->OFF:Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;->a:[Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;->a:[Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;

    return-object v0
.end method
