.class final Lcom/sec/android/app/samsungapps/widget/purchase/t;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field protected a:Landroid/content/Context;

.field protected b:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodSelectable;

.field protected c:I

.field protected d:Landroid/widget/CheckBox;

.field protected e:Landroid/widget/ImageView;

.field protected f:Landroid/widget/TextView;

.field protected g:Landroid/widget/TextView;

.field protected h:Landroid/widget/ImageView;

.field protected i:Landroid/widget/ImageView;

.field protected j:Landroid/view/View;

.field final synthetic k:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;Landroid/content/Context;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 216
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->k:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 217
    const v0, 0x7f0c017d

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->d:Landroid/widget/CheckBox;

    .line 218
    const v0, 0x7f0c0200

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->e:Landroid/widget/ImageView;

    .line 219
    const v0, 0x7f0c0201

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->f:Landroid/widget/TextView;

    .line 220
    const v0, 0x7f0c0202

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->g:Landroid/widget/TextView;

    .line 221
    const v0, 0x7f0c0203

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->h:Landroid/widget/ImageView;

    .line 222
    const v0, 0x7f0c0204

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->i:Landroid/widget/ImageView;

    .line 223
    const v0, 0x7f0c01fe

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->j:Landroid/view/View;

    .line 224
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->a:Landroid/content/Context;

    .line 226
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .prologue
    .line 229
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->c:I

    .line 230
    return-void
.end method

.method public final a(Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodSelectable;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const v4, 0x7f0802aa

    const/16 v2, 0x8

    const/4 v3, 0x0

    .line 234
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodSelectable;

    .line 235
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->f:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->d:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->d:Landroid/widget/CheckBox;

    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setEnabled(Z)V

    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/widget/purchase/r;->b:[I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodSelectable;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodSelectable;->getPaymentMethodSpec()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 237
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodSelectable;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodSelectable;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->d:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 238
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->d:Landroid/widget/CheckBox;

    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 242
    :goto_1
    return-void

    .line 235
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->k:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->e(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f08029a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->k:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->f(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f08029f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->k:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->g(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;)Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/ICreditCardCommandBuilder;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/ICreditCardCommandBuilder;->getCreditCardInfo()Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;->getCreditCardType()Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;->getCreditCardLast4Digit()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->i:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    sget-object v1, Lcom/sec/android/app/samsungapps/widget/purchase/r;->a:[I

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;->getCreditCardType()Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo$CreditCardType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    :goto_2
    :pswitch_3
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " xxx-"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/CreditCardInfo;->getCreditCardLast4Digit()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_4
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->e:Landroid/widget/ImageView;

    const v2, 0x7f02012f

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->e:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    :pswitch_5
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->e:Landroid/widget/ImageView;

    const v2, 0x7f020135

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->e:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    :pswitch_6
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->e:Landroid/widget/ImageView;

    const v2, 0x7f020137

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->e:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    :pswitch_7
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->e:Landroid/widget/ImageView;

    const v2, 0x7f020139

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->e:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    :pswitch_8
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->e:Landroid/widget/ImageView;

    const v2, 0x7f02013c

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->e:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->d:Landroid/widget/CheckBox;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->d:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setEnabled(Z)V

    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->k:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->h(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0802ae

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :pswitch_9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->k:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->i(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f080325

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :pswitch_a
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->k:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->j(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f080270

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :pswitch_b
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->k:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->k(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :pswitch_c
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->k:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->l(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :pswitch_d
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->k:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->m(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :pswitch_e
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->k:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->n(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f080301

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :pswitch_f
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->k:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->o(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f08031d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :pswitch_10
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->k:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;->p(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f08023c

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 240
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/t;->d:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_1

    .line 235
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_3
        :pswitch_3
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method
