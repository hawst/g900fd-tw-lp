.class final Lcom/sec/android/app/samsungapps/vlibrary2/comment/o;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML$IModifyCommentInfo;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;)V
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/o;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getCommentID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/o;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->mIMyReviewCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewCommandData;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewCommandData;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewCommandData;->getCommentID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getCommentText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/o;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->mIMyReviewDetailView:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewDetailView;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->access$300(Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewDetailView;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewDetailView;->getCommentText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getProductID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/o;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->mIMyReviewCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewCommandData;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewCommandData;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewCommandData;->getProductID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getRatingValue()I
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/o;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->mIMyReviewDetailView:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewDetailView;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->access$300(Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewDetailView;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewDetailView;->getRatingValue()I

    move-result v0

    return v0
.end method
