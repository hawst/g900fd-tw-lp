.class public Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManager;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManager$ICacheClearManagerObserver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;

    .line 55
    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;)V
    .locals 1

    .prologue
    .line 24
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;)Z

    .line 25
    return-void
.end method


# virtual methods
.method public getState()Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;

    return-object v0
.end method

.method public bridge synthetic getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 8
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManager;->getState()Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;

    move-result-object v0

    return-object v0
.end method

.method public onAction(Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Action;)V
    .locals 2

    .prologue
    .line 44
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/a;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Action;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 53
    :cond_0
    :goto_0
    return-void

    .line 47
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManager$ICacheClearManagerObserver;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManager$ICacheClearManagerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManager$ICacheClearManagerObserver;->onClearCache()V

    goto :goto_0

    .line 44
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic onAction(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Action;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManager;->onAction(Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Action;)V

    return-void
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManager$ICacheClearManagerObserver;)V
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManager$ICacheClearManagerObserver;

    .line 30
    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;)V
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;

    .line 35
    return-void
.end method

.method public bridge synthetic setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 8
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManager;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$State;)V

    return-void
.end method

.method public toBMode()V
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;->GO_BMODE:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;)V

    .line 15
    return-void
.end method

.method public toNormalMode()V
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;->GO_NormalMODE:Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/cacheclearmanager/CacheClearManagerStateMachine$Event;)V

    .line 20
    return-void
.end method
