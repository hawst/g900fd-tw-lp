.class public Lcom/sec/android/app/samsungapps/commands/CAlipayCommandBuilder;
.super Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AbsAlipayCommandBuilder;
.source "ProGuard"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AbsAlipayCommandBuilder;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    .line 23
    return-void
.end method


# virtual methods
.method public createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
    .locals 1

    .prologue
    .line 32
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/r;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/commands/r;-><init>(Lcom/sec/android/app/samsungapps/commands/CAlipayCommandBuilder;)V

    return-object v0
.end method

.method protected getInstallerFactory()Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->createInstallerFactory()Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

    move-result-object v0

    return-object v0
.end method

.method public onNotifyAlipay(Z)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 5

    .prologue
    .line 49
    sget-object v1, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    .line 50
    const v0, 0x7f08023e

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 51
    const/4 v2, 0x1

    if-ne p1, v2, :cond_0

    .line 53
    const v0, 0x7f08023f

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 56
    :cond_0
    new-instance v2, Lcom/sec/android/app/samsungapps/commands/NotiCommand;

    const v3, 0x7f0802ef

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f08023d

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v0, v3, v1}, Lcom/sec/android/app/samsungapps/commands/NotiCommand;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method public process()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lcom/sec/android/app/samsungapps/AlipayModule;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/AlipayModule;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;)V

    return-object v0
.end method
