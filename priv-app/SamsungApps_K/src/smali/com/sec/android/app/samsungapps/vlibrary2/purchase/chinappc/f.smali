.class final Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/f;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCPurchaseParam;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;)V
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/f;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/f;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;->_IChinaPPCCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;->getPurchaseInfo()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getAppliedCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    move-result-object v0

    return-object v0
.end method

.method public final getPaymentMethodID()Ljava/lang/String;
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/f;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;->_PrepaidCardList:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCardInfo;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;->access$300(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCardInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/f;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;->_PrepaidCardList:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCardInfo;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;->access$300(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCardInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCardInfo;->getCardOperatorCount()I

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/f;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;->_PrepaidCardList:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCardInfo;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;->access$300(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCardInfo;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCardInfo;->getCardPaymentMethodID(I)Ljava/lang/String;

    move-result-object v0

    .line 98
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final getPaymentPrice()D
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/f;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;->_IChinaPPCCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;->getPurchaseInfo()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getBuyPrice()D

    move-result-wide v0

    return-wide v0
.end method

.method public final getProductInfo()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/f;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;->_IChinaPPCCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PPCCompleteCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/IChinaPPCCommandBuilder;->getPurchaseInfo()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getProduct()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    move-result-object v0

    return-object v0
.end method
