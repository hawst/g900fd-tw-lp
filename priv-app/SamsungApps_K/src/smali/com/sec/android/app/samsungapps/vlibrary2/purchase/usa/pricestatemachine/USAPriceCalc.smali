.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEventManager$IAppEventObserver;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/price/IPriceRelatedFields;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceContext;


# instance fields
.field private _Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field private _Context:Landroid/content/Context;

.field private _ILoadingDialogCreator:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;

.field private _IUSAPriceCalcObserver:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc$IUSAPriceCalcObserver;

.field private _NormalPrice:D

.field private _Price:D

.field private _Receiver:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

.field _State:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;

.field _TaxIncludedNormalPrice:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/CheckTaxIncludedPrice;

.field _TaxIncludedSellingPrice:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/CheckTaxIncludedPrice;

.field dlg:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

.field reqCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;)V
    .locals 3

    .prologue
    const-wide/16 v0, 0x0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->_Price:D

    .line 22
    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->_NormalPrice:D

    .line 46
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;->INIT:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->_State:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;

    .line 73
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->reqCount:I

    .line 181
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->dlg:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    .line 30
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->_Context:Landroid/content/Context;

    .line 31
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 32
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->_ILoadingDialogCreator:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;

    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_GLOBAL_CREDIT_CARD:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getPaymentPrice(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;)Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;->getSellingPrice()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->_Price:D

    .line 34
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_GLOBAL_CREDIT_CARD:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getPaymentPrice(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;)Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;->getNormalPrice()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->_NormalPrice:D

    .line 36
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/CheckTaxIncludedPrice;

    iget-wide v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->_NormalPrice:D

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/CheckTaxIncludedPrice;-><init>(D)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->_TaxIncludedNormalPrice:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/CheckTaxIncludedPrice;

    .line 37
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/CheckTaxIncludedPrice;

    iget-wide v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->_Price:D

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/CheckTaxIncludedPrice;-><init>(D)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->_TaxIncludedSellingPrice:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/CheckTaxIncludedPrice;

    .line 38
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEventManager;->addObserver(Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEventManager$IAppEventObserver;)V

    .line 39
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;Z)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->onReceiveTaxPrice(Z)V

    return-void
.end method

.method private onReceiveTaxPrice(Z)V
    .locals 2

    .prologue
    .line 106
    if-eqz p1, :cond_2

    .line 108
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->isDiscounted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 110
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->reqCount:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 112
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceEvent;->RECEIVE_PRICE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceEvent;

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceEvent;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceContext;)V

    .line 124
    :cond_0
    :goto_0
    return-void

    .line 117
    :cond_1
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceEvent;->RECEIVE_PRICE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceEvent;

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceEvent;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceContext;)V

    goto :goto_0

    .line 122
    :cond_2
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceEvent;->RECEIVE_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceEvent;

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceEvent;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceContext;)V

    goto :goto_0
.end method


# virtual methods
.method public endLoading()V
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->dlg:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->dlg:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->endLoading()V

    .line 179
    :cond_0
    return-void
.end method

.method public getDiscountPhoneBillPrice()D
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getDiscountPhoneBillPrice()D

    move-result-wide v0

    return-wide v0
.end method

.method public getPhoneBillPrice()D
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getPhoneBillPrice()D

    move-result-wide v0

    return-wide v0
.end method

.method public getReducePrice()D
    .locals 2

    .prologue
    .line 161
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->getState()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;->READY:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;

    if-ne v0, v1, :cond_0

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->_TaxIncludedSellingPrice:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/CheckTaxIncludedPrice;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/CheckTaxIncludedPrice;->getTaxIncludedPrice()D

    move-result-wide v0

    .line 165
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getReducePrice()D

    move-result-wide v0

    goto :goto_0
.end method

.method public getSellingPrice()D
    .locals 2

    .prologue
    .line 148
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->getState()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;->READY:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;

    if-ne v0, v1, :cond_1

    .line 150
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->isDiscounted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->_TaxIncludedNormalPrice:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/CheckTaxIncludedPrice;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/CheckTaxIncludedPrice;->getTaxIncludedPrice()D

    move-result-wide v0

    .line 156
    :goto_0
    return-wide v0

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->_TaxIncludedSellingPrice:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/CheckTaxIncludedPrice;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/CheckTaxIncludedPrice;->getTaxIncludedPrice()D

    move-result-wide v0

    goto :goto_0

    .line 156
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getSellingPrice()D

    move-result-wide v0

    goto :goto_0
.end method

.method public getState()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->_State:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;

    return-object v0
.end method

.method public isDiscounted()Z
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isDiscounted()Z

    move-result v0

    return v0
.end method

.method public isPhoneBillDiscounted()Z
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isPhoneBillDiscounted()Z

    move-result v0

    return v0
.end method

.method public onAppEvent(Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEvent;)V
    .locals 2

    .prologue
    .line 204
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/c;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEvent;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 210
    :goto_0
    return-void

    .line 207
    :pswitch_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceEvent;->CREDITCARD_REGISTERED:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceEvent;

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceEvent;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceContext;)V

    goto :goto_0

    .line 204
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public ready(Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 2

    .prologue
    .line 69
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->_Receiver:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    .line 70
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceEvent;->PREPARE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceEvent;

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceEvent;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$USAPriceContext;)V

    .line 71
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    .line 43
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEventManager;->removeObserver(Lcom/sec/android/app/samsungapps/vlibrary2/eventmanager/AppEventManager$IAppEventObserver;)Z

    .line 44
    return-void
.end method

.method public requestPrice()V
    .locals 3

    .prologue
    .line 76
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->reqCount:I

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->_TaxIncludedSellingPrice:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/CheckTaxIncludedPrice;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/a;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/CheckTaxIncludedPrice;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 88
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->isDiscounted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->_TaxIncludedNormalPrice:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/CheckTaxIncludedPrice;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/b;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/CheckTaxIncludedPrice;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 102
    :cond_0
    return-void
.end method

.method public sendFailSignal()V
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->_Receiver:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;->onCommandResult(Z)V

    .line 129
    return-void
.end method

.method public sendSuccessSignal()V
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->_Receiver:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;->onCommandResult(Z)V

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->_IUSAPriceCalcObserver:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc$IUSAPriceCalcObserver;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->_IUSAPriceCalcObserver:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc$IUSAPriceCalcObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc$IUSAPriceCalcObserver;->onPriceUpdated()V

    .line 60
    :cond_0
    return-void
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc$IUSAPriceCalcObserver;)V
    .locals 0

    .prologue
    .line 194
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->_IUSAPriceCalcObserver:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc$IUSAPriceCalcObserver;

    .line 195
    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->_State:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalcStateMachine$E_UPRICESTATE;

    .line 65
    return-void
.end method

.method public startLoading()V
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->_ILoadingDialogCreator:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;->createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->dlg:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->dlg:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/usa/pricestatemachine/USAPriceCalc;->dlg:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->startLoading()V

    .line 190
    :cond_0
    return-void
.end method
