.class public Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;
.super Landroid/widget/TextView;
.source "ProGuard"


# instance fields
.field a:Ljava/lang/String;

.field private b:I

.field private c:I

.field private d:Z

.field private e:Ljava/lang/String;

.field private f:Z

.field protected mListener:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView$ITextSingleLineChanged;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 13
    const-string v0, "SamsungAppsDescriptionTextView::"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->a:Ljava/lang/String;

    .line 14
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->b:I

    .line 16
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->c:I

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->mListener:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView$ITextSingleLineChanged;

    .line 24
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->d:Z

    .line 28
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->f:Z

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 13
    const-string v0, "SamsungAppsDescriptionTextView::"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->a:Ljava/lang/String;

    .line 14
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->b:I

    .line 16
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->c:I

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->mListener:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView$ITextSingleLineChanged;

    .line 24
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->d:Z

    .line 28
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->f:Z

    .line 36
    return-void
.end method

.method private static a([F)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 205
    .line 206
    array-length v2, p0

    move v1, v0

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, p0, v0

    .line 207
    float-to-int v3, v3

    add-int/2addr v1, v3

    .line 206
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 209
    :cond_0
    return v1
.end method

.method private a()Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 128
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->b:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineEnd(I)I

    move-result v5

    .line 130
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->b:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineWidth(I)F

    move-result v0

    float-to-int v6, v0

    .line 131
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->b:I

    if-le v0, v2, :cond_8

    .line 132
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->b:I

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineEnd(I)I

    move-result v0

    move v4, v0

    .line 134
    :goto_0
    const/16 v0, 0xf

    if-le v6, v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/text/Layout;->getEllipsizedWidth()I

    move-result v0

    sub-int/2addr v0, v6

    const/16 v1, 0x28

    if-le v0, v1, :cond_1

    const-string v0, " ..."

    .line 136
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 138
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    add-int/lit8 v8, v5, -0x3

    invoke-interface {v7, v8, v5}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "..."

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 139
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1, v4, v5}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 140
    const/16 v7, 0xa

    invoke-virtual {v1, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 142
    const/4 v7, -0x1

    if-ne v1, v7, :cond_4

    .line 143
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    add-int/lit8 v7, v5, -0x1

    invoke-interface {v1, v7, v5}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v7, " "

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 144
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    add-int/lit8 v7, v5, -0x1

    invoke-interface {v6, v3, v7}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 159
    :goto_2
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v1, v4, v6}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 161
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->getLayout()Landroid/text/Layout;

    move-result-object v6

    invoke-virtual {v6}, Landroid/text/Layout;->getEllipsizedWidth()I

    move-result v6

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    new-array v7, v7, [F

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v8

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->getTextSize()F

    move-result v9

    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setTextSize(F)V

    invoke-virtual {v8, v4, v7}, Landroid/graphics/Paint;->getTextWidths(Ljava/lang/String;[F)I

    invoke-static {v7}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->a([F)I

    move-result v4

    if-lt v4, v6, :cond_7

    :goto_3
    if-eqz v2, :cond_0

    .line 162
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit8 v4, v5, -0x3

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 166
    :cond_0
    return-object v1

    .line 134
    :cond_1
    const-string v0, "..."

    goto/16 :goto_1

    .line 146
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->getLayout()Landroid/text/Layout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/text/Layout;->getEllipsizedWidth()I

    move-result v1

    sub-int/2addr v1, v6

    const/16 v6, 0x14

    if-le v1, v6, :cond_3

    .line 147
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6, v3, v5}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 149
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    add-int/lit8 v7, v5, -0x3

    invoke-interface {v6, v3, v7}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    .line 153
    :cond_4
    if-eqz v1, :cond_5

    if-ne v1, v2, :cond_6

    .line 154
    :cond_5
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    add-int/2addr v1, v4

    invoke-interface {v7, v3, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, "..."

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    .line 156
    :cond_6
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    add-int/2addr v1, v4

    invoke-interface {v7, v3, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    :cond_7
    move v2, v3

    goto/16 :goto_3

    :cond_8
    move v4, v3

    goto/16 :goto_0
.end method


# virtual methods
.method public clearData()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 170
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->b:I

    .line 174
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->mListener:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView$ITextSingleLineChanged;

    .line 175
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->d:Z

    .line 176
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->e:Ljava/lang/String;

    .line 177
    return-void
.end method

.method public clearOriginLineCount()V
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->e:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    return-void
.end method

.method public isExpanded()Z
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->d:Z

    return v0
.end method

.method public isNeedMoreBtn(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 107
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    .line 108
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 109
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->getTextSize()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 111
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v2, 0x42500000    # 52.0f

    mul-float/2addr v1, v2

    .line 112
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    float-to-int v1, v1

    sub-int v1, v2, v1

    .line 113
    int-to-float v1, v1

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v3, v1, v2}, Landroid/graphics/Paint;->breakText(Ljava/lang/String;ZF[F)I

    move-result v0

    .line 114
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v0, :cond_0

    .line 115
    iput-boolean v3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->f:Z

    .line 119
    :goto_0
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->f:Z

    return v0

    .line 117
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->f:Z

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 49
    :try_start_0
    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->onMeasure(II)V

    .line 51
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->c:I

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 54
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->e:Ljava/lang/String;

    .line 57
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->b:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineEnd(I)I

    move-result v0

    if-lez v0, :cond_4

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->b:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineEnd(I)I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->f:Z

    .line 64
    :goto_0
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->c:I

    if-lez v0, :cond_3

    .line 65
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->f:Z

    if-eqz v0, :cond_7

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->mListener:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView$ITextSingleLineChanged;

    if-eqz v0, :cond_6

    .line 67
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->d:Z

    if-nez v0, :cond_5

    .line 68
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->b:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->setMaxLines(I)V

    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 70
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->mListener:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView$ITextSingleLineChanged;

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView$ITextSingleLineChanged;->onTextSingleLineChanged(Landroid/widget/TextView;Z)V

    .line 92
    :cond_3
    :goto_2
    return-void

    .line 61
    :cond_4
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->f:Z

    goto :goto_0

    .line 92
    :catch_0
    move-exception v0

    goto :goto_2

    .line 73
    :cond_5
    const/16 v0, 0x3e8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->setMaxLines(I)V

    .line 74
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->e:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 80
    :cond_6
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->b:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->setMaxLines(I)V

    .line 81
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 84
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->mListener:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView$ITextSingleLineChanged;

    if-eqz v0, :cond_3

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->mListener:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView$ITextSingleLineChanged;

    const/4 v1, 0x1

    invoke-interface {v0, p0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView$ITextSingleLineChanged;->onTextSingleLineChanged(Landroid/widget/TextView;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2
.end method

.method public setExpandState(Z)V
    .locals 0

    .prologue
    .line 99
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->d:Z

    .line 100
    return-void
.end method

.method public setMaxLineCount(I)V
    .locals 0

    .prologue
    .line 39
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->b:I

    .line 40
    return-void
.end method

.method public setOnTextSingleLineChangedListener(Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView$ITextSingleLineChanged;)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView;->mListener:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDescriptionTextView$ITextSingleLineChanged;

    .line 44
    return-void
.end method
