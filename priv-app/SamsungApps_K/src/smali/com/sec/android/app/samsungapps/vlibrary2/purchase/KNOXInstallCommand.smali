.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/KNOXInstallCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _IKNOXInstallCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/KNOXInstallCommand$IKNOXInstallCommandData;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/KNOXInstallCommand$IKNOXInstallCommandData;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/KNOXInstallCommand;->_IKNOXInstallCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/KNOXInstallCommand$IKNOXInstallCommandData;

    .line 17
    return-void
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 3

    .prologue
    .line 21
    const-string v0, "hcjo"

    const-string v1, "call KNOX installPackage()"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/KNOXInstallCommand;->_Context:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallInfo;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/KNOXInstallCommand;->_IKNOXInstallCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/KNOXInstallCommand$IKNOXInstallCommandData;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/KNOXInstallCommand$IKNOXInstallCommandData;->getPackagePath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallInfo;-><init>(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallQueue;->execute(Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXInstallInfo;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 24
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/KNOXInstallCommand;->onFinalResult(Z)V

    .line 26
    :cond_0
    return-void
.end method
