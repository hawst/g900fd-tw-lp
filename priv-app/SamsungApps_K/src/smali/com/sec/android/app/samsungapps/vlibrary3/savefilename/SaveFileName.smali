.class public abstract Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/SaveFileName;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/SaveFileNameInfo;


# direct methods
.method protected constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/SaveFileNameInfo;)V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/SaveFileName;->a:Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/SaveFileNameInfo;

    .line 8
    return-void
.end method


# virtual methods
.method protected abstract fileExtension()Ljava/lang/String;
.end method

.method public final getFileName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 12
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/SaveFileName;->a:Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/SaveFileNameInfo;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/SaveFileNameInfo;->getVersionCode()Ljava/lang/String;

    move-result-object v0

    .line 13
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 15
    :cond_0
    const-string v0, "0"

    .line 17
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "samsungapps-"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/SaveFileName;->a:Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/SaveFileNameInfo;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/SaveFileNameInfo;->getProductID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/SaveFileName;->a:Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/SaveFileNameInfo;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/SaveFileNameInfo;->getExpectedFileSize()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/savefilename/SaveFileName;->fileExtension()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 19
    return-object v0
.end method
