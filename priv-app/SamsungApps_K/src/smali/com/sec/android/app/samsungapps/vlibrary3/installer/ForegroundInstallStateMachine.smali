.class public Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstallStateMachine;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;
.source "ProGuard"


# static fields
.field private static a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstallStateMachine;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;-><init>()V

    .line 16
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstallStateMachine;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstallStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstallStateMachine;

    if-nez v0, :cond_0

    .line 22
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstallStateMachine;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstallStateMachine;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstallStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstallStateMachine;

    .line 24
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstallStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstallStateMachine;

    return-object v0
.end method


# virtual methods
.method protected entry(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 2

    .prologue
    .line 56
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/n;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 69
    :goto_0
    return-void

    .line 59
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;->DELETE_FILE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 60
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;->SIG_INSTALL_COMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 61
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;->REMOVE_PACKAGEMANAGER_OBSERVER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 64
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;->REGISTER_PACKAGEMANAGER_OBSERVER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 65
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;->REQUEST_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 66
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;->SIG_INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 56
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;)Z
    .locals 2

    .prologue
    .line 30
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/n;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 51
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    return v0

    .line 33
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/n;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 36
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$State;->INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstallStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 43
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/n;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 46
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$State;->INSTALL_COMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstallStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 30
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch

    .line 33
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch

    .line 43
    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_4
    .end packed-switch
.end method

.method public bridge synthetic execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 10
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/ForegroundInstallStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/installer/IForegroundInstallState$Event;)Z

    move-result v0

    return v0
.end method

.method protected exit(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 0

    .prologue
    .line 74
    return-void
.end method
