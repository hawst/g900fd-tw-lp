.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static final AD_APK_DEFAULT_VER_NAME:Ljava/lang/String; = "1.0.0.00"

.field private static final AD_APK_PKGNAME:Ljava/lang/String; = "com.sec.android.app.secad"

.field private static final AD_APK_PRODUCT_NAME:Ljava/lang/String; = "Samsung Ad Utility"

.field public static final ALIPAY_APK_DEFAULT_VER_NAME:Ljava/lang/String; = "0.1"

.field public static final ALIPAY_APK_PKGNAME:Ljava/lang/String; = "com.alipay.android.app"

.field public static final ALIPAY_APK_PRODUCT_NAME:Ljava/lang/String; = "\u652f\u4ed8\u5b9d\u5b89\u5168\u652f\u4ed8\u670d\u52a1"

.field private static ODC_PACKAGE_NAME:Ljava/lang/String; = null

.field public static final SAMSUNGAPPS_APK_DEFAULT_VER_NAME:Ljava/lang/String; = "0.1"

.field public static final SAMSUNGAPPS_APK_PKGNAME:Ljava/lang/String; = "com.sec.android.app.samsungapps"

.field public static final SAMSUNGAPPS_APK_PRODUCT_NAME:Ljava/lang/String; = "Samsung Apps"

.field public static final SAMSUNGAPPS_UNA1_APK_PKGNAME:Ljava/lang/String; = "com.sec.android.app.samsungapps.una"

.field public static final SAMSUNGAPPS_UNA2_APK_PKGNAME:Ljava/lang/String; = "com.sec.android.app.samsungapps.una2"

.field public static final SAMSUNGAPPS_UNA_APK_DEFAULT_VER_NAME:Ljava/lang/String; = "0.1"

.field public static final SAMSUNGAPPS_UNA_APK_PRODUCT_NAME:Ljava/lang/String; = "Samsung Apps UNA"

.field public static final SAMSUNGAPPS_WIDGETAPP_PKGNAME:Ljava/lang/String; = "com.sec.android.widgetapp.samsungapps"

.field public static final SAMSUNGAPPS_WIDGETAPP_PRODUCT_NAME:Ljava/lang/String; = "Samsung Apps Widget"

.field public static final SAMSUNGAPPS_WIDGETAPP_VER_NAME:Ljava/lang/String; = "0.1"

.field public static final SAMSUNGAPPS_WIDGET_PKGNAME:Ljava/lang/String; = "com.sec.android.widget.samsungapps"

.field public static final SAMSUNGAPPS_WIDGET_PRODUCT_NAME:Ljava/lang/String; = "Samsung Apps Widget"

.field public static final SAMSUNGAPPS_WIDGET_VER_NAME:Ljava/lang/String; = "0.1"

.field public static final SAMSUNG_IAP_PKGNAME:Ljava/lang/String; = "com.sec.android.iap"

.field public static final SAMSUNG_IAP_PRODUCT_NAME:Ljava/lang/String; = "Samsung IAP"

.field public static final SAMSUNG_IAP_VER_NAME:Ljava/lang/String; = "0.1"

.field private static final SPP_APK_DEFAULT_VER_NAME:Ljava/lang/String; = "0.10.10.0"

.field private static final SPP_APK_PKGNAME:Ljava/lang/String; = "com.sec.spp.push"

.field private static final SPP_APK_PRODUCT_NAME:Ljava/lang/String; = "Samsung Push Service"

.field private static final TENCENT_APK_DEFAULT_VER_NAME:Ljava/lang/String; = "1.0.0.00"

.field private static final TENCENT_APK_PRODUCT_NAME:Ljava/lang/String; = "\u5e94\u7528\u5b9d"

.field public static final UP_APK_DEFAULT_VER_NAME:Ljava/lang/String; = "1.00.00"

.field public static final UP_APK_PRODUCT_NAME:Ljava/lang/String; = "Samsung Billing"

.field public static final UP_PACKAGENAME:Ljava/lang/String; = "com.sec.android.app.billing"


# instance fields
.field private mAppList:Ljava/util/ArrayList;

.field private mAppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

.field private mNeedUpdateList:Ljava/util/ArrayList;

.field private mRequestString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-string v0, "com.sec.android.app.samsungapps"

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->ODC_PACKAGE_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mRequestString:Ljava/lang/String;

    .line 27
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mAppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    .line 28
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mAppList:Ljava/util/ArrayList;

    .line 29
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mNeedUpdateList:Ljava/util/ArrayList;

    .line 113
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mAppList:Ljava/util/ArrayList;

    .line 114
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mNeedUpdateList:Ljava/util/ArrayList;

    .line 115
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mAppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    .line 117
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;

    const-string v2, "Samsung Push Service"

    const-string v3, "com.sec.spp.push"

    const-string v4, "0.10.10.0"

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mAppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;)V

    .line 118
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 120
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;

    const-string v2, "Samsung Ad Utility"

    const-string v3, "com.sec.android.app.secad"

    const-string v4, "1.0.0.00"

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mAppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;)V

    .line 121
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 123
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;

    const-string v2, "Samsung Billing"

    const-string v3, "com.sec.android.app.billing"

    const-string v4, "1.00.00"

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mAppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;)V

    .line 124
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 131
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;

    const-string v2, "\u652f\u4ed8\u5b9d\u5b89\u5168\u652f\u4ed8\u670d\u52a1"

    const-string v3, "com.alipay.android.app"

    const-string v4, "0.1"

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mAppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;)V

    .line 132
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mAppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    const-string v1, "com.sec.android.widget.samsungapps"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v0

    .line 138
    if-ne v0, v6, :cond_0

    .line 139
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;

    const-string v2, "Samsung Apps Widget"

    const-string v3, "com.sec.android.widget.samsungapps"

    const-string v4, "0.1"

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mAppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;)V

    .line 140
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mAppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    const-string v1, "com.sec.android.widgetapp.samsungapps"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v0

    .line 144
    if-ne v0, v6, :cond_1

    .line 145
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;

    const-string v2, "Samsung Apps Widget"

    const-string v3, "com.sec.android.widgetapp.samsungapps"

    const-string v4, "0.1"

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mAppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;)V

    .line 146
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 149
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->isPlatformVersionOverICS()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 151
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;

    const-string v2, "Samsung IAP"

    const-string v3, "com.sec.android.iap"

    const-string v4, "0.1"

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mAppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;)V

    .line 152
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 156
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->makeAppListInfo()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mRequestString:Ljava/lang/String;

    .line 157
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mRequestString:Ljava/lang/String;

    .line 27
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mAppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    .line 28
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mAppList:Ljava/util/ArrayList;

    .line 29
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mNeedUpdateList:Ljava/util/ArrayList;

    .line 170
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mAppList:Ljava/util/ArrayList;

    .line 171
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mNeedUpdateList:Ljava/util/ArrayList;

    .line 172
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mAppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    .line 174
    const-string v0, "com.alipay.android.app"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v1, :cond_1

    .line 176
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;

    const-string v2, "\u652f\u4ed8\u5b9d\u5b89\u5168\u652f\u4ed8\u670d\u52a1"

    const-string v3, "com.alipay.android.app"

    const-string v4, "0.1"

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mAppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;)V

    .line 177
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 190
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->makeAppListInfo()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mRequestString:Ljava/lang/String;

    .line 191
    return-void

    .line 179
    :cond_1
    const-string v0, "com.sec.android.app.samsungapps"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v1, :cond_2

    .line 181
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;

    const-string v2, "Samsung Apps"

    const-string v3, "com.sec.android.app.samsungapps"

    const-string v4, "0.1"

    const-string v5, "odc9820938409234.apk"

    iget-object v6, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mAppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;)V

    .line 182
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 184
    :cond_2
    const-string v0, "com.sec.android.app.samsungapps.una2"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v1, :cond_0

    .line 186
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;

    const-string v2, "Samsung Apps UNA"

    const-string v3, "com.sec.android.app.samsungapps.una2"

    const-string v4, "0.1"

    const-string v5, "odc9820938409234.apk"

    iget-object v6, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mAppManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;)V

    .line 187
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private findAppByGUID(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mAppList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 264
    const-string v0, "SAUtilityAppList::findAppByGUID::Apps List is empty"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move-object v0, v1

    .line 276
    :goto_0
    return-object v0

    .line 268
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;

    .line 270
    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->compareGUID(Ljava/lang/String;)Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 276
    goto :goto_0
.end method

.method private isPlatformVersionOverICS()Z
    .locals 2

    .prologue
    .line 160
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private makeAppListInfo()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 199
    const-string v2, ""

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mAppList:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 203
    const-string v0, "SAUtilityAppList::makeAppListInfo::App List is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 222
    :cond_0
    return-object v2

    .line 208
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mAppList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v3

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;

    .line 210
    if-ne v1, v3, :cond_2

    .line 212
    const/4 v1, 0x0

    .line 219
    :goto_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->getApkInfo()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 216
    :cond_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "||"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method


# virtual methods
.method public addUpdateApp(Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;)V
    .locals 2

    .prologue
    .line 231
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mNeedUpdateList:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 233
    const-string v0, "SAUtilityAppList::addUpdateApp::Update List is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 253
    :cond_0
    :goto_0
    return-void

    .line 237
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;->needUpdate()Z

    move-result v0

    .line 238
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 240
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mNeedUpdateList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 242
    iget-object v0, p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;->GUID:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->findAppByGUID(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;

    move-result-object v0

    .line 243
    if-nez v0, :cond_2

    .line 245
    const-string v0, "SAUtilityAppList::addUpdateApp::Not found App by GUID"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 249
    :cond_2
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;->setName(Ljava/lang/String;)V

    .line 250
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityApp;->getPkgFileName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;->setPkgFileName(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getRequestString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mRequestString:Ljava/lang/String;

    return-object v0
.end method

.method public getUpdateApp(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;
    .locals 4

    .prologue
    .line 325
    const/4 v1, 0x0

    .line 327
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mNeedUpdateList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 331
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mNeedUpdateList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 339
    :goto_0
    return-object v0

    .line 333
    :catch_0
    move-exception v0

    .line 335
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SAUtilityAppList::getUpdateApp::"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public getUpdateAppCount()I
    .locals 2

    .prologue
    .line 308
    const/4 v0, 0x0

    .line 310
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mNeedUpdateList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 312
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;->mNeedUpdateList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 315
    :cond_0
    return v0
.end method

.method public isSPP(Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 349
    .line 351
    if-eqz p1, :cond_0

    iget-object v1, p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;->GUID:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 353
    :cond_0
    const-string v1, "SAUtilityAppList::isSPP::Not ready Object"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 362
    :cond_1
    :goto_0
    return v0

    .line 357
    :cond_2
    iget-object v1, p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityNeedUpdate;->GUID:Ljava/lang/String;

    const-string v2, "com.sec.spp.push"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    .line 359
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public requestUpdateCheck(Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ResultReceiver;)V
    .locals 3

    .prologue
    .line 285
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    const/4 v1, 0x0

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/ah;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ah;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ResultReceiver;)V

    invoke-virtual {v0, p0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->updateCheck(Lcom/sec/android/app/samsungapps/vlibrary/doc/SAUtilityAppList;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 294
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 295
    return-void
.end method
