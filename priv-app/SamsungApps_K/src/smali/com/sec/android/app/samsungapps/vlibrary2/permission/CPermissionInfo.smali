.class public Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfo;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo;


# instance fields
.field private _Description:Ljava/lang/String;

.field private _GroupDescription:Ljava/lang/String;

.field private _GroupID:Ljava/lang/String;

.field private _GroupTitle:Ljava/lang/String;

.field private _Label:Ljava/lang/String;

.field private _PermissionID:Ljava/lang/String;

.field private _PermissionType:Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo$EnumPermissionType;

.field private _hasInfo:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo$EnumPermissionType;->CREATE:Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo$EnumPermissionType;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfo;->_PermissionType:Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo$EnumPermissionType;

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfo;->_hasInfo:Z

    .line 31
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfo;->_PermissionID:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfo;->_Label:Ljava/lang/String;

    .line 33
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfo;->_Description:Ljava/lang/String;

    .line 34
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo$EnumPermissionType;->CREATE:Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo$EnumPermissionType;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfo;->_PermissionType:Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo$EnumPermissionType;

    .line 19
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfo;->_hasInfo:Z

    .line 20
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfo;->_PermissionID:Ljava/lang/String;

    .line 21
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfo;->_Label:Ljava/lang/String;

    .line 22
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfo;->_Description:Ljava/lang/String;

    .line 23
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfo;->_GroupID:Ljava/lang/String;

    .line 24
    iput-object p5, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfo;->_GroupTitle:Ljava/lang/String;

    .line 25
    iput-object p6, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfo;->_GroupDescription:Ljava/lang/String;

    .line 26
    return-void
.end method


# virtual methods
.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfo;->_Description:Ljava/lang/String;

    return-object v0
.end method

.method public getGroupDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfo;->_GroupDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getGroupID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfo;->_GroupID:Ljava/lang/String;

    return-object v0
.end method

.method public getGroupTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfo;->_GroupTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfo;->_Label:Ljava/lang/String;

    return-object v0
.end method

.method public getPermissionID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfo;->_PermissionID:Ljava/lang/String;

    return-object v0
.end method

.method public getPermissionType()Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo$EnumPermissionType;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfo;->_PermissionType:Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo$EnumPermissionType;

    return-object v0
.end method

.method public hasPermissionInfo()Z
    .locals 1

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfo;->_hasInfo:Z

    return v0
.end method

.method public setPermissionType(Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo$EnumPermissionType;)V
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/CPermissionInfo;->_PermissionType:Lcom/sec/android/app/samsungapps/vlibrary2/permission/IPermissionInfo$EnumPermissionType;

    .line 95
    return-void
.end method
