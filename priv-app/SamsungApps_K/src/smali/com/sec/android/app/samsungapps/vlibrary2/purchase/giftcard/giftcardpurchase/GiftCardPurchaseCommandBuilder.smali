.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchaseCommandBuilder;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchase$IGiftCardPurchaseData;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/IGiftCardPurchaseCommandBuilder;


# instance fields
.field private _FinalMap:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

.field private _ILoadingDialogCreator:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;

.field private _PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchaseCommandBuilder;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    .line 17
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchaseCommandBuilder;->_FinalMap:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    .line 18
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchaseCommandBuilder;->_ILoadingDialogCreator:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;

    .line 19
    return-void
.end method


# virtual methods
.method public createGiftCardPurchase()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchase;
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchase;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchase;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchase$IGiftCardPurchaseData;)V

    return-object v0
.end method

.method public createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchaseCommandBuilder;->_ILoadingDialogCreator:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;->createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v0

    return-object v0
.end method

.method public getFinalResultMap()Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchaseCommandBuilder;->_FinalMap:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    return-object v0
.end method

.method public getGiftCardCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchaseCommandBuilder;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getSelectedGiftCard()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchaseCommandBuilder;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getSelectedGiftCard()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;->getGiftCardCode()Ljava/lang/String;

    move-result-object v0

    .line 43
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getProductID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchaseCommandBuilder;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getProduct()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;->getProductID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
