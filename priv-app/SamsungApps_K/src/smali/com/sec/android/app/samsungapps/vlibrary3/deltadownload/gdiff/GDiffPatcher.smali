.class public Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/GDiffPatcher;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private a:Ljava/nio/ByteBuffer;

.field private b:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const/16 v0, 0x800

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/GDiffPatcher;->a:Ljava/nio/ByteBuffer;

    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/GDiffPatcher;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/GDiffPatcher;->b:[B

    .line 43
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const/16 v0, 0x800

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/GDiffPatcher;->a:Ljava/nio/ByteBuffer;

    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/GDiffPatcher;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/GDiffPatcher;->b:[B

    .line 46
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/GDiffPatcher;->patch(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    return-void
.end method

.method private a(ILjava/io/BufferedInputStream;Ljava/io/BufferedOutputStream;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 212
    :goto_0
    if-lez p1, :cond_1

    .line 213
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/GDiffPatcher;->b:[B

    array-length v0, v0

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 214
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/GDiffPatcher;->b:[B

    invoke-virtual {p2, v1, v3, v0}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v1

    .line 215
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 216
    new-instance v1, Ljava/io/EOFException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "cannot read "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " len : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 217
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/GDiffPatcher;->b:[B

    invoke-virtual {p3, v0, v3, v1}, Ljava/io/BufferedOutputStream;->write([BII)V

    .line 218
    sub-int/2addr p1, v1

    .line 219
    goto :goto_0

    .line 220
    :cond_1
    return-void
.end method

.method private a(JILcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/SeekableSource;Ljava/io/BufferedOutputStream;)V
    .locals 3

    .prologue
    .line 199
    invoke-interface {p4, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/SeekableSource;->seek(J)V

    .line 200
    :goto_0
    if-lez p3, :cond_1

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/GDiffPatcher;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    invoke-static {v0, p3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 202
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/GDiffPatcher;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/nio/Buffer;->limit(I)Ljava/nio/Buffer;

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/GDiffPatcher;->a:Ljava/nio/ByteBuffer;

    invoke-interface {p4, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/SeekableSource;->read(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 204
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 205
    new-instance v0, Ljava/io/EOFException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "in copy "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 206
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/GDiffPatcher;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p5, v1, v2, v0}, Ljava/io/BufferedOutputStream;->write([BII)V

    .line 207
    sub-int/2addr p3, v0

    .line 208
    goto :goto_0

    .line 209
    :cond_1
    return-void
.end method


# virtual methods
.method public patch(Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/SeekableSource;Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 9

    .prologue
    const/16 v2, 0xff

    const/16 v1, 0xd1

    .line 114
    new-instance v5, Ljava/io/BufferedOutputStream;

    invoke-direct {v5, p3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 115
    new-instance v6, Ljava/io/BufferedInputStream;

    invoke-direct {v6, p2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 117
    new-instance v7, Ljava/io/DataOutputStream;

    invoke-direct {v7, v5}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 118
    new-instance v8, Ljava/io/DataInputStream;

    invoke-direct {v8, v6}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 121
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readUnsignedByte()I

    move-result v0

    if-ne v0, v1, :cond_0

    invoke-virtual {v8}, Ljava/io/DataInputStream;->readUnsignedByte()I

    move-result v0

    if-ne v0, v2, :cond_0

    invoke-virtual {v8}, Ljava/io/DataInputStream;->readUnsignedByte()I

    move-result v0

    if-ne v0, v1, :cond_0

    invoke-virtual {v8}, Ljava/io/DataInputStream;->readUnsignedByte()I

    move-result v0

    if-ne v0, v2, :cond_0

    invoke-virtual {v8}, Ljava/io/DataInputStream;->readUnsignedByte()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    .line 127
    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/PatchException;

    const-string v1, "magic string not found, aborting!"

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/PatchException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 141
    :cond_1
    const/16 v1, 0xf6

    if-gt v0, v1, :cond_3

    .line 142
    invoke-direct {p0, v0, v6, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/GDiffPatcher;->a(ILjava/io/BufferedInputStream;Ljava/io/BufferedOutputStream;)V

    .line 131
    :cond_2
    :goto_0
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readUnsignedByte()I

    move-result v0

    .line 132
    if-nez v0, :cond_1

    .line 134
    invoke-virtual {v7}, Ljava/io/DataOutputStream;->flush()V

    .line 135
    return-void

    .line 146
    :cond_3
    packed-switch v0, :pswitch_data_0

    .line 191
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "command "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 148
    :pswitch_0
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v0

    .line 149
    invoke-direct {p0, v0, v6, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/GDiffPatcher;->a(ILjava/io/BufferedInputStream;Ljava/io/BufferedOutputStream;)V

    goto :goto_0

    .line 152
    :pswitch_1
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    .line 153
    invoke-direct {p0, v0, v6, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/GDiffPatcher;->a(ILjava/io/BufferedInputStream;Ljava/io/BufferedOutputStream;)V

    goto :goto_0

    .line 156
    :pswitch_2
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v0

    .line 157
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readUnsignedByte()I

    move-result v3

    .line 158
    int-to-long v1, v0

    move-object v0, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/GDiffPatcher;->a(JILcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/SeekableSource;Ljava/io/BufferedOutputStream;)V

    goto :goto_0

    .line 161
    :pswitch_3
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v0

    .line 162
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v3

    .line 163
    int-to-long v1, v0

    move-object v0, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/GDiffPatcher;->a(JILcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/SeekableSource;Ljava/io/BufferedOutputStream;)V

    goto :goto_0

    .line 166
    :pswitch_4
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v0

    .line 167
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    .line 168
    int-to-long v1, v0

    move-object v0, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/GDiffPatcher;->a(JILcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/SeekableSource;Ljava/io/BufferedOutputStream;)V

    goto :goto_0

    .line 171
    :pswitch_5
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    .line 172
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readUnsignedByte()I

    move-result v3

    .line 173
    int-to-long v1, v0

    move-object v0, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/GDiffPatcher;->a(JILcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/SeekableSource;Ljava/io/BufferedOutputStream;)V

    goto :goto_0

    .line 176
    :pswitch_6
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    .line 177
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v3

    .line 178
    int-to-long v1, v0

    move-object v0, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/GDiffPatcher;->a(JILcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/SeekableSource;Ljava/io/BufferedOutputStream;)V

    goto :goto_0

    .line 181
    :pswitch_7
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    .line 182
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    .line 183
    int-to-long v1, v0

    move-object v0, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/GDiffPatcher;->a(JILcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/SeekableSource;Ljava/io/BufferedOutputStream;)V

    goto/16 :goto_0

    .line 186
    :pswitch_8
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v1

    .line 187
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    move-object v0, p0

    move-object v4, p1

    .line 188
    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/GDiffPatcher;->a(JILcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/SeekableSource;Ljava/io/BufferedOutputStream;)V

    goto/16 :goto_0

    .line 146
    :pswitch_data_0
    .packed-switch 0xf7
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public patch(Ljava/io/File;Ljava/io/File;Ljava/io/File;)V
    .locals 4

    .prologue
    .line 78
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/RandomAccessFileSeekableSource;

    new-instance v0, Ljava/io/RandomAccessFile;

    const-string v2, "r"

    invoke-direct {v0, p1, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/RandomAccessFileSeekableSource;-><init>(Ljava/io/RandomAccessFile;)V

    .line 79
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 80
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 82
    :try_start_0
    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/GDiffPatcher;->patch(Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/SeekableSource;Ljava/io/InputStream;Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 86
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/RandomAccessFileSeekableSource;->close()V

    .line 87
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 88
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V

    .line 89
    return-void

    .line 83
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 86
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/RandomAccessFileSeekableSource;->close()V

    .line 87
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 88
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V

    throw v0
.end method

.method public patch(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const-wide/32 v5, 0x7fffffff

    .line 50
    const-string v0, "Gdiff"

    const-string v1, "start patching file"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 52
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 53
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 55
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v3

    cmp-long v3, v3, v5

    if-gtz v3, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v3

    cmp-long v3, v3, v5

    if-lez v3, :cond_1

    .line 57
    :cond_0
    const-string v0, "Gdiff"

    const-string v1, "source or patch is too large, max length is 2147483647"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    const-string v0, "Gdiff"

    const-string v1, "aborting.."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    :goto_0
    return-void

    .line 63
    :cond_1
    :try_start_0
    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/GDiffPatcher;->patch(Ljava/io/File;Ljava/io/File;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 68
    :goto_1
    const-string v0, "Gdiff"

    const-string v1, "finished patching file"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 65
    :catch_0
    move-exception v0

    const-string v0, "Gdiff"

    const-string v1, "exception diff patch"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public patch([BLjava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 1

    .prologue
    .line 96
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/ByteBufferSeekableSource;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/ByteBufferSeekableSource;-><init>([B)V

    invoke-virtual {p0, v0, p2, p3}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/GDiffPatcher;->patch(Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/SeekableSource;Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 97
    return-void
.end method

.method public patch([B[B)[B
    .locals 2

    .prologue
    .line 103
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 104
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {p0, p1, v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/GDiffPatcher;->patch([BLjava/io/InputStream;Ljava/io/OutputStream;)V

    .line 105
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method
