.class public Lcom/sec/android/app/samsungapps/CommandBuilder;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static a:Lcom/sec/android/app/samsungapps/CommandBuilder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7
    new-instance v0, Lcom/sec/android/app/samsungapps/CommandBuilder;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/CommandBuilder;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/CommandBuilder;->a:Lcom/sec/android/app/samsungapps/CommandBuilder;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/CommandBuilder;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/sec/android/app/samsungapps/CommandBuilder;->a:Lcom/sec/android/app/samsungapps/CommandBuilder;

    return-object v0
.end method


# virtual methods
.method public createSignUp()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 3

    .prologue
    .line 20
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/SignUpCommand;

    const-class v1, Lcom/sec/android/app/samsungapps/SignUpActivity;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SignUpCommand;-><init>(Ljava/lang/Class;Z)V

    return-object v0
.end method
