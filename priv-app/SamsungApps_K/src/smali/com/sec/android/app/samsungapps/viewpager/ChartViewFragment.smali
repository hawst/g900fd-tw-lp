.class public Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;
.super Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/uieventmanager/UIEventObserver;
.implements Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;

.field private b:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

.field private c:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

.field private d:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;-><init>()V

    .line 40
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->a:Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;

    .line 41
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->b:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    .line 42
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->c:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    .line 43
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->d:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;)Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->a:Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;)V
    .locals 2

    .prologue
    .line 38
    if-nez p1, :cond_0

    const-string v0, "ChartViewFragment::_onHorizontalContentClick::Content is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x6

    invoke-static {v0, p1, v1}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)V
    .locals 2

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x4

    invoke-static {v0, p1, v1}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;I)V

    return-void
.end method


# virtual methods
.method public handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 343
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isKorea()Z

    move-result v0

    if-nez v0, :cond_1

    .line 393
    :cond_0
    :goto_0
    return v2

    .line 348
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->REAL_AGE_NAME_VERIFIED:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    if-ne v0, v1, :cond_5

    .line 350
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->a:Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;

    if-eqz v0, :cond_2

    .line 352
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->a:Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->refreshWidget()V

    .line 354
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->b:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    if-eqz v0, :cond_3

    .line 356
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->b:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->refreshWidget()V

    .line 358
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->c:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    if-eqz v0, :cond_4

    .line 360
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->c:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->refreshWidget()V

    .line 362
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->d:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    if-eqz v0, :cond_0

    .line 364
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->d:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->refreshWidget()V

    goto :goto_0

    .line 367
    :cond_5
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->AccountEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    if-ne v0, v1, :cond_0

    .line 368
    sget-object v0, Lcom/sec/android/app/samsungapps/viewpager/k;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getAccountEvent()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;->getAccountEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 372
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->a:Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;

    if-eqz v0, :cond_6

    .line 374
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->a:Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->refreshWidget()V

    .line 376
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->b:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    if-eqz v0, :cond_7

    .line 378
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->b:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->refreshWidget()V

    .line 380
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->c:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    if-eqz v0, :cond_8

    .line 382
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->c:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->refreshWidget()V

    .line 384
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->d:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    if-eqz v0, :cond_0

    .line 386
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->d:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->refreshWidget()V

    goto :goto_0

    .line 368
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public handleUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V
    .locals 0

    .prologue
    .line 112
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 50
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->onCreate(Landroid/os/Bundle;)V

    .line 51
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->addSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V

    .line 52
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const v5, 0x7f0c0049

    const/16 v4, 0x8

    .line 90
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 92
    const v0, 0x7f040009

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->setMainView(I)V

    .line 94
    const v0, 0x7f0c0048

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->a:Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->a:Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;

    new-instance v2, Lcom/sec/android/app/samsungapps/viewpager/g;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/viewpager/g;-><init>(Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->setListener(Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->a:Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;

    new-instance v2, Lcom/sec/android/app/samsungapps/widget/ChartBannerWidgetHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/android/app/samsungapps/widget/ChartBannerWidgetHelper;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->setWidgetData(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->a:Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->loadWidget()V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isFreeStore()Z

    move-result v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_0

    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->b:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->b:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->setType(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->b:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    new-instance v2, Lcom/sec/android/app/samsungapps/viewpager/h;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/viewpager/h;-><init>(Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->setListener(Lcom/sec/android/app/samsungapps/widget/interfaces/IHorizontalContentListWidgetClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->b:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    new-instance v2, Lcom/sec/android/app/samsungapps/widget/HorizontalContentListWidgetHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/android/app/samsungapps/widget/HorizontalContentListWidgetHelper;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->setWidgetData(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->b:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->loadWidget()V

    :goto_0
    const v0, 0x7f0c004a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->c:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->c:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->setType(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->c:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    new-instance v2, Lcom/sec/android/app/samsungapps/viewpager/i;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/viewpager/i;-><init>(Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->setListener(Lcom/sec/android/app/samsungapps/widget/interfaces/IHorizontalContentListWidgetClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->c:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    new-instance v2, Lcom/sec/android/app/samsungapps/widget/HorizontalContentListWidgetHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/android/app/samsungapps/widget/HorizontalContentListWidgetHelper;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->setWidgetData(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->c:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->loadWidget()V

    const v0, 0x7f0c004b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->d:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->d:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->setType(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->d:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    new-instance v2, Lcom/sec/android/app/samsungapps/viewpager/j;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/viewpager/j;-><init>(Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->setListener(Lcom/sec/android/app/samsungapps/widget/interfaces/IHorizontalContentListWidgetClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->d:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    new-instance v2, Lcom/sec/android/app/samsungapps/widget/HorizontalContentListWidgetHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/android/app/samsungapps/widget/HorizontalContentListWidgetHelper;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->setWidgetData(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->d:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->loadWidget()V

    .line 96
    return-object v1

    .line 94
    :cond_0
    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->b:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->b:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->setVisibility(I)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 57
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->removeSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V

    .line 58
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->onDestroy()V

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->a:Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->a:Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->release()V

    .line 63
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->a:Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->b:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    if-eqz v0, :cond_1

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->b:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->release()V

    .line 69
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->b:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    .line 72
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->c:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    if-eqz v0, :cond_2

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->c:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->release()V

    .line 75
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->c:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    .line 78
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->d:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    if-eqz v0, :cond_3

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->d:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->release()V

    .line 81
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->d:Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;

    .line 84
    :cond_3
    invoke-static {}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->getInstance()Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->removeObserver(Lcom/sec/android/app/samsungapps/uieventmanager/UIEventObserver;)Z

    .line 85
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 102
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->onResume()V

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->a:Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ChartViewFragment;->a:Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->initHover()V

    .line 106
    :cond_0
    return-void
.end method

.method public setResizeInMultiWindow()V
    .locals 0

    .prologue
    .line 336
    return-void
.end method
