.class public Lcom/sec/android/app/samsungapps/initializer/KnoxAppsInitProcess;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess$IAppsInitProcessData;


# instance fields
.field a:Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;

.field private b:Landroid/app/Activity;

.field private c:Lcom/sec/android/app/samsungapps/initializer/KnoxAppsInitProcess$IKnoxAppsInitProcessData;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Z)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/KnoxAppsInitProcess;->d:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;

    .line 22
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/initializer/KnoxAppsInitProcess;->b:Landroid/app/Activity;

    .line 24
    new-instance v0, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;

    invoke-direct {v0, p1, p2}, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;-><init>(Landroid/app/Activity;Z)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/KnoxAppsInitProcess;->a:Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;

    .line 25
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/initializer/KnoxAppsInitProcess;)Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/KnoxAppsInitProcess;->d:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/initializer/KnoxAppsInitProcess;)Lcom/sec/android/app/samsungapps/initializer/KnoxAppsInitProcess$IKnoxAppsInitProcessData;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/KnoxAppsInitProcess;->c:Lcom/sec/android/app/samsungapps/initializer/KnoxAppsInitProcess$IKnoxAppsInitProcessData;

    return-object v0
.end method


# virtual methods
.method public onInitCompleted()V
    .locals 2

    .prologue
    .line 72
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/initializer/KnoxAppsInitProcess;->requestGetKnoxMainHome()V

    .line 81
    :goto_0
    return-void

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/KnoxAppsInitProcess;->c:Lcom/sec/android/app/samsungapps/initializer/KnoxAppsInitProcess$IKnoxAppsInitProcessData;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/initializer/KnoxAppsInitProcess$IKnoxAppsInitProcessData;->onInitCompleted(I)V

    goto :goto_0
.end method

.method public onInitFailed()V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/KnoxAppsInitProcess;->c:Lcom/sec/android/app/samsungapps/initializer/KnoxAppsInitProcess$IKnoxAppsInitProcessData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/initializer/KnoxAppsInitProcess$IKnoxAppsInitProcessData;->onInitFailed()V

    .line 86
    return-void
.end method

.method public requestGetKnoxMainHome()V
    .locals 3

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/KnoxAppsInitProcess;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/initializer/KnoxAppsInitProcess;->d:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;

    new-instance v2, Lcom/sec/android/app/samsungapps/initializer/l;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/initializer/l;-><init>(Lcom/sec/android/app/samsungapps/initializer/KnoxAppsInitProcess;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->getKnoxMainHome(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 66
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/initializer/KnoxAppsInitProcess;->b:Landroid/app/Activity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 67
    return-void
.end method

.method public startThreadForKnoxInitializingData(Lcom/sec/android/app/samsungapps/initializer/KnoxAppsInitProcess$IKnoxAppsInitProcessData;)V
    .locals 1

    .prologue
    .line 29
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/initializer/KnoxAppsInitProcess;->c:Lcom/sec/android/app/samsungapps/initializer/KnoxAppsInitProcess$IKnoxAppsInitProcessData;

    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/initializer/KnoxAppsInitProcess;->a:Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess;->startThreadForInitializingData(Lcom/sec/android/app/samsungapps/initializer/AppsInitProcess$IAppsInitProcessData;)V

    .line 31
    return-void
.end method
