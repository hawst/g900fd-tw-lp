.class public interface abstract Lcom/sec/android/app/samsungapps/downloadhelper/IContentDownloadObserver;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract notifyDownloadCompleted(Ljava/lang/String;Z)V
.end method

.method public abstract notifyDownloadProgress(Ljava/lang/String;JJ)V
.end method

.method public abstract notifyInstallCompleted(Ljava/lang/String;Z)V
.end method

.method public abstract notifyStartDownload(Ljava/lang/String;)V
.end method

.method public abstract notifyStartInstall(Ljava/lang/String;)V
.end method
