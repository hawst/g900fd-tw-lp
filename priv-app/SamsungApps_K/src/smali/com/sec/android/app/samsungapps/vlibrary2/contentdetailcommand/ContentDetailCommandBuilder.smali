.class public Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/ContentDetailCommandBuilder;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand$IGetCategoryProductListCommandData;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand$IGetSellerProductListCommandData;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/IContentDetailCommandBuilder;


# instance fields
.field private _IContentDetailMain:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;)V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/ContentDetailCommandBuilder;->_IContentDetailMain:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    .line 14
    return-void
.end method


# virtual methods
.method public getCategoryProductListCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetCategoryProductListCommand$IGetCategoryProductListCommandData;)V

    return-object v0
.end method

.method public getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/ContentDetailCommandBuilder;->_IContentDetailMain:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    return-object v0
.end method

.method public getSellerProductListCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 2

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/ContentDetailCommandBuilder;->_IContentDetailMain:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    if-eqz v0, :cond_0

    .line 20
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/ContentDetailCommandBuilder;->_IContentDetailMain:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;->getVProductID()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand$IGetSellerProductListCommandData;Ljava/lang/String;)V

    .line 23
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;

    const-string v1, ""

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/GetSellerProductListCommand$IGetSellerProductListCommandData;Ljava/lang/String;)V

    goto :goto_0
.end method
