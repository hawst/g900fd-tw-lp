.class public Lcom/sec/android/app/samsungapps/networkstatereceiver/NetworkStateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ProGuard"


# static fields
.field private static a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/samsungapps/networkstatereceiver/NetworkStateReceiver;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 18
    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 20
    if-nez v0, :cond_0

    .line 46
    :goto_0
    return-void

    .line 25
    :cond_0
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 26
    if-nez v0, :cond_1

    .line 27
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/samsungapps/networkstatereceiver/NetworkStateReceiver;->a:Z

    goto :goto_0

    .line 31
    :cond_1
    sget-boolean v0, Lcom/sec/android/app/samsungapps/networkstatereceiver/NetworkStateReceiver;->a:Z

    if-nez v0, :cond_2

    .line 33
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/networkstatereceiver/WaitQueueForNetworkActivate;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/networkstatereceiver/WaitQueueForNetworkActivate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/networkstatereceiver/WaitQueueForNetworkActivate;->notifyNetworkActivated()V

    .line 35
    :cond_2
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/samsungapps/autoupdateservice/AutoUpdateService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/samsungapps/preloadupdate/PreloadUpdateService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 36
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/samsungapps/networkstatereceiver/NetworkStateReceiver;->a:Z

    goto :goto_0
.end method
