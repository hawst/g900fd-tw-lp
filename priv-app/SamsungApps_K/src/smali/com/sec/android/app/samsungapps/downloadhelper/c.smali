.class final Lcom/sec/android/app/samsungapps/downloadhelper/c;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/Downloader$IDownloadSingleItemResult;


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field final synthetic c:Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/downloadhelper/c;->c:Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;

    iput-object p2, p0, Lcom/sec/android/app/samsungapps/downloadhelper/c;->a:Landroid/content/Context;

    iput-object p3, p0, Lcom/sec/android/app/samsungapps/downloadhelper/c;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 109
    const/4 v0, -0x4

    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 113
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 114
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    const/4 v0, 0x1

    .line 119
    :goto_0
    return v0

    :catch_0
    move-exception v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final onDownloadCanceled()V
    .locals 0

    .prologue
    .line 175
    return-void
.end method

.method public final onDownloadFailed()V
    .locals 0

    .prologue
    .line 169
    return-void
.end method

.method public final onDownloadSuccess()V
    .locals 0

    .prologue
    .line 163
    return-void
.end method

.method public final onInstallFailedWithErrCode(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 124
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadhelper/c;->a:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadhelper/c;->a:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-nez v0, :cond_1

    .line 157
    :cond_0
    :goto_0
    return-void

    .line 128
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadhelper/c;->a:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 129
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 133
    new-instance v1, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadhelper/c;->a:Landroid/content/Context;

    invoke-direct {v1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;)V

    .line 134
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadhelper/c;->a:Landroid/content/Context;

    const v2, 0x7f080276

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 136
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/downloadhelper/c;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadhelper/c;->a:Landroid/content/Context;

    const v2, 0x7f080168

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 150
    :goto_1
    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadhelper/c;->a:Landroid/content/Context;

    const v2, 0x7f0802ef

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 156
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->show()V

    goto :goto_0

    .line 143
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadhelper/c;->a:Landroid/content/Context;

    const v2, 0x7f0802c2

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 145
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 146
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 147
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final onPaymentSuccess()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadhelper/c;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->isFreeContent()Z

    move-result v0

    if-ne v0, v5, :cond_1

    .line 193
    :cond_0
    :goto_0
    return-void

    .line 184
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadhelper/c;->a:Landroid/content/Context;

    instance-of v0, v0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/downloadhelper/c;->a:Landroid/content/Context;

    move-object v4, v0

    check-cast v4, Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    .line 186
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->PURCHASE_COMPLETE:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getPreviousPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/downloadhelper/c;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/downloadhelper/c;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->isLinkApp()Z

    move-result v3

    if-ne v3, v5, :cond_2

    sget-object v3, Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;->GOOGLE:Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;

    :goto_1
    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getEntryPoint()Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/downloadhelper/c;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getVcategoryID()Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendClickPurchaseLog(Lcom/sec/android/app/samsungapps/tobelog/LogEvent;Lcom/sec/android/app/samsungapps/tobelog/LogPage;Ljava/lang/String;Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    sget-object v3, Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;->SAMSUNG:Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;

    goto :goto_1
.end method

.method public final onProgress(JJ)V
    .locals 0

    .prologue
    .line 105
    return-void
.end method

.method public final onStateChanged()V
    .locals 0

    .prologue
    .line 99
    return-void
.end method
