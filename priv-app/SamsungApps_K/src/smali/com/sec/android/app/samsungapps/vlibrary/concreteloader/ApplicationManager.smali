.class public Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field public static final INSTALL_FAILED_ALREADY_EXISTS:I = -0x1

.field public static final INSTALL_FAILED_CONFLICTING_PROVIDER:I = -0xd

.field public static final INSTALL_FAILED_CONTAINER_ERROR:I = -0x12

.field public static final INSTALL_FAILED_CPU_ABI_INCOMPATIBLE:I = -0x10

.field public static final INSTALL_FAILED_DEXOPT:I = -0xb

.field public static final INSTALL_FAILED_DUPLICATE_PACKAGE:I = -0x5

.field public static final INSTALL_FAILED_INSUFFICIENT_STORAGE:I = -0x4

.field public static final INSTALL_FAILED_INTERNAL_ERROR:I = -0x6e

.field public static final INSTALL_FAILED_INVALID_APK:I = -0x2

.field public static final INSTALL_FAILED_INVALID_INSTALL_LOCATION:I = -0x13

.field public static final INSTALL_FAILED_INVALID_URI:I = -0x3

.field public static final INSTALL_FAILED_MEDIA_UNAVAILABLE:I = -0x14

.field public static final INSTALL_FAILED_MISSING_FEATURE:I = -0x11

.field public static final INSTALL_FAILED_MISSING_SHARED_LIBRARY:I = -0x9

.field public static final INSTALL_FAILED_NEWER_SDK:I = -0xe

.field public static final INSTALL_FAILED_NO_SHARED_USER:I = -0x6

.field public static final INSTALL_FAILED_OLDER_SDK:I = -0xc

.field public static final INSTALL_FAILED_PACKAGE_CHANGED:I = -0x17

.field public static final INSTALL_FAILED_REPLACE_COULDNT_DELETE:I = -0xa

.field public static final INSTALL_FAILED_SHARED_USER_INCOMPATIBLE:I = -0x8

.field public static final INSTALL_FAILED_TEST_ONLY:I = -0xf

.field public static final INSTALL_FAILED_UID_CHANGED:I = -0x18

.field public static final INSTALL_FAILED_UPDATE_INCOMPATIBLE:I = -0x7

.field public static final INSTALL_FAILED_USER_RESTRICTED:I = -0x6f

.field public static final INSTALL_FAILED_VERIFICATION_FAILURE:I = -0x16

.field public static final INSTALL_FAILED_VERIFICATION_TIMEOUT:I = -0x15

.field public static final INSTALL_FAILED_VERSION_DOWNGRADE:I = -0x19

.field public static final INSTALL_PARSE_FAILED_BAD_MANIFEST:I = -0x65

.field public static final INSTALL_PARSE_FAILED_BAD_PACKAGE_NAME:I = -0x6a

.field public static final INSTALL_PARSE_FAILED_BAD_SHARED_USER_ID:I = -0x6b

.field public static final INSTALL_PARSE_FAILED_CERTIFICATE_ENCODING:I = -0x69

.field public static final INSTALL_PARSE_FAILED_INCONSISTENT_CERTIFICATES:I = -0x68

.field public static final INSTALL_PARSE_FAILED_MANIFEST_EMPTY:I = -0x6d

.field public static final INSTALL_PARSE_FAILED_MANIFEST_MALFORMED:I = -0x6c

.field public static final INSTALL_PARSE_FAILED_NOT_APK:I = -0x64

.field public static final INSTALL_PARSE_FAILED_NO_CERTIFICATES:I = -0x67

.field public static final INSTALL_PARSE_FAILED_UNEXPECTED_EXCEPTION:I = -0x66

.field public static final INSTALL_SUCCEEDED:I = 0x1


# instance fields
.field public final INSTALL_REPLACE_EXISTING:I

.field private deleteHandler:Landroid/os/Handler;

.field private deleteObserver:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/c;

.field private handler:Landroid/os/Handler;

.field isSuccessful:Z

.field private method:Ljava/lang/reflect/Method;

.field private methodForExistingPackage:Ljava/lang/reflect/Method;

.field private observer:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/d;

.field private onInstalledPackaged:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/OnInstalledPackaged;

.field private onPackageDeleted:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/OnPackageDeleted;

.field pkgname:Ljava/lang/String;

.field private pm:Landroid/content/pm/PackageManager;

.field returncode:I

.field private uninstallmethod:Ljava/lang/reflect/Method;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 440
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput v4, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->INSTALL_REPLACE_EXISTING:I

    .line 409
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/a;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->handler:Landroid/os/Handler;

    .line 416
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/b;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->deleteHandler:Landroid/os/Handler;

    .line 442
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/d;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->observer:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/d;

    .line 443
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/c;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->deleteObserver:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/c;

    .line 444
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->pm:Landroid/content/pm/PackageManager;

    .line 446
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Class;

    const-class v1, Landroid/net/Uri;

    aput-object v1, v0, v2

    const-class v1, Landroid/content/pm/IPackageInstallObserver;

    aput-object v1, v0, v3

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v1, v0, v4

    const-class v1, Ljava/lang/String;

    aput-object v1, v0, v5

    .line 447
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->pm:Landroid/content/pm/PackageManager;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "installPackage"

    invoke-virtual {v1, v2, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->method:Ljava/lang/reflect/Method;

    .line 450
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->pm:Landroid/content/pm/PackageManager;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "installExistingPackage"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->methodForExistingPackage:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 457
    :goto_0
    const/4 v0, 0x3

    :try_start_1
    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Landroid/content/pm/IPackageDeleteObserver;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v2, v0, v1

    .line 458
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->pm:Landroid/content/pm/PackageManager;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "deletePackage"

    invoke-virtual {v1, v2, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->uninstallmethod:Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1

    .line 464
    :goto_1
    return-void

    .line 452
    :catch_0
    move-exception v0

    iput-object v6, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->methodForExistingPackage:Ljava/lang/reflect/Method;

    goto :goto_0

    .line 462
    :catch_1
    move-exception v0

    iput-object v6, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->uninstallmethod:Ljava/lang/reflect/Method;

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;)Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/OnInstalledPackaged;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->onInstalledPackaged:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/OnInstalledPackaged;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;)Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/OnPackageDeleted;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->onPackageDeleted:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/OnPackageDeleted;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->deleteHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public installExistingPackage(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 501
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->methodForExistingPackage:Ljava/lang/reflect/Method;

    if-nez v0, :cond_0

    .line 502
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Can not find installExistingPackage method."

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 504
    :cond_0
    const/4 v1, 0x0

    .line 506
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "installExistingPackage:"

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 509
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->methodForExistingPackage:Ljava/lang/reflect/Method;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->pm:Landroid/content/pm/PackageManager;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-virtual {v0, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 510
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->returncode:I

    .line 511
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->returncode:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v0, v2, :cond_3

    move-object v0, v1

    move v1, v2

    .line 524
    :goto_0
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->pkgname:Ljava/lang/String;

    .line 526
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->onInstalledPackaged:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/OnInstalledPackaged;

    if-eqz v4, :cond_2

    .line 527
    iget v4, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->returncode:I

    if-eq v4, v2, :cond_1

    .line 530
    const-string v4, "[ERROR] Existing Package install return code : %d"

    new-array v2, v2, [Ljava/lang/Object;

    iget v5, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->returncode:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v3

    invoke-static {v4, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    .line 532
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    .line 533
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->handler:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 536
    :cond_2
    if-nez v1, :cond_4

    if-eqz v0, :cond_4

    .line 537
    throw v0

    .line 516
    :cond_3
    :try_start_1
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Failed to install existing package."

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move v1, v3

    .line 522
    goto :goto_0

    .line 518
    :catch_0
    move-exception v0

    .line 519
    const/16 v1, -0x6e

    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->returncode:I

    move v1, v3

    .line 521
    goto :goto_0

    .line 539
    :cond_4
    return-void
.end method

.method public installPackage(Landroid/net/Uri;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    .line 496
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "installPackage:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 497
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->method:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->pm:Landroid/content/pm/PackageManager;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->observer:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/d;

    aput-object v4, v2, v3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x3

    const/4 v4, 0x0

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 498
    return-void
.end method

.method public installPackage(Ljava/io/File;)V
    .locals 1

    .prologue
    .line 486
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 488
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 491
    :cond_0
    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 492
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->installPackage(Landroid/net/Uri;)V

    .line 493
    return-void
.end method

.method public installPackage(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 482
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->installPackage(Ljava/io/File;)V

    .line 483
    return-void
.end method

.method public setOnDeletePackage(Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/OnPackageDeleted;)V
    .locals 0

    .prologue
    .line 472
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->onPackageDeleted:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/OnPackageDeleted;

    .line 473
    return-void
.end method

.method public setOnInstalledPackaged(Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/OnInstalledPackaged;)V
    .locals 0

    .prologue
    .line 467
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->onInstalledPackaged:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/OnInstalledPackaged;

    .line 468
    return-void
.end method

.method public uninstallPackage(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 476
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->uninstallmethod:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    .line 477
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->uninstallmethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->pm:Landroid/content/pm/PackageManager;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v5

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ApplicationManager;->deleteObserver:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/c;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 479
    :cond_0
    return-void
.end method
