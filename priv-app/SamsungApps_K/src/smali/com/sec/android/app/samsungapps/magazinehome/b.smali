.class final Lcom/sec/android/app/samsungapps/magazinehome/b;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/magazinehome/a;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/magazinehome/a;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/magazinehome/b;->a:Lcom/sec/android/app/samsungapps/magazinehome/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceiveResult(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 4

    .prologue
    .line 78
    const/4 v0, 0x1

    if-ne p2, v0, :cond_2

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/b;->a:Lcom/sec/android/app/samsungapps/magazinehome/a;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/magazinehome/a;->a:Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->a(Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v0

    .line 81
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/b;->a:Lcom/sec/android/app/samsungapps/magazinehome/a;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/magazinehome/a;->a:Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->stopService()V

    .line 98
    :goto_0
    return-void

    .line 85
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/magazinehome/b;->a:Lcom/sec/android/app/samsungapps/magazinehome/a;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/magazinehome/a;->a:Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->size()I

    move-result v2

    const/16 v3, 0xa

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->a(Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;I)I

    .line 86
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/magazinehome/b;->a:Lcom/sec/android/app/samsungapps/magazinehome/a;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/magazinehome/a;->a:Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->size()I

    move-result v2

    const/16 v3, 0x14

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->b(Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;I)I

    .line 88
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/magazinehome/b;->a:Lcom/sec/android/app/samsungapps/magazinehome/a;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/magazinehome/a;->a:Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->requestImages(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;)V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 89
    :catch_0
    move-exception v0

    .line 90
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MagazineHomeService::ArrayIndexOutOfBoundsException::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/b;->a:Lcom/sec/android/app/samsungapps/magazinehome/a;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/magazinehome/a;->a:Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->stopService()V

    goto :goto_0

    .line 94
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/magazinehome/b;->a:Lcom/sec/android/app/samsungapps/magazinehome/a;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/magazinehome/a;->a:Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/magazinehome/MagazineHomeService;->stopService()V

    goto :goto_0
.end method
