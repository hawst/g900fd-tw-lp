.class public Lcom/sec/android/app/samsungapps/view/CategoryItemListAdapter;
.super Landroid/widget/BaseAdapter;
.source "ProGuard"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Landroid/view/LayoutInflater;

.field private c:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListAdapter;->a:Landroid/content/Context;

    .line 22
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListAdapter;->a:Landroid/content/Context;

    .line 23
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListAdapter;->c:Ljava/util/ArrayList;

    .line 24
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListAdapter;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/sec/android/app/samsungapps/view/h;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListAdapter;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/view/h;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/view/CategoryItemListAdapter;->getItem(I)Lcom/sec/android/app/samsungapps/view/h;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 38
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/16 v6, 0x8

    const/4 v1, 0x0

    .line 43
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/view/CategoryItemListAdapter;->getItem(I)Lcom/sec/android/app/samsungapps/view/h;

    move-result-object v3

    .line 45
    if-nez p2, :cond_4

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListAdapter;->a:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListAdapter;->b:Landroid/view/LayoutInflater;

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListAdapter;->b:Landroid/view/LayoutInflater;

    const v2, 0x7f040051

    invoke-virtual {v0, v2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 48
    new-instance v0, Lcom/sec/android/app/samsungapps/view/d;

    invoke-direct {v0, p0, p2, v1}, Lcom/sec/android/app/samsungapps/view/d;-><init>(Lcom/sec/android/app/samsungapps/view/CategoryItemListAdapter;Landroid/view/View;B)V

    .line 49
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v2, v0

    .line 54
    :goto_0
    if-eqz v3, :cond_3

    .line 55
    iget-object v4, v3, Lcom/sec/android/app/samsungapps/view/h;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    .line 56
    iget-object v0, v2, Lcom/sec/android/app/samsungapps/view/d;->a:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, v2, Lcom/sec/android/app/samsungapps/view/d;->a:Landroid/widget/TextView;

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    :cond_0
    const-string v0, "1"

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->getNewCategoryYn()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_1
    iget-object v5, v2, Lcom/sec/android/app/samsungapps/view/d;->c:Landroid/widget/ImageView;

    if-eqz v5, :cond_1

    if-eqz v0, :cond_6

    iget-object v0, v2, Lcom/sec/android/app/samsungapps/view/d;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 58
    :cond_1
    :goto_2
    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/view/h;->a()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 59
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-object v3, v2, Lcom/sec/android/app/samsungapps/view/d;->d:Landroid/widget/ImageView;

    if-eqz v3, :cond_2

    if-eqz v0, :cond_7

    iget-object v0, v2, Lcom/sec/android/app/samsungapps/view/d;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 60
    :cond_2
    :goto_3
    invoke-static {v2, v6, v7, v7}, Lcom/sec/android/app/samsungapps/view/d;->a(Lcom/sec/android/app/samsungapps/view/d;ILcom/sec/android/app/samsungapps/vlibrary/doc/Category;Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;)V

    .line 61
    add-int/lit8 v0, p1, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/CategoryItemListAdapter;->getCount()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 62
    add-int/lit8 v0, p1, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/CategoryItemListAdapter;->getItem(I)Lcom/sec/android/app/samsungapps/view/h;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/view/h;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    invoke-static {v2, v1, v4, v0}, Lcom/sec/android/app/samsungapps/view/d;->a(Lcom/sec/android/app/samsungapps/view/d;ILcom/sec/android/app/samsungapps/vlibrary/doc/Category;Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;)V

    .line 65
    :cond_3
    return-object p2

    .line 51
    :cond_4
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/view/d;

    move-object v2, v0

    goto :goto_0

    :cond_5
    move v0, v1

    .line 57
    goto :goto_1

    :cond_6
    iget-object v0, v2, Lcom/sec/android/app/samsungapps/view/d;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 59
    :cond_7
    iget-object v0, v2, Lcom/sec/android/app/samsungapps/view/d;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_3
.end method
