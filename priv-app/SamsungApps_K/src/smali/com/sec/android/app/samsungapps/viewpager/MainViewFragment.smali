.class public Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;
.super Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/uieventmanager/UIEventObserver;
.implements Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;

.field private b:Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;

.field private c:Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;

.field private d:Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;

.field private e:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 54
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;-><init>()V

    .line 56
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->a:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;

    .line 57
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->b:Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;

    .line 58
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->c:Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;

    .line 59
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->d:Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;

    .line 61
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->e:Z

    return-void
.end method

.method private a()V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 186
    const v0, 0x7f0c01b9

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 187
    const v0, 0x7f0c01ba

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 188
    const v1, 0x7f0c01bb

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 189
    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 191
    :cond_0
    const-string v0, "MainViewFragment_registerStringServiceInfo::TextView is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 214
    :goto_0
    return-void

    .line 195
    :cond_1
    const-string v3, "<u>Samsung Electronics Co., Ltd.</u>"

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 197
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v3

    .line 198
    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v3

    .line 199
    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isKorea()Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    .line 201
    const v3, 0x7f080244

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 202
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 203
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 204
    new-instance v0, Lcom/sec/android/app/samsungapps/viewpager/s;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/viewpager/s;-><init>(Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;)V

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 213
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v2, 0x7f0800a4

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 208
    :cond_2
    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 209
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 210
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method private b()Lcom/sec/android/app/samsungapps/widget/interfaces/ISmallBannerClickListener;
    .locals 1

    .prologue
    .line 300
    new-instance v0, Lcom/sec/android/app/samsungapps/viewpager/u;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/viewpager/u;-><init>(Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;)V

    return-object v0
.end method


# virtual methods
.method public handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 431
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 467
    :cond_0
    :goto_0
    return v3

    .line 436
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->REAL_AGE_NAME_VERIFIED:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    if-ne v0, v1, :cond_2

    .line 438
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->b:Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;

    if-eqz v0, :cond_0

    .line 440
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->b:Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->refreshWidget()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 463
    :catch_0
    move-exception v0

    .line 464
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MainViewFragment::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 443
    :cond_2
    :try_start_1
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->AccountEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    if-ne v0, v1, :cond_3

    .line 444
    sget-object v0, Lcom/sec/android/app/samsungapps/viewpager/v;->b:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getAccountEvent()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;->getAccountEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 448
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->b:Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;

    if-eqz v0, :cond_0

    .line 450
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->b:Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->refreshWidget()V

    goto :goto_0

    .line 457
    :cond_3
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->CheckAppUpgradeEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    if-ne v0, v1, :cond_0

    .line 458
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getCheckAppUpgradeEvent()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/CheckAppUpgradeEvent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/CheckAppUpgradeEvent;->getCheckAppUpgradeEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/CheckAppUpgradeEvent$CheckAppUpgradeEventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/CheckAppUpgradeEvent$CheckAppUpgradeEventType;->CheckAppUpgradeUpdated:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/CheckAppUpgradeEvent$CheckAppUpgradeEventType;

    if-ne v0, v1, :cond_0

    .line 459
    const-string v0, "GOT CheckAppUpgradeEvent"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 460
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->a()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 444
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public handleUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V
    .locals 6

    .prologue
    .line 153
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getEventType()Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    move-result-object v0

    .line 154
    sget-object v1, Lcom/sec/android/app/samsungapps/viewpager/v;->a:[I

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 176
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MainViewFragment::handleUIEvent::event="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 179
    :cond_0
    :goto_0
    return-void

    .line 160
    :pswitch_0
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getContentDisplayEvent()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->getContentDisplayEventType()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    move-result-object v0

    .line 161
    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;->DisplayContentDetail:Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    if-ne v0, v1, :cond_1

    .line 163
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getContentDisplayEvent()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v1

    const/16 v2, 0x8

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;I)V

    goto :goto_0

    .line 166
    :cond_1
    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;->DisplayBannerContentList:Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    if-ne v0, v1, :cond_0

    .line 168
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getContentDisplayEvent()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    move-result-object v4

    .line 170
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->ProductSetList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->getDescription()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->getID()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x4

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 154
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onBannerClick(Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 318
    .line 320
    if-nez p1, :cond_0

    .line 322
    const-string v0, "MainViewFragment::onBannerClick::Not Ready Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 379
    :goto_0
    return v1

    .line 326
    :cond_0
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->needProductDetail()Z

    move-result v2

    if-ne v2, v0, :cond_1

    .line 331
    invoke-static {}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->getInstance()Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;

    move-result-object v1

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->getContentDetailContainer()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->showContentDetail(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    .line 333
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->getBannerIndex()I

    move-result v1

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;->CONTENTID:Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->getProductID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, p1, v1, v2, v3}, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->sendBannerClickLog(Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;ILcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;Ljava/lang/String;)V

    :goto_1
    move v1, v0

    .line 379
    goto :goto_0

    .line 335
    :cond_1
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->isURLBanner()Z

    move-result v2

    if-ne v2, v0, :cond_3

    .line 340
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->getBannerLinkURL()Ljava/lang/String;

    move-result-object v1

    .line 341
    if-eqz v1, :cond_2

    const-string v2, "samsungapps://"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-ne v2, v0, :cond_2

    .line 346
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 347
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 348
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->createDeepLink(Landroid/content/Intent;)V

    .line 349
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDeepLink()Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->setIsInternal(Z)V

    .line 350
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->runDeepLink()Z

    .line 358
    :goto_2
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->getBannerIndex()I

    move-result v2

    sget-object v3, Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;->URL:Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;

    invoke-virtual {p0, p1, v2, v3, v1}, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->sendBannerClickLog(Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;ILcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;Ljava/lang/String;)V

    goto :goto_1

    .line 354
    :cond_2
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 355
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->commonStartActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    goto :goto_2

    .line 360
    :cond_3
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->needGetProductSetList()Z

    move-result v2

    if-ne v2, v0, :cond_4

    .line 366
    invoke-static {}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->getInstance()Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;

    move-result-object v1

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->getProductID()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->getBannerDescription()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->showBannerContentList(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->getBannerIndex()I

    move-result v1

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;->CONTENTSET:Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->getProductID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, p1, v1, v2, v3}, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->sendBannerClickLog(Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;ILcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 376
    :cond_4
    const-string v0, "MainViewFragment::onBannerClick::Not defined"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move v0, v1

    goto/16 :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 68
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->onCreate(Landroid/os/Bundle;)V

    .line 69
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->addSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V

    .line 70
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 129
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 131
    const v0, 0x7f040074

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->setMainView(I)V

    .line 133
    new-instance v2, Lcom/sec/android/app/samsungapps/widget/BigBannerWidgetHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/sec/android/app/samsungapps/widget/BigBannerWidgetHelper;-><init>(Landroid/content/Context;)V

    const v0, 0x7f0c01b6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->a:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->a:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;

    new-instance v3, Lcom/sec/android/app/samsungapps/viewpager/t;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/viewpager/t;-><init>(Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;)V

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->setWidgetClickListener(Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->a:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->setWidgetData(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->a:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->setBannerData(Lcom/sec/android/app/samsungapps/widget/interfaces/IBannerData;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->a:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->loadWidget()V

    const v0, 0x7f0c0078

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->b:Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->b:Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->loadWidget()V

    new-instance v2, Lcom/sec/android/app/samsungapps/widget/BannerWidgetHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/sec/android/app/samsungapps/widget/BannerWidgetHelper;-><init>(Landroid/content/Context;)V

    const v0, 0x7f0c01b7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->c:Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->c:Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->setType(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->c:Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->b()Lcom/sec/android/app/samsungapps/widget/interfaces/ISmallBannerClickListener;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->setClickListener(Lcom/sec/android/app/samsungapps/widget/interfaces/ISmallBannerClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->c:Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->setWidgetData(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->c:Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->loadWidget()V

    const v0, 0x7f0c01b8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->d:Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->d:Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->setType(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->d:Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->b()Lcom/sec/android/app/samsungapps/widget/interfaces/ISmallBannerClickListener;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->setClickListener(Lcom/sec/android/app/samsungapps/widget/interfaces/ISmallBannerClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->d:Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->setWidgetData(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->d:Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->loadWidget()V

    .line 135
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->a()V

    .line 137
    return-object v1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 94
    invoke-static {}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->getInstance()Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->removeObserver(Lcom/sec/android/app/samsungapps/uieventmanager/UIEventObserver;)Z

    .line 95
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->removeSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V

    .line 97
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->setBigBannerHandler(Z)V

    .line 99
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->onDestroy()V

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->a:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->a:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->release()V

    .line 104
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->a:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->b:Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;

    if-eqz v0, :cond_1

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->b:Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;->release()V

    .line 110
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->b:Lcom/sec/android/app/samsungapps/widget/list/PromotionMainWidget;

    .line 113
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->c:Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;

    if-eqz v0, :cond_2

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->c:Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->release()V

    .line 116
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->c:Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;

    .line 119
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->d:Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;

    if-eqz v0, :cond_3

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->d:Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;->release()V

    .line 122
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->d:Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerWidget;

    .line 124
    :cond_3
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 75
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->onPause()V

    .line 77
    invoke-static {}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->getInstance()Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->removeObserver(Lcom/sec/android/app/samsungapps/uieventmanager/UIEventObserver;)Z

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->a:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->stopAutoRotation()V

    .line 79
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 84
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->onResume()V

    .line 86
    invoke-static {}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->getInstance()Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->addObserver(Lcom/sec/android/app/samsungapps/uieventmanager/UIEventObserver;)V

    .line 88
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->e:Z

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->setBigBannerHandler(Z)V

    .line 89
    return-void
.end method

.method public sendBannerClickLog(Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;ILcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 384
    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_BIG_BANNER:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    .line 385
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->isBigBanner()Z

    move-result v0

    if-ne v0, v3, :cond_1

    .line 386
    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_BIG_BANNER:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    .line 393
    :cond_0
    :goto_0
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->STAFF_PICKS:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    const/4 v8, 0x0

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, v2

    move-object v7, v2

    invoke-static/range {v0 .. v8}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendNormalClickLog(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;Ljava/lang/String;ILcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;Ljava/lang/String;Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;Ljava/lang/String;I)V

    .line 395
    return-void

    .line 387
    :cond_1
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->isFeaturedBanner()Z

    move-result v0

    if-ne v0, v3, :cond_2

    .line 388
    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_MIDDLE_BANNER:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    goto :goto_0

    .line 389
    :cond_2
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->isNewBanner()Z

    move-result v0

    if-ne v0, v3, :cond_0

    .line 390
    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_BOTTOM_BANNER:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    goto :goto_0
.end method

.method public setBigBannerHandler(Z)V
    .locals 1

    .prologue
    .line 402
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->e:Z

    .line 404
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->a:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;

    if-nez v0, :cond_0

    .line 406
    const-string v0, "MainViewFragmentsetBigBannerHandler::Widget is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 418
    :goto_0
    return-void

    .line 410
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 412
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->a:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->startAutoRotation()V

    goto :goto_0

    .line 416
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/MainViewFragment;->a:Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerWidget;->stopAutoRotation()V

    goto :goto_0
.end method

.method public setResizeInMultiWindow()V
    .locals 0

    .prologue
    .line 425
    return-void
.end method
