.class public Lcom/sec/android/app/samsungapps/vlibrary2/baselist/CalcListStEd;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x36e446edc9376948L


# instance fields
.field private _ItemCountPerPage:I

.field private mFirstEndNum:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/CalcListStEd;->mFirstEndNum:I

    .line 15
    iput p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/CalcListStEd;->_ItemCountPerPage:I

    .line 16
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/CalcListStEd;->mFirstEndNum:I

    .line 20
    iput p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/CalcListStEd;->_ItemCountPerPage:I

    .line 21
    iput p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/CalcListStEd;->mFirstEndNum:I

    .line 22
    return-void
.end method


# virtual methods
.method public getNextEndNumber(Lcom/sec/android/app/samsungapps/vlibrary2/baselist/CalcListStEd$ICalcListInfo;)I
    .locals 2

    .prologue
    .line 35
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/CalcListStEd$ICalcListInfo;->getEndNumber()I

    move-result v0

    .line 36
    if-nez v0, :cond_1

    .line 38
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/CalcListStEd;->mFirstEndNum:I

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/CalcListStEd;->_ItemCountPerPage:I

    if-le v0, v1, :cond_0

    .line 40
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/CalcListStEd;->mFirstEndNum:I

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/CalcListStEd;->_ItemCountPerPage:I

    div-int/2addr v0, v1

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/CalcListStEd;->_ItemCountPerPage:I

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/CalcListStEd;->_ItemCountPerPage:I

    add-int/2addr v0, v1

    .line 44
    :goto_0
    return v0

    .line 42
    :cond_0
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/CalcListStEd;->_ItemCountPerPage:I

    goto :goto_0

    .line 44
    :cond_1
    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/CalcListStEd;->_ItemCountPerPage:I

    div-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/CalcListStEd;->_ItemCountPerPage:I

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/CalcListStEd;->_ItemCountPerPage:I

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public getNextStartNumber(Lcom/sec/android/app/samsungapps/vlibrary2/baselist/CalcListStEd$ICalcListInfo;)I
    .locals 2

    .prologue
    .line 26
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/CalcListStEd$ICalcListInfo;->getEndNumber()I

    move-result v0

    .line 27
    if-nez v0, :cond_0

    .line 28
    const/4 v0, 0x1

    .line 30
    :goto_0
    return v0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/CalcListStEd;->_ItemCountPerPage:I

    div-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/CalcListStEd;->_ItemCountPerPage:I

    mul-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
