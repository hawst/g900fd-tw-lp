.class public Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;
.super Lcom/sec/android/app/samsungapps/vlibrary/net/RequestGET;
.source "ProGuard"


# instance fields
.field mContent:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

.field mContext:Landroid/content/Context;

.field mDestFileName:Ljava/lang/String;

.field mHeight:I

.field mHttpGet:Lorg/apache/http/client/methods/HttpGet;

.field mIsCanceled:Z

.field mURL:Ljava/lang/String;

.field mWidth:I

.field retryCount:I


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;Ljava/lang/String;Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 36
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestGET;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mURL:Ljava/lang/String;

    .line 26
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    .line 28
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->retryCount:I

    .line 30
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mWidth:I

    .line 31
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mHeight:I

    .line 32
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mContent:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 33
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mIsCanceled:Z

    .line 37
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mContent:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mContent:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getImgURL()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->checkStagingConditionURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mURL:Ljava/lang/String;

    .line 39
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mDestFileName:Ljava/lang/String;

    .line 40
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mContext:Landroid/content/Context;

    .line 41
    return-void
.end method

.method private checkStagingConditionURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 68
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getSAConfig()Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->isUsingStageURL()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 70
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getSAConfig()Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getStagingImgHostUrl()Ljava/lang/String;

    move-result-object v0

    .line 71
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 75
    :try_start_0
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 76
    invoke-virtual {v1}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v1

    .line 77
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    :cond_0
    :goto_0
    return-object p1

    .line 78
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/net/MalformedURLException;->printStackTrace()V

    goto :goto_0
.end method

.method private cropCenterBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 275
    if-nez p1, :cond_1

    .line 276
    const/4 p1, 0x0

    .line 308
    :cond_0
    :goto_0
    return-object p1

    .line 279
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    .line 280
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 282
    if-ge v1, p2, :cond_2

    if-lt v0, p3, :cond_0

    .line 289
    :cond_2
    if-le v1, p2, :cond_6

    .line 290
    sub-int v3, v1, p2

    div-int/lit8 v3, v3, 0x2

    .line 293
    :goto_1
    if-le v0, p3, :cond_3

    .line 294
    sub-int v2, v0, p3

    div-int/lit8 v2, v2, 0x2

    .line 300
    :cond_3
    if-le p2, v1, :cond_4

    move p2, v1

    .line 304
    :cond_4
    if-ge p3, v0, :cond_5

    move p3, v0

    .line 308
    :cond_5
    invoke-static {p1, v3, v2, p2, p3}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object p1

    goto :goto_0

    :cond_6
    move v3, v2

    goto :goto_1
.end method

.method private decodeBitmap(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 237
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 239
    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 241
    invoke-static {p1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 243
    return-object v0
.end method

.method private notifyImgDownloadResult(Z)V
    .locals 1

    .prologue
    .line 184
    if-nez p1, :cond_0

    .line 186
    const-string v0, "notifyimage result fail"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 189
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->notify(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V

    .line 190
    return-void
.end method


# virtual methods
.method public cancel()Z
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 215
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mIsCanceled:Z

    .line 216
    return v0
.end method

.method protected cropScaledCenterBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 249
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 250
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 253
    int-to-float v2, v0

    const v3, 0x3f4ccccd    # 0.8f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 254
    sub-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    .line 258
    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    .line 261
    invoke-static {p1, v0, v1, v2, v2}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 263
    const/4 v0, 0x1

    invoke-static {v1, p3, p3, v0}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 264
    if-nez v2, :cond_0

    .line 265
    const/4 v0, 0x0

    .line 271
    :goto_0
    return-object v0

    .line 268
    :cond_0
    invoke-direct {p0, v2, p2, p3}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->cropCenterBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 269
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 270
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0
.end method

.method public getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mContent:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    return-object v0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mDestFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mHeight:I

    return v0
.end method

.method public getHttpGet()Lorg/apache/http/client/methods/HttpGet;
    .locals 4

    .prologue
    .line 45
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mURL:Ljava/lang/String;

    .line 46
    const/4 v0, 0x0

    .line 47
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getSAConfig()Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->isUsingStageURL()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 49
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getSAConfig()Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getStagingImgHostUrl()Ljava/lang/String;

    move-result-object v2

    .line 50
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    .line 52
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/URLHostReplacer;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mURL:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/URLHostReplacer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/URLHostReplacer;->getNewURL()Ljava/lang/String;

    move-result-object v1

    .line 54
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/URLHostReplacer;->getOldHost()Ljava/lang/String;

    move-result-object v0

    .line 57
    :cond_0
    new-instance v2, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v2, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    .line 58
    if-eqz v0, :cond_1

    .line 60
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    const-string v2, "Host"

    invoke-virtual {v1, v2, v0}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    return-object v0
.end method

.method public getThreadNo()I
    .locals 1

    .prologue
    .line 227
    const/4 v0, 0x1

    return v0
.end method

.method public getURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mURL:Ljava/lang/String;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mWidth:I

    return v0
.end method

.method public invalidRetryCount()V
    .locals 1

    .prologue
    .line 210
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->retryCount:I

    .line 211
    return-void
.end method

.method public isCanceled()Z
    .locals 1

    .prologue
    .line 221
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mIsCanceled:Z

    return v0
.end method

.method public isSucceed()Z
    .locals 1

    .prologue
    .line 199
    const/4 v0, 0x1

    return v0
.end method

.method public needRetry()Z
    .locals 2

    .prologue
    .line 205
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->retryCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->retryCount:I

    .line 206
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->retryCount:I

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onFail()V
    .locals 1

    .prologue
    .line 232
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->notifyImgDownloadResult(Z)V

    .line 233
    return-void
.end method

.method public onResult(Lorg/apache/http/HttpResponse;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 108
    .line 112
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mIsCanceled:Z

    if-ne v0, v4, :cond_1

    .line 113
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Http request will be aborted : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mURL:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_9
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_c
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 116
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {v0}, Lorg/apache/http/client/methods/HttpGet;->abort()V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_c
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mContent:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mContent:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 175
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Failed to get image of "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mContent:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 178
    :cond_0
    invoke-direct {p0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->notifyImgDownloadResult(Z)V

    .line 179
    :goto_0
    return-void

    .line 119
    :cond_1
    :try_start_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Received response of "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mURL:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 121
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 122
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    .line 124
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->decodeBitmap(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_c
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    .line 125
    if-nez v2, :cond_4

    .line 126
    if-eqz v2, :cond_2

    .line 159
    :try_start_3
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 166
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mContent:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mContent:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 175
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Failed to get image of "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mContent:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 178
    :cond_3
    invoke-direct {p0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->notifyImgDownloadResult(Z)V

    goto :goto_0

    .line 162
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 130
    :cond_4
    :try_start_4
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mWidth:I

    .line 131
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mHeight:I

    .line 133
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mWidth:I
    :try_end_4
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_13
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_12
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_11
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    const/16 v5, 0x200

    if-ge v0, v5, :cond_7

    .line 135
    :try_start_5
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->invalidRetryCount()V
    :try_end_5
    .catch Ljava/lang/IllegalStateException; {:try_start_5 .. :try_end_5} :catch_13
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_12
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_11
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 158
    if-eqz v2, :cond_5

    .line 159
    :try_start_6
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    .line 166
    :cond_5
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mContent:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mContent:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 175
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Failed to get image of "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mContent:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 178
    :cond_6
    invoke-direct {p0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->notifyImgDownloadResult(Z)V

    goto/16 :goto_0

    .line 162
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 139
    :cond_7
    :try_start_7
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-virtual {p0, v2, v0, v5}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->cropScaledCenterBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    :try_end_7
    .catch Ljava/lang/IllegalStateException; {:try_start_7 .. :try_end_7} :catch_13
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_12
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_11
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    move-result-object v1

    .line 140
    if-nez v1, :cond_b

    .line 141
    if-eqz v2, :cond_8

    .line 159
    :try_start_8
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2

    .line 166
    :cond_8
    :goto_3
    if-eqz v1, :cond_9

    .line 167
    :try_start_9
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3

    .line 174
    :cond_9
    :goto_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mContent:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mContent:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 175
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Failed to get image of "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mContent:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 178
    :cond_a
    invoke-direct {p0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->notifyImgDownloadResult(Z)V

    goto/16 :goto_0

    .line 162
    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 170
    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_4

    .line 145
    :cond_b
    :try_start_a
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->writeFile(Landroid/graphics/Bitmap;)V
    :try_end_a
    .catch Ljava/lang/IllegalStateException; {:try_start_a .. :try_end_a} :catch_13
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_12
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_11
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 158
    if-eqz v2, :cond_c

    .line 159
    :try_start_b
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_4

    .line 166
    :cond_c
    :goto_5
    if-eqz v1, :cond_d

    .line 167
    :try_start_c
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_5

    .line 174
    :cond_d
    :goto_6
    invoke-direct {p0, v4}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->notifyImgDownloadResult(Z)V

    goto/16 :goto_0

    .line 162
    :catch_4
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5

    .line 170
    :catch_5
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_6

    .line 147
    :catch_6
    move-exception v0

    move-object v2, v1

    .line 149
    :goto_7
    :try_start_d
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    .line 158
    if-eqz v2, :cond_e

    .line 159
    :try_start_e
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_7

    .line 166
    :cond_e
    :goto_8
    if-eqz v1, :cond_f

    .line 167
    :try_start_f
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_8

    .line 174
    :cond_f
    :goto_9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mContent:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mContent:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 175
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Failed to get image of "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mContent:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 178
    :cond_10
    invoke-direct {p0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->notifyImgDownloadResult(Z)V

    goto/16 :goto_0

    .line 162
    :catch_7
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_8

    .line 170
    :catch_8
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_9

    .line 150
    :catch_9
    move-exception v0

    move-object v2, v1

    .line 152
    :goto_a
    :try_start_10
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_3

    .line 158
    if-eqz v2, :cond_11

    .line 159
    :try_start_11
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_a

    .line 166
    :cond_11
    :goto_b
    if-eqz v1, :cond_12

    .line 167
    :try_start_12
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_b

    .line 174
    :cond_12
    :goto_c
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mContent:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mContent:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 175
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Failed to get image of "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mContent:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 178
    :cond_13
    invoke-direct {p0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->notifyImgDownloadResult(Z)V

    goto/16 :goto_0

    .line 162
    :catch_a
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_b

    .line 170
    :catch_b
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_c

    .line 153
    :catch_c
    move-exception v0

    move-object v2, v1

    .line 155
    :goto_d
    :try_start_13
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_3

    .line 158
    if-eqz v2, :cond_14

    .line 159
    :try_start_14
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_d

    .line 166
    :cond_14
    :goto_e
    if-eqz v1, :cond_15

    .line 167
    :try_start_15
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_e

    .line 174
    :cond_15
    :goto_f
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mContent:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mContent:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 175
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Failed to get image of "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mContent:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 178
    :cond_16
    invoke-direct {p0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->notifyImgDownloadResult(Z)V

    goto/16 :goto_0

    .line 162
    :catch_d
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_e

    .line 170
    :catch_e
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_f

    .line 157
    :catchall_0
    move-exception v0

    move-object v2, v1

    move v3, v4

    .line 158
    :goto_10
    if-eqz v2, :cond_17

    .line 159
    :try_start_16
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_f

    .line 166
    :cond_17
    :goto_11
    if-eqz v1, :cond_18

    .line 167
    :try_start_17
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_10

    .line 174
    :cond_18
    :goto_12
    if-nez v3, :cond_19

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mContent:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    if-eqz v1, :cond_19

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mContent:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_19

    .line 175
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to get image of "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mContent:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 178
    :cond_19
    invoke-direct {p0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->notifyImgDownloadResult(Z)V

    throw v0

    .line 162
    :catch_f
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_11

    .line 170
    :catch_10
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_12

    .line 157
    :catchall_1
    move-exception v0

    move-object v2, v1

    goto :goto_10

    :catchall_2
    move-exception v0

    move v3, v4

    goto :goto_10

    :catchall_3
    move-exception v0

    goto :goto_10

    .line 153
    :catch_11
    move-exception v0

    goto/16 :goto_d

    .line 150
    :catch_12
    move-exception v0

    goto/16 :goto_a

    .line 147
    :catch_13
    move-exception v0

    goto/16 :goto_7
.end method

.method public writeFile(Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 313
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mDestFileName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mDestFileName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 331
    :cond_0
    :goto_0
    return-void

    .line 320
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mDestFileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    .line 322
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mDestFileName:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestMagazineHomeImageFile;->mContext:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v1

    .line 325
    :try_start_0
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 329
    :try_start_1
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 331
    :catch_0
    move-exception v0

    goto :goto_0

    .line 329
    :catchall_0
    move-exception v0

    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 330
    :goto_1
    throw v0

    :catch_1
    move-exception v1

    goto :goto_1
.end method
