.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _IDownloadCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;

.field private _ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

.field private mDirectBillingPurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;

.field private mTimeoutRetrier:Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 31
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 25
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->mTimeoutRetrier:Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;

    .line 26
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->mDirectBillingPurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;

    .line 32
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->_IDownloadCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;

    .line 33
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->mDirectBillingPurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;

    .line 34
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;Z)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->confirmPayment(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->mDirectBillingPurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;Z)V
    .locals 0

    .prologue
    .line 23
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;)Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->mTimeoutRetrier:Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;Z)V
    .locals 0

    .prologue
    .line 23
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;Z)V
    .locals 0

    .prologue
    .line 23
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->onFinalResult(Z)V

    return-void
.end method

.method private confirmPayment(Z)V
    .locals 5

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->mTimeoutRetrier:Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;

    if-nez v0, :cond_0

    .line 55
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->mDirectBillingPurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;->getResponseTime()J

    move-result-wide v1

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->mDirectBillingPurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;->getRetryCount()I

    move-result v3

    new-instance v4, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/a;

    invoke-direct {v4, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;)V

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;-><init>(JILcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier$RetryAction;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->mTimeoutRetrier:Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;

    .line 65
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->getConfirmParam(Z)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/IDirectBillingConfirmParam;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->mDirectBillingPurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;->getConfirmResult()Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    move-result-object v1

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->getResultReceiver()Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->requestConfirmPurchase(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/IDirectBillingConfirmParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    .line 66
    return-void
.end method

.method private getConfirmParam(Z)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/IDirectBillingConfirmParam;
    .locals 1

    .prologue
    .line 87
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/b;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;Z)V

    return-object v0
.end method

.method private getResultReceiver()Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;
    .locals 1

    .prologue
    .line 115
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/c;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;)V

    return-object v0
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->_IDownloadCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;->createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    .line 40
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->startLoading()V

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->mTimeoutRetrier:Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->mDirectBillingPurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingPurchaseInfo;->getRetryCount()I

    move-result v0

    .line 45
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingConfirmCommand;->confirmPayment(Z)V

    .line 46
    return-void

    .line 45
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public requestConfirmPurchase(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/IDirectBillingConfirmParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V
    .locals 2

    .prologue
    .line 76
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->confirmOptBillingPurchase(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/IDirectBillingConfirmParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 77
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 78
    return-void
.end method
