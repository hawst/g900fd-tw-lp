.class final Lcom/sec/android/app/samsungapps/viewholder/h;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/viewholder/h;->a:Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/h;->a:Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/h;->a:Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->_Context:Landroid/content/Context;

    const v2, 0x7f08015a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/h;->a:Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/h;->a:Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->_Context:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewholder/h;->a:Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getGUID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->launch(Landroid/content/Context;Ljava/lang/String;)Z

    .line 118
    :goto_0
    return-void

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/h;->a:Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->a(Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/h;->a:Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->_Context:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/h;->a:Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->_Context:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewholder/h;->a:Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverFactory;

    invoke-direct {v3}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverFactory;-><init>()V

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->updateContent(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieverFactory;)V

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/h;->a:Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/h;->a:Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->btnRightly:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/h;->a:Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->_Context:Landroid/content/Context;

    const v2, 0x7f08023d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 111
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/h;->a:Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->_Context:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/h;->a:Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getGUID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->cancelDownload(Ljava/lang/String;)Z

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/h;->a:Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/h;->a:Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->btnRightly:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/h;->a:Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->_Context:Landroid/content/Context;

    const v2, 0x7f0802f3

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/h;->a:Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/viewholder/DownloadContentListViewHolder;->_Adapter:Lcom/sec/android/app/samsungapps/view/IContentArrayAdapter;

    invoke-static {}, Lcom/sec/android/app/samsungapps/BadgeNotification;->getOldBadgeCount()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/view/IContentArrayAdapter;->refreshUpdateAllMenu(I)V

    goto :goto_0
.end method
