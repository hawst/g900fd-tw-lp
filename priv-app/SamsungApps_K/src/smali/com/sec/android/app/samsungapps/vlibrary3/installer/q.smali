.class final Lcom/sec/android/app/samsungapps/vlibrary3/installer/q;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/installer/BDeviceInstallManager$IBDeviceInstallManagerObserver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;)V
    .locals 0

    .prologue
    .line 180
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/q;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onInstallFailed()V
    .locals 2

    .prologue
    .line 203
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/q;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->B_INSTALL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;)V

    .line 204
    return-void
.end method

.method public final onInstallSuccess()V
    .locals 2

    .prologue
    .line 198
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/q;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->B_INSTALL_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;)V

    .line 199
    return-void
.end method

.method public final onPrepareSuccess()V
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/q;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;)Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->amISystemApp()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSlienceInstallSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/q;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->B_PREPARE_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;)V

    .line 194
    :goto_0
    return-void

    .line 192
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/q;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->B_PREPARE_SUCCESS_NOT_SYSTEMAPP:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManager;Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;)V

    goto :goto_0
.end method
