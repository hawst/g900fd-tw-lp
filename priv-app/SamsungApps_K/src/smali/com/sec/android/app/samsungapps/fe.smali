.class final Lcom/sec/android/app/samsungapps/fe;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:I

.field final synthetic b:Landroid/os/Handler;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lcom/sec/android/app/samsungapps/MobileSecurePayer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/MobileSecurePayer;ILandroid/os/Handler;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/fe;->d:Lcom/sec/android/app/samsungapps/MobileSecurePayer;

    iput p2, p0, Lcom/sec/android/app/samsungapps/fe;->a:I

    iput-object p3, p0, Lcom/sec/android/app/samsungapps/fe;->b:Landroid/os/Handler;

    iput-object p4, p0, Lcom/sec/android/app/samsungapps/fe;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 80
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/fe;->d:Lcom/sec/android/app/samsungapps/MobileSecurePayer;

    iget-object v1, v0, Lcom/sec/android/app/samsungapps/MobileSecurePayer;->b:Ljava/lang/Integer;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 82
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/fe;->d:Lcom/sec/android/app/samsungapps/MobileSecurePayer;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/MobileSecurePayer;->c:Lcom/alipay/android/app/IAlixPay;

    if-nez v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/fe;->d:Lcom/sec/android/app/samsungapps/MobileSecurePayer;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/MobileSecurePayer;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V

    .line 85
    :cond_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 87
    :try_start_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/fe;->d:Lcom/sec/android/app/samsungapps/MobileSecurePayer;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/MobileSecurePayer;->c:Lcom/alipay/android/app/IAlixPay;

    if-nez v0, :cond_1

    .line 88
    const-string v0, "mAlixPay is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    .line 90
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 91
    iget v1, p0, Lcom/sec/android/app/samsungapps/fe;->a:I

    iput v1, v0, Landroid/os/Message;->what:I

    .line 92
    const-string v1, "mAlixPay is null"

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 93
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/fe;->b:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 125
    :goto_0
    return-void

    .line 85
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 115
    :catch_0
    move-exception v0

    .line 117
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/app/samsungapps/MobileSecurePayer;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "::Exception::"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 120
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 121
    iget v2, p0, Lcom/sec/android/app/samsungapps/fe;->a:I

    iput v2, v1, Landroid/os/Message;->what:I

    .line 122
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/fe;->b:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 98
    :cond_1
    :try_start_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/fe;->d:Lcom/sec/android/app/samsungapps/MobileSecurePayer;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/MobileSecurePayer;->c:Lcom/alipay/android/app/IAlixPay;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/fe;->d:Lcom/sec/android/app/samsungapps/MobileSecurePayer;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/MobileSecurePayer;->a(Lcom/sec/android/app/samsungapps/MobileSecurePayer;)Lcom/alipay/android/app/IRemoteServiceCallback;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/alipay/android/app/IAlixPay;->registerCallback(Lcom/alipay/android/app/IRemoteServiceCallback;)V

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/fe;->d:Lcom/sec/android/app/samsungapps/MobileSecurePayer;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/MobileSecurePayer;->c:Lcom/alipay/android/app/IAlixPay;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/fe;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/alipay/android/app/IAlixPay;->Pay(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 105
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/fe;->d:Lcom/sec/android/app/samsungapps/MobileSecurePayer;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/sec/android/app/samsungapps/MobileSecurePayer;->d:Z

    .line 106
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/fe;->d:Lcom/sec/android/app/samsungapps/MobileSecurePayer;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/MobileSecurePayer;->c:Lcom/alipay/android/app/IAlixPay;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/fe;->d:Lcom/sec/android/app/samsungapps/MobileSecurePayer;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/MobileSecurePayer;->a(Lcom/sec/android/app/samsungapps/MobileSecurePayer;)Lcom/alipay/android/app/IRemoteServiceCallback;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/alipay/android/app/IAlixPay;->unregisterCallback(Lcom/alipay/android/app/IRemoteServiceCallback;)V

    .line 107
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/fe;->d:Lcom/sec/android/app/samsungapps/MobileSecurePayer;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/MobileSecurePayer;->e:Landroid/app/Activity;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/fe;->d:Lcom/sec/android/app/samsungapps/MobileSecurePayer;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/MobileSecurePayer;->b(Lcom/sec/android/app/samsungapps/MobileSecurePayer;)Landroid/content/ServiceConnection;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Activity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 110
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 111
    iget v2, p0, Lcom/sec/android/app/samsungapps/fe;->a:I

    iput v2, v1, Landroid/os/Message;->what:I

    .line 112
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/fe;->b:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0
.end method
