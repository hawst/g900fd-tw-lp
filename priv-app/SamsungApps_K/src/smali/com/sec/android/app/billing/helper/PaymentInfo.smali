.class public Lcom/sec/android/app/billing/helper/PaymentInfo;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public confirmPasswordYN:Ljava/lang/String;

.field public exceptionPaymentMethods:Ljava/lang/String;

.field public freeTrialPeriod:Ljava/lang/String;

.field public offerType:Ljava/lang/String;

.field public paymentMethods:[Ljava/lang/String;

.field public paymentType:Ljava/lang/String;

.field public subscriptionStartDate:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    new-instance v0, Lcom/sec/android/app/billing/helper/g;

    invoke-direct {v0}, Lcom/sec/android/app/billing/helper/g;-><init>()V

    sput-object v0, Lcom/sec/android/app/billing/helper/PaymentInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 6
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static readFromParcel(Landroid/os/Parcel;)Lcom/sec/android/app/billing/helper/PaymentInfo;
    .locals 2

    .prologue
    .line 42
    new-instance v0, Lcom/sec/android/app/billing/helper/PaymentInfo;

    invoke-direct {v0}, Lcom/sec/android/app/billing/helper/PaymentInfo;-><init>()V

    .line 44
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/helper/PaymentInfo;->paymentType:Ljava/lang/String;

    .line 45
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 46
    if-eqz v1, :cond_0

    .line 47
    new-array v1, v1, [Ljava/lang/String;

    iput-object v1, v0, Lcom/sec/android/app/billing/helper/PaymentInfo;->paymentMethods:[Ljava/lang/String;

    .line 48
    iget-object v1, v0, Lcom/sec/android/app/billing/helper/PaymentInfo;->paymentMethods:[Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Parcel;->readStringArray([Ljava/lang/String;)V

    .line 50
    :cond_0
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/helper/PaymentInfo;->offerType:Ljava/lang/String;

    .line 51
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/helper/PaymentInfo;->freeTrialPeriod:Ljava/lang/String;

    .line 52
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/helper/PaymentInfo;->subscriptionStartDate:Ljava/lang/String;

    .line 53
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/helper/PaymentInfo;->exceptionPaymentMethods:Ljava/lang/String;

    .line 54
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/billing/helper/PaymentInfo;->confirmPasswordYN:Ljava/lang/String;

    .line 56
    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/billing/helper/PaymentInfo;->paymentType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 27
    iget-object v0, p0, Lcom/sec/android/app/billing/helper/PaymentInfo;->paymentMethods:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 28
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 34
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/billing/helper/PaymentInfo;->offerType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 35
    iget-object v0, p0, Lcom/sec/android/app/billing/helper/PaymentInfo;->freeTrialPeriod:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lcom/sec/android/app/billing/helper/PaymentInfo;->subscriptionStartDate:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lcom/sec/android/app/billing/helper/PaymentInfo;->exceptionPaymentMethods:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/billing/helper/PaymentInfo;->confirmPasswordYN:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 39
    return-void

    .line 30
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/billing/helper/PaymentInfo;->paymentMethods:[Ljava/lang/String;

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 31
    iget-object v0, p0, Lcom/sec/android/app/billing/helper/PaymentInfo;->paymentMethods:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    goto :goto_0
.end method
