.class public Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;
.super Lcom/sec/android/app/samsungapps/widget/CommonWidget;
.source "ProGuard"


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/view/MenuView;

.field private b:Lcom/sec/android/app/samsungapps/MenuItemList;

.field private d:Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget$MenuCreator;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;)V

    .line 17
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;->a:Lcom/sec/android/app/samsungapps/view/MenuView;

    .line 18
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;->b:Lcom/sec/android/app/samsungapps/MenuItemList;

    .line 19
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;->d:Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget$MenuCreator;

    .line 23
    const v0, 0x7f04004e

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;->initView(Landroid/content/Context;I)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 27
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 17
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;->a:Lcom/sec/android/app/samsungapps/view/MenuView;

    .line 18
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;->b:Lcom/sec/android/app/samsungapps/MenuItemList;

    .line 19
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;->d:Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget$MenuCreator;

    .line 28
    const v0, 0x7f04004e

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;->initView(Landroid/content/Context;I)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 32
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 17
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;->a:Lcom/sec/android/app/samsungapps/view/MenuView;

    .line 18
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;->b:Lcom/sec/android/app/samsungapps/MenuItemList;

    .line 19
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;->d:Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget$MenuCreator;

    .line 33
    const v0, 0x7f04004e

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;->initView(Landroid/content/Context;I)V

    .line 34
    return-void
.end method


# virtual methods
.method protected createMenuItemList()Lcom/sec/android/app/samsungapps/MenuItemList;
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;->b:Lcom/sec/android/app/samsungapps/MenuItemList;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;->b:Lcom/sec/android/app/samsungapps/MenuItemList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/MenuItemList;->clear()V

    .line 60
    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/MenuItemList;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/MenuItemList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;->b:Lcom/sec/android/app/samsungapps/MenuItemList;

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;->b:Lcom/sec/android/app/samsungapps/MenuItemList;

    if-eqz v0, :cond_1

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;->b:Lcom/sec/android/app/samsungapps/MenuItemList;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;->d:Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget$MenuCreator;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget$MenuCreator;->createQuestions()Lcom/sec/android/app/samsungapps/MenuItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/MenuItemList;->add(Lcom/sec/android/app/samsungapps/MenuItem;)Z

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;->b:Lcom/sec/android/app/samsungapps/MenuItemList;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;->d:Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget$MenuCreator;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget$MenuCreator;->createFaq()Lcom/sec/android/app/samsungapps/MenuItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/MenuItemList;->add(Lcom/sec/android/app/samsungapps/MenuItem;)Z

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;->b:Lcom/sec/android/app/samsungapps/MenuItemList;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;->d:Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget$MenuCreator;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget$MenuCreator;->createContactUs()Lcom/sec/android/app/samsungapps/MenuItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/MenuItemList;->add(Lcom/sec/android/app/samsungapps/MenuItem;)Z

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;->b:Lcom/sec/android/app/samsungapps/MenuItemList;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;->d:Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget$MenuCreator;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget$MenuCreator;->createContactNumbers()Lcom/sec/android/app/samsungapps/MenuItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/MenuItemList;->add(Lcom/sec/android/app/samsungapps/MenuItem;)Z

    .line 68
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;->b:Lcom/sec/android/app/samsungapps/MenuItemList;

    return-object v0
.end method

.method public loadWidget()V
    .locals 2

    .prologue
    .line 48
    const v0, 0x7f0c0132

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/view/MenuView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;->a:Lcom/sec/android/app/samsungapps/view/MenuView;

    .line 49
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget$MenuCreator;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget$MenuCreator;-><init>(Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;->d:Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget$MenuCreator;

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;->a:Lcom/sec/android/app/samsungapps/view/MenuView;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;->a:Lcom/sec/android/app/samsungapps/view/MenuView;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;->createMenuItemList()Lcom/sec/android/app/samsungapps/MenuItemList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/MenuView;->setMenuItemList(Lcom/sec/android/app/samsungapps/MenuItemList;)V

    .line 54
    :cond_0
    return-void
.end method

.method public refreshWidget()V
    .locals 0

    .prologue
    .line 140
    return-void
.end method

.method public release()V
    .locals 0

    .prologue
    .line 42
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->release()V

    .line 43
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;->removeAllViews()V

    .line 44
    return-void
.end method

.method public setWidgetData(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 146
    return-void
.end method

.method public updateWidget()V
    .locals 0

    .prologue
    .line 134
    return-void
.end method
