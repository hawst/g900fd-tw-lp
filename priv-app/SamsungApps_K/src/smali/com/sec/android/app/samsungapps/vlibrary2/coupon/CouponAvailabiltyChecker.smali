.class public Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponAvailabiltyChecker;
.super Ljava/lang/Object;
.source "ProGuard"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    return-void
.end method

.method private checkAvailabilityByRate(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;D)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const-wide/16 v6, 0x0

    .line 66
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->getMinPrice()D

    move-result-wide v1

    .line 67
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->getMaxPrice()D

    move-result-wide v3

    .line 69
    cmpl-double v5, v1, v6

    if-nez v5, :cond_1

    cmpl-double v5, v3, v6

    if-nez v5, :cond_1

    .line 84
    :cond_0
    :goto_0
    return v0

    .line 74
    :cond_1
    cmpl-double v1, p2, v1

    if-ltz v1, :cond_2

    cmpg-double v1, p2, v3

    if-lez v1, :cond_0

    .line 84
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private checkByPrice(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;DZ)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 90
    if-eqz p5, :cond_1

    .line 104
    :cond_0
    :goto_0
    return v0

    .line 93
    :cond_1
    invoke-interface {p2, p3, p4}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->calcPaymentPrice(D)D

    move-result-wide v2

    .line 94
    const-wide/16 v4, 0x0

    cmpg-double v4, v2, v4

    if-gtz v4, :cond_2

    move v0, v1

    .line 95
    goto :goto_0

    .line 97
    :cond_2
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponAvailabiltyChecker;->isValidPaymentMethodForCoupon(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 100
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->getPaymentMinPrice()D

    move-result-wide v4

    .line 101
    cmpg-double v2, v2, v4

    if-ltz v2, :cond_0

    move v0, v1

    .line 104
    goto :goto_0
.end method

.method private checkByRate(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;DZ)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 34
    if-eqz p5, :cond_1

    .line 53
    :cond_0
    :goto_0
    return v0

    .line 39
    :cond_1
    invoke-direct {p0, p2, p3, p4}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponAvailabiltyChecker;->checkAvailabilityByRate(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;D)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 42
    invoke-interface {p2, p3, p4}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->calcPaymentPrice(D)D

    move-result-wide v2

    .line 43
    const-wide/16 v4, 0x0

    cmpg-double v4, v2, v4

    if-gtz v4, :cond_2

    move v0, v1

    .line 44
    goto :goto_0

    .line 46
    :cond_2
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponAvailabiltyChecker;->isValidPaymentMethodForCoupon(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 49
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->getPaymentMinPrice()D

    move-result-wide v4

    .line 50
    cmpg-double v2, v2, v4

    if-ltz v2, :cond_0

    move v0, v1

    .line 53
    goto :goto_0
.end method

.method private isValidPaymentMethodForCoupon(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;)Z
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_GLOBAL_CREDIT_CARD:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_KOREA_INICIS:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_UKRAINE_CREDIT_CARD:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_KOREA_DANAL:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_KOREA_UPOINT:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public checkAvailability(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;DZ)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 15
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 29
    :cond_0
    :goto_0
    return v0

    .line 18
    :cond_1
    if-nez p5, :cond_0

    .line 21
    const/4 v1, 0x2

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->getDiscountType()I

    move-result v2

    if-ne v1, v2, :cond_2

    .line 23
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponAvailabiltyChecker;->checkByPrice(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;DZ)Z

    move-result v0

    goto :goto_0

    .line 25
    :cond_2
    const/4 v1, 0x1

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->getDiscountType()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 27
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponAvailabiltyChecker;->checkByRate(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;DZ)Z

    move-result v0

    goto :goto_0
.end method
