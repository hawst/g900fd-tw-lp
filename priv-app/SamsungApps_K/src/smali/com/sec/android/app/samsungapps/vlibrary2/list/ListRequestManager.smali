.class public Lcom/sec/android/app/samsungapps/vlibrary2/list/ListRequestManager;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field _ContentList:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2;

.field private _List:Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;

.field _ReceiveList:Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;

.field mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListRequestManager;->_ReceiveList:Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;

    .line 23
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListRequestManager;->mContext:Landroid/content/Context;

    .line 24
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListRequestManager;->_List:Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;

    .line 25
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/list/ListRequestManager;)Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListRequestManager;->_List:Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/list/ListRequestManager;Z)V
    .locals 0

    .prologue
    .line 15
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListRequestManager;->onFinalResult(Z)V

    return-void
.end method

.method private request()V
    .locals 8

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListRequestManager;->_List:Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;

    if-nez v0, :cond_0

    .line 62
    :goto_0
    return-void

    .line 42
    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;

    const/16 v1, 0xf

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListRequestManager;->_ReceiveList:Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;

    .line 44
    new-instance v6, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListGenerator;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListRequestManager;->_ReceiveList:Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;

    invoke-direct {v6, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;)V

    .line 45
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    const-string v1, "bestSelling"

    const-string v2, "0000002474"

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListRequestManager;->_List:Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;->getNextStartNumber()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListRequestManager;->_List:Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;->getNextEndNumber()I

    move-result v4

    const/4 v5, 0x0

    new-instance v7, Lcom/sec/android/app/samsungapps/vlibrary2/list/b;

    invoke-direct {v7, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/list/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/list/ListRequestManager;)V

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->contentCategoryProductList2Notc(Ljava/lang/String;Ljava/lang/String;IIILcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 59
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListRequestManager;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 61
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    goto :goto_0
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListRequestManager;->request()V

    .line 31
    return-void
.end method
