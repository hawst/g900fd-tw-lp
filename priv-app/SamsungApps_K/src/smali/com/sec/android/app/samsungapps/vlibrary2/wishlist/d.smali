.class final Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/d;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/d;->a:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceiveResult(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/d;->a:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;)Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/d;->a:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;)Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->endLoading()V

    .line 50
    :cond_0
    if-eqz p2, :cond_1

    .line 52
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->WishListChanged:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->broadcast(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;)V

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/d;->a:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;->_IRemoveWishItemData:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem$IRemoveWishItemData;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;->access$100(Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;)Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem$IRemoveWishItemData;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem$IRemoveWishItemData;->setDeletedWishItem()V

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/d;->a:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;->onFinalResult(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;Z)V

    .line 60
    :goto_0
    return-void

    .line 58
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/d;->a:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;->onFinalResult(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;->access$300(Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/RemoveWishItem;Z)V

    goto :goto_0
.end method
