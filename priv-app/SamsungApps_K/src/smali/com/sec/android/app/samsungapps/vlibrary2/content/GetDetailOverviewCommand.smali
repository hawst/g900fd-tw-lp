.class public Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field protected _ContentDetailOverview:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;

.field protected _IGetDetailOverviewCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand$IGetDetailOverviewCommandData;

.field _receiver:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

.field handler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand$IGetDetailOverviewCommandData;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 97
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/content/f;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/f;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;->handler:Landroid/os/Handler;

    .line 102
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/content/g;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/g;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;->_receiver:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    .line 27
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;->_IGetDetailOverviewCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand$IGetDetailOverviewCommandData;

    .line 28
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;Z)V
    .locals 0

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;Z)V
    .locals 0

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;Z)V
    .locals 0

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;->onFinalResult(Z)V

    return-void
.end method

.method private onRequestUncModeOverview()V
    .locals 5

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;->_IGetDetailOverviewCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand$IGetDetailOverviewCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand$IGetDetailOverviewCommandData;->getProductDetailResultMap()Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    move-result-object v1

    .line 67
    if-nez v1, :cond_0

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/content/d;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 95
    :goto_0
    return-void

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;->_ContentDetailOverview:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->openMap()V

    .line 80
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 81
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 83
    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 84
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;->_ContentDetailOverview:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;

    invoke-virtual {v4, v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 86
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;->_ContentDetailOverview:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;->closeMap()V

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/content/e;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/e;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 5

    .prologue
    .line 32
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;->_IGetDetailOverviewCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand$IGetDetailOverviewCommandData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand$IGetDetailOverviewCommandData;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;->_ContentDetailOverview:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;

    .line 35
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;->_IGetDetailOverviewCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand$IGetDetailOverviewCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand$IGetDetailOverviewCommandData;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    .line 36
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UncModeChecker;->isUncMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;->onRequestUncModeOverview()V

    .line 62
    :goto_0
    return-void

    .line 43
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getLoadType()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getLoadType()Ljava/lang/String;

    move-result-object v1

    const-string v2, "2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 47
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getOrderID()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;->_ContentDetailOverview:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;->_receiver:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->productDetailOverview(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 54
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 55
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 57
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 60
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;->onFinalResult(Z)V

    goto :goto_0

    .line 51
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;->_ContentDetailOverview:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailOverview;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailOverviewCommand;->_receiver:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->guidProductDetailOverview(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_1
.end method
