.class public Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ProGuard"


# instance fields
.field private a:I

.field private b:Landroid/view/LayoutInflater;

.field private c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 28
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;->a:I

    .line 29
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;->b:Landroid/view/LayoutInflater;

    .line 30
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;->c:Landroid/content/Context;

    .line 36
    iput p2, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;->a:I

    .line 37
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;->b:Landroid/view/LayoutInflater;

    .line 38
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;->c:Landroid/content/Context;

    .line 39
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 44
    if-nez p2, :cond_0

    .line 48
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;->b:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;->a:I

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 52
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 57
    if-eqz v0, :cond_2

    .line 59
    if-eqz p2, :cond_1

    if-nez v0, :cond_3

    :cond_1
    const-string v1, "_displayData::Not Already Object"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 60
    :goto_0
    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->setAdapter(Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;)V

    .line 61
    invoke-virtual {v0, p2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->setView(Landroid/view/View;)V

    .line 63
    :cond_2
    return-object p2

    .line 59
    :cond_3
    const v1, 0x7f0c0088

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;->c:Landroid/content/Context;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;->c:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/TencentUtil;->isAvailableTencent(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_5

    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;->c:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->isKitkatMenuAvailable(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_8

    :cond_5
    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_1
    const v1, 0x7f0c004d

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->getTitle()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_6
    const v1, 0x7f0c008a

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/OptionMenuAdapter;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-lez v2, :cond_a

    if-ne v2, p1, :cond_a

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    :cond_7
    :goto_2
    const v1, 0x7f0c0089

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->hasCheck()Z

    move-result v2

    if-ne v2, v6, :cond_c

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->isChecked()Z

    move-result v2

    if-ne v2, v6, :cond_b

    invoke-virtual {v1, v6}, Landroid/widget/RadioButton;->setChecked(Z)V

    :goto_3
    invoke-virtual {v1, v4}, Landroid/widget/RadioButton;->setFocusable(Z)V

    invoke-virtual {v1, v4}, Landroid/widget/RadioButton;->setVisibility(I)V

    goto :goto_0

    :cond_8
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->getResourceImg()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_9

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->getResourceImg()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    :cond_9
    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    :cond_a
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_b
    invoke-virtual {v1, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_3

    :cond_c
    invoke-virtual {v1, v5}, Landroid/widget/RadioButton;->setVisibility(I)V

    goto/16 :goto_0
.end method
