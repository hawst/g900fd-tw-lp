.class public Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;
.super Lcom/sec/android/app/samsungapps/ActionBarActivity;
.source "ProGuard"

# interfaces
.implements Landroid/hardware/SensorEventListener;
.implements Lcom/sec/android/app/samsungapps/uieventmanager/UIEventObserver;
.implements Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment$OnActionBarConfigurationListener;


# static fields
.field public static final EXTRA_BUTTONSTATE:Ljava/lang/String; = "_buttonState"

.field public static final EXTRA_CATEGORY:Ljava/lang/String; = "_category"

.field public static final EXTRA_DEEPLINK_CATEGORYID:Ljava/lang/String; = "_deeplink_categoryId"

.field public static final EXTRA_DESCRIPTION:Ljava/lang/String; = "_description"

.field public static final EXTRA_LISTTYPE:Ljava/lang/String; = "_listType"

.field public static final EXTRA_PRODUCTSETID:Ljava/lang/String; = "_productSetListId"

.field public static final EXTRA_TITLETEXT:Ljava/lang/String; = "_titleText"


# instance fields
.field a:[I

.field b:Z

.field private c:Landroid/content/Intent;

.field private d:Ljava/lang/String;

.field private e:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

.field private f:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

.field private k:Z

.field private l:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

.field private m:Landroid/support/v4/view/ViewPager;

.field private n:Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;

.field private o:Landroid/hardware/SensorManager;

.field private p:Landroid/hardware/Sensor;

.field private q:I

.field private r:Lcom/sec/android/app/samsungapps/tobelog/LogData;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 48
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;-><init>()V

    .line 57
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->a:[I

    .line 63
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->d:Ljava/lang/String;

    .line 65
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->f:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    .line 66
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->g:Ljava/lang/String;

    .line 67
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->h:Ljava/lang/String;

    .line 68
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->i:Ljava/lang/String;

    .line 70
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->k:Z

    .line 79
    const/16 v0, 0xff

    iput v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->q:I

    .line 376
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->b:Z

    .line 453
    return-void

    .line 57
    nop

    :array_0
    .array-data 4
        0xa0005
        0xa0007
        0xa0008
    .end array-data
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->a(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;)V

    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)V
    .locals 1

    .prologue
    .line 332
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getContentQuery(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v0

    .line 333
    if-eqz v0, :cond_0

    .line 334
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->initAllListFirstItem()V

    .line 336
    :cond_0
    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;)V
    .locals 9

    .prologue
    .line 188
    const v0, 0x7f0c0045

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->l:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    .line 189
    const v0, 0x7f0c0046

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->m:Landroid/support/v4/view/ViewPager;

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->l:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->m:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_1

    .line 209
    :cond_0
    :goto_0
    return-void

    .line 194
    :cond_1
    new-instance v0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->c:Landroid/content/Intent;

    const-string v2, "_titleText"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getExtra(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    move-object v1, p0

    move-object v2, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move-object v8, p4

    invoke-direct/range {v0 .. v8}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;-><init>(Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->n:Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->n:Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;

    if-eqz v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->n:Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->setOnActionBarConfigurationListener(Lcom/sec/android/app/samsungapps/viewpager/CategorizedListFragment$OnActionBarConfigurationListener;)V

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->m:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->n:Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->m:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->n:Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity$MyPagerAdapter;->getCount()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 203
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->m:Landroid/support/v4/view/ViewPager;

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->j:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    sget-object v3, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->ALL_NEW:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    if-eq v2, v3, :cond_2

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isFreeStore()Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopNew:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    if-ne v2, v3, :cond_3

    const/4 v0, 0x1

    :cond_3
    :goto_1
    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 204
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->l:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->m:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 206
    sget-object v0, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->NONE:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    if-ne p4, v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->l:Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/viewpager/SamsungAppsTabStrip;->setVisibility(I)V

    goto :goto_0

    .line 203
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->j:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    sget-object v3, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->ALL_PAID_FREE_NEW:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->k:Z

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopNew:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    if-ne v2, v3, :cond_5

    const/4 v0, 0x3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopFree:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    if-ne v2, v3, :cond_6

    const/4 v0, 0x2

    goto :goto_1

    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopPaid:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    if-ne v2, v3, :cond_3

    const/4 v0, 0x1

    goto :goto_1

    :cond_7
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopNew:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    if-ne v2, v3, :cond_8

    const/4 v0, 0x3

    goto :goto_1

    :cond_8
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopFree:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    if-ne v2, v3, :cond_9

    const/4 v0, 0x1

    goto :goto_1

    :cond_9
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopPaid:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    if-ne v2, v3, :cond_3

    const/4 v0, 0x2

    goto :goto_1
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;)Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->j:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;)V
    .locals 0

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->initialized()V

    return-void
.end method

.method static synthetic f(Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;)Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->k:Z

    return v0
.end method

.method static synthetic g(Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 382
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->b:Z

    .line 384
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z
    .locals 6

    .prologue
    .line 404
    sget-object v0, Lcom/sec/android/app/samsungapps/viewpager/b;->c:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 421
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z

    move-result v0

    :goto_0
    return v0

    .line 406
    :pswitch_0
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getAccountEvent()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;->getAccountEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;->LogedOut:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    if-ne v0, v1, :cond_1

    .line 407
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z

    .line 419
    :cond_0
    :goto_1
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;

    invoke-static {p1}, Lcom/sec/android/app/samsungapps/CommonActivity;->handleAccountEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;)Z

    move-result v0

    goto :goto_0

    .line 408
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getAccountEvent()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;->getAccountEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;->LogedIn:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    if-ne v0, v1, :cond_0

    .line 409
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 410
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isFreeStore()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 411
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    const v1, 0xa0008

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->removeAction(I)Ljava/lang/Boolean;

    goto :goto_1

    .line 413
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 414
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->d:Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->a:[I

    const v5, 0xb0001

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    goto :goto_1

    .line 404
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public handleUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V
    .locals 4

    .prologue
    .line 389
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getEventType()Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;->ContentDisplayEvent:Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    if-ne v0, v1, :cond_0

    .line 390
    sget-object v0, Lcom/sec/android/app/samsungapps/viewpager/b;->b:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getContentDisplayEvent()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->getContentDisplayEventType()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 400
    :cond_0
    :goto_0
    return-void

    .line 393
    :pswitch_0
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getContentDisplayEvent()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    const/16 v1, 0x8

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;I)V

    goto :goto_0

    .line 396
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->ProductSetList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getContentDisplayEvent()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getContentDisplayEvent()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->getID()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x4

    invoke-static {p0, v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 390
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public initSensorManager()V
    .locals 2

    .prologue
    .line 131
    const-string v0, "sensor"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->o:Landroid/hardware/SensorManager;

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->o:Landroid/hardware/SensorManager;

    if-nez v0, :cond_0

    .line 137
    :goto_0
    return-void

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->o:Landroid/hardware/SensorManager;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->p:Landroid/hardware/Sensor;

    goto :goto_0
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    .prologue
    .line 542
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 85
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 87
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->populateUI()V

    .line 88
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->initSensorManager()V

    .line 94
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    invoke-static {}, Lcom/sec/android/app/samsungapps/initializer/Global;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 96
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->initializer(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializerFactory;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/viewpager/a;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/viewpager/a;-><init>(Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;)V

    invoke-interface {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializerFactory;->createInitializer(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializer$IIntializerObserver;)Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializer;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializer;->execute()V

    .line 115
    :goto_0
    return-void

    .line 114
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->initialized()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->o:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->o:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 124
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onDestroy()V

    .line 125
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 340
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onPause()V

    .line 341
    invoke-static {}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->getInstance()Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->removeObserver(Lcom/sec/android/app/samsungapps/uieventmanager/UIEventObserver;)Z

    .line 343
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->o:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_0

    .line 344
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->o:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 347
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->r:Lcom/sec/android/app/samsungapps/tobelog/LogData;

    if-eqz v0, :cond_1

    .line 348
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->r:Lcom/sec/android/app/samsungapps/tobelog/LogData;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendNormalPageViewLog(Lcom/sec/android/app/samsungapps/tobelog/LogData;)V

    .line 349
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->r:Lcom/sec/android/app/samsungapps/tobelog/LogData;

    .line 352
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->b:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CONTENTS_SET_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-ne v0, v1, :cond_2

    .line 353
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CONTENTS_SET_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->MOVE_OUT_FROM_MAIN_WITHOUT_ACTION:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendSummaryNormalLog(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;)V

    .line 355
    :cond_2
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 359
    invoke-static {}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->getInstance()Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->addObserver(Lcom/sec/android/app/samsungapps/uieventmanager/UIEventObserver;)V

    .line 361
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->f:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    if-eqz v0, :cond_0

    .line 362
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCategoryContainer()Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

    move-result-object v0

    .line 363
    if-eqz v0, :cond_0

    .line 364
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->f:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->findCategoryByID(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->f:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    .line 365
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->f:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->setSel(Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;Z)Z

    .line 368
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onResume()V

    .line 370
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->o:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->p:Landroid/hardware/Sensor;

    if-eqz v0, :cond_1

    .line 371
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->o:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->p:Landroid/hardware/Sensor;

    const/4 v2, 0x3

    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 373
    :cond_1
    iput-boolean v3, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->b:Z

    .line 374
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 2

    .prologue
    .line 546
    sget-object v0, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 547
    sget-object v0, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 549
    iget v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->q:I

    if-eq v1, v0, :cond_0

    .line 551
    iput v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->q:I

    .line 552
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->invalidate()V

    .line 555
    :cond_0
    return-void
.end method

.method public populateUI()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 143
    const v0, 0x7f040008

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->setMainView(I)V

    .line 144
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isPaidTabFirst()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->k:Z

    .line 146
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->c:Landroid/content/Intent;

    .line 147
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->c:Landroid/content/Intent;

    const-string v1, "_deeplink_categoryId"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->h:Ljava/lang/String;

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->c:Landroid/content/Intent;

    const-string v1, "_productSetListId"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->i:Ljava/lang/String;

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->c:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "_listType"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->c:Landroid/content/Intent;

    const-string v1, "_category"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getExtra(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->f:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->c:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "_buttonState"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->j:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    .line 153
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "CHM"

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getCSC()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154
    sget-object v0, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->NONE:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->j:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->c:Landroid/content/Intent;

    const-string v1, "_description"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->g:Ljava/lang/String;

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/android/app/samsungapps/viewpager/b;->a:[I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_1
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopFree:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->a(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)V

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopPaid:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->a(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)V

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopAll:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->a(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)V

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopNew:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->a(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)V

    .line 160
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->showFeaturedTitle()V

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->f:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    if-eqz v0, :cond_3

    .line 163
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCategoryContainer()Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

    move-result-object v0

    .line 164
    if-eqz v0, :cond_3

    .line 165
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->f:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->findCategoryByID(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->f:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    .line 166
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->f:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->setSel(Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;Z)Z

    .line 170
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->i:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->h:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->j:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->a(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;)V

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->e:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->ProductSetList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->i:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CONTENTS_SET_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->setCurrentPage(Lcom/sec/android/app/samsungapps/tobelog/LogPage;)V

    .line 178
    :goto_1
    return-void

    .line 159
    :pswitch_0
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 175
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CATEGORY_PRODUCT_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->setCurrentPage(Lcom/sec/android/app/samsungapps/tobelog/LogPage;)V

    goto :goto_1

    .line 159
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public setOnActionBarConfiguration(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 426
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 427
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->d:Ljava/lang/String;

    .line 428
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->d:Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->a:[I

    const v5, 0xb0001

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    .line 429
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 430
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/status/SamsungAppsStatus;->isAutoLoginInProgress()Z

    move-result v0

    if-ne v0, v3, :cond_0

    .line 431
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    const v1, 0xa0008

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->removeAction(I)Ljava/lang/Boolean;

    .line 434
    :cond_0
    return-void
.end method

.method public declared-synchronized setOnToBeLog(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V
    .locals 5

    .prologue
    .line 437
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CATEGORY_PRODUCT_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->r:Lcom/sec/android/app/samsungapps/tobelog/LogData;

    if-nez v0, :cond_0

    .line 439
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 441
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->PAGE_PV_PATH_TIME:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getPreviousPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getPreviousPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    move-object v1, v0

    :goto_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->categoryID:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v2, v3, v1, v0, v4}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->createLogDataForPageView(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;Lcom/sec/android/app/samsungapps/tobelog/LogPage;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/tobelog/LogData;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->r:Lcom/sec/android/app/samsungapps/tobelog/LogData;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 451
    :cond_0
    monitor-exit p0

    return-void

    .line 441
    :cond_1
    :try_start_1
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->EMPTY:Lcom/sec/android/app/samsungapps/tobelog/LogPage;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v1, v0

    goto :goto_0

    .line 437
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public showFeaturedTitle()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 250
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->c:Landroid/content/Intent;

    const-string v1, "_titleText"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getExtra(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 252
    if-nez v0, :cond_6

    .line 254
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->d:Ljava/lang/String;

    .line 274
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->checkActionbarTitleEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->d:Ljava/lang/String;

    .line 276
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 278
    :cond_1
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/AppsTitle;->getString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->d:Ljava/lang/String;

    .line 282
    :cond_2
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 284
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->d:Ljava/lang/String;

    const v1, 0x7f0802fa

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 287
    :cond_3
    const v0, 0x7f08009d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->d:Ljava/lang/String;

    .line 291
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->d:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 292
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->d:Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->a:[I

    const v5, 0xb0001

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    .line 293
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 294
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/status/SamsungAppsStatus;->isAutoLoginInProgress()Z

    move-result v0

    if-ne v0, v3, :cond_5

    .line 295
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    const v1, 0xa0008

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->removeAction(I)Ljava/lang/Boolean;

    .line 298
    :cond_5
    return-void

    .line 256
    :cond_6
    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 258
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 259
    if-lez v0, :cond_0

    .line 261
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 264
    :cond_7
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 266
    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->d:Ljava/lang/String;

    goto/16 :goto_0
.end method
