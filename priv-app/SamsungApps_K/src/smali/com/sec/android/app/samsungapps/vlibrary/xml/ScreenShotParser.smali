.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/ScreenShotParser;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;
.source "ProGuard"


# instance fields
.field mContent:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ScreenShotParser;->mContent:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 11
    return-void
.end method


# virtual methods
.method protected onCreateObject(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 3

    .prologue
    .line 21
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;-><init>()V

    .line 22
    const-class v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->mappingClass(Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 23
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;->calculate()V

    .line 24
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/ScreenShotParser;->mContent:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->setScreenShot(Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;)V

    .line 25
    return-void
.end method

.method protected onPostParseError()V
    .locals 0

    .prologue
    .line 16
    return-void
.end method

.method protected onPostParseResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 31
    return-void
.end method
