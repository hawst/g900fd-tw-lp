.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _CardValidatorCmd:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

.field private _IDownloadCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;

.field private _IGCreditCardParam:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardParam;

.field private _IGCreditCardReqComm:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardReqComm;

.field private _ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

.field private _IResultMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardReqComm;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;->_IDownloadCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;

    .line 25
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;->_IGCreditCardReqComm:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardReqComm;

    .line 26
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;->_IGCreditCardParam:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardParam;

    .line 27
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;->_CardValidatorCmd:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 28
    iput-object p5, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;->_IResultMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    .line 29
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;)Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;->_Context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardParam;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;->_IGCreditCardParam:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardParam;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;)Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;->_IResultMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardReqComm;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;->_IGCreditCardReqComm:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardReqComm;

    return-object v0
.end method

.method private requestPayment()V
    .locals 3

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;->_IDownloadCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IDownloadCommandBuilder;->createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    .line 34
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;->_CardValidatorCmd:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/c;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 56
    return-void
.end method


# virtual methods
.method public impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;->requestPayment()V

    .line 66
    return-void
.end method

.method protected onPaymentResult(Z)V
    .locals 0

    .prologue
    .line 60
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardPayment;->onFinalResult(Z)V

    .line 61
    return-void
.end method
