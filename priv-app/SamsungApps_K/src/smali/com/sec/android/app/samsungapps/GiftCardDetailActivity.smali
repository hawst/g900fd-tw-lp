.class public Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;
.super Lcom/sec/android/app/samsungapps/ActionBarActivity;
.source "ProGuard"


# static fields
.field public static final GIFT_CARD_CODE_STRING:Ljava/lang/String; = "_gift_card_code_string"


# instance fields
.field a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;

.field b:Ljava/lang/String;

.field protected mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 44
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;-><init>()V

    .line 49
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->mContext:Landroid/content/Context;

    .line 50
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;

    .line 51
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->b:Ljava/lang/String;

    return-void
.end method

.method private static a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 289
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "<b>"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</b>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 290
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "<font color=\'#252525\'>"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</font><font color=\'#9E9A96\'>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</font>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v3, v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 291
    return-void
.end method


# virtual methods
.method protected deleteExpiredGiftCards()V
    .locals 2

    .prologue
    .line 271
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;

    if-nez v0, :cond_0

    .line 286
    :goto_0
    return-void

    .line 274
    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/GiftCardCommandBuilder;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/commands/GiftCardCommandBuilder;-><init>()V

    .line 275
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->getGiftCardCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/commands/GiftCardCommandBuilder;->deleteGiftCard(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    .line 276
    new-instance v1, Lcom/sec/android/app/samsungapps/eb;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/eb;-><init>(Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;)V

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0
.end method

.method protected displayData()V
    .locals 2

    .prologue
    const v1, 0x7f040085

    .line 57
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->setMainViewAndEmptyView(I)V

    .line 62
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->requestGiftCardDetail()V

    .line 63
    return-void

    .line 60
    :cond_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->setMainView(I)V

    goto :goto_0
.end method

.method public displaySoftkey()V
    .locals 2

    .prologue
    .line 233
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;

    if-nez v0, :cond_1

    .line 268
    :cond_0
    :goto_0
    return-void

    .line 237
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->getGiftCardStatusCode()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;->ACTIVE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;

    if-eq v0, v1, :cond_0

    .line 241
    const v0, 0x7f0c0169

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 243
    const v0, 0x7f0c0096

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 244
    const v1, 0x7f0802ea

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 246
    new-instance v1, Lcom/sec/android/app/samsungapps/dy;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/dy;-><init>(Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 68
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 70
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "_gift_card_code_string"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->b:Ljava/lang/String;

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 73
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->finish()V

    .line 76
    :cond_1
    const v0, 0x7f08017b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v0, 0x0

    new-array v4, v0, [I

    const v5, 0xb0003

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    .line 77
    iput-object p0, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->mContext:Landroid/content/Context;

    .line 78
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->displayData()V

    .line 79
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 84
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onDestroy()V

    .line 85
    return-void
.end method

.method protected onReceiveGiftCardDetail(Z)V
    .locals 7

    .prologue
    const v0, 0x7f0c01e3

    const v6, 0x7f0c007e

    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 89
    if-eqz p1, :cond_b

    .line 92
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const v0, 0x7f0c01d6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    const v1, 0x7f0c01e5

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;

    if-eqz v3, :cond_0

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 96
    :cond_0
    :goto_0
    return-void

    .line 92
    :cond_1
    invoke-virtual {p0, v6}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->getGiftCardImagePath()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_6

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getNetAPI()Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setNetAPI(Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setURL(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->getGiftCardStatusCode()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;

    move-result-object v0

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;->ACTIVE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;

    if-eq v0, v2, :cond_2

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    :cond_2
    :goto_1
    const v0, 0x7f0c01d7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;

    if-eqz v1, :cond_3

    if-nez v0, :cond_8

    :cond_3
    :goto_2
    const v0, 0x7f0c01e6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;

    if-eqz v1, :cond_4

    if-nez v0, :cond_9

    :cond_4
    :goto_3
    const v0, 0x7f0c01e8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;

    if-eqz v1, :cond_5

    if-nez v0, :cond_a

    :cond_5
    :goto_4
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->setItemPeriod()V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->setItemLastUsed()V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->setItemStatus()V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->setItemAvailableServices()V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->setItemDescription()V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->displaySoftkey()V

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->getGiftCardStatusCode()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;->ACTIVE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;

    if-ne v2, v3, :cond_7

    const v1, 0x7f0200f7

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setImageResource(I)V

    goto :goto_1

    :cond_7
    const v2, 0x7f0200f0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setImageResource(I)V

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_1

    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->getGiftCardCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_9
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f08017c

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0c01e7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->giftCardAmount()D

    move-result-wide v2

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->getCurrencyUnit()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getFormattedPrice(DLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_a
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08023a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0c01e9

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->getBalanceAmount()D

    move-result-wide v2

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->getCurrencyUnit()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getFormattedPrice(DLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 94
    :cond_b
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v6}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0c007f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0c0080

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0c0084

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0c0086

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/app/samsungapps/ed;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/ed;-><init>(Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0
.end method

.method public onSamsungAppsCreateOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;)Z
    .locals 1

    .prologue
    .line 296
    const/4 v0, 0x0

    return v0
.end method

.method protected requestGiftCardDetail()V
    .locals 4

    .prologue
    .line 301
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 318
    :cond_0
    :goto_0
    return-void

    .line 306
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;

    if-nez v0, :cond_2

    .line 307
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;

    .line 310
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;

    new-instance v3, Lcom/sec/android/app/samsungapps/ec;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/ec;-><init>(Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->giftCardDetail(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 317
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    goto :goto_0
.end method

.method public setItemAvailableServices()V
    .locals 5

    .prologue
    .line 214
    const v0, 0x7f0c01ed

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 215
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;

    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    .line 221
    :cond_0
    :goto_0
    return-void

    .line 219
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->getAvailableServices()Ljava/lang/String;

    move-result-object v1

    .line 220
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080181

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setItemDescription()V
    .locals 3

    .prologue
    .line 224
    const v0, 0x7f0c01ee

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 225
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;

    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    .line 230
    :cond_0
    :goto_0
    return-void

    .line 229
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f080272

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->getGiftCardDescription()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setItemLastUsed()V
    .locals 5

    .prologue
    .line 186
    const v0, 0x7f0c01eb

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 187
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;

    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    .line 196
    :cond_0
    :goto_0
    return-void

    .line 191
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->getLastUsageDate()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/util/AppsDateFormat;->getSystemLocalTimeItem(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 192
    if-eqz v1, :cond_2

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 193
    :cond_2
    const v1, 0x7f080139

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 195
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08017e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setItemPeriod()V
    .locals 5

    .prologue
    .line 173
    const v0, 0x7f0c01ea

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 174
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->mContext:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 183
    :cond_0
    :goto_0
    return-void

    .line 178
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->getUserRegisterDate()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/util/AppsDateFormat;->getSystemDateByLocalTimeItem(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 179
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->getUserExpireDate()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/util/AppsDateFormat;->getSystemDateByLocalTimeItem(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 181
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " ~ "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 182
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08016e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setItemStatus()V
    .locals 5

    .prologue
    .line 199
    const v0, 0x7f0c01ec

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 200
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;

    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    .line 211
    :cond_0
    :goto_0
    return-void

    .line 204
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardDetail;->getGiftCardStatusCode()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;->ACTIVE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard$IGiftCardStatus;

    if-ne v1, v2, :cond_2

    .line 206
    const v1, 0x7f080180

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 210
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08017f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 208
    :cond_2
    const v1, 0x7f0801f1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/GiftCardDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method
