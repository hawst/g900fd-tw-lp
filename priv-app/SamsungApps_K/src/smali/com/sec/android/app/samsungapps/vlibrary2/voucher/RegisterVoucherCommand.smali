.class public Lcom/sec/android/app/samsungapps/vlibrary2/voucher/RegisterVoucherCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _IRegisterVoucherView:Lcom/sec/android/app/samsungapps/vlibrary2/voucher/IRegisterVoucherView;

.field private _IViewInvoker:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/voucher/RegisterVoucherCommand;->_IViewInvoker:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    .line 21
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/voucher/RegisterVoucherCommand;Z)V
    .locals 0

    .prologue
    .line 14
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/voucher/RegisterVoucherCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/voucher/RegisterVoucherCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/voucher/IRegisterVoucherView;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/voucher/RegisterVoucherCommand;->_IRegisterVoucherView:Lcom/sec/android/app/samsungapps/vlibrary2/voucher/IRegisterVoucherView;

    return-object v0
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 2

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/voucher/RegisterVoucherCommand;->_IViewInvoker:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/voucher/RegisterVoucherCommand;->_Context:Landroid/content/Context;

    invoke-interface {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;->invoke(Landroid/content/Context;Ljava/lang/Object;)V

    .line 27
    return-void
.end method

.method public invokeCompleted(Lcom/sec/android/app/samsungapps/vlibrary2/voucher/IRegisterVoucherView;)V
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/voucher/RegisterVoucherCommand;->_IRegisterVoucherView:Lcom/sec/android/app/samsungapps/vlibrary2/voucher/IRegisterVoucherView;

    .line 32
    return-void
.end method

.method public registerCoupon(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/voucher/RegisterVoucherCommand;->_IRegisterVoucherView:Lcom/sec/android/app/samsungapps/vlibrary2/voucher/IRegisterVoucherView;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/voucher/IRegisterVoucherView;->startLoading()V

    .line 36
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/voucher/a;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/voucher/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/voucher/RegisterVoucherCommand;)V

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->customerCouponRegister(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 44
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 45
    return-void
.end method
