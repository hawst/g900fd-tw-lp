.class public Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue$IDownloadSingleItemObserver;
.implements Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/Downloader$IDownloadSingleItemResult;


# instance fields
.field a:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;

.field b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;

.field private e:Landroid/content/Context;

.field private f:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;->a:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;

    .line 30
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->e:Landroid/content/Context;

    .line 31
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 32
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->f:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;

    .line 33
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;

    .line 34
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->addObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue$IDownloadSingleItemObserver;)V

    .line 35
    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;)V
    .locals 5

    .prologue
    .line 39
    :goto_0
    const-string v0, "DetailButtonManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "exit "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/a;->a:[I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 40
    :cond_0
    :goto_1
    :pswitch_0
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;

    .line 41
    const-string v0, "DetailButtonManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "entry "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/a;->a:[I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 42
    :goto_2
    :pswitch_1
    return-void

    .line 39
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;->hideProgressBar()V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->removeObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/Downloader$IDownloadSingleItemResult;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    goto :goto_1

    .line 41
    :pswitch_3
    const-string v0, "DetailButtonManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "existInDLQueue GUID:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->getItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_3
    if-eqz v0, :cond_2

    sget-object p1, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;->c:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;

    goto/16 :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_3

    :cond_2
    sget-object p1, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;->d:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;

    goto/16 :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;->hideProgressBar()V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->f:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->getButtonState()Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;->setDetailButton(Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->f:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->getDeleteButtonState()Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DeleteButtonState;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;->setDeleteButton(Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DeleteButtonState;)V

    goto :goto_2

    :pswitch_5
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->getItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->addObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/Downloader$IDownloadSingleItemResult;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->getOldDownloadedSize()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->getOldDownloadedSize()J

    move-result-wide v1

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->getTotalSize()J

    move-result-wide v3

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;->notifyProgress(JJ)V

    goto/16 :goto_2

    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;->showProgressBar()V

    goto/16 :goto_2

    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;->showProgressBar()V

    goto/16 :goto_2

    :pswitch_6
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->getItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->removeObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/Downloader$IDownloadSingleItemResult;)V

    :cond_5
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue;->removeObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadStateQueue$IDownloadSingleItemObserver;)V

    goto/16 :goto_2

    .line 39
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 41
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method


# virtual methods
.method public isExecutable()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 127
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/a;->a:[I

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 156
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 132
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->f:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->getButtonState()Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 135
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/a;->b:[I

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->f:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;->getButtonState()Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    goto :goto_0

    .line 154
    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    .line 127
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
    .end packed-switch

    .line 135
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 203
    const-string v0, "DetailButtonManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onDestroy"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/a;->a:[I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 218
    :goto_0
    return-void

    .line 206
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;->e:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;)V

    goto :goto_0

    .line 209
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;->e:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;)V

    goto :goto_0

    .line 212
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;->e:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;)V

    goto :goto_0

    .line 215
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;->e:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;)V

    goto :goto_0

    .line 204
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onDetailLoaded()V
    .locals 3

    .prologue
    .line 175
    const-string v0, "DetailButtonManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onDetailLoaded"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/a;->a:[I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 183
    :goto_0
    :pswitch_0
    return-void

    .line 179
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;->b:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;)V

    goto :goto_0

    .line 182
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;->b:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;)V

    goto :goto_0

    .line 176
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onDownloadCanceled()V
    .locals 0

    .prologue
    .line 278
    return-void
.end method

.method public onDownloadFailed()V
    .locals 0

    .prologue
    .line 272
    return-void
.end method

.method public onDownloadSuccess()V
    .locals 0

    .prologue
    .line 284
    return-void
.end method

.method public onInstallFailedWithErrCode(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 355
    return-void
.end method

.method public onItemAdded(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;)V
    .locals 3

    .prologue
    .line 222
    const-string v0, "DetailButtonManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onItemAdded "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 225
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/a;->a:[I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 228
    :cond_0
    :goto_0
    return-void

    .line 227
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;->c:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;)V

    goto :goto_0

    .line 225
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public onItemRemoved(Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;)V
    .locals 3

    .prologue
    .line 237
    const-string v0, "DetailButtonManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onItemRemoved "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 240
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/a;->a:[I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 248
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 244
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;->d:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/b;)V

    goto :goto_0

    .line 240
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onLogedIn()V
    .locals 0

    .prologue
    .line 194
    return-void
.end method

.method public onLogedOff()V
    .locals 0

    .prologue
    .line 199
    return-void
.end method

.method public onPaymentSuccess()V
    .locals 0

    .prologue
    .line 361
    return-void
.end method

.method public onProgress(JJ)V
    .locals 3

    .prologue
    .line 288
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->getTotalSize()J

    move-result-wide v1

    invoke-interface {v0, p1, p2, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;->notifyProgress(JJ)V

    .line 290
    return-void
.end method

.method public onStateChanged()V
    .locals 5

    .prologue
    .line 294
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    if-eqz v0, :cond_0

    .line 296
    const-string v0, "DetailButtonManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onStateChanged = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->getState()Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/a;->c:[I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->getState()Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItemStateMachine$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 349
    :cond_0
    :goto_0
    return-void

    .line 300
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;->notifyProgressIndeterminated()V

    .line 301
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->CANCEL_DISABLED:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;->setDetailButton(Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;)V

    goto :goto_0

    .line 304
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;->notifyProgressIndeterminated()V

    .line 305
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->CANCEL_DISABLED:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;->setDetailButton(Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;)V

    goto :goto_0

    .line 308
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;->notifyProgressIndeterminated()V

    .line 309
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->CANCELLABLE:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;->setDetailButton(Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;)V

    goto :goto_0

    .line 312
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;

    const-wide/16 v1, 0x0

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/DownloadSingleItem;->getTotalSize()J

    move-result-wide v3

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;->notifyProgress(JJ)V

    .line 313
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->CANCELLABLE:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;->setDetailButton(Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;)V

    goto :goto_0

    .line 316
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;->notifyProgressIndeterminated()V

    .line 317
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->CANCEL_DISABLED:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;->setDetailButton(Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;)V

    goto :goto_0

    .line 320
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;->notifyProgressIndeterminated()V

    .line 321
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->CANCEL_DISABLED:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;->setDetailButton(Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;)V

    goto :goto_0

    .line 324
    :pswitch_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;->notifyProgressIndeterminated()V

    .line 325
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->CANCEL_DISABLED:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;->setDetailButton(Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;)V

    goto :goto_0

    .line 328
    :pswitch_7
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;->notifyProgressIndeterminated()V

    .line 329
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonProgressText;->INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonProgressText;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;->notifyProgressText(Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonProgressText;)V

    .line 330
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->CANCEL_DISABLED:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;->setDetailButton(Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;)V

    goto :goto_0

    .line 333
    :pswitch_8
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;->notifyProgressIndeterminated()V

    .line 334
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->CANCELLABLE:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;->setDetailButton(Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;)V

    goto/16 :goto_0

    .line 337
    :pswitch_9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;->notifyProgressIndeterminated()V

    goto/16 :goto_0

    .line 340
    :pswitch_a
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;->notifyProgressIndeterminated()V

    .line 341
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->CANCELLABLE:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;->setDetailButton(Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;)V

    goto/16 :goto_0

    .line 344
    :pswitch_b
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;->notifyProgressIndeterminated()V

    .line 345
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->d:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->CANCELLABLE:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;->setDetailButton(Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;)V

    goto/16 :goto_0

    .line 297
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method
