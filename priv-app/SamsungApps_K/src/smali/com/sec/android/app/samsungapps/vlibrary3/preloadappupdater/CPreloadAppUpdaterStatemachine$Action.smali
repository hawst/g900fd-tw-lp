.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum CHECK_UPDATE_PRECONDITON:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;

.field public static final enum CLEAR_OBSERVER:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;

.field public static final enum SIGNAL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;

.field public static final enum SIGNAL_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;

.field public static final enum START_DOWNLOAD:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 25
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;

    const-string v1, "CLEAR_OBSERVER"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;->CLEAR_OBSERVER:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;

    const-string v1, "CHECK_UPDATE_PRECONDITON"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;->CHECK_UPDATE_PRECONDITON:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;

    const-string v1, "SIGNAL_SUCCESS"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;->SIGNAL_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;

    const-string v1, "SIGNAL_FAILED"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;->SIGNAL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;

    const-string v1, "START_DOWNLOAD"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;->START_DOWNLOAD:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;

    .line 23
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;->CLEAR_OBSERVER:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;->CHECK_UPDATE_PRECONDITON:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;->SIGNAL_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;->SIGNAL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;->START_DOWNLOAD:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/CPreloadAppUpdaterStatemachine$Action;

    return-object v0
.end method
