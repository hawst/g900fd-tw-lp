.class public Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;
.super Lcom/sec/android/app/samsungapps/ContentListActivity;
.source "ProGuard"

# interfaces
.implements Landroid/hardware/SensorEventListener;
.implements Lcom/sec/android/app/samsungapps/widget/interfaces/IAdapterObserver;


# static fields
.field public static final EXTRA_BUTTONSTATE:Ljava/lang/String; = "_buttonState"

.field public static final EXTRA_CATEGORY:Ljava/lang/String; = "_category"

.field public static final EXTRA_DEEPLINK_CATEGORYID:Ljava/lang/String; = "_deeplink_categoryId"

.field public static final EXTRA_DESCRIPTION:Ljava/lang/String; = "_description"

.field public static final EXTRA_LISTTYPE:Ljava/lang/String; = "_listType"

.field public static final EXTRA_PRODUCTSETID:Ljava/lang/String; = "_productSetListId"

.field public static final EXTRA_TITLETEXT:Ljava/lang/String; = "_titleText"


# instance fields
.field a:[I

.field b:Landroid/view/View;

.field c:Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;

.field d:Landroid/widget/LinearLayout;

.field e:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

.field f:Landroid/widget/GridView;

.field g:Z

.field h:Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView$ITextSingleLineChanged;

.field private j:Landroid/content/Intent;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Landroid/hardware/SensorManager;

.field private n:Landroid/hardware/Sensor;

.field private o:I

.field private p:I

.field private q:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

.field private r:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

.field private s:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

.field private t:Lcom/sec/android/app/samsungapps/tobelog/LogData;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/16 v2, 0xff

    const/4 v1, 0x0

    .line 52
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ContentListActivity;-><init>()V

    .line 61
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->a:[I

    .line 68
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->k:Ljava/lang/String;

    .line 69
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->l:Ljava/lang/String;

    .line 73
    iput v2, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->o:I

    .line 74
    iput v2, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->p:I

    .line 76
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->b:Landroid/view/View;

    .line 77
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->c:Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;

    .line 78
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->d:Landroid/widget/LinearLayout;

    .line 83
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->q:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    .line 84
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->e:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    .line 86
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->f:Landroid/widget/GridView;

    .line 87
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->s:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    .line 185
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->g:Z

    .line 383
    new-instance v0, Lcom/sec/android/app/samsungapps/m;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/m;-><init>(Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->h:Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView$ITextSingleLineChanged;

    return-void

    .line 61
    nop

    :array_0
    .array-data 4
        0xa0005
        0xa0007
        0xa0008
    .end array-data
.end method

.method private a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 245
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->e:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->e:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 249
    :cond_0
    const-string v0, ""

    .line 256
    :goto_0
    return-object v0

    .line 253
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->e:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->listTitle:Ljava/lang/String;

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 670
    if-eqz p0, :cond_1

    .line 671
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 672
    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 673
    :cond_0
    const/4 p0, 0x0

    .line 677
    :cond_1
    return-object p0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;)V
    .locals 3

    .prologue
    .line 52
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/samsungapps/DescriptionTextActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "descriptionText"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "titleString"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->l:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 52
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->d:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->d:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/app/samsungapps/l;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/l;-><init>(Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    goto :goto_0
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)V
    .locals 1

    .prologue
    .line 623
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getContentQuery(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v0

    .line 625
    if-eqz v0, :cond_0

    .line 626
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->initAllListFirstItem()V

    .line 628
    :cond_0
    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v1, 0x0

    .line 318
    const v0, 0x7f0c02fa

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->d:Landroid/widget/LinearLayout;

    .line 319
    const v0, 0x7f0c02fc

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->b:Landroid/view/View;

    .line 321
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->d:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->b:Landroid/view/View;

    if-nez v0, :cond_1

    .line 352
    :cond_0
    :goto_0
    return-void

    .line 325
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 326
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 327
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->d:Landroid/widget/LinearLayout;

    const v1, 0x7f0c02fb

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->c:Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;

    .line 328
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->c:Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->h:Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView$ITextSingleLineChanged;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->setOnTextSingleLineChangedListener(Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView$ITextSingleLineChanged;)V

    .line 329
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->b:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 331
    const-string v0, "window"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 332
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getOrientation()I

    move-result v0

    .line 334
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 338
    :pswitch_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_2

    .line 339
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->c:Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->setMaxLines(I)V

    .line 343
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->c:Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->setMaxLineCount(I)V

    goto :goto_0

    .line 348
    :cond_2
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->c:Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->setMaxLines(I)V

    .line 349
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->c:Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->setMaxLineCount(I)V

    goto :goto_0

    .line 334
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 191
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->g:Z

    .line 193
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ContentListActivity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public getQueryType()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->r:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    return-object v0
.end method

.method public handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z
    .locals 2

    .prologue
    .line 516
    sget-object v0, Lcom/sec/android/app/samsungapps/n;->b:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 530
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/ContentListActivity;->handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z

    move-result v0

    :goto_0
    return v0

    .line 518
    :pswitch_0
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getYesOrNoEvent()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getExtraData()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->handleYesOrNoEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 520
    :pswitch_1
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getAccountEvent()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;->getAccountEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;->LogedOut:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    if-ne v0, v1, :cond_0

    .line 521
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/ContentListActivity;->handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z

    .line 524
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getAccountEvent()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;->getAccountEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;->LogedIn:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    if-ne v0, v1, :cond_1

    .line 525
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/ContentListActivity;->handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z

    .line 528
    :cond_1
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;

    invoke-static {p1}, Lcom/sec/android/app/samsungapps/CommonActivity;->handleAccountEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;)Z

    move-result v0

    goto :goto_0

    .line 516
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public handleUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V
    .locals 4

    .prologue
    .line 493
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getEventType()Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;->ContentDisplayEvent:Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    if-ne v0, v1, :cond_0

    .line 494
    sget-object v0, Lcom/sec/android/app/samsungapps/n;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getContentDisplayEvent()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->getContentDisplayEventType()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 510
    :cond_0
    :goto_0
    return-void

    .line 497
    :pswitch_0
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getContentDisplayEvent()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    const/16 v1, 0x8

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;I)V

    goto :goto_0

    .line 505
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->ProductSetList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getContentDisplayEvent()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getContentDisplayEvent()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->getID()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x4

    invoke-static {p0, v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 494
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public initSensorManager()V
    .locals 2

    .prologue
    .line 158
    const-string v0, "sensor"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->m:Landroid/hardware/SensorManager;

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->m:Landroid/hardware/SensorManager;

    if-nez v0, :cond_0

    .line 164
    :goto_0
    return-void

    .line 163
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->m:Landroid/hardware/SensorManager;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->n:Landroid/hardware/Sensor;

    goto :goto_0
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    .prologue
    .line 652
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 356
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ContentListActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 358
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->f:Landroid/widget/GridView;

    if-eqz v0, :cond_1

    .line 359
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v1, :cond_2

    .line 360
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->f:Landroid/widget/GridView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 364
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->mContentListQuery:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->e:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->mContentListQuery:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    if-eq v0, v1, :cond_0

    .line 365
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->mContentListQuery:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->e:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    .line 367
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->f:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->e:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->getIndexOfFirstItem()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setSelection(I)V

    .line 369
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->b()V

    .line 372
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->setTabTextOnConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 373
    return-void

    .line 362
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->f:Landroid/widget/GridView;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setNumColumns(I)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    .line 101
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ContentListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 104
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->j:Landroid/content/Intent;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->j:Landroid/content/Intent;

    const-string v1, "_deeplink_categoryId"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->j:Landroid/content/Intent;

    const-string v2, "_productSetListId"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->j:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "_listType"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->r:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->r:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/samsungapps/n;->c:[I

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->r:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    :cond_0
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopFree:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->a(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)V

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopPaid:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->a(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)V

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopAll:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->a(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)V

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopNew:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->a(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)V

    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->showFeaturedTitle()V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->j:Landroid/content/Intent;

    const-string v3, "_category"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getExtra(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->s:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->s:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    if-eqz v0, :cond_2

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCategoryContainer()Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->s:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    iget-object v3, v3, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryID:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->findCategoryByID(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->s:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->s:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->setSel(Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;Z)Z

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->r:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    if-nez v0, :cond_3

    if-eqz v1, :cond_4

    :cond_3
    const v0, 0x7f0400e6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->setMainView(I)V

    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->r:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->ProductSetList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    if-ne v0, v3, :cond_6

    if-eqz v2, :cond_6

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->createProductSetList(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->q:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->q:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->createBannerProductList(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;)Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->e:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CONTENTS_SET_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->setCurrentPage(Lcom/sec/android/app/samsungapps/tobelog/LogPage;)V

    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->e:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->setLogicalView(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->e:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->setAdapterObserver(Lcom/sec/android/app/samsungapps/widget/interfaces/IAdapterObserver;)V

    const v0, 0x7f0c0078

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->f:Landroid/widget/GridView;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->f:Landroid/widget/GridView;

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v5, :cond_8

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->f:Landroid/widget/GridView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setNumColumns(I)V

    :cond_5
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->showDescription()V

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "CHM"

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getCSC()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    sget-object v0, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->NONE:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->setListViewButtonState(Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;)V

    :goto_3
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->manageListButtons()V

    .line 105
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->initSensorManager()V

    .line 112
    invoke-static {}, Lcom/sec/android/app/samsungapps/initializer/Global;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_a

    .line 114
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->initializer(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializerFactory;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/k;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/k;-><init>(Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;)V

    invoke-interface {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializerFactory;->createInitializer(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializer$IIntializerObserver;)Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializer;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializer;->execute()V

    .line 137
    :goto_4
    return-void

    .line 104
    :pswitch_0
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->r:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    if-nez v0, :cond_7

    if-eqz v1, :cond_7

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->createContentCategoryList(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->q:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    :goto_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->q:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->create(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;)Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->e:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CATEGORY_PRODUCT_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->setCurrentPage(Lcom/sec/android/app/samsungapps/tobelog/LogPage;)V

    goto/16 :goto_1

    :cond_7
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->j:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "_listType"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getContentQuery(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->q:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    goto :goto_5

    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->f:Landroid/widget/GridView;

    invoke-virtual {v0, v5}, Landroid/widget/GridView;->setNumColumns(I)V

    goto/16 :goto_2

    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->j:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "_buttonState"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->setListViewButtonState(Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;)V

    goto :goto_3

    .line 136
    :cond_a
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->initialized()V

    goto :goto_4

    .line 104
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onDataLoadCompleted()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 536
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->j:Landroid/content/Intent;

    .line 537
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->j:Landroid/content/Intent;

    const-string v1, "_deeplink_categoryId"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 538
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->j:Landroid/content/Intent;

    const-string v1, "_titleText"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 539
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->j:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "_listType"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->r:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    .line 541
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->showDescription()V

    .line 543
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CATEGORY_PRODUCT_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->t:Lcom/sec/android/app/samsungapps/tobelog/LogData;

    if-nez v0, :cond_0

    .line 545
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->e:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->e:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 547
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v5

    sget-object v6, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->PAGE_PV_PATH_TIME:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getPreviousPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getPreviousPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    move-object v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->e:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    const/4 v7, 0x0

    invoke-virtual {v0, v7}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->categoryID:Ljava/lang/String;

    invoke-static {v5, v6, v1, v0, v2}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->createLogDataForPageView(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;Lcom/sec/android/app/samsungapps/tobelog/LogPage;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/tobelog/LogData;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->t:Lcom/sec/android/app/samsungapps/tobelog/LogData;

    .line 560
    :cond_0
    if-eqz v4, :cond_4

    .line 562
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->e:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->e:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->getCount()I

    move-result v0

    if-eqz v0, :cond_4

    .line 566
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->e:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_4

    .line 571
    if-eqz v3, :cond_1

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 573
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->a()Ljava/lang/String;

    move-result-object v0

    .line 576
    :goto_1
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 578
    if-eqz v1, :cond_2

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 580
    :cond_2
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/AppsTitle;->getString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 583
    :cond_3
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->a:[I

    const v5, 0xb0001

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    .line 589
    :cond_4
    return-void

    .line 547
    :cond_5
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->EMPTY:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-object v1, v0

    goto :goto_0

    :cond_6
    move-object v0, v3

    goto :goto_1
.end method

.method public onDataLoadingMore()V
    .locals 0

    .prologue
    .line 595
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->e:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->e:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->setAdapterObserver(Lcom/sec/android/app/samsungapps/widget/interfaces/IAdapterObserver;)V

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->e:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->release()V

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->q:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->q:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    instance-of v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;

    if-eqz v0, :cond_1

    .line 147
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->q:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;

    .line 148
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;->clearSortOrder()V

    .line 151
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->m:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_2

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->m:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 154
    :cond_2
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ContentListActivity;->onDestroy()V

    .line 155
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 632
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ContentListActivity;->onPause()V

    .line 634
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->m:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_0

    .line 635
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->m:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 638
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->t:Lcom/sec/android/app/samsungapps/tobelog/LogData;

    if-eqz v0, :cond_1

    .line 639
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->t:Lcom/sec/android/app/samsungapps/tobelog/LogData;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendNormalPageViewLog(Lcom/sec/android/app/samsungapps/tobelog/LogData;)V

    .line 640
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->t:Lcom/sec/android/app/samsungapps/tobelog/LogData;

    .line 643
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->g:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getCurrentPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CONTENTS_SET_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-ne v0, v1, :cond_2

    .line 644
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CONTENTS_SET_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->MOVE_OUT_FROM_MAIN_WITHOUT_ACTION:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendSummaryNormalLog(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;)V

    .line 646
    :cond_2
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->s:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    if-eqz v0, :cond_0

    .line 169
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCategoryContainer()Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

    move-result-object v0

    .line 170
    if-eqz v0, :cond_0

    .line 171
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->s:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->findCategoryByID(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->s:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    .line 172
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->s:Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->setSel(Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;Z)Z

    .line 176
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ContentListActivity;->onResume()V

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->m:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->n:Landroid/hardware/Sensor;

    if-eqz v0, :cond_1

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->m:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->n:Landroid/hardware/Sensor;

    const/4 v2, 0x3

    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 182
    :cond_1
    iput-boolean v3, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->g:Z

    .line 183
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 3

    .prologue
    .line 656
    sget-object v0, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 657
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    .line 659
    iget v2, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->o:I

    if-eq v2, v0, :cond_0

    .line 661
    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    iput v1, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->p:I

    .line 662
    iput v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->o:I

    .line 663
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->invalidate()V

    .line 665
    :cond_0
    return-void
.end method

.method public showDescription()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->e:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->getContentListQuery()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v0

    .line 264
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->r:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    if-nez v1, :cond_1

    .line 288
    :cond_0
    :goto_0
    return-void

    .line 269
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->j:Landroid/content/Intent;

    const-string v2, "_description"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->k:Ljava/lang/String;

    .line 271
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->k:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->k:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 273
    :cond_2
    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 275
    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getListDescription()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->k:Ljava/lang/String;

    .line 279
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 281
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->b()V

    .line 282
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 283
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->c:Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;

    if-eqz v0, :cond_0

    .line 285
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->c:Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public showFeaturedTitle()V
    .locals 6

    .prologue
    .line 198
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->j:Landroid/content/Intent;

    const-string v1, "_titleText"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getExtra(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 200
    if-nez v0, :cond_7

    .line 202
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->l:Ljava/lang/String;

    .line 217
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->l:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->l:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 219
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->l:Ljava/lang/String;

    .line 222
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->l:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->l:Ljava/lang/String;

    .line 224
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->l:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->l:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 226
    :cond_3
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/AppsTitle;->getString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->l:Ljava/lang/String;

    .line 230
    :cond_4
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 232
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->l:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->l:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->l:Ljava/lang/String;

    const v1, 0x7f0802fa

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 235
    :cond_5
    const v0, 0x7f08009d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->l:Ljava/lang/String;

    .line 239
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->l:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 240
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->l:Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->a:[I

    const v5, 0xb0001

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    .line 241
    return-void

    .line 204
    :cond_7
    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 206
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 207
    if-lez v0, :cond_0

    .line 209
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->l:Ljava/lang/String;

    goto/16 :goto_0

    .line 212
    :cond_8
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 214
    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CategorizedContentListActivity;->l:Ljava/lang/String;

    goto/16 :goto_0
.end method
