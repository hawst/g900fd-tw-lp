.class public final enum Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum ContentDisplayEvent:Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

.field public static final enum FeaturedTabEvent:Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

.field public static final enum FlextibleTabUpdated:Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

.field public static final enum MainTabEvent:Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

.field public static final enum MyTabEvent:Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

.field public static final enum SupportTabEvent:Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 6
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    const-string v1, "FeaturedTabEvent"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;->FeaturedTabEvent:Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    .line 7
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    const-string v1, "ContentDisplayEvent"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;->ContentDisplayEvent:Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    .line 8
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    const-string v1, "MyTabEvent"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;->MyTabEvent:Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    .line 9
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    const-string v1, "MainTabEvent"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;->MainTabEvent:Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    const-string v1, "FlextibleTabUpdated"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;->FlextibleTabUpdated:Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    .line 10
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    const-string v1, "SupportTabEvent"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;->SupportTabEvent:Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    .line 4
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;->FeaturedTabEvent:Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;->ContentDisplayEvent:Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;->MyTabEvent:Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;->MainTabEvent:Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;->FlextibleTabUpdated:Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;->SupportTabEvent:Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;->a:[Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;
    .locals 1

    .prologue
    .line 4
    const-class v0, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;
    .locals 1

    .prologue
    .line 4
    sget-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;->a:[Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    return-object v0
.end method
