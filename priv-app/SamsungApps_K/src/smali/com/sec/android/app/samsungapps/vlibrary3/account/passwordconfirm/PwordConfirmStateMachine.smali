.class public Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;
.source "ProGuard"


# static fields
.field private static a:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;-><init>()V

    .line 22
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine;

    return-object v0
.end method


# virtual methods
.method protected entry(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 3

    .prologue
    .line 78
    const-string v0, "PwordConfirmStateMachine"

    const-string v1, "entry"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 79
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/e;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 101
    :goto_0
    :pswitch_0
    return-void

    .line 82
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Action;->EXECUTE_P2CONFIRM:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 87
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Action;->CLEAR_CONFIRMED_TIME:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 90
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 91
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$State;->NOT_CONFIRMED:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 94
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Action;->NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 95
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$State;->CONFIRMED:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 98
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Action;->SET_CONFIRMED_TIME:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 79
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_5
        :pswitch_1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Event;)Z
    .locals 3

    .prologue
    .line 38
    const-string v0, "PwordConfirmStateMachine"

    const-string v1, "execute"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 39
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/e;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 73
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    return v0

    .line 44
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/e;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 48
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$State;->CONFIRMATION:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 51
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$State;->CONFIRMED:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 54
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$State;->NOT_CONFIRMED:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 59
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/e;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    :pswitch_6
    goto :goto_0

    .line 68
    :pswitch_7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$State;->CONFIRMATION:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 62
    :pswitch_8
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 65
    :pswitch_9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 39
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_5
    .end packed-switch

    .line 44
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 59
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public bridge synthetic execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 9
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Event;

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Event;)Z

    move-result v0

    return v0
.end method

.method protected exit(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 3

    .prologue
    .line 105
    const-string v0, "PwordConfirmStateMachine"

    const-string v1, "exit"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 106
    return-void
.end method
