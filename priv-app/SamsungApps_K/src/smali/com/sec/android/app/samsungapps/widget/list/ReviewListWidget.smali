.class public Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;
.super Lcom/sec/android/app/samsungapps/widget/CommonWidget;
.source "ProGuard"


# instance fields
.field a:Ljava/lang/String;

.field b:I

.field d:Landroid/widget/AbsListView$OnScrollListener;

.field private e:Landroid/content/Context;

.field private f:Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;

.field private g:Ljava/util/ArrayList;

.field private h:[Z

.field private final i:I

.field private final j:I

.field private final k:I

.field private l:I

.field private m:Lcom/sec/android/app/samsungapps/widget/interfaces/ICommentListWidgetClickListener;

.field private n:Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;

.field private o:Landroid/widget/ListView;

.field private p:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 42
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;)V

    .line 23
    const-string v0, "ReviewListWidget::"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->a:Ljava/lang/String;

    .line 28
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->h:[Z

    .line 30
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->i:I

    .line 31
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->j:I

    .line 32
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->k:I

    .line 34
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->l:I

    .line 38
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->o:Landroid/widget/ListView;

    .line 39
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->p:I

    .line 161
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/list/al;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/list/al;-><init>(Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->d:Landroid/widget/AbsListView$OnScrollListener;

    .line 43
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->a(Landroid/content/Context;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 47
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    const-string v0, "ReviewListWidget::"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->a:Ljava/lang/String;

    .line 28
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->h:[Z

    .line 30
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->i:I

    .line 31
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->j:I

    .line 32
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->k:I

    .line 34
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->l:I

    .line 38
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->o:Landroid/widget/ListView;

    .line 39
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->p:I

    .line 161
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/list/al;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/list/al;-><init>(Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->d:Landroid/widget/AbsListView$OnScrollListener;

    .line 48
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->a(Landroid/content/Context;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 53
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    const-string v0, "ReviewListWidget::"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->a:Ljava/lang/String;

    .line 28
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->h:[Z

    .line 30
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->i:I

    .line 31
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->j:I

    .line 32
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->k:I

    .line 34
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->l:I

    .line 38
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->o:Landroid/widget/ListView;

    .line 39
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->p:I

    .line 161
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/list/al;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/list/al;-><init>(Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->d:Landroid/widget/AbsListView$OnScrollListener;

    .line 54
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->a(Landroid/content/Context;)V

    .line 55
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;I)I
    .locals 0

    .prologue
    .line 22
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->l:I

    return p1
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;)Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->n:Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;

    return-object v0
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 58
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->e:Landroid/content/Context;

    .line 59
    const v0, 0x7f0400bc

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->initView(Landroid/content/Context;I)V

    .line 60
    const v0, 0x7f0c007d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 61
    new-instance v1, Lcom/sec/android/app/samsungapps/widget/list/aj;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/list/aj;-><init>(Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;[Z)[Z
    .locals 0

    .prologue
    .line 22
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->h:[Z

    return-object p1
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;)Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->f:Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;)V
    .locals 3

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->g:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->g:Ljava/util/ArrayList;

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->f:Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->getCommentList()Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ICommentListResult;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->g:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->g:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->e:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;)I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->l:I

    return v0
.end method


# virtual methods
.method public clearSavedPositon()V
    .locals 1

    .prologue
    .line 237
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->p:I

    .line 238
    return-void
.end method

.method public getAdapter()Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->n:Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;

    return-object v0
.end method

.method public loadMoreCommentList()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->n:Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;->getCount()I

    move-result v0

    if-gtz v0, :cond_1

    .line 228
    :cond_0
    :goto_0
    return-void

    .line 200
    :cond_1
    iput v2, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->l:I

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->f:Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;

    if-eqz v0, :cond_0

    .line 202
    const v0, 0x7f0c007b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 203
    const v0, 0x7f0c007c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 204
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->setWidgetState(I)V

    .line 205
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->f:Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;

    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->b:I

    new-instance v2, Lcom/sec/android/app/samsungapps/widget/list/am;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/widget/list/am;-><init>(Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->sendRequest(ILcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0
.end method

.method public loadOldY()I
    .locals 1

    .prologue
    .line 234
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->p:I

    return v0
.end method

.method public loadWidget()V
    .locals 3

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->f:Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;

    if-eqz v0, :cond_0

    .line 101
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->onWidgetViewState(I)V

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->f:Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;

    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->b:I

    new-instance v2, Lcom/sec/android/app/samsungapps/widget/list/ak;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/widget/list/ak;-><init>(Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;->sendRequest(ILcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 130
    :cond_0
    return-void
.end method

.method public refreshWidget()V
    .locals 0

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->loadWidget()V

    .line 159
    return-void
.end method

.method public release()V
    .locals 0

    .prologue
    .line 95
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->release()V

    .line 96
    return-void
.end method

.method public releaseWidget()V
    .locals 1

    .prologue
    .line 241
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->f:Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;

    .line 242
    return-void
.end method

.method public saveOldY(I)V
    .locals 0

    .prologue
    .line 231
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->p:I

    .line 232
    return-void
.end method

.method public setAdapter(Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;)V
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->n:Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;

    .line 83
    return-void
.end method

.method public setCommentListWidgetClickListener(Lcom/sec/android/app/samsungapps/widget/interfaces/ICommentListWidgetClickListener;)V
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->m:Lcom/sec/android/app/samsungapps/widget/interfaces/ICommentListWidgetClickListener;

    .line 87
    return-void
.end method

.method public setType(I)V
    .locals 0

    .prologue
    .line 78
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->b:I

    .line 79
    return-void
.end method

.method public setWidgetData(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 75
    check-cast p1, Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;

    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->f:Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;

    .line 76
    return-void
.end method

.method public updateWidget()V
    .locals 7

    .prologue
    .line 136
    const v0, 0x7f0c0132

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->o:Landroid/widget/ListView;

    .line 137
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->b:I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->f:Lcom/sec/android/app/samsungapps/widget/CommentListWidgetHelper;

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 138
    const v2, 0x7f04003e

    .line 143
    :goto_0
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->e:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->g:Ljava/util/ArrayList;

    iget v4, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->b:I

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->m:Lcom/sec/android/app/samsungapps/widget/interfaces/ICommentListWidgetClickListener;

    iget-object v6, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->h:[Z

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;ILcom/sec/android/app/samsungapps/widget/interfaces/ICommentListWidgetClickListener;[Z)V

    .line 145
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->l:I

    .line 147
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->setAdapter(Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;)V

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->o:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->n:Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 149
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "loadOldY() ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->loadOldY()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->o:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->loadOldY()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->o:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->o:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->d:Landroid/widget/AbsListView$OnScrollListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 153
    return-void

    .line 140
    :cond_0
    const v2, 0x7f04003c

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->o:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/ReviewListWidget;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f02012e

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
