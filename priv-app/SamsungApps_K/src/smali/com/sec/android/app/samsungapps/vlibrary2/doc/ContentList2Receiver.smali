.class public Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2Receiver;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# instance fields
.field private _Received:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2;

.field map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2Receiver;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 12
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2;

    const/16 v1, 0x19

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2Receiver;->_Received:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2;

    .line 13
    return-void
.end method


# virtual methods
.method public addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2Receiver;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2Receiver;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 36
    :cond_0
    return-void
.end method

.method public clearContainer()V
    .locals 0

    .prologue
    .line 42
    return-void
.end method

.method public closeMap()V
    .locals 2

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2Receiver;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    if-eqz v0, :cond_0

    .line 25
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2Receiver;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 26
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2Receiver;->_Received:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2;->add(Ljava/lang/Object;)Z

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2Receiver;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 29
    :cond_0
    return-void
.end method

.method public findString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    return-object v0
.end method

.method public getResult()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2Receiver;->_Received:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2;

    return-object v0
.end method

.method public openMap()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2Receiver;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 19
    return-void
.end method

.method public setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2Receiver;->_Received:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentList2;->setHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 59
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    return v0
.end method
