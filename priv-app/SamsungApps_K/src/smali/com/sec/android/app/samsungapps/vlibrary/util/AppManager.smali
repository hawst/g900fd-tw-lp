.class public Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field mContext:Landroid/content/Context;

.field mPackageManager:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;Landroid/content/pm/PackageManager;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/pm/PackageManager;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 37
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->mContext:Landroid/content/Context;

    .line 38
    return-void
.end method

.method public static compareVersion(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;
    .locals 7

    .prologue
    .line 313
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;->same:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;

    .line 315
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->getDecimalList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 316
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->getDecimalList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 320
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-eq v0, v4, :cond_0

    .line 322
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;->irregularFormat:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;

    .line 347
    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 354
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 356
    return-object v0

    .line 326
    :cond_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 327
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 329
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 331
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 332
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 334
    if-eq v6, v0, :cond_1

    .line 336
    if-le v6, v0, :cond_2

    .line 341
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;->lefthigher:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;

    goto :goto_0

    .line 346
    :cond_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;->leftlower:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method private static getDecimalList(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 4

    .prologue
    .line 240
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 244
    if-eqz p0, :cond_2

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2

    .line 246
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_2

    .line 251
    const/16 v0, 0x2e

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 253
    if-nez v2, :cond_0

    .line 256
    const-string v0, ""

    move-object v3, p0

    move-object p0, v0

    move-object v0, v3

    .line 269
    :goto_1
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 280
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 258
    :cond_0
    const/4 v0, -0x1

    if-eq v2, v0, :cond_1

    .line 260
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 261
    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    .line 266
    :cond_1
    const-string v0, ""

    move-object v3, p0

    move-object p0, v0

    move-object v0, v3

    goto :goto_1

    .line 276
    :catch_0
    move-exception v0

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 285
    :cond_2
    return-object v1
.end method


# virtual methods
.method public amISystemApp()Z
    .locals 1

    .prologue
    .line 451
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 452
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isSystemApp(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public checkHashcodeOfSignature(ILjava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 586
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/16 v2, 0x40

    invoke-virtual {v1, p2, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget-object v2, v1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    .line 589
    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 590
    invoke-virtual {v4}, Landroid/content/pm/Signature;->hashCode()I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 592
    if-ne p1, v4, :cond_1

    .line 593
    const/4 v0, 0x1

    .line 599
    :cond_0
    :goto_1
    return v0

    .line 589
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 597
    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method public doIHaveDeletePackagePermission()Z
    .locals 1

    .prologue
    .line 69
    const-string v0, "android.permission.DELETE_PACKAGES"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->doIHavePermission(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public doIHavePermission(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 60
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 61
    const/4 v0, 0x1

    .line 63
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getApkSourceDir(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 536
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->mPackageManager:Landroid/content/pm/PackageManager;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 537
    if-nez v1, :cond_0

    .line 551
    :goto_0
    return-object v0

    .line 541
    :cond_0
    iget-object v0, v1, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 543
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 547
    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public getLaunchIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 467
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 468
    return-object v0
.end method

.method public getMyPackageInfo()Landroid/content/pm/PackageInfo;
    .locals 3

    .prologue
    .line 161
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->getThisPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 162
    if-eqz v0, :cond_0

    .line 171
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPacakgeVersionName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 362
    const-string v0, ""

    .line 366
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->getPackageInfo(Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 368
    if-eqz v1, :cond_0

    .line 370
    iget-object v0, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 377
    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public getPackageInfo(Ljava/lang/String;)Landroid/content/pm/PackageInfo;
    .locals 2

    .prologue
    .line 221
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->mPackageManager:Landroid/content/pm/PackageManager;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 222
    if-eqz v0, :cond_0

    .line 231
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPackagePermission(Ljava/lang/String;)[Ljava/lang/String;
    .locals 2

    .prologue
    .line 382
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->mPackageManager:Landroid/content/pm/PackageManager;

    const/16 v1, 0x1000

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 384
    if-eqz v0, :cond_0

    .line 385
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 391
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPackageVersion(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 203
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->mPackageManager:Landroid/content/pm/PackageManager;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 205
    if-eqz v0, :cond_0

    .line 207
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 214
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPackageVersionCode(Ljava/lang/String;)J
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 181
    .line 185
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->mPackageManager:Landroid/content/pm/PackageManager;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 187
    if-eqz v1, :cond_0

    .line 189
    iget v0, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 196
    :cond_0
    :goto_0
    int-to-long v0, v0

    return-wide v0

    .line 194
    :catch_0
    move-exception v0

    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getThisPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTopPackageName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 473
    const-string v1, ""

    .line 477
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->mContext:Landroid/content/Context;

    const-string v2, "activity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 478
    if-nez v0, :cond_0

    .line 480
    const-string v0, "PackageManager::getTopPackageName::actMgr is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move-object v0, v1

    .line 504
    :goto_0
    return-object v0

    .line 486
    :cond_0
    const/4 v2, 0x1

    :try_start_0
    invoke-virtual {v0, v2}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v0

    .line 487
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    .line 488
    if-nez v0, :cond_1

    .line 490
    const-string v0, "PackageManager::getTopPackageName::topActivity is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move-object v0, v1

    .line 491
    goto :goto_0

    .line 494
    :cond_1
    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 498
    :catch_0
    move-exception v0

    const-string v0, "PackageManager::getTopPackageName: ecurityException"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 499
    const-string v0, ""

    goto :goto_0
.end method

.method public install(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 140
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 141
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 142
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "application/vnd.android.package-archive"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 144
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_0

    .line 146
    :try_start_0
    const-string v1, "android.intent.extra.INSTALLER_PACKAGE_NAME"

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->getThisPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    .line 154
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 155
    return-void

    :catch_0
    move-exception v1

    goto :goto_0

    .line 152
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public isDeletableApp(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 425
    if-eqz p1, :cond_0

    const-string v2, ""

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-ne v2, v1, :cond_1

    .line 446
    :cond_0
    :goto_0
    return v0

    .line 430
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->getPackageInfo(Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 432
    if-eqz v2, :cond_0

    .line 437
    iget-object v2, v2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v2, v2, 0x1

    if-nez v2, :cond_0

    move v0, v1

    .line 446
    goto :goto_0
.end method

.method public isExecutable(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 457
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 458
    if-nez v0, :cond_0

    .line 460
    const/4 v0, 0x0

    .line 462
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isPackageInstalled(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 42
    if-nez p1, :cond_1

    .line 54
    :cond_0
    :goto_0
    return v0

    .line 46
    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->mPackageManager:Landroid/content/pm/PackageManager;

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 54
    goto :goto_0

    .line 48
    :catch_0
    move-exception v2

    const-string v2, "com.sec.android.app.camerafirmware_"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-ne v2, v1, :cond_0

    move v0, v1

    .line 49
    goto :goto_0
.end method

.method public isSystemApp(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 397
    const/4 v1, 0x0

    .line 401
    if-eqz p1, :cond_0

    const-string v2, ""

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eq v2, v0, :cond_0

    .line 403
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->getPackageInfo(Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 408
    if-eqz v2, :cond_0

    .line 410
    iget-object v2, v2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 420
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public launchApp(Ljava/lang/String;Z)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 124
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->getLaunchIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 125
    if-eqz v1, :cond_0

    .line 126
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    .line 135
    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 128
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 131
    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Error;->printStackTrace()V

    goto :goto_0
.end method

.method public readMetaData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 510
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->mPackageManager:Landroid/content/pm/PackageManager;

    const/16 v2, 0x80

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 511
    if-nez v1, :cond_1

    .line 530
    :cond_0
    :goto_0
    return-object v0

    .line 515
    :cond_1
    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    .line 516
    if-eqz v1, :cond_0

    .line 520
    invoke-virtual {v1, p2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    goto :goto_0

    .line 522
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 526
    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public readMetaDataInt(Ljava/lang/String;Ljava/lang/String;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 557
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->mPackageManager:Landroid/content/pm/PackageManager;

    const/16 v2, 0x80

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 558
    if-nez v1, :cond_1

    .line 581
    :cond_0
    :goto_0
    return v0

    .line 562
    :cond_1
    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    .line 563
    if-eqz v1, :cond_0

    .line 567
    invoke-virtual {v1, p2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result v0

    goto :goto_0

    .line 569
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 573
    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 577
    :catch_2
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
