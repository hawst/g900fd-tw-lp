.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum CHECK_AM_I_SYSTEMAPP:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

.field public static final enum CHECK_PACKAGE:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

.field public static final enum DELETE_AS_NOT_SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

.field public static final enum DELETE_AS_SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

.field public static final enum FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

.field public static final enum IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

.field public static final enum SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 12
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    const-string v1, "CHECK_PACKAGE"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;->CHECK_PACKAGE:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    const-string v1, "CHECK_AM_I_SYSTEMAPP"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;->CHECK_AM_I_SYSTEMAPP:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    const-string v1, "DELETE_AS_SYSTEM_APP"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;->DELETE_AS_SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    const-string v1, "DELETE_AS_NOT_SYSTEM_APP"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;->DELETE_AS_NOT_SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    const-string v1, "SUCCESS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    .line 10
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;->CHECK_PACKAGE:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;->CHECK_AM_I_SYSTEMAPP:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;->DELETE_AS_SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;->DELETE_AS_NOT_SYSTEM_APP:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$State;

    return-object v0
.end method
