.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponInterface;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponList;


# instance fields
.field private _Context:Landroid/content/Context;

.field private _CouponGiftCardReceiver:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;

.field private _IGiftCardCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardCommandBuilder;

.field private _IGiftCardCouponWidget:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponWidget;

.field private _IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

.field private _ItemList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

.field private _bDestroyed:Z

.field private registerGiftCardResponseItem:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/RegisterGiftCardResponseItem;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponWidget;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->_CouponGiftCardReceiver:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;

    .line 27
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->registerGiftCardResponseItem:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/RegisterGiftCardResponseItem;

    .line 30
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->_Context:Landroid/content/Context;

    .line 31
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->_ItemList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    .line 32
    iput-object p5, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->_IGiftCardCouponWidget:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponWidget;

    .line 33
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->_IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

    .line 34
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->_IGiftCardCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardCommandBuilder;

    .line 36
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;

    invoke-direct {v0, p4, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->_CouponGiftCardReceiver:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;

    .line 37
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;)Z
    .locals 1

    .prologue
    .line 19
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->_bDestroyed:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->onReceiveCouponGiftCardList()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->onFailToReceiveCouponGiftCardList()V

    return-void
.end method

.method private onFailToReceiveCouponGiftCardList()V
    .locals 2

    .prologue
    .line 164
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->_IGiftCardCouponWidget:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponWidget;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponWidget$WidgetState;->WIDGET_STATE_RETRY:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponWidget$WidgetState;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponWidget;->setState(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponWidget$WidgetState;)V

    .line 165
    return-void
.end method

.method private onReceiveCouponGiftCardList()V
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->_ItemList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->_IGiftCardCouponWidget:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponWidget;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponWidget$WidgetState;->WIDGET_STATE_NODATA:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponWidget$WidgetState;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponWidget;->setState(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponWidget$WidgetState;)V

    .line 121
    :goto_0
    return-void

    .line 118
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->updateCouponGiftAvailability()V

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->_IGiftCardCouponWidget:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponWidget;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponWidget$WidgetState;->WIDGET_STATE_LOAD_COMPLETE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponWidget$WidgetState;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponWidget;->setState(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponWidget$WidgetState;)V

    goto :goto_0
.end method

.method private updateCouponGiftAvailability()V
    .locals 12

    .prologue
    const/4 v9, 0x0

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->_ItemList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;->size()I

    move-result v10

    .line 134
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardAvailabilityChecker;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardAvailabilityChecker;-><init>()V

    .line 135
    new-instance v11, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponAvailabiltyChecker;

    invoke-direct {v11}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponAvailabiltyChecker;-><init>()V

    move v8, v9

    .line 137
    :goto_0
    if-ge v8, v10, :cond_3

    .line 139
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->_ItemList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    invoke-interface {v1, v8}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;->get(I)Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;

    .line 140
    invoke-interface {v7}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;->isGiftCard()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 142
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->_IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;->getSelPaymentMethod()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    move-result-object v1

    .line 144
    invoke-interface {v7}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;->getGiftCard()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->_IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getPaymentPrice()D

    move-result-wide v3

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->_IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

    invoke-interface {v5}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isDiscounted()Z

    move-result v5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardAvailabilityChecker;->checkAvailability(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;DZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 146
    check-cast v7, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;

    const/4 v1, 0x1

    invoke-virtual {v7, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;->setEnabled(Z)V

    .line 137
    :cond_0
    :goto_1
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    goto :goto_0

    .line 150
    :cond_1
    check-cast v7, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;

    invoke-virtual {v7, v9}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;->setEnabled(Z)V

    goto :goto_1

    .line 153
    :cond_2
    invoke-interface {v7}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;->isCoupon()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 155
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->_IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;->getSelPaymentMethod()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    move-result-object v2

    .line 156
    invoke-interface {v7}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;->getCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    move-result-object v3

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->_IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getPaymentPrice()D

    move-result-wide v4

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->_IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isDiscounted()Z

    move-result v6

    move-object v1, v11

    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponAvailabiltyChecker;->checkAvailability(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;DZ)Z

    move-result v1

    .line 157
    check-cast v7, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;

    invoke-virtual {v7, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;->setEnabled(Z)V

    goto :goto_1

    .line 160
    :cond_3
    return-void
.end method


# virtual methods
.method public clearSel()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 209
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->_ItemList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;->clearSel()Z

    .line 210
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->_IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;->selCoupon(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;)Z

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->_IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;->selGiftCard(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;)Z

    .line 212
    const/4 v0, 0x1

    return v0
.end method

.method protected createCommand(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 2

    .prologue
    .line 79
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/PurchaseCouponGiftCardRequestor;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->_IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/PurchaseCouponGiftCardRequestor;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftCardReceiver;)V

    return-object v0
.end method

.method public get(I)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->_ItemList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;->get(I)Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;

    return-object v0
.end method

.method public isKorea()Z
    .locals 1

    .prologue
    .line 200
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    .line 201
    if-nez v0, :cond_0

    .line 202
    const/4 v0, 0x0

    .line 204
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isKorea()Z

    move-result v0

    goto :goto_0
.end method

.method public onClickRegisterGiftCard(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 175
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/RegisterGiftCardResponseItem;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/RegisterGiftCardResponseItem;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->registerGiftCardResponseItem:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/RegisterGiftCardResponseItem;

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->_IGiftCardCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardCommandBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->registerGiftCardResponseItem:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/RegisterGiftCardResponseItem;

    invoke-interface {v0, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardCommandBuilder;->registerGiftCard(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/RegisterGiftCardResponseItem;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    .line 177
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/b;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 191
    return-void
.end method

.method public onPaymentMethodChanged()V
    .locals 0

    .prologue
    .line 127
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->clearSel()Z

    .line 128
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->onReceiveCouponGiftCardList()V

    .line 129
    return-void
.end method

.method public onSelItem(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 52
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->_ItemList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;->selectItem(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 54
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;->Null()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->_IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;->selCoupon(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;)Z

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->_IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;->selGiftCard(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;)Z

    .line 59
    :cond_0
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;->isCoupon()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->_IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;->getCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;->selCoupon(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;)Z

    .line 67
    :goto_0
    const/4 v0, 0x1

    .line 69
    :goto_1
    return v0

    .line 65
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->_IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;->getGiftCard()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;->selGiftCard(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;)Z

    goto :goto_0

    .line 69
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public release()V
    .locals 1

    .prologue
    .line 169
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->_bDestroyed:Z

    .line 170
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->_IGiftCardCouponWidget:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponWidget;

    .line 171
    return-void
.end method

.method protected request()V
    .locals 3

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->_IGiftCardCouponWidget:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponWidget;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponWidget$WidgetState;->WIDGET_STATE_LOADING:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponWidget$WidgetState;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponWidget;->setState(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/IGiftCardCouponWidget$WidgetState;)V

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->_CouponGiftCardReceiver:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->createCommand(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    .line 90
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/a;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 108
    return-void
.end method

.method public retryLoading()V
    .locals 0

    .prologue
    .line 195
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->request()V

    .line 196
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->_ItemList:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;->size()I

    move-result v0

    return v0
.end method

.method public start()V
    .locals 0

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardMgr;->request()V

    .line 75
    return-void
.end method
