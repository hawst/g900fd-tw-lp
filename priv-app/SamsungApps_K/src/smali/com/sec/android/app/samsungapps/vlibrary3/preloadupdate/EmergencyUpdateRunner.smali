.class public Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;


# instance fields
.field a:Landroid/os/Handler;

.field b:Ljava/util/ArrayList;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

.field private d:Landroid/content/Context;

.field private e:Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;

.field private f:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner$IEmergencyUpdateObserver;

.field private g:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/PreloadAppUpdaterFactory;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/PreloadAppUpdaterFactory;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->c:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    .line 40
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->a:Landroid/os/Handler;

    .line 196
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->b:Ljava/util/ArrayList;

    .line 35
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->d:Landroid/content/Context;

    .line 36
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->e:Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;

    .line 37
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->g:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/PreloadAppUpdaterFactory;

    .line 38
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 99
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Event;->ALL_UPDATE_DONE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->a(Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Event;)V

    .line 125
    :goto_0
    return-void

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadItem;

    .line 104
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 105
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->d:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IBaseContextUtil;->getFakeModel(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 106
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->g:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/PreloadAppUpdaterFactory;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->d:Landroid/content/Context;

    invoke-interface {v2, v3, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/PreloadAppUpdaterFactory;->createUpdater(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/PreloadUpdateInfo;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/PreloadAppUpdater;

    move-result-object v0

    .line 108
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/b;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;)V

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/PreloadAppUpdater;->addObserver(Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/PreloadAppUpdater$IPreloadSingleItemUpdaterObserver;)V

    .line 124
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/PreloadAppUpdater;->execute()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->a()V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Event;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->a(Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Event;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadListResult;)V
    .locals 7

    .prologue
    .line 26
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->d:Landroid/content/Context;

    invoke-direct {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadListResult;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadItem;

    :try_start_0
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadItem;->getGUID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->getPackageVersionCode(Ljava/lang/String;)J

    move-result-wide v3

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadItem;->getRegisteredVersionCode()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Event;->RECEIVE_EMPTY:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->a(Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Event;)V

    :goto_1
    return-void

    :cond_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Event;->RECEIVE_NOT_EMPTY:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->a(Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Event;)V

    goto :goto_1
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Event;)V
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->a:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/a;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Event;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 56
    return-void
.end method


# virtual methods
.method public getState()Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->c:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    return-object v0
.end method

.method public bridge synthetic getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->getState()Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    move-result-object v0

    return-object v0
.end method

.method public onAction(Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 75
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/e;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 93
    :cond_0
    :goto_0
    return-void

    .line 78
    :pswitch_0
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadListResult;

    invoke-direct {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadListResult;-><init>()V

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadListResult;->clear()V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->Is3GAvailable(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->IsWifiAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_3

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Event;->NETWORK_NOT_AVAILABE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->a(Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Event;)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/d;

    invoke-direct {v3, p0, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadListResult;)V

    invoke-virtual {v0, v2, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->getEmergencyDownloadList(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    goto :goto_0

    .line 81
    :pswitch_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadItem;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/GetEmergencyDownloadItem;->productName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->e:Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->d:Landroid/content/Context;

    invoke-interface {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;->createNotiPopup(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopup;

    move-result-object v0

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/c;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;)V

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopup;->askEmergencyUpdatePopup(Ljava/util/ArrayList;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopup$INotiResponseOkCancel;)V

    goto :goto_0

    .line 84
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->f:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner$IEmergencyUpdateObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->f:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner$IEmergencyUpdateObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner$IEmergencyUpdateObserver;->onFailed()V

    goto :goto_0

    .line 87
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->f:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner$IEmergencyUpdateObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->f:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner$IEmergencyUpdateObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner$IEmergencyUpdateObserver;->onFinished()V

    goto :goto_0

    .line 90
    :pswitch_4
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->a()V

    goto/16 :goto_0

    .line 75
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public bridge synthetic onAction(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->onAction(Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Action;)V

    return-void
.end method

.method public run()V
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Event;->RUN:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->a(Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$Event;)V

    .line 45
    return-void
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner$IEmergencyUpdateObserver;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->f:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner$IEmergencyUpdateObserver;

    .line 61
    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;)V
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->c:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    .line 66
    return-void
.end method

.method public bridge synthetic setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunner;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/EmergencyUpdateRunnerStateMachine$State;)V

    return-void
.end method
