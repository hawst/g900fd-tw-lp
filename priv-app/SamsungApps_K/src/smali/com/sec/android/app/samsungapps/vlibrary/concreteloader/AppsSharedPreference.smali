.class public Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static final APPS_PREFERENCES:Ljava/lang/String; = "SamsungAppsPreferences"

.field private static final APPS_SHARED_PREFERENCES:Ljava/lang/String; = "SamsungAppsSharedPreferences"

.field public static final SHARED_PREFERENCES_PLASMA_KEY_SELECTEDMCC:Ljava/lang/String; = "SelectedMcc"

.field public static final SHARED_PREFERENCES_UNA_KEY_SETTING:Ljava/lang/String; = "una_setting"

.field public static final SP_KEY_AD_PREFERENCE_SETTING:Ljava/lang/String; = "ad_preference_setting"

.field public static final SP_KEY_ALLOW_ALL_HOST:Ljava/lang/String; = "allowAllHost"

.field public static final SP_KEY_APPS_LATEST_VERSION:Ljava/lang/String; = "samsungAppsLatestVersion"

.field public static final SP_KEY_AUTO_COMPLETE_SEARCH_SETTING:Ljava/lang/String; = "auto_complete_search_setting"

.field public static final SP_KEY_BADGE_COUNT:Ljava/lang/String; = "BadgeCount"

.field public static final SP_KEY_CACHED_TIME:Ljava/lang/String; = "CachedTime"

.field public static final SP_KEY_CHINA_CHARGE_NEVER_SHOW:Ljava/lang/String; = "ChinaChargePopupShowAgain"

.field public static final SP_KEY_CHINA_DATA_CHARGE_NEVER_SHOW:Ljava/lang/String; = "ChinaDataChargeNeverShow"

.field public static final SP_KEY_CHINA_WLAN_CONNECT_NEVER_SHOW:Ljava/lang/String; = "ChinaConnectWLANNeverShow"

.field public static final SP_KEY_DF_AD_SETTING:Ljava/lang/String; = "DF_AD_SETTING"

.field public static final SP_KEY_DF_ALREADY_NAME_AUTH:Ljava/lang/String; = "DF_already_name_auth"

.field public static final SP_KEY_DF_FIRST_AUTO_LOGIN:Ljava/lang/String; = "DF_FIRST_AUTO_LOGIN"

.field public static final SP_KEY_DF_REAL_AGE:Ljava/lang/String; = "DF_real_Age"

.field public static final SP_KEY_DF_SKIP_SACCOUNT:Ljava/lang/String; = "DF_SKIP_SACCOUNT"

.field public static final SP_KEY_DISCLAIMER_SKIP:Ljava/lang/String; = "DisclaimerSkip"

.field public static final SP_KEY_DISCLAIMER_VERSION:Ljava/lang/String; = "DisclaimerVersion"

.field public static final SP_KEY_ENABLE_TENCENT:Ljava/lang/String; = "EnableTencent"

.field public static final SP_KEY_KNOX_WELCOME_PAGE_CHECK:Ljava/lang/String; = "knox_welcome_page_check"

.field public static final SP_KEY_LAST_PHONE_NUMBER:Ljava/lang/String; = "LASTPHONENUMBER"

.field public static final SP_KEY_MIN_PRICE_CREDIT:Ljava/lang/String; = "minPriceCreditCard"

.field public static final SP_KEY_NEED_ALIPAY_UPDATE:Ljava/lang/String; = "needAlipayUpdate"

.field public static final SP_KEY_NEED_APPS_UPDATE:Ljava/lang/String; = "needSamsungAppsUpdate"

.field protected static final SP_KEY_NOTIFY_APP_UPDATES_SETTING:Ljava/lang/String; = "notify_app_updates_setting"

.field public static final SP_KEY_NOTIFY_STORE_ACTIVITIES_SETTING:Ljava/lang/String; = "notify_store_activities_setting"

.field public static final SP_KEY_PURCHASE_METHOD:Ljava/lang/String; = "PurchaseMethod"

.field public static final SP_KEY_PURCHASE_PROTECTION_SETTING:Ljava/lang/String; = "purchase_protection_setting"

.field public static final SP_KEY_REAL_COUNTRY_CODE:Ljava/lang/String; = "real_country_code"

.field public static final SP_KEY_SERVER_LOAD_LEVEL:Ljava/lang/String; = "serverLoadLevel"

.field public static final SP_KEY_SUGGEST_ALLSHARE_CONTENT:Ljava/lang/String; = "SuggestAllShareContent"

.field public static final SP_KEY_UPDATE_ITEM_DISABLED_LIST:Ljava/lang/String; = "update_item_disabled_list"

.field public static final SP_KEY_UPDATE_ITEM_MAIN_SETTING:Ljava/lang/String; = "update_main_setting"

.field public static final SP_KEY_UPDATE_ITEM_SELF_SETTING:Ljava/lang/String; = "update_self_setting"

.field public static final SP_KEY_USER_KEYWORDS_SEARCH_SETTING:Ljava/lang/String; = "user_keywords_search_setting"

.field public static final SP_UPDATE_INTERVAL:Ljava/lang/String; = "auto_update_interval"


# instance fields
.field private mPrefer:Landroid/content/SharedPreferences;

.field private mSharedPrefer:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->mPrefer:Landroid/content/SharedPreferences;

    .line 11
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->mSharedPrefer:Landroid/content/SharedPreferences;

    .line 60
    const-string v0, "SamsungAppsPreferences"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->mPrefer:Landroid/content/SharedPreferences;

    .line 63
    const-string v0, "SamsungAppsSharedPreferences"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->mSharedPrefer:Landroid/content/SharedPreferences;

    .line 65
    return-void
.end method


# virtual methods
.method protected checkNameAuth()Z
    .locals 1

    .prologue
    .line 286
    const/4 v0, 0x0

    return v0
.end method

.method public clearAllSharedPreferenceData()V
    .locals 2

    .prologue
    .line 168
    const-string v0, "SelectedMcc"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setSharedConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 169
    const-string v0, "una_setting"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setSharedConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 170
    const-string v0, "DisclaimerSkip"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setSharedConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 171
    const-string v0, "DisclaimerVersion"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setSharedConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 173
    const-string v0, "BadgeCount"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 174
    const-string v0, "ChinaChargePopupShowAgain"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 175
    const-string v0, "CachedTime"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 176
    const-string v0, "LASTPHONENUMBER"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 177
    const-string v0, "PurchaseMethod"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 178
    const-string v0, "serverLoadLevel"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 179
    const-string v0, "SuggestAllShareContent"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 180
    const-string v0, "DF_AD_SETTING"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 181
    const-string v0, "DF_already_name_auth"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 182
    const-string v0, "DF_FIRST_AUTO_LOGIN"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 183
    const-string v0, "DF_real_Age"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 184
    const-string v0, "DF_SKIP_SACCOUNT"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 185
    const-string v0, "EnableTencent"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 186
    const-string v0, "real_country_code"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 189
    return-void
.end method

.method public getConfigItem(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 75
    const-string v0, ""

    .line 76
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->mPrefer:Landroid/content/SharedPreferences;

    invoke-interface {v1, p1, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 78
    return-object v0
.end method

.method public getPurchaseProtectionSettingValue()I
    .locals 3

    .prologue
    .line 427
    const-string v0, "purchase_protection_setting"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getSharedConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 431
    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 434
    :cond_0
    const/4 v0, 0x0

    .line 439
    :goto_0
    return v0

    .line 436
    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public getSharedConfigItem(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 117
    const-string v0, ""

    .line 118
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->mSharedPrefer:Landroid/content/SharedPreferences;

    invoke-interface {v1, p1, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 120
    return-object v0
.end method

.method public setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 89
    const/4 v0, 0x0

    .line 90
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->mPrefer:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 93
    if-nez p2, :cond_0

    .line 95
    const-string p2, ""

    .line 98
    :cond_0
    invoke-interface {v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 100
    if-eqz v2, :cond_1

    .line 102
    const/4 v0, 0x1

    .line 104
    :cond_1
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 106
    return v0
.end method

.method public setSharedConfigItem(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 131
    const/4 v1, 0x0

    .line 132
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->mSharedPrefer:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 133
    const/4 v2, 0x0

    .line 135
    if-nez p2, :cond_0

    .line 137
    const-string p2, ""

    .line 140
    :cond_0
    const-string v4, "SelectedMcc"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-ne v4, v0, :cond_1

    .line 144
    :try_start_0
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 145
    invoke-interface {v3, p1, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 157
    :goto_0
    if-eqz v2, :cond_2

    .line 161
    :goto_1
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 163
    return v0

    .line 154
    :cond_1
    invoke-interface {v3, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    goto :goto_0

    .line 150
    :catch_0
    move-exception v4

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method
