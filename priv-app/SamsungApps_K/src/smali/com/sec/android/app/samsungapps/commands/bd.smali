.class final Lcom/sec/android/app/samsungapps/commands/bd;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/ICardInfo;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;)V
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/commands/bd;->a:Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final hasRegisteredCard()Z
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/bd;->a:Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;->b(Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->hasCard()Z

    move-result v0

    return v0
.end method

.method public final setCardInfo(Z)V
    .locals 2

    .prologue
    .line 148
    if-eqz p1, :cond_0

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/bd;->a:Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;->a(Lcom/sec/android/app/samsungapps/commands/DownloadCommandBuilder;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v0

    const/4 v1, 0x1

    iput v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->cardInfo:I

    .line 152
    :cond_0
    return-void
.end method
