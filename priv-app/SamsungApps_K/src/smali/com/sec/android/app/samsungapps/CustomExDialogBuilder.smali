.class public Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;
.super Lcom/sec/android/app/samsungapps/CustomDialogBuilder;
.source "ProGuard"


# instance fields
.field private b:Landroid/content/Context;

.field protected mCheckBox:Landroid/widget/CheckBox;

.field protected mHelper:Lcom/sec/android/app/samsungapps/ActivityHelper;

.field protected mImageView:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

.field protected mLayout:Landroid/view/View;

.field protected mTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;-><init>(Landroid/content/Context;)V

    .line 37
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->b:Landroid/content/Context;

    .line 38
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->a(Landroid/content/Context;)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->b:Landroid/content/Context;

    .line 49
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->a(Landroid/content/Context;)V

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 63
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->b:Landroid/content/Context;

    .line 64
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->a(Landroid/content/Context;)V

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 5

    .prologue
    const v4, 0x7f0c01ce

    const/4 v3, 0x1

    .line 71
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->b:Landroid/content/Context;

    .line 72
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 74
    const v1, 0x7f040081

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->mLayout:Landroid/view/View;

    .line 75
    new-instance v0, Lcom/sec/android/app/samsungapps/ActivityHelper;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->mLayout:Landroid/view/View;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/ActivityHelper;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->mHelper:Lcom/sec/android/app/samsungapps/ActivityHelper;

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->mHelper:Lcom/sec/android/app/samsungapps/ActivityHelper;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/samsungapps/ActivityHelper;->show(I)V

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->mLayout:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->mCheckBox:Landroid/widget/CheckBox;

    .line 80
    new-array v0, v3, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->mCheckBox:Landroid/widget/CheckBox;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->isNull([Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isChina()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->mCheckBox:Landroid/widget/CheckBox;

    new-instance v1, Lcom/sec/android/app/samsungapps/co;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/co;-><init>(Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_1

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 88
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->mCheckBox:Landroid/widget/CheckBox;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->getPaddingLeft()I

    move-result v2

    const/high16 v3, 0x41200000    # 10.0f

    mul-float/2addr v0, v3

    const/high16 v3, 0x3f000000    # 0.5f

    add-float/2addr v0, v3

    float-to-int v0, v0

    add-int/2addr v0, v2

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->getPaddingRight()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CheckBox;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/widget/CheckBox;->setPadding(IIII)V

    .line 94
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    if-eqz v0, :cond_2

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->a:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->mLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setView(Landroid/view/View;)V

    .line 98
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->mLayout:Landroid/view/View;

    const v1, 0x7f0c01cb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->mTextView:Landroid/widget/TextView;

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->mLayout:Landroid/view/View;

    const v1, 0x7f0c01cc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->mImageView:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    .line 100
    return-void
.end method


# virtual methods
.method public getCheckBoxValue()Z
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->mHelper:Lcom/sec/android/app/samsungapps/ActivityHelper;

    const v1, 0x7f0c01ce

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ActivityHelper;->getCheckBoxValue(I)Z

    move-result v0

    .line 114
    return v0
.end method

.method public setImageURL(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 119
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->mImageView:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getNetAPI()Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setNetAPI(Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;)V

    .line 122
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->mImageView:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setURL(Ljava/lang/String;)V

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/CustomExDialogBuilder;->mImageView:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setVisibility(I)V

    .line 125
    :cond_0
    return-void
.end method
