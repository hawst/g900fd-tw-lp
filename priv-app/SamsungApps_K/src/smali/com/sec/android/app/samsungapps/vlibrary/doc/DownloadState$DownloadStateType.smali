.class public final enum Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;

.field public static final enum DOWNLOADCOMPLETED:Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;

.field public static final enum DOWNLOADING:Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;

.field public static final enum IDLE:Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;

.field public static final enum INSTALLCOMPLETED:Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;

.field public static final enum INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 6
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;

    .line 7
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;

    const-string v1, "DOWNLOADING"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;->DOWNLOADING:Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;

    .line 8
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;

    const-string v1, "DOWNLOADCOMPLETED"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;->DOWNLOADCOMPLETED:Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;

    .line 9
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;

    const-string v1, "INSTALLING"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;->INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;

    .line 10
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;

    const-string v1, "INSTALLCOMPLETED"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;->INSTALLCOMPLETED:Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;

    .line 4
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;->DOWNLOADING:Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;->DOWNLOADCOMPLETED:Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;->INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;->INSTALLCOMPLETED:Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;
    .locals 1

    .prologue
    .line 4
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;
    .locals 1

    .prologue
    .line 4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary/doc/DownloadState$DownloadStateType;

    return-object v0
.end method
