.class final Lcom/sec/android/app/samsungapps/widget/purchase/s;
.super Landroid/widget/ArrayAdapter;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;

.field private b:Landroid/view/LayoutInflater;

.field private c:I

.field private d:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;Landroid/content/Context;Ljava/util/List;)V
    .locals 2

    .prologue
    const v1, 0x7f04008a

    .line 169
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/s;->a:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;

    .line 170
    invoke-direct {p0, p2, v1, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 172
    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/s;->b:Landroid/view/LayoutInflater;

    .line 173
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/s;->c:I

    .line 174
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/s;->d:Landroid/content/Context;

    .line 175
    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 179
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/widget/purchase/s;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodSelectable;

    .line 184
    if-nez p2, :cond_0

    .line 185
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/s;->b:Landroid/view/LayoutInflater;

    iget v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/s;->c:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 186
    new-instance v1, Lcom/sec/android/app/samsungapps/widget/purchase/t;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/s;->a:Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/s;->d:Landroid/content/Context;

    invoke-direct {v1, v2, v3, p2}, Lcom/sec/android/app/samsungapps/widget/purchase/t;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/PaymentMethodWidget;Landroid/content/Context;Landroid/view/View;)V

    .line 188
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 192
    :goto_0
    invoke-virtual {v1, p1}, Lcom/sec/android/app/samsungapps/widget/purchase/t;->a(I)V

    .line 193
    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/t;->a(Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodSelectable;)V

    .line 194
    return-object p2

    .line 190
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/widget/purchase/t;

    goto :goto_0
.end method
