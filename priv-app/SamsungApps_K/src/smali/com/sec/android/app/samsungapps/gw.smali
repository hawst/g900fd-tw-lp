.class final Lcom/sec/android/app/samsungapps/gw;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;)V
    .locals 0

    .prologue
    .line 636
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 699
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 693
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const v7, 0x7f08014e

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 640
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->a(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 641
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->setEnabledCardAll()V

    .line 642
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->cardCvcNum:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->d:[Landroid/text/InputFilter;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 643
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->cardCvcNum:Landroid/widget/EditText;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    const v3, 0x7f08028f

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v2, v7, v3}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 644
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->cardCvcNum:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v1, v7, v2}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 646
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->c(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 648
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->f:Ljava/util/regex/Pattern;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->c(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 650
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->card_img_1:Landroid/widget/ImageView;

    const v1, 0x7f02013c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 651
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->d(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->b(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 685
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->checkButton()V

    .line 687
    return-void

    .line 653
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->g:Ljava/util/regex/Pattern;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->c(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 655
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->card_img_2:Landroid/widget/ImageView;

    const v1, 0x7f020139

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 656
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->e(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->b(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 658
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->h:Ljava/util/regex/Pattern;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->c(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 660
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->card_img_3:Landroid/widget/ImageView;

    const v1, 0x7f02012f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 661
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->f(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->b(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 662
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->cardCvcNum:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->e:[Landroid/text/InputFilter;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 663
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->cardCvcNum:Landroid/widget/EditText;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    const v3, 0x7f08028f

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v2, v7, v3}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 664
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->cardCvcNum:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v1, v7, v2}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 666
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->i:Ljava/util/regex/Pattern;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->c(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 668
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->card_img_4:Landroid/widget/ImageView;

    const v1, 0x7f020135

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 669
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->g(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->b(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0

    .line 671
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->c(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "31180"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->c(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "9900"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 673
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->d(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->b(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0

    .line 675
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->j:Ljava/util/regex/Pattern;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->c(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 677
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->card_img_5:Landroid/widget/ImageView;

    const v1, 0x7f020137

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 678
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->h(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->b(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0

    .line 682
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gw;->a:Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;->b(Lcom/sec/android/app/samsungapps/RegisterCreditCardActivity;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0
.end method
