.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSConfirmMSGCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _IIPSMSConfirmMSGData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSConfirmMSGCommand$IIPSMSConfirmMSGData;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSConfirmMSGCommand$IIPSMSConfirmMSGData;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSConfirmMSGCommand;->_IIPSMSConfirmMSGData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSConfirmMSGCommand$IIPSMSConfirmMSGData;

    .line 15
    return-void
.end method


# virtual methods
.method protected askConfirmMSG()V
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSConfirmMSGCommand;->_IIPSMSConfirmMSGData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSConfirmMSGCommand$IIPSMSConfirmMSGData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSConfirmMSGCommand$IIPSMSConfirmMSGData;->getAskConfirmMSGViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSConfirmMSGCommand;->_Context:Landroid/content/Context;

    invoke-interface {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;->invoke(Landroid/content/Context;Ljava/lang/Object;)V

    .line 37
    return-void
.end method

.method protected existConfirmMSG()Z
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSConfirmMSGCommand;->_IIPSMSConfirmMSGData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSConfirmMSGCommand$IIPSMSConfirmMSGData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSConfirmMSGCommand$IIPSMSConfirmMSGData;->existConfirmMSG()Z

    move-result v0

    return v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSConfirmMSGCommand;->_IIPSMSConfirmMSGData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSConfirmMSGCommand$IIPSMSConfirmMSGData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSConfirmMSGCommand$IIPSMSConfirmMSGData;->getConfirmMessage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSConfirmMSGCommand;->existConfirmMSG()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 21
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSConfirmMSGCommand;->askConfirmMSG()V

    .line 27
    :goto_0
    return-void

    .line 25
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSConfirmMSGCommand;->onFinalResult(Z)V

    goto :goto_0
.end method

.method public onUserAgree(Z)V
    .locals 0

    .prologue
    .line 41
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSConfirmMSGCommand;->onFinalResult(Z)V

    .line 42
    return-void
.end method
