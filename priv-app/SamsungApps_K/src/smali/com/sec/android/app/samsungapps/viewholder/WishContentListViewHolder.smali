.class public Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;
.super Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;
.source "ProGuard"


# instance fields
.field a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

.field private b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;Landroid/content/Context;Landroid/view/View;Landroid/graphics/drawable/Drawable;Ljava/lang/String;ZILcom/sec/android/app/samsungapps/view/IContentArrayAdapter;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct/range {p0 .. p8}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;-><init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;Landroid/content/Context;Landroid/view/View;Landroid/graphics/drawable/Drawable;Ljava/lang/String;ZILcom/sec/android/app/samsungapps/view/IContentArrayAdapter;)V

    .line 21
    check-cast p8, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    iput-object p8, p0, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    .line 22
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->b:Landroid/content/Context;

    .line 23
    return-void
.end method


# virtual methods
.method protected prepareView()V
    .locals 2

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->displayNormalItem()V

    .line 109
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->showCommonStatus()V

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->isCheckDeletionMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->deletecheckbox:Landroid/widget/CheckedTextView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->deletecheckbox:Landroid/widget/CheckedTextView;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;->isSelectedToBeDeleted()Z

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 111
    :cond_0
    return-void
.end method

.method public showCommonStatus()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/16 v5, 0x8

    const/4 v2, 0x0

    .line 29
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    if-nez v0, :cond_1

    .line 103
    :cond_0
    :goto_0
    return-void

    .line 34
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    iget v3, p0, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->_Position:I

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;

    .line 35
    const-string v3, "N"

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;->forSaleYn:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    .line 36
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->layout_list_itemly:Landroid/view/View;

    invoke-virtual {v3, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 38
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->layout_list_itemly:Landroid/view/View;

    new-instance v4, Lcom/sec/android/app/samsungapps/viewholder/s;

    invoke-direct {v4, p0}, Lcom/sec/android/app/samsungapps/viewholder/s;-><init>(Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 46
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->layout_list_itemly:Landroid/view/View;

    new-instance v4, Lcom/sec/android/app/samsungapps/viewholder/t;

    invoke-direct {v4, p0}, Lcom/sec/android/app/samsungapps/viewholder/t;-><init>(Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 55
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->isCheckDeletionMode()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 57
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->layout_list_itemly_rightly:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 58
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->layout_list_itemly_pricely:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 59
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->deletecheckbox:Landroid/widget/CheckedTextView;

    invoke-virtual {v3, v2}, Landroid/widget/CheckedTextView;->setVisibility(I)V

    .line 60
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->deleteCheckboxGap:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 70
    :goto_2
    if-eqz v0, :cond_7

    .line 71
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->hideRating()V

    .line 72
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->setViewDisabled()V

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->layout_list_itemly_pricely:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->layout_list_itemly_pricely:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 77
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->categoryName:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->categoryName:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 81
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->notavailableAppLayout:Landroid/view/View;

    if-eqz v0, :cond_4

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->notavailableAppLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 100
    :cond_4
    :goto_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->_View:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->_View:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    :cond_5
    move v0, v2

    .line 35
    goto :goto_1

    .line 64
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->layout_list_itemly_rightly:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 65
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->layout_list_itemly_pricely:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 66
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->deletecheckbox:Landroid/widget/CheckedTextView;

    invoke-virtual {v3, v5}, Landroid/widget/CheckedTextView;->setVisibility(I)V

    .line 67
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->deleteCheckboxGap:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 86
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->showRating()V

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->layout_list_itemly_pricely:Landroid/view/View;

    if-eqz v0, :cond_8

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->layout_list_itemly_pricely:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 91
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->categoryName:Landroid/widget/TextView;

    if-eqz v0, :cond_9

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->categoryName:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 95
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->notavailableAppLayout:Landroid/view/View;

    if-eqz v0, :cond_4

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/WishContentListViewHolder;->notavailableAppLayout:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3
.end method
