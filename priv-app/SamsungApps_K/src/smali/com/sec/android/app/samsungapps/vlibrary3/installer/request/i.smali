.class final Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/i;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;)V
    .locals 0

    .prologue
    .line 479
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/i;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 484
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getNetAPI()Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    move-result-object v0

    .line 485
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/i;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->c(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;)Lorg/apache/http/client/methods/HttpGet;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->execute(Lorg/apache/http/client/methods/HttpGet;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 486
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/i;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;

    invoke-static {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;Lorg/apache/http/HttpResponse;)V
    :try_end_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_3

    .line 500
    :goto_0
    return-void

    .line 487
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    .line 489
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/i;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->REQUEST_FAIL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;)V

    goto :goto_0

    .line 490
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 492
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/i;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->REQUEST_FAIL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;)V

    goto :goto_0

    .line 493
    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 495
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/i;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->REQUEST_FAIL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;)V

    goto :goto_0

    .line 496
    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    .line 498
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/i;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;->REQUEST_FAIL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILE;Lcom/sec/android/app/samsungapps/vlibrary3/installer/request/RequestFILEStateMachine$Event;)V

    goto :goto_0
.end method
