.class public final enum Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;

.field public static final enum ADD_TO_WISH:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;

.field public static final enum DELETE_WISH:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;

.field public static final enum DISABLED:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;

.field public static final enum GO_TO_WISH:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 38
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;

    const-string v1, "DISABLED"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;->DISABLED:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;

    .line 39
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;

    const-string v1, "ADD_TO_WISH"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;->ADD_TO_WISH:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;

    .line 40
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;

    const-string v1, "DELETE_WISH"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;->DELETE_WISH:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;

    .line 41
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;

    const-string v1, "GO_TO_WISH"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;->GO_TO_WISH:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;

    .line 36
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;->DISABLED:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;->ADD_TO_WISH:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;->DELETE_WISH:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;->GO_TO_WISH:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishButtonState$ButtonState;

    return-object v0
.end method
