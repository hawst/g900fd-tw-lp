.class public Lcom/sec/android/app/samsungapps/MainTabEventStorage;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;


# static fields
.field private static b:Lcom/sec/android/app/samsungapps/MainTabEventStorage;


# instance fields
.field a:Ljava/util/ArrayList;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/MainTabEventStorage;->a:Ljava/util/ArrayList;

    .line 14
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/MainTabEventStorage;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/sec/android/app/samsungapps/MainTabEventStorage;->b:Lcom/sec/android/app/samsungapps/MainTabEventStorage;

    if-nez v0, :cond_0

    .line 20
    new-instance v0, Lcom/sec/android/app/samsungapps/MainTabEventStorage;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/MainTabEventStorage;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/MainTabEventStorage;->b:Lcom/sec/android/app/samsungapps/MainTabEventStorage;

    .line 22
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/MainTabEventStorage;->b:Lcom/sec/android/app/samsungapps/MainTabEventStorage;

    return-object v0
.end method


# virtual methods
.method public activate()V
    .locals 1

    .prologue
    .line 27
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->addSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V

    .line 28
    return-void
.end method

.method public deactivate()V
    .locals 1

    .prologue
    .line 32
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->removeSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V

    .line 33
    return-void
.end method

.method public handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z
    .locals 2

    .prologue
    .line 48
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->YesOrNoEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    if-ne v0, v1, :cond_0

    .line 50
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/MainTabEventStorage;->a:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/app/samsungapps/ez;

    invoke-direct {v1, p1, p2}, Lcom/sec/android/app/samsungapps/ez;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 52
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public replay(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V
    .locals 3

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/MainTabEventStorage;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/ez;

    .line 39
    if-eqz p1, :cond_0

    .line 40
    iget-object v2, v0, Lcom/sec/android/app/samsungapps/ez;->a:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

    iget-boolean v0, v0, Lcom/sec/android/app/samsungapps/ez;->b:Z

    invoke-interface {p1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;->handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z

    goto :goto_0

    .line 43
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/MainTabEventStorage;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 44
    return-void
.end method
