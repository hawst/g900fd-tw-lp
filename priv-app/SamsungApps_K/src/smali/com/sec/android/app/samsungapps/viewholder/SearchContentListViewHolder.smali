.class public Lcom/sec/android/app/samsungapps/viewholder/SearchContentListViewHolder;
.super Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;
.source "ProGuard"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;Landroid/content/Context;Landroid/view/View;Landroid/graphics/drawable/Drawable;Ljava/lang/String;ZILcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct/range {p0 .. p8}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;-><init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;Landroid/content/Context;Landroid/view/View;Landroid/graphics/drawable/Drawable;Ljava/lang/String;ZILcom/sec/android/app/samsungapps/view/IContentArrayAdapter;)V

    .line 15
    return-void
.end method


# virtual methods
.method protected handleDownloading()V
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/SearchContentListViewHolder;->progressLayout:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 38
    return-void
.end method

.method protected handleInstalling()V
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/SearchContentListViewHolder;->progressLayout:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 48
    return-void
.end method

.method protected handleWating()V
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/SearchContentListViewHolder;->progressLayout:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 43
    return-void
.end method

.method protected prepareView()V
    .locals 0

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/SearchContentListViewHolder;->displayNormalItem()V

    .line 30
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/SearchContentListViewHolder;->showDownloadStatus()V

    .line 31
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/SearchContentListViewHolder;->showInstalledText()V

    .line 32
    return-void
.end method

.method public showCommonStatus()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 19
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/SearchContentListViewHolder;->deletecheckbox:Landroid/widget/CheckedTextView;

    if-eqz v0, :cond_0

    .line 20
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/SearchContentListViewHolder;->deletecheckbox:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setVisibility(I)V

    .line 23
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/SearchContentListViewHolder;->layout_list_itemly_rightly:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 24
    return-void
.end method
