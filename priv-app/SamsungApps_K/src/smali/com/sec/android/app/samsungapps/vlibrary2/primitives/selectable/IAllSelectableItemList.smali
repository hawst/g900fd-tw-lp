.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/IAllSelectableItemList;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;


# virtual methods
.method public abstract addIAllSelectableItemListListener(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/IAllSelectableItemList$IAllSelectableItemListListener;)V
.end method

.method public abstract deSelectAll()Z
.end method

.method public abstract deSelectItem(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;)Z
.end method

.method public abstract isAllDeselected()Z
.end method

.method public abstract isAllSelected()Z
.end method

.method public abstract isAllSelected(Landroid/content/Context;)Z
.end method

.method public abstract selectAll()Z
.end method

.method public abstract selectAll(Landroid/content/Context;)Z
.end method
