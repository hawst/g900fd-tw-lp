.class public Lcom/sec/android/app/samsungapps/widget/SectionListAdapter$ResultFilter;
.super Landroid/widget/Filter;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;

.field private b:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;)V
    .locals 1

    .prologue
    .line 150
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter$ResultFilter;->a:Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    .line 151
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter$ResultFilter;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getCurrentWord()Ljava/lang/String;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter$ResultFilter;->b:Ljava/lang/String;

    return-object v0
.end method

.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 7

    .prologue
    .line 155
    new-instance v1, Landroid/widget/Filter$FilterResults;

    invoke-direct {v1}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter$ResultFilter;->a:Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;->mOrgItems:Ljava/util/ArrayList;

    iput-object v0, v1, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter$ResultFilter;->a:Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;->mOrgItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, v1, Landroid/widget/Filter$FilterResults;->count:I

    .line 160
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-ltz v0, :cond_2

    .line 161
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter$ResultFilter;->b:Ljava/lang/String;

    .line 162
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    .line 164
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter$ResultFilter;->a:Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;->mOrgItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SectionListItemView;

    .line 167
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SectionListItemView;->isHeader()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SectionListItemView;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 168
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 171
    :cond_1
    iput-object v3, v1, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 172
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, v1, Landroid/widget/Filter$FilterResults;->count:I

    .line 174
    :cond_2
    return-object v1
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 3

    .prologue
    .line 179
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter$ResultFilter;->a:Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;

    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, v1, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;->mFilteredItems:Ljava/util/ArrayList;

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter$ResultFilter;->a:Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;->clear()V

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter$ResultFilter;->a:Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;->mFilteredItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SectionListItemView;

    .line 182
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter$ResultFilter;->a:Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 184
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter$ResultFilter;->a:Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SectionListAdapter;->notifyDataSetChanged()V

    .line 185
    return-void
.end method
