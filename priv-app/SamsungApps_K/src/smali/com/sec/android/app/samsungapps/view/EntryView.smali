.class public Lcom/sec/android/app/samsungapps/view/EntryView;
.super Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;
.source "ProGuard"


# static fields
.field protected static final CARD_CVC_MIN_LENGTH:I = 0x3

.field protected static final CARD_NUMBER_MIN_LENGTH:I = 0xc

.field protected static final EMAIL_MAX_LENGTH:I = 0x32

.field protected static final EMAIL_MIN_LENGTH:I = 0x7

.field protected static final ID_DIALOG_DATE_PICKER:I = 0x2

.field protected static final ID_DIALOG_INVALID_BIRTH_DATE:I = 0x8

.field protected static final ID_DIALOG_INVALID_CARD_NUM:I = 0xa

.field protected static final ID_DIALOG_INVALID_EMAIL:I = 0x5

.field protected static final ID_DIALOG_INVALID_NAME_AUTH:I = 0xf

.field protected static final ID_DIALOG_INVALID_PASSWORD:I = 0x6

.field protected static final ID_DIALOG_INVALID_PASSWORD_CONFIRM:I = 0x7

.field protected static final ID_DIALOG_INVALID_USER_ID:I = 0xd

.field protected static final ID_DIALOG_INVALID_VALUE:I = 0xe

.field protected static final ID_DIALOG_NOT_ACCEPT_TERMS:I = 0x3

.field protected static final ID_DIALOG_VALID_INPUT:I = 0x4

.field protected static final ID_MAX_LENGTH:I = 0x32

.field protected static final PASSWORD_MIN_LENGTH:I = 0x6

.field protected static final SSN_LEFT_MAX_LENGTH:I = 0x6

.field protected static final SSN_RIGHT_MAX_LENGTH:I = 0x7

.field protected static final TYPE_VALID_DATA_CARDCVC:I = 0x5

.field protected static final TYPE_VALID_DATA_CARDNUMBER:I = 0x4

.field protected static final TYPE_VALID_DATA_CARDTYPE:I = 0xe

.field protected static final TYPE_VALID_DATA_DATE:I = 0x3

.field protected static final TYPE_VALID_DATA_EMAIL:I = 0x1

.field protected static final TYPE_VALID_DATA_EMAIL_EX:I = 0xb

.field protected static final TYPE_VALID_DATA_ID:I = 0x6

.field protected static final TYPE_VALID_DATA_MONTH:I = 0xc

.field protected static final TYPE_VALID_DATA_NAME_AUTH:I = 0x8

.field protected static final TYPE_VALID_DATA_PASSWD:I = 0x2

.field protected static final TYPE_VALID_DATA_SSN_LEFT:I = 0x9

.field protected static final TYPE_VALID_DATA_SSN_RIGHT:I = 0xa

.field protected static final TYPE_VALID_DATA_TEXT:I = 0x0

.field protected static final TYPE_VALID_DATA_VALUE:I = 0x7

.field protected static final TYPE_VALID_DATA_YEAR:I = 0xd


# instance fields
.field protected mCurDlgType:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/DinamicOrientationActivity;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/view/EntryView;->mCurDlgType:I

    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 228
    const/4 v0, 0x1

    .line 230
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    move v2, v1

    .line 231
    :goto_0
    if-ge v2, v3, :cond_1

    .line 235
    add-int/lit8 v4, v2, 0x1

    :try_start_0
    invoke-virtual {p1, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 236
    invoke-virtual {p0, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_0

    move v0, v1

    .line 231
    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 241
    :catch_0
    move-exception v0

    .line 243
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "EntryView::_isAllowedString::"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move v0, v1

    .line 244
    goto :goto_1

    .line 248
    :cond_1
    return v0
.end method


# virtual methods
.method protected checkValidData(Ljava/lang/String;I)Z
    .locals 6

    .prologue
    const/16 v5, 0xc

    const/4 v3, 0x6

    const v4, 0x7f0802cb

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 307
    .line 311
    packed-switch p2, :pswitch_data_0

    .line 448
    :pswitch_0
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1a

    .line 451
    :cond_0
    invoke-virtual {p0, v4}, Lcom/sec/android/app/samsungapps/view/EntryView;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2, v1, v0}, Lcom/sec/android/app/samsungapps/NotiDialog;->showDialog(Landroid/content/Context;Ljava/lang/String;ZZ)V

    .line 461
    :goto_0
    return v0

    .line 314
    :pswitch_1
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/view/EntryView;->isValidEmail(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 316
    const v2, 0x7f0802c6

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/view/EntryView;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2, v1, v0}, Lcom/sec/android/app/samsungapps/NotiDialog;->showDialog(Landroid/content/Context;Ljava/lang/String;ZZ)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 321
    goto :goto_0

    .line 324
    :pswitch_2
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "@"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_3

    .line 328
    :cond_2
    const v2, 0x7f0802c6

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/view/EntryView;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2, v1, v0}, Lcom/sec/android/app/samsungapps/NotiDialog;->showDialog(Landroid/content/Context;Ljava/lang/String;ZZ)V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 333
    goto :goto_0

    .line 336
    :pswitch_3
    if-eqz p1, :cond_4

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v2, v3, :cond_5

    .line 338
    :cond_4
    const v2, 0x7f0802c7

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/view/EntryView;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2, v1, v0}, Lcom/sec/android/app/samsungapps/NotiDialog;->showDialog(Landroid/content/Context;Ljava/lang/String;ZZ)V

    goto :goto_0

    :cond_5
    move v0, v1

    .line 343
    goto :goto_0

    .line 346
    :pswitch_4
    if-eqz p1, :cond_6

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_7

    .line 348
    :cond_6
    const v2, 0x7f0802c5

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/view/EntryView;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2, v1, v0}, Lcom/sec/android/app/samsungapps/NotiDialog;->showDialog(Landroid/content/Context;Ljava/lang/String;ZZ)V

    goto :goto_0

    :cond_7
    move v0, v1

    .line 353
    goto :goto_0

    .line 356
    :pswitch_5
    if-eqz p1, :cond_8

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_9

    .line 358
    :cond_8
    const v2, 0x7f080249

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/view/EntryView;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2, v1, v0}, Lcom/sec/android/app/samsungapps/NotiDialog;->showDialog(Landroid/content/Context;Ljava/lang/String;ZZ)V

    goto :goto_0

    :cond_9
    move v0, v1

    .line 362
    goto :goto_0

    .line 365
    :pswitch_6
    if-eqz p1, :cond_a

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_a

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_a

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    if-le v2, v5, :cond_b

    .line 367
    :cond_a
    const v2, 0x7f08024a

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/view/EntryView;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2, v1, v0}, Lcom/sec/android/app/samsungapps/NotiDialog;->showDialog(Landroid/content/Context;Ljava/lang/String;ZZ)V

    goto/16 :goto_0

    :cond_b
    move v0, v1

    .line 372
    goto/16 :goto_0

    .line 375
    :pswitch_7
    if-eqz p1, :cond_c

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_d

    .line 377
    :cond_c
    const v2, 0x7f08025b

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/view/EntryView;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2, v1, v0}, Lcom/sec/android/app/samsungapps/NotiDialog;->showDialog(Landroid/content/Context;Ljava/lang/String;ZZ)V

    goto/16 :goto_0

    :cond_d
    move v0, v1

    .line 382
    goto/16 :goto_0

    .line 386
    :pswitch_8
    if-eqz p1, :cond_e

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v2, v5, :cond_f

    .line 388
    :cond_e
    const v2, 0x7f0802c4

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/view/EntryView;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2, v1, v0}, Lcom/sec/android/app/samsungapps/NotiDialog;->showDialog(Landroid/content/Context;Ljava/lang/String;ZZ)V

    goto/16 :goto_0

    :cond_f
    move v0, v1

    .line 393
    goto/16 :goto_0

    .line 396
    :pswitch_9
    if-eqz p1, :cond_10

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x3

    if-ge v2, v3, :cond_11

    .line 398
    :cond_10
    const v2, 0x7f0802c9

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/view/EntryView;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2, v1, v0}, Lcom/sec/android/app/samsungapps/NotiDialog;->showDialog(Landroid/content/Context;Ljava/lang/String;ZZ)V

    goto/16 :goto_0

    :cond_11
    move v0, v1

    .line 403
    goto/16 :goto_0

    .line 406
    :pswitch_a
    if-eqz p1, :cond_12

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_12

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x32

    if-le v2, v3, :cond_13

    .line 409
    :cond_12
    const v2, 0x7f0802ca

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/view/EntryView;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2, v1, v0}, Lcom/sec/android/app/samsungapps/NotiDialog;->showDialog(Landroid/content/Context;Ljava/lang/String;ZZ)V

    goto/16 :goto_0

    :cond_13
    move v0, v1

    .line 414
    goto/16 :goto_0

    .line 417
    :pswitch_b
    if-eqz p1, :cond_14

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_14

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_15

    .line 420
    :cond_14
    const v2, 0x7f0802e1

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/view/EntryView;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2, v1, v0}, Lcom/sec/android/app/samsungapps/NotiDialog;->showDialog(Landroid/content/Context;Ljava/lang/String;ZZ)V

    goto/16 :goto_0

    :cond_15
    move v0, v1

    .line 425
    goto/16 :goto_0

    .line 428
    :pswitch_c
    if-eqz p1, :cond_16

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v2, v3, :cond_17

    .line 430
    :cond_16
    invoke-virtual {p0, v4}, Lcom/sec/android/app/samsungapps/view/EntryView;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2, v1, v0}, Lcom/sec/android/app/samsungapps/NotiDialog;->showDialog(Landroid/content/Context;Ljava/lang/String;ZZ)V

    goto/16 :goto_0

    :cond_17
    move v0, v1

    .line 435
    goto/16 :goto_0

    .line 438
    :pswitch_d
    if-eqz p1, :cond_18

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x7

    if-ge v2, v3, :cond_19

    .line 440
    :cond_18
    invoke-virtual {p0, v4}, Lcom/sec/android/app/samsungapps/view/EntryView;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2, v1, v0}, Lcom/sec/android/app/samsungapps/NotiDialog;->showDialog(Landroid/content/Context;Ljava/lang/String;ZZ)V

    goto/16 :goto_0

    :cond_19
    move v0, v1

    .line 445
    goto/16 :goto_0

    :cond_1a
    move v0, v1

    .line 455
    goto/16 :goto_0

    .line 311
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_2
        :pswitch_6
        :pswitch_5
        :pswitch_7
    .end packed-switch
.end method

.method protected getEditTextString(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 71
    const-string v1, ""

    .line 75
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/view/EntryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 76
    if-nez v0, :cond_1

    .line 78
    const-string v0, "EntryView::getEditTextString::view is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move-object v0, v1

    .line 93
    :cond_0
    :goto_0
    return-object v0

    .line 82
    :cond_1
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 83
    if-nez v0, :cond_0

    .line 85
    const-string v0, "EntryView::getEditTextString::value is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move-object v0, v1

    .line 86
    goto :goto_0
.end method

.method protected getFocus()V
    .locals 0

    .prologue
    .line 511
    return-void
.end method

.method protected getHeaderView()Landroid/view/View;
    .locals 1

    .prologue
    .line 476
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getScrollView()Landroid/widget/ScrollView;
    .locals 1

    .prologue
    .line 466
    const v0, 0x7f0c0220

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/EntryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    return-object v0
.end method

.method protected getSpinnerValue(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 195
    const-string v1, ""

    .line 199
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/view/EntryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    .line 200
    if-nez v0, :cond_1

    .line 202
    const-string v0, "EntryView::getSpinnerValue::view is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move-object v0, v1

    .line 217
    :cond_0
    :goto_0
    return-object v0

    .line 206
    :cond_1
    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    .line 207
    if-nez v0, :cond_0

    .line 209
    const-string v0, "EntryView::getEditTextString::value is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move-object v0, v1

    .line 210
    goto :goto_0
.end method

.method protected getTailView()Landroid/view/View;
    .locals 1

    .prologue
    .line 471
    const v0, 0x7f0c0095

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/EntryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected getTextString(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 133
    const-string v1, ""

    .line 137
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/view/EntryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 138
    if-nez v0, :cond_1

    .line 140
    const-string v0, "EntryView::getTextString::view is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move-object v0, v1

    .line 155
    :cond_0
    :goto_0
    return-object v0

    .line 144
    :cond_1
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 145
    if-nez v0, :cond_0

    .line 147
    const-string v0, "EntryView::getTextString::value is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move-object v0, v1

    .line 148
    goto :goto_0
.end method

.method protected getaddingResourceId()I
    .locals 2

    .prologue
    .line 495
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/EntryView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 498
    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 499
    const v0, 0x7f0c0029

    .line 504
    :goto_0
    return v0

    .line 501
    :cond_0
    const v0, 0x7f0c0148

    goto :goto_0
.end method

.method protected getremovingResourceId()I
    .locals 2

    .prologue
    .line 481
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/EntryView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 484
    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 485
    const v0, 0x7f0c0148

    .line 490
    :goto_0
    return v0

    .line 487
    :cond_0
    const v0, 0x7f0c0029

    goto :goto_0
.end method

.method protected isValidEmail(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v5, -0x1

    .line 253
    .line 257
    if-eqz p1, :cond_6

    .line 259
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    .line 266
    const/4 v3, 0x7

    if-lt v2, v3, :cond_0

    const/16 v3, 0x32

    if-le v2, v3, :cond_2

    .line 268
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "EntryView::isValidEmail::invalid length, "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move v0, v1

    .line 302
    :cond_1
    :goto_0
    return v0

    .line 275
    :cond_2
    const-string v2, "."

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 276
    const-string v3, "@"

    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 277
    const-string v4, "@"

    invoke-virtual {p1, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    .line 278
    if-ne v3, v4, :cond_3

    if-eq v3, v5, :cond_3

    if-ne v2, v5, :cond_4

    .line 281
    :cond_3
    const-string v0, "EntryView::isValidEmail::invalid format"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move v0, v1

    .line 282
    goto :goto_0

    .line 288
    :cond_4
    const-string v2, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.@"

    invoke-static {v2, p1}, Lcom/sec/android/app/samsungapps/view/EntryView;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/samsungapps/view/EntryView;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 294
    :cond_5
    const-string v0, "EntryView::isValidEmail::not allowed string"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move v0, v1

    .line 295
    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method protected requestFocus()V
    .locals 0

    .prologue
    .line 517
    return-void
.end method

.method protected setEditTextString(ILjava/lang/String;)Z
    .locals 2

    .prologue
    .line 101
    const/4 v1, 0x0

    .line 105
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/view/EntryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 106
    if-nez v0, :cond_0

    .line 108
    const-string v0, "EntryView::setEditTextString::view is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->d(Ljava/lang/String;)V

    move v0, v1

    .line 125
    :goto_0
    return v0

    .line 113
    :cond_0
    if-nez p2, :cond_1

    .line 115
    const-string v0, "EntryView::setEditTextString::value is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move v0, v1

    .line 116
    goto :goto_0

    .line 119
    :cond_1
    invoke-virtual {v0, p2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 121
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected setTextString(ILjava/lang/String;)Z
    .locals 2

    .prologue
    .line 163
    const/4 v1, 0x0

    .line 167
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/view/EntryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 168
    if-nez v0, :cond_0

    .line 170
    const-string v0, "EntryView::setTextString::view is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->d(Ljava/lang/String;)V

    move v0, v1

    .line 187
    :goto_0
    return v0

    .line 175
    :cond_0
    if-nez p2, :cond_1

    .line 177
    const-string v0, "EntryView::setTextString::value is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move v0, v1

    .line 178
    goto :goto_0

    .line 181
    :cond_1
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 183
    const/4 v0, 0x1

    goto :goto_0
.end method
