.class public Lcom/sec/android/app/samsungapps/WishListActivity;
.super Lcom/sec/android/app/samsungapps/ContentListActivity;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/view/WishArrayAdapter$LoadListHandler;


# static fields
.field public static final EXTRA_BUTTONSTATE:Ljava/lang/String; = "_buttonState"

.field public static final EXTRA_LISTTYPE:Ljava/lang/String; = "_listType"

.field public static final EXTRA_TITLETEXT:Ljava/lang/String; = "_titleText"


# instance fields
.field a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

.field b:Z

.field c:Z

.field private d:Landroid/widget/CheckedTextView;

.field private e:Landroid/widget/ListView;

.field private f:Landroid/content/Context;

.field private g:Landroid/view/View;

.field private h:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 27
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ContentListActivity;-><init>()V

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->g:Landroid/view/View;

    .line 36
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->h:I

    .line 38
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->b:Z

    .line 39
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->c:Z

    return-void
.end method

.method private a()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 259
    .line 260
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->getCount()I

    move-result v3

    move v2, v0

    move v1, v0

    .line 261
    :goto_0
    if-ge v2, v3, :cond_0

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;

    .line 263
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;->isSelectedToBeDeleted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 264
    add-int/lit8 v0, v1, 0x1

    .line 261
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 267
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/WishListActivity;I)I
    .locals 0

    .prologue
    .line 27
    iput p1, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->h:I

    return p1
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/WishListActivity;Landroid/view/View;)Landroid/view/View;
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->g:Landroid/view/View;

    return-object p1
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/WishListActivity;)Landroid/widget/CheckedTextView;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->d:Landroid/widget/CheckedTextView;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/WishListActivity;)I
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/WishListActivity;->a()I

    move-result v0

    return v0
.end method

.method private b()V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v2, 0x0

    const v1, 0x7f0802f7

    const/4 v6, 0x0

    const/4 v3, 0x1

    .line 391
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->isCheckDeletionMode()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 393
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->isListItemMode()I

    move-result v0

    if-ne v0, v3, :cond_2

    .line 394
    new-array v4, v3, [I

    const v0, 0xa000e

    aput v0, v4, v6

    const v5, 0xb0002

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/WishListActivity;->setActionBarConfiguration(ILcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    .line 401
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->showOptionItem(Z)V

    .line 402
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->getSelectedWishItemCount()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/WishListActivity;->setUpPopupMenu(I)V

    .line 404
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->getSelectedWishItemCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 405
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/WishListActivity;->deletionModeActionItemEnable(Z)V

    .line 409
    :goto_1
    const v0, 0x7f0c0067

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/WishListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 410
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 411
    const v1, 0x7f0c0068

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/WishListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckedTextView;

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->d:Landroid/widget/CheckedTextView;

    .line 412
    new-instance v1, Lcom/sec/android/app/samsungapps/mc;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/mc;-><init>(Lcom/sec/android/app/samsungapps/WishListActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 441
    :cond_1
    :goto_2
    return-void

    .line 396
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->isListItemMode()I

    move-result v0

    if-ne v0, v4, :cond_0

    .line 397
    new-array v4, v3, [I

    const v0, 0xa0018

    aput v0, v4, v6

    const v5, 0xb0002

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/WishListActivity;->setActionBarConfiguration(ILcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    goto :goto_0

    .line 407
    :cond_3
    invoke-virtual {p0, v6}, Lcom/sec/android/app/samsungapps/WishListActivity;->deletionModeActionItemEnable(Z)V

    goto :goto_1

    .line 429
    :cond_4
    new-array v4, v4, [I

    fill-array-data v4, :array_0

    const v5, 0xb0001

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/WishListActivity;->setActionBarConfiguration(ILcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    .line 433
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0, v6}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->showOptionItem(Z)V

    .line 434
    const v0, 0xa0010

    invoke-virtual {p0, v0, v6}, Lcom/sec/android/app/samsungapps/WishListActivity;->enableActionItem(IZ)V

    .line 435
    const v0, 0x7f0c0067

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/WishListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 436
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 437
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    if-eqz v0, :cond_1

    .line 438
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->cancelDeletion()V

    goto :goto_2

    .line 429
    :array_0
    .array-data 4
        0xa0010
        0xa0008
    .end array-data
.end method


# virtual methods
.method public deletionModeActionItemEnable(Z)V
    .locals 2

    .prologue
    .line 383
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->isListItemMode()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 384
    const v0, 0xa000e

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/samsungapps/WishListActivity;->enableActionItem(IZ)V

    .line 388
    :cond_0
    :goto_0
    return-void

    .line 385
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->isListItemMode()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 386
    const v0, 0xa0018

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/samsungapps/WishListActivity;->enableActionItem(IZ)V

    goto :goto_0
.end method

.method public displayNormalList()V
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->cancelDeletion()V

    .line 111
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->g:Landroid/view/View;

    .line 112
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->h:I

    .line 113
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/WishListActivity;->b()V

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->d:Landroid/widget/CheckedTextView;

    if-eqz v0, :cond_1

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->d:Landroid/widget/CheckedTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 117
    :cond_1
    return-void
.end method

.method public enableActionItem(IZ)V
    .locals 1

    .prologue
    .line 444
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->enableActionItem(IZ)V

    .line 445
    return-void
.end method

.method public handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z
    .locals 2

    .prologue
    .line 463
    sget-object v0, Lcom/sec/android/app/samsungapps/md;->b:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 471
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/ContentListActivity;->handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    move-object v0, p1

    .line 465
    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;

    .line 466
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;->getAccountEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;->LogedOut:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    if-ne v0, v1, :cond_0

    .line 467
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/WishListActivity;->finish()V

    .line 468
    const/4 v0, 0x0

    goto :goto_0

    .line 463
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public handleUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 343
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getEventType()Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;->ContentDisplayEvent:Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    if-ne v0, v1, :cond_0

    .line 344
    sget-object v0, Lcom/sec/android/app/samsungapps/md;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getContentDisplayEvent()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->getContentDisplayEventType()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 363
    :cond_0
    :goto_0
    return-void

    .line 347
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->isCheckDeletionMode()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 350
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getContentDisplayEvent()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;

    .line 351
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->getCount()I

    move-result v5

    move v4, v3

    .line 352
    :goto_1
    if-ge v4, v5, :cond_3

    .line 353
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;

    .line 355
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 356
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;->isSelectedToBeDeleted()Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x1

    :goto_2
    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;->selDeleteSelection(Z)V

    .line 352
    :cond_1
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    :cond_2
    move v2, v3

    .line 356
    goto :goto_2

    .line 359
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 361
    :cond_4
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getContentDisplayEvent()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    const/16 v1, 0x8

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;I)V

    goto :goto_0

    .line 344
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public isCheckAll()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 318
    const/4 v2, 0x1

    .line 319
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->getCount()I

    move-result v4

    move v3, v1

    .line 320
    :goto_0
    if-ge v3, v4, :cond_1

    .line 321
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;

    .line 322
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;->isSelectedToBeDeleted()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 327
    :goto_1
    return v0

    .line 320
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method public isCheckAllButtonChecked()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->d:Landroid/widget/CheckedTextView;

    if-nez v0, :cond_0

    .line 247
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 249
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->d:Landroid/widget/CheckedTextView;

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method public isSelectAll()Z
    .locals 1

    .prologue
    .line 519
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->d:Landroid/widget/CheckedTextView;

    if-nez v0, :cond_0

    .line 520
    const/4 v0, 0x0

    .line 522
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->d:Landroid/widget/CheckedTextView;

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->isSelected()Z

    move-result v0

    goto :goto_0
.end method

.method public isSelectedItem()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 301
    .line 302
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->getCount()I

    move-result v3

    move v2, v1

    .line 303
    :goto_0
    if-ge v2, v3, :cond_1

    .line 304
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;

    .line 305
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;->isSelectedToBeDeleted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 306
    const/4 v0, 0x1

    .line 310
    :goto_1
    return v0

    .line 303
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public loadNormalList()V
    .locals 0

    .prologue
    .line 373
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/WishListActivity;->displayNormalList()V

    .line 374
    return-void
.end method

.method public onActionItemActionBar(ILandroid/view/View;)V
    .locals 6

    .prologue
    const v5, 0x7f0802a1

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 152
    sparse-switch p1, :sswitch_data_0

    .line 181
    :goto_0
    return-void

    .line 154
    :sswitch_0
    invoke-static {p0, v3}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;I)V

    goto :goto_0

    .line 159
    :sswitch_1
    const/4 v0, 0x3

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;I)V

    goto :goto_0

    .line 164
    :sswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/WishListActivity;->isSelectedItem()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 165
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/WishListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/WishListActivity;->a()I

    move-result v0

    if-ne v0, v3, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/WishListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f080243

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {p0, v1, v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->createInfoDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/WishListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08023d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/ma;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/app/samsungapps/ma;-><init>(Lcom/sec/android/app/samsungapps/WishListActivity;Lcom/sec/android/app/samsungapps/CustomDialogBuilder;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setNegativeButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/WishListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/mb;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/mb;-><init>(Lcom/sec/android/app/samsungapps/WishListActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setPositiveButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/WishListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f080242

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v2, v3, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/WishListActivity;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 167
    :cond_1
    const v0, 0x7f080287

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/WishListActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/uiutil/ToastUtil;->toastMessageShortTime(Landroid/content/Context;Ljava/lang/String;)Landroid/widget/Toast;

    goto/16 :goto_0

    .line 171
    :sswitch_3
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/WishListActivity;->displayNormalList()V

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->showOptionItem(Z)V

    .line 173
    invoke-virtual {p0, v4}, Lcom/sec/android/app/samsungapps/WishListActivity;->setSelectAll(Z)V

    goto/16 :goto_0

    .line 177
    :sswitch_4
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/WishListActivity;->onPositive(I)V

    goto/16 :goto_0

    .line 152
    :sswitch_data_0
    .sparse-switch
        0xa0005 -> :sswitch_0
        0xa0007 -> :sswitch_1
        0xa000a -> :sswitch_3
        0xa000e -> :sswitch_2
        0xa0010 -> :sswitch_4
        0xa0018 -> :sswitch_2
    .end sparse-switch
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->isCheckDeletionMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    .line 128
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->setSelectedWishItemCount(I)V

    .line 129
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/WishListActivity;->displayNormalList()V

    .line 130
    const v1, 0xa0010

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/samsungapps/WishListActivity;->enableActionItem(IZ)V

    .line 134
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 127
    goto :goto_0

    .line 132
    :cond_1
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ContentListActivity;->onBackPressed()V

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 43
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ContentListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 44
    iput-object p0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->f:Landroid/content/Context;

    .line 46
    const v0, 0x7f040052

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/WishListActivity;->setMainView(I)V

    .line 47
    const v0, 0x7f0c0078

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/WishListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->e:Landroid/widget/ListView;

    .line 48
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->e:Landroid/widget/ListView;

    instance-of v0, v0, Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->e:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->e:Landroid/widget/ListView;

    new-instance v1, Lcom/sec/android/app/samsungapps/lz;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/lz;-><init>(Lcom/sec/android/app/samsungapps/WishListActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 68
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/WishListActivity;->b()V

    .line 69
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    if-eqz v0, :cond_0

    .line 333
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->cancelDeletion()V

    .line 334
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->release()V

    .line 337
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ContentListActivity;->onDestroy()V

    .line 338
    return-void
.end method

.method public onNavigationActionBar(ILandroid/view/View;)V
    .locals 1

    .prologue
    .line 138
    packed-switch p1, :pswitch_data_0

    .line 146
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/ContentListActivity;->onNavigationActionBar(ILandroid/view/View;)V

    .line 147
    :goto_0
    return-void

    .line 140
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->isCheckDeletionMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/WishListActivity;->onBackPressed()V

    goto :goto_0

    .line 138
    nop

    :pswitch_data_0
    .packed-switch 0xa0000
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 121
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ContentListActivity;->onPause()V

    .line 122
    return-void
.end method

.method public onPositive(I)V
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->enterDeletionMode(I)V

    .line 101
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/WishListActivity;->b()V

    .line 102
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const v3, 0xa0010

    const/4 v2, 0x1

    .line 73
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ContentListActivity;->onResume()V

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->isCheckDeletionMode()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->getCount()I

    move-result v0

    if-eqz v0, :cond_1

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->isListItemMode()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->enterDeletionMode(I)V

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->isListItemMode()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->enterDeletionMode(I)V

    .line 93
    :goto_0
    return-void

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->enterDeletionMode(I)V

    goto :goto_0

    .line 82
    :cond_1
    new-instance v0, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->createWishList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/WishListActivity;->setLogicalView(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V

    .line 84
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/WishListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0801d0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/WishListActivity;->setEmptyText(Ljava/lang/String;)V

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->setLoadListHandler(Lcom/sec/android/app/samsungapps/view/WishArrayAdapter$LoadListHandler;)V

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 88
    const/4 v0, 0x0

    invoke-virtual {p0, v3, v0}, Lcom/sec/android/app/samsungapps/WishListActivity;->enableActionItem(IZ)V

    goto :goto_0

    .line 90
    :cond_2
    invoke-virtual {p0, v3, v2}, Lcom/sec/android/app/samsungapps/WishListActivity;->enableActionItem(IZ)V

    goto :goto_0
.end method

.method public onSamsungAppsOpenOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;)Z
    .locals 1

    .prologue
    .line 449
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->isCheckDeletionMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 450
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ContentListActivity;->onSamsungAppsOpenOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;)Z

    .line 451
    const v0, 0x7f0c000b

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->removeItem(I)Z

    .line 452
    const/high16 v0, 0x7f0c0000

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->removeItem(I)Z

    .line 453
    const/4 v0, 0x1

    .line 456
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public refreshBeforeSelectedItem()V
    .locals 2

    .prologue
    .line 505
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->isCheckDeletionMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 507
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->g:Landroid/view/View;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->h:I

    if-ltz v0, :cond_0

    .line 508
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    iget v1, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->h:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;

    .line 509
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;->isSelectedToBeDeleted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 510
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->g:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 516
    :cond_0
    :goto_0
    return-void

    .line 512
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->g:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    goto :goto_0
.end method

.method public setBeforeSelectedItem(Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 500
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->g:Landroid/view/View;

    .line 501
    iput p2, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->h:I

    .line 502
    return-void
.end method

.method public setCheckAllLayoutChecked(Z)V
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->d:Landroid/widget/CheckedTextView;

    if-eqz v0, :cond_0

    .line 378
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->d:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, p1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 380
    :cond_0
    return-void
.end method

.method public setCheckedAll()V
    .locals 1

    .prologue
    .line 527
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/WishListActivity;->isCheckAll()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/WishListActivity;->setCheckAllLayoutChecked(Z)V

    .line 528
    return-void
.end method

.method public setSelectAll(Z)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 223
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    if-nez v0, :cond_0

    .line 238
    :goto_0
    return-void

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->getCount()I

    move-result v3

    move v1, v2

    .line 228
    :goto_1
    if-ge v1, v3, :cond_1

    .line 229
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;

    .line 230
    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;->selDeleteSelection(Z)V

    .line 228
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 232
    :cond_1
    if-eqz p1, :cond_2

    .line 233
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->setSelectedWishItemCount(I)V

    .line 237
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 235
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->a:Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/view/WishArrayAdapter;->setSelectedWishItemCount(I)V

    goto :goto_2
.end method

.method public setUpPopupMenu(I)V
    .locals 5

    .prologue
    .line 480
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    if-eqz v0, :cond_0

    .line 481
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->f:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08010e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->setSelectedText(Ljava/lang/String;)V

    .line 483
    :cond_0
    return-void
.end method

.method public updateItemClick(I)V
    .locals 1

    .prologue
    .line 496
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/samsungapps/WishListActivity;->updatePopupMenuItemsVisibility(ZI)V

    .line 497
    return-void
.end method

.method public updatePopupMenuItemsVisibility(ZI)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 486
    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->b:Z

    .line 488
    if-lez p2, :cond_1

    .line 489
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->c:Z

    .line 493
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 486
    goto :goto_0

    .line 491
    :cond_1
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/WishListActivity;->c:Z

    goto :goto_1
.end method
