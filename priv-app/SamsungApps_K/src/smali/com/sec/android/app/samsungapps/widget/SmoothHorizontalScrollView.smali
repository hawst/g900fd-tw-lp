.class public Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;
.super Landroid/widget/HorizontalScrollView;
.source "ProGuard"


# instance fields
.field private a:Z

.field private b:F

.field private c:F

.field private d:F

.field private e:F

.field private f:F

.field protected stickyPositions:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 20
    invoke-direct {p0, p1}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;)V

    .line 11
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->stickyPositions:Ljava/util/ArrayList;

    .line 12
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->a:Z

    .line 17
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->f:F

    .line 21
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->setHorizontalFadingEdgeEnabled(Z)V

    .line 22
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->setHorizontalScrollBarEnabled(Z)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 26
    invoke-direct {p0, p1, p2}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 11
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->stickyPositions:Ljava/util/ArrayList;

    .line 12
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->a:Z

    .line 17
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->f:F

    .line 27
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->setHorizontalFadingEdgeEnabled(Z)V

    .line 28
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->setHorizontalScrollBarEnabled(Z)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 32
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 11
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->stickyPositions:Ljava/util/ArrayList;

    .line 12
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->a:Z

    .line 17
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->f:F

    .line 33
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->setHorizontalFadingEdgeEnabled(Z)V

    .line 34
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->setHorizontalScrollBarEnabled(Z)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZZ)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0, p1}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;)V

    .line 11
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->stickyPositions:Ljava/util/ArrayList;

    .line 12
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->a:Z

    .line 17
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->f:F

    .line 39
    invoke-virtual {p0, p2}, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->setHorizontalFadingEdgeEnabled(Z)V

    .line 40
    invoke-virtual {p0, p3}, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->setHorizontalScrollBarEnabled(Z)V

    .line 41
    return-void
.end method


# virtual methods
.method public addStickyPosition(I)V
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->stickyPositions:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->stickyPositions:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 129
    return-void
.end method

.method public clearStickyPositions()V
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->stickyPositions:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 133
    return-void
.end method

.method public fling(I)V
    .locals 4

    .prologue
    .line 45
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->a:Z

    .line 47
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/16 v1, 0x12c

    if-le v0, v1, :cond_5

    .line 48
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->f:F

    div-int/lit8 v1, p1, 0x3

    int-to-float v1, v1

    add-float/2addr v0, v1

    float-to-int v1, v0

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->stickyPositions:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->stickyPositions:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->stickyPositions:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 52
    if-lez p1, :cond_3

    .line 53
    if-le v1, v2, :cond_2

    .line 54
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->getScrollY()I

    move-result v0

    invoke-virtual {p0, v2, v0}, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->smoothScrollTo(II)V

    .line 73
    :cond_1
    :goto_0
    return-void

    .line 56
    :cond_2
    if-le v0, v1, :cond_0

    .line 57
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->getScrollY()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->smoothScrollTo(II)V

    goto :goto_0

    .line 60
    :cond_3
    if-gez p1, :cond_0

    .line 61
    if-gez v1, :cond_4

    .line 62
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->getScrollY()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->smoothScrollTo(II)V

    goto :goto_0

    .line 64
    :cond_4
    if-le v0, v1, :cond_0

    .line 65
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->getScrollY()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->smoothScrollTo(II)V

    goto :goto_0

    .line 71
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->holdScrollPosition()V

    goto :goto_0
.end method

.method public holdScrollPosition()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 113
    .line 116
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->stickyPositions:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v2, v1

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 117
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->getScrollX()I

    move-result v4

    sub-int/2addr v4, v0

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    .line 118
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-le v5, v4, :cond_2

    .line 119
    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 120
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :goto_1
    move-object v2, v1

    move-object v1, v0

    .line 122
    goto :goto_0

    .line 123
    :cond_1
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->getScrollY()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->smoothScrollTo(II)V

    .line 124
    return-void

    :cond_2
    move-object v0, v1

    move-object v1, v2

    goto :goto_1
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 90
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 109
    :cond_0
    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Landroid/widget/HorizontalScrollView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_1
    return v0

    .line 92
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->getScrollX()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->f:F

    .line 93
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->b:F

    .line 94
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->c:F

    .line 95
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->d:F

    .line 96
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->e:F

    goto :goto_0

    .line 99
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 100
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 101
    iget v2, p0, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->b:F

    iget v3, p0, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->d:F

    sub-float v3, v0, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    add-float/2addr v2, v3

    iput v2, p0, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->b:F

    .line 102
    iget v2, p0, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->c:F

    iget v3, p0, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->e:F

    sub-float v3, v1, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    add-float/2addr v2, v3

    iput v2, p0, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->c:F

    .line 103
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->d:F

    .line 104
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->e:F

    .line 105
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->b:F

    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->c:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 106
    const/4 v0, 0x0

    goto :goto_1

    .line 90
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 78
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->a:Z

    .line 80
    invoke-super {p0, p1}, Landroid/widget/HorizontalScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 82
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->a:Z

    if-nez v1, :cond_1

    .line 83
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;->holdScrollPosition()V

    .line 85
    :cond_1
    return v0
.end method
