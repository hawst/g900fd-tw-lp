.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/TermInformationParser;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;
.source "ProGuard"


# instance fields
.field mDisclaimer:Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;

.field mHelpInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/TermInformationParser;->mDisclaimer:Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;

    .line 12
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/TermInformationParser;->mHelpInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo;

    .line 16
    return-void
.end method


# virtual methods
.method protected onCreateObject(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/TermInformationParser;->mHelpInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo;

    if-eqz v0, :cond_1

    .line 28
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/TermInformationParser;->mHelpInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/HelpInfo;

    invoke-virtual {p1, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->mappingClass(Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 35
    :cond_0
    :goto_0
    return-void

    .line 31
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/TermInformationParser;->mDisclaimer:Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;

    if-eqz v0, :cond_0

    .line 33
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/TermInformationParser;->mDisclaimer:Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;

    invoke-virtual {p1, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->mappingClass(Ljava/lang/Class;Ljava/lang/Object;Z)Z

    goto :goto_0
.end method

.method protected onPostParseError()V
    .locals 0

    .prologue
    .line 21
    return-void
.end method

.method protected onPostParseResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 41
    return-void
.end method
