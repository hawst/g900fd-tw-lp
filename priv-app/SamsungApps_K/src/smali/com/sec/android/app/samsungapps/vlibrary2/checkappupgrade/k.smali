.class final Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/k;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateObserver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/k;->a:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDLStateChanged(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V
    .locals 4

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/k;->a:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;->_IODCUpdateView:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand$IODCUpdateView;

    if-nez v0, :cond_1

    .line 106
    const-string v0, "ODCManualUpdateCommand::onNotifyProgress::Update View is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 107
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    const-string v1, "com.sec.android.app.samsungapps"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/k;->a:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;->mDownloadObserver:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateObserver;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->removeDLStateObserver(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateObserver;)Z

    .line 153
    :cond_0
    :goto_0
    return-void

    .line 111
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getGUID()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.app.samsungapps"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    .line 113
    const-string v0, "ODCManualUpdateCommand::onNotifyProgress::Wrong package name"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 114
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    const-string v1, "com.sec.android.app.samsungapps"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/k;->a:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;->mDownloadObserver:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateObserver;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->removeDLStateObserver(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateObserver;)Z

    goto :goto_0

    .line 118
    :cond_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/l;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getState()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 121
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/k;->a:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;->_IODCUpdateView:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand$IODCUpdateView;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getDownloadedSize()I

    move-result v1

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getTotalSize()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand$IODCUpdateView;->notifyProgress(II)V

    goto :goto_0

    .line 125
    :pswitch_1
    const-string v0, "ODCManualUpdateCommand::onDLStateChanged::Download failed"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 126
    const-string v1, "0"

    .line 133
    :try_start_0
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getTagStr()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 140
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/k;->a:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;->_IODCUpdateView:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand$IODCUpdateView;

    if-eqz v1, :cond_0

    .line 142
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/k;->a:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;->_IODCUpdateView:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand$IODCUpdateView;

    invoke-interface {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand$IODCUpdateView;->notifyInstallFailed(Ljava/lang/String;)V

    goto :goto_0

    .line 135
    :catch_0
    move-exception v0

    .line 137
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ODCManualUpdateCommand::onDLStateChanged::"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_1

    .line 148
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/k;->a:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;->_IODCUpdateView:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand$IODCUpdateView;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCUpdateCommand$IODCUpdateView;->notifyInstalling()V

    .line 149
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    const-string v1, "com.sec.android.app.samsungapps"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/k;->a:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ODCManualUpdateCommand;->mDownloadObserver:Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateObserver;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->removeDLStateObserver(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateObserver;)Z

    .line 150
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->exitSamsungApps()V

    goto/16 :goto_0

    .line 118
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
