.class public final Lcom/sec/android/app/samsungapps/Constant;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field public static final ACTIONBAR_DEFAULT_SIZE:I = 0x13

.field public static final DEFAULT_GRID_MAX_ITEM_COUNT:I = 0x1e

.field public static final DEFAULT_LIST_MAX_ITEM_COUNT:I = 0xf

.field public static final DEFAULT_MAIN_GRID_MAX_ITEM_COUNT:I = 0x18

.field public static final ICECREAMSANDWICH_OS_VERSION:I = 0xe

.field public static final JELLYBEAN_OS_VERSION:I = 0x10

.field public static final KITKAT_OS_VERSION:I = 0x13

.field public static final SAMSUNG_INFO:Ljava/lang/String; = "\uc0ac\uc5c5\uc790\uc815\ubcf4\ud655\uc778"

.field public static final STARTERKIT_FILE_NAME:Ljava/lang/String; = "SamsungAppsStarterKitFile.txt"

.field public static final STARTERKIT_IMAGE_URL:Ljava/lang/String; = ""

.field public static final TAB_SELECTED_SHADOW_TEXT_COLOR:I = 0x51000000

.field public static final TAB_SELECTED_TEXT_COLOR:I = -0xf94627

.field public static final TAB_TEXT_COLOR:I = -0xa0a0b


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    .line 8
    return-void
.end method
