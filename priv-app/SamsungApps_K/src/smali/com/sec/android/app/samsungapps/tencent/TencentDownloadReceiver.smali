.class public Lcom/sec/android/app/samsungapps/tencent/TencentDownloadReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ProGuard"


# instance fields
.field a:Landroid/os/Bundle;

.field b:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method final a()V
    .locals 7

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tencent/TencentDownloadReceiver;->a:Landroid/os/Bundle;

    if-nez v0, :cond_1

    .line 100
    :cond_0
    :goto_0
    return-void

    .line 76
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tencent/TencentDownloadReceiver;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/TencentUtil;->isAvailableTencent(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tencent/TencentDownloadReceiver;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/TencentUtil;->isTencentValid(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tencent/TencentDownloadReceiver;->a:Landroid/os/Bundle;

    const-string v1, "PackageName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 81
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tencent/TencentDownloadReceiver;->a:Landroid/os/Bundle;

    const-string v2, "AppName"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 82
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/tencent/TencentDownloadReceiver;->a:Landroid/os/Bundle;

    const-string v3, "ProductID"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 83
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/tencent/TencentDownloadReceiver;->a:Landroid/os/Bundle;

    const-string v4, "VersionCode"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 84
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/tencent/TencentDownloadReceiver;->a:Landroid/os/Bundle;

    const-string v5, "TotalSize"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 85
    iget-object v5, p0, Lcom/sec/android/app/samsungapps/tencent/TencentDownloadReceiver;->a:Landroid/os/Bundle;

    const-string v6, "IconUrl"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 87
    new-instance v5, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    .line 88
    const-string v6, "GUID"

    invoke-virtual {v5, v6, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 89
    const-string v0, "productName"

    invoke-virtual {v5, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 90
    const-string v0, "productID"

    invoke-virtual {v5, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 91
    const-string v0, "versionCode"

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 92
    const-string v0, "contentsSize"

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 93
    const-string v0, "linkProductStore"

    const-string v1, "Tencent"

    invoke-virtual {v5, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 94
    const-string v0, "sellingPrice"

    const-string v1, "0"

    invoke-virtual {v5, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 95
    const-string v0, "reducePrice"

    const-string v1, "0"

    invoke-virtual {v5, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 97
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;

    invoke-direct {v0, v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 98
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/LogSender;->sendDownloadClickLog(Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;)V

    goto/16 :goto_0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 26
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/tencent/TencentDownloadReceiver;->b:Landroid/content/Context;

    .line 27
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    .line 29
    if-nez p2, :cond_1

    .line 48
    :cond_0
    :goto_0
    return-void

    .line 33
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Tencent Intent Received : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 35
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/tencent/TencentDownloadReceiver;->a:Landroid/os/Bundle;

    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tencent/TencentDownloadReceiver;->a:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 40
    invoke-static {}, Lcom/sec/android/app/samsungapps/initializer/Global;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tencent/TencentDownloadReceiver;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/tencent/TencentDownloadReceiver;->b:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;)V

    new-instance v0, Lcom/sec/android/app/samsungapps/tencent/a;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/tencent/a;-><init>(Lcom/sec/android/app/samsungapps/tencent/TencentDownloadReceiver;)V

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManager$InitializeManagerResultListener;)V

    goto :goto_0

    .line 46
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/tencent/TencentDownloadReceiver;->a()V

    goto :goto_0
.end method
