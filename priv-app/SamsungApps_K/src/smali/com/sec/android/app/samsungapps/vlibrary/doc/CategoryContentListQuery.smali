.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;
.super Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;
.source "ProGuard"


# instance fields
.field _PaidTypeMap:Ljava/util/HashMap;

.field protected mCurCategoryIDGetter:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryIDGetter;


# direct methods
.method protected constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryIDGetter;)V
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->Category:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)V

    .line 8
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;->mCurCategoryIDGetter:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryIDGetter;

    .line 60
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;->_PaidTypeMap:Ljava/util/HashMap;

    .line 12
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;->mCurCategoryIDGetter:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryIDGetter;

    .line 13
    return-void
.end method


# virtual methods
.method public clearSortOrder()V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;->_PaidTypeMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 80
    return-void
.end method

.method public getCategoryGetter()Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryIDGetter;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;->mCurCategoryIDGetter:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryIDGetter;

    return-object v0
.end method

.method public getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 27
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;->mCurCategoryIDGetter:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryIDGetter;

    if-nez v1, :cond_1

    .line 35
    :cond_0
    :goto_0
    return-object v0

    .line 30
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;->mCurCategoryIDGetter:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryIDGetter;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryIDGetter;->getSelectedCategory()Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;->mCurCategoryIDGetter:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryIDGetter;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryIDGetter;->getSelectedCategory()Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    move-result-object v0

    .line 35
    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryID:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;->getContentListByCategoryID(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v0

    goto :goto_0
.end method

.method public getContentList(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;
    .locals 1

    .prologue
    .line 54
    if-nez p1, :cond_0

    .line 55
    const/4 v0, 0x0

    .line 57
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;->getContentListByCategoryID(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v0

    goto :goto_0
.end method

.method protected getContentListByCategoryID(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;
    .locals 4

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;->mContentListArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/subclass/ContentListWithID;

    .line 42
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;->getSortOrder()Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;->getno()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;->getPaidTypeFilter()I

    move-result v3

    invoke-virtual {v0, p1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/subclass/ContentListWithID;->compareID(Ljava/lang/String;II)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 44
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/subclass/ContentListWithID;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v0

    .line 49
    :goto_0
    return-object v0

    .line 47
    :cond_1
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/subclass/ContentListWithID;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;->getSortOrder()Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;->getno()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;->getPaidTypeFilter()I

    move-result v3

    invoke-direct {v0, v1, p1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/subclass/ContentListWithID;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;Ljava/lang/String;II)V

    .line 48
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;->mContentListArray:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 49
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/subclass/ContentListWithID;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v0

    goto :goto_0
.end method

.method public getSortOrder()Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;->_PaidTypeMap:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;->getPaidTypeFilter()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    .line 65
    if-nez v0, :cond_0

    .line 66
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;->bestselling:Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder$SortMethod;)V

    .line 68
    :cond_0
    return-object v0
.end method

.method public setCurCategoryGetter(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryIDGetter;)V
    .locals 0

    .prologue
    .line 17
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;->mCurCategoryIDGetter:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryIDGetter;

    .line 18
    return-void
.end method

.method public setSortOrder(Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;)V
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;->_PaidTypeMap:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContentListQuery;->getPaidTypeFilter()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    return-void
.end method
