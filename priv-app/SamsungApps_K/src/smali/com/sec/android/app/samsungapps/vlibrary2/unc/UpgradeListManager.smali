.class public Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private _Context:Landroid/content/Context;

.field private _IListViewState:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;

.field private _ItemCount:I

.field _List:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;

.field _UpgradeListResult:Lcom/sec/android/app/samsungapps/vlibrary2/unc/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;I)V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->_Context:Landroid/content/Context;

    .line 28
    iput p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->_ItemCount:I

    .line 29
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->_IListViewState:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;

    .line 30
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->_ItemCount:I

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->_List:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;

    .line 31
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;)Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->_IListViewState:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->setUncList()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;)I
    .locals 1

    .prologue
    .line 18
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->_ItemCount:I

    return v0
.end method

.method private request(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V
    .locals 6

    .prologue
    .line 108
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/b;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/unc/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->_UpgradeListResult:Lcom/sec/android/app/samsungapps/vlibrary2/unc/b;

    .line 109
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->_Context:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->_UpgradeListResult:Lcom/sec/android/app/samsungapps/vlibrary2/unc/b;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->_List:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->getNextStartNumber()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->_List:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->getNextEndNumber()I

    move-result v4

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->upgradeListEx(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;IILcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 112
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 113
    return-void
.end method

.method private requestUncList()V
    .locals 1

    .prologue
    .line 80
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/a;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/unc/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;)V

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->request(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    .line 104
    return-void
.end method

.method private setUncList()V
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->_List:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;

    if-nez v0, :cond_0

    .line 119
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->_ItemCount:I

    if-lez v0, :cond_1

    .line 121
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->_ItemCount:I

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->_List:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;

    .line 129
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->_List:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->_UpgradeListResult:Lcom/sec/android/app/samsungapps/vlibrary2/unc/b;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/unc/b;->a()Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->append(Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;)V

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->_UpgradeListResult:Lcom/sec/android/app/samsungapps/vlibrary2/unc/b;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/unc/b;->clearContainer()V

    .line 131
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->_UpgradeListResult:Lcom/sec/android/app/samsungapps/vlibrary2/unc/b;

    .line 132
    return-void

    .line 125
    :cond_1
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;

    const/16 v1, 0xf

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->_List:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;

    goto :goto_0
.end method


# virtual methods
.method public getUncList()Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->_List:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;

    return-object v0
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 63
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->_IListViewState:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;

    .line 64
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->_Context:Landroid/content/Context;

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->_List:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->_List:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->clear()V

    .line 69
    :cond_0
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->_List:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->_UpgradeListResult:Lcom/sec/android/app/samsungapps/vlibrary2/unc/b;

    if-eqz v0, :cond_1

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->_UpgradeListResult:Lcom/sec/android/app/samsungapps/vlibrary2/unc/b;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/unc/b;->clearContainer()V

    .line 75
    :cond_1
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->_UpgradeListResult:Lcom/sec/android/app/samsungapps/vlibrary2/unc/b;

    .line 76
    return-void
.end method

.method public requestMore()V
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->_IListViewState:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;->STATE_LOADINGMORE:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;->setState(Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;)V

    .line 53
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->requestUncList()V

    .line 54
    return-void
.end method

.method public setIListViewStateManager(Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;)V
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->_IListViewState:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;

    .line 36
    return-void
.end method

.method public start()V
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->_IListViewState:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;->STATE_LOADING:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;->setState(Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;)V

    .line 44
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->requestUncList()V

    .line 45
    return-void
.end method
