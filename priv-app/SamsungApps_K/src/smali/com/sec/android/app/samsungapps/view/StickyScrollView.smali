.class public Lcom/sec/android/app/samsungapps/view/StickyScrollView;
.super Landroid/widget/ScrollView;
.source "ProGuard"


# static fields
.field public static final FLAG_HASTRANSPARANCY:Ljava/lang/String; = "-hastransparancy"

.field public static final FLAG_NONCONSTANT:Ljava/lang/String; = "-nonconstant"

.field public static final STICKY_TAG:Ljava/lang/String; = "sticky"


# instance fields
.field private a:Ljava/util/ArrayList;

.field private b:Landroid/view/View;

.field private c:F

.field private d:Z

.field private e:Z

.field private f:Z

.field private final g:Ljava/lang/Runnable;

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 64
    const v0, 0x1010080

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 65
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    new-instance v0, Lcom/sec/android/app/samsungapps/view/bc;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/view/bc;-><init>(Lcom/sec/android/app/samsungapps/view/StickyScrollView;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->g:Ljava/lang/Runnable;

    .line 199
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->h:Z

    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->setup()V

    .line 70
    return-void
.end method

.method private a(Landroid/view/View;)I
    .locals 3

    .prologue
    .line 77
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    move v1, v0

    .line 78
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eq v0, v2, :cond_0

    .line 79
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 80
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v2

    add-int/2addr v1, v2

    move-object p1, v0

    goto :goto_0

    .line 82
    :cond_0
    return v1
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/view/StickyScrollView;Landroid/view/View;)I
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->a(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/view/StickyScrollView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->b:Landroid/view/View;

    return-object v0
.end method

.method private a()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 232
    .line 234
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v2, v1

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 235
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->b(Landroid/view/View;)I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->getScrollY()I

    move-result v6

    sub-int v6, v4, v6

    iget-boolean v4, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->e:Z

    if-eqz v4, :cond_1

    move v4, v3

    :goto_1
    add-int/2addr v6, v4

    .line 236
    if-gtz v6, :cond_3

    .line 237
    if-eqz v2, :cond_0

    invoke-direct {p0, v2}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->b(Landroid/view/View;)I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->getScrollY()I

    move-result v7

    sub-int v7, v4, v7

    iget-boolean v4, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->e:Z

    if-eqz v4, :cond_2

    move v4, v3

    :goto_2
    add-int/2addr v4, v7

    if-le v6, v4, :cond_d

    :cond_0
    move-object v2, v0

    .line 238
    goto :goto_0

    .line 235
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->getPaddingTop()I

    move-result v4

    goto :goto_1

    .line 237
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->getPaddingTop()I

    move-result v4

    goto :goto_2

    .line 241
    :cond_3
    if-eqz v1, :cond_4

    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->b(Landroid/view/View;)I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->getScrollY()I

    move-result v7

    sub-int v7, v4, v7

    iget-boolean v4, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->e:Z

    if-eqz v4, :cond_5

    move v4, v3

    :goto_3
    add-int/2addr v4, v7

    if-ge v6, v4, :cond_d

    :cond_4
    :goto_4
    move-object v1, v0

    .line 245
    goto :goto_0

    .line 241
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->getPaddingTop()I

    move-result v4

    goto :goto_3

    .line 246
    :cond_6
    if-eqz v2, :cond_c

    .line 247
    if-nez v1, :cond_a

    const/4 v0, 0x0

    :goto_5
    iput v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->c:F

    .line 248
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->b:Landroid/view/View;

    if-eq v2, v0, :cond_9

    .line 249
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->b:Landroid/view/View;

    if-eqz v0, :cond_7

    .line 250
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->b()V

    .line 252
    :cond_7
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->b:Landroid/view/View;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "-hastransparancy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->b:Landroid/view/View;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->e(Landroid/view/View;)V

    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "-nonconstant"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->g:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->post(Ljava/lang/Runnable;)Z

    .line 257
    :cond_9
    :goto_6
    return-void

    .line 247
    :cond_a
    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->b(Landroid/view/View;)I

    move-result v0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->getScrollY()I

    move-result v1

    sub-int v1, v0, v1

    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->e:Z

    if-eqz v0, :cond_b

    move v0, v3

    :goto_7
    add-int/2addr v0, v1

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-float v0, v0

    goto :goto_5

    :cond_b
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->getPaddingTop()I

    move-result v0

    goto :goto_7

    .line 254
    :cond_c
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->b:Landroid/view/View;

    if-eqz v0, :cond_9

    .line 255
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->b()V

    goto :goto_6

    :cond_d
    move-object v0, v1

    goto :goto_4
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/view/StickyScrollView;)F
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->c:F

    return v0
.end method

.method private b(Landroid/view/View;)I
    .locals 3

    .prologue
    .line 86
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v0

    move v1, v0

    .line 87
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eq v0, v2, :cond_0

    .line 88
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 89
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v2

    add-int/2addr v1, v2

    move-object p1, v0

    goto :goto_0

    .line 91
    :cond_0
    return v1
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/view/StickyScrollView;Landroid/view/View;)I
    .locals 3

    .prologue
    .line 20
    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eq v0, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v2

    add-int/2addr v1, v2

    move-object p1, v0

    goto :goto_0

    :cond_0
    return v1
.end method

.method private b()V
    .locals 2

    .prologue
    .line 270
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "-hastransparancy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 271
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->b:Landroid/view/View;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->f(Landroid/view/View;)V

    .line 273
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->b:Landroid/view/View;

    .line 274
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->g:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 275
    return-void
.end method

.method private c(Landroid/view/View;)I
    .locals 3

    .prologue
    .line 95
    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v0

    move v1, v0

    .line 96
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eq v0, v2, :cond_0

    .line 97
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 98
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v2

    add-int/2addr v1, v2

    move-object p1, v0

    goto :goto_0

    .line 100
    :cond_0
    return v1
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/view/StickyScrollView;Landroid/view/View;)I
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->c(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method private c()V
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->b:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 286
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->b()V

    .line 288
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 289
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->d(Landroid/view/View;)V

    .line 290
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->a()V

    .line 291
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->invalidate()V

    .line 292
    return-void
.end method

.method private d(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 295
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    .line 296
    check-cast p1, Landroid/view/ViewGroup;

    .line 297
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 298
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 299
    if-eqz v1, :cond_1

    const-string v2, "sticky"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 300
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->a:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 297
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 301
    :cond_1
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    instance-of v1, v1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    .line 302
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->d(Landroid/view/View;)V

    goto :goto_1

    .line 306
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 307
    if-eqz v0, :cond_3

    const-string v1, "sticky"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 308
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 311
    :cond_3
    return-void
.end method

.method private static e(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 319
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 320
    invoke-virtual {p0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 327
    :goto_0
    return-void

    .line 322
    :cond_0
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 323
    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 324
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 325
    invoke-virtual {p0, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method private static f(Landroid/view/View;)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 330
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 331
    invoke-virtual {p0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 338
    :goto_0
    return-void

    .line 333
    :cond_0
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 334
    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 335
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 336
    invoke-virtual {p0, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method


# virtual methods
.method public addView(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 130
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->addView(Landroid/view/View;)V

    .line 131
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->d(Landroid/view/View;)V

    .line 132
    return-void
.end method

.method public addView(Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 136
    invoke-super {p0, p1, p2}, Landroid/widget/ScrollView;->addView(Landroid/view/View;I)V

    .line 137
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->d(Landroid/view/View;)V

    .line 138
    return-void
.end method

.method public addView(Landroid/view/View;II)V
    .locals 0

    .prologue
    .line 148
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ScrollView;->addView(Landroid/view/View;II)V

    .line 149
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->d(Landroid/view/View;)V

    .line 150
    return-void
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    .prologue
    .line 142
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ScrollView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 143
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->d(Landroid/view/View;)V

    .line 144
    return-void
.end method

.method public addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    .prologue
    .line 154
    invoke-super {p0, p1, p2}, Landroid/widget/ScrollView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 155
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->d(Landroid/view/View;)V

    .line 156
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 160
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->b:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 162
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 163
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->getPaddingLeft()I

    move-result v0

    int-to-float v2, v0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->getScrollY()I

    move-result v0

    int-to-float v0, v0

    iget v3, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->c:F

    add-float/2addr v3, v0

    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->e:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->getPaddingTop()I

    move-result v0

    :goto_0
    int-to-float v0, v0

    add-float/2addr v0, v3

    invoke-virtual {p1, v2, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 164
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->e:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->c:F

    neg-float v0, v0

    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->b:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/graphics/Canvas;->clipRect(FFFF)Z

    .line 165
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "-hastransparancy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->b:Landroid/view/View;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->f(Landroid/view/View;)V

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->b:Landroid/view/View;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->e(Landroid/view/View;)V

    .line 172
    :goto_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 174
    :cond_0
    return-void

    .line 163
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 164
    goto :goto_1

    .line 170
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    goto :goto_2
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 178
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 179
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->d:Z

    .line 182
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->d:Z

    if-eqz v0, :cond_5

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->b:Landroid/view/View;

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->d:Z

    .line 184
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->d:Z

    if-eqz v0, :cond_1

    .line 185
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->b:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->c:F

    add-float/2addr v3, v4

    cmpg-float v0, v0, v3

    if-gtz v0, :cond_4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->b:Landroid/view/View;

    invoke-direct {p0, v3}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->a(Landroid/view/View;)I

    move-result v3

    int-to-float v3, v3

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->b:Landroid/view/View;

    invoke-direct {p0, v3}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->c(Landroid/view/View;)I

    move-result v3

    int-to-float v3, v3

    cmpg-float v0, v0, v3

    if-gtz v0, :cond_4

    :goto_1
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->d:Z

    .line 193
    :cond_1
    :goto_2
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->d:Z

    if-eqz v0, :cond_2

    .line 194
    const/4 v0, 0x0

    const/high16 v1, -0x40800000    # -1.0f

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->getScrollY()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->c:F

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->b:Landroid/view/View;

    invoke-direct {p0, v3}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->b(Landroid/view/View;)I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 196
    :cond_2
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    :cond_3
    move v0, v2

    .line 183
    goto :goto_0

    :cond_4
    move v1, v2

    .line 185
    goto :goto_1

    .line 190
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->b:Landroid/view/View;

    if-nez v0, :cond_1

    .line 191
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->d:Z

    goto :goto_2
.end method

.method public notifyStickyAttributeChanged()V
    .locals 0

    .prologue
    .line 281
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->c()V

    .line 282
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 114
    invoke-super/range {p0 .. p5}, Landroid/widget/ScrollView;->onLayout(ZIIII)V

    .line 115
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->f:Z

    if-nez v0, :cond_0

    .line 116
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->e:Z

    .line 118
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->c()V

    .line 119
    return-void
.end method

.method protected onScrollChanged(IIII)V
    .locals 0

    .prologue
    .line 227
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    .line 228
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->a()V

    .line 229
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 203
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->d:Z

    if-eqz v0, :cond_0

    .line 204
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->getScrollY()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->c:F

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->b:Landroid/view/View;

    invoke-direct {p0, v2}, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->b(Landroid/view/View;)I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 207
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    .line 208
    iput-boolean v3, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->h:Z

    .line 211
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->h:Z

    if-eqz v0, :cond_2

    .line 212
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 213
    invoke-virtual {v0, v3}, Landroid/view/MotionEvent;->setAction(I)V

    .line 214
    invoke-super {p0, v0}, Landroid/widget/ScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 215
    iput-boolean v3, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->h:Z

    .line 218
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-eq v0, v4, :cond_3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    .line 219
    :cond_3
    iput-boolean v4, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->h:Z

    .line 222
    :cond_4
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public setClipToPadding(Z)V
    .locals 1

    .prologue
    .line 123
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->setClipToPadding(Z)V

    .line 124
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->e:Z

    .line 125
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->f:Z

    .line 126
    return-void
.end method

.method public setup()V
    .locals 1

    .prologue
    .line 73
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/StickyScrollView;->a:Ljava/util/ArrayList;

    .line 74
    return-void
.end method
