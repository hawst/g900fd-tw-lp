.class public Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;
.source "ProGuard"


# static fields
.field private static a:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;-><init>()V

    .line 23
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine;

    if-nez v0, :cond_0

    .line 34
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine;

    .line 36
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine;

    return-object v0
.end method


# virtual methods
.method protected entry(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 3

    .prologue
    .line 95
    const-string v0, "PreloadUpdateManagerStateMachine"

    const-string v1, "entry"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 96
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/m;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 120
    :goto_0
    :pswitch_0
    return-void

    .line 99
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;->NOTIFY_FINISH:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 100
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 105
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 106
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 109
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;->REQUEST_UPDATECHECK:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 112
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;->REGISTER_NOTIFICATION:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 113
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$State;->FINISH:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 116
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;->START_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 117
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;->CHECK_TIMEOUT_FOR_UPDATE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 96
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_5
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method

.method public execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;)Z
    .locals 3

    .prologue
    .line 42
    const-string v0, "PreloadUpdateManagerStateMachine"

    const-string v1, "execute"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 43
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/m;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 82
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 46
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/m;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 49
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$State;->CHECK_TIMEOUT_FOR_UPDATE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 52
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$State;->REQ_UPDATE_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 57
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/m;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 60
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;->NOTIFY_FINISH:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 61
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 64
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$State;->REQ_UPDATE_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 67
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;->NOTIFY_FINISH:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 68
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 73
    :pswitch_7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/m;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    goto :goto_0

    .line 76
    :pswitch_8
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 79
    :pswitch_9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$State;->NOTIFY_NOTIFICATION:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 43
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_7
    .end packed-switch

    .line 46
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 57
    :pswitch_data_2
    .packed-switch 0x3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 73
    :pswitch_data_3
    .packed-switch 0x6
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public bridge synthetic execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 10
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;)Z

    move-result v0

    return v0
.end method

.method protected exit(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 3

    .prologue
    .line 124
    const-string v0, "PreloadUpdateManagerStateMachine"

    const-string v1, "exit"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 125
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/m;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 131
    :goto_0
    return-void

    .line 128
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;->STOP_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 125
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method
