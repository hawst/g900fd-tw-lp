.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field mEmailAddress:Ljava/lang/String;

.field mValidatorResult:Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator$ValidatorResult;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator;->mEmailAddress:Ljava/lang/String;

    .line 9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator;->mEmailAddress:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator;->validateEmail(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator$ValidatorResult;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator;->mValidatorResult:Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator$ValidatorResult;

    .line 10
    return-void
.end method

.method private isAllowedString(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 27
    const/4 v0, 0x1

    move v2, v1

    .line 31
    :goto_0
    :try_start_0
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 33
    add-int/lit8 v3, v2, 0x1

    invoke-virtual {p2, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    move v0, v1

    .line 31
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 41
    :catch_0
    move-exception v0

    move v0, v1

    .line 43
    :cond_1
    return v0
.end method


# virtual methods
.method public getValidationResult()Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator$ValidatorResult;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator;->mValidatorResult:Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator$ValidatorResult;

    return-object v0
.end method

.method public validateEmail(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator$ValidatorResult;
    .locals 7

    .prologue
    .line 49
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator$ValidatorResult;->INVALID:Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator$ValidatorResult;

    .line 53
    :try_start_0
    const-string v0, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.@"

    .line 54
    const-string v1, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

    .line 56
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    .line 57
    const-string v3, "@"

    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 58
    const-string v4, "@"

    invoke-virtual {p1, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    .line 59
    const-string v5, "."

    invoke-virtual {p1, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    .line 61
    const/4 v6, 0x7

    if-ge v2, v6, :cond_0

    .line 63
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator$ValidatorResult;->INVALID:Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator$ValidatorResult;

    .line 99
    :goto_0
    return-object v0

    .line 65
    :cond_0
    const/16 v6, 0x32

    if-le v2, v6, :cond_1

    .line 67
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator$ValidatorResult;->INVALID:Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator$ValidatorResult;

    goto :goto_0

    .line 69
    :cond_1
    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator;->isAllowedString(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 71
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator$ValidatorResult;->INVALID:Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator$ValidatorResult;

    goto :goto_0

    .line 73
    :cond_2
    const/4 v0, 0x0

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator;->isAllowedString(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 75
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator$ValidatorResult;->INVALID:Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator$ValidatorResult;

    goto :goto_0

    .line 77
    :cond_3
    if-gez v3, :cond_4

    .line 79
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator$ValidatorResult;->INVALID:Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator$ValidatorResult;

    goto :goto_0

    .line 81
    :cond_4
    if-eq v3, v4, :cond_5

    .line 83
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator$ValidatorResult;->INVALID:Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator$ValidatorResult;

    goto :goto_0

    .line 85
    :cond_5
    if-gez v5, :cond_6

    .line 87
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator$ValidatorResult;->INVALID:Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator$ValidatorResult;

    goto :goto_0

    .line 91
    :cond_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator$ValidatorResult;->VALID:Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator$ValidatorResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 96
    :catch_0
    move-exception v0

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator$ValidatorResult;->INVALID:Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator$ValidatorResult;

    goto :goto_0
.end method
