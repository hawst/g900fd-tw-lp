.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum CHECK_PACKAGE_EXIST:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

.field public static final enum COMPARE_PERMISSION_WITH_APK:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

.field public static final enum NOTIFY_FAILURE:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

.field public static final enum NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

.field public static final enum REQUEST_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

.field public static final enum SHOW_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

.field public static final enum START_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

.field public static final enum STOP_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 23
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

    const-string v1, "REQUEST_PERMISSION"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;->REQUEST_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

    const-string v1, "NOTIFY_FAILURE"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;->NOTIFY_FAILURE:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

    const-string v1, "SHOW_PERMISSION"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;->SHOW_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

    const-string v1, "NOTIFY_SUCCESS"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;->NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

    const-string v1, "START_TIMER"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;->START_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

    const-string v1, "STOP_TIMER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;->STOP_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

    const-string v1, "CHECK_PACKAGE_EXIST"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;->CHECK_PACKAGE_EXIST:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

    const-string v1, "COMPARE_PERMISSION_WITH_APK"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;->COMPARE_PERMISSION_WITH_APK:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

    .line 21
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;->REQUEST_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;->NOTIFY_FAILURE:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;->SHOW_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;->NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;->START_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;->STOP_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;->CHECK_PACKAGE_EXIST:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;->COMPARE_PERMISSION_WITH_APK:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManagerStateMachine$Action;

    return-object v0
.end method
