.class public final enum Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;

.field public static final enum irregularFormat:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;

.field public static final enum lefthigher:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;

.field public static final enum leftlower:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;

.field public static final enum same:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 306
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;

    const-string v1, "same"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;->same:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;

    .line 307
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;

    const-string v1, "lefthigher"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;->lefthigher:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;

    .line 308
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;

    const-string v1, "leftlower"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;->leftlower:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;

    .line 309
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;

    const-string v1, "irregularFormat"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;->irregularFormat:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;

    .line 304
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;->same:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;->lefthigher:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;->leftlower:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;->irregularFormat:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 304
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;
    .locals 1

    .prologue
    .line 304
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;
    .locals 1

    .prologue
    .line 304
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;

    return-object v0
.end method
