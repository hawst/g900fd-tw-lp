.class public abstract Lcom/sec/android/app/samsungapps/view/MyGridListView;
.super Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;
.source "ProGuard"


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/view/ViewList;

.field private b:Lcom/sec/android/app/samsungapps/view/ViewList;

.field private c:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;-><init>(Landroid/content/Context;)V

    .line 14
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/MyGridListView;->c:Ljava/util/ArrayList;

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 14
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/MyGridListView;->c:Ljava/util/ArrayList;

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsGridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 14
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/MyGridListView;->c:Ljava/util/ArrayList;

    .line 18
    return-void
.end method


# virtual methods
.method public addStateObserver(Lcom/sec/android/app/samsungapps/view/MyGridListView$ListViewObserver;)V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MyGridListView;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 82
    return-void
.end method

.method protected abstract empty()Z
.end method

.method public notifyLoadCompleted()V
    .locals 2

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MyGridListView;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/view/MyGridListView$ListViewObserver;

    .line 101
    invoke-interface {v0, p0}, Lcom/sec/android/app/samsungapps/view/MyGridListView$ListViewObserver;->loadCompleted(Landroid/view/View;)V

    goto :goto_0

    .line 103
    :cond_0
    return-void
.end method

.method public notifyLoading()V
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MyGridListView;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/view/MyGridListView$ListViewObserver;

    .line 93
    invoke-interface {v0, p0}, Lcom/sec/android/app/samsungapps/view/MyGridListView$ListViewObserver;->loading(Landroid/view/View;)V

    goto :goto_0

    .line 95
    :cond_0
    return-void
.end method

.method public release()V
    .locals 0

    .prologue
    .line 77
    return-void
.end method

.method public removeStateObserver(Lcom/sec/android/app/samsungapps/view/MyGridListView$ListViewObserver;)V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MyGridListView;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 87
    return-void
.end method

.method public setEmptyViews(Lcom/sec/android/app/samsungapps/view/ViewList;Lcom/sec/android/app/samsungapps/view/ViewList;)V
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/MyGridListView;->a:Lcom/sec/android/app/samsungapps/view/ViewList;

    .line 31
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/view/MyGridListView;->b:Lcom/sec/android/app/samsungapps/view/ViewList;

    .line 32
    return-void
.end method

.method public setLoadingEmptyView()V
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MyGridListView;->b:Lcom/sec/android/app/samsungapps/view/ViewList;

    if-eqz v0, :cond_2

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MyGridListView;->a:Lcom/sec/android/app/samsungapps/view/ViewList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MyGridListView;->a:Lcom/sec/android/app/samsungapps/view/ViewList;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 39
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/MyGridListView;->empty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 40
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MyGridListView;->b:Lcom/sec/android/app/samsungapps/view/ViewList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 42
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MyGridListView;->b:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ViewList;->getTopView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/MyGridListView;->setEmptyView(Landroid/view/View;)V

    .line 44
    :cond_2
    return-void
.end method

.method public setNoDataEmptyView()V
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MyGridListView;->a:Lcom/sec/android/app/samsungapps/view/ViewList;

    if-eqz v0, :cond_2

    .line 50
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MyGridListView;->b:Lcom/sec/android/app/samsungapps/view/ViewList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MyGridListView;->b:Lcom/sec/android/app/samsungapps/view/ViewList;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 51
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/MyGridListView;->empty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 52
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MyGridListView;->a:Lcom/sec/android/app/samsungapps/view/ViewList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 54
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/MyGridListView;->a:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ViewList;->getTopView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/MyGridListView;->setEmptyView(Landroid/view/View;)V

    .line 56
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/MyGridListView;->invalidate()V

    .line 57
    return-void
.end method
