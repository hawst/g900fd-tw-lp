.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# instance fields
.field public apnName:Ljava/lang/String;

.field private mConfirmMsg:Ljava/lang/String;

.field private mResponseMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

.field private mResponseTime:J

.field public optUrl:Ljava/lang/String;

.field public orderID:Ljava/lang/String;

.field public paymentID:Ljava/lang/String;

.field public productID:Ljava/lang/String;

.field public retryCount:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;->retryCount:I

    .line 14
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;->productID:Ljava/lang/String;

    .line 15
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;->orderID:Ljava/lang/String;

    .line 16
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;->paymentID:Ljava/lang/String;

    .line 17
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;->optUrl:Ljava/lang/String;

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;->apnName:Ljava/lang/String;

    .line 20
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;->mResponseTime:J

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;->mConfirmMsg:Ljava/lang/String;

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;->mResponseMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 26
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;->mResponseMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 27
    return-void
.end method


# virtual methods
.method public addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;->mResponseMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 57
    return-void
.end method

.method public clearContainer()V
    .locals 0

    .prologue
    .line 39
    return-void
.end method

.method public closeMap()V
    .locals 3

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;->mResponseMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    const-class v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->mappingClass(Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 51
    return-void
.end method

.method public findString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;->mResponseMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getApnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;->apnName:Ljava/lang/String;

    return-object v0
.end method

.method public getConfirmMsg()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;->mConfirmMsg:Ljava/lang/String;

    return-object v0
.end method

.method public getOptUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;->optUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getOrderID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;->orderID:Ljava/lang/String;

    return-object v0
.end method

.method public getPaymentID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;->paymentID:Ljava/lang/String;

    return-object v0
.end method

.method public getProductID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;->productID:Ljava/lang/String;

    return-object v0
.end method

.method public getProductID(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;->productID:Ljava/lang/String;

    .line 118
    return-void
.end method

.method public getResponseMap()Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;->mResponseMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    return-object v0
.end method

.method public getResponseTime()J
    .locals 2

    .prologue
    .line 82
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;->mResponseTime:J

    return-wide v0
.end method

.method public getRetryCount()I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;->retryCount:I

    return v0
.end method

.method public openMap()V
    .locals 0

    .prologue
    .line 33
    return-void
.end method

.method public setApnName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;->apnName:Ljava/lang/String;

    .line 143
    return-void
.end method

.method public setConfirmMsg(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;->mConfirmMsg:Ljava/lang/String;

    .line 148
    return-void
.end method

.method public setOptUrl(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;->optUrl:Ljava/lang/String;

    .line 138
    return-void
.end method

.method public setOrderID(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 122
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;->orderID:Ljava/lang/String;

    .line 123
    return-void
.end method

.method public setPaymentID(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;->paymentID:Ljava/lang/String;

    .line 113
    return-void
.end method

.method public setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 159
    return-void
.end method

.method public setResponseMap(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 152
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;->mResponseMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 153
    return-void
.end method

.method public setResponseTime(J)V
    .locals 0

    .prologue
    .line 127
    iput-wide p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;->mResponseTime:J

    .line 128
    return-void
.end method

.method public setRetryCount(I)V
    .locals 0

    .prologue
    .line 132
    iput p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/directbilling/DirectBillingInitResult;->retryCount:I

    .line 133
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x1

    return v0
.end method
