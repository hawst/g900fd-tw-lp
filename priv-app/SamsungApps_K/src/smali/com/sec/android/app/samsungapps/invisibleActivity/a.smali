.class final Lcom/sec/android/app/samsungapps/invisibleActivity/a;
.super Landroid/os/Handler;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/a;->a:Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 89
    iget v1, p1, Landroid/os/Message;->what:I

    .line 90
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/content/Intent;

    .line 93
    packed-switch v1, :pswitch_data_0

    .line 117
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/a;->a:Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;->finish()V

    .line 125
    :goto_1
    return-void

    .line 99
    :pswitch_0
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/a;->a:Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;->a(Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;)Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IJellyBeanNetworkPopupResponse;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IJellyBeanNetworkPopupResponse;->notifiedAndEndApp()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 121
    :catch_0
    move-exception v0

    .line 122
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "JBNetworkErrorPopupActivity::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/a;->a:Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;->finish()V

    goto :goto_1

    .line 102
    :pswitch_1
    if-eqz v0, :cond_1

    .line 104
    :try_start_2
    const-string v1, "retVal"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 105
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/a;->a:Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;->a(Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;)Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IJellyBeanNetworkPopupResponse;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IJellyBeanNetworkPopupResponse;->notifiedAndNeedRetry()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 124
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/a;->a:Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;->finish()V

    throw v0

    .line 110
    :cond_0
    :try_start_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/a;->a:Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;->a(Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;)Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IJellyBeanNetworkPopupResponse;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IJellyBeanNetworkPopupResponse;->notifiedAndEndApp()V

    goto :goto_0

    .line 115
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/invisibleActivity/a;->a:Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;->a(Lcom/sec/android/app/samsungapps/invisibleActivity/JBNetworkErrorPopupActivity;)Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IJellyBeanNetworkPopupResponse;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/neterrorcheck/INetworkErrorPopup$IJellyBeanNetworkPopupResponse;->notifiedAndEndApp()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 93
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
