.class public Lcom/sec/android/app/samsungapps/widget/ChartBannerWidgetHelper;
.super Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentCategoryContentListQuery;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerWidgetData;


# static fields
.field public static final CONTENT_GAME_CATEGORY_ID:Ljava/lang/String;


# instance fields
.field a:Landroid/content/Context;

.field private final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-string v0, "35,35,35,35,35,3d,35,38,37,35,"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->coverLang(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/samsungapps/widget/ChartBannerWidgetHelper;->CONTENT_GAME_CATEGORY_ID:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/sec/android/app/samsungapps/widget/ChartBannerWidgetHelper;->CONTENT_GAME_CATEGORY_ID:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentCategoryContentListQuery;-><init>(Ljava/lang/String;)V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/ChartBannerWidgetHelper;->a:Landroid/content/Context;

    .line 22
    const-string v0, "ChartBannerWidgetHelper"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/ChartBannerWidgetHelper;->b:Ljava/lang/String;

    .line 33
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/ChartBannerWidgetHelper;->a:Landroid/content/Context;

    .line 34
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/ChartBannerWidgetHelper;->a:Landroid/content/Context;

    .line 42
    return-void
.end method

.method public getContent(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;
    .locals 2

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/ChartBannerWidgetHelper;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v0

    .line 86
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->size()I

    move-result v1

    if-gt v1, p1, :cond_1

    .line 88
    :cond_0
    const-string v0, "ChartBannerWidgetHelper::getContent::contentList is empty or less"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 89
    const/4 v0, 0x0

    .line 94
    :goto_0
    return-object v0

    .line 92
    :cond_1
    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    goto :goto_0
.end method

.method public getContentCount()I
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/ChartBannerWidgetHelper;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v0

    .line 64
    if-nez v0, :cond_0

    .line 66
    const-string v0, "ChartBannerWidgetHelper::getContentCount::content list is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 67
    const/4 v0, 0x0

    .line 72
    :goto_0
    return v0

    .line 70
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/ChartBannerWidgetHelper;->a:Landroid/content/Context;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/ChartBannerWidgetHelper;->requestDataGet(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;Landroid/content/Context;)V

    .line 52
    return-void
.end method
