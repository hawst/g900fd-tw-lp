.class final Lcom/sec/android/app/samsungapps/vlibrary2/purchase/c;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/ICountry;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/c;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getCurCountryType()Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;
    .locals 2

    .prologue
    .line 65
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    .line 67
    if-nez v0, :cond_0

    .line 69
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;->Other:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    .line 102
    :goto_0
    return-object v0

    .line 72
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isKorea()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 74
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;->Korea:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    goto :goto_0

    .line 77
    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isChina()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 79
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;->China:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    goto :goto_0

    .line 82
    :cond_2
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isIran()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 84
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;->Iran:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    goto :goto_0

    .line 87
    :cond_3
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isUkraine()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 89
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;->Ukraine:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    goto :goto_0

    .line 92
    :cond_4
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isFrance()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 94
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;->France:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    goto :goto_0

    .line 97
    :cond_5
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isGermany()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 99
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;->Germany:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    goto :goto_0

    .line 102
    :cond_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;->Other:Lcom/sec/android/app/samsungapps/vlibrary2/countryinterface/CountryType;

    goto :goto_0
.end method
