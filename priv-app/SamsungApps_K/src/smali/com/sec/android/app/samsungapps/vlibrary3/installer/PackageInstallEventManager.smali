.class public Lcom/sec/android/app/samsungapps/vlibrary3/installer/PackageInstallEventManager;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/PackageInstallEventManager;


# instance fields
.field a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/PackageInstallEventManager;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    .line 60
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/installer/PackageInstallEventManager;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/PackageInstallEventManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/PackageInstallEventManager;

    if-nez v0, :cond_0

    .line 12
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/PackageInstallEventManager;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/PackageInstallEventManager;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/PackageInstallEventManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/PackageInstallEventManager;

    .line 14
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/PackageInstallEventManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/PackageInstallEventManager;

    return-object v0
.end method


# virtual methods
.method public addObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/PackageInstallEventManager$IPackageInstallEventObserver;)V
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/PackageInstallEventManager;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;->add(Ljava/lang/Object;)Z

    .line 21
    return-void
.end method

.method public notifyPackageAdded(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/PackageInstallEventManager;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;->clone()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/PackageInstallEventManager$IPackageInstallEventObserver;

    .line 32
    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/PackageInstallEventManager$IPackageInstallEventObserver;->onPackageAdded(Ljava/lang/String;)V

    goto :goto_0

    .line 34
    :cond_0
    return-void
.end method

.method public notifyPackageInstalled(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/PackageInstallEventManager;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;->clone()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/PackageInstallEventManager$IPackageInstallEventObserver;

    .line 56
    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/PackageInstallEventManager$IPackageInstallEventObserver;->onPackageInstalled(Ljava/lang/String;)V

    goto :goto_0

    .line 58
    :cond_0
    return-void
.end method

.method public notifyPackageRemoved(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/PackageInstallEventManager;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;->clone()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/PackageInstallEventManager$IPackageInstallEventObserver;

    .line 40
    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/PackageInstallEventManager$IPackageInstallEventObserver;->onPackageRemoved(Ljava/lang/String;)V

    goto :goto_0

    .line 42
    :cond_0
    return-void
.end method

.method public notifyPackageReplaced(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/PackageInstallEventManager;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;->clone()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/PackageInstallEventManager$IPackageInstallEventObserver;

    .line 48
    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/PackageInstallEventManager$IPackageInstallEventObserver;->onPackageReplaced(Ljava/lang/String;)V

    goto :goto_0

    .line 50
    :cond_0
    return-void
.end method

.method public removeObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/PackageInstallEventManager$IPackageInstallEventObserver;)V
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/PackageInstallEventManager;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;->remove(Ljava/lang/Object;)Z

    .line 26
    return-void
.end method
