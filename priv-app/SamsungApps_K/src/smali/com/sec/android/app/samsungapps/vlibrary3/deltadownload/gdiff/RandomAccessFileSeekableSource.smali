.class public Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/RandomAccessFileSeekableSource;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/SeekableSource;


# instance fields
.field private a:Ljava/io/RandomAccessFile;


# direct methods
.method public constructor <init>(Ljava/io/RandomAccessFile;)V
    .locals 2

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    if-nez p1, :cond_0

    .line 47
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "raf"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/RandomAccessFileSeekableSource;->a:Ljava/io/RandomAccessFile;

    .line 49
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/RandomAccessFileSeekableSource;->a:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V

    .line 65
    return-void
.end method

.method public length()J
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/RandomAccessFileSeekableSource;->a:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v0

    return-wide v0
.end method

.method public read(Ljava/nio/ByteBuffer;)I
    .locals 5

    .prologue
    const/4 v0, -0x1

    .line 68
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/RandomAccessFileSeekableSource;->a:Ljava/io/RandomAccessFile;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v1

    .line 69
    if-ne v1, v0, :cond_0

    .line 72
    :goto_0
    return v0

    .line 71
    :cond_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    add-int/2addr v0, v1

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    move v0, v1

    .line 72
    goto :goto_0
.end method

.method public read([BII)I
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/RandomAccessFileSeekableSource;->a:Ljava/io/RandomAccessFile;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v0

    return v0
.end method

.method public seek(J)V
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/gdiff/RandomAccessFileSeekableSource;->a:Ljava/io/RandomAccessFile;

    invoke-virtual {v0, p1, p2}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 53
    return-void
.end method
