.class public Lcom/sec/android/app/samsungapps/tobelog/LogManager;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field public static final ACTION_DEREGI:Ljava/lang/String; = "com.sec.spp.push.REQUEST_DEREGISTER"

.field public static final ACTION_REGI:Ljava/lang/String; = "com.sec.spp.push.REQUEST_REGISTER"

.field public static final ALARM_HOUR_OF_DAY:I = 0x17

.field public static final ALARM_MINUTE:I = 0x32

.field public static final ALARM_PERIOD:J = 0x5265c00L

.field public static final EXTRA_INTENTFILTER:Ljava/lang/String; = "EXTRA_INTENTFILTER"

.field public static final EXTRA_PACKAGENAME:Ljava/lang/String; = "EXTRA_PACKAGENAME"

.field public static final FILE_NAME_OF_SUMMARY_LOG:Ljava/lang/String; = "summary_log.dat"

.field public static final INTENTFILTER:Ljava/lang/String; = "com.sec.android.app.samsungapps.DLC_FILTER"

.field public static final IS_TESTMODE:Z = false

.field public static final LOG_SPEC_VERSION:Ljava/lang/String; = "99.02.00"

.field public static final LOG_TYPE:Ljava/lang/String; = "BIZ"

.field public static final MSG_DLC_DEREGISTER:I = 0xc

.field public static final MSG_DLC_REGISTER:I = 0xb

.field public static final MSG_DLC_SVC_AVAILABLE:I = 0xd

.field public static final MSG_DLC_SVC_UNAVAILABLE:I = 0xe

.field public static final SERVICE_CODE:Ljava/lang/String; = "023"

.field public static final TEST_ALARM_PERIOD:J = 0x493e0L

.field private static a:Lcom/sec/android/app/samsungapps/tobelog/LogManager;


# instance fields
.field private b:Z

.field private c:Landroid/content/Context;

.field private d:Lcom/sec/android/app/samsungapps/tobelog/e;

.field private e:Lcom/sec/android/app/samsungapps/tobelog/e;

.field private f:Lcom/sec/android/app/samsungapps/tobelog/a;

.field private g:Lcom/sec/android/app/samsungapps/tobelog/f;

.field private h:Ljava/util/Timer;

.field private i:Lcom/sec/android/app/samsungapps/tobelog/Preferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->a:Lcom/sec/android/app/samsungapps/tobelog/LogManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->b:Z

    .line 56
    return-void
.end method

.method private static a()J
    .locals 6

    .prologue
    .line 173
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 174
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/4 v3, 0x5

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/16 v4, 0x17

    const/16 v5, 0x32

    invoke-virtual/range {v0 .. v5}, Ljava/util/Calendar;->set(IIIII)V

    .line 177
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    .line 179
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 180
    const-wide/32 v2, 0x5265c00

    add-long/2addr v0, v2

    .line 183
    :cond_0
    return-wide v0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/tobelog/LogManager;)Lcom/sec/android/app/samsungapps/tobelog/Preferences;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->i:Lcom/sec/android/app/samsungapps/tobelog/Preferences;

    return-object v0
.end method

.method private b()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 307
    const-string v0, "== Send Previous logs"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->e:Lcom/sec/android/app/samsungapps/tobelog/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->e:Lcom/sec/android/app/samsungapps/tobelog/e;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/tobelog/e;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 308
    :cond_0
    :goto_0
    return-void

    .line 307
    :cond_1
    invoke-static {}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->a()J

    move-result-wide v0

    const-wide/32 v2, 0x5265c00

    sub-long/2addr v0, v2

    new-instance v2, Lcom/sec/android/app/samsungapps/tobelog/f;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->c:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->e:Lcom/sec/android/app/samsungapps/tobelog/e;

    invoke-direct {v2, v3, v4, v5, v5}, Lcom/sec/android/app/samsungapps/tobelog/f;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/tobelog/e;ZZ)V

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/f;->a(J)V

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/tobelog/f;->start()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/tobelog/LogManager;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->b()V

    return-void
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/tobelog/LogManager;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->c:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/tobelog/LogManager;)Lcom/sec/android/app/samsungapps/tobelog/e;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->e:Lcom/sec/android/app/samsungapps/tobelog/e;

    return-object v0
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/tobelog/LogManager;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->a:Lcom/sec/android/app/samsungapps/tobelog/LogManager;

    if-nez v0, :cond_0

    .line 60
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->a:Lcom/sec/android/app/samsungapps/tobelog/LogManager;

    .line 63
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->a:Lcom/sec/android/app/samsungapps/tobelog/LogManager;

    return-object v0
.end method


# virtual methods
.method public createLogData(Lcom/sec/android/app/samsungapps/tobelog/ILogPage;Lcom/sec/android/app/samsungapps/tobelog/ILogEvent;Ljava/lang/String;Lcom/sec/android/app/samsungapps/tobelog/ILogBody;)Lcom/sec/android/app/samsungapps/tobelog/LogData;
    .locals 8

    .prologue
    .line 259
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 260
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/NotInitializedException;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/tobelog/NotInitializedException;-><init>()V

    throw v0

    .line 263
    :cond_0
    const-string v1, "023"

    const-string v2, "BIZ"

    const-string v6, "99.02.00"

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v7, p4

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->createLogData(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/tobelog/ILogPage;Lcom/sec/android/app/samsungapps/tobelog/ILogEvent;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/tobelog/ILogBody;)Lcom/sec/android/app/samsungapps/tobelog/LogData;

    move-result-object v0

    .line 265
    return-object v0
.end method

.method protected createLogData(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/tobelog/ILogPage;Lcom/sec/android/app/samsungapps/tobelog/ILogEvent;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/tobelog/ILogBody;)Lcom/sec/android/app/samsungapps/tobelog/LogData;
    .locals 8

    .prologue
    .line 251
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogData;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object v7, p7

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/samsungapps/tobelog/LogData;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/tobelog/ILogPage;Lcom/sec/android/app/samsungapps/tobelog/ILogEvent;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/tobelog/ILogBody;)V

    .line 253
    return-object v0
.end method

.method public destroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 188
    const-string v0, "Destroy LogManager"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V

    .line 190
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->b:Z

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->h:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->h:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 194
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->h:Ljava/util/Timer;

    .line 197
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->c:Landroid/content/Context;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->f:Lcom/sec/android/app/samsungapps/tobelog/a;

    if-eqz v0, :cond_1

    .line 198
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->f:Lcom/sec/android/app/samsungapps/tobelog/a;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 201
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->g:Lcom/sec/android/app/samsungapps/tobelog/f;

    if-eqz v0, :cond_2

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->g:Lcom/sec/android/app/samsungapps/tobelog/f;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/tobelog/f;->b()V

    .line 206
    :cond_2
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->e:Lcom/sec/android/app/samsungapps/tobelog/e;

    if-eqz v0, :cond_3

    .line 207
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->e:Lcom/sec/android/app/samsungapps/tobelog/e;

    const-string v1, "summary_log.dat"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/e;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 214
    :cond_3
    :goto_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/tobelog/h;->a()Lcom/sec/android/app/samsungapps/tobelog/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/tobelog/h;->b()V

    .line 216
    sput-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->a:Lcom/sec/android/app/samsungapps/tobelog/LogManager;

    .line 217
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->c:Landroid/content/Context;

    .line 218
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->d:Lcom/sec/android/app/samsungapps/tobelog/e;

    .line 219
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->e:Lcom/sec/android/app/samsungapps/tobelog/e;

    .line 220
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->f:Lcom/sec/android/app/samsungapps/tobelog/a;

    .line 221
    return-void

    .line 209
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public initialize(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 78
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->isInitialized()Z

    move-result v0

    if-ne v0, v5, :cond_0

    .line 116
    :goto_0
    return-void

    .line 82
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->c:Landroid/content/Context;

    .line 84
    const-string v0, "Initializing"

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->c:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 87
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "initial values are never NULL"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 90
    :cond_1
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/Preferences;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/Preferences;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->i:Lcom/sec/android/app/samsungapps/tobelog/Preferences;

    .line 91
    invoke-static {}, Lcom/sec/android/app/samsungapps/tobelog/h;->a()Lcom/sec/android/app/samsungapps/tobelog/h;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->c:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/h;->a(Landroid/content/Context;)V

    .line 93
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/e;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/e;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->d:Lcom/sec/android/app/samsungapps/tobelog/e;

    .line 94
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/e;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/e;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->e:Lcom/sec/android/app/samsungapps/tobelog/e;

    .line 96
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->e:Lcom/sec/android/app/samsungapps/tobelog/e;

    const-string v1, "summary_log.dat"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/e;->b(Ljava/lang/String;)V

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->e:Lcom/sec/android/app/samsungapps/tobelog/e;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/tobelog/e;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 102
    :goto_1
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/a;

    new-instance v1, Lcom/sec/android/app/samsungapps/tobelog/d;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/tobelog/d;-><init>(Lcom/sec/android/app/samsungapps/tobelog/LogManager;)V

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/a;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->f:Lcom/sec/android/app/samsungapps/tobelog/a;

    .line 104
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/f;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->d:Lcom/sec/android/app/samsungapps/tobelog/e;

    invoke-direct {v0, v1, v2, v5, v5}, Lcom/sec/android/app/samsungapps/tobelog/f;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/tobelog/e;ZZ)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->g:Lcom/sec/android/app/samsungapps/tobelog/f;

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->g:Lcom/sec/android/app/samsungapps/tobelog/f;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/tobelog/f;->start()V

    .line 107
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.sec.android.app.samsungapps.DLC_FILTER"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->f:Lcom/sec/android/app/samsungapps/tobelog/a;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 109
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0, v5}, Ljava/util/Timer;-><init>(Z)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->h:Ljava/util/Timer;

    .line 110
    new-instance v0, Ljava/util/Date;

    invoke-static {}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->a()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Setting timer for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->h:Ljava/util/Timer;

    new-instance v2, Lcom/sec/android/app/samsungapps/tobelog/c;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/tobelog/c;-><init>(Lcom/sec/android/app/samsungapps/tobelog/LogManager;)V

    const-wide/32 v3, 0x5265c00

    invoke-virtual {v1, v2, v0, v3, v4}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;Ljava/util/Date;J)V

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->i:Lcom/sec/android/app/samsungapps/tobelog/Preferences;

    const-string v1, "registered"

    invoke-virtual {v0, v1, v6}, Lcom/sec/android/app/samsungapps/tobelog/Preferences;->getBoolean(Ljava/lang/String;Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v5, :cond_2

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->b()V

    .line 115
    :goto_2
    iput-boolean v5, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->b:Z

    goto/16 :goto_0

    .line 98
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1

    .line 113
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.spp.push.REQUEST_REGISTER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "EXTRA_PACKAGENAME"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "EXTRA_INTENTFILTER"

    const-string v2, "com.sec.android.app.samsungapps.DLC_FILTER"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->c:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_2
.end method

.method protected isInitialized()Z
    .locals 1

    .prologue
    .line 119
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->b:Z

    return v0
.end method

.method public putNormalLog(Lcom/sec/android/app/samsungapps/tobelog/LogData;)V
    .locals 1

    .prologue
    .line 270
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 271
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/NotInitializedException;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/tobelog/NotInitializedException;-><init>()V

    throw v0

    .line 274
    :cond_0
    const-string v0, "putNormalLog as below"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V

    .line 275
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/tobelog/LogData;->a()V

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->d:Lcom/sec/android/app/samsungapps/tobelog/e;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/tobelog/e;->offer(Ljava/lang/Object;)Z

    .line 278
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->g:Lcom/sec/android/app/samsungapps/tobelog/f;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/tobelog/f;->a()V

    .line 279
    return-void
.end method

.method public putSummaryLog(Lcom/sec/android/app/samsungapps/tobelog/LogData;)V
    .locals 4

    .prologue
    .line 283
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 284
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/NotInitializedException;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/tobelog/NotInitializedException;-><init>()V

    throw v0

    .line 287
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->e:Lcom/sec/android/app/samsungapps/tobelog/e;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/tobelog/LogData;->getPageId()Lcom/sec/android/app/samsungapps/tobelog/ILogPage;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/tobelog/LogData;->getEventId()Lcom/sec/android/app/samsungapps/tobelog/ILogEvent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/tobelog/e;->a(Lcom/sec/android/app/samsungapps/tobelog/ILogPage;Lcom/sec/android/app/samsungapps/tobelog/ILogEvent;)Lcom/sec/android/app/samsungapps/tobelog/LogData;

    move-result-object v0

    .line 289
    if-eqz v0, :cond_1

    .line 291
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "putSummaryLog - Found matched log data with "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/tobelog/LogData;->getActivity()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V

    .line 293
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogData;->getBody()Lcom/sec/android/app/samsungapps/tobelog/ILogBody;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/tobelog/LogData;->getBody()Lcom/sec/android/app/samsungapps/tobelog/ILogBody;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/tobelog/ILogBody;->getPageTimeDuration()J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/ILogBody;->sumPageTimeDuration(J)J

    .line 294
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogData;->getBody()Lcom/sec/android/app/samsungapps/tobelog/ILogBody;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/tobelog/ILogBody;->increaseEventCount()I

    .line 296
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogData;->a()V

    .line 304
    :goto_0
    return-void

    .line 299
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "putSummaryLog - Found NO matched data with "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/tobelog/LogData;->getActivity()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V

    .line 300
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/tobelog/LogData;->a()V

    .line 302
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->e:Lcom/sec/android/app/samsungapps/tobelog/e;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/tobelog/e;->offer(Ljava/lang/Object;)Z

    goto :goto_0
.end method
