.class public Lcom/sec/android/app/samsungapps/vlibrary2/download/InstallerRunner;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private _CurInstall:Lcom/sec/android/app/samsungapps/vlibrary2/download/IInstaller;

.field private _InstallerItemGetter:Lcom/sec/android/app/samsungapps/vlibrary2/download/InstallerItemGetter;

.field private _bRunning:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/download/InstallerItemGetter;)V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/InstallerRunner;->_bRunning:Z

    .line 10
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/InstallerRunner;->_InstallerItemGetter:Lcom/sec/android/app/samsungapps/vlibrary2/download/InstallerItemGetter;

    .line 11
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/download/InstallerRunner;Z)V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/InstallerRunner;->onItemInstalled(Z)V

    return-void
.end method

.method private onItemInstalled(Z)V
    .locals 0

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/download/InstallerRunner;->next()V

    .line 47
    return-void
.end method


# virtual methods
.method public isRunning()Z
    .locals 1

    .prologue
    .line 14
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/InstallerRunner;->_bRunning:Z

    return v0
.end method

.method protected next()V
    .locals 2

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/InstallerRunner;->_InstallerItemGetter:Lcom/sec/android/app/samsungapps/vlibrary2/download/InstallerItemGetter;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/download/InstallerItemGetter;->getNextInstaller()Lcom/sec/android/app/samsungapps/vlibrary2/download/IInstaller;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/InstallerRunner;->_CurInstall:Lcom/sec/android/app/samsungapps/vlibrary2/download/IInstaller;

    .line 24
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/InstallerRunner;->_CurInstall:Lcom/sec/android/app/samsungapps/vlibrary2/download/IInstaller;

    if-nez v0, :cond_0

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/InstallerRunner;->_bRunning:Z

    .line 42
    :goto_0
    return-void

    .line 29
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/InstallerRunner;->_bRunning:Z

    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/download/InstallerRunner;->_CurInstall:Lcom/sec/android/app/samsungapps/vlibrary2/download/IInstaller;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/download/b;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/download/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/download/InstallerRunner;)V

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/IInstaller;->install(Lcom/sec/android/app/samsungapps/vlibrary2/download/IInstallerObserver;)Z

    goto :goto_0
.end method

.method public start()V
    .locals 0

    .prologue
    .line 18
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/download/InstallerRunner;->next()V

    .line 19
    return-void
.end method
