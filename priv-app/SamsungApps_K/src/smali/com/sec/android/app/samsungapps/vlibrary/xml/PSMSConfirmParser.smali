.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSConfirmParser;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;
.source "ProGuard"


# instance fields
.field private mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;

.field private mPSMSInitResult:Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;)V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;-><init>()V

    .line 13
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSConfirmParser;->mPSMSInitResult:Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;

    .line 14
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSConfirmParser;->mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;

    .line 15
    return-void
.end method


# virtual methods
.method protected onCreateObject(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 3

    .prologue
    .line 25
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSConfirmResult;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSConfirmResult;-><init>()V

    .line 26
    const-class v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSConfirmResult;

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->mappingClass(Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 27
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSConfirmParser;->mPSMSInitResult:Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSInitResult;->setConfirmResult(Lcom/sec/android/app/samsungapps/vlibrary/doc/PSMSConfirmResult;)V

    .line 28
    const-string v0, "orderID"

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 29
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSConfirmParser;->mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;

    invoke-interface {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;->setOrderID(Ljava/lang/String;)V

    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/PSMSConfirmParser;->mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;

    const-string v1, "1"

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;->setOrderIDSeq(Ljava/lang/String;)V

    .line 31
    return-void
.end method

.method protected onPostParseError()V
    .locals 0

    .prologue
    .line 20
    return-void
.end method

.method protected onPostParseResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 37
    return-void
.end method
