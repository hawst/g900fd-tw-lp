.class public Lcom/sec/android/app/samsungapps/NotiDialog;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field public static final ADDED_TO_WISH_LIST_STR_ID:I = 0xff14

.field public static final ADD_TO_WISH_LIST_STR_ID:I = 0xff13

.field public static final AIR_PLANE_MODE_STR_ID:I = 0xff0c

.field public static final ALREADY_INSTALLED_LATEST_VERION:I = 0xff42

.field public static final ALREADY_JOINED_STR_ID:I = 0xbb9

.field public static final ALREADY_PURCHASE_STR_ID:I = 0x1773

.field public static final ALREADY_REGISTERED_CARD_STR_ID:I = 0x158b

.field public static final ALREADY_REGISTERED_VOUCHER_CODE_STR_ID:I = 0x1b5c

.field public static final ALREADY_VOUCHER_CODE_STR_ID:I = 0x1b5e

.field public static final BANK_DECLINED_THE_PAYMENT:I = 0x138e

.field public static final BANK_IS_TEMPORARILY_NOT_AVAILABLE:I = 0x1396

.field public static final BANK_REQUIRES_CARRY_OUT_THE_TRANSACTION:I = 0x1391

.field public static final CHATON_RESTART_NOTI_STR_ID:I = 0xff32

.field public static final CHECK_APP_UPGRADE_STR_ID:I = 0xff01

.field public static final CHINA_CONNECT_WLAN_WARN:I = 0xff48

.field public static final CHINA_DATA_CHARGE_WARN:I = 0xff49

.field public static final COMMON_POSTFIX_ERROR:I = 0x1869f

.field public static final COMMON_POSTFIX_ERROR_STR_ID:I = 0xff3f

.field public static final CONFIG_RESTART_SAMSUNGAPPS_STR_ID:I = 0xff11

.field public static final CONNECT_CHARGE_NOTI:I = 0xff44

.field public static final CONNECT_VIA_OPERATORS_STR_ID:I = 0xff02

.field public static final CONNECT_WIBRO_STR_ID:I = 0xff0b

.field public static final CREDIT_CARDS_CNTRY_CODE_NOT_SUPPORTED:I = 0x1389

.field public static final CREDIT_CARD_ERROR_E_STR_ID:I = 0x13eb

.field public static final CREDIT_CARD_ERROR_STR_ID:I = 0x13ec

.field public static final CREDIT_CARD_ERROR_S_STR_ID:I = 0x1398

.field public static final CREDIT_CARD_REGISTER_INFO_ERROR_STR_ID:I = 0x14ba

.field public static final DANAL_FAILED_AUTH_NUM_STR_ID:I = 0x16ab

.field public static final DANAL_FAILED_MIN_AGE19_STR_ID:I = 0x16ad

.field public static final DANAL_FAILED_OPERATOR_STR_ID:I = 0x16a9

.field public static final DANAL_FAILED_PURCHASE_STR_ID:I = 0x16a8

.field public static final DANAL_FAILED_SSN_STR_ID:I = 0x16aa

.field public static final DANAL_FAILED_TIMEOUT_STR_ID:I = 0x16ac

.field public static final DEVICE_DOES_NOT_SUPPRT_SAPPS:I = 0x3ec

.field public static final DUPLICATED_REVIEW_STR_ID:I = 0xfa0

.field public static final EMAIL_ADDRESS_ALREADY_IN_USE_OSP_STR_ID:I = 0xbc4

.field public static final EMAIL_ADDRESS_ALREADY_IN_USE_SA_STR_ID:I = 0xbc5

.field public static final EXCEED_MIGRATION_DEVICE_STR_ID:I = 0xff37

.field public static final EXPIRED_VOUCHER_CODE_STR_ID:I = 0x1b5a

.field public static final FAILED_ADD_TO_LIKE_STR_ID:I = 0xfa7

.field public static final FAILED_ADD_TO_WISH_LIST_STR_ID:I = 0xfa6

.field public static final FAILED_INSTALL_STR_ID:I = 0xff30

.field public static final FAILED_REGISTER_IRAN_SHETAB_CARD_STR_ID:I = 0xff3c

.field public static final FAILED_REMOVE_TO_LIKE_STR_ID:I = 0xfa9

.field public static final FAILED_REMOVE_TO_WISH_LIST_STR_ID:I = 0xfa8

.field public static final FAILED_SEND_SMS_MESSAGE_STR_ID:I = 0xff34

.field public static final FAILED_TO_PURCHASE_STR_ID:I = 0xff1f

.field public static final FAILED_TO_REGISTER_PREPAID_STR_ID:I = 0x1586

.field public static final FAILED_TO_VERIFY_NAME_STR_ID_1:I = 0x1c21

.field public static final FAILED_TO_VERIFY_NAME_STR_ID_2:I = 0x1c22

.field public static final FAILED_TO_VERIFY_NAME_STR_ID_3:I = 0x1c25

.field public static final FAILED_TO_VERIFY_NAME_STR_ID_4:I = 0x1c35

.field public static final FLEXIBLE_DOWNLOAD_ALL_CHARGE_STR_ID:I = 0xff40

.field public static final IINCORRECT_PASSWORD_CYBERCASH_ACCOUNT:I = 0x14b4

.field public static final INCOMPATIBLE_DEVICE_STR_ID:I = 0x1f42

.field public static final INCOMPATIBLE_FIRMWARE_STR_ID:I = 0x1f40

.field public static final INCOMPATIBLE_OS_STR_ID:I = 0x1f41

.field public static final INSTALL_ISP_PACKAGE_STR_ID:I = 0xff20

.field public static final INVALID_CARD_TYPE:I = 0x138c

.field public static final INVALID_COMPANY_PREPAID_STR_ID:I = 0x1588

.field public static final INVALID_CVC_STR_ID:I = 0xff2e

.field public static final INVALID_DATE_MIN_AGE14_STR_ID:I = 0xff25

.field public static final INVALID_DATE_MIN_AGE18_STR_ID:I = 0xff26

.field public static final INVALID_DATE_MIN_AGE19_STR_ID:I = 0xff27

.field public static final INVALID_DATE_STR_ID:I = 0xff23

.field public static final INVALID_EMAIL_STR_ID:I = 0xbbe

.field public static final INVALID_EXPIRY_DATE:I = 0x138b

.field public static final INVALID_NUMBER_PREPAID_STR_ID:I = 0x1589

.field public static final INVALID_PASSWORD_PREPAID_STR_ID:I = 0x1587

.field public static final INVALID_PASSWORD_STR_ID:I = 0xbbf

.field public static final INVALID_PWD_STR_ID:I = 0xff33

.field public static final INVALID_SECURITY_CODE:I = 0x138d

.field public static final INVALID_VALUE_PREPAID_STR_ID:I = 0x158a

.field public static final INVALID_VOUCHER_CODE_STR_ID:I = 0x1b5b

.field public static final JOIN_INVALID_EMAIL_STR_ID:I = 0xbba

.field public static final JOIN_INVALID_PASSWORD_STR_ID_1:I = 0xbbb

.field public static final JOIN_INVALID_PASSWORD_STR_ID_2:I = 0xbbc

.field public static final KOREA_NO_CREDIT_INFO_STR_ID:I = 0xff18

.field public static final MANUALLY_PHONE_MICRO_PAYMENT_STR_ID:I = 0xff29

.field public static final MAXIMUM_NUMBER_OF_DOWNLOADS_REACHED:I = 0xfa5

.field public static final MIGRATION_QUESTION_STR_ID:I = 0xff36

.field public static final NEED_A_SPECIFIC_SIM_CARD_TO_USE_SAPPS:I = 0x3f0

.field public static final NETWORK_ERROR_OCCURRED_STR_ID:I = 0xff22

.field public static final NETWORK_OPERATOR_DOWNLOAD_TRY_STR_ID:I = 0xff2d

.field public static final NETWORK_OPERATOR_TRY_STR_ID:I = 0xff2c

.field public static final NOTICE_APP_WILL_CLOSE_STR_ID:I = 0xff47

.field public static final NOTICE_PRODUCT_INFO_CHANGED_STR_ID:I = 0xff3a

.field public static final NOTICE_PSMS_COMMON_STR_ID:I = 0xff35

.field public static final NOTICE_STORE_CHANGED:I = 0xff38

.field public static final NOTICE_STORE_LIMITED:I = 0xff39

.field public static final NOT_ENOUGH_BALANCE_ON_CARD:I = 0x1591

.field public static final NOT_ENOUGH_BALANCE_STR_ID:I = 0x15e1

.field public static final NOT_ENOUGH_BALANCE_YOUR_CYBERCASH:I = 0x1518

.field public static final NOT_ENOUGH_MEMORY_STR_ID:I = 0xff06

.field public static final NOT_ENTER_PROBLEM_DETAILS_STR_ID:I = 0xff31

.field public static final NOT_FOUND_AVAILABLE_COUNTRY_STR_ID:I = 0xff0f

.field public static final NOT_FOUND_COUNTRY_URL_STR_ID:I = 0xff0e

.field public static final NOT_SUPPORTED_PAYMETHOD_STR_ID:I = 0xff21

.field public static final NOT_SUPPORT_MORE_BUTTON_STR_ID:I = 0xff41

.field public static final NOT_VOUCHER_CODE_STR_ID:I = 0x1b59

.field public static final NO_CREDIT_CARD_INFO:I = 0xbc2

.field public static final NO_CYBERCASH_ACCOUNT_WAS_FOUND:I = 0x157c

.field public static final NO_ITEMS_SELECTED_STR_ID:I = 0xff12

.field public static final NO_SELECT_PAY_METHOD_STR_ID:I = 0xff19

.field public static final NO_SIM_CARD_STR_ID:I = 0xff07

.field public static final OVERSIZE_CONTENTS_LIMIT_STR_ID:I = 0xff03

.field public static final OVER_18_CONTENT_STR_ID:I = 0xff2f

.field public static final OVER_REGISTERED_PREPAID_CARD_STR_ID:I = 0xff1e

.field public static final PAYMENT_BEING_PROCESSED_STR_ID:I = 0x15e2

.field public static final PROCESS_SERVER_ERROR_STR_ID:I = 0x3e8

.field public static final PRODUCT_NOT_EXIST_STORE_STR_ID:I = 0xfa2

.field public static final PROHIBITED_WORD_STR_ID:I = 0xfa1

.field public static final PSMS_ERROR_E_STR_ID:I = 0x1450

.field public static final PSMS_ERROR_S_STR_ID:I = 0x13ed

.field public static final QUESTION_CHANGE_CREDIT_CARD_STR_ID:I = 0xff46

.field public static final QUESTION_DEL_CREDIT_CARD_STR_ID:I = 0xff2b

.field public static final Q_COMMENT_DELETE:I = 0xff45

.field public static final Q_DOWNLOAD_CANCEL_ALL:I = 0xff43

.field public static final Q_REGISTER_IRAN_SHETAB_CARD_STR_ID:I = 0xff3b

.field public static final Q_REMOVE_SNS_ACCOUNT_STR_ID:I = 0xff3e

.field public static final REMOVED_CREATE_A_NEW_SAMSUNG_ACCOUNT:I = 0xbc3

.field public static final REMOVED_TO_WISH_LIST_STR_ID:I = 0xff15

.field public static final RETRY_STR_ID:I = 0xff04

.field public static final RETRY_STR_ID_NO:I = 0xff05

.field public static final SAPPS_IS_NOT_SUPPORTED_IN_THIS_COUNTRY:I = 0x7d1

.field public static final SAPPS_POP_INVALID_DATE_OF_BIRTH:I = 0xbc0

.field public static final SERVICE_SERVER_ERR_STR_ID:I = 0x7d0

.field public static final SERVICE_UNAVAILABLE_STR_ID:I = 0xff0a

.field public static final SIGN_IN_TIME_OUT_STR_ID:I = 0xff17

.field public static final SUCCESS_DEL_CREDIT_CARD_STR_ID:I = 0xff2a

.field public static final SUCCESS_REGISTERED_PREPAID_STR_ID:I = 0xff1b

.field public static final SUCCESS_REGISTER_IRAN_SHETAB_CARD_STR_ID:I = 0xff3d

.field public static final SUCCESS_SEND_AUTH_MSG_STR_ID:I = 0xff24

.field public static final SUCCESS_SENT_STR_ID:I = 0xff28

.field public static final SUCCESS_VERIFY_NAME_STR_ID:I = 0xff1c

.field public static final THE_EXPIRY_DATE_HAS_PASSED:I = 0x1393

.field public static final TRANSMITTED_CARD_BRAND_IS_INVALID:I = 0x1394

.field public static final TRANSMITTED_CARD_NUMBER_IS_INVALID:I = 0x1390

.field public static final UNABLE_TO_DOWNLOAD_TRY_AGAIN_STR_ID:I = 0xff10

.field public static final UNABLE_TO_LAUNCH_APP_STR_ID:I = 0xff1a

.field public static final UNABLE_TO_USE_RESTRICT_AGE_STR_ID:I = 0xff1d

.field public static final UNA_UNINSTALLED_STR_ID:I = 0xff0d

.field public static final USE_FULL_VOUCHER_STR_ID:I = 0xff16

.field public static final YOU_MUST_BE_AT_LEAST_PD_YEARS_OLD:I = 0x1c71

.field static a:Landroid/content/Context;

.field private static b:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->b:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(I)Ljava/lang/String;
    .locals 2

    .prologue
    const v1, 0x7f0802ef

    .line 740
    sparse-switch p0, :sswitch_data_0

    .line 761
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 765
    :goto_0
    return-object v0

    .line 745
    :sswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f08028e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 749
    :sswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 753
    :sswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f0802a1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 757
    :sswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f080235

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 740
    :sswitch_data_0
    .sparse-switch
        0xff03 -> :sswitch_1
        0xff04 -> :sswitch_0
        0xff2b -> :sswitch_2
        0xff46 -> :sswitch_3
    .end sparse-switch
.end method

.method private static a(IZ)Ljava/lang/String;
    .locals 2

    .prologue
    .line 693
    const-string v0, ""

    .line 695
    sparse-switch p0, :sswitch_data_0

    .line 724
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 726
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080276

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 735
    :goto_0
    :sswitch_0
    return-object v0

    .line 716
    :sswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0802a1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 720
    :sswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080235

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 730
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f08027d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 695
    :sswitch_data_0
    .sparse-switch
        0x1f41 -> :sswitch_0
        0xff03 -> :sswitch_0
        0xff06 -> :sswitch_0
        0xff2b -> :sswitch_1
        0xff2d -> :sswitch_0
        0xff2f -> :sswitch_0
        0xff46 -> :sswitch_2
    .end sparse-switch
.end method

.method private static a(Lcom/sec/android/app/samsungapps/NotiDialogObserver;ILjava/lang/String;ZZZI)V
    .locals 5

    .prologue
    const v4, 0x7f08023d

    .line 348
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 349
    if-nez v0, :cond_0

    .line 351
    const-string v0, "NotiDialog::commonDialog::activity is destroyed"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 408
    :goto_0
    return-void

    .line 355
    :cond_0
    if-nez p2, :cond_1

    .line 360
    invoke-static {v0, p1}, Lcom/sec/android/app/samsungapps/NotiDialog;->getMessage(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    .line 368
    :goto_1
    new-instance v2, Lcom/sec/android/app/samsungapps/CommonDialogInterface;

    invoke-direct {v2}, Lcom/sec/android/app/samsungapps/CommonDialogInterface;-><init>()V

    .line 369
    invoke-virtual {v2, p6}, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->setTid(I)V

    .line 370
    invoke-virtual {v2, p3}, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->setFinish(Z)V

    .line 371
    invoke-virtual {v2, p0}, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->setNotiDialogObserver(Lcom/sec/android/app/samsungapps/NotiDialogObserver;)V

    .line 372
    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->setContext(Landroid/app/Activity;)V

    .line 373
    invoke-virtual {v2, p1}, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->setNotiType(I)V

    .line 379
    new-instance v3, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-direct {v3, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;)V

    .line 380
    invoke-virtual {v3, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 381
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setCancelable(Z)V

    .line 382
    invoke-static {p1, p4}, Lcom/sec/android/app/samsungapps/NotiDialog;->a(IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 383
    invoke-static {}, Lcom/sec/android/app/samsungapps/NotiDialog;->getSearchKeyDisabled()Landroid/content/DialogInterface$OnKeyListener;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 388
    const/4 v0, 0x1

    if-ne p5, v0, :cond_2

    .line 390
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/NotiDialog;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 391
    packed-switch p1, :pswitch_data_0

    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v3, v0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setNegativeButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 398
    :goto_3
    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->show()V

    goto :goto_0

    .line 364
    :cond_1
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    .line 365
    invoke-static {p2}, Lcom/sec/android/app/samsungapps/NotiDialog;->getErrCodeMsg(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 391
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 395
    :cond_2
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/NotiDialog;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    goto :goto_3

    .line 391
    :pswitch_data_0
    .packed-switch 0xff03
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static addQueue(Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;)V
    .locals 1

    .prologue
    .line 212
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    if-nez p0, :cond_1

    .line 214
    :cond_0
    const-string v0, "NotiDialog::addQueue::Dialog is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 219
    :goto_0
    return-void

    .line 218
    :cond_1
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static getErrCodeMsg(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 631
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 637
    :cond_0
    const-string v0, "NotiDialog::setErrCodeMsg:: error code is null."

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    .line 638
    const-string p0, "0"

    .line 641
    :cond_1
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/NotiDialog;->getMessage(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 645
    return-object v0
.end method

.method public static getMessage(Landroid/content/Context;I)Ljava/lang/String;
    .locals 9

    .prologue
    const/16 v1, 0x1398

    const/16 v8, 0x12

    const v7, 0x7f0802c7

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 792
    const-string v0, ""

    .line 793
    sget-object v2, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    if-nez v2, :cond_0

    .line 794
    sput-object p0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    .line 800
    :cond_0
    const v2, 0x1869f

    if-le p1, v2, :cond_1

    .line 804
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 805
    const v3, 0x1869f

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 806
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p1

    .line 814
    :cond_1
    :goto_0
    const-string v2, "(%d)"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 817
    if-lt p1, v1, :cond_d

    const/16 v3, 0x13eb

    if-gt p1, v3, :cond_d

    .line 822
    :goto_1
    const/16 v3, 0x13ed

    if-lt v1, v3, :cond_2

    const/16 v3, 0x1450

    if-gt v1, v3, :cond_2

    .line 824
    const/16 v1, 0x13ed

    .line 827
    :cond_2
    sparse-switch v1, :sswitch_data_0

    .line 1398
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f080238

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1402
    :cond_3
    :goto_2
    :sswitch_0
    return-object v0

    .line 810
    :catch_0
    move-exception v2

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "NotiDialog::getMessage::NumberFormatException, "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 830
    :sswitch_1
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 831
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f08011d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 833
    :cond_4
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f08034a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 837
    :sswitch_2
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 838
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f080128

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 840
    :cond_5
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f0801a8

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 845
    :sswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f0802ba

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 848
    :sswitch_4
    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isChina()Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_2

    .line 862
    :sswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f0802cf

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 866
    :sswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f08026b

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 870
    :sswitch_7
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f0801b7

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 874
    :sswitch_8
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f0801b3

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 878
    :sswitch_9
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f0801cd

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 884
    :sswitch_a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f080201

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 888
    :sswitch_b
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f0802fe

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 892
    :sswitch_c
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f0802d9

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 896
    :sswitch_d
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f0802d1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 900
    :sswitch_e
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f080290

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 904
    :sswitch_f
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f0802de

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 908
    :sswitch_10
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f0801be

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 912
    :sswitch_11
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f0802f8

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 916
    :sswitch_12
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f0802d7

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 920
    :sswitch_13
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f0802dc

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 924
    :sswitch_14
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f0802d3

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 928
    :sswitch_15
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f080168

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 944
    :sswitch_16
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f080250

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 947
    :sswitch_17
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f0802c6

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 950
    :sswitch_18
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 951
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f08011b

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 953
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f08024c

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 957
    :sswitch_19
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 958
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f080126

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 960
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f08024d

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 965
    :sswitch_1a
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 966
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f08012c

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 968
    :cond_8
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f08024e

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 973
    :sswitch_1b
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f080251

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 976
    :sswitch_1c
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f080252

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 981
    :sswitch_1d
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    invoke-virtual {v1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 985
    :sswitch_1e
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f08024f

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 989
    :sswitch_1f
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f0802b8

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 994
    :sswitch_20
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f080253

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 998
    :sswitch_21
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f080254

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1003
    :sswitch_22
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f080258

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1007
    :sswitch_23
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f080259

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1011
    :sswitch_24
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f08025a

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1015
    :sswitch_25
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f08025b

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1019
    :sswitch_26
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f0802c9

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1023
    :sswitch_27
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f08025c

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1027
    :sswitch_28
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f08025d

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1031
    :sswitch_29
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f08025e

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1035
    :sswitch_2a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f08025f

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1039
    :sswitch_2b
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f080260

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1043
    :sswitch_2c
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f080261

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1047
    :sswitch_2d
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDeviceInfoLoader()Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->getExtraPhoneType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1054
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f0802df

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1050
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f080306

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1059
    :sswitch_2e
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f0802c3

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1063
    :sswitch_2f
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f080256

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1067
    :sswitch_30
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f080257

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1071
    :sswitch_31
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f0802de

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1075
    :sswitch_32
    const-string v0, "Configuration Exchanged!, Force shut down!!"

    goto/16 :goto_2

    .line 1079
    :sswitch_33
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f080287

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1083
    :sswitch_34
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f0802bf

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1087
    :sswitch_35
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f08031c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1091
    :sswitch_36
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f0802d4

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1095
    :sswitch_37
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f0802d0

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1099
    :sswitch_38
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f0802db

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1103
    :sswitch_39
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f0802c0

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1107
    :sswitch_3a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f0802b9

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1111
    :sswitch_3b
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    invoke-virtual {v1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1115
    :sswitch_3c
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f080266

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1119
    :sswitch_3d
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f0802c4

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1123
    :sswitch_3e
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f080267

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1127
    :sswitch_3f
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f080268

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1131
    :sswitch_40
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f0802e0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1138
    :sswitch_41
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f0802e1

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1142
    :sswitch_42
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f0802d5

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1146
    :sswitch_43
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f080302

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1151
    :sswitch_44
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f0802cc

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1156
    :sswitch_45
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f080300

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1160
    :sswitch_46
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f0802e2

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1164
    :sswitch_47
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f0802ce

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1168
    :sswitch_48
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f0802c5

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1172
    :sswitch_49
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f080255

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1176
    :sswitch_4a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f080269

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1180
    :sswitch_4b
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f080309

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1184
    :sswitch_4c
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f08030a

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1188
    :sswitch_4d
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f0802c8

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1192
    :sswitch_4e
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f08030b

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1196
    :sswitch_4f
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f08030c

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1200
    :sswitch_50
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f080314

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1204
    :sswitch_51
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f080308

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v6, [Ljava/lang/Object;

    const/16 v2, 0xe

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1208
    :sswitch_52
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f080308

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1212
    :sswitch_53
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f080307

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1216
    :sswitch_54
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f080307

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1220
    :sswitch_55
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f0802d8

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1224
    :sswitch_56
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f08020e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1227
    :sswitch_57
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08020e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1231
    :sswitch_58
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f0802a3

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1235
    :sswitch_59
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f080315

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1239
    :sswitch_5a
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isIran()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1241
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f080324

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1245
    :cond_9
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f0802bb

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1250
    :sswitch_5b
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isIran()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1252
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f080328

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1256
    :cond_a
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f080213

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1261
    :sswitch_5c
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f080233

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1265
    :sswitch_5d
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f0802cd

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1269
    :sswitch_5e
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f0802bc

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1273
    :sswitch_5f
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f0802be

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1277
    :sswitch_60
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f080316

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1283
    :sswitch_61
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f0802c2

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1287
    :sswitch_62
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    invoke-virtual {v0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1291
    :sswitch_63
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f0802c1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1295
    :sswitch_64
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f0802d2

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1299
    :sswitch_65
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f080262

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1302
    :sswitch_66
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f0802d5

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1306
    :sswitch_67
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f080263

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1310
    :sswitch_68
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f080264

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1314
    :sswitch_69
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v3, 0x7f080265

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1319
    :sswitch_6a
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f080238

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1323
    :sswitch_6b
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f08032d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1327
    :sswitch_6c
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f08032e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1331
    :sswitch_6d
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1332
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f08011d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1334
    :cond_b
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f08034a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1339
    :sswitch_6e
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f08032f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1343
    :sswitch_6f
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f0802e3

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1347
    :sswitch_70
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f080327

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1351
    :sswitch_71
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f08032b

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1355
    :sswitch_72
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f08032a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1359
    :sswitch_73
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1360
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f08012e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1362
    :cond_c
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f08032c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1367
    :sswitch_74
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v2, 0x7f080332

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/uiutil/StringUtil;->replaceChineseString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1371
    :sswitch_75
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f080334

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1375
    :sswitch_76
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f080333

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1379
    :sswitch_77
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f080335

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1383
    :sswitch_78
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v1, 0x7f080339

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1387
    :sswitch_79
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v2, 0x7f080119

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/uiutil/StringUtil;->replaceChineseString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 1391
    :sswitch_7a
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    sget-object v1, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    const v2, 0x7f0800e7

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/uiutil/StringUtil;->replaceChineseString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    :cond_d
    move v1, p1

    goto/16 :goto_1

    .line 827
    :sswitch_data_0
    .sparse-switch
        0x3ec -> :sswitch_18
        0x3f0 -> :sswitch_19
        0x7d1 -> :sswitch_1a
        0xbb9 -> :sswitch_20
        0xbba -> :sswitch_17
        0xbbb -> :sswitch_1d
        0xbbc -> :sswitch_1e
        0xbbe -> :sswitch_16
        0xbbf -> :sswitch_1d
        0xbc0 -> :sswitch_1b
        0xbc2 -> :sswitch_1f
        0xbc3 -> :sswitch_1c
        0xbc4 -> :sswitch_20
        0xbc5 -> :sswitch_21
        0xfa1 -> :sswitch_38
        0xfa2 -> :sswitch_36
        0xfa5 -> :sswitch_22
        0xfa6 -> :sswitch_8
        0xfa7 -> :sswitch_9
        0xfa9 -> :sswitch_a
        0x1389 -> :sswitch_23
        0x138b -> :sswitch_24
        0x138c -> :sswitch_25
        0x138d -> :sswitch_26
        0x138e -> :sswitch_27
        0x1390 -> :sswitch_28
        0x1391 -> :sswitch_29
        0x1393 -> :sswitch_2a
        0x1394 -> :sswitch_2b
        0x1396 -> :sswitch_2c
        0x1398 -> :sswitch_58
        0x13eb -> :sswitch_58
        0x13ec -> :sswitch_56
        0x13ed -> :sswitch_65
        0x1450 -> :sswitch_66
        0x14b4 -> :sswitch_67
        0x14ba -> :sswitch_57
        0x1518 -> :sswitch_68
        0x157c -> :sswitch_69
        0x1586 -> :sswitch_39
        0x1587 -> :sswitch_3b
        0x1588 -> :sswitch_3c
        0x1589 -> :sswitch_3d
        0x158a -> :sswitch_3e
        0x158b -> :sswitch_3a
        0x1591 -> :sswitch_3f
        0x15e1 -> :sswitch_37
        0x15e2 -> :sswitch_64
        0x16a8 -> :sswitch_4b
        0x16a9 -> :sswitch_4c
        0x16aa -> :sswitch_4d
        0x16ab -> :sswitch_4e
        0x16ac -> :sswitch_4f
        0x16ad -> :sswitch_54
        0x1773 -> :sswitch_49
        0x1b59 -> :sswitch_44
        0x1b5a -> :sswitch_46
        0x1b5b -> :sswitch_44
        0x1b5c -> :sswitch_45
        0x1b5e -> :sswitch_45
        0x1c21 -> :sswitch_41
        0x1c22 -> :sswitch_41
        0x1c25 -> :sswitch_41
        0x1c35 -> :sswitch_41
        0x1c71 -> :sswitch_4a
        0x1f41 -> :sswitch_2f
        0x1f42 -> :sswitch_30
        0xff01 -> :sswitch_1
        0xff02 -> :sswitch_3
        0xff03 -> :sswitch_4
        0xff04 -> :sswitch_5
        0xff06 -> :sswitch_15
        0xff07 -> :sswitch_0
        0xff0b -> :sswitch_3
        0xff0c -> :sswitch_2d
        0xff0d -> :sswitch_2e
        0xff0f -> :sswitch_31
        0xff10 -> :sswitch_11
        0xff11 -> :sswitch_32
        0xff12 -> :sswitch_33
        0xff13 -> :sswitch_6
        0xff14 -> :sswitch_7
        0xff16 -> :sswitch_b
        0xff17 -> :sswitch_c
        0xff18 -> :sswitch_d
        0xff19 -> :sswitch_e
        0xff1a -> :sswitch_f
        0xff1b -> :sswitch_10
        0xff1c -> :sswitch_12
        0xff1d -> :sswitch_13
        0xff1e -> :sswitch_14
        0xff1f -> :sswitch_42
        0xff20 -> :sswitch_43
        0xff21 -> :sswitch_40
        0xff22 -> :sswitch_47
        0xff23 -> :sswitch_48
        0xff24 -> :sswitch_50
        0xff25 -> :sswitch_51
        0xff26 -> :sswitch_52
        0xff27 -> :sswitch_53
        0xff28 -> :sswitch_55
        0xff29 -> :sswitch_59
        0xff2a -> :sswitch_5a
        0xff2b -> :sswitch_5b
        0xff2c -> :sswitch_5d
        0xff2d -> :sswitch_5e
        0xff2e -> :sswitch_5f
        0xff2f -> :sswitch_60
        0xff30 -> :sswitch_61
        0xff31 -> :sswitch_34
        0xff32 -> :sswitch_35
        0xff33 -> :sswitch_62
        0xff34 -> :sswitch_63
        0xff35 -> :sswitch_6a
        0xff36 -> :sswitch_6b
        0xff37 -> :sswitch_6c
        0xff38 -> :sswitch_6d
        0xff39 -> :sswitch_6f
        0xff3a -> :sswitch_6e
        0xff3b -> :sswitch_70
        0xff3c -> :sswitch_71
        0xff3d -> :sswitch_72
        0xff3e -> :sswitch_73
        0xff40 -> :sswitch_74
        0xff41 -> :sswitch_75
        0xff42 -> :sswitch_76
        0xff43 -> :sswitch_77
        0xff44 -> :sswitch_78
        0xff46 -> :sswitch_5c
        0xff47 -> :sswitch_2
        0xff48 -> :sswitch_7a
        0xff49 -> :sswitch_79
    .end sparse-switch

    .line 1047
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static getSearchKeyDisabled()Landroid/content/DialogInterface$OnKeyListener;
    .locals 1

    .prologue
    .line 601
    new-instance v0, Lcom/sec/android/app/samsungapps/fh;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/fh;-><init>()V

    return-object v0
.end method

.method public static onResume()V
    .locals 3

    .prologue
    .line 184
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->b:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 186
    const-string v0, "NotiDialog::onResume::Dialog queue is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 208
    :cond_0
    return-void

    .line 190
    :cond_1
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 196
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 197
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 199
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    .line 201
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->show()V

    .line 203
    sget-object v2, Lcom/sec/android/app/samsungapps/NotiDialog;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static questionDlg(IIZ)V
    .locals 7

    .prologue
    .line 589
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 590
    if-eqz v0, :cond_0

    .line 592
    check-cast v0, Lcom/sec/android/app/samsungapps/NotiDialogObserver;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    move v1, p0

    move v3, p2

    move v6, p1

    invoke-static/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/NotiDialog;->a(Lcom/sec/android/app/samsungapps/NotiDialogObserver;ILjava/lang/String;ZZZI)V

    .line 594
    :cond_0
    return-void
.end method

.method public static questionDlg(Lcom/sec/android/app/samsungapps/NotiDialogObserver;IIZ)V
    .locals 7

    .prologue
    .line 577
    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    sput-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    .line 578
    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move v1, p1

    move v3, p3

    move v6, p2

    invoke-static/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/NotiDialog;->a(Lcom/sec/android/app/samsungapps/NotiDialogObserver;ILjava/lang/String;ZZZI)V

    .line 579
    return-void
.end method

.method public static resErrDlg(Ljava/lang/String;IZ)V
    .locals 7

    .prologue
    .line 416
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 417
    if-eqz v0, :cond_0

    .line 419
    check-cast v0, Lcom/sec/android/app/samsungapps/NotiDialogObserver;

    const/4 v1, -0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v2, p0

    move v3, p2

    move v6, p1

    invoke-static/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/NotiDialog;->a(Lcom/sec/android/app/samsungapps/NotiDialogObserver;ILjava/lang/String;ZZZI)V

    .line 421
    :cond_0
    return-void
.end method

.method public static showDialog(IZZ)V
    .locals 7

    .prologue
    .line 475
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 476
    if-eqz v0, :cond_0

    .line 478
    check-cast v0, Lcom/sec/android/app/samsungapps/NotiDialogObserver;

    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v6, -0x1

    move v1, p0

    move v3, p2

    move v4, p1

    invoke-static/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/NotiDialog;->a(Lcom/sec/android/app/samsungapps/NotiDialogObserver;ILjava/lang/String;ZZZI)V

    .line 480
    :cond_0
    return-void
.end method

.method public static showDialog(Landroid/content/Context;Ljava/lang/String;ZZ)V
    .locals 0

    .prologue
    .line 490
    .line 491
    sput-object p0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    if-eqz p0, :cond_0

    .line 493
    invoke-static {p1, p2, p3}, Lcom/sec/android/app/samsungapps/NotiDialog;->showDialog(Ljava/lang/String;ZZ)V

    .line 495
    :cond_0
    return-void
.end method

.method public static showDialog(Lcom/sec/android/app/samsungapps/NotiDialogObserver;IZZ)V
    .locals 7

    .prologue
    .line 463
    move-object v0, p0

    check-cast v0, Landroid/content/Context;

    sput-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    .line 464
    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v6, -0x1

    move-object v0, p0

    move v1, p1

    move v3, p3

    move v4, p2

    invoke-static/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/NotiDialog;->a(Lcom/sec/android/app/samsungapps/NotiDialogObserver;ILjava/lang/String;ZZZI)V

    .line 465
    return-void
.end method

.method public static showDialog(Ljava/lang/String;ZZ)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 504
    sget-object v0, Lcom/sec/android/app/samsungapps/NotiDialog;->a:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 505
    if-eqz v0, :cond_0

    .line 507
    new-instance v1, Lcom/sec/android/app/samsungapps/CommonDialogInterface;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/CommonDialogInterface;-><init>()V

    .line 508
    invoke-virtual {v1, v3}, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->setTid(I)V

    .line 509
    invoke-virtual {v1, p2}, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->setFinish(Z)V

    .line 510
    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->setContext(Landroid/app/Activity;)V

    .line 515
    new-instance v2, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-direct {v2, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;)V

    .line 516
    invoke-virtual {v2, p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 517
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setCancelable(Z)V

    .line 518
    invoke-static {v3, p1}, Lcom/sec/android/app/samsungapps/NotiDialog;->a(IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 519
    invoke-static {v3}, Lcom/sec/android/app/samsungapps/NotiDialog;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 520
    invoke-static {}, Lcom/sec/android/app/samsungapps/NotiDialog;->getSearchKeyDisabled()Landroid/content/DialogInterface$OnKeyListener;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 522
    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->show()V

    .line 534
    :cond_0
    return-void
.end method
