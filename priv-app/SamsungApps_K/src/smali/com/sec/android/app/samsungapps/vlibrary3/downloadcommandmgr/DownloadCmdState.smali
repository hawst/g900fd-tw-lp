.class public Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;
.source "ProGuard"


# static fields
.field private static a:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState;->a:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;-><init>()V

    .line 37
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState;->a:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState;

    return-object v0
.end method


# virtual methods
.method protected entry(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 2

    .prologue
    .line 74
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/d;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 88
    :goto_0
    :pswitch_0
    return-void

    .line 79
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$Action;->PRE_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 84
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$Action;->START_DOWNLOAD:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 85
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 74
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$Event;)Z
    .locals 2

    .prologue
    .line 42
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/d;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 69
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 44
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/d;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 47
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$State;->PRECHECK:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 50
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$State;->DESTROYED:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 55
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/d;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 58
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$State;->DESTROYED:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 61
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$State;->DOWNLOAD:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 64
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 42
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
    .end packed-switch

    .line 44
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 55
    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public bridge synthetic execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 9
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$Event;

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdState$Event;)Z

    move-result v0

    return v0
.end method

.method protected exit(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 0

    .prologue
    .line 94
    return-void
.end method
