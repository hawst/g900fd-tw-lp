.class public Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _IValidateDisclaimerCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand$IValidateDisclaimerCommandData;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand$IValidateDisclaimerCommandData;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;->_IValidateDisclaimerCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand$IValidateDisclaimerCommandData;

    .line 18
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;Z)V
    .locals 0

    .prologue
    .line 12
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;Z)V
    .locals 0

    .prologue
    .line 12
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;Z)V
    .locals 0

    .prologue
    .line 12
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;->onFinalResult(Z)V

    return-void
.end method

.method private isSkipShowingDisclaimerCondition(Landroid/content/Context;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 52
    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;->noneDisclaimerMode()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;->isAcceptedDisclaimerOfSamsungAccount(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 55
    :cond_1
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private noneDisclaimerMode()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 60
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CSC;->isVZW()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v1

    if-ne v1, v0, :cond_0

    .line 64
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;->isSkipShowingDisclaimerCondition(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 24
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;->onFinalResult(Z)V

    .line 47
    :goto_0
    return-void

    .line 28
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;->_IValidateDisclaimerCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand$IValidateDisclaimerCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand$IValidateDisclaimerCommandData;->needDisclaimerClear()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;->_IValidateDisclaimerCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand$IValidateDisclaimerCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand$IValidateDisclaimerCommandData;->clearDisclaimer()V

    .line 32
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;->_IValidateDisclaimerCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand$IValidateDisclaimerCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand$IValidateDisclaimerCommandData;->isDiscalimerNeverAgreedByUser()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;->_IValidateDisclaimerCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand$IValidateDisclaimerCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand$IValidateDisclaimerCommandData;->needDisclaimerClear()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 34
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;->onRequestDisclaimer()V

    goto :goto_0

    .line 38
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;->_IValidateDisclaimerCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand$IValidateDisclaimerCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand$IValidateDisclaimerCommandData;->existNewDisclaimerVersion()Z

    .line 40
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;->onRequestDisclaimer()V

    goto :goto_0
.end method

.method protected isAcceptedDisclaimerOfSamsungAccount(Landroid/content/Context;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 162
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isExistSamsungAccount(Landroid/content/Context;)Z

    move-result v1

    if-ne v1, v0, :cond_0

    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isRegisteredSamsungAccount(Landroid/content/Context;)Z

    move-result v1

    if-ne v1, v0, :cond_0

    .line 168
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCompareVersion()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;->_IValidateDisclaimerCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand$IValidateDisclaimerCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand$IValidateDisclaimerCommandData;->getOldDisclaimer()Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;->_IValidateDisclaimerCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand$IValidateDisclaimerCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand$IValidateDisclaimerCommandData;->isDiscalimerNeverAgreedByUser()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 110
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;->onNeedToDisplayDisclaimer()V

    .line 140
    :goto_0
    return-void

    .line 113
    :cond_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/e;->a:[I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;->_IValidateDisclaimerCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand$IValidateDisclaimerCommandData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand$IValidateDisclaimerCommandData;->getOldDisclaimer()Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;->_IValidateDisclaimerCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand$IValidateDisclaimerCommandData;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand$IValidateDisclaimerCommandData;->getNewDisclaimerVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;->compareVersion(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 116
    :pswitch_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isTestMode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 118
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;->onFinalResult(Z)V

    goto :goto_0

    .line 123
    :cond_2
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;->_IValidateDisclaimerCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand$IValidateDisclaimerCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand$IValidateDisclaimerCommandData;->existNewDisclaimer()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 125
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;->onNeedToDisplayDisclaimer()V

    goto :goto_0

    .line 129
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;->onRequestDisclaimerToDisplay()V

    goto :goto_0

    .line 136
    :pswitch_2
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;->onFinalResult(Z)V

    goto :goto_0

    .line 113
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method protected onNeedToDisplayDisclaimer()V
    .locals 3

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;->_IValidateDisclaimerCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand$IValidateDisclaimerCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand$IValidateDisclaimerCommandData;->askUserToAcceptDisclaimerCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    .line 144
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/d;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 151
    return-void
.end method

.method protected onRequestDisclaimer()V
    .locals 3

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;->_IValidateDisclaimerCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand$IValidateDisclaimerCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand$IValidateDisclaimerCommandData;->requestDisclaimerCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/b;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 85
    return-void
.end method

.method protected onRequestDisclaimerToDisplay()V
    .locals 3

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;->_IValidateDisclaimerCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand$IValidateDisclaimerCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand$IValidateDisclaimerCommandData;->requestDisclaimerCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/c;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 104
    return-void
.end method
