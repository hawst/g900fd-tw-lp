.class public abstract Lcom/sec/android/app/samsungapps/vlibrary/net/Request;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;


# instance fields
.field protected mErrorPreProcessor:Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;

.field mReceiver:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/Request;->mReceiver:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    .line 5
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/Request;->mErrorPreProcessor:Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;

    .line 9
    return-void
.end method


# virtual methods
.method public abstract cancel()Z
.end method

.method public getThreadNo()I
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    return v0
.end method

.method public abstract getURL()Ljava/lang/String;
.end method

.method protected hasErrorPreProcessor()Z
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/Request;->mErrorPreProcessor:Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract isCanceled()Z
.end method

.method public isSecure()Z
    .locals 1

    .prologue
    .line 14
    const/4 v0, 0x0

    return v0
.end method

.method public abstract isSucceed()Z
.end method

.method public abstract needRetry()Z
.end method

.method public notify(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/Request;->mReceiver:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    if-eqz v0, :cond_0

    .line 24
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/Request;->mReceiver:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    invoke-interface {v0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;->onReceiveResult(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V

    .line 26
    :cond_0
    return-void
.end method

.method public abstract send(Lcom/sec/android/app/samsungapps/vlibrary/net/INetAPI;)V
.end method

.method public setErrorPreprocessor(Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;)V
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/Request;->mErrorPreProcessor:Lcom/sec/android/app/samsungapps/vlibrary/net/ErrorPreProcessor;

    .line 41
    return-void
.end method

.method public setNetResultReceiver(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V
    .locals 0

    .prologue
    .line 18
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/Request;->mReceiver:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    .line 19
    return-void
.end method
