.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContent;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract getDetailOverview()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;
.end method

.method public abstract getDetailRelated()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailRelated;
.end method

.method public abstract getVGUID()Ljava/lang/String;
.end method

.method public abstract getVaverageRating()F
.end method

.method public abstract getVcategoryID()Ljava/lang/String;
.end method

.method public abstract getVcategoryID2()Ljava/lang/String;
.end method

.method public abstract getVcategoryName()Ljava/lang/String;
.end method

.method public abstract getVcategoryName2()Ljava/lang/String;
.end method

.method public abstract getVcontentType()Ljava/lang/String;
.end method

.method public abstract getVcurrencyUnit()Ljava/lang/String;
.end method

.method public abstract getVdiscountFlag()Z
.end method

.method public abstract getVdiscountPrice()D
.end method

.method public abstract getVprice()D
.end method

.method public abstract getVproductID()Ljava/lang/String;
.end method

.method public abstract getVproductImgUrl()Ljava/lang/String;
.end method

.method public abstract getVproductName()Ljava/lang/String;
.end method

.method public abstract isLinkApp()Z
.end method
