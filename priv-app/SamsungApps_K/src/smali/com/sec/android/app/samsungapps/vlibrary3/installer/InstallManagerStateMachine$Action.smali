.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum CANCEL_B_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

.field public static final enum CHECK_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

.field public static final enum CMD_PREPARE_B:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

.field public static final enum DELETE_FILE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

.field public static final enum RELEASE_LOCK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

.field public static final enum REQUEST_B_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

.field public static final enum REQUEST_LOCK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

.field public static final enum REQ_FOREGROUD_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

.field public static final enum REQ_KNOX_INTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

.field public static final enum REQ_SILENCE_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

.field public static final enum SIG_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

.field public static final enum SIG_FAILED_WITH_RETURNCODE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

.field public static final enum SIG_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 28
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    const-string v1, "REQ_FOREGROUD_INSTALL"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->REQ_FOREGROUD_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    const-string v1, "REQ_KNOX_INTALL"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->REQ_KNOX_INTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    const-string v1, "REQ_SILENCE_INSTALL"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->REQ_SILENCE_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    const-string v1, "SIG_SUCCESS"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->SIG_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    const-string v1, "SIG_FAILED_WITH_RETURNCODE"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->SIG_FAILED_WITH_RETURNCODE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    const-string v1, "DELETE_FILE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->DELETE_FILE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    const-string v1, "SIG_FAILED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->SIG_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    const-string v1, "REQUEST_B_INSTALL"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->REQUEST_B_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    const-string v1, "CMD_PREPARE_B"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->CMD_PREPARE_B:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    const-string v1, "CANCEL_B_INSTALL"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->CANCEL_B_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    const-string v1, "RELEASE_LOCK"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->RELEASE_LOCK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    const-string v1, "REQUEST_LOCK"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->REQUEST_LOCK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    const-string v1, "CHECK_INSTALL"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->CHECK_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    .line 26
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->REQ_FOREGROUD_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->REQ_KNOX_INTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->REQ_SILENCE_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->SIG_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->SIG_FAILED_WITH_RETURNCODE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->DELETE_FILE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->SIG_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->REQUEST_B_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->CMD_PREPARE_B:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->CANCEL_B_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->RELEASE_LOCK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->REQUEST_LOCK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->CHECK_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    return-object v0
.end method
