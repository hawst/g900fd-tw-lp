.class public Lcom/sec/android/app/samsungapps/view/CommonAdapter;
.super Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue$DLStateQueueObserver;


# instance fields
.field a:Lcom/sec/android/app/samsungapps/widget/interfaces/ICommonListRequest;

.field final b:Ljava/lang/String;

.field final c:Ljava/lang/String;

.field d:Z

.field private e:Ljava/util/ArrayList;

.field private f:Ljava/util/ArrayList;

.field private g:Z

.field private h:I

.field private i:Z

.field private j:I


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 36
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;-><init>(Landroid/content/Context;II)V

    .line 21
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->e:Ljava/util/ArrayList;

    .line 22
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->f:Ljava/util/ArrayList;

    .line 27
    iput v1, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->j:I

    .line 65
    const-string v0, "Junos Pulse for Galaxy S and Tab 7.0"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->b:Ljava/lang/String;

    .line 67
    const-string v0, "Junos Pulse"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->c:Ljava/lang/String;

    .line 69
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->d:Z

    .line 37
    new-instance v0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->_ListViewInfo:Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;

    .line 38
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->addObserver(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue$DLStateQueueObserver;)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;I)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 42
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;I)V

    .line 21
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->e:Ljava/util/ArrayList;

    .line 22
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->f:Ljava/util/ArrayList;

    .line 27
    iput v1, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->j:I

    .line 65
    const-string v0, "Junos Pulse for Galaxy S and Tab 7.0"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->b:Ljava/lang/String;

    .line 67
    const-string v0, "Junos Pulse"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->c:Ljava/lang/String;

    .line 69
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->d:Z

    .line 43
    new-instance v0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->_ListViewInfo:Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;

    .line 44
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->addObserver(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue$DLStateQueueObserver;)V

    .line 45
    return-void
.end method

.method public static create(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/view/CommonAdapter;
    .locals 3

    .prologue
    .line 51
    new-instance v0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;

    const v1, 0x7f04004d

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/samsungapps/view/CommonAdapter;-><init>(Landroid/content/Context;II)V

    .line 52
    return-object v0
.end method

.method public static createPurchasedList(Landroid/content/Context;Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter$UpdateAllListener;Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;)Lcom/sec/android/app/samsungapps/view/CommonAdapter;
    .locals 2

    .prologue
    .line 56
    new-instance v0, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    const v1, 0x7f0400b9

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;-><init>(Landroid/content/Context;ILcom/sec/android/app/samsungapps/view/PurchasedListAdapter$UpdateAllListener;Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;)V

    .line 57
    return-object v0
.end method

.method public static createUncList(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/view/CommonAdapter;
    .locals 3

    .prologue
    .line 61
    new-instance v0, Lcom/sec/android/app/samsungapps/view/UncListAdapter;

    const v1, 0x7f040010

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/samsungapps/view/UncListAdapter;-><init>(Landroid/content/Context;II)V

    .line 62
    return-object v0
.end method


# virtual methods
.method public enterDeletionMode(I)V
    .locals 0

    .prologue
    .line 203
    iput p1, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->j:I

    .line 204
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->notifyDataSetChanged()V

    .line 205
    return-void
.end method

.method public getIndexOfFirstItem()I
    .locals 1

    .prologue
    .line 140
    const/4 v0, 0x0

    return v0
.end method

.method public getListRequest()Lcom/sec/android/app/samsungapps/widget/interfaces/ICommonListRequest;
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->a:Lcom/sec/android/app/samsungapps/widget/interfaces/ICommonListRequest;

    return-object v0
.end method

.method public getTotalListCount()I
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->e:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 281
    const/4 v0, 0x0

    .line 283
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getUpdatableListCount()I
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->f:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 289
    const/4 v0, 0x0

    .line 291
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public isCheckDeletionMode()Z
    .locals 1

    .prologue
    .line 173
    iget v0, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->j:I

    packed-switch v0, :pswitch_data_0

    .line 181
    const/4 v0, 0x0

    .line 185
    :goto_0
    return v0

    .line 177
    :pswitch_0
    const/4 v0, 0x1

    .line 178
    goto :goto_0

    .line 173
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public isCompleteEmptyList()Z
    .locals 1

    .prologue
    .line 165
    const/4 v0, 0x0

    return v0
.end method

.method public isDeleteMode()Z
    .locals 1

    .prologue
    .line 169
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->g:Z

    return v0
.end method

.method public isListItemMode()I
    .locals 1

    .prologue
    .line 194
    iget v0, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->j:I

    return v0
.end method

.method public isLoading()Z
    .locals 1

    .prologue
    .line 145
    const/4 v0, 0x0

    return v0
.end method

.method public isSearchList()Z
    .locals 1

    .prologue
    .line 160
    const/4 v0, 0x0

    return v0
.end method

.method public onDLStateAdded(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V
    .locals 0

    .prologue
    .line 150
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->notifyDataSetChanged()V

    .line 151
    return-void
.end method

.method public onDLStateRemoved(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V
    .locals 0

    .prologue
    .line 155
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->notifyDataSetChanged()V

    .line 156
    return-void
.end method

.method public refreshUpdateAllMenu(I)V
    .locals 0

    .prologue
    .line 48
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 131
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->e:Ljava/util/ArrayList;

    .line 132
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->f:Ljava/util/ArrayList;

    .line 134
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->release()V

    .line 135
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->removeObserver(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue$DLStateQueueObserver;)V

    .line 136
    return-void
.end method

.method public requestMoreData()V
    .locals 0

    .prologue
    .line 277
    return-void
.end method

.method public setContentList(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 254
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->e:Ljava/util/ArrayList;

    .line 255
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->f:Ljava/util/ArrayList;

    .line 256
    return-void
.end method

.method public setIndexOfFirstItem(I)V
    .locals 0

    .prologue
    .line 125
    return-void
.end method

.method public setListCount(I)V
    .locals 0

    .prologue
    .line 259
    iput p1, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->h:I

    .line 260
    return-void
.end method

.method public setListEOF(Z)V
    .locals 0

    .prologue
    .line 263
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->i:Z

    .line 264
    return-void
.end method

.method public setListRequest(Lcom/sec/android/app/samsungapps/widget/interfaces/ICommonListRequest;)V
    .locals 0

    .prologue
    .line 267
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->a:Lcom/sec/android/app/samsungapps/widget/interfaces/ICommonListRequest;

    .line 268
    return-void
.end method

.method public updateView()V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 99
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->e:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_3

    .line 100
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->getCount()I

    move-result v2

    .line 101
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->e:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 102
    iget-boolean v4, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->d:Z

    if-eqz v4, :cond_1

    :goto_0
    add-int/2addr v0, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_4

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 104
    if-eqz v0, :cond_2

    sget-object v2, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;->isRemoveContent(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 105
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->add(Ljava/lang/Object;)V

    .line 102
    :cond_0
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0

    .line 107
    :cond_2
    if-eqz v0, :cond_0

    .line 108
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "JunosPulse checked : "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->i(Ljava/lang/String;)V

    goto :goto_2

    .line 121
    :catch_0
    move-exception v0

    :cond_3
    :goto_3
    return-void

    .line 112
    :cond_4
    iget v0, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->h:I

    if-le v0, v3, :cond_5

    .line 113
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->setHasLoading(Z)V

    goto :goto_3

    .line 115
    :cond_5
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->setHasLoading(Z)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3
.end method

.method public updateViewEOF()V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 73
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->e:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_3

    .line 74
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->getCount()I

    move-result v2

    .line 75
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->e:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 76
    iget-boolean v4, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->d:Z

    if-eqz v4, :cond_1

    :goto_0
    add-int/2addr v0, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_4

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 78
    if-eqz v0, :cond_2

    sget-object v2, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;->isRemoveContent(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 79
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->add(Ljava/lang/Object;)V

    .line 76
    :cond_0
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0

    .line 81
    :cond_2
    if-eqz v0, :cond_0

    .line 82
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "JunosPulse checked : "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->i(Ljava/lang/String;)V

    goto :goto_2

    .line 95
    :catch_0
    move-exception v0

    :cond_3
    :goto_3
    return-void

    .line 86
    :cond_4
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->i:Z

    if-nez v0, :cond_5

    .line 87
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->setHasLoading(Z)V

    goto :goto_3

    .line 89
    :cond_5
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->setHasLoading(Z)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3
.end method

.method public updatesListView()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 232
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_3

    .line 233
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->getCount()I

    move-result v1

    .line 234
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->f:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 235
    iget-boolean v3, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->d:Z

    if-eqz v3, :cond_0

    const/4 v0, 0x1

    :cond_0
    add-int/2addr v0, v1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_4

    .line 236
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 237
    if-eqz v0, :cond_2

    sget-object v3, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;->isRemoveContent(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 238
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->add(Ljava/lang/Object;)V

    .line 235
    :cond_1
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 240
    :cond_2
    if-eqz v0, :cond_1

    .line 241
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "JunosPulse checked : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->i(Ljava/lang/String;)V

    goto :goto_1

    .line 251
    :catch_0
    move-exception v0

    :cond_3
    :goto_2
    return-void

    .line 245
    :cond_4
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->setHasLoading(Z)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2
.end method

.method public updatesListViewEOF()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 209
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_3

    .line 210
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->getCount()I

    move-result v1

    .line 211
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->f:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 212
    iget-boolean v3, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->d:Z

    if-eqz v3, :cond_0

    const/4 v0, 0x1

    :cond_0
    add-int/2addr v0, v1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_4

    .line 213
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 214
    if-eqz v0, :cond_2

    sget-object v3, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;->isRemoveContent(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 215
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->add(Ljava/lang/Object;)V

    .line 212
    :cond_1
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 217
    :cond_2
    if-eqz v0, :cond_1

    .line 218
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "JunosPulse checked : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->i(Ljava/lang/String;)V

    goto :goto_1

    .line 228
    :catch_0
    move-exception v0

    :cond_3
    :goto_2
    return-void

    .line 222
    :cond_4
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/CommonAdapter;->setHasLoading(Z)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2
.end method
