.class final Lcom/sec/android/app/samsungapps/hq;
.super Landroid/content/BroadcastReceiver;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/SMSManager;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/SMSManager;)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/hq;->a:Lcom/sec/android/app/samsungapps/SMSManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 145
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 146
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/hq;->a:Lcom/sec/android/app/samsungapps/SMSManager;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/SMSManager;->a:Landroid/content/Context;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/hq;->a:Lcom/sec/android/app/samsungapps/SMSManager;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/SMSManager;->i:Lcom/sec/android/app/samsungapps/SMSManager$Listener;

    if-nez v1, :cond_1

    .line 169
    :cond_0
    :goto_0
    return-void

    .line 151
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/hq;->a:Lcom/sec/android/app/samsungapps/SMSManager;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/SMSManager;->getRequestCode()I

    move-result v1

    .line 152
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/hq;->getResultCode()I

    move-result v2

    .line 154
    const-string v3, "ACTION_MSG_SENT"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eq v3, v4, :cond_2

    const-string v3, "android.provider.Telephony.SMS_RECEIVED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v4, :cond_0

    .line 157
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/hq;->a:Lcom/sec/android/app/samsungapps/SMSManager;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/SMSManager;->i:Lcom/sec/android/app/samsungapps/SMSManager$Listener;

    invoke-interface {v0, p1, p2, v1, v2}, Lcom/sec/android/app/samsungapps/SMSManager$Listener;->onSendResult(Landroid/content/Context;Landroid/content/Intent;II)V

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/hq;->a:Lcom/sec/android/app/samsungapps/SMSManager;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/SMSManager;->f:Landroid/app/PendingIntent;

    if-eqz v0, :cond_3

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/hq;->a:Lcom/sec/android/app/samsungapps/SMSManager;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/SMSManager;->f:Landroid/app/PendingIntent;

    invoke-virtual {v0}, Landroid/app/PendingIntent;->cancel()V

    .line 163
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/hq;->a:Lcom/sec/android/app/samsungapps/SMSManager;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/SMSManager;->h:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/hq;->a:Lcom/sec/android/app/samsungapps/SMSManager;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/SMSManager;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/hq;->a:Lcom/sec/android/app/samsungapps/SMSManager;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/SMSManager;->h:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/hq;->a:Lcom/sec/android/app/samsungapps/SMSManager;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/sec/android/app/samsungapps/SMSManager;->h:Landroid/content/BroadcastReceiver;

    goto :goto_0
.end method
