.class public Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;
.super Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;
.source "ProGuard"


# instance fields
.field a:Landroid/widget/ImageView;

.field b:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;Landroid/content/Context;Landroid/view/View;Landroid/graphics/drawable/Drawable;Ljava/lang/String;ZILcom/sec/android/app/samsungapps/view/IContentArrayAdapter;)V
    .locals 3

    .prologue
    .line 36
    invoke-direct/range {p0 .. p8}, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;-><init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;Landroid/content/Context;Landroid/view/View;Landroid/graphics/drawable/Drawable;Ljava/lang/String;ZILcom/sec/android/app/samsungapps/view/IContentArrayAdapter;)V

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;->b:Z

    .line 38
    const v0, 0x7f0c0191

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;->a:Landroid/widget/ImageView;

    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;->webimage:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    if-eqz v0, :cond_0

    .line 40
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;->webimage:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    const-string v1, "isa_samsungapps_default"

    const-string v2, "drawable"

    invoke-static {p2, v1, v2}, Lcom/sec/android/app/samsungapps/uiutil/KnoxResourceManager;->findResource(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setBackgroundResource(I)V

    .line 42
    :cond_0
    return-void
.end method


# virtual methods
.method public displayNormalItem()V
    .locals 0

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;->showProductImg()V

    .line 121
    return-void
.end method

.method public getRoundedCornerBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    const/high16 v7, 0x41700000    # 15.0f

    const/4 v6, 0x0

    .line 130
    if-nez p1, :cond_0

    .line 132
    const-string v0, "BigBannerContentHolder::getRoundedCornerBitmap:: bitmap is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->err(Ljava/lang/String;)V

    .line 133
    const/4 v0, 0x0

    .line 153
    :goto_0
    return-object v0

    .line 136
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 137
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 139
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 141
    new-instance v3, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-direct {v3, v6, v6, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 142
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4, v3}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 143
    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 146
    invoke-virtual {v1, v6, v6, v6, v6}, Landroid/graphics/Canvas;->drawARGB(IIII)V

    .line 147
    const v5, -0xbdbdbe

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 148
    invoke-virtual {v1, v4, v7, v7, v2}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 150
    new-instance v4, Landroid/graphics/PorterDuffXfermode;

    sget-object v5, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v4, v5}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 151
    invoke-virtual {v1, p1, v3, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method protected prepareView()V
    .locals 0

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;->displayNormalItem()V

    .line 126
    return-void
.end method

.method public setContent(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;)V
    .locals 1

    .prologue
    .line 46
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 47
    iget-object v0, p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->productID:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 48
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;->prepareView()V

    .line 52
    :cond_0
    return-void
.end method

.method public setHovering(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 55
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;->b:Z

    if-nez v0, :cond_0

    .line 56
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;->b:Z

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;->_Context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.feature.hovering_ui"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-ne v0, v2, :cond_0

    .line 58
    new-instance v0, Lcom/sec/android/app/samsungapps/viewholder/a;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/viewholder/a;-><init>(Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 84
    :cond_0
    return-void
.end method

.method public showNoHaveImg()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;->webimage:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setClickable(Z)V

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;->webimage:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setFocusable(Z)V

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 116
    return-void
.end method

.method public showProductImg()V
    .locals 4

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;->webimage:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    if-eqz v0, :cond_1

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;->webimage:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 90
    instance-of v1, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_0

    .line 91
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 92
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 93
    if-eqz v0, :cond_0

    .line 94
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;->getRoundedCornerBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 95
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;->webimage:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;->_Context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;->webimage:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;->_Context:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getNetAPI()Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setNetAPI(Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;)V

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;->webimage:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    new-instance v1, Lcom/sec/android/app/samsungapps/viewholder/b;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/viewholder/b;-><init>(Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setConverter(Lcom/sec/android/app/samsungapps/view/CacheWebImageView$BitmapConverter;)V

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;->webimage:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductImageURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setURL(Ljava/lang/String;)V

    .line 109
    :cond_1
    return-void
.end method
