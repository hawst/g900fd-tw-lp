.class public Lcom/sec/android/app/samsungapps/view/CacheWebImageView;
.super Lcom/sec/android/app/samsungapps/view/WebImageView;
.source "ProGuard"


# static fields
.field private static i:Lcom/sec/android/app/samsungapps/view/ImageCache;


# instance fields
.field private j:Lcom/sec/android/app/samsungapps/view/CacheWebImageView$BitmapConverter;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/view/WebImageView;-><init>(Landroid/content/Context;)V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->j:Lcom/sec/android/app/samsungapps/view/CacheWebImageView$BitmapConverter;

    .line 55
    invoke-static {}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->a()V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/view/WebImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->j:Lcom/sec/android/app/samsungapps/view/CacheWebImageView$BitmapConverter;

    .line 60
    invoke-static {}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->a()V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/view/WebImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->j:Lcom/sec/android/app/samsungapps/view/CacheWebImageView$BitmapConverter;

    .line 65
    invoke-static {}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->a()V

    .line 66
    return-void
.end method

.method private static a()V
    .locals 2

    .prologue
    .line 48
    sget-object v0, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->i:Lcom/sec/android/app/samsungapps/view/ImageCache;

    if-nez v0, :cond_0

    .line 49
    new-instance v0, Lcom/sec/android/app/samsungapps/view/ImageCache;

    const/16 v1, 0xc8

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/view/ImageCache;-><init>(I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->i:Lcom/sec/android/app/samsungapps/view/ImageCache;

    .line 51
    :cond_0
    return-void
.end method

.method public static clearCache()V
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->i:Lcom/sec/android/app/samsungapps/view/ImageCache;

    if-eqz v0, :cond_0

    .line 28
    sget-object v0, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->i:Lcom/sec/android/app/samsungapps/view/ImageCache;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ImageCache;->clearCache()V

    .line 30
    :cond_0
    return-void
.end method


# virtual methods
.method protected applyBitmap(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 35
    const/4 v0, 0x0

    .line 37
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->j:Lcom/sec/android/app/samsungapps/view/CacheWebImageView$BitmapConverter;

    if-eqz v1, :cond_0

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->j:Lcom/sec/android/app/samsungapps/view/CacheWebImageView$BitmapConverter;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView$BitmapConverter;->postProcess(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 41
    :cond_0
    sget-object v1, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->i:Lcom/sec/android/app/samsungapps/view/ImageCache;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/app/samsungapps/view/ImageCache;->setBitmap(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    .line 43
    if-nez v0, :cond_1

    :goto_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setImageLoadedUsingAnimation(Landroid/graphics/Bitmap;)V

    .line 44
    return-void

    :cond_1
    move-object p1, v0

    .line 43
    goto :goto_0
.end method

.method protected requestImage(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 72
    sget-object v0, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->i:Lcom/sec/android/app/samsungapps/view/ImageCache;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ImageCache;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 74
    sget-object v1, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->i:Lcom/sec/android/app/samsungapps/view/ImageCache;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/view/ImageCache;->findCacheBitmap(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    .line 76
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->j:Lcom/sec/android/app/samsungapps/view/CacheWebImageView$BitmapConverter;

    if-eqz v1, :cond_0

    .line 78
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->j:Lcom/sec/android/app/samsungapps/view/CacheWebImageView$BitmapConverter;

    invoke-interface {v1, v0}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView$BitmapConverter;->postProcess(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 83
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setImage(Landroid/graphics/Bitmap;)V

    .line 85
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->c:Z

    .line 87
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->f:Z

    if-eqz v0, :cond_1

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->mContext:Landroid/content/Context;

    const v1, 0x7f050001

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 89
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 95
    :cond_1
    :goto_0
    return-void

    .line 94
    :cond_2
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/view/WebImageView;->requestImage(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setConverter(Lcom/sec/android/app/samsungapps/view/CacheWebImageView$BitmapConverter;)V
    .locals 0

    .prologue
    .line 23
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->j:Lcom/sec/android/app/samsungapps/view/CacheWebImageView$BitmapConverter;

    .line 24
    return-void
.end method
