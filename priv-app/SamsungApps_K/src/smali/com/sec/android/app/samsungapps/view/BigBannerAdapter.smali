.class public Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ProGuard"


# instance fields
.field a:I

.field b:Landroid/view/LayoutInflater;

.field private c:Landroid/content/Context;

.field private d:Ljava/util/ArrayList;

.field private e:Ljava/util/List;

.field public mWidgetClickListener:Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetClickListener;

.field public mWidgetData:Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetData;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->e:Ljava/util/List;

    .line 38
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->b:Landroid/view/LayoutInflater;

    .line 39
    iput p2, p0, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->a:I

    .line 40
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->d:Ljava/util/ArrayList;

    .line 41
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->c:Landroid/content/Context;

    .line 43
    return-void
.end method

.method private a()V
    .locals 10

    .prologue
    const v9, 0x7f0c018f

    .line 68
    const/4 v0, 0x0

    .line 69
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;

    .line 70
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->b:Landroid/view/LayoutInflater;

    iget v3, p0, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->a:I

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 71
    const v1, 0x7f0c0193

    invoke-virtual {v5, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;

    .line 72
    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->setBanner(Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;)V

    .line 73
    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->setBannerId(I)V

    .line 74
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->mWidgetData:Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetData;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->setBannerWidgetData(Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetData;)V

    .line 75
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->mWidgetClickListener:Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetClickListener;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->setWidgetClickListener(Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetClickListener;)V

    .line 77
    add-int/lit8 v3, v2, 0x1

    .line 79
    invoke-virtual {v5, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    .line 80
    if-eqz v2, :cond_0

    .line 81
    iget-object v6, p0, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->c:Landroid/content/Context;

    const-string v7, "isa_special_banner_default"

    const-string v8, "drawable"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/samsungapps/uiutil/KnoxResourceManager;->findResource(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v2, v6}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setImageResource(I)V

    .line 83
    :cond_0
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->getURL()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 84
    iget-object v6, p0, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->c:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getNetAPI()Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setNetAPI(Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;)V

    .line 85
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->getURL()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setURL(Ljava/lang/String;)V

    .line 86
    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 89
    :cond_1
    invoke-virtual {v5, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 90
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->getBanner()Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->getTitle()Ljava/lang/String;

    move-result-object v2

    .line 91
    if-eqz v2, :cond_2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 92
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->c:Landroid/content/Context;

    const v6, 0x7f080110

    invoke-virtual {v2, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 95
    :cond_3
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 96
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->getBannerOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->e:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v2, v3

    .line 98
    goto/16 :goto_0

    .line 99
    :cond_4
    return-void
.end method


# virtual methods
.method public calcPosition(I)I
    .locals 3

    .prologue
    .line 144
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->e:Ljava/util/List;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->isNull([Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 145
    :cond_0
    const/4 v0, -0x1

    .line 147
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    rem-int v0, p1, v0

    goto :goto_0
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->d:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 104
    const/4 v0, 0x0

    .line 111
    :goto_0
    return v0

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x2

    if-le v0, v1, :cond_1

    .line 108
    const v0, 0x7fffffff

    goto :goto_0

    .line 111
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->d:Ljava/util/ArrayList;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->calcPosition(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->getItem(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 119
    int-to-long v0, p1

    return-wide v0
.end method

.method public getReadAllViews()Ljava/util/List;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->e:Ljava/util/List;

    return-object v0
.end method

.method public getReadCount()I
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getReadView(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 136
    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 123
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->calcPosition(I)I

    move-result v0

    if-ltz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->e:Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->calcPosition(I)I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 126
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/view/View;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public init()V
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 48
    if-nez v0, :cond_1

    .line 64
    :cond_0
    :goto_0
    return-void

    .line 59
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->a()V

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 62
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->a()V

    goto :goto_0
.end method

.method public release()V
    .locals 4

    .prologue
    .line 226
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->e:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 227
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    .line 229
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_2

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->e:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 231
    if-eqz v0, :cond_1

    .line 232
    const v1, 0x7f0c0193

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;

    .line 234
    if-eqz v1, :cond_0

    .line 235
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/banner/BigBannerItemWidget;->release()V

    .line 238
    :cond_0
    check-cast v0, Landroid/view/ViewGroup;

    .line 239
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 229
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 242
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 243
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->e:Ljava/util/List;

    .line 245
    :cond_3
    return-void
.end method

.method public setBannerWidgetData(Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetData;)V
    .locals 0

    .prologue
    .line 222
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->mWidgetData:Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetData;

    .line 223
    return-void
.end method

.method public setWidgetClickListener(Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetClickListener;)V
    .locals 0

    .prologue
    .line 218
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/BigBannerAdapter;->mWidgetClickListener:Lcom/sec/android/app/samsungapps/widget/interfaces/IBigBannerWidgetClickListener;

    .line 219
    return-void
.end method
