.class public Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManagerStateMachine;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;
.source "ProGuard"


# static fields
.field private static a:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManagerStateMachine;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;-><init>()V

    .line 32
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManagerStateMachine;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManagerStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManagerStateMachine;

    if-nez v0, :cond_0

    .line 16
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManagerStateMachine;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManagerStateMachine;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManagerStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManagerStateMachine;

    .line 18
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManagerStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManagerStateMachine;

    return-object v0
.end method


# virtual methods
.method protected entry(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 3

    .prologue
    .line 74
    const-string v0, "PurchaseManagerStateMachine"

    const-string v1, "entry"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManagerStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 75
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/d;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 95
    :goto_0
    :pswitch_0
    return-void

    .line 80
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Action;->CHECK_EXIST_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 83
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Action;->REQUEST_PAYMENT:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 86
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Action;->REQUEST_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 89
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 92
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Action;->NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 75
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;)Z
    .locals 3

    .prologue
    .line 24
    const-string v0, "PurchaseManagerStateMachine"

    const-string v1, "execute"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManagerStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 25
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/d;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 69
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 28
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/d;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 31
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$State;->PERMISSION_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 36
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/d;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 39
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$State;->REQUEST_PAYMENT:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 42
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$State;->REQUEST_PERMISSION:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 47
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/d;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    goto :goto_0

    .line 50
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 53
    :pswitch_7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 58
    :pswitch_8
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/d;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_4

    goto :goto_0

    .line 61
    :pswitch_9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$State;->REQUEST_PAYMENT:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 64
    :pswitch_a
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 25
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_5
        :pswitch_8
    .end packed-switch

    .line 28
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch

    .line 36
    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 47
    :pswitch_data_3
    .packed-switch 0x4
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 58
    :pswitch_data_4
    .packed-switch 0x6
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public bridge synthetic execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 9
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManagerStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/IPurchaseState$Event;)Z

    move-result v0

    return v0
.end method

.method protected exit(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 3

    .prologue
    .line 99
    const-string v0, "PurchaseManagerStateMachine"

    const-string v1, "exit"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/purchasemanager/PurchaseManagerStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 100
    return-void
.end method
