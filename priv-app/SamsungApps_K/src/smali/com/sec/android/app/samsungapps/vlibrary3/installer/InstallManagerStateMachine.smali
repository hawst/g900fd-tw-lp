.class public Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;
.source "ProGuard"


# static fields
.field private static a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;-><init>()V

    .line 37
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine;

    if-nez v0, :cond_0

    .line 43
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine;

    .line 45
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine;

    return-object v0
.end method


# virtual methods
.method protected entry(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 3

    .prologue
    .line 160
    const-string v0, "InstallManagerStateMachine"

    const-string v1, "entry"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 161
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/w;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 202
    :goto_0
    :pswitch_0
    return-void

    .line 163
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->REQUEST_LOCK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 166
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->REQ_FOREGROUD_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 167
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->RELEASE_LOCK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 172
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->REQ_KNOX_INTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 175
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->REQ_SILENCE_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 178
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->DELETE_FILE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 179
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->SIG_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 180
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->RELEASE_LOCK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 183
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->DELETE_FILE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 184
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->SIG_FAILED_WITH_RETURNCODE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 185
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->RELEASE_LOCK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 188
    :pswitch_7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->DELETE_FILE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 189
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->SIG_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 190
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->RELEASE_LOCK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 193
    :pswitch_8
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->REQUEST_B_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 196
    :pswitch_9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->CMD_PREPARE_B:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 199
    :pswitch_a
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->CHECK_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 161
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_a
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_8
        :pswitch_9
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;)Z
    .locals 3

    .prologue
    .line 51
    const-string v0, "InstallManagerStateMachine"

    const-string v1, "execute"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 52
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/w;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 155
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 55
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/w;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 58
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->REQUEST_LOCK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 63
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/w;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 66
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->CHECK_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 71
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/w;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    goto :goto_0

    .line 74
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->KNOX_INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 77
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->FOREGROUND_INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 80
    :pswitch_7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->SILENCE_INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 83
    :pswitch_8
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->PREPARE_B:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 88
    :pswitch_9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/w;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_4

    goto :goto_0

    .line 91
    :pswitch_a
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->SILENCE_INSTALL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 94
    :pswitch_b
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->SILENCE_INSTALL_COMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 97
    :pswitch_c
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->FOREGROUND_INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 100
    :pswitch_d
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->BINSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 105
    :pswitch_e
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/w;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_5

    goto :goto_0

    .line 108
    :pswitch_f
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->KNOX_INSTALL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 111
    :pswitch_10
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->SILENCE_INSTALL_COMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 116
    :pswitch_11
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/w;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_6

    :pswitch_12
    goto/16 :goto_0

    .line 122
    :pswitch_13
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->BINSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 119
    :pswitch_14
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->SILENCE_INSTALL_COMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 127
    :pswitch_15
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/w;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_7

    goto/16 :goto_0

    .line 130
    :pswitch_16
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->SILENCE_INSTALL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 133
    :pswitch_17
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->SILENCE_INSTALL_COMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 136
    :pswitch_18
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;->CANCEL_B_INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 141
    :pswitch_19
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/w;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_8

    :pswitch_1a
    goto/16 :goto_0

    .line 144
    :pswitch_1b
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->SILENCE_INSTALL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 147
    :pswitch_1c
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->SILENCE_INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 150
    :pswitch_1d
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;->FOREGROUND_INSTALLING:Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 52
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_9
        :pswitch_e
        :pswitch_11
        :pswitch_15
        :pswitch_19
    .end packed-switch

    .line 55
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch

    .line 63
    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_3
    .end packed-switch

    .line 71
    :pswitch_data_3
    .packed-switch 0x3
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 88
    :pswitch_data_4
    .packed-switch 0x7
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch

    .line 105
    :pswitch_data_5
    .packed-switch 0xb
        :pswitch_f
        :pswitch_10
    .end packed-switch

    .line 116
    :pswitch_data_6
    .packed-switch 0xa
        :pswitch_13
        :pswitch_12
        :pswitch_12
        :pswitch_14
    .end packed-switch

    .line 127
    :pswitch_data_7
    .packed-switch 0xe
        :pswitch_16
        :pswitch_17
        :pswitch_18
    .end packed-switch

    .line 141
    :pswitch_data_8
    .packed-switch 0xe
        :pswitch_1b
        :pswitch_1a
        :pswitch_1a
        :pswitch_1c
        :pswitch_1d
    .end packed-switch
.end method

.method public bridge synthetic execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 9
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine$Event;)Z

    move-result v0

    return v0
.end method

.method protected exit(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 3

    .prologue
    .line 207
    const-string v0, "InstallManagerStateMachine"

    const-string v1, "exit"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallManagerStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 208
    return-void
.end method
