.class public Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# instance fields
.field private prohibitWords:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 15
    const-string v1, "prohibitWord"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 17
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 19
    const-string v1, ","

    invoke-virtual {p2, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;->prohibitWords:[Ljava/lang/String;

    .line 21
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;->prohibitWords:[Ljava/lang/String;

    array-length v4, v3

    move v1, v0

    :goto_0
    if-ge v0, v4, :cond_1

    aget-object v2, v3, v0

    .line 23
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    .line 24
    iget-object v6, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;->prohibitWords:[Ljava/lang/String;

    add-int/lit8 v2, v1, 0x1

    aput-object v5, v6, v1

    .line 21
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    goto :goto_0

    .line 29
    :cond_0
    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;->prohibitWords:[Ljava/lang/String;

    .line 32
    :cond_1
    return-void
.end method

.method public clearContainer()V
    .locals 0

    .prologue
    .line 51
    return-void
.end method

.method public closeMap()V
    .locals 0

    .prologue
    .line 63
    return-void
.end method

.method public findString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    return-object v0
.end method

.method public getProhibitWordCount()I
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;->prohibitWords:[Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;->prohibitWords:[Ljava/lang/String;

    array-length v0, v0

    .line 45
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getProhibitWords()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;->prohibitWords:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;->prohibitWords:[Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    goto :goto_0
.end method

.method public openMap()V
    .locals 0

    .prologue
    .line 57
    return-void
.end method

.method public setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 81
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    return v0
.end method
