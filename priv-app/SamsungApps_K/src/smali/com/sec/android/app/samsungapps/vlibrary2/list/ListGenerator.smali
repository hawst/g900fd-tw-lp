.class public Lcom/sec/android/app/samsungapps/vlibrary2/list/ListGenerator;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# instance fields
.field private _List:Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;

.field map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;)V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListGenerator;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 13
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListGenerator;->_List:Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;

    .line 14
    return-void
.end method


# virtual methods
.method public addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListGenerator;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListGenerator;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 44
    :cond_0
    return-void
.end method

.method public clearContainer()V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListGenerator;->_List:Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;->clear()V

    .line 49
    return-void
.end method

.method public closeMap()V
    .locals 2

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListGenerator;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    if-eqz v0, :cond_1

    .line 25
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListGenerator;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 26
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getKnoxAPI()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;->isKnoxMode()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->isKNOXApp()Z

    move-result v1

    if-nez v1, :cond_1

    .line 32
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListGenerator;->_List:Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;->add(Ljava/lang/Object;)Z

    .line 35
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListGenerator;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 36
    return-void
.end method

.method public findString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    return-object v0
.end method

.method public openMap()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListGenerator;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 19
    return-void
.end method

.method public setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListGenerator;->_List:Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;->setHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 64
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/list/ListGenerator;->_List:Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;->size()I

    move-result v0

    return v0
.end method
