.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/NoticeDetailParser;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;
.source "ProGuard"


# instance fields
.field private mNoticeContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/NoticeDetailParser;->mNoticeContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;

    .line 19
    return-void
.end method


# virtual methods
.method protected onCreateObject(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 3

    .prologue
    .line 30
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;-><init>()V

    .line 31
    const-class v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;

    const/4 v2, 0x1

    invoke-virtual {p1, v1, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->mappingClass(Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 32
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/NoticeDetailParser;->mNoticeContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;->getNoticeDetailQuery()Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetailQuery;->setNoticeDetail(Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeDetail;)V

    .line 33
    return-void
.end method

.method protected onPostParseError()V
    .locals 0

    .prologue
    .line 25
    return-void
.end method

.method protected onPostParseResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 39
    return-void
.end method
