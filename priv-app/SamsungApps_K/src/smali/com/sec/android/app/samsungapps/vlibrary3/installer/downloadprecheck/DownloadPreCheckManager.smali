.class public Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/IDownloadPreCheckManager;
.implements Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;


# static fields
.field private static p:Landroid/os/Handler;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/IDownloadPreCheckManager$IDownloadPreCheckManagerObserver;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field private e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/ILoginForDownloadManager;

.field private f:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/IPermissionManagerFactory;

.field private g:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

.field private h:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

.field private i:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

.field private j:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

.field private k:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

.field private l:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

.field private m:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/IRealNameAgeCheckerFactory;

.field private n:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;

.field private o:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

.field private q:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->p:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/ILoginForDownloadManager;Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/IPermissionManagerFactory;Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/IRealNameAgeCheckerFactory;Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    .line 99
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->q:Z

    .line 51
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->a:Landroid/content/Context;

    .line 52
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 53
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/ILoginForDownloadManager;

    .line 54
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->f:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/IPermissionManagerFactory;

    .line 55
    iput-object p5, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->g:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

    .line 56
    iput-object p6, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->h:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

    .line 57
    iput-object p7, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->i:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

    .line 58
    iput-object p8, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->j:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

    .line 59
    iput-object p9, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->k:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

    .line 60
    iput-object p10, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->l:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

    .line 61
    iput-object p11, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->m:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/IRealNameAgeCheckerFactory;

    .line 62
    iput-object p12, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->n:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;

    .line 63
    iput-object p13, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->o:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

    .line 64
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;)V

    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;)V
    .locals 2

    .prologue
    .line 74
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->p:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/a;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 81
    return-void
.end method


# virtual methods
.method public execute()V
    .locals 1

    .prologue
    .line 86
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;)V

    .line 87
    return-void
.end method

.method public getState()Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    return-object v0
.end method

.method public bridge synthetic getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->getState()Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    move-result-object v0

    return-object v0
.end method

.method public onAction(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 103
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/f;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 174
    :cond_0
    :goto_0
    return-void

    .line 106
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/ILoginForDownloadManager;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/e;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/e;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;)V

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/ILoginForDownloadManager;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/ILoginForDownloadManager$ILoginForDownloadManagerObserver;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->e:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/ILoginForDownloadManager;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/loginfordl/ILoginForDownloadManager;->execute()V

    goto :goto_0

    .line 109
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/IDownloadPreCheckManager$IDownloadPreCheckManagerObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/IDownloadPreCheckManager$IDownloadPreCheckManagerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/IDownloadPreCheckManager$IDownloadPreCheckManagerObserver;->onDownloadPrecheckFailed()V

    goto :goto_0

    .line 112
    :pswitch_2
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/d;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter$IDetailGetterObserver;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/detailgetter/DetailGetter;->validate()V

    goto :goto_0

    .line 115
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->g:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/c;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup$IConditionalPopupResult;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->g:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->execute()V

    goto :goto_0

    .line 118
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->k:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/b;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup$IConditionalPopupResult;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->k:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->execute()V

    goto :goto_0

    .line 121
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->i:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/n;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/n;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup$IConditionalPopupResult;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->i:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->execute()V

    goto :goto_0

    .line 124
    :pswitch_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->h:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/m;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/m;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup$IConditionalPopupResult;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->h:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->execute()V

    goto :goto_0

    .line 127
    :pswitch_7
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->isFreeContent()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->hasOrderID()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getPurchaseProtectionSettingValue()I

    move-result v0

    if-ne v0, v2, :cond_2

    :cond_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->P2CONFIRM_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;)V

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->n:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/l;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/l;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->addObserver(Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager$IPwordConfirmObserver;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->n:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->check()V

    goto/16 :goto_0

    .line 130
    :pswitch_8
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->m:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/IRealNameAgeCheckerFactory;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/IRealNameAgeCheckerFactory;->create(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/IRealNameAgeCheck;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/k;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/k;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;)V

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/IRealNameAgeCheck;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeCheck$IRealNameAgeCheckObserver;)V

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/IRealNameAgeCheck;->check()V

    goto/16 :goto_0

    .line 133
    :pswitch_9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->f:Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/IPermissionManagerFactory;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/IPermissionManagerFactory;->create(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/IPermissionManager;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/j;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/j;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;)V

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/IPermissionManager;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/PermissionManager$IPermissionManagerObserver;)V

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/IPermissionManager;->execute()V

    goto/16 :goto_0

    .line 136
    :pswitch_a
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->j:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/i;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/i;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup$IConditionalPopupResult;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->j:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->execute()V

    goto/16 :goto_0

    .line 139
    :pswitch_b
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->l:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/h;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/h;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup$IConditionalPopupResult;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->l:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->execute()V

    goto/16 :goto_0

    .line 142
    :pswitch_c
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/IDownloadPreCheckManager$IDownloadPreCheckManagerObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/IDownloadPreCheckManager$IDownloadPreCheckManagerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/IDownloadPreCheckManager$IDownloadPreCheckManagerObserver;->onDownloadPrecheckSucceed()V

    goto/16 :goto_0

    .line 145
    :pswitch_d
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->q:Z

    if-eqz v0, :cond_3

    .line 147
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->o:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/g;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/g;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup$IConditionalPopupResult;)V

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->o:Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;->execute()V

    goto/16 :goto_0

    .line 163
    :cond_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->CHECK_NEEDTOSHOW_ALREADY_PURCHASED_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;)V

    goto/16 :goto_0

    .line 167
    :pswitch_e
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->q:Z

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->isFreeContent()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->hasOrderID()Z

    move-result v0

    if-nez v0, :cond_0

    .line 170
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->q:Z

    goto/16 :goto_0

    .line 103
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method public bridge synthetic onAction(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 27
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->onAction(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Action;)V

    return-void
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/IDownloadPreCheckManager$IDownloadPreCheckManagerObserver;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/IDownloadPreCheckManager$IDownloadPreCheckManagerObserver;

    .line 70
    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->b:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    .line 92
    return-void
.end method

.method public bridge synthetic setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 27
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckManager;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$State;)V

    return-void
.end method
