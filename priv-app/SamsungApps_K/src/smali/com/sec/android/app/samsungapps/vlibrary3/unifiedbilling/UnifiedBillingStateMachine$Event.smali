.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

.field public static final enum ON_ACTIVITY_DIED:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

.field public static final enum ON_UNIFIED_ACTIVITY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

.field public static final enum ON_UNIFIED_ACTIVITY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

.field public static final enum REQUEST_INIT_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

.field public static final enum REQUEST_INIT_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

.field public static final enum SEND_COMPLETE_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

.field public static final enum SEND_COMPLETE_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

.field public static final enum VIEW_INVOKE_COMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 18
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    const-string v1, "EXECUTE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    .line 19
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    const-string v1, "REQUEST_INIT_SUCCESS"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;->REQUEST_INIT_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    const-string v1, "VIEW_INVOKE_COMPLETED"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;->VIEW_INVOKE_COMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    const-string v1, "ON_UNIFIED_ACTIVITY_SUCCESS"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;->ON_UNIFIED_ACTIVITY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    const-string v1, "ON_UNIFIED_ACTIVITY_FAILED"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;->ON_UNIFIED_ACTIVITY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    const-string v1, "REQUEST_INIT_FAILED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;->REQUEST_INIT_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    const-string v1, "SEND_COMPLETE_SUCCESS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;->SEND_COMPLETE_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    const-string v1, "SEND_COMPLETE_FAILED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;->SEND_COMPLETE_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    const-string v1, "ON_ACTIVITY_DIED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;->ON_ACTIVITY_DIED:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    .line 16
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;->REQUEST_INIT_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;->VIEW_INVOKE_COMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;->ON_UNIFIED_ACTIVITY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;->ON_UNIFIED_ACTIVITY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;->REQUEST_INIT_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;->SEND_COMPLETE_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;->SEND_COMPLETE_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;->ON_ACTIVITY_DIED:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    return-object v0
.end method
