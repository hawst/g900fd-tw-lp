.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationManager;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract addInstallItem(Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;)Z
.end method

.method public abstract addWaitingItem(Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;)Z
.end method

.method public abstract registerPushNotify(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
.end method

.method public abstract registerPushNotify(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Landroid/app/PendingIntent;)V
.end method

.method public abstract removeItem(Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;)Z
.end method

.method public abstract setDownloadProgress(Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;)Z
.end method

.method public abstract setItemInstalled(Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;)Z
.end method

.method public abstract setItemInstalledFromGoogleInstaller(Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;)Z
.end method
