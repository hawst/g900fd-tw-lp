.class final Lcom/sec/android/app/billing/helper/n;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Lcom/sec/android/app/billing/helper/UPHelper;

.field private final synthetic b:Landroid/app/Activity;

.field private final synthetic c:I

.field private final synthetic d:I

.field private final synthetic e:Ljava/lang/String;

.field private final synthetic f:Ljava/lang/String;

.field private final synthetic g:Ljava/lang/String;

.field private final synthetic h:Landroid/os/Handler;


# direct methods
.method constructor <init>(Lcom/sec/android/app/billing/helper/UPHelper;Landroid/app/Activity;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/billing/helper/n;->a:Lcom/sec/android/app/billing/helper/UPHelper;

    iput-object p2, p0, Lcom/sec/android/app/billing/helper/n;->b:Landroid/app/Activity;

    iput p3, p0, Lcom/sec/android/app/billing/helper/n;->c:I

    iput p4, p0, Lcom/sec/android/app/billing/helper/n;->d:I

    iput-object p5, p0, Lcom/sec/android/app/billing/helper/n;->e:Ljava/lang/String;

    iput-object p6, p0, Lcom/sec/android/app/billing/helper/n;->f:Ljava/lang/String;

    iput-object p7, p0, Lcom/sec/android/app/billing/helper/n;->g:Ljava/lang/String;

    iput-object p8, p0, Lcom/sec/android/app/billing/helper/n;->h:Landroid/os/Handler;

    .line 273
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 8

    .prologue
    const/4 v7, 0x3

    .line 284
    const-string v0, "UPHelper"

    const-string v1, "[UP_API_VERSION : 20016] startSetupRetry() : Billing service connected."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    invoke-static {p2}, Lcom/sec/android/app/billing/service/IBillingService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/android/app/billing/service/IBillingService;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/billing/helper/UPHelper;->a(Lcom/sec/android/app/billing/service/IBillingService;)V

    .line 287
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/android/app/billing/helper/UPHelper;->a(Z)V

    .line 290
    :try_start_0
    const-string v0, "UPHelper"

    const-string v1, "[UP_API_VERSION : 20016] startSetupRetry() : requestBillingAIDL() : start"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    iget-object v0, p0, Lcom/sec/android/app/billing/helper/n;->a:Lcom/sec/android/app/billing/helper/UPHelper;

    iget-object v0, p0, Lcom/sec/android/app/billing/helper/n;->b:Landroid/app/Activity;

    iget v1, p0, Lcom/sec/android/app/billing/helper/n;->c:I

    iget v2, p0, Lcom/sec/android/app/billing/helper/n;->d:I

    iget-object v3, p0, Lcom/sec/android/app/billing/helper/n;->e:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/billing/helper/n;->f:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/app/billing/helper/n;->g:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/app/billing/helper/n;->h:Landroid/os/Handler;

    invoke-static/range {v0 .. v6}, Lcom/sec/android/app/billing/helper/UPHelper;->a(Landroid/app/Activity;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 302
    :cond_0
    :goto_0
    return-void

    .line 293
    :catch_0
    move-exception v0

    .line 294
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 295
    const-string v1, "UPHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[UP_API_VERSION : 20016] startSetupRetry() : requestBillingAIDL() : end : Exception : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    iget-object v1, p0, Lcom/sec/android/app/billing/helper/n;->h:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 297
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 298
    iput v7, v1, Landroid/os/Message;->what:I

    .line 299
    iget-object v2, p0, Lcom/sec/android/app/billing/helper/n;->a:Lcom/sec/android/app/billing/helper/UPHelper;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Lcom/sec/android/app/billing/helper/UPHelper;->a(ILjava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 300
    iget-object v0, p0, Lcom/sec/android/app/billing/helper/n;->h:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 276
    const-string v0, "UPHelper"

    const-string v1, "[UP_API_VERSION : 20016] startSetupRetry() : Billing service disconnected."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/app/billing/helper/UPHelper;->a(Lcom/sec/android/app/billing/service/IBillingService;)V

    .line 279
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/app/billing/helper/UPHelper;->a(Z)V

    .line 280
    return-void
.end method
