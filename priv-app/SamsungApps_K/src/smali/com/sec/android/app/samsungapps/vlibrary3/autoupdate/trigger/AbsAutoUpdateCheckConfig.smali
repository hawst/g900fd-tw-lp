.class public abstract Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AbsAutoUpdateCheckConfig;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateCheckConfig;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AbsAutoUpdateCheckConfig;->b:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;

    .line 13
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AbsAutoUpdateCheckConfig;->a:Landroid/content/Context;

    .line 14
    return-void
.end method


# virtual methods
.method public existAlarm()Z
    .locals 3

    .prologue
    .line 119
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AlarmRegisterer;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AbsAutoUpdateCheckConfig;->a:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AbsAutoUpdateCheckConfig;->getClassNameForAlarmRegister()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AlarmRegisterer;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 121
    :try_start_0
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AlarmRegisterer;->checkAlarmExist()Z
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 125
    :goto_0
    return v0

    .line 122
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    .line 125
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract getClassNameForAlarmRegister()Ljava/lang/String;
.end method

.method public getDefaultInterval()Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;
    .locals 3

    .prologue
    .line 82
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;-><init>()V

    .line 83
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AbsAutoUpdateCheckConfig;->getDefaultIntervalSec()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;->setSeconds(J)V

    .line 84
    return-object v0
.end method

.method protected abstract getDefaultIntervalSec()J
.end method

.method public getInterval()Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AbsAutoUpdateCheckConfig;->b:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;

    return-object v0
.end method

.method protected abstract getIntervalSharedPrefName()Ljava/lang/String;
.end method

.method protected abstract getLastUpdateCheckStr()Ljava/lang/String;
.end method

.method protected abstract getUpdateCycleServerResponseFieldName()Ljava/lang/String;
.end method

.method public isTimedOutByCompareLoadedIntervalAndLastUpdTime()Z
    .locals 4

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AbsAutoUpdateCheckConfig;->readLastUpdateCheckTime()J

    move-result-wide v0

    .line 91
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 93
    sub-long v0, v2, v0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AbsAutoUpdateCheckConfig;->b:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;->getMills()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 95
    const/4 v0, 0x1

    .line 99
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadInterval()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 62
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AbsAutoUpdateCheckConfig;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;-><init>(Landroid/content/Context;)V

    .line 63
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AbsAutoUpdateCheckConfig;->getIntervalSharedPrefName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 64
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    .line 67
    :try_start_0
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;

    invoke-direct {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AbsAutoUpdateCheckConfig;->b:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;

    .line 68
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AbsAutoUpdateCheckConfig;->b:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    int-to-long v3, v1

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;->setSeconds(J)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 69
    const/4 v0, 0x1

    .line 76
    :cond_0
    :goto_0
    return v0

    .line 72
    :catch_0
    move-exception v1

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AbsAutoUpdateCheckConfig;->b:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;

    goto :goto_0
.end method

.method public readLastUpdateCheckTime()J
    .locals 4

    .prologue
    const-wide/16 v0, 0x0

    .line 45
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AbsAutoUpdateCheckConfig;->a:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;-><init>(Landroid/content/Context;)V

    .line 46
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AbsAutoUpdateCheckConfig;->getLastUpdateCheckStr()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 47
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    .line 56
    :cond_0
    :goto_0
    return-wide v0

    .line 52
    :cond_1
    :try_start_0
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    goto :goto_0

    .line 56
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public registerAlarm(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;)V
    .locals 3

    .prologue
    .line 35
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AlarmRegisterer;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AbsAutoUpdateCheckConfig;->a:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AbsAutoUpdateCheckConfig;->getClassNameForAlarmRegister()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AlarmRegisterer;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 37
    :try_start_0
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;->getMills()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AlarmRegisterer;->register(J)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 41
    :goto_0
    return-void

    .line 38
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public saveAndSetInterval(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;)V
    .locals 4

    .prologue
    .line 111
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AbsAutoUpdateCheckConfig;->b:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;

    .line 113
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AbsAutoUpdateCheckConfig;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;-><init>(Landroid/content/Context;)V

    .line 114
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AbsAutoUpdateCheckConfig;->getIntervalSharedPrefName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AbsAutoUpdateCheckConfig;->b:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/UpdateInterval;->getSeconds()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 115
    return-void
.end method

.method public writeLastUpdateCheckedTimeNow()V
    .locals 4

    .prologue
    .line 24
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AbsAutoUpdateCheckConfig;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;-><init>(Landroid/content/Context;)V

    .line 25
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AbsAutoUpdateCheckConfig;->getLastUpdateCheckStr()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 26
    return-void
.end method
