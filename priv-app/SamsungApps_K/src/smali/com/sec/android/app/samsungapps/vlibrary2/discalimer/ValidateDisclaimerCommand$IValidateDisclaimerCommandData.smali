.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/ValidateDisclaimerCommand$IValidateDisclaimerCommandData;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract askUserToAcceptDisclaimerCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract clearDisclaimer()V
.end method

.method public abstract existNewDisclaimer()Z
.end method

.method public abstract existNewDisclaimerVersion()Z
.end method

.method public abstract getNewDisclaimerVersion()Ljava/lang/String;
.end method

.method public abstract getOldDisclaimer()Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;
.end method

.method public abstract isDiscalimerNeverAgreedByUser()Z
.end method

.method public abstract needDisclaimerClear()Z
.end method

.method public abstract requestDisclaimerCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method
