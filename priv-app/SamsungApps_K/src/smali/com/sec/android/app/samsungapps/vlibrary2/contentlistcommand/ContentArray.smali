.class public Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/ContentArray;
.super Ljava/util/ArrayList;
.source "ProGuard"


# static fields
.field private static final serialVersionUID:J = 0xd9fe4547fa91903L


# instance fields
.field private _CountPerPage:I

.field private _EdNum:I

.field private _StNum:I

.field private _bEOF:Z


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 19
    iput p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/ContentArray;->_CountPerPage:I

    .line 20
    return-void
.end method


# virtual methods
.method public applyResult(Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/ContentArray;)V
    .locals 2

    .prologue
    .line 75
    monitor-enter p0

    .line 77
    :try_start_0
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/ContentArray;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 79
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/ContentArray;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 87
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 81
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/ContentArray;->isComplete()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/ContentArray;->setEOF(Z)V

    .line 82
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/ContentArray;->_StNum:I

    if-nez v0, :cond_1

    .line 84
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/ContentArray;->getStartNumber()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/ContentArray;->_StNum:I

    .line 86
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/ContentArray;->getEndNumber()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/ContentArray;->setEndNumber(I)V

    .line 87
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 92
    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/ContentArray;->_StNum:I

    .line 93
    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/ContentArray;->_EdNum:I

    .line 94
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/ContentArray;->_bEOF:Z

    .line 95
    return-void
.end method

.method public getEndNumber()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/ContentArray;->_EdNum:I

    return v0
.end method

.method public getNextEndNumber()I
    .locals 2

    .prologue
    .line 47
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/ContentArray;->_EdNum:I

    if-nez v0, :cond_0

    .line 48
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/ContentArray;->_CountPerPage:I

    .line 50
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/ContentArray;->_EdNum:I

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/ContentArray;->_CountPerPage:I

    div-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/ContentArray;->_CountPerPage:I

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/ContentArray;->_CountPerPage:I

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public getNextStartNum()I
    .locals 2

    .prologue
    .line 39
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/ContentArray;->_EdNum:I

    if-nez v0, :cond_0

    .line 40
    const/4 v0, 0x1

    .line 42
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/ContentArray;->_EdNum:I

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/ContentArray;->_CountPerPage:I

    div-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/ContentArray;->_CountPerPage:I

    mul-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getStartNumber()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/ContentArray;->_StNum:I

    return v0
.end method

.method public isComplete()Z
    .locals 1

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/ContentArray;->_bEOF:Z

    return v0
.end method

.method public setCountPerPage(I)V
    .locals 0

    .prologue
    .line 24
    iput p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/ContentArray;->_CountPerPage:I

    .line 25
    return-void
.end method

.method public setEOF(Z)V
    .locals 0

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/ContentArray;->_bEOF:Z

    .line 66
    return-void
.end method

.method public setEndNumber(I)V
    .locals 0

    .prologue
    .line 60
    iput p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/ContentArray;->_EdNum:I

    .line 61
    return-void
.end method

.method public setStartNumber(I)V
    .locals 0

    .prologue
    .line 55
    iput p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/ContentArray;->_StNum:I

    .line 56
    return-void
.end method
