.class public Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/DeleteSelectedWishItemCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

.field private _IWishListCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishListCommandBuilder;

.field items:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishListCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/DeleteSelectedWishItemCommand;->items:Ljava/util/ArrayList;

    .line 24
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/DeleteSelectedWishItemCommand;->_IWishListCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishListCommandBuilder;

    .line 25
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/DeleteSelectedWishItemCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    .line 26
    return-void
.end method


# virtual methods
.method protected deleteItems()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/DeleteSelectedWishItemCommand;->items:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 47
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/DeleteSelectedWishItemCommand;->onFinalResult(Z)V

    .line 48
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/DeleteSelectedWishItemCommand;->endLoading()V

    .line 64
    :goto_0
    return-void

    .line 51
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/DeleteSelectedWishItemCommand;->items:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;

    .line 52
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/DeleteSelectedWishItemCommand;->items:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 53
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/c;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/DeleteSelectedWishItemCommand;)V

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->deleteWishList(Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 61
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/DeleteSelectedWishItemCommand;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 63
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    goto :goto_0
.end method

.method protected endLoading()V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/DeleteSelectedWishItemCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->endLoading()V

    .line 74
    return-void
.end method

.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 3

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/DeleteSelectedWishItemCommand;->startLoading()V

    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/DeleteSelectedWishItemCommand;->items:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 31
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/DeleteSelectedWishItemCommand;->_IWishListCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishListCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishListCommandBuilder;->getWishListArray()Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/WishArray;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 33
    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;

    .line 34
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishItem;->isSelectedToBeDeleted()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 36
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/DeleteSelectedWishItemCommand;->items:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 39
    :cond_1
    return-void
.end method

.method protected startLoading()V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/DeleteSelectedWishItemCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->startLoading()V

    .line 69
    return-void
.end method
