.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/AgeCalculator;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field mCurDay:I

.field mCurMonth:I

.field mCurYear:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 15
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AgeCalculator;->mCurYear:I

    .line 16
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AgeCalculator;->mCurMonth:I

    .line 17
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AgeCalculator;->mCurDay:I

    .line 18
    return-void
.end method

.method public constructor <init>(III)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AgeCalculator;->mCurYear:I

    .line 23
    iput p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AgeCalculator;->mCurMonth:I

    .line 24
    iput p3, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AgeCalculator;->mCurDay:I

    .line 25
    return-void
.end method


# virtual methods
.method public calcAge(III)I
    .locals 2

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AgeCalculator;->mCurYear:I

    sub-int/2addr v0, p1

    .line 30
    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AgeCalculator;->mCurMonth:I

    if-gt p2, v1, :cond_0

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AgeCalculator;->mCurMonth:I

    if-ne p2, v1, :cond_1

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AgeCalculator;->mCurDay:I

    if-le p3, v1, :cond_1

    .line 32
    :cond_0
    add-int/lit8 v0, v0, -0x1

    .line 34
    :cond_1
    return v0
.end method
