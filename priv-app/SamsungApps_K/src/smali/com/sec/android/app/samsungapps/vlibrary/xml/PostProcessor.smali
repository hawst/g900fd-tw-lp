.class public abstract Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/xml/IXmlParserData;


# instance fields
.field private mID:I

.field protected mServerError:Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;->mID:I

    return-void
.end method

.method private getServerError()Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;->mServerError:Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;

    return-object v0
.end method


# virtual methods
.method public getID()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;->mID:I

    return v0
.end method

.method protected isSuccess()Z
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;->getServerError()Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;->getServerError()Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;->isError()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract onCreateObject(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
.end method

.method public onEndParse()V
    .locals 0

    .prologue
    .line 34
    return-void
.end method

.method protected abstract onPostParseError()V
.end method

.method protected abstract onPostParseResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
.end method

.method public onReceiveParsingResult(Lcom/sec/android/app/samsungapps/vlibrary/xml/result/IResponseParseResult;)V
    .locals 2

    .prologue
    .line 48
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/result/IResponseParseResult;->getHeaderMap()Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;->onPostParseResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 50
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/result/IResponseParseResult;->getServerErrorInfo()Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;->setServerError(Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;)V

    .line 51
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;->onPostParseError()V

    .line 53
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/result/IResponseParseResult;->getBodyListMap()Ljava/util/ArrayList;

    move-result-object v0

    .line 54
    if-eqz v0, :cond_0

    .line 56
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 58
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;->onCreateObject(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    goto :goto_0

    .line 62
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;->onEndParse()V

    .line 63
    return-void
.end method

.method public setID(I)V
    .locals 0

    .prologue
    .line 38
    iput p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;->mID:I

    .line 39
    return-void
.end method

.method public setServerError(Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;)V
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;->mServerError:Lcom/sec/android/app/samsungapps/vlibrary/net/ServerError;

    .line 28
    return-void
.end method
