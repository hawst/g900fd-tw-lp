.class public final enum Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

.field public static final enum ALIPAY_INSTALL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

.field public static final enum ALIPAY_INSTALL_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

.field public static final enum COMPLETE_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

.field public static final enum COMPLETE_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

.field public static final enum EXECUTE_ALIPAY_INSTALLED:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

.field public static final enum EXECUTE_ALIPAY_NOT_INSTALLED:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

.field public static final enum INIT_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

.field public static final enum INIT_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

.field public static final enum PROCESS_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

.field public static final enum PROCESS_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

.field public static final enum USER_AGREE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

.field public static final enum USER_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 6
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    const-string v1, "EXECUTE_ALIPAY_INSTALLED"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;->EXECUTE_ALIPAY_INSTALLED:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    .line 7
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    const-string v1, "EXECUTE_ALIPAY_NOT_INSTALLED"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;->EXECUTE_ALIPAY_NOT_INSTALLED:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    .line 8
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    const-string v1, "USER_CANCEL"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;->USER_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    .line 9
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    const-string v1, "USER_AGREE"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;->USER_AGREE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    .line 10
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    const-string v1, "ALIPAY_INSTALL_FAILED"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;->ALIPAY_INSTALL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    .line 11
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    const-string v1, "ALIPAY_INSTALL_SUCCESS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;->ALIPAY_INSTALL_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    .line 12
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    const-string v1, "INIT_FAILED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;->INIT_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    .line 13
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    const-string v1, "INIT_SUCCESS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;->INIT_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    .line 14
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    const-string v1, "PROCESS_FAILED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;->PROCESS_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    .line 15
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    const-string v1, "PROCESS_SUCCESS"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;->PROCESS_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    .line 16
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    const-string v1, "COMPLETE_SUCCESS"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;->COMPLETE_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    .line 17
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    const-string v1, "COMPLETE_FAILED"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;->COMPLETE_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    .line 4
    const/16 v0, 0xc

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;->EXECUTE_ALIPAY_INSTALLED:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;->EXECUTE_ALIPAY_NOT_INSTALLED:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;->USER_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;->USER_AGREE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;->ALIPAY_INSTALL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;->ALIPAY_INSTALL_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;->INIT_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;->INIT_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;->PROCESS_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;->PROCESS_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;->COMPLETE_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;->COMPLETE_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;
    .locals 1

    .prologue
    .line 4
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;
    .locals 1

    .prologue
    .line 4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPaymentEvent$Event;

    return-object v0
.end method
