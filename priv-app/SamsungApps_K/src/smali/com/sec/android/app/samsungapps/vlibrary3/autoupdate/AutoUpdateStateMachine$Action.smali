.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum CHECK_IF_UPDATETIMING:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

.field public static final enum CHECK_SELF_UPDATE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

.field public static final enum CHECK_WHETHER_INITIALIZED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

.field public static final enum NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

.field public static final enum NOTIFY_FINISHED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

.field public static final enum NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

.field public static final enum REQUEST_SELLER_UPDATE_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

.field public static final enum RUN_SELLER_UPDATECHECK:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 19
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

    const-string v1, "CHECK_WHETHER_INITIALIZED"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;->CHECK_WHETHER_INITIALIZED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

    const-string v1, "NOTIFY_FAILED"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

    const-string v1, "CHECK_IF_UPDATETIMING"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;->CHECK_IF_UPDATETIMING:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

    const-string v1, "NOTIFY_FINISHED"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;->NOTIFY_FINISHED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

    const-string v1, "RUN_SELLER_UPDATECHECK"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;->RUN_SELLER_UPDATECHECK:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

    const-string v1, "NOTIFY_SUCCESS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;->NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

    const-string v1, "CHECK_SELF_UPDATE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;->CHECK_SELF_UPDATE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

    const-string v1, "REQUEST_SELLER_UPDATE_CANCEL"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;->REQUEST_SELLER_UPDATE_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

    .line 18
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;->CHECK_WHETHER_INITIALIZED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;->CHECK_IF_UPDATETIMING:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;->NOTIFY_FINISHED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;->RUN_SELLER_UPDATECHECK:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;->NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;->CHECK_SELF_UPDATE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;->REQUEST_SELLER_UPDATE_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateStateMachine$Action;

    return-object v0
.end method
