.class public Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;


# instance fields
.field a:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;

.field b:Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$State;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

.field private e:Landroid/content/Context;

.field private f:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field private g:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

.field private h:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$State;

    .line 97
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;

    .line 149
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;

    .line 33
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->f:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 34
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->d:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    .line 35
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->e:Landroid/content/Context;

    .line 36
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->h:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    .line 37
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;)V

    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;)V
    .locals 1

    .prologue
    .line 47
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;)Z

    .line 48
    return-void
.end method


# virtual methods
.method public check()V
    .locals 1

    .prologue
    .line 180
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;)V

    .line 181
    return-void
.end method

.method public execute(Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 1

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->g:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    .line 42
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;)V

    .line 43
    return-void
.end method

.method public getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->f:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    return-object v0
.end method

.method public getInitResult()Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;

    return-object v0
.end method

.method public getState()Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$State;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$State;

    return-object v0
.end method

.method public bridge synthetic getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->getState()Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$State;

    move-result-object v0

    return-object v0
.end method

.method public invokeCompleted()V
    .locals 1

    .prologue
    .line 190
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;->VIEW_INVOKE_COMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;)V

    .line 191
    return-void
.end method

.method public onAction(Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;)V
    .locals 5

    .prologue
    .line 62
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/c;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 90
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 65
    :pswitch_1
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->userID:Ljava/lang/String;

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->f:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;

    new-instance v4, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/a;

    invoke-direct {v4, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;)V

    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->initPayment(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->e:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;->REQUEST_INIT_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;)V

    goto :goto_0

    .line 68
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->d:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->e:Landroid/content/Context;

    invoke-interface {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;->invoke(Landroid/content/Context;Ljava/lang/Object;)V

    goto :goto_0

    .line 75
    :pswitch_3
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->a:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/InitPaymentResult;->signature:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->h:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    new-instance v4, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/b;

    invoke-direct {v4, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;)V

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->completeOrder(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->e:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    goto :goto_0

    .line 78
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->g:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->g:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;->onCommandResult(Z)V

    goto :goto_0

    .line 84
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->g:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->g:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;->onCommandResult(Z)V

    goto :goto_0

    .line 62
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public bridge synthetic onAction(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->onAction(Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;)V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 94
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;->ON_ACTIVITY_DIED:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;)V

    .line 95
    return-void
.end method

.method public onUnifiedBillingResult(Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager$UnifiedBillingResult;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 153
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager$UnifiedBillingResult;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager$UnifiedBillingResult;

    if-ne p1, v0, :cond_1

    .line 155
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;

    invoke-direct {v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CompleteOrderInfo;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;->ON_UNIFIED_ACTIVITY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;)V

    .line 169
    :goto_0
    return-void

    .line 162
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;->ON_UNIFIED_ACTIVITY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;)V

    goto :goto_0

    .line 167
    :cond_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;->ON_UNIFIED_ACTIVITY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;)V

    goto :goto_0
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$State;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$State;

    .line 53
    return-void
.end method

.method public bridge synthetic setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$State;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingManager;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$State;)V

    return-void
.end method
