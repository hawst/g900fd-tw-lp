.class public Lcom/sec/android/app/samsungapps/vlibrary/util/PlatformVersionString;
.super Ljava/lang/Object;
.source "ProGuard"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getPlatformVersion(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    packed-switch p0, :pswitch_data_0

    .line 54
    const-string v0, ""

    :goto_0
    return-object v0

    .line 21
    :pswitch_0
    const-string v0, "Android 1.0"

    goto :goto_0

    .line 23
    :pswitch_1
    const-string v0, "Android 1.1"

    goto :goto_0

    .line 25
    :pswitch_2
    const-string v0, "CupCake 1.5"

    goto :goto_0

    .line 27
    :pswitch_3
    const-string v0, "Donut 1.6"

    goto :goto_0

    .line 29
    :pswitch_4
    const-string v0, "Eclair 2.0"

    goto :goto_0

    .line 31
    :pswitch_5
    const-string v0, "Eclair 2.0.1"

    goto :goto_0

    .line 33
    :pswitch_6
    const-string v0, "Eclair 2.1"

    goto :goto_0

    .line 35
    :pswitch_7
    const-string v0, "Froyo 2.2"

    goto :goto_0

    .line 37
    :pswitch_8
    const-string v0, "GingerBread 2.3"

    goto :goto_0

    .line 39
    :pswitch_9
    const-string v0, "GingerBread 2.3.3"

    goto :goto_0

    .line 41
    :pswitch_a
    const-string v0, "HoneyComb 3.0"

    goto :goto_0

    .line 43
    :pswitch_b
    const-string v0, "HoneyComb 3.1"

    goto :goto_0

    .line 45
    :pswitch_c
    const-string v0, "HoneyComb 3.2"

    goto :goto_0

    .line 47
    :pswitch_d
    const-string v0, "Ice Cream Sandwich 4.0"

    goto :goto_0

    .line 49
    :pswitch_e
    const-string v0, "ICE_CREAM_SANDWICH_MR1"

    goto :goto_0

    .line 51
    :pswitch_f
    const-string v0, "JELLY_BEAN"

    goto :goto_0

    .line 19
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method public static getPlatformVersion(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 8
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 14
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/PlatformVersionString;->getPlatformVersion(I)Ljava/lang/String;

    move-result-object p0

    :goto_0
    return-object p0

    .line 12
    :catch_0
    move-exception v0

    goto :goto_0
.end method
