.class public Lcom/sec/android/app/samsungapps/SignUpActivity;
.super Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;
.source "ProGuard"


# static fields
.field protected static final ID_DIALOG_DATE_PICKER:I = 0x2


# instance fields
.field a:Lcom/sec/android/app/samsungapps/lj;

.field b:Lcom/sec/android/app/samsungapps/lj;

.field c:Lcom/sec/android/app/samsungapps/lj;

.field d:Z

.field e:Lcom/sec/android/app/samsungapps/WebTermConditionManager;

.field f:Ljava/lang/String;

.field g:Ljava/lang/String;

.field h:Ljava/lang/String;

.field i:I

.field j:I

.field k:I

.field l:Lcom/sec/android/app/samsungapps/vlibrary2/account/SignUpCommand;

.field m:Z

.field n:Landroid/content/Context;

.field o:Landroid/text/TextWatcher;

.field private p:Landroid/app/DatePickerDialog$OnDateSetListener;

.field private q:Landroid/preference/PreferenceScreen;

.field private r:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

.field private s:Z

.field private t:Ljava/lang/CharSequence;

.field private u:Z

.field private v:Landroid/view/inputmethod/InputMethodManager;

.field private w:Landroid/widget/EditText;

.field private x:Lcom/sec/android/app/samsungapps/LoadingDialog;

.field private y:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 53
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;-><init>()V

    .line 57
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->p:Landroid/app/DatePickerDialog$OnDateSetListener;

    .line 59
    new-instance v0, Lcom/sec/android/app/samsungapps/lj;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/lj;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->a:Lcom/sec/android/app/samsungapps/lj;

    .line 60
    new-instance v0, Lcom/sec/android/app/samsungapps/lj;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/lj;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->b:Lcom/sec/android/app/samsungapps/lj;

    .line 61
    new-instance v0, Lcom/sec/android/app/samsungapps/lj;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/lj;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->c:Lcom/sec/android/app/samsungapps/lj;

    .line 62
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->d:Z

    .line 72
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->m:Z

    .line 73
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->r:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    .line 74
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->s:Z

    .line 76
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->n:Landroid/content/Context;

    .line 220
    new-instance v0, Lcom/sec/android/app/samsungapps/lc;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/lc;-><init>(Lcom/sec/android/app/samsungapps/SignUpActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->o:Landroid/text/TextWatcher;

    .line 245
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->t:Ljava/lang/CharSequence;

    .line 247
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->u:Z

    .line 527
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->v:Landroid/view/inputmethod/InputMethodManager;

    .line 528
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->w:Landroid/widget/EditText;

    .line 807
    new-instance v0, Lcom/sec/android/app/samsungapps/la;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/la;-><init>(Lcom/sec/android/app/samsungapps/SignUpActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->y:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/SignUpActivity;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->t:Ljava/lang/CharSequence;

    return-object p1
.end method

.method private a(III)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 338
    iput p1, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->i:I

    .line 339
    iput p2, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->j:I

    .line 340
    iput p3, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->k:I

    .line 341
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->u:Z

    .line 342
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->getPositive()Landroid/widget/Button;

    move-result-object v0

    .line 343
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 344
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 348
    :goto_0
    return-void

    .line 346
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/SignUpActivity;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->b()V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/SignUpActivity;I)V
    .locals 6

    .prologue
    .line 53
    new-instance v0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080276

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080308

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->getDialog()Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->r:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    return-void
.end method

.method private a(Z)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const v6, 0x7f0802a7

    const v5, 0xb0001

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 129
    const v0, 0x7f0c0039

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    .line 131
    if-eqz p1, :cond_0

    .line 132
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "(1/2)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v4, v4, [I

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/SignUpActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    .line 140
    :goto_0
    return-void

    .line 136
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "(2/2)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v4, v4, [I

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/SignUpActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/SignUpActivity;III)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 53
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->l:Lcom/sec/android/app/samsungapps/vlibrary2/account/SignUpCommand;

    invoke-virtual {v1, p1, p2, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SignUpCommand;->checkAgeLimitation(III)Z

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/SignUpActivity;Z)Z
    .locals 0

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->s:Z

    return p1
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 322
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 323
    const/4 v0, 0x1

    .line 325
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 286
    if-nez p0, :cond_1

    .line 292
    :cond_0
    :goto_0
    return v0

    .line 289
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lt v1, p1, :cond_0

    .line 292
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static b(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 330
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 333
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, -0x1

    goto :goto_0
.end method

.method private b()V
    .locals 9

    .prologue
    const v8, 0x7f0c02d9

    const v7, 0x7f0c02d7

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v6, -0x1

    .line 102
    iput-boolean v3, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->m:Z

    .line 103
    const v0, 0x7f0400d3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->setContentView(I)V

    .line 105
    invoke-direct {p0, v3}, Lcom/sec/android/app/samsungapps/SignUpActivity;->a(Z)V

    .line 107
    const v0, 0x7f0802ed

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f08023d

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/SignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->showPositiveNegativeButton(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->c()V

    .line 111
    new-instance v0, Lcom/sec/android/app/samsungapps/WebTermConditionManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->n:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/WebTermConditionManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->e:Lcom/sec/android/app/samsungapps/WebTermConditionManager;

    .line 112
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->getPositive()Landroid/widget/Button;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/lb;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/lb;-><init>(Lcom/sec/android/app/samsungapps/SignUpActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    new-instance v0, Lcom/sec/android/app/samsungapps/lf;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/lf;-><init>(Lcom/sec/android/app/samsungapps/SignUpActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->p:Landroid/app/DatePickerDialog$OnDateSetListener;

    const v0, 0x7f0c02dd

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/samsungapps/lg;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/lg;-><init>(Lcom/sec/android/app/samsungapps/SignUpActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    new-instance v1, Lcom/sec/android/app/samsungapps/lh;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/lh;-><init>(Lcom/sec/android/app/samsungapps/SignUpActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v1, Lcom/sec/android/app/samsungapps/li;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/li;-><init>(Lcom/sec/android/app/samsungapps/SignUpActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    new-instance v1, Lcom/sec/android/app/samsungapps/ku;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/ku;-><init>(Lcom/sec/android/app/samsungapps/SignUpActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 114
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->isStartedWithOSPID()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, v7}, Lcom/sec/android/app/samsungapps/SignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {p0, v8}, Lcom/sec/android/app/samsungapps/SignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    const v2, 0x7f0c02db

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/SignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getEmailID()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getEmailID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getPassword()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getPassword()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getPassword()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 115
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->isStartedWithOSPID()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->birthDay:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/SignUpActivity;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->birthMonth:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/SignUpActivity;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->birthYear:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/SignUpActivity;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->birthYear:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/SignUpActivity;->b(Ljava/lang/String;)I

    move-result v1

    iget-object v2, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->birthMonth:Ljava/lang/String;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/SignUpActivity;->b(Ljava/lang/String;)I

    move-result v2

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->birthDay:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->b(Ljava/lang/String;)I

    move-result v0

    if-eq v1, v6, :cond_2

    if-eq v2, v6, :cond_2

    if-eq v0, v6, :cond_2

    invoke-direct {p0, v1, v2, v0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->a(III)V

    move v0, v3

    :goto_0
    if-eqz v0, :cond_3

    .line 116
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getEmailID()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->f:Ljava/lang/String;

    .line 117
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getPassword()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->g:Ljava/lang/String;

    .line 118
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getPassword()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->h:Ljava/lang/String;

    .line 119
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->e()V

    .line 125
    :goto_1
    return-void

    :cond_2
    move v0, v4

    .line 115
    goto :goto_0

    .line 121
    :cond_3
    invoke-virtual {p0, v7}, Lcom/sec/android/app/samsungapps/SignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {p0, v8}, Lcom/sec/android/app/samsungapps/SignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    const v2, 0x7f0c02db

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/SignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->o:Landroid/text/TextWatcher;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->o:Landroid/text/TextWatcher;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->o:Landroid/text/TextWatcher;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 122
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getPositiveButton(Landroid/app/Activity;)Landroid/widget/Button;

    move-result-object v0

    .line 123
    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_1
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/SignUpActivity;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->d()V

    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/SignUpActivity;III)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/SignUpActivity;->a(III)V

    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 143
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->getNegative()Landroid/widget/Button;

    move-result-object v0

    .line 144
    new-instance v1, Lcom/sec/android/app/samsungapps/kt;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/kt;-><init>(Lcom/sec/android/app/samsungapps/SignUpActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 157
    return-void
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/SignUpActivity;)Z
    .locals 2

    .prologue
    const/4 v1, 0x6

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->g:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/SignUpActivity;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->h:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/SignUpActivity;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->g:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()V
    .locals 4

    .prologue
    .line 267
    const v0, 0x7f0c02d7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 268
    const v1, 0x7f0c02d9

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/SignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 269
    const v2, 0x7f0c02db

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/SignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    .line 271
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->f:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 272
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->g:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 273
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->h:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 275
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 276
    iget v1, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->i:I

    invoke-virtual {v0, v1}, Ljava/util/Date;->setYear(I)V

    .line 277
    iget v1, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->j:I

    invoke-virtual {v0, v1}, Ljava/util/Date;->setMonth(I)V

    .line 278
    iget v1, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->k:I

    invoke-virtual {v0, v1}, Ljava/util/Date;->setDate(I)V

    .line 280
    const v0, 0x7f0c02dd

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 281
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->t:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 283
    return-void
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/SignUpActivity;)Z
    .locals 2

    .prologue
    .line 53
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->f:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator;->getValidationResult()Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator$ValidatorResult;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator$ValidatorResult;->VALID:Lcom/sec/android/app/samsungapps/vlibrary/doc/EmailAddressValidator$ValidatorResult;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()V
    .locals 5

    .prologue
    const v4, 0x7f0802b0

    const/4 v3, 0x0

    .line 542
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    instance-of v1, v0, Landroid/widget/EditText;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->w:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->v:Landroid/view/inputmethod/InputMethodManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->w:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->v:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->w:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 543
    :cond_0
    iput-boolean v3, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->m:Z

    .line 544
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->d:Z

    .line 545
    const v0, 0x7f0400d4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->setContentView(I)V

    .line 546
    invoke-direct {p0, v3}, Lcom/sec/android/app/samsungapps/SignUpActivity;->a(Z)V

    .line 548
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->q:Landroid/preference/PreferenceScreen;

    .line 549
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->q:Landroid/preference/PreferenceScreen;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 552
    new-instance v0, Lcom/sec/android/app/samsungapps/lk;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->a:Lcom/sec/android/app/samsungapps/lj;

    invoke-direct {v0, p0, p0, v1}, Lcom/sec/android/app/samsungapps/lk;-><init>(Lcom/sec/android/app/samsungapps/SignUpActivity;Landroid/content/Context;Lcom/sec/android/app/samsungapps/lj;)V

    .line 553
    const v1, 0x7f08028c

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/SignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/lk;->a(Ljava/lang/String;)V

    .line 555
    new-instance v1, Lcom/sec/android/app/samsungapps/lk;

    new-instance v2, Lcom/sec/android/app/samsungapps/lj;

    invoke-direct {v2}, Lcom/sec/android/app/samsungapps/lj;-><init>()V

    invoke-direct {v1, p0, p0, v2, v3}, Lcom/sec/android/app/samsungapps/lk;-><init>(Lcom/sec/android/app/samsungapps/SignUpActivity;Landroid/content/Context;Lcom/sec/android/app/samsungapps/lj;B)V

    .line 556
    invoke-virtual {p0, v4}, Lcom/sec/android/app/samsungapps/SignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/lk;->a(Ljava/lang/String;)V

    .line 558
    new-instance v2, Lcom/sec/android/app/samsungapps/lk;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->b:Lcom/sec/android/app/samsungapps/lj;

    invoke-direct {v2, p0, p0, v3}, Lcom/sec/android/app/samsungapps/lk;-><init>(Lcom/sec/android/app/samsungapps/SignUpActivity;Landroid/content/Context;Lcom/sec/android/app/samsungapps/lj;)V

    .line 559
    invoke-virtual {p0, v4}, Lcom/sec/android/app/samsungapps/SignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/lk;->a(Ljava/lang/String;)V

    .line 561
    new-instance v3, Lcom/sec/android/app/samsungapps/lk;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->c:Lcom/sec/android/app/samsungapps/lj;

    invoke-direct {v3, p0, p0, v4}, Lcom/sec/android/app/samsungapps/lk;-><init>(Lcom/sec/android/app/samsungapps/SignUpActivity;Landroid/content/Context;Lcom/sec/android/app/samsungapps/lj;)V

    .line 562
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isKorea()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 563
    const v4, 0x7f08031e

    invoke-virtual {p0, v4}, Lcom/sec/android/app/samsungapps/SignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/samsungapps/lk;->a(Ljava/lang/String;)V

    .line 568
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->q:Landroid/preference/PreferenceScreen;

    invoke-virtual {v4, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 569
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->q:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 570
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->q:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 571
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->q:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 573
    new-instance v0, Lcom/sec/android/app/samsungapps/kv;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/kv;-><init>(Lcom/sec/android/app/samsungapps/SignUpActivity;)V

    invoke-virtual {v3, v0}, Lcom/sec/android/app/samsungapps/lk;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 582
    new-instance v0, Lcom/sec/android/app/samsungapps/kw;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/kw;-><init>(Lcom/sec/android/app/samsungapps/SignUpActivity;)V

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/lk;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 592
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0802a7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f08023d

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/SignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->showPositiveNegativeButton(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    .line 596
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->getPositive()Landroid/widget/Button;

    move-result-object v0

    .line 597
    new-instance v1, Lcom/sec/android/app/samsungapps/kx;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/kx;-><init>(Lcom/sec/android/app/samsungapps/SignUpActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 605
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->c()V

    .line 606
    return-void

    .line 565
    :cond_1
    const v4, 0x7f0802a0

    invoke-virtual {p0, v4}, Lcom/sec/android/app/samsungapps/SignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/samsungapps/lk;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic e(Lcom/sec/android/app/samsungapps/SignUpActivity;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->e()V

    return-void
.end method

.method static synthetic f(Lcom/sec/android/app/samsungapps/SignUpActivity;)Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->s:Z

    return v0
.end method

.method static synthetic g(Lcom/sec/android/app/samsungapps/SignUpActivity;)Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->r:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/android/app/samsungapps/SignUpActivity;)V
    .locals 4

    .prologue
    .line 53
    new-instance v0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080276

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0802c5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->getDialog()Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->r:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    return-void
.end method

.method static synthetic i(Lcom/sec/android/app/samsungapps/SignUpActivity;)V
    .locals 7

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->b:Lcom/sec/android/app/samsungapps/lj;

    iget-boolean v0, v0, Lcom/sec/android/app/samsungapps/lj;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->c:Lcom/sec/android/app/samsungapps/lj;

    iget-boolean v0, v0, Lcom/sec/android/app/samsungapps/lj;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->isStartedWithOSPID()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v0

    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->g:Ljava/lang/String;

    iget v3, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->i:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->j:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->k:I

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->a:Lcom/sec/android/app/samsungapps/lj;

    iget-boolean v6, v6, Lcom/sec/android/app/samsungapps/lj;->a:Z

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->setJoinInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    new-instance v1, Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/LoadingDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->x:Lcom/sec/android/app/samsungapps/LoadingDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->x:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/LoadingDialog;->start()V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->l:Lcom/sec/android/app/samsungapps/vlibrary2/account/SignUpCommand;

    new-instance v2, Lcom/sec/android/app/samsungapps/kz;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/kz;-><init>(Lcom/sec/android/app/samsungapps/SignUpActivity;)V

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SignUpCommand;->joinOld(Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ResultReceiver;)V

    :goto_2
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;-><init>()V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->showErrorPopupUserMustCheck()V

    goto :goto_2
.end method

.method static synthetic j(Lcom/sec/android/app/samsungapps/SignUpActivity;)V
    .locals 3

    .prologue
    .line 53
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->setEmail(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->setPass(Ljava/lang/String;)V

    new-instance v1, Lcom/sec/android/app/samsungapps/ky;

    invoke-direct {v1, p0, p0}, Lcom/sec/android/app/samsungapps/ky;-><init>(Lcom/sec/android/app/samsungapps/SignUpActivity;Lcom/sec/android/app/samsungapps/SignUpActivity;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->login(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;Z)V

    return-void
.end method

.method static synthetic k(Lcom/sec/android/app/samsungapps/SignUpActivity;)V
    .locals 4

    .prologue
    .line 53
    new-instance v0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080276

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0802bd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    return-void
.end method

.method static synthetic l(Lcom/sec/android/app/samsungapps/SignUpActivity;)V
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->y:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->y:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method

.method static synthetic m(Lcom/sec/android/app/samsungapps/SignUpActivity;)Lcom/sec/android/app/samsungapps/LoadingDialog;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->x:Lcom/sec/android/app/samsungapps/LoadingDialog;

    return-object v0
.end method


# virtual methods
.method final a()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 250
    const v0, 0x7f0c02d7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 251
    const v1, 0x7f0c02d9

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/SignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 252
    const v2, 0x7f0c02db

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/SignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    .line 254
    invoke-virtual {v0}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->f:Ljava/lang/String;

    .line 255
    invoke-virtual {v1}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->g:Ljava/lang/String;

    .line 256
    invoke-virtual {v2}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->h:Ljava/lang/String;

    .line 257
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->f:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/SignUpActivity;->a(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v3

    .line 263
    :goto_0
    return v0

    .line 260
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->u:Z

    if-nez v0, :cond_1

    move v0, v3

    .line 261
    goto :goto_0

    .line 263
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected getNegative()Landroid/widget/Button;
    .locals 1

    .prologue
    .line 98
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getNegativeButton(Landroid/app/Activity;)Landroid/widget/Button;

    move-result-object v0

    return-object v0
.end method

.method protected getPositive()Landroid/widget/Button;
    .locals 1

    .prologue
    .line 94
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getPositiveButton(Landroid/app/Activity;)Landroid/widget/Button;

    move-result-object v0

    return-object v0
.end method

.method protected isStartedWithOSPID()Z
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->l:Lcom/sec/android/app/samsungapps/vlibrary2/account/SignUpCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SignUpCommand;->isStartedWithOSPID()Z

    move-result v0

    return v0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 519
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->m:Z

    if-eqz v0, :cond_0

    .line 520
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->onBackPressed()V

    .line 525
    :goto_0
    return-void

    .line 522
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->b()V

    .line 523
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->d()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 80
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 81
    iput-object p0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->n:Landroid/content/Context;

    .line 82
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->readObject(Landroid/content/Intent;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/account/SignUpCommand;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->l:Lcom/sec/android/app/samsungapps/vlibrary2/account/SignUpCommand;

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->l:Lcom/sec/android/app/samsungapps/vlibrary2/account/SignUpCommand;

    if-nez v0, :cond_0

    .line 85
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->finish()V

    .line 91
    :goto_0
    return-void

    .line 89
    :cond_0
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->v:Landroid/view/inputmethod/InputMethodManager;

    .line 90
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->b()V

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 7

    .prologue
    const/4 v6, -0x1

    .line 372
    packed-switch p1, :pswitch_data_0

    .line 420
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    .line 424
    :goto_0
    return-object v0

    .line 376
    :pswitch_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 377
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 378
    const/4 v0, 0x2

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    .line 379
    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 380
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->isStartedWithOSPID()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 381
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v5

    .line 383
    if-eqz v5, :cond_0

    .line 384
    iget-object v3, v5, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->birthDay:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/SignUpActivity;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v5, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->birthMonth:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/SignUpActivity;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v5, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->birthYear:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/SignUpActivity;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 386
    iget-object v3, v5, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->birthYear:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/SignUpActivity;->b(Ljava/lang/String;)I

    move-result v3

    .line 387
    iget-object v4, v5, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->birthMonth:Ljava/lang/String;

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/SignUpActivity;->b(Ljava/lang/String;)I

    move-result v4

    .line 388
    iget-object v5, v5, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->birthDay:Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/app/samsungapps/SignUpActivity;->b(Ljava/lang/String;)I

    move-result v5

    .line 389
    if-eq v3, v6, :cond_0

    if-eq v4, v6, :cond_0

    if-eq v5, v6, :cond_0

    .line 398
    :goto_1
    new-instance v0, Landroid/app/DatePickerDialog;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/SignUpActivity;->p:Landroid/app/DatePickerDialog$OnDateSetListener;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    .line 400
    new-instance v1, Lcom/sec/android/app/samsungapps/ld;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/ld;-><init>(Lcom/sec/android/app/samsungapps/SignUpActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 408
    new-instance v1, Lcom/sec/android/app/samsungapps/le;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/le;-><init>(Lcom/sec/android/app/samsungapps/SignUpActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    goto :goto_0

    :cond_0
    move v4, v0

    move v3, v1

    move v5, v2

    goto :goto_1

    .line 372
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 207
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->onDestroy()V

    .line 208
    return-void
.end method

.method protected showErrorPopupUserMustCheck()V
    .locals 3

    .prologue
    .line 795
    const v0, 0x7f0802d6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/SignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/NotiDialog;->showDialog(Landroid/content/Context;Ljava/lang/String;ZZ)V

    .line 799
    return-void
.end method
