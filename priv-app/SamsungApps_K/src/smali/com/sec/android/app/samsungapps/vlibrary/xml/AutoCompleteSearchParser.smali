.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/AutoCompleteSearchParser;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;
.source "ProGuard"


# instance fields
.field mAutoSearchResultSet:Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/AutoCompleteSearchParser;->mAutoSearchResultSet:Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;

    .line 15
    return-void
.end method


# virtual methods
.method protected onCreateObject(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 3

    .prologue
    .line 25
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResult;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResult;-><init>()V

    .line 26
    const-class v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResult;

    const/4 v2, 0x1

    invoke-virtual {p1, v1, v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->mappingClass(Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 27
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/AutoCompleteSearchParser;->mAutoSearchResultSet:Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->add(Ljava/lang/Object;)Z

    .line 28
    return-void
.end method

.method protected onPostParseError()V
    .locals 0

    .prologue
    .line 20
    return-void
.end method

.method protected onPostParseResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 2

    .prologue
    .line 33
    const-string v0, "endOfList"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->getBool(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/AutoCompleteSearchParser;->mAutoSearchResultSet:Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResultSet;->setCompleted(Z)V

    .line 37
    :cond_0
    return-void
.end method
