.class public abstract Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;
.super Landroid/support/v4/app/Fragment;
.source "ProGuard"


# static fields
.field public static mCurActivity:Landroid/app/Activity;


# instance fields
.field protected mContainer:Landroid/view/ViewGroup;

.field public mCurWidget:Lcom/sec/android/app/samsungapps/widget/CommonWidget;

.field protected mInflater:Landroid/view/LayoutInflater;

.field protected mMainView:Landroid/view/ViewGroup;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->mCurActivity:Landroid/app/Activity;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 23
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 27
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->mCurWidget:Lcom/sec/android/app/samsungapps/widget/CommonWidget;

    .line 29
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->mContainer:Landroid/view/ViewGroup;

    .line 30
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->mMainView:Landroid/view/ViewGroup;

    .line 31
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->mInflater:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public findViewById(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->mMainView:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->mMainView:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 78
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getWidget()Lcom/sec/android/app/samsungapps/widget/CommonWidget;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->mCurWidget:Lcom/sec/android/app/samsungapps/widget/CommonWidget;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 38
    const v0, 0x7f040078

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 39
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->mCurActivity:Landroid/app/Activity;

    .line 40
    const v0, 0x7f0c003a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->mMainView:Landroid/view/ViewGroup;

    .line 41
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->mContainer:Landroid/view/ViewGroup;

    .line 42
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->mInflater:Landroid/view/LayoutInflater;

    .line 44
    return-object v1
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 50
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 51
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 94
    sput-object v1, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->mCurActivity:Landroid/app/Activity;

    .line 95
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->mInflater:Landroid/view/LayoutInflater;

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->mContainer:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 100
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->mContainer:Landroid/view/ViewGroup;

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->mMainView:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->mMainView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 106
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->mMainView:Landroid/view/ViewGroup;

    .line 109
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->mCurWidget:Lcom/sec/android/app/samsungapps/widget/CommonWidget;

    if-eqz v0, :cond_2

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->mCurWidget:Lcom/sec/android/app/samsungapps/widget/CommonWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->release()V

    .line 112
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->mCurWidget:Lcom/sec/android/app/samsungapps/widget/CommonWidget;

    .line 114
    :cond_2
    return-void
.end method

.method protected runDeepLink()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 125
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDeepLink()Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    move-result-object v2

    .line 127
    if-nez v2, :cond_1

    .line 163
    :cond_0
    :goto_0
    return v0

    .line 132
    :cond_1
    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isBannerList()Z

    move-result v3

    if-ne v3, v1, :cond_2

    .line 134
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->ProductSetList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/uiutil/AppsTitle;->getString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->getBannerListID()Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x4

    invoke-static {v0, v3, v4, v2, v5}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;Ljava/lang/String;Ljava/lang/String;I)V

    move v0, v1

    .line 136
    goto :goto_0

    .line 139
    :cond_2
    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isCategoryList()Z

    move-result v3

    if-ne v3, v1, :cond_3

    .line 141
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->getCategoryID()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x17

    invoke-static {v0, v2, v3}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Ljava/lang/String;I)V

    move v0, v1

    .line 142
    goto :goto_0

    .line 145
    :cond_3
    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isDetailPage()Z

    move-result v3

    if-ne v3, v1, :cond_4

    .line 150
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->getDetailID()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x1a

    invoke-static {v0, v2, v3}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Ljava/lang/String;I)V

    move v0, v1

    .line 151
    goto :goto_0

    .line 154
    :cond_4
    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isMainView()Z

    move-result v3

    if-ne v3, v1, :cond_5

    .line 156
    const-string v3, "CommonFragment::runDeepLink::Not Defined"

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 159
    :cond_5
    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->isInterimPage()Z

    move-result v2

    if-ne v2, v1, :cond_0

    .line 161
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/16 v2, 0x27

    invoke-static {v1, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method protected setMainView(I)V
    .locals 3

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->mMainView:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    if-lez p1, :cond_0

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->mMainView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->mInflater:Landroid/view/LayoutInflater;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 59
    :cond_0
    return-void
.end method

.method protected setMainViewAndEmptyView(I)V
    .locals 4

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->mMainView:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->mCurActivity:Landroid/app/Activity;

    if-eqz v0, :cond_0

    if-lez p1, :cond_0

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->mMainView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->mInflater:Landroid/view/LayoutInflater;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->mMainView:Landroid/view/ViewGroup;

    const v1, 0x7f0c0082

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 67
    if-eqz v0, :cond_0

    .line 68
    sget-object v1, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->mCurActivity:Landroid/app/Activity;

    const-string v2, "isa_samsungapps_icon_dummy"

    const-string v3, "drawable"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/samsungapps/uiutil/KnoxResourceManager;->findResource(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 71
    :cond_0
    return-void
.end method

.method public abstract setResizeInMultiWindow()V
.end method

.method public setWidget(Lcom/sec/android/app/samsungapps/widget/CommonWidget;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->mCurWidget:Lcom/sec/android/app/samsungapps/widget/CommonWidget;

    .line 85
    return-void
.end method
