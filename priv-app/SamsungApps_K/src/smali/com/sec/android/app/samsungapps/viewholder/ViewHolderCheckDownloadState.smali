.class public Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;
.super Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateObserver;


# instance fields
.field private a:I

.field private b:J

.field c:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;

.field d:J

.field e:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;Landroid/content/Context;Landroid/view/View;Landroid/graphics/drawable/Drawable;Ljava/lang/String;ZILcom/sec/android/app/samsungapps/view/IContentArrayAdapter;)V
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 41
    invoke-direct/range {p0 .. p8}, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;-><init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;Landroid/content/Context;Landroid/view/View;Landroid/graphics/drawable/Drawable;Ljava/lang/String;ZILcom/sec/android/app/samsungapps/view/IContentArrayAdapter;)V

    .line 152
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->c:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;

    .line 651
    iput-wide v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->d:J

    .line 44
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->a:I

    .line 45
    iput-wide v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->b:J

    .line 48
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->downloadCancel:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->downloadCancel:Landroid/widget/ImageView;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08023d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 50
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->downloadCancel:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/android/app/samsungapps/viewholder/o;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/viewholder/o;-><init>(Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->btnRightly:Landroid/widget/Button;

    if-eqz v0, :cond_1

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->btnRightly:Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/app/samsungapps/viewholder/p;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/viewholder/p;-><init>(Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    :cond_1
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 85
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->removeDLStateObserver(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateObserver;)Z

    .line 86
    return-void
.end method

.method private b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    if-nez v0, :cond_0

    .line 211
    const/4 v0, 0x0

    .line 213
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductID()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected cancelDl(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 641
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->_Context:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/initializer/Global;->cancelDownload(Ljava/lang/String;)Z

    .line 642
    return-void
.end method

.method protected createDownloadHelperFactory(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;
    .locals 3

    .prologue
    .line 198
    invoke-static {}, Lcom/sec/android/app/samsungapps/helper/DownloadHelpFacade;->getInstance()Lcom/sec/android/app/samsungapps/helper/DownloadHelpFacade;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/helper/DownloadHelpFacade;->createDownloadHelperFactory(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/helper/DownloadHelperFactory;

    move-result-object v0

    .line 199
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->_Context:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-interface {v0, v1, p1, v2}, Lcom/sec/android/app/samsungapps/helper/DownloadHelperFactory;->createDownloadCmdManager(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Z)Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;

    move-result-object v0

    .line 200
    return-object v0
.end method

.method protected disableButton(Landroid/widget/Button;)V
    .locals 2

    .prologue
    .line 272
    if-eqz p1, :cond_0

    .line 274
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 275
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->_Context:Landroid/content/Context;

    const v1, 0x1010034

    invoke-virtual {p1, v0, v1}, Landroid/widget/Button;->setTextAppearance(Landroid/content/Context;I)V

    .line 277
    :cond_0
    return-void
.end method

.method protected downloadContent(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V
    .locals 1

    .prologue
    .line 645
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->c:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;

    if-eqz v0, :cond_0

    .line 647
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->c:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->execute()V

    .line 649
    :cond_0
    return-void
.end method

.method protected enableButton(Landroid/widget/Button;)V
    .locals 2

    .prologue
    .line 281
    if-eqz p1, :cond_0

    .line 283
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 284
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->_Context:Landroid/content/Context;

    const v1, 0x1010034

    invoke-virtual {p1, v0, v1}, Landroid/widget/Button;->setTextAppearance(Landroid/content/Context;I)V

    .line 286
    :cond_0
    return-void
.end method

.method protected findDownloadingContent(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;
    .locals 1

    .prologue
    .line 624
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getDLStateItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    move-result-object v0

    return-object v0
.end method

.method protected getWatingString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 629
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->_Context:Landroid/content/Context;

    const v1, 0x7f080296

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected handleDownloading()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/16 v1, 0x8

    const/4 v6, 0x0

    .line 425
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->hideRating()V

    .line 426
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->layout_list_itemly_pricely:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 428
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->layout_list_itemly_pricely:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 431
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressLayout:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 433
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressLayout:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 436
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->layout_list_itemly_rightly:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 438
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->layout_list_itemly_rightly:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 441
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->categoryName:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    .line 443
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->categoryName:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 446
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressStatusText:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    .line 448
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressStatusText:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 451
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->btnRightly:Landroid/widget/Button;

    if-eqz v0, :cond_5

    .line 453
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 456
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->downloadCancel:Landroid/widget/ImageView;

    if-eqz v0, :cond_6

    .line 458
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->downloadCancel:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 461
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressStatusText:Landroid/widget/TextView;

    if-eqz v0, :cond_7

    .line 463
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressAllText:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 466
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressPercentText:Landroid/widget/TextView;

    if-eqz v0, :cond_8

    .line 468
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressPercentText:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 471
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressbar:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_9

    .line 473
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressbar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 476
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progresstext:Landroid/widget/TextView;

    if-eqz v0, :cond_a

    .line 478
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progresstext:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 481
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressAllText:Landroid/widget/TextView;

    if-eqz v0, :cond_b

    .line 483
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressAllText:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 486
    :cond_b
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getDLStateItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    move-result-object v1

    .line 487
    if-eqz v1, :cond_e

    .line 489
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "ViewHolderCheckDownloadState::handleDownloading()  dlState is "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    .line 490
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getTotalSize()I

    move-result v0

    int-to-long v2, v0

    iput-wide v2, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->b:J

    .line 491
    iget-wide v2, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->b:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_d

    .line 492
    iput v6, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->a:I

    .line 506
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressStatusText:Landroid/widget/TextView;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressbar:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_c

    .line 508
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->e:Z

    if-eqz v0, :cond_f

    .line 510
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressStatusText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->_Context:Landroid/content/Context;

    const v2, 0x7f08027f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->setDownloadProgressText(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 511
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressbar:Landroid/widget/ProgressBar;

    const/16 v1, 0x64

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->setDownloadProgressBar(Landroid/widget/ProgressBar;I)V

    .line 532
    :cond_c
    :goto_1
    return-void

    .line 495
    :cond_d
    :try_start_0
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getDownloadedSize()I

    move-result v0

    int-to-long v2, v0

    const-wide/16 v4, 0x64

    mul-long/2addr v2, v4

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getTotalSize()I

    move-result v0

    int-to-long v4, v0

    div-long/2addr v2, v4

    long-to-int v0, v2

    iput v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->a:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 496
    :catch_0
    move-exception v0

    .line 498
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ViewHolderCheckDownloadState::Exception::"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 503
    :cond_e
    const-string v0, "ViewHolderCheckDownloadState::handleDownloading() state is Null!!!!!!!!!!!"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 515
    :cond_f
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressStatusText:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 516
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressPercentText:Landroid/widget/TextView;

    if-eqz v0, :cond_10

    .line 518
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressPercentText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->a:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 519
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "ViewHolderCheckDownloadState::handleDownloading() lastProgress::"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->a:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 520
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "ViewHolderCheckDownloadState::handleDownloading() progressPercentText:: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressPercentText:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 522
    :cond_10
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressAllText:Landroid/widget/TextView;

    const-string v2, "%s"

    new-array v3, v7, [Ljava/lang/Object;

    iget-wide v4, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->formatMBSize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->setDownloadProgressText(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 523
    if-eqz v1, :cond_11

    .line 524
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progresstext:Landroid/widget/TextView;

    const-string v2, "%s / "

    new-array v3, v7, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getDownloadedSize()I

    move-result v4

    int-to-long v4, v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->formatMBSize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->setDownloadProgressText(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 525
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "ViewHolderCheckDownloadState::handleDownloading()  TotalSize:"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " lastProgress:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->a:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " state.getDownloadedSize():"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getDownloadedSize()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    .line 529
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressbar:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->a:I

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->setDownloadProgressBar(Landroid/widget/ProgressBar;I)V

    goto/16 :goto_1

    .line 527
    :cond_11
    const-string v0, "ViewHolderCheckDownloadState::handleDownloading() state is Null!!!!!!!!!!!"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_2
.end method

.method protected handleInstalling()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/16 v3, 0x8

    .line 374
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->hideRating()V

    .line 375
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->layout_list_itemly_pricely:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 377
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->layout_list_itemly_pricely:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 380
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressLayout:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 382
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 385
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->layout_list_itemly_rightly:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 387
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->layout_list_itemly_rightly:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 390
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->btnRightly:Landroid/widget/Button;

    if-eqz v0, :cond_3

    .line 392
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 395
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->categoryName:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    .line 397
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->categoryName:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 400
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->downloadCancel:Landroid/widget/ImageView;

    if-eqz v0, :cond_5

    .line 402
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->downloadCancel:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 405
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressbar:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_6

    .line 406
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressbar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 407
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressbar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 408
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressbar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 411
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressStatusText:Landroid/widget/TextView;

    if-eqz v0, :cond_7

    .line 413
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressStatusText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->_Context:Landroid/content/Context;

    const v2, 0x7f08027f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 414
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressAllText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 415
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progresstext:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 416
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressPercentText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 418
    :cond_7
    return-void
.end method

.method protected handleNotDownloading()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 537
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->showRating()V

    .line 539
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->layout_list_itemly_pricely:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 541
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->layout_list_itemly_pricely:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 544
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->categoryName:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 546
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    if-eqz v0, :cond_8

    .line 548
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getLoadType()Ljava/lang/String;

    move-result-object v0

    .line 549
    if-eqz v0, :cond_7

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, "2"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 551
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->categoryName:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 564
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressLayout:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 566
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 569
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->layout_list_itemly_rightly:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 571
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->layout_list_itemly_rightly:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 574
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressStatusText:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    .line 575
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressStatusText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 578
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressPercentText:Landroid/widget/TextView;

    if-eqz v0, :cond_5

    .line 579
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressPercentText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 582
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressbar:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_6

    .line 583
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressbar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 585
    :cond_6
    return-void

    .line 555
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->categoryName:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 560
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->categoryName:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected handleWating()V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    const/16 v5, 0x8

    .line 295
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->hideRating()V

    .line 296
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->layout_list_itemly_pricely:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 298
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->layout_list_itemly_pricely:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 301
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressLayout:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 303
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressLayout:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 306
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->layout_list_itemly_rightly:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 308
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->layout_list_itemly_rightly:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 311
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressStatusText:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    .line 313
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressStatusText:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 316
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progresstext:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    .line 318
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progresstext:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 321
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->categoryName:Landroid/widget/TextView;

    if-eqz v0, :cond_5

    .line 323
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->categoryName:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 326
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->btnRightly:Landroid/widget/Button;

    if-eqz v0, :cond_6

    .line 328
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->btnRightly:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 331
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->downloadCancel:Landroid/widget/ImageView;

    if-eqz v0, :cond_7

    .line 333
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->downloadCancel:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 334
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->downloadCancel:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 337
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressStatusText:Landroid/widget/TextView;

    if-eqz v0, :cond_8

    .line 339
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressAllText:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 343
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressbar:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_9

    .line 344
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressbar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 345
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressbar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 348
    :cond_9
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getDLStateItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    move-result-object v0

    .line 349
    if-eqz v0, :cond_a

    .line 351
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progresstext:Landroid/widget/TextView;

    if-eqz v1, :cond_a

    .line 352
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progresstext:Landroid/widget/TextView;

    const-string v2, "0.00"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 353
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progresstext:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getDownloadedSize()I

    move-result v0

    int-to-long v2, v0

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->formatMBSize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->setDownloadProgressText(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 357
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressAllText:Landroid/widget/TextView;

    if-eqz v0, :cond_b

    .line 358
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressAllText:Landroid/widget/TextView;

    const-string v1, "0.00"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 359
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressAllText:Landroid/widget/TextView;

    const-string v1, " / %s"

    new-array v2, v4, [Ljava/lang/Object;

    iget-wide v3, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->b:J

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->formatMBSize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->setDownloadProgressText(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 362
    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressStatusText:Landroid/widget/TextView;

    if-eqz v0, :cond_c

    .line 364
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressStatusText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->_Context:Landroid/content/Context;

    const v2, 0x7f080296

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 365
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressPercentText:Landroid/widget/TextView;

    if-eqz v0, :cond_c

    .line 366
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressPercentText:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 369
    :cond_c
    return-void
.end method

.method public launch(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 78
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 79
    invoke-virtual {v0, p2, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->launchApp(Ljava/lang/String;Z)Z

    .line 80
    return v1
.end method

.method protected makeDownloadProgressText(Ljava/lang/Long;I)Ljava/lang/String;
    .locals 5

    .prologue
    .line 226
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->formatMBSize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 228
    if-nez p2, :cond_0

    .line 229
    const/4 v0, 0x0

    .line 235
    :goto_0
    return-object v0

    .line 232
    :cond_0
    const-string v1, "%d%% / %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public onDLStateChanged(Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;)V
    .locals 5

    .prologue
    const-wide/16 v3, 0x0

    const/4 v2, 0x0

    .line 656
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 700
    :cond_0
    :goto_0
    return-void

    .line 659
    :cond_1
    sget-object v0, Lcom/sec/android/app/samsungapps/viewholder/r;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getState()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 662
    :pswitch_0
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->e:Z

    .line 663
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->progressbar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 664
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->d:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 666
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->d:J

    .line 667
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getDownloadedSize()I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getTotalSize()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->onDownloadProgress(JJ)V

    goto :goto_0

    .line 671
    :pswitch_1
    iput-wide v3, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->d:J

    .line 672
    iput v2, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->a:I

    .line 673
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->e:Z

    .line 674
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->handleWating()V

    goto :goto_0

    .line 677
    :pswitch_2
    iput-wide v3, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->d:J

    .line 678
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->e:Z

    .line 679
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->handleInstalling()V

    goto :goto_0

    .line 682
    :pswitch_3
    iput-wide v3, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->d:J

    .line 683
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->handleWating()V

    goto :goto_0

    .line 686
    :pswitch_4
    iput v2, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->a:I

    .line 687
    iput-wide v3, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->d:J

    .line 688
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->e:Z

    .line 689
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->handleWating()V

    goto :goto_0

    .line 694
    :pswitch_5
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->e:Z

    .line 695
    iput-wide v3, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->d:J

    .line 696
    iput v2, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->a:I

    .line 697
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->handleNotDownloading()V

    goto :goto_0

    .line 659
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_4
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_5
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method public onDownloadProgress(JJ)V
    .locals 3

    .prologue
    .line 255
    iput-wide p3, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->b:J

    .line 258
    const-wide/16 v0, 0x64

    mul-long/2addr v0, p1

    :try_start_0
    div-long/2addr v0, p3

    long-to-int v0, v0

    .line 260
    iput v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->a:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 267
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->handleDownloading()V

    .line 268
    return-void

    .line 262
    :catch_0
    move-exception v0

    .line 264
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ViewHolderCheckDownloadState::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public release()V
    .locals 0

    .prologue
    .line 148
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->release()V

    .line 149
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->a()V

    .line 150
    return-void
.end method

.method public setContent(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;)V
    .locals 3

    .prologue
    .line 157
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->a()V

    .line 158
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 159
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->addDLStateObserver(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateObserver;)Z

    .line 160
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->prepareView()V

    .line 161
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->createDownloadHelperFactory(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->c:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    if-eqz v0, :cond_0

    .line 163
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getDLStateItem(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    move-result-object v0

    .line 164
    if-eqz v0, :cond_0

    .line 165
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ViewHolderCheckDownloadState::setContent() dlState is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    .line 166
    sget-object v1, Lcom/sec/android/app/samsungapps/viewholder/r;->a:[I

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getState()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState$IDLStateEnum;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 185
    :cond_0
    :goto_0
    return-void

    .line 168
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->handleDownloading()V

    goto :goto_0

    .line 174
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->handleWating()V

    goto :goto_0

    .line 178
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->handleInstalling()V

    goto :goto_0

    .line 184
    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->handleNotDownloading()V

    goto :goto_0

    .line 166
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method protected setDownloadProgressBar(Landroid/widget/ProgressBar;I)V
    .locals 0

    .prologue
    .line 634
    if-eqz p1, :cond_0

    .line 635
    invoke-virtual {p1, p2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 637
    :cond_0
    return-void
.end method

.method protected setDownloadProgressText(Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 240
    if-eqz p1, :cond_1

    .line 242
    if-nez p2, :cond_0

    .line 244
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->getWatingString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 252
    :goto_0
    return-void

    .line 247
    :cond_0
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 250
    :cond_1
    const-string v0, "ViewHolderCheckDownloadState::view is Null!!!!!!!!!!!!!!"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public showDownloadStatus()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 589
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->empty_loading:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 591
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->empty_loading:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 594
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->layout_list_itemly:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 596
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->layout_list_itemly:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 599
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    if-eqz v0, :cond_2

    .line 600
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->findDownloadingContent(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;

    move-result-object v0

    .line 601
    if-eqz v0, :cond_4

    .line 603
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getTotalSize()I

    move-result v1

    int-to-long v1, v1

    iput-wide v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->b:J

    .line 604
    iget-wide v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->b:J

    cmp-long v1, v1, v4

    if-gtz v1, :cond_3

    .line 605
    iput v3, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->a:I

    .line 606
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->handleNotDownloading()V

    .line 620
    :cond_2
    :goto_0
    return-void

    .line 608
    :cond_3
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getDownloadedSize()I

    move-result v1

    int-to-long v1, v1

    const-wide/16 v3, 0x64

    mul-long/2addr v1, v3

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLState;->getTotalSize()I

    move-result v0

    int-to-long v3, v0

    div-long v0, v1, v3

    long-to-int v0, v0

    .line 609
    iput v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->a:I

    .line 610
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->handleDownloading()V

    goto :goto_0

    .line 615
    :cond_4
    iput-wide v4, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->b:J

    .line 616
    iput v3, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->a:I

    .line 617
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->handleNotDownloading()V

    goto :goto_0
.end method

.method public showProductName()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    if-nez v0, :cond_1

    .line 143
    :cond_0
    :goto_0
    return-void

    .line 98
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->google_tencent_icon:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->google_tencent_icon:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 102
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->textname:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->textname:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->isLinkApp()Z

    move-result v0

    if-ne v0, v2, :cond_3

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->google_tencent_icon:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->_Context:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->CHINA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->checkCountry(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->google_tencent_icon:Landroid/widget/ImageView;

    const v1, 0x7f02012d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 116
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->google_tencent_icon:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 120
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->_Context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.feature.hovering_ui"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-ne v0, v2, :cond_0

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->textname:Landroid/widget/TextView;

    new-instance v1, Lcom/sec/android/app/samsungapps/viewholder/q;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/viewholder/q;-><init>(Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    goto :goto_0

    .line 114
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;->google_tencent_icon:Landroid/widget/ImageView;

    const v1, 0x7f020124

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method
