.class public Lcom/sec/android/app/samsungapps/helper/NoAccountDownloadHelperFactory;
.super Lcom/sec/android/app/samsungapps/helper/NormalDownloadHelperFactory;
.source "ProGuard"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/helper/NormalDownloadHelperFactory;-><init>(Lcom/sec/android/app/samsungapps/downloadhelper/DownloadSingleItemCreator;)V

    .line 10
    return-void
.end method


# virtual methods
.method public createDownloadPrecheckerFactory(Z)Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/IDownloadPrecheckerFactory;
    .locals 2

    .prologue
    .line 16
    new-instance v0, Lcom/sec/android/app/samsungapps/helper/CDownloadPrecheckerWithoutLoginFactory;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/helper/NoAccountDownloadHelperFactory;->createPermissionFactory(Z)Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/IPermissionManagerFactory;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/helper/CDownloadPrecheckerWithoutLoginFactory;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/permissionmanager/IPermissionManagerFactory;)V

    return-object v0
.end method
