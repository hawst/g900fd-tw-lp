.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/comment/ICommentListCommandBuilder;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract deleteComment(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract loadComment()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract loadExpertCommend()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract modifyComment(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract myReviewCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract registerComment()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method
