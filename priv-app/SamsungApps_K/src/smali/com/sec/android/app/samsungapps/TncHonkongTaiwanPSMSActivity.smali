.class public Lcom/sec/android/app/samsungapps/TncHonkongTaiwanPSMSActivity;
.super Lcom/sec/android/app/samsungapps/ActionBarActivity;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC$IPSMSTNCView;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/TncHonkongTaiwanPSMSActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/TncHonkongTaiwanPSMSActivity;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/TncHonkongTaiwanPSMSActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;

    return-object v0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/TncHonkongTaiwanPSMSActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;->onTNCAgreedByUser(Z)V

    .line 108
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onBackPressed()V

    .line 109
    return-void
.end method

.method public onCompleted(Z)V
    .locals 0

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/TncHonkongTaiwanPSMSActivity;->finish()V

    .line 121
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 29
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 34
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/TncHonkongTaiwanPSMSActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->readObject(Landroid/content/Intent;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/TncHonkongTaiwanPSMSActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;

    .line 35
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/TncHonkongTaiwanPSMSActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;

    if-nez v0, :cond_0

    .line 37
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/TncHonkongTaiwanPSMSActivity;->finish()V

    .line 51
    :goto_0
    return-void

    .line 40
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/TncHonkongTaiwanPSMSActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;->invokeCompleted(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC$IPSMSTNCView;)V

    .line 46
    const v0, 0x7f0400ae

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/TncHonkongTaiwanPSMSActivity;->setContentView(I)V

    .line 47
    const v0, 0x7f0c0039

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/TncHonkongTaiwanPSMSActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/TncHonkongTaiwanPSMSActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    .line 48
    const v1, 0x7f0802b0

    const/4 v3, 0x1

    const v5, 0xb0003

    move-object v0, p0

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/TncHonkongTaiwanPSMSActivity;->setActionBarConfiguration(ILcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    .line 50
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/TncHonkongTaiwanPSMSActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;->getProductName()Ljava/lang/String;

    move-result-object v1

    const v0, 0x7f0c026d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/TncHonkongTaiwanPSMSActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/TncHonkongTaiwanPSMSActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;->getFormattedSellingPrice()Ljava/lang/String;

    move-result-object v1

    const v0, 0x7f0c026e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/TncHonkongTaiwanPSMSActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/TncHonkongTaiwanPSMSActivity;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/ValidatePSMSTNC;->getTNCMessage()Ljava/lang/String;

    move-result-object v1

    const v0, 0x7f0c0270

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/TncHonkongTaiwanPSMSActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0802eb

    const v1, 0x7f08023d

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->showPositiveNegativeButton(Landroid/app/Activity;II)V

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getPositiveButton(Landroid/app/Activity;)Landroid/widget/Button;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/lm;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/lm;-><init>(Lcom/sec/android/app/samsungapps/TncHonkongTaiwanPSMSActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getNegativeButton(Landroid/app/Activity;)Landroid/widget/Button;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/ln;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/ln;-><init>(Lcom/sec/android/app/samsungapps/TncHonkongTaiwanPSMSActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method
