.class final Lcom/sec/android/app/samsungapps/vlibrary2/unc/a;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/a;->a:Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceiveResult(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/a;->a:Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->_IListViewState:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;)Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;

    move-result-object v0

    if-nez v0, :cond_0

    .line 87
    const-string v0, "UncList is destroyed"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 101
    :goto_0
    return-void

    .line 91
    :cond_0
    if-eqz p2, :cond_1

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/a;->a:Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->setUncList()V
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->access$100(Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;)V

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/a;->a:Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->_IListViewState:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;)Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;->STATE_LOADCOMPLETE:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;->setState(Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;)V

    goto :goto_0

    .line 98
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/a;->a:Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->_IListViewState:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;)Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;->STATE_LOADFAIL:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;->setState(Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;)V

    goto :goto_0
.end method
