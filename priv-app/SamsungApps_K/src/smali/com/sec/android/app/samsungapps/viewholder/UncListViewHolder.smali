.class public Lcom/sec/android/app/samsungapps/viewholder/UncListViewHolder;
.super Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;
.source "ProGuard"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;Landroid/content/Context;Landroid/view/View;Landroid/graphics/drawable/Drawable;Ljava/lang/String;ZILcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct/range {p0 .. p8}, Lcom/sec/android/app/samsungapps/viewholder/ViewHolderCheckDownloadState;-><init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;Landroid/content/Context;Landroid/view/View;Landroid/graphics/drawable/Drawable;Ljava/lang/String;ZILcom/sec/android/app/samsungapps/view/IContentArrayAdapter;)V

    .line 17
    return-void
.end method


# virtual methods
.method protected prepareView()V
    .locals 0

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/UncListViewHolder;->showProductType()V

    .line 22
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/UncListViewHolder;->showPurchaseStatus()V

    .line 23
    return-void
.end method

.method public showCommonStatus()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/UncListViewHolder;->layout_list_itemly_rightly:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 28
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/UncListViewHolder;->categoryName:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 29
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/UncListViewHolder;->layout_list_itemly_pricely:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 30
    return-void
.end method

.method public showPurchaseStatus()V
    .locals 2

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/UncListViewHolder;->_View:Landroid/view/View;

    if-nez v0, :cond_0

    .line 46
    :goto_0
    return-void

    .line 37
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/UncListViewHolder;->empty_loading:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/UncListViewHolder;->empty_loading:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 40
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/UncListViewHolder;->layout_list_itemly:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 42
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/UncListViewHolder;->showCommonStatus()V

    .line 43
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/UncListViewHolder;->showProductName()V

    .line 44
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/UncListViewHolder;->showProductImg()V

    .line 45
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/UncListViewHolder;->showProductRating()V

    goto :goto_0
.end method
