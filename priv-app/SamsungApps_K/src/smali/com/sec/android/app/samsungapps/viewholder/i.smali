.class final Lcom/sec/android/app/samsungapps/viewholder/i;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;)V
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/viewholder/i;->a:Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/16 v4, 0x9

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 102
    invoke-virtual {p2, v3}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 103
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v4, :cond_0

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/i;->a:Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->_Context:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/i;->a:Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->textname:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewholder/i;->a:Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/view/SamsungAppsHoveringView;->setTextViewHovering(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;)V

    .line 116
    :cond_0
    :goto_0
    return v3

    .line 106
    :cond_1
    invoke-virtual {p2, v3}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 107
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v4, :cond_2

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/i;->a:Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->_Context:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/i;->a:Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->textname:Landroid/widget/TextView;

    invoke-static {v0, v1, v3}, Lcom/sec/android/app/samsungapps/view/SamsungAppsHoveringView;->setFHAnimation(Landroid/content/Context;Landroid/view/View;Z)V

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/i;->a:Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->textname:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/HoverPopupWindow;->isHoverPopupPossible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/i;->a:Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->textname:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setHovered(Z)V

    goto :goto_0

    .line 112
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/i;->a:Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->textname:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setHovered(Z)V

    goto :goto_0
.end method
