.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;


# virtual methods
.method public abstract getCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;
.end method

.method public abstract getGiftCard()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;
.end method

.method public abstract isCoupon()Z
.end method

.method public abstract isGiftCard()Z
.end method
