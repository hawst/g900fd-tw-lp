.class public Lcom/sec/android/app/samsungapps/widget/SettingsDialogListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ProGuard"


# instance fields
.field protected items:Ljava/util/ArrayList;

.field protected mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SettingsDialogListAdapter;->mContext:Landroid/content/Context;

    .line 21
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/widget/SettingsDialogListAdapter;->items:Ljava/util/ArrayList;

    .line 22
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/SettingsDialogListAdapter;->mContext:Landroid/content/Context;

    .line 23
    return-void
.end method


# virtual methods
.method protected getPreferenceDefaultValue()I
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 27
    .line 29
    if-nez p2, :cond_0

    .line 31
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SettingsDialogListAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 32
    const v1, 0x7f04001d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 35
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SettingsDialogListAdapter;->items:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/OptionItem;

    .line 37
    if-eqz v0, :cond_1

    .line 39
    const v1, 0x7f0c0092

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 40
    const v2, 0x7f0c0093

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 42
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/OptionItem;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 44
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/OptionItem;->getSubTitle()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 46
    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 47
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/OptionItem;->getSubTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    :goto_0
    const v0, 0x7f0c0094

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 56
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SettingsDialogListAdapter;->getPreferenceDefaultValue()I

    move-result v1

    if-ne p1, v1, :cond_3

    .line 58
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 65
    :cond_1
    :goto_1
    return-object p2

    .line 51
    :cond_2
    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 62
    :cond_3
    invoke-virtual {v0, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_1
.end method
