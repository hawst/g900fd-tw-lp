.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/BannerClickLogXML;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
.source "ProGuard"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;I)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 17
    const-string v2, "bannerClickLog"

    const-string v3, "2440"

    move-object v0, p0

    move-object v1, p1

    move v4, p3

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 19
    const-string v0, "imei"

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getIMEI()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary/xml/BannerClickLogXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 21
    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->needProductDetail()Z

    move-result v0

    if-ne v0, v7, :cond_1

    .line 23
    const-string v0, "productID"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/BannerClickLogXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 34
    :cond_0
    :goto_0
    const-string v0, "bannerPos"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->getBannerPosByGUI()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/BannerClickLogXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 35
    const-string v0, "bannerIndex"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->getBannerIndex()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/BannerClickLogXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 36
    const-string v1, "interactionBannerYn"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->isInteractionBanner()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "1"

    :goto_1
    invoke-virtual {p0, v1, v0, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/BannerClickLogXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 38
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    .line 39
    if-eqz v0, :cond_4

    .line 41
    const-string v1, "emailID"

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getEmailID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/BannerClickLogXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 47
    :goto_2
    return-void

    .line 25
    :cond_1
    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->needGetProductSetList()Z

    move-result v0

    if-ne v0, v7, :cond_2

    .line 27
    const-string v0, "productSetID"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/BannerClickLogXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 29
    :cond_2
    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->isURLBanner()Z

    move-result v0

    if-ne v0, v7, :cond_0

    .line 31
    const-string v0, "contentCategoryID"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->getBannerLinkURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/BannerClickLogXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 36
    :cond_3
    const-string v0, "0"

    goto :goto_1

    .line 45
    :cond_4
    const-string v0, "emailID"

    const-string v1, ""

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/BannerClickLogXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_2
.end method
