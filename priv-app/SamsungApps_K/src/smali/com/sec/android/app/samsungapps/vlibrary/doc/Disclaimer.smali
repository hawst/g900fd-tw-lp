.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# instance fields
.field bUserAgreed:Z

.field public disclaimerVer:Ljava/lang/String;

.field public fileFlag:I

.field public value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;->value:Ljava/lang/String;

    .line 12
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;->bUserAgreed:Z

    .line 17
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;->value:Ljava/lang/String;

    .line 12
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;->bUserAgreed:Z

    .line 21
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;->disclaimerVer:Ljava/lang/String;

    .line 22
    return-void
.end method


# virtual methods
.method public addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 91
    const-string v0, "value"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;->value:Ljava/lang/String;

    .line 95
    :cond_0
    const-string v0, "fileFlag"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 97
    const/4 v0, 0x0

    invoke-static {p2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->strToInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;->fileFlag:I

    .line 99
    :cond_1
    const-string v0, "disclaimerVer"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 101
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;->disclaimerVer:Ljava/lang/String;

    .line 103
    :cond_2
    return-void
.end method

.method public clear(Lcom/sec/android/app/samsungapps/vlibrary/doc/IDisclaimerDB;)V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;->bUserAgreed:Z

    .line 33
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;->disclaimerVer:Ljava/lang/String;

    .line 34
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;->save(Lcom/sec/android/app/samsungapps/vlibrary/doc/IDisclaimerDB;)V

    .line 35
    return-void
.end method

.method public clearContainer()V
    .locals 0

    .prologue
    .line 109
    return-void
.end method

.method public closeMap()V
    .locals 0

    .prologue
    .line 87
    return-void
.end method

.method public compareVersion(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;->disclaimerVer:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->compareVersion(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;

    move-result-object v0

    return-object v0
.end method

.method public findString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x0

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;->value:Ljava/lang/String;

    return-object v0
.end method

.method public isAgreed()Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;->bUserAgreed:Z

    return v0
.end method

.method public load(Lcom/sec/android/app/samsungapps/vlibrary/doc/IDisclaimerDB;)V
    .locals 1

    .prologue
    .line 26
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IDisclaimerDB;->getDisclaimerAgreed()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;->bUserAgreed:Z

    .line 27
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IDisclaimerDB;->getDisclaimerVersion()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;->disclaimerVer:Ljava/lang/String;

    .line 28
    return-void
.end method

.method public openMap()V
    .locals 0

    .prologue
    .line 81
    return-void
.end method

.method public save(Lcom/sec/android/app/samsungapps/vlibrary/doc/IDisclaimerDB;)V
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;->bUserAgreed:Z

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IDisclaimerDB;->setDisclaimerAgreed(Z)V

    .line 40
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;->disclaimerVer:Ljava/lang/String;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IDisclaimerDB;->setDisclaimerVersion(Ljava/lang/String;)V

    .line 41
    return-void
.end method

.method public setAgree(Lcom/sec/android/app/samsungapps/vlibrary/doc/IDisclaimerDB;Z)V
    .locals 0

    .prologue
    .line 59
    iput-boolean p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;->bUserAgreed:Z

    .line 60
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;->save(Lcom/sec/android/app/samsungapps/vlibrary/doc/IDisclaimerDB;)V

    .line 61
    return-void
.end method

.method public setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 127
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 114
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Disclaimer;->getText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
