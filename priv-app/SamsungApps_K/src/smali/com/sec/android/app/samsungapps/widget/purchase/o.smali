.class final Lcom/sec/android/app/samsungapps/widget/purchase/o;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field protected a:Landroid/content/Context;

.field protected b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;

.field protected c:I

.field protected d:Landroid/widget/TextView;

.field protected e:Landroid/widget/TextView;

.field protected f:Landroid/widget/TextView;

.field protected g:Landroid/widget/TextView;

.field protected h:Landroid/widget/TextView;

.field protected i:Landroid/widget/LinearLayout;

.field protected j:Landroid/widget/CheckBox;

.field protected k:Z

.field final synthetic l:Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;Landroid/content/Context;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 576
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->l:Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 577
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->a:Landroid/content/Context;

    .line 578
    const v0, 0x7f0c020c

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->d:Landroid/widget/TextView;

    .line 579
    const v0, 0x7f0c0246

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->e:Landroid/widget/TextView;

    .line 580
    const v0, 0x7f0c0247

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->f:Landroid/widget/TextView;

    .line 581
    const v0, 0x7f0c0248

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->g:Landroid/widget/TextView;

    .line 582
    const v0, 0x7f0c0244

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->j:Landroid/widget/CheckBox;

    .line 583
    const v0, 0x7f0c0245

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->h:Landroid/widget/TextView;

    .line 584
    const v0, 0x7f0c01f5

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->i:Landroid/widget/LinearLayout;

    .line 585
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .prologue
    .line 588
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->c:I

    .line 589
    return-void
.end method

.method public final a(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;Lcom/sec/android/app/samsungapps/widget/purchase/m;)V
    .locals 11

    .prologue
    const/16 v10, 0x8

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 592
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;

    .line 593
    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/widget/purchase/m;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->k:Z

    .line 594
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080277

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08023a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->h:Landroid/widget/TextView;

    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->g:Landroid/widget/TextView;

    const/4 v5, 0x4

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->j:Landroid/widget/CheckBox;

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setEnabled(Z)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->l:Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->n(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)Z

    move-result v1

    if-ne v1, v2, :cond_1

    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->k:Z

    if-nez v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->j:Landroid/widget/CheckBox;

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setEnabled(Z)V

    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;->getGiftCard()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCard;->Null()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    move-result-object v5

    if-ne v1, v5, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->k:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->g:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->l:Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->q(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)D

    move-result-wide v4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;->isCoupon()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;->getCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->getDiscountType()I

    move-result v0

    if-ne v2, v0, :cond_a

    move v0, v2

    :goto_1
    if-eqz v0, :cond_8

    const-string v0, "-1"

    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->j:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;->isSelected()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 595
    return-void

    .line 594
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;->isCoupon()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;->getCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    move-result-object v5

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->d:Landroid/widget/TextView;

    invoke-interface {v5}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->getCouponName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-interface {v5}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->getDiscountType()I

    move-result v1

    if-ne v2, v1, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->e:Landroid/widget/TextView;

    invoke-interface {v5}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->getDiscountRate()D

    move-result-wide v6

    const-wide/high16 v8, 0x4059000000000000L    # 100.0

    mul-double/2addr v6, v8

    double-to-int v1, v6

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    const-string v6, "%s%%"

    new-array v7, v2, [Ljava/lang/Object;

    aput-object v1, v7, v3

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    :goto_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->f:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->l:Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->o(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)Landroid/content/Context;

    move-result-object v4

    invoke-interface {v5}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->getAvailablePeriodEndDate()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/samsungapps/vlibrary2/util/AppsDateFormat;->getSystemLocalTimeItem(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_4
    const/4 v1, 0x2

    invoke-interface {v5}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->getDiscountType()I

    move-result v6

    if-ne v1, v6, :cond_3

    iget-object v6, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->e:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->k:Z

    if-eqz v0, :cond_5

    invoke-interface {v5}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->getDiscountPrice()D

    move-result-wide v0

    iget-object v8, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->l:Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;

    invoke-static {v8}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->q(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)D

    move-result-wide v8

    sub-double/2addr v0, v8

    :goto_5
    iget-object v8, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->a:Landroid/content/Context;

    invoke-static {v8}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v8

    invoke-interface {v5}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->getCurrencyUnit()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v0, v1, v9}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getFormattedPrice(DLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    :cond_5
    invoke-interface {v5}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->getDiscountPrice()D

    move-result-wide v0

    goto :goto_5

    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;->getGiftCard()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    move-result-object v5

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->d:Landroid/widget/TextView;

    invoke-interface {v5}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;->getGiftCardName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v6, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->e:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->k:Z

    if-eqz v0, :cond_7

    invoke-interface {v5}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;->getBalanceAmount()D

    move-result-wide v0

    iget-object v8, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->l:Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;

    invoke-static {v8}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->q(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)D

    move-result-wide v8

    sub-double/2addr v0, v8

    :goto_6
    iget-object v8, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->a:Landroid/content/Context;

    invoke-static {v8}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v8

    invoke-interface {v5}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;->getCurrencyUnit()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v0, v1, v9}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getFormattedPrice(DLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->f:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->l:Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->p(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)Landroid/content/Context;

    move-result-object v4

    invoke-interface {v5}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;->userExpireDate()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/samsungapps/vlibrary2/util/AppsDateFormat;->getSystemLocalTimeItem(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_7
    invoke-interface {v5}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;->getBalanceAmount()D

    move-result-wide v0

    goto :goto_6

    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "-"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->l:Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;->r(Lcom/sec/android/app/samsungapps/widget/purchase/GiftCardCouponWidget;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v4, v5, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getFormattedPrice(DLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/o;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    :cond_a
    move v0, v3

    goto/16 :goto_1
.end method
