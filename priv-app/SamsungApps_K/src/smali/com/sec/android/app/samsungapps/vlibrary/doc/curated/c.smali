.class final Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/c;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;

.field final synthetic b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/c;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;

    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/c;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceiveResult(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 3

    .prologue
    .line 139
    if-eqz p2, :cond_0

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/c;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/c;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;

    iput-object v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->mCuratedProductListInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/c;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/c;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->createFile()Ljava/io/File;
    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->access$200(Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;)Ljava/io/File;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/c;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->p:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->getResultXml()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->save(Ljava/io/File;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->access$300(Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;Ljava/io/File;Ljava/lang/String;)V

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/c;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->onReceiveSuccess()V
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->access$400(Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;)V

    .line 149
    :goto_0
    return-void

    .line 147
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/c;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->onReceiveFailed()V
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->access$500(Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;)V

    goto :goto_0
.end method
