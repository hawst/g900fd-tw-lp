.class public Lcom/sec/android/app/samsungapps/vlibrary3/notification/NotiInfoCreator;
.super Ljava/lang/Object;
.source "ProGuard"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJZLjava/lang/String;Z)Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;
    .locals 14

    .prologue
    .line 13
    new-instance v7, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    invoke-direct {v7}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;-><init>()V

    .line 14
    move-wide/from16 v0, p4

    invoke-virtual {v7, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->setSize(J)V

    .line 15
    new-instance v8, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    invoke-direct {v8}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;-><init>()V

    .line 16
    move-wide/from16 v0, p6

    invoke-virtual {v8, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->setSize(J)V

    .line 17
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v3, p0

    move-object v4, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move/from16 v11, p8

    move-object/from16 v12, p9

    move/from16 v13, p10

    invoke-direct/range {v2 .. v13}, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;ZZZLjava/lang/String;Z)V

    return-object v2
.end method
