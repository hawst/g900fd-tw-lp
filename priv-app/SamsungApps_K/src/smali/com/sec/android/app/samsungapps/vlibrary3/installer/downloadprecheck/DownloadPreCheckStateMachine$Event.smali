.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum CHECK_AGE_LIMIT_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

.field public static final enum CHECK_AGE_LIMIT_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

.field public static final enum CHECK_NEEDTOSHOW_ALREADY_PURCHASED_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

.field public static final enum CHECK_NEEDTOSHOW_ALREADY_PURCHASED_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

.field public static final enum CHECK_REAL_NAME_AGE_NEED_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

.field public static final enum CHECK_REAL_NAME_AGE_NEED_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

.field public static final enum DETAIL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

.field public static final enum DETAIL_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

.field public static final enum EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

.field public static final enum FREE_STORAGE_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

.field public static final enum FREE_STORAGE_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

.field public static final enum LOGIN_CHECK_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

.field public static final enum LOGIN_CHECK_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

.field public static final enum MULTIPLE_DOWNLOADCOUNT_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

.field public static final enum MULTIPLE_DOWNLOADCOUNT_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

.field public static final enum NET_SIZE_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

.field public static final enum NET_SIZE_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

.field public static final enum P2CONFIRM_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

.field public static final enum P2CONFIRM_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

.field public static final enum PERMISSION_CHECK_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

.field public static final enum PERMISSION_CHECK_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

.field public static final enum TURKEY_CONDITION_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

.field public static final enum TURKEY_CONDITION_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

.field public static final enum VALIDATE_COMPATIBLE_OS_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

.field public static final enum VALIDATE_COMPATIBLE_OS_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 18
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    const-string v1, "EXECUTE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    const-string v1, "LOGIN_CHECK_SUCCESS"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->LOGIN_CHECK_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    const-string v1, "LOGIN_CHECK_FAILED"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->LOGIN_CHECK_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    const-string v1, "TURKEY_CONDITION_OK"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->TURKEY_CONDITION_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    const-string v1, "TURKEY_CONDITION_FAILED"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->TURKEY_CONDITION_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    const-string v1, "DETAIL_FAILED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->DETAIL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    const-string v1, "DETAIL_OK"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->DETAIL_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    const-string v1, "FREE_STORAGE_OK"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->FREE_STORAGE_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    const-string v1, "FREE_STORAGE_FAILED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->FREE_STORAGE_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    const-string v1, "NET_SIZE_FAILED"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->NET_SIZE_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    const-string v1, "NET_SIZE_OK"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->NET_SIZE_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    const-string v1, "MULTIPLE_DOWNLOADCOUNT_OK"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->MULTIPLE_DOWNLOADCOUNT_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    const-string v1, "MULTIPLE_DOWNLOADCOUNT_FAILED"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->MULTIPLE_DOWNLOADCOUNT_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    const-string v1, "P2CONFIRM_OK"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->P2CONFIRM_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    const-string v1, "P2CONFIRM_FAILED"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->P2CONFIRM_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    const-string v1, "CHECK_AGE_LIMIT_FAILED"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->CHECK_AGE_LIMIT_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    const-string v1, "CHECK_AGE_LIMIT_OK"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->CHECK_AGE_LIMIT_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    const-string v1, "VALIDATE_COMPATIBLE_OS_OK"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->VALIDATE_COMPATIBLE_OS_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    const-string v1, "VALIDATE_COMPATIBLE_OS_FAILED"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->VALIDATE_COMPATIBLE_OS_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    const-string v1, "CHECK_REAL_NAME_AGE_NEED_OK"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->CHECK_REAL_NAME_AGE_NEED_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    const-string v1, "CHECK_REAL_NAME_AGE_NEED_FAILED"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->CHECK_REAL_NAME_AGE_NEED_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    const-string v1, "PERMISSION_CHECK_OK"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->PERMISSION_CHECK_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    const-string v1, "PERMISSION_CHECK_FAILED"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->PERMISSION_CHECK_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    const-string v1, "CHECK_NEEDTOSHOW_ALREADY_PURCHASED_OK"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->CHECK_NEEDTOSHOW_ALREADY_PURCHASED_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    const-string v1, "CHECK_NEEDTOSHOW_ALREADY_PURCHASED_FAILED"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->CHECK_NEEDTOSHOW_ALREADY_PURCHASED_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    .line 16
    const/16 v0, 0x19

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->LOGIN_CHECK_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->LOGIN_CHECK_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->TURKEY_CONDITION_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->TURKEY_CONDITION_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->DETAIL_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->DETAIL_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->FREE_STORAGE_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->FREE_STORAGE_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->NET_SIZE_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->NET_SIZE_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->MULTIPLE_DOWNLOADCOUNT_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->MULTIPLE_DOWNLOADCOUNT_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->P2CONFIRM_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->P2CONFIRM_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->CHECK_AGE_LIMIT_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->CHECK_AGE_LIMIT_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->VALIDATE_COMPATIBLE_OS_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->VALIDATE_COMPATIBLE_OS_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->CHECK_REAL_NAME_AGE_NEED_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->CHECK_REAL_NAME_AGE_NEED_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->PERMISSION_CHECK_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->PERMISSION_CHECK_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->CHECK_NEEDTOSHOW_ALREADY_PURCHASED_OK:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->CHECK_NEEDTOSHOW_ALREADY_PURCHASED_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadprecheck/DownloadPreCheckStateMachine$Event;

    return-object v0
.end method
