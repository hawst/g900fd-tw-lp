.class public Lcom/sec/android/app/samsungapps/pushclient/PushMsgReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ProGuard"


# instance fields
.field a:Lcom/sec/android/app/samsungapps/pushclient/PushResponseAppData;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 12
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushMsgReceiver;->a:Lcom/sec/android/app/samsungapps/pushclient/PushResponseAppData;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12

    .prologue
    const/16 v2, 0x64

    const/4 v9, 0x0

    const/4 v11, 0x1

    .line 109
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    .line 111
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 115
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->isUsablePushService(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 286
    :cond_0
    :goto_0
    return-void

    .line 123
    :cond_1
    sget-object v1, Lcom/sec/android/app/samsungapps/pushclient/PushService;->SAMSUNG_APPS_ID:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v11, :cond_b

    .line 125
    const-string v0, "notificationType"

    invoke-virtual {p2, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 127
    if-ne v0, v2, :cond_a

    .line 129
    const-string v0, "appId"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 130
    sget-object v0, Lcom/sec/android/app/samsungapps/pushclient/PushService;->SAMSUNG_APPS_ID:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eq v0, v11, :cond_2

    .line 132
    const-string v0, "Invalid Notification Type"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 138
    :cond_2
    const-string v0, "msg"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 139
    const-string v0, "ack"

    invoke-virtual {p2, v0, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 140
    const-string v0, "notificationId"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 141
    const-string v0, "sender"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 142
    const-string v0, "appData"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 143
    const-string v6, "timeStamp"

    const-wide/16 v7, 0x0

    invoke-virtual {p2, v6, v7, v8}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    .line 144
    const-string v7, "sessionInfo"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 145
    const-string v8, "connectionTerm"

    invoke-virtual {p2, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    .line 147
    const-string v9, "Received push msg from server."

    invoke-static {p0, v9}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 148
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "msg:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {p0, v9}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 149
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "appID:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v9, "bAck:"

    invoke-direct {v2, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 151
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "notiID:"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 152
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "sender:"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 153
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "appData:"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 154
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "timestamp:"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 155
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "sessionInfo:"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "connectionTerm:"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 217
    if-nez v1, :cond_3

    .line 218
    const-string v1, ""

    .line 220
    :cond_3
    if-nez v0, :cond_4

    .line 221
    const-string v0, ""

    .line 224
    :cond_4
    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    .line 225
    sget-object v2, Lcom/sec/android/app/samsungapps/pushclient/PushService;->SENDER_UPPER_STR_SAMSUNGAPPS_WEB:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-ne v2, v11, :cond_9

    .line 229
    if-eqz v0, :cond_6

    new-instance v1, Lcom/sec/android/app/samsungapps/pushclient/PushResponseAppData;

    invoke-direct {v1, v0}, Lcom/sec/android/app/samsungapps/pushclient/PushResponseAppData;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/PushMsgReceiver;->a:Lcom/sec/android/app/samsungapps/pushclient/PushResponseAppData;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/PushMsgReceiver;->a:Lcom/sec/android/app/samsungapps/pushclient/PushResponseAppData;

    const-string v1, "requestType"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/pushclient/PushResponseAppData;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/PushMsgReceiver;->a:Lcom/sec/android/app/samsungapps/pushclient/PushResponseAppData;

    const-string v2, "productID"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/pushclient/PushResponseAppData;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/pushclient/PushMsgReceiver;->a:Lcom/sec/android/app/samsungapps/pushclient/PushResponseAppData;

    const-string v3, "mcc"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/pushclient/PushResponseAppData;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/pushclient/PushMsgReceiver;->a:Lcom/sec/android/app/samsungapps/pushclient/PushResponseAppData;

    const-string v4, "accountName"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/samsungapps/pushclient/PushResponseAppData;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/pushclient/PushMsgReceiver;->a:Lcom/sec/android/app/samsungapps/pushclient/PushResponseAppData;

    const-string v5, "GUID"

    invoke-virtual {v4, v5}, Lcom/sec/android/app/samsungapps/pushclient/PushResponseAppData;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/pushclient/PushMsgReceiver;->a:Lcom/sec/android/app/samsungapps/pushclient/PushResponseAppData;

    const-string v6, "version"

    invoke-virtual {v5, v6}, Lcom/sec/android/app/samsungapps/pushclient/PushResponseAppData;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "######### App Data #########"

    invoke-static {p0, v6}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "requestType\t:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v6}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "productID\t\t:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v6}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "mcc\t\t\t:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "accountName\t:"

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "GUID\t\t\t:"

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "Version\t\t:"

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "############################"

    invoke-static {p0, v2}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v0, :cond_5

    if-nez v1, :cond_7

    if-nez v4, :cond_7

    :cond_5
    const-string v0, "ResponseAppData Parsing Error"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    const-string v0, "ResponseAppData NULL"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-direct {v0, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "extra_command"

    const-string v6, "cmd_downloading"

    invoke-virtual {v0, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "extra_product_id"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "extra_account_name"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    if-eqz v4, :cond_8

    if-eqz v5, :cond_8

    const-string v1, "extra_guid"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "extra_version"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_8
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0

    .line 232
    :cond_9
    sget-object v2, Lcom/sec/android/app/samsungapps/pushclient/PushService;->SENDER_UPPER_STR_SAMSUNGAPPS_ADMIN:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v11, :cond_0

    .line 238
    invoke-virtual {p0, v0, v3, p1}, Lcom/sec/android/app/samsungapps/pushclient/PushMsgReceiver;->startAdminDeeplinkService(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 242
    :cond_a
    const/16 v1, 0x65

    if-ne v0, v1, :cond_0

    .line 244
    const-string v0, "received result of notification ack from server"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 246
    const-string v0, "appId"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 248
    const-string v1, "ackResult"

    invoke-virtual {p2, v1, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 250
    const-string v2, "notificationId"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 253
    const-string v3, "######### SPP App Result #########"

    invoke-static {p0, v3}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 254
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mAppId\t\t:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 255
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "bSuccess\t\t:"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 256
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "mNotiId\t\t:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 257
    const-string v0, "##################################"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 263
    :cond_b
    const-string v1, "intent.action.pushservice.start"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v11, :cond_c

    .line 265
    const-string v0, "intent.action.pushservice.start"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 267
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 268
    const-string v1, "extra_command"

    const-string v2, "cmd_registration"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 270
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0

    .line 275
    :cond_c
    const-string v1, "intent.action.pushservice.end"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v11, :cond_0

    .line 277
    const-string v0, "intent.action.pushservice.end"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 279
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 280
    const-string v1, "extra_command"

    const-string v2, "cmd_deregistration"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 282
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0
.end method

.method public startAdminDeeplinkService(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 70
    if-nez p1, :cond_0

    .line 72
    const-string v0, "PushMsgReceiver::startAdminDeeplinkService::AppData is invalid"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    :goto_0
    return-void

    .line 76
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    .line 81
    const-string v0, "######### App Data #########"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "uri"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    const-string v0, "############################"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-direct {v0, p3, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 91
    const-string v1, "extra_command"

    const-string v2, "cmd_deeplink"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 93
    const-string v1, "extra_deeplink_uri"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 94
    const-string v1, "extra_noti_msg"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 96
    invoke-virtual {p3, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method
