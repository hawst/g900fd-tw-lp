.class public Lcom/sec/android/app/samsungapps/vlibrary2/price/Price;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/price/IPrice;


# instance fields
.field private _DiscountPrice:D

.field private _NormalPrice:D

.field private _bDiscount:Z


# direct methods
.method public constructor <init>(DDZ)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-wide p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/Price;->_NormalPrice:D

    .line 11
    iput-wide p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/Price;->_DiscountPrice:D

    .line 12
    iput-boolean p5, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/Price;->_bDiscount:Z

    .line 13
    return-void
.end method


# virtual methods
.method public getDiscountPrice()D
    .locals 2

    .prologue
    .line 26
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/Price;->_DiscountPrice:D

    return-wide v0
.end method

.method public getNormalPrice()D
    .locals 2

    .prologue
    .line 31
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/Price;->_NormalPrice:D

    return-wide v0
.end method

.method public getSellingPrice()D
    .locals 2

    .prologue
    .line 16
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/Price;->_bDiscount:Z

    if-eqz v0, :cond_0

    .line 18
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/Price;->_DiscountPrice:D

    .line 21
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/Price;->_NormalPrice:D

    goto :goto_0
.end method

.method public isDiscounted()Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/price/Price;->_bDiscount:Z

    return v0
.end method
