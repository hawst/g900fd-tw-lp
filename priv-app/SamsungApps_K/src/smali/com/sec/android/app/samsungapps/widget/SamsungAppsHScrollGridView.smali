.class public Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;
.super Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;
.source "ProGuard"


# static fields
.field static final a:[[I

.field public static defaultsize:I

.field public static orientation:I


# instance fields
.field protected adapter:Landroid/widget/BaseAdapter;

.field protected adapterView:Ljava/util/ArrayList;

.field private b:Landroid/content/Context;

.field protected contentLayout:Landroid/widget/RelativeLayout;

.field protected onItemClickListener:Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView$OnItemClickListenerCustome;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 22
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->orientation:I

    .line 37
    new-array v0, v3, [[I

    const/4 v1, 0x0

    new-array v2, v3, [I

    fill-array-data v2, :array_0

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-array v2, v3, [I

    fill-array-data v2, :array_1

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->a:[[I

    return-void

    nop

    :array_0
    .array-data 4
        0xa
        0x1
    .end array-data

    :array_1
    .array-data 4
        0xc
        0x7
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 45
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;-><init>(Landroid/content/Context;)V

    .line 20
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->b:Landroid/content/Context;

    .line 24
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->contentLayout:Landroid/widget/RelativeLayout;

    .line 25
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->adapterView:Ljava/util/ArrayList;

    .line 46
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->b:Landroid/content/Context;

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 50
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->b:Landroid/content/Context;

    .line 24
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->contentLayout:Landroid/widget/RelativeLayout;

    .line 25
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->adapterView:Ljava/util/ArrayList;

    .line 51
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->b:Landroid/content/Context;

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 55
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/SmoothHorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 20
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->b:Landroid/content/Context;

    .line 24
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->contentLayout:Landroid/widget/RelativeLayout;

    .line 25
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->adapterView:Ljava/util/ArrayList;

    .line 56
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->b:Landroid/content/Context;

    .line 57
    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->b:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 89
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 91
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->contentLayout:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_0

    .line 92
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->contentLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 93
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->contentLayout:Landroid/widget/RelativeLayout;

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v3, -0x1

    const/high16 v4, 0x43200000    # 160.0f

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v4

    float-to-int v0, v0

    invoke-direct {v2, v3, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method


# virtual methods
.method public clearAll()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 185
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->adapter:Landroid/widget/BaseAdapter;

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->adapter:Landroid/widget/BaseAdapter;

    check-cast v0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->clear()V

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->adapter:Landroid/widget/BaseAdapter;

    check-cast v0, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->release()V

    .line 188
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->adapter:Landroid/widget/BaseAdapter;

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->contentLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->contentLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 193
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->contentLayout:Landroid/widget/RelativeLayout;

    .line 196
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->adapterView:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->adapterView:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 198
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->adapterView:Ljava/util/ArrayList;

    .line 200
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->removeAllViews()V

    .line 201
    return-void
.end method

.method protected getRules(I)[I
    .locals 2

    .prologue
    .line 181
    if-lez p1, :cond_0

    sget-object v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->a:[[I

    rem-int/lit8 v1, p1, 0x2

    aget-object v0, v0, v1

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->adapter:Landroid/widget/BaseAdapter;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->adapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public orientationsize()V
    .locals 2

    .prologue
    .line 98
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 99
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 100
    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 102
    if-ge v1, v0, :cond_0

    .line 103
    div-int/lit8 v0, v1, 0x2

    add-int/lit8 v0, v0, -0x2

    sput v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->defaultsize:I

    .line 104
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->orientation:I

    .line 109
    :goto_0
    return-void

    .line 106
    :cond_0
    div-int/lit8 v0, v1, 0x3

    add-int/lit8 v0, v0, -0x2

    sput v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->defaultsize:I

    .line 107
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->orientation:I

    goto :goto_0
.end method

.method public setAdapter(Landroid/widget/BaseAdapter;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 66
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->adapter:Landroid/widget/BaseAdapter;

    .line 67
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->contentLayout:Landroid/widget/RelativeLayout;

    .line 68
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->adapterView:Ljava/util/ArrayList;

    .line 70
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->a()V

    .line 71
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->update()V

    .line 72
    return-void
.end method

.method public setAdapter(Landroid/widget/BaseAdapter;Landroid/widget/RelativeLayout;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->adapter:Landroid/widget/BaseAdapter;

    .line 76
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->contentLayout:Landroid/widget/RelativeLayout;

    .line 77
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->adapterView:Ljava/util/ArrayList;

    .line 79
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->a()V

    .line 80
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->update()V

    .line 81
    return-void
.end method

.method public setOnItemClickListener(Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView$OnItemClickListenerCustome;)V
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->onItemClickListener:Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView$OnItemClickListenerCustome;

    .line 35
    return-void
.end method

.method public update()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v2, 0x0

    const/4 v8, 0x1

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->b:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->adapterView:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->contentLayout:Landroid/widget/RelativeLayout;

    if-nez v0, :cond_1

    .line 169
    :cond_0
    :goto_0
    return-void

    .line 117
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->b:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 119
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->getSize()I

    move-result v4

    .line 121
    if-eqz v0, :cond_2

    if-gtz v4, :cond_3

    .line 122
    :cond_2
    const-string v0, "initGridPage failed"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 126
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->contentLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->contentLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->removeAllViews()V

    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->removeAllViews()V

    .line 127
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->orientationsize()V

    move v3, v2

    .line 129
    :goto_1
    if-ge v3, v4, :cond_9

    .line 130
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    sget v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->defaultsize:I

    const/4 v1, -0x2

    invoke-direct {v5, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->adapterView:Ljava/util/ArrayList;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->adapterView:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v4, :cond_7

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->adapterView:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 134
    add-int/lit8 v1, v3, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    .line 141
    :goto_2
    add-int/lit8 v1, v3, 0x1

    rem-int/lit8 v1, v1, 0x2

    if-ne v1, v8, :cond_5

    .line 142
    sget v1, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->defaultsize:I

    div-int/lit8 v6, v3, 0x2

    mul-int/2addr v1, v6

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->addStickyPosition(I)V

    .line 145
    :cond_5
    if-eqz v0, :cond_6

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->onItemClickListener:Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView$OnItemClickListenerCustome;

    if-eqz v1, :cond_6

    .line 146
    invoke-virtual {v0, v8}, Landroid/view/View;->setClickable(Z)V

    .line 147
    invoke-virtual {v0, v8}, Landroid/view/View;->setFocusable(Z)V

    .line 149
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .line 150
    new-instance v6, Lcom/sec/android/app/samsungapps/widget/aj;

    invoke-direct {v6, p0, v1}, Lcom/sec/android/app/samsungapps/widget/aj;-><init>(Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;I)V

    invoke-virtual {v0, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 158
    :cond_6
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->getRules(I)[I

    move-result-object v6

    .line 160
    if-eqz v6, :cond_8

    if-lez v3, :cond_8

    move v1, v2

    .line 161
    :goto_3
    array-length v7, v6

    if-ge v1, v7, :cond_8

    .line 162
    aget v7, v6, v1

    invoke-virtual {v5, v7, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 161
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 136
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->adapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v0, v3, v9, v9}, Landroid/widget/BaseAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 137
    add-int/lit8 v1, v3, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    .line 138
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->adapterView:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 165
    :cond_8
    invoke-virtual {v0, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 166
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->contentLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 129
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 168
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->contentLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsHScrollGridView;->addView(Landroid/view/View;)V

    goto/16 :goto_0
.end method
