.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseCouponListRequestXML;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/CouponListRequestXML;
.source "ProGuard"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/requestbuilder/IPurchaseCouponQueryCondition;)V
    .locals 2

    .prologue
    .line 19
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CouponListRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;I)V

    .line 20
    const-string v0, "purchaseCouponList2Notc"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseCouponListRequestXML;->setReqName(Ljava/lang/String;)V

    .line 21
    const-string v0, "2231"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseCouponListRequestXML;->setReqID(Ljava/lang/String;)V

    .line 23
    const-string v0, "productID"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/requestbuilder/IPurchaseCouponQueryCondition;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseCouponListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    const-string v1, "allCouponYn"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/requestbuilder/IPurchaseCouponQueryCondition;->isAllCoupon()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "1"

    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseCouponListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    return-void

    .line 24
    :cond_0
    const-string v0, "0"

    goto :goto_0
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;ZI)V
    .locals 2

    .prologue
    .line 8
    invoke-direct {p0, p1, p4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/CouponListRequestXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;I)V

    .line 9
    const-string v0, "purchaseCouponList2Notc"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseCouponListRequestXML;->setReqName(Ljava/lang/String;)V

    .line 10
    const-string v0, "2231"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseCouponListRequestXML;->setReqID(Ljava/lang/String;)V

    .line 13
    const-string v0, "productID"

    invoke-virtual {p0, v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseCouponListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    const-string v1, "allCouponYn"

    if-eqz p3, :cond_0

    const-string v0, "1"

    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PurchaseCouponListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    return-void

    .line 14
    :cond_0
    const-string v0, "0"

    goto :goto_0
.end method
