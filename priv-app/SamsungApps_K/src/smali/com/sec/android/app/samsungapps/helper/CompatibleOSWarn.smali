.class public Lcom/sec/android/app/samsungapps/helper/CompatibleOSWarn;
.super Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;
.source "ProGuard"


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;-><init>(Landroid/content/Context;)V

    .line 20
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/helper/CompatibleOSWarn;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 21
    return-void
.end method

.method private a()Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/helper/CompatibleOSWarn;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    .line 70
    :goto_0
    return v0

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/helper/CompatibleOSWarn;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->compatibleOS:Ljava/lang/String;

    .line 50
    if-nez v0, :cond_1

    .line 51
    const-string v0, "7"

    .line 53
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/helper/CompatibleOSWarn;->_Context:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getNetHeaderInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getSamsungApps()Lcom/sec/android/app/samsungapps/vlibrary/doc/SamsungApps;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SamsungApps;->getOpenAPIVersion()Ljava/lang/String;

    move-result-object v3

    .line 54
    :try_start_0
    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 59
    invoke-static {v3}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v6

    .line 61
    cmpg-double v0, v6, v4

    if-gez v0, :cond_2

    move v0, v2

    .line 63
    goto :goto_0

    .line 68
    :catch_0
    move-exception v0

    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v1

    .line 70
    goto :goto_0
.end method


# virtual methods
.method protected matchCondition()Z
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/helper/CompatibleOSWarn;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 40
    const/4 v0, 0x1

    .line 42
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onInvokePopup(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 25
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/NotiCommand;

    const/16 v1, 0x1f41

    invoke-static {p1, v1}, Lcom/sec/android/app/samsungapps/NotiDialog;->getMessage(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0802ef

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f08023d

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/commands/NotiCommand;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/helper/CompatibleOSWarn;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/helper/g;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/helper/g;-><init>(Lcom/sec/android/app/samsungapps/helper/CompatibleOSWarn;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/commands/NotiCommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 33
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/helper/CompatibleOSWarn;->invokeCompleted()V

    .line 34
    return-void
.end method
