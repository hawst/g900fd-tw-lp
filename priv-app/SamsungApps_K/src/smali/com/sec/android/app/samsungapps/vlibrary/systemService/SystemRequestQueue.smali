.class public Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestQueue;
.super Ljava/util/ArrayList;
.source "ProGuard"


# static fields
.field private static final serialVersionUID:J = -0x48f32d742e02f17cL


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    return-void
.end method


# virtual methods
.method public addQueue(Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequest;)V
    .locals 0

    .prologue
    .line 14
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestQueue;->add(Ljava/lang/Object;)Z

    .line 15
    return-void
.end method

.method public readAt(I)Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequest;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestQueue;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequest;

    return-object v0
.end method

.method public take()Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequest;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestQueue;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 25
    const/4 v0, 0x0

    .line 29
    :goto_0
    return-object v0

    .line 27
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestQueue;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequest;

    .line 28
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/systemService/SystemRequestQueue;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method
