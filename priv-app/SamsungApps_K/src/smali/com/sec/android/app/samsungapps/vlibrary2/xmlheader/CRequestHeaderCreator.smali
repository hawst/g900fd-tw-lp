.class public Lcom/sec/android/app/samsungapps/vlibrary2/xmlheader/CRequestHeaderCreator;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private _FakeModel:Ljava/lang/String;

.field private _NetHeader:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;)V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/xmlheader/CRequestHeaderCreator;->_NetHeader:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    .line 17
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/xmlheader/CRequestHeaderCreator;->_NetHeader:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    .line 18
    return-void
.end method


# virtual methods
.method public setFakeModel(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 22
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/xmlheader/CRequestHeaderCreator;->_FakeModel:Ljava/lang/String;

    .line 23
    return-void
.end method

.method public writeCloseSamsungHeader(Lorg/xmlpull/v1/XmlSerializer;)V
    .locals 2

    .prologue
    .line 59
    const-string v0, ""

    const-string v1, "SamsungProtocol"

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 60
    return-void
.end method

.method public writeOpenSamsungHeader(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 27
    const-string v0, ""

    const-string v1, "SamsungProtocol"

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 29
    const-string v0, ""

    const-string v1, "networkType"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/xmlheader/CRequestHeaderCreator;->_NetHeader:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getNetInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/NetworkInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetworkInfo;->getNetworkType()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 30
    const-string v0, ""

    const-string v1, "version2"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/xmlheader/CRequestHeaderCreator;->_NetHeader:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getPlatformKeyVersion()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 32
    const-string v0, ""

    const-string v1, "lang"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/xmlheader/CRequestHeaderCreator;->_NetHeader:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getLang()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 33
    const-string v0, ""

    const-string v1, "openApiVersion"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/xmlheader/CRequestHeaderCreator;->_NetHeader:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getSamsungApps()Lcom/sec/android/app/samsungapps/vlibrary/doc/SamsungApps;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SamsungApps;->getOpenAPIVersion()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 34
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/xmlheader/CRequestHeaderCreator;->_FakeModel:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/xmlheader/CRequestHeaderCreator;->_FakeModel:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_1

    .line 36
    const-string v0, ""

    const-string v1, "deviceModel"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/xmlheader/CRequestHeaderCreator;->_FakeModel:Ljava/lang/String;

    invoke-interface {p1, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 43
    :goto_0
    const-string v0, ""

    const-string v1, "mcc"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/xmlheader/CRequestHeaderCreator;->_NetHeader:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMCC()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 44
    const-string v0, ""

    const-string v1, "mnc"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/xmlheader/CRequestHeaderCreator;->_NetHeader:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMNC()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 45
    const-string v0, ""

    const-string v1, "csc"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/xmlheader/CRequestHeaderCreator;->_NetHeader:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getCSC()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 47
    const-string v0, ""

    const-string v1, "odcVersion"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/xmlheader/CRequestHeaderCreator;->_NetHeader:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getSamsungApps()Lcom/sec/android/app/samsungapps/vlibrary/doc/SamsungApps;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SamsungApps;->getODCVersion()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 48
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    const-string v0, ""

    const-string v1, "gosversion"

    invoke-interface {p1, v0, v1, p2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 53
    :cond_0
    const-string v0, ""

    const-string v1, "version"

    const-string v2, "4.0"

    invoke-interface {p1, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 54
    const-string v0, ""

    const-string v1, "filter"

    const-string v2, "1"

    invoke-interface {p1, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 55
    return-void

    .line 40
    :cond_1
    const-string v0, ""

    const-string v1, "deviceModel"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/xmlheader/CRequestHeaderCreator;->_NetHeader:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getModelName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0
.end method
