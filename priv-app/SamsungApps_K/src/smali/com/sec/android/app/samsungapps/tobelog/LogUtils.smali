.class public Lcom/sec/android/app/samsungapps/tobelog/LogUtils;
.super Ljava/lang/Object;
.source "ProGuard"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static findEntryPoint(Landroid/app/Activity;)Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;
    .locals 2

    .prologue
    .line 60
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "deepLinkAppName"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 62
    if-eqz v0, :cond_0

    const-string v1, "GameHub"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 64
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->SAMSUNG_GAMES:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    .line 66
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogUtils;->findRecentServiceCode(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    move-result-object v0

    goto :goto_0
.end method

.method public static findRecentServiceCode(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 17
    const-string v0, "activity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 18
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/app/ActivityManager;->getRecentTasks(II)Ljava/util/List;

    move-result-object v4

    .line 20
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_0

    .line 21
    const-string v0, "No recent task to find Service code of ToBeLog"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V

    .line 22
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->LAUNCHER:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    .line 56
    :goto_0
    return-object v0

    :cond_0
    move v3, v2

    .line 26
    :goto_1
    :try_start_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_4

    .line 29
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RecentTaskInfo;

    .line 30
    iget-object v0, v0, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    .line 32
    if-nez v5, :cond_2

    .line 33
    const-string v0, "No caller package Name"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V

    .line 28
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 37
    :cond_2
    invoke-static {}, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->values()[Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    move-result-object v6

    array-length v7, v6

    move v1, v2

    :goto_2
    if-ge v1, v7, :cond_1

    aget-object v0, v6, v1

    .line 38
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->packageName()Ljava/lang/String;

    move-result-object v8

    .line 40
    if-eqz v8, :cond_3

    .line 41
    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 45
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Found caller : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 50
    :catch_0
    move-exception v0

    .line 51
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "LogUtils::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 52
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->LAUNCHER:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    goto :goto_0

    .line 37
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 55
    :cond_4
    const-string v0, "Could not find caller package"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V

    .line 56
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->LAUNCHER:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    goto :goto_0
.end method
