.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/AvailableCountryListRequestXML;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
.source "ProGuard"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;I)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 8
    const-string v2, "availableCountryList"

    const-string v3, "2330"

    move-object v0, p0

    move-object v1, p1

    move v4, p2

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 10
    const-string v0, "deviceid"

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getModelName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/AvailableCountryListRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    return-void
.end method


# virtual methods
.method public getURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/AvailableCountryListRequestXML;->_IConnectURLGetter:Lcom/sec/android/app/samsungapps/vlibrary2/xmlheader/IURLGetter;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/xmlheader/IURLGetter;->getHubURL()Ljava/lang/String;

    move-result-object v0

    .line 16
    return-object v0
.end method
