.class public Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManagerStateMachine;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;
.source "ProGuard"


# static fields
.field private static a:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManagerStateMachine;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;-><init>()V

    .line 32
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManagerStateMachine;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManagerStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManagerStateMachine;

    if-nez v0, :cond_0

    .line 16
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManagerStateMachine;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManagerStateMachine;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManagerStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManagerStateMachine;

    .line 18
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManagerStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManagerStateMachine;

    return-object v0
.end method


# virtual methods
.method protected entry(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 3

    .prologue
    .line 78
    const-string v0, "InitializeManagerStateMachine"

    const-string v1, "entry"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManagerStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 79
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/g;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 101
    :goto_0
    :pswitch_0
    return-void

    .line 84
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;->REQ_NETWORK_STATE_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 87
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;->CHECK_COUNTRY_INFO:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 90
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 91
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 94
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;->REQ_COUNTRY_SEARCH:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 97
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;->NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 98
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 79
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;)Z
    .locals 3

    .prologue
    .line 24
    const-string v0, "InitializeManagerStateMachine"

    const-string v1, "execute"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManagerStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 25
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/g;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 69
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    return v0

    .line 28
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/g;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 31
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$State;->NETWORK_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 36
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/g;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 39
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 42
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$State;->CHECK_COUNTRY_INFO:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 47
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/g;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    goto :goto_0

    .line 50
    :pswitch_7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 53
    :pswitch_8
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$State;->COUNTRY_SEARCH:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 60
    :pswitch_9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/g;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_4

    goto :goto_0

    .line 63
    :pswitch_a
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 66
    :pswitch_b
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManagerStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 25
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_6
        :pswitch_0
        :pswitch_9
    .end packed-switch

    .line 28
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch

    .line 36
    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 47
    :pswitch_data_3
    .packed-switch 0x4
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 60
    :pswitch_data_4
    .packed-switch 0x6
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public bridge synthetic execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 9
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManagerStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Event;)Z

    move-result v0

    return v0
.end method

.method protected exit(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 3

    .prologue
    .line 105
    const-string v0, "InitializeManagerStateMachine"

    const-string v1, "exit"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/InitializeManagerStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 106
    return-void
.end method
