.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum CLEAR_RETRYCOUNT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

.field public static final enum INCREASE_RETRYCOUNT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

.field public static final enum NOTIFY_CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

.field public static final enum NOTIFY_DOWNLOADDONE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

.field public static final enum NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

.field public static final enum REQUEST_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

.field public static final enum REQUEST_DOWNLOAD:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

.field public static final enum REQUEST_URL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

.field public static final enum START_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

.field public static final enum STOP_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 20
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    const-string v1, "REQUEST_DOWNLOAD"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;->REQUEST_DOWNLOAD:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    const-string v1, "REQUEST_CANCEL"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;->REQUEST_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    const-string v1, "NOTIFY_CANCELED"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;->NOTIFY_CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    const-string v1, "NOTIFY_DOWNLOADDONE"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;->NOTIFY_DOWNLOADDONE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    const-string v1, "CLEAR_RETRYCOUNT"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;->CLEAR_RETRYCOUNT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    const-string v1, "INCREASE_RETRYCOUNT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;->INCREASE_RETRYCOUNT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    const-string v1, "START_TIMER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;->START_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    const-string v1, "STOP_TIMER"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;->STOP_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    const-string v1, "NOTIFY_FAILED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    const-string v1, "REQUEST_URL"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;->REQUEST_URL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    .line 18
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;->REQUEST_DOWNLOAD:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;->REQUEST_CANCEL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;->NOTIFY_CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;->NOTIFY_DOWNLOADDONE:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;->CLEAR_RETRYCOUNT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;->INCREASE_RETRYCOUNT:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;->START_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;->STOP_TIMER:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;->REQUEST_URL:Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/installer/downloadingmgr/IDownloadingState$Action;

    return-object v0
.end method
