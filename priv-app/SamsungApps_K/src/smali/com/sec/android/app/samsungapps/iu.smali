.class final Lcom/sec/android/app/samsungapps/iu;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;


# instance fields
.field a:I

.field b:Ljava/lang/String;

.field final synthetic c:Lcom/sec/android/app/samsungapps/widget/AdPreferenceArrayAdapter;

.field final synthetic d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;Lcom/sec/android/app/samsungapps/widget/AdPreferenceArrayAdapter;)V
    .locals 1

    .prologue
    .line 1267
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/iu;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    iput-object p2, p0, Lcom/sec/android/app/samsungapps/iu;->c:Lcom/sec/android/app/samsungapps/widget/AdPreferenceArrayAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1268
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/iu;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->h(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/iu;->a:I

    .line 1269
    iget v0, p0, Lcom/sec/android/app/samsungapps/iu;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/iu;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final onClick(Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;I)V
    .locals 7

    .prologue
    const-wide/16 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1274
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 1275
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/iu;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/Context;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1384
    :cond_0
    :goto_0
    return-void

    .line 1278
    :cond_1
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/iu;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;-><init>(Landroid/content/Context;)V

    .line 1280
    iget v2, p0, Lcom/sec/android/app/samsungapps/iu;->a:I

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 1282
    :pswitch_0
    if-nez p2, :cond_2

    .line 1287
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/iu;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->H:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v2

    invoke-interface {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeADChecked(Z)V

    .line 1288
    const-string v2, "ad_preference_setting"

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setSharedConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 1289
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/iu;->c:Lcom/sec/android/app/samsungapps/widget/AdPreferenceArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/AdPreferenceArrayAdapter;->notifyDataSetChanged()V

    .line 1290
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 1291
    new-instance v1, Lcom/sec/android/app/samsungapps/iv;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/iv;-><init>(Lcom/sec/android/app/samsungapps/iu;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;)V

    invoke-virtual {v0, v1, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 1299
    :cond_2
    if-ne p2, v3, :cond_0

    .line 1304
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/iu;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->H:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v2

    invoke-interface {v2, v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeADChecked(Z)V

    .line 1305
    const-string v2, "ad_preference_setting"

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setSharedConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 1306
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/iu;->c:Lcom/sec/android/app/samsungapps/widget/AdPreferenceArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/AdPreferenceArrayAdapter;->notifyDataSetChanged()V

    .line 1307
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 1308
    new-instance v1, Lcom/sec/android/app/samsungapps/iw;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/iw;-><init>(Lcom/sec/android/app/samsungapps/iu;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;)V

    invoke-virtual {v0, v1, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 1318
    :pswitch_1
    if-nez p2, :cond_3

    .line 1320
    const-string v2, "ad_preference_setting"

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setSharedConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 1321
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/iu;->c:Lcom/sec/android/app/samsungapps/widget/AdPreferenceArrayAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/widget/AdPreferenceArrayAdapter;->notifyDataSetChanged()V

    .line 1326
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/iu;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->H:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v2

    invoke-interface {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeADChecked(Z)V

    .line 1328
    new-instance v2, Lcom/sec/android/app/samsungapps/AdNotificationDialogBuilder;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/iu;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/android/app/samsungapps/AdNotificationDialogBuilder;-><init>(Landroid/content/Context;)V

    .line 1329
    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/AdNotificationDialogBuilder;->create()Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    move-result-object v2

    .line 1330
    invoke-virtual {v2, v4}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setCanceledOnTouchOutside(Z)V

    .line 1331
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/iu;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0802ef

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/samsungapps/ix;

    invoke-direct {v4, p0, v1, v0}, Lcom/sec/android/app/samsungapps/ix;-><init>(Lcom/sec/android/app/samsungapps/iu;Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 1339
    new-instance v0, Lcom/sec/android/app/samsungapps/iy;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/samsungapps/iy;-><init>(Lcom/sec/android/app/samsungapps/iu;Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;)V

    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setBackKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 1361
    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->show()V

    goto/16 :goto_0

    .line 1365
    :cond_3
    if-ne p2, v3, :cond_0

    .line 1370
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/iu;->d:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->H:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v2

    invoke-interface {v2, v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeADChecked(Z)V

    .line 1371
    const-string v2, "ad_preference_setting"

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setSharedConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 1372
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/iu;->c:Lcom/sec/android/app/samsungapps/widget/AdPreferenceArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/AdPreferenceArrayAdapter;->notifyDataSetChanged()V

    .line 1373
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 1374
    new-instance v1, Lcom/sec/android/app/samsungapps/ja;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/ja;-><init>(Lcom/sec/android/app/samsungapps/iu;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;)V

    invoke-virtual {v0, v1, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 1280
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
