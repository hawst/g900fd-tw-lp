.class public Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;
.super Lcom/sec/android/app/samsungapps/widget/CommonWidget;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPriceChangeListener;


# instance fields
.field a:Landroid/widget/CheckBox;

.field b:Landroid/widget/TextView;

.field d:Landroid/widget/TextView;

.field e:Z

.field private f:Lcom/sec/android/app/samsungapps/view/ResizableTextButton;

.field private g:Lcom/sec/android/app/samsungapps/view/ResizableTextButton;

.field private h:I

.field private i:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;

.field private j:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget$OnPurchaseCancelListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;)V

    .line 28
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->h:I

    .line 128
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->e:Z

    .line 33
    const v0, 0x7f0400b0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->initView(Landroid/content/Context;I)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->h:I

    .line 128
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->e:Z

    .line 38
    const v0, 0x7f0400b0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->initView(Landroid/content/Context;I)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->h:I

    .line 128
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->e:Z

    .line 43
    const v0, 0x7f0400b0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->initView(Landroid/content/Context;I)V

    .line 44
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;->isKorea()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;->isPaymentAbleCondition()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->e:Z

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->f:Lcom/sec/android/app/samsungapps/view/ResizableTextButton;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->setEnabled(Z)V

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->f:Lcom/sec/android/app/samsungapps/view/ResizableTextButton;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->setFocusable(Z)V

    .line 149
    :goto_0
    return-void

    .line 137
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->f:Lcom/sec/android/app/samsungapps/view/ResizableTextButton;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->setEnabled(Z)V

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->f:Lcom/sec/android/app/samsungapps/view/ResizableTextButton;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->setFocusable(Z)V

    goto :goto_0

    .line 141
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;->isPaymentAbleCondition()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->f:Lcom/sec/android/app/samsungapps/view/ResizableTextButton;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->setEnabled(Z)V

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->f:Lcom/sec/android/app/samsungapps/view/ResizableTextButton;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->setFocusable(Z)V

    goto :goto_0

    .line 145
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->f:Lcom/sec/android/app/samsungapps/view/ResizableTextButton;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->setEnabled(Z)V

    .line 146
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->f:Lcom/sec/android/app/samsungapps/view/ResizableTextButton;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->setFocusable(Z)V

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const v5, 0x7f0c0144

    const v4, 0x7f0c0140

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 183
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getAlertDialogButtonOrder(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 185
    invoke-virtual {p0, v4}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 186
    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 193
    :goto_0
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getPositiveButtonID(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->f:Lcom/sec/android/app/samsungapps/view/ResizableTextButton;

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->f:Lcom/sec/android/app/samsungapps/view/ResizableTextButton;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->setVisibility(I)V

    .line 195
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->f:Lcom/sec/android/app/samsungapps/view/ResizableTextButton;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->setText(Ljava/lang/CharSequence;)V

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->f:Lcom/sec/android/app/samsungapps/view/ResizableTextButton;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->onPositive()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 199
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/uiutil/ButtonUtil;->getNegativeButtonID(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->g:Lcom/sec/android/app/samsungapps/view/ResizableTextButton;

    .line 200
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->g:Lcom/sec/android/app/samsungapps/view/ResizableTextButton;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->setVisibility(I)V

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->g:Lcom/sec/android/app/samsungapps/view/ResizableTextButton;

    invoke-virtual {v0, p3}, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->setText(Ljava/lang/CharSequence;)V

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->g:Lcom/sec/android/app/samsungapps/view/ResizableTextButton;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->onNegative()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 203
    return-void

    .line 190
    :cond_0
    invoke-virtual {p0, v5}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 191
    invoke-virtual {p0, v4}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->a()V

    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;)Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 153
    const v0, 0x7f0c0274

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->b:Landroid/widget/TextView;

    .line 154
    const v0, 0x7f0c0273

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->d:Landroid/widget/TextView;

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;->isDiscounted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->b:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->b:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "(-"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;->getDiscountPrice()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;->getNormalPrice()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    :goto_0
    return-void

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->b:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;->getNormalPrice()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public loadWidget()V
    .locals 0

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->updateWidget()V

    .line 49
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 254
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->refreshWidget()V

    .line 255
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->configurationListener:Lcom/sec/android/app/samsungapps/widget/CommonWidget$ConfChangedCompleteListener;

    if-eqz v0, :cond_0

    .line 256
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->configurationListener:Lcom/sec/android/app/samsungapps/widget/CommonWidget$ConfChangedCompleteListener;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget$ConfChangedCompleteListener;->configurationChangedComplete()V

    .line 258
    :cond_0
    return-void
.end method

.method public onNegative()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 235
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/purchase/ac;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/ac;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;)V

    return-object v0
.end method

.method public onPositive()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 206
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/purchase/ab;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/ab;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;)V

    return-object v0
.end method

.method public onPriceChanged()V
    .locals 0

    .prologue
    .line 263
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->b()V

    .line 264
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->a()V

    .line 265
    return-void
.end method

.method public refresh()V
    .locals 0

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->updateWidget()V

    .line 57
    return-void
.end method

.method public refreshWidget()V
    .locals 0

    .prologue
    .line 174
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->updateWidget()V

    .line 175
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    .line 226
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->release()V

    .line 228
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;

    if-eqz v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;->release()V

    .line 230
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;

    .line 232
    :cond_0
    return-void
.end method

.method public setInterface(Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;

    .line 53
    return-void
.end method

.method public setOnPurchaseCancelListener(Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget$OnPurchaseCancelListener;)V
    .locals 0

    .prologue
    .line 246
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->j:Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget$OnPurchaseCancelListener;

    .line 247
    return-void
.end method

.method public setType(I)V
    .locals 0

    .prologue
    .line 169
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->h:I

    .line 170
    return-void
.end method

.method public setWidgetData(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 179
    return-void
.end method

.method public updateWidget()V
    .locals 8

    .prologue
    const v3, 0x7f0801c4

    const v7, 0x7f08023d

    const/16 v6, 0x8

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;

    if-nez v0, :cond_1

    .line 63
    invoke-virtual {p0, v6}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->setVisibility(I)V

    .line 126
    :cond_0
    :goto_0
    return-void

    .line 66
    :cond_1
    const v0, 0x7f0c0244

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->a:Landroid/widget/CheckBox;

    .line 67
    const v0, 0x7f0c009e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 69
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->b()V

    .line 71
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;

    if-eqz v1, :cond_6

    .line 73
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;->isFreeProduct()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 75
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->mContext:Landroid/content/Context;

    const v3, 0x7f080348

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    :cond_2
    :goto_1
    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->h:I

    if-eq v1, v5, :cond_7

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->i:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;->isKorea()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 91
    new-instance v1, Lcom/sec/android/app/samsungapps/widget/purchase/z;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/z;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 104
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->a:Landroid/widget/CheckBox;

    new-instance v2, Lcom/sec/android/app/samsungapps/widget/purchase/aa;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/widget/purchase/aa;-><init>(Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 118
    :cond_3
    :goto_2
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->f:Lcom/sec/android/app/samsungapps/view/ResizableTextButton;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->setEnabled(Z)V

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->f:Lcom/sec/android/app/samsungapps/view/ResizableTextButton;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/samsungapps/view/ResizableTextButton;->setFocusable(Z)V

    .line 123
    :cond_4
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->h:I

    if-eq v0, v5, :cond_0

    .line 124
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->a()V

    goto :goto_0

    .line 79
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->USA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->checkCountry(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 81
    const v1, 0x7f0c0275

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 87
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 112
    :cond_7
    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 113
    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->h:I

    if-ne v1, v5, :cond_3

    .line 114
    const v1, 0x7f0c0271

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/purchase/PurchaseButtonPopupWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method
