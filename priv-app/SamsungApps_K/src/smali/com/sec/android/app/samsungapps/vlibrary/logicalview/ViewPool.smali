.class public Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ViewPool;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static instance:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ViewPool;


# instance fields
.field mViewArray:Ljava/util/ArrayList;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ViewPool;->mViewArray:Ljava/util/ArrayList;

    .line 23
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ViewPool;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ViewPool;->instance:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ViewPool;

    if-nez v0, :cond_0

    .line 10
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ViewPool;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ViewPool;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ViewPool;->instance:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ViewPool;

    .line 12
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ViewPool;->instance:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ViewPool;

    return-object v0
.end method

.method public static isExistViewPoolInstance()Z
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ViewPool;->instance:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ViewPool;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addView(Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView;)V
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ViewPool;->mViewArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 30
    return-void
.end method

.method public findView(JLjava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView;
    .locals 4

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ViewPool;->mViewArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView;

    .line 35
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView;->getType()J

    move-result-wide v2

    cmp-long v2, p1, v2

    if-nez v2, :cond_0

    .line 37
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView;->getID()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 39
    if-nez p3, :cond_0

    .line 52
    :goto_0
    return-object v0

    .line 44
    :cond_1
    if-eqz p3, :cond_0

    .line 45
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView;->getID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 52
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeView(Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView;)V
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ViewPool;->mViewArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 57
    return-void
.end method
