.class final Lcom/sec/android/app/samsungapps/commands/t;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;)V
    .locals 0

    .prologue
    .line 323
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/commands/t;->a:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 327
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0c02e4

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/t;->a:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->b(Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 328
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/t;->a:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->a(Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;Z)Z

    .line 329
    new-instance v0, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    sget-object v1, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/commands/t;->a:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->a(Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f080351

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/commands/t;->a:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->a(Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;)Landroid/content/Context;

    move-result-object v3

    const v4, 0xff47

    invoke-static {v3, v4}, Lcom/sec/android/app/samsungapps/NotiDialog;->getMessage(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/commands/t;->a:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->a(Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0802ef

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/commands/u;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/commands/u;-><init>(Lcom/sec/android/app/samsungapps/commands/t;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setPositiveButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 344
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/commands/t;->a:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->a(Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f08023d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/commands/v;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/commands/v;-><init>(Lcom/sec/android/app/samsungapps/commands/t;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setNegativeButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 354
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    .line 361
    :cond_0
    :goto_0
    return-void

    .line 356
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/t;->a:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->a:Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    if-eqz v0, :cond_0

    .line 357
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/t;->a:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;->a:Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->getDialog()Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->dismiss()V

    goto :goto_0
.end method
