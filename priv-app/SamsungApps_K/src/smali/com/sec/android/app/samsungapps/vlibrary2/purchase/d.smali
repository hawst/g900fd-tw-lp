.class final Lcom/sec/android/app/samsungapps/vlibrary2/purchase/d;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentMethodCheckInfo;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/d;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final isAlipayPurchaseSupported()Z
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/d;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isAlipayPurchaseSupported()Z

    move-result v0

    return v0
.end method

.method public final isCreditCardPurchaseAvailable()Z
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/d;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isCreditCardPurchaseAvailable()Z

    move-result v0

    return v0
.end method

.method public final isCyberCashSupported()Z
    .locals 1

    .prologue
    .line 143
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/d;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isCyberCashSupported()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 147
    :goto_0
    return v0

    .line 144
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 147
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isDirectBillingPurchaseAvailable()Z
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/d;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isDirectBillingPurchaseAvailable()Z

    move-result v0

    return v0
.end method

.method public final isPPCSupported()Z
    .locals 1

    .prologue
    .line 154
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/d;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isPPCSupported()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 158
    :goto_0
    return v0

    .line 155
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 158
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isPSMSPurchaseAvailable()Z
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/d;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    iget-boolean v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->pSmsPurchaseYN:Z

    return v0
.end method

.method public final isPhoneBillPurchaseAvailable()Z
    .locals 1

    .prologue
    .line 114
    const/4 v0, 0x1

    return v0
.end method

.method public final isTestPSMSPurchaseSupported()Z
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/d;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    iget-boolean v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->testPSMSPurchaseYn:Z

    return v0
.end method
