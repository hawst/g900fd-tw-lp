.class public Lcom/sec/android/app/samsungapps/vlibrary2/initialize/InitializeCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _IInitializeCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/InitializeCommand$IInitializeCommandData;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/InitializeCommand$IInitializeCommandData;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/InitializeCommand;->_IInitializeCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/InitializeCommand$IInitializeCommandData;

    .line 15
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/InitializeCommand;)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/InitializeCommand;->stepNextCountrySearch()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/InitializeCommand;Z)V
    .locals 0

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/InitializeCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/InitializeCommand;Z)V
    .locals 0

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/InitializeCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/InitializeCommand;Z)V
    .locals 0

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/InitializeCommand;->onFinalResult(Z)V

    return-void
.end method

.method private countrySearch()V
    .locals 3

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/InitializeCommand;->_IInitializeCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/InitializeCommand$IInitializeCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/InitializeCommand$IInitializeCommandData;->countrySearch()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/InitializeCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/c;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/InitializeCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 40
    return-void
.end method

.method private stepNextCountrySearch()V
    .locals 3

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/InitializeCommand;->_IInitializeCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/InitializeCommand$IInitializeCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/InitializeCommand$IInitializeCommandData;->validateDisclaimer()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/InitializeCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/d;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/InitializeCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 58
    return-void
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/InitializeCommand;->countrySearch()V

    .line 20
    return-void
.end method
