.class public Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskCQStorePassword;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _IAskCQStorePasswordData:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskCQStorePassword$IAskCQStorePasswordData;

.field private _ResultMap:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;

.field private loadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskCQStorePassword$IAskCQStorePasswordData;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 19
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskCQStorePassword;->_ResultMap:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;

    .line 24
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskCQStorePassword;->_IAskCQStorePasswordData:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskCQStorePassword$IAskCQStorePasswordData;

    .line 25
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskCQStorePassword;)Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskCQStorePassword;->loadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskCQStorePassword;)Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskCQStorePassword;->_ResultMap:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskCQStorePassword;Z)V
    .locals 0

    .prologue
    .line 17
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskCQStorePassword;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskCQStorePassword;Z)V
    .locals 0

    .prologue
    .line 17
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskCQStorePassword;->onFinalResult(Z)V

    return-void
.end method

.method private invokeUI()V
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskCQStorePassword;->_IAskCQStorePasswordData:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskCQStorePassword$IAskCQStorePasswordData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskCQStorePassword$IAskCQStorePasswordData;->getCQPasswordViewInvoker()Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskCQStorePassword;->_Context:Landroid/content/Context;

    invoke-interface {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;->invoke(Landroid/content/Context;Ljava/lang/Object;)V

    .line 60
    return-void
.end method


# virtual methods
.method public checkQAPassword(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskCQStorePassword;->_IAskCQStorePasswordData:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskCQStorePassword$IAskCQStorePasswordData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskCQStorePassword$IAskCQStorePasswordData;->createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskCQStorePassword;->loadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    .line 35
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskCQStorePassword;->loadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->startLoading()V

    .line 36
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskCQStorePassword;->_ResultMap:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/a;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskCQStorePassword;)V

    invoke-virtual {v0, p1, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->verificationAuthority(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 54
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 55
    return-void
.end method

.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskCQStorePassword;->invokeUI()V

    .line 30
    return-void
.end method
