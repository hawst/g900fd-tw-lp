.class final Lcom/sec/android/app/samsungapps/lu;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/updatechecksvc/UpdateCheckSVCManager$IUpdateCheckSVCManagerObserver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/lt;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/lt;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/lu;->a:Lcom/sec/android/app/samsungapps/lt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onUpdateCheckFailed(I)V
    .locals 3

    .prologue
    .line 100
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/lu;->a:Lcom/sec/android/app/samsungapps/lt;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/lt;->a:Lcom/sec/android/app/samsungapps/updateCheck/aidl/IUpdateCheckResult;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/updateCheck/aidl/IUpdateCheckResult;->onUpdateCheckFailed(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 104
    :goto_0
    return-void

    .line 101
    :catch_0
    move-exception v0

    .line 102
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UpdateCheckSVC::RemoteException::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onUpdateCheckSuccess(I)V
    .locals 3

    .prologue
    .line 109
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/lu;->a:Lcom/sec/android/app/samsungapps/lt;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/lt;->a:Lcom/sec/android/app/samsungapps/updateCheck/aidl/IUpdateCheckResult;

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/updateCheck/aidl/IUpdateCheckResult;->onUpdateCheckSuccess(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 113
    :goto_0
    return-void

    .line 110
    :catch_0
    move-exception v0

    .line 111
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UpdateCheckSVC::RemoteException::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method
