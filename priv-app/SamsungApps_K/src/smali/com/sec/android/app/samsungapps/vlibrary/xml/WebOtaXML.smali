.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/WebOtaXML;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
.source "ProGuard"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V
    .locals 0

    .prologue
    .line 9
    invoke-direct/range {p0 .. p6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 11
    return-void
.end method

.method public static deregister(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/WebOtaXML;
    .locals 7

    .prologue
    .line 27
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/WebOtaXML;

    const-string v2, "deregisterWebOTAService"

    const-string v3, "6011"

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v1, p0

    move v4, p2

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/WebOtaXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 28
    const-string v1, "regId"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/WebOtaXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    return-object v0
.end method

.method public static register(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/sec/android/app/samsungapps/vlibrary/xml/WebOtaXML;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 15
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/WebOtaXML;

    const-string v2, "registerWebOTAService"

    const-string v3, "6010"

    const/4 v5, 0x0

    move-object v1, p0

    move v4, p6

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/WebOtaXML;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 16
    const-string v1, "regId"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/WebOtaXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    const-string v1, "deviceOSType"

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/WebOtaXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    const-string v1, "deviceOSVersion"

    invoke-virtual {v0, v1, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/WebOtaXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    const-string v1, "imei"

    invoke-virtual {v0, v1, p4, v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/WebOtaXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 20
    const-string v1, "emailid"

    invoke-virtual {v0, v1, p5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/WebOtaXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    return-object v0
.end method
