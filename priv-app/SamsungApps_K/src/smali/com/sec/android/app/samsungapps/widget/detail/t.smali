.class final Lcom/sec/android/app/samsungapps/widget/detail/t;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/text/InputFilter;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)V
    .locals 1

    .prologue
    .line 286
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/t;->b:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    const/16 v0, 0x8c

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/t;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x0

    .line 292
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/t;->b:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->b(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 338
    :goto_0
    return-object v0

    .line 297
    :cond_0
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/t;->a:I

    invoke-interface {p4}, Landroid/text/Spanned;->length()I

    move-result v2

    sub-int v3, p6, p5

    sub-int/2addr v2, v3

    sub-int v2, v0, v2

    .line 299
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/t;->b:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->b(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/content/Context;

    move-result-object v0

    const-string v3, "layout_inflater"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 300
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/detail/t;->b:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->b(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080202

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    iget v6, p0, Lcom/sec/android/app/samsungapps/widget/detail/t;->a:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 302
    const v4, 0x7f040024

    invoke-virtual {v0, v4, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 303
    const v0, 0x7f0c0099

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 306
    if-nez p2, :cond_1

    if-nez p3, :cond_1

    move-object v0, v1

    .line 307
    goto :goto_0

    .line 310
    :cond_1
    if-gtz v2, :cond_3

    .line 312
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/t;->b:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->m(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/widget/Toast;

    move-result-object v1

    if-nez v1, :cond_2

    .line 313
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/t;->b:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    new-instance v2, Landroid/widget/Toast;

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/widget/detail/t;->b:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v5}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->b(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/content/Context;

    move-result-object v5

    invoke-direct {v2, v5}, Landroid/widget/Toast;-><init>(Landroid/content/Context;)V

    invoke-static {v1, v2}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->a(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 314
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 315
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/t;->b:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->m(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    .line 316
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/t;->b:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->m(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/Toast;->setDuration(I)V

    .line 320
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/t;->b:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->m(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 321
    const-string v0, ""

    goto/16 :goto_0

    .line 318
    :cond_2
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 323
    :cond_3
    sub-int v5, p3, p2

    if-lt v2, v5, :cond_4

    move-object v0, v1

    .line 325
    goto/16 :goto_0

    .line 329
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/t;->b:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->m(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/widget/Toast;

    move-result-object v1

    if-nez v1, :cond_5

    .line 330
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/t;->b:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    new-instance v5, Landroid/widget/Toast;

    iget-object v6, p0, Lcom/sec/android/app/samsungapps/widget/detail/t;->b:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v6}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->b(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/widget/Toast;-><init>(Landroid/content/Context;)V

    invoke-static {v1, v5}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->a(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 331
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 332
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/t;->b:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->m(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    .line 333
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/t;->b:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->m(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/Toast;->setDuration(I)V

    .line 337
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/t;->b:Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->m(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 338
    add-int v0, p2, v2

    invoke-interface {p1, p2, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    goto/16 :goto_0

    .line 335
    :cond_5
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method
