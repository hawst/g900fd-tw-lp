.class public final enum Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum CustomerSupport:Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;

.field public static final enum Information:Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;

.field public static final enum Notices:Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 6
    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;

    const-string v1, "Notices"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;->Notices:Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;

    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;

    const-string v1, "CustomerSupport"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;->CustomerSupport:Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;

    new-instance v0, Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;

    const-string v1, "Information"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;->Information:Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;

    .line 4
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;->Notices:Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;->CustomerSupport:Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;->Information:Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;->a:[Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;
    .locals 1

    .prologue
    .line 4
    const-class v0, Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;
    .locals 1

    .prologue
    .line 4
    sget-object v0, Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;->a:[Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/uieventmanager/SupportTabEvent$SupportTabEventType;

    return-object v0
.end method
