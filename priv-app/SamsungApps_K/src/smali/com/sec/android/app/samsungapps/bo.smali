.class final Lcom/sec/android/app/samsungapps/bo;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;

.field final synthetic b:Lcom/sec/android/app/samsungapps/ContentDetailActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;)V
    .locals 0

    .prologue
    .line 819
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/bo;->b:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    iput-object p2, p0, Lcom/sec/android/app/samsungapps/bo;->a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCommandResult(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 822
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/bo;->b:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 839
    :cond_0
    :goto_0
    return-void

    .line 825
    :cond_1
    if-eqz p1, :cond_2

    .line 826
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/bo;->b:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/bo;->b:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->f(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;->getCommentList()Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->b(Lcom/sec/android/app/samsungapps/ContentDetailActivity;Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    .line 827
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Review Expert review is successful.    mExpertCommentList size is ="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/bo;->b:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->g(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    .line 829
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/bo;->a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/bo;->b:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->g(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->setWidgetData(ILjava/lang/Object;)V

    .line 831
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/bo;->a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->loadWidget(I)V

    .line 836
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/bo;->b:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a(Lcom/sec/android/app/samsungapps/ContentDetailActivity;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 837
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/bo;->a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->onWidgetSetViewState()V

    goto :goto_0

    .line 833
    :cond_2
    const-string v0, "Expert review is failed"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    .line 834
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/bo;->a:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->setWidgetState(I)V

    goto :goto_1
.end method
