.class public abstract Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;
.super Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;


# instance fields
.field protected mHasSearchIcon:Z

.field protected mMainView:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mMainView:Landroid/widget/LinearLayout;

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mHasSearchIcon:Z

    return-void
.end method

.method private a(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;ZLcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;[II)V
    .locals 3

    .prologue
    .line 275
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    if-eqz v0, :cond_2

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->removeAll()Ljava/lang/Boolean;

    .line 279
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->setTitle(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;)Ljava/lang/Boolean;

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1, p4}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->setIconVisible(Ljava/lang/Boolean;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;)Ljava/lang/Boolean;

    .line 285
    if-eqz p5, :cond_1

    .line 287
    const/4 v0, 0x0

    :goto_0
    array-length v1, p5

    if-ge v0, v1, :cond_1

    .line 289
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    aget v2, p5, v0

    invoke-virtual {v1, v2, p0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->addAction(ILcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;)Ljava/lang/Boolean;

    .line 290
    aget v1, p5, v0

    const v2, 0xa0005

    if-ne v1, v2, :cond_0

    .line 292
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mHasSearchIcon:Z

    .line 287
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 296
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0, p6}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->changeActionBarBG(I)V

    .line 297
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->showActionBar()Ljava/lang/Boolean;

    .line 299
    :cond_2
    return-void
.end method


# virtual methods
.method protected addAction(ILcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;)V
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->addAction(ILcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;)Ljava/lang/Boolean;

    .line 312
    :cond_0
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 322
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x52

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->isLongPress()Z

    .line 327
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public enableActionItem(IZ)V
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    if-eqz v0, :cond_0

    .line 339
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->enableActionItem(IZ)V

    .line 341
    :cond_0
    return-void
.end method

.method protected getActionBarVisibility()I
    .locals 2

    .prologue
    .line 151
    const/4 v0, 0x4

    .line 153
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    if-eqz v1, :cond_0

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->getVisibility()I

    move-result v0

    .line 161
    :cond_0
    return v0
.end method

.method public handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 442
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->UpdateAppEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    if-ne v0, v1, :cond_0

    .line 444
    sget-object v0, Lcom/sec/android/app/samsungapps/e;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getUpdateAppEvent()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/UpdateAppEvent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/UpdateAppEvent;->getUpdateAppEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/UpdateAppEvent$UpdateAppEventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/UpdateAppEvent$UpdateAppEventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 452
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->ExitSamsungApps:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    if-ne v0, v1, :cond_1

    .line 454
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getExtraData()Ljava/lang/Object;

    move-result-object v0

    .line 455
    instance-of v0, v0, Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 456
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getExtraData()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 457
    if-lez v0, :cond_1

    .line 458
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->getTaskId()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 459
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->finish()V

    .line 468
    :cond_1
    :goto_1
    return v2

    .line 447
    :pswitch_0
    invoke-static {p0, v2}, Lcom/sec/android/app/samsungapps/BadgeNotification;->setBadgeNotification(Landroid/content/Context;I)Z

    goto :goto_0

    .line 463
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->finish()V

    goto :goto_1

    .line 444
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected hideActionBar()V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->hideActionBar()V

    .line 143
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 33
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 34
    const v0, 0x7f040005

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->setContentView(I)V

    .line 36
    const v0, 0x7f0c003a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mMainView:Landroid/widget/LinearLayout;

    .line 37
    const v0, 0x7f0c0039

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    .line 40
    :try_start_0
    new-instance v0, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;-><init>(Landroid/app/Activity;)V

    .line 41
    new-instance v1, Lcom/sec/android/app/samsungapps/d;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/d;-><init>(Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->setStateChangeListener(Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;)Z
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 68
    :goto_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->addSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V

    .line 69
    return-void

    .line 61
    :catch_0
    move-exception v0

    .line 62
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ActionBarPreferenceActivity::Exception"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NoSuchMethodError;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 87
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->removeSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mMainView:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mMainView:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 94
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->onDestroy()V

    .line 95
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 351
    const/16 v0, 0x52

    if-ne p1, v0, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 355
    invoke-virtual {p2}, Landroid/view/KeyEvent;->startTracking()V

    .line 357
    const/4 v0, 0x1

    .line 360
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyLongPress(ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/16 v2, 0x52

    const/4 v0, 0x1

    .line 373
    if-ne p1, v2, :cond_2

    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mHasSearchIcon:Z

    if-eqz v1, :cond_2

    .line 375
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->getLocalClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    .line 376
    const-string v2, "SearchResultActivity"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 378
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->onBackPressed()V

    .line 394
    :cond_0
    :goto_0
    return v0

    .line 382
    :cond_1
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->createSearchResultActivityIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 383
    const/high16 v2, 0x4000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 384
    invoke-static {p0, v1, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->commonStartActivity(Landroid/app/Activity;Landroid/content/Intent;Z)V

    goto :goto_0

    .line 389
    :cond_2
    if-eq p1, v2, :cond_0

    .line 394
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->onKeyLongPress(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 401
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isTracking()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 403
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 430
    :goto_0
    return v0

    .line 407
    :cond_1
    const/16 v1, 0x54

    if-ne p1, v1, :cond_3

    .line 409
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isUncStore()Z

    move-result v1

    if-ne v1, v0, :cond_2

    .line 411
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    .line 414
    :cond_2
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/CommonActivity;->createSearchResultActivityIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 415
    const/high16 v2, 0x4000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 416
    invoke-static {p0, v1, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->commonStartActivity(Landroid/app/Activity;Landroid/content/Intent;Z)V

    goto :goto_0

    .line 420
    :cond_3
    const/16 v0, 0x52

    if-ne p1, v0, :cond_4

    .line 422
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    if-eqz v0, :cond_4

    .line 425
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    const v1, 0xa0008

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->doNonHardKeyOptionMenu(I)V

    .line 430
    :cond_4
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .prologue
    .line 73
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/CommonPreferenceActivity;->onWindowFocusChanged(Z)V

    .line 75
    if-eqz p1, :cond_0

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->onWindowStatusChangedListener()V

    .line 80
    :cond_0
    return-void
.end method

.method protected removeMainView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mMainView:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mMainView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 203
    :cond_0
    return-void
.end method

.method public setActionBarConfiguration(ILcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;ZLcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;[II)V
    .locals 7

    .prologue
    .line 262
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v0, p0

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->a(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;ZLcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;[II)V

    .line 263
    return-void
.end method

.method protected setActionBarConfiguration(ILcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V
    .locals 7

    .prologue
    .line 249
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v0, p0

    move-object v2, p2

    move v3, p3

    move-object v4, p0

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->a(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;ZLcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;[II)V

    .line 250
    return-void
.end method

.method protected setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;ZI)V
    .locals 7

    .prologue
    .line 225
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p0

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->a(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;ZLcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;[II)V

    .line 226
    return-void
.end method

.method protected setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;ZLcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;I)V
    .locals 7

    .prologue
    .line 214
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p0

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->a(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;ZLcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;[II)V

    .line 215
    return-void
.end method

.method protected setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V
    .locals 7

    .prologue
    .line 237
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p0

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->a(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;ZLcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;[II)V

    .line 238
    return-void
.end method

.method protected setMainView(I)V
    .locals 3

    .prologue
    .line 170
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mMainView:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    if-lez p1, :cond_0

    .line 172
    const-string v0, "layout_inflater"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 173
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mMainView:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 174
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mMainView:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 176
    :cond_0
    return-void
.end method

.method protected setMainView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mMainView:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mMainView:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mMainView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 190
    :cond_0
    return-void
.end method

.method protected setNagativeText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->setNegativeText(Ljava/lang/String;)Z

    .line 121
    :cond_0
    return-void
.end method

.method protected setPositiveText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->setPositiveText(Ljava/lang/String;)Z

    .line 108
    :cond_0
    return-void
.end method

.method protected showActionBar()V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ActionBarPreferenceActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->showActionBar()Ljava/lang/Boolean;

    .line 132
    :cond_0
    return-void
.end method
