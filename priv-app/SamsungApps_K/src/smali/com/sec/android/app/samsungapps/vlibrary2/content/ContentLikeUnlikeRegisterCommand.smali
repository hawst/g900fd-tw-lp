.class public Lcom/sec/android/app/samsungapps/vlibrary2/content/ContentLikeUnlikeRegisterCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field mContentLikeUnlike:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentLikeUnlike;

.field mIContentLikeUnLikeData:Lcom/sec/android/app/samsungapps/vlibrary2/content/ContentLikeUnlikeRegisterCommand$IContentLikeUnlikeRegisterData;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/content/ContentLikeUnlikeRegisterCommand$IContentLikeUnlikeRegisterData;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 18
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentLikeUnlike;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentLikeUnlike;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ContentLikeUnlikeRegisterCommand;->mContentLikeUnlike:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentLikeUnlike;

    .line 22
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ContentLikeUnlikeRegisterCommand;->mIContentLikeUnLikeData:Lcom/sec/android/app/samsungapps/vlibrary2/content/ContentLikeUnlikeRegisterCommand$IContentLikeUnlikeRegisterData;

    .line 23
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/content/ContentLikeUnlikeRegisterCommand;Z)V
    .locals 0

    .prologue
    .line 15
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/ContentLikeUnlikeRegisterCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/content/ContentLikeUnlikeRegisterCommand;Z)V
    .locals 0

    .prologue
    .line 15
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/ContentLikeUnlikeRegisterCommand;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 5

    .prologue
    .line 28
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ContentLikeUnlikeRegisterCommand;->mIContentLikeUnLikeData:Lcom/sec/android/app/samsungapps/vlibrary2/content/ContentLikeUnlikeRegisterCommand$IContentLikeUnlikeRegisterData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/ContentLikeUnlikeRegisterCommand$IContentLikeUnlikeRegisterData;->getProductID()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ContentLikeUnlikeRegisterCommand;->mIContentLikeUnLikeData:Lcom/sec/android/app/samsungapps/vlibrary2/content/ContentLikeUnlikeRegisterCommand$IContentLikeUnlikeRegisterData;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/content/ContentLikeUnlikeRegisterCommand$IContentLikeUnlikeRegisterData;->IsLikeYn()Z

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/ContentLikeUnlikeRegisterCommand;->mContentLikeUnlike:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentLikeUnlike;

    new-instance v4, Lcom/sec/android/app/samsungapps/vlibrary2/content/a;

    invoke-direct {v4, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/content/ContentLikeUnlikeRegisterCommand;)V

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->productLikeUnlikeRegister(Ljava/lang/String;ZLcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 49
    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 50
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 51
    return-void
.end method
