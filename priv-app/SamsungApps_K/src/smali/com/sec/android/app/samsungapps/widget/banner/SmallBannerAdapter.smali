.class public Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ProGuard"


# instance fields
.field private a:Ljava/util/ArrayList;

.field private b:Landroid/content/Context;

.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;I)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 28
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter;->a:Ljava/util/ArrayList;

    .line 29
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter;->b:Landroid/content/Context;

    .line 30
    iput p4, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter;->c:I

    .line 31
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    .line 35
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SmallBannerWidget :: getCount() "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->d(Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter;->getItem(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 46
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;

    .line 62
    if-nez p2, :cond_3

    .line 63
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter;->b:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 64
    const v2, 0x7f0400d6

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 65
    new-instance v2, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter$ViewHolder;

    invoke-direct {v2}, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter$ViewHolder;-><init>()V

    .line 66
    const v1, 0x7f0c02e0

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    iput-object v1, v2, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter$ViewHolder;->a:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    .line 67
    iget-object v1, v2, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter$ViewHolder;->a:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter;->b:Landroid/content/Context;

    const-string v4, "isa_samsungapps_default"

    const-string v5, "drawable"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/samsungapps/uiutil/KnoxResourceManager;->findResource(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setImageResource(I)V

    .line 68
    const v1, 0x7f0c02df

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, v2, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter$ViewHolder;->b:Landroid/widget/FrameLayout;

    .line 69
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v2

    .line 74
    :goto_0
    iget-object v2, v1, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter$ViewHolder;->a:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter;->b:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getNetAPI()Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setNetAPI(Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;)V

    .line 75
    iget-object v2, v1, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter$ViewHolder;->a:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->getURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setURL(Ljava/lang/String;)V

    .line 76
    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/IBannerResult;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 78
    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter;->b:Landroid/content/Context;

    const v2, 0x7f080110

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 81
    :cond_1
    iget-object v2, v1, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter$ViewHolder;->b:Landroid/widget/FrameLayout;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter;->b:Landroid/content/Context;

    const v4, 0x7f0800d6

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/FrameLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 83
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter;->c:I

    if-lez v0, :cond_2

    .line 84
    iget-object v0, v1, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter$ViewHolder;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 85
    iget v2, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter;->c:I

    int-to-double v2, v2

    const-wide v4, 0x4000a3d70a3d70a4L    # 2.08

    div-double/2addr v2, v4

    double-to-int v2, v2

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 86
    iget v2, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter;->c:I

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 87
    iget-object v1, v1, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter$ViewHolder;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 90
    :cond_2
    return-object p2

    .line 71
    :cond_3
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter$ViewHolder;

    goto :goto_0
.end method

.method public release()V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 53
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter;->b:Landroid/content/Context;

    .line 54
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/SmallBannerAdapter;->c:I

    .line 55
    return-void
.end method
