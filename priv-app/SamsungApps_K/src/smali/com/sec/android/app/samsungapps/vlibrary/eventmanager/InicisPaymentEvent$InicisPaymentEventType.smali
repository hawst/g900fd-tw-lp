.class public final enum Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;

.field public static final enum FINALFAILED:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;

.field public static final enum FINALSUCCESS:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;

.field public static final enum INITFAILED:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;

.field public static final enum INITSUCCESS:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 6
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;

    const-string v1, "INITSUCCESS"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;->INITSUCCESS:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;

    const-string v1, "INITFAILED"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;->INITFAILED:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;

    const-string v1, "FINALSUCCESS"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;->FINALSUCCESS:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;

    const-string v1, "FINALFAILED"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;->FINALFAILED:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;

    .line 4
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;->INITSUCCESS:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;->INITFAILED:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;->FINALSUCCESS:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;->FINALFAILED:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;
    .locals 1

    .prologue
    .line 4
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;
    .locals 1

    .prologue
    .line 4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/InicisPaymentEvent$InicisPaymentEventType;

    return-object v0
.end method
