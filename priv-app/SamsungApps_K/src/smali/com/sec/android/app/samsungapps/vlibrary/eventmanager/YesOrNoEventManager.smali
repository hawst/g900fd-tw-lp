.class public Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEventManager;
.super Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/BaseEventManager;
.source "ProGuard"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/BaseEventManager;-><init>()V

    return-void
.end method


# virtual methods
.method public findEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoEventType;)Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent;
    .locals 4

    .prologue
    .line 16
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getSystemEventQueue()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventQueue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventQueue;->getList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;

    .line 18
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->YesOrNoEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    if-ne v2, v3, :cond_0

    .line 20
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getYesOrNoEvent()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent;->getYesOrNoEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoEventType;

    move-result-object v2

    if-ne v2, p1, :cond_0

    .line 22
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getYesOrNoEvent()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent;

    move-result-object v0

    .line 26
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getMyEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->YesOrNoEvent:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    return-object v0
.end method

.method public notifyTrafficChargeCanOccur(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoResult;)V
    .locals 2

    .prologue
    .line 31
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoEventType;->NotifyTrafficChargeCanBeOccur:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoEventType;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoEventType;Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEvent$YesOrNoResult;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/YesOrNoEventManager;->notifyBroadcastEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V

    .line 32
    return-void
.end method
