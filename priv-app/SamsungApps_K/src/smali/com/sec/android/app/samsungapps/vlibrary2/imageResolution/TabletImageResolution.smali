.class public Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/TabletImageResolution;
.super Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;
.source "ProGuard"


# instance fields
.field mScreenshotHeight:I

.field mScreenshotWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;-><init>(Landroid/content/Context;)V

    .line 7
    const/16 v0, 0x320

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/TabletImageResolution;->mScreenshotWidth:I

    .line 8
    const/16 v0, 0x1e0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/TabletImageResolution;->mScreenshotHeight:I

    .line 13
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/TabletImageResolution;->_Context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/TabletImageResolution;->_Context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/TabletImageResolution;->mScreenshotWidth:I

    .line 14
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/TabletImageResolution;->mScreenshotWidth:I

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/TabletImageResolution;->mScreenshotHeight:I

    .line 15
    return-void
.end method


# virtual methods
.method public getHeight(Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;)I
    .locals 2

    .prologue
    .line 33
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/b;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 39
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;->getHeight(Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;)I

    move-result v0

    :goto_0
    return v0

    .line 36
    :pswitch_0
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/TabletImageResolution;->mScreenshotHeight:I

    goto :goto_0

    .line 33
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public getWidth(Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;)I
    .locals 2

    .prologue
    .line 20
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/b;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 26
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;->getWidth(Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;)I

    move-result v0

    :goto_0
    return v0

    .line 23
    :pswitch_0
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/TabletImageResolution;->mScreenshotWidth:I

    goto :goto_0

    .line 20
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
