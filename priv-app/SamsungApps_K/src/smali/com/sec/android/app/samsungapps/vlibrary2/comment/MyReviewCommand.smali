.class public Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private mCommentError:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;

.field private mIMyReviewCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewCommandData;

.field private mIMyReviewDetailView:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewDetailView;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewCommandData;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 24
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->mCommentError:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;

    .line 28
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->mIMyReviewCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewCommandData;

    .line 29
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;Z)V
    .locals 0

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->_Context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewCommandData;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->mIMyReviewCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewCommandData;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewDetailView;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->mIMyReviewDetailView:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewDetailView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;Z)V
    .locals 0

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->mCommentError:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;Z)V
    .locals 0

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;Z)V
    .locals 0

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->onFinalResult(Z)V

    return-void
.end method

.method private checkRatingAuthority()V
    .locals 3

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->mIMyReviewDetailView:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewDetailView;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewDetailView;->onRequestRatingAuthority()V

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->mIMyReviewCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewCommandData;->checkRatingAuthorityCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/comment/m;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/m;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 70
    return-void
.end method

.method private getRegisterCommentInfo()Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML$IRegisterCommentInfo;
    .locals 1

    .prologue
    .line 103
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/n;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/n;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;)V

    return-object v0
.end method


# virtual methods
.method public addComment()V
    .locals 4

    .prologue
    .line 163
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->mIMyReviewDetailView:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewDetailView;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewDetailView;->onCommentSaving()V

    .line 164
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->getRegisterCommentInfo()Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML$IRegisterCommentInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->mCommentError:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/comment/p;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/p;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->commentRegister(Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML$IRegisterCommentInfo;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 195
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 197
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 198
    return-void
.end method

.method public getCommentText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->mIMyReviewCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewCommandData;->getOldCommentText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getModifyCommentInfo()Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML$IModifyCommentInfo;
    .locals 1

    .prologue
    .line 133
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/o;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/o;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;)V

    return-object v0
.end method

.method public getProhibitWords()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->mIMyReviewCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewCommandData;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->mIMyReviewCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewCommandData;->getProhibitWords()[Ljava/lang/String;

    move-result-object v0

    .line 92
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRatingValue()I
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->mIMyReviewCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewCommandData;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->mIMyReviewCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewCommandData;->getOldRatingValue()I

    move-result v0

    .line 83
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0xa

    goto :goto_0
.end method

.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 3

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->mIMyReviewCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewCommandData;->validateLogin()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/comment/l;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/l;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 46
    return-void
.end method

.method public invokeCompleted(Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewDetailView;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->mIMyReviewDetailView:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewDetailView;

    .line 51
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->checkRatingAuthority()V

    .line 52
    return-void
.end method

.method public isMyReviewDuplicated()Z
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->mIMyReviewCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewCommandData;->isMyReviewDuplicated()Z

    move-result v0

    return v0
.end method

.method public modifyComment()V
    .locals 4

    .prologue
    .line 202
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->mIMyReviewDetailView:Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewDetailView;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand$IMyReviewDetailView;->onCommentSaving()V

    .line 203
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->getModifyCommentInfo()Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML$IModifyCommentInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->mCommentError:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentError;

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/comment/q;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/q;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->commentModify(Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML$IModifyCommentInfo;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 218
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/MyReviewCommand;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 220
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 221
    return-void
.end method
