.class public Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;
.super Lcom/sec/android/app/samsungapps/view/MyListView;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;


# instance fields
.field a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

.field b:Lcom/sec/android/app/samsungapps/widget/interfaces/GetGiftCardCountObserver;

.field private c:Landroid/content/Context;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

.field private e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;

.field private f:Lcom/sec/android/app/samsungapps/view/n;

.field private g:Landroid/widget/ScrollView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 61
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/view/MyListView;-><init>(Landroid/content/Context;)V

    .line 38
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->c:Landroid/content/Context;

    .line 39
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    .line 40
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/GetGiftCardCountObserver;

    .line 42
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 43
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;

    .line 46
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->f:Lcom/sec/android/app/samsungapps/view/n;

    .line 48
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->g:Landroid/widget/ScrollView;

    .line 62
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->a(Landroid/content/Context;)V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 66
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/view/MyListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->c:Landroid/content/Context;

    .line 39
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    .line 40
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/GetGiftCardCountObserver;

    .line 42
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 43
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;

    .line 46
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->f:Lcom/sec/android/app/samsungapps/view/n;

    .line 48
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->g:Landroid/widget/ScrollView;

    .line 67
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->a(Landroid/content/Context;)V

    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 71
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/view/MyListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->c:Landroid/content/Context;

    .line 39
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    .line 40
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/GetGiftCardCountObserver;

    .line 42
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 43
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;

    .line 46
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->f:Lcom/sec/android/app/samsungapps/view/n;

    .line 48
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->g:Landroid/widget/ScrollView;

    .line 72
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->a(Landroid/content/Context;)V

    .line 73
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->setContext(Landroid/content/Context;)V

    .line 82
    new-instance v0, Lcom/sec/android/app/samsungapps/view/n;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/samsungapps/view/n;-><init>(Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->f:Lcom/sec/android/app/samsungapps/view/n;

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->f:Lcom/sec/android/app/samsungapps/view/n;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 84
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->addSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V

    .line 85
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;)V
    .locals 4

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->f:Lcom/sec/android/app/samsungapps/view/n;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->f:Lcom/sec/android/app/samsungapps/view/n;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/n;->clear()V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;->size()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;->get(I)Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->f:Lcom/sec/android/app/samsungapps/view/n;

    invoke-virtual {v3, v0}, Lcom/sec/android/app/samsungapps/view/n;->add(Ljava/lang/Object;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/GetGiftCardCountObserver;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/GetGiftCardCountObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/GetGiftCardCountObserver;->showGiftCardNotiArea()V

    :cond_1
    :goto_1
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->setNoDataEmptyView()V

    goto :goto_1
.end method


# virtual methods
.method public clearList()V
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->f:Lcom/sec/android/app/samsungapps/view/n;

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->f:Lcom/sec/android/app/samsungapps/view/n;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/n;->clear()V

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->f:Lcom/sec/android/app/samsungapps/view/n;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/n;->notifyDataSetChanged()V

    .line 191
    :cond_0
    return-void
.end method

.method protected empty()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 174
    .line 176
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->f:Lcom/sec/android/app/samsungapps/view/n;

    if-eqz v1, :cond_0

    .line 177
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->f:Lcom/sec/android/app/samsungapps/view/n;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/view/n;->getCount()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 180
    :cond_0
    return v0
.end method

.method public getParentScrollView()Landroid/widget/ScrollView;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->g:Landroid/widget/ScrollView;

    return-object v0
.end method

.method public handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z
    .locals 2

    .prologue
    .line 211
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v0

    .line 212
    sget-object v1, Lcom/sec/android/app/samsungapps/view/m;->a:[I

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 219
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 215
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->refresh()V

    goto :goto_0

    .line 212
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onMeasure(II)V
    .locals 2

    .prologue
    .line 382
    const v0, 0x1fffffff

    const/high16 v1, -0x80000000

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 383
    invoke-super {p0, p1, v0}, Lcom/sec/android/app/samsungapps/view/MyListView;->onMeasure(II)V

    .line 384
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 385
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->getMeasuredHeight()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 386
    return-void
.end method

.method public refresh()V
    .locals 3

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->c:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 132
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "::mContext is empty"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 165
    :goto_0
    return-void

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;

    if-eqz v0, :cond_1

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;->release()V

    .line 140
    :cond_1
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ExclusiveSelectableItemList;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ExclusiveSelectableItemList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;->clear()V

    .line 142
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectableItemList;Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;

    .line 143
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->e:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardReceiver;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftCardReceiver;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->d:Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->c:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/view/l;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/view/l;-><init>(Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 164
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->setLoadingEmptyView()V

    goto :goto_0
.end method

.method public release()V
    .locals 1

    .prologue
    .line 198
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->removeSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V

    .line 199
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/view/MyListView;->release()V

    .line 200
    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->c:Landroid/content/Context;

    .line 53
    return-void
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/widget/interfaces/GetGiftCardCountObserver;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/GetGiftCardCountObserver;

    .line 58
    return-void
.end method

.method public setParentScrollView(Landroid/widget/ScrollView;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/CouponGiftCardListView;->g:Landroid/widget/ScrollView;

    .line 89
    return-void
.end method
