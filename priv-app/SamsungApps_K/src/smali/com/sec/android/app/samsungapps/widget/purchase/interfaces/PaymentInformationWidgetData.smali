.class public Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PaymentInformationWidgetData;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/IPaymentInformationWidgetData;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PaymentInformationWidgetData;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 21
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PaymentInformationWidgetData;->a:Landroid/content/Context;

    .line 22
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PaymentInformationWidgetData;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

    .line 23
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PaymentInformationWidgetData;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 24
    return-void
.end method


# virtual methods
.method public getInformationString()Ljava/lang/String;
    .locals 9

    .prologue
    const v8, 0x7f08024b

    const v7, 0x7f080136

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 96
    const-string v0, ""

    .line 97
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PaymentInformationWidgetData;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    .line 99
    sget-object v2, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/a;->a:[I

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PaymentInformationWidgetData;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;->getSelPaymentMethod()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 139
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isKorea()Z

    move-result v1

    if-ne v1, v4, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PaymentInformationWidgetData;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getPaymentPrice()D

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmpg-double v1, v1, v3

    if-gtz v1, :cond_0

    .line 144
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PaymentInformationWidgetData;->a:Landroid/content/Context;

    const v1, 0x7f080134

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 163
    :cond_0
    :goto_0
    return-object v0

    .line 103
    :pswitch_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PaymentInformationWidgetData;->a:Landroid/content/Context;

    invoke-virtual {v0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 108
    :goto_1
    const-string v1, "%s\n\n%s"

    new-array v2, v6, [Ljava/lang/Object;

    aput-object v0, v2, v5

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PaymentInformationWidgetData;->a:Landroid/content/Context;

    const v3, 0x7f080303

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 106
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PaymentInformationWidgetData;->a:Landroid/content/Context;

    invoke-virtual {v0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 113
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PaymentInformationWidgetData;->a:Landroid/content/Context;

    const v1, 0x7f080298

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PaymentInformationWidgetData;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getCyberCashInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PaymentInformationWidgetData;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getCyberCashInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;->getCount()I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PaymentInformationWidgetData;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getCyberCashInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;->getAt(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashOperator;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashOperator;->getURL()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eq v2, v4, :cond_0

    const-string v2, "%s %s"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v0, v3, v5

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 120
    :pswitch_2
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PaymentInformationWidgetData;->a:Landroid/content/Context;

    invoke-virtual {v0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 123
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PaymentInformationWidgetData;->a:Landroid/content/Context;

    invoke-virtual {v0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 129
    :pswitch_3
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isTurkey()Z

    move-result v1

    if-ne v1, v4, :cond_0

    .line 134
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PaymentInformationWidgetData;->a:Landroid/content/Context;

    const v1, 0x7f0802ff

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 147
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PaymentInformationWidgetData;->a:Landroid/content/Context;

    const v1, 0x7f080245

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 99
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public infomationExists()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 44
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PaymentInformationWidgetData;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;->getSelPaymentMethod()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    move-result-object v2

    if-nez v2, :cond_1

    .line 59
    :cond_0
    :goto_0
    return v0

    .line 47
    :cond_1
    sget-object v2, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/a;->a:[I

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PaymentInformationWidgetData;->b:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;->getSelPaymentMethod()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    move v0, v1

    .line 55
    goto :goto_0

    .line 57
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PaymentInformationWidgetData;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isTurkey()Z

    move-result v2

    if-ne v2, v1, :cond_0

    move v0, v1

    goto :goto_0

    .line 47
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public release()V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/purchase/interfaces/PaymentInformationWidgetData;->a:Landroid/content/Context;

    .line 30
    return-void
.end method
