.class public Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;
.super Lcom/sec/android/app/samsungapps/widget/CommonWidget;
.source "ProGuard"


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

.field private b:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Ljava/lang/String;

.field private h:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

.field private i:Landroid/widget/RatingBar;

.field private j:Landroid/widget/ImageView;

.field private k:Z

.field private l:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;)V

    .line 41
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->mContext:Landroid/content/Context;

    .line 42
    const v0, 0x7f040066

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->initView(Landroid/content/Context;I)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->mContext:Landroid/content/Context;

    .line 49
    const v0, 0x7f040066

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->initView(Landroid/content/Context;I)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->mContext:Landroid/content/Context;

    .line 56
    const v0, 0x7f040066

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->initView(Landroid/content/Context;I)V

    .line 57
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->b:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic e(Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->d:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic i(Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->f:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic j(Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic k(Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->e:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public loadWidget()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 71
    const v0, 0x7f0c0195

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->h:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->h:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->h:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->mContext:Landroid/content/Context;

    const-string v2, "isa_chart_default_big"

    const-string v3, "drawable"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/samsungapps/uiutil/KnoxResourceManager;->findResource(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setBackgroundResource(I)V

    .line 75
    :cond_0
    const v0, 0x7f0c0196

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->b:Landroid/widget/TextView;

    .line 76
    const v0, 0x7f0c0197

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->d:Landroid/widget/TextView;

    .line 77
    const v0, 0x7f0c0063

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->e:Landroid/widget/TextView;

    .line 78
    const v0, 0x7f0c0062

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->f:Landroid/widget/TextView;

    .line 79
    const v0, 0x7f0c0198

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RatingBar;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->i:Landroid/widget/RatingBar;

    .line 80
    const v0, 0x7f0c005b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->j:Landroid/widget/ImageView;

    .line 81
    const v0, 0x7f0c0194

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 82
    const-string v1, "%02d"

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->showProductName()V

    .line 85
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->showProductImg()V

    .line 86
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->showAdultIcon()V

    .line 87
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->showCategory()V

    .line 88
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->showProductRating()V

    .line 89
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->showProductnormalPrice()V

    .line 90
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 65
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->onFinishInflate()V

    .line 66
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->setFocusable(Z)V

    .line 67
    return-void
.end method

.method public refreshWidget()V
    .locals 0

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->showProductImg()V

    .line 113
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->showAdultIcon()V

    .line 114
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 94
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->b:Landroid/widget/TextView;

    .line 95
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->d:Landroid/widget/TextView;

    .line 96
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->f:Landroid/widget/TextView;

    .line 97
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->h:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    .line 98
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->e:Landroid/widget/TextView;

    .line 99
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->i:Landroid/widget/RatingBar;

    .line 100
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->j:Landroid/widget/ImageView;

    .line 101
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->removeAllViews()V

    .line 102
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->release()V

    .line 103
    return-void
.end method

.method public setContent(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 61
    return-void
.end method

.method public setWidgetData(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 119
    return-void
.end method

.method public showAdultIcon()V
    .locals 2

    .prologue
    .line 173
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->j:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/view/restrictedappcheckutil/RestrictedAppCheckUtil;->isAdultIcon(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->l:Z

    .line 175
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->l:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->j:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 181
    :cond_0
    :goto_0
    return-void

    .line 178
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->j:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public showCategory()V
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->d:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->categoryName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.feature.hovering_ui"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->d:Landroid/widget/TextView;

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/banner/j;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/banner/j;-><init>(Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 203
    :cond_0
    return-void
.end method

.method public showProductImg()V
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->h:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/view/restrictedappcheckutil/RestrictedAppCheckUtil;->isAdultBlur(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->k:Z

    .line 152
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->k:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->h:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    const v1, 0x7f02000e

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setBackgroundResource(I)V

    .line 169
    :cond_0
    :goto_0
    return-void

    .line 158
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->h:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getNetAPI()Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setNetAPI(Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;)V

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->h:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/banner/i;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/banner/i;-><init>(Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setConverter(Lcom/sec/android/app/samsungapps/view/CacheWebImageView$BitmapConverter;)V

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->h:Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductImageURL()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setURL(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public showProductName()V
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->b:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.feature.hovering_ui"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->b:Landroid/widget/TextView;

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/banner/h;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/banner/h;-><init>(Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 144
    :cond_0
    return-void
.end method

.method public showProductRating()V
    .locals 5

    .prologue
    .line 207
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 209
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->i:Landroid/widget/RatingBar;

    if-eqz v0, :cond_0

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->i:Landroid/widget/RatingBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RatingBar;->setVisibility(I)V

    .line 217
    :cond_0
    :goto_0
    return-void

    .line 214
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->i:Landroid/widget/RatingBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->i:Landroid/widget/RatingBar;

    const-wide/high16 v1, 0x3fe0000000000000L    # 0.5

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    iget v3, v3, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->averageRating:I

    int-to-double v3, v3

    mul-double/2addr v1, v3

    double-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/RatingBar;->setRating(F)V

    goto :goto_0
.end method

.method public showProductnormalPrice()V
    .locals 11

    .prologue
    const-wide/16 v9, 0x0

    .line 222
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 223
    if-nez v0, :cond_1

    .line 285
    :cond_0
    :goto_0
    return-void

    .line 228
    :cond_1
    iget-wide v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->price:D

    .line 229
    iget-wide v3, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->discountPrice:D

    .line 231
    iget-object v5, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f08029b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->g:Ljava/lang/String;

    .line 233
    iget-boolean v5, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->discountFlag:Z

    if-eqz v5, :cond_3

    .line 235
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->e:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 236
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->e:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->e:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v2

    or-int/lit8 v2, v2, 0x10

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 237
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->e:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "("

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v5

    iget-wide v6, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->price:D

    iget-object v8, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->currencyUnit:Ljava/lang/String;

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getFormattedPrice(DLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ")"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 238
    cmpl-double v1, v3, v9

    if-nez v1, :cond_2

    .line 240
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->g:Ljava/lang/String;

    .line 246
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 265
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.feature.hovering_ui"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 266
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->f:Landroid/widget/TextView;

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/banner/k;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/banner/k;-><init>(Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 275
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->e:Landroid/widget/TextView;

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/banner/l;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/banner/l;-><init>(Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    goto/16 :goto_0

    .line 244
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->currencyUnit:Ljava/lang/String;

    invoke-virtual {v1, v3, v4, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getFormattedPrice(DLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 251
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->e:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 253
    cmpl-double v3, v1, v9

    if-nez v3, :cond_4

    .line 255
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->g:Ljava/lang/String;

    .line 261
    :goto_3
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 259
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v3

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->currencyUnit:Ljava/lang/String;

    invoke-virtual {v3, v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getFormattedPrice(DLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method public updateWidget()V
    .locals 0

    .prologue
    .line 108
    return-void
.end method
