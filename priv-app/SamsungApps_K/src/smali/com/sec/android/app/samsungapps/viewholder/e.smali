.class final Lcom/sec/android/app/samsungapps/viewholder/e;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;)V
    .locals 0

    .prologue
    .line 329
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/viewholder/e;->a:Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 333
    invoke-virtual {p2, v3}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 334
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/16 v1, 0x9

    if-ne v0, v1, :cond_0

    .line 335
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/e;->a:Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Context:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/e;->a:Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->categoryName:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewholder/e;->a:Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->categoryName:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/view/SamsungAppsHoveringView;->setTextViewHovering(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;)V

    .line 340
    :cond_0
    :goto_0
    return v3

    .line 337
    :cond_1
    invoke-virtual {p2, v3}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 338
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/e;->a:Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->_Context:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/e;->a:Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;->categoryName:Landroid/widget/TextView;

    invoke-static {v0, v1, v3}, Lcom/sec/android/app/samsungapps/view/SamsungAppsHoveringView;->setFHAnimation(Landroid/content/Context;Landroid/view/View;Z)V

    goto :goto_0
.end method
