.class public Lcom/sec/android/app/samsungapps/vlibrary/util/mock/MockTimeoutRetrier;
.super Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;
.source "ProGuard"


# instance fields
.field public bCalledRetry:Z


# direct methods
.method public constructor <init>(JILcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier$RetryAction;)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;-><init>(JILcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier$RetryAction;)V

    .line 12
    return-void
.end method


# virtual methods
.method protected createTimer()V
    .locals 0

    .prologue
    .line 17
    return-void
.end method

.method public runHandlerRetry()V
    .locals 2

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/mock/MockTimeoutRetrier;->bCalledRetry:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/mock/MockTimeoutRetrier;->bCalledRetry:Z

    .line 32
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/mock/MockTimeoutRetrier;->runAction()V

    .line 34
    :cond_0
    return-void
.end method

.method protected sendRetryMsgToHandler()V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/util/mock/MockTimeoutRetrier;->bCalledRetry:Z

    .line 25
    return-void
.end method
