.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/LogoutResponseParser;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;
.source "ProGuard"


# instance fields
.field mAccount:Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;-><init>()V

    .line 9
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/LogoutResponseParser;->mAccount:Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    .line 10
    return-void
.end method


# virtual methods
.method protected onCreateObject(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 29
    return-void
.end method

.method protected onPostParseError()V
    .locals 2

    .prologue
    .line 14
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/LogoutResponseParser;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/LogoutResponseParser;->mAccount:Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->setLoginInfo(Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;)V

    .line 23
    :cond_0
    return-void
.end method

.method protected onPostParseResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 34
    return-void
.end method
