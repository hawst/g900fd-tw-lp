.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/IDDuplicationCheckInfo;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private mEmailID:Ljava/lang/String;

.field private mPassword:Ljava/lang/String;

.field private mRePassword:Ljava/lang/String;

.field private mResult:Lcom/sec/android/app/samsungapps/vlibrary/doc/IDDuplicationCheckResult;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/IDDuplicationCheckInfo;->mResult:Lcom/sec/android/app/samsungapps/vlibrary/doc/IDDuplicationCheckResult;

    .line 10
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/IDDuplicationCheckInfo;->mEmailID:Ljava/lang/String;

    .line 11
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/IDDuplicationCheckInfo;->mPassword:Ljava/lang/String;

    .line 12
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/IDDuplicationCheckInfo;->mRePassword:Ljava/lang/String;

    .line 13
    return-void
.end method


# virtual methods
.method public getEmailID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/IDDuplicationCheckInfo;->mEmailID:Ljava/lang/String;

    return-object v0
.end method

.method public getPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/IDDuplicationCheckInfo;->mPassword:Ljava/lang/String;

    return-object v0
.end method

.method public getRePassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/IDDuplicationCheckInfo;->mRePassword:Ljava/lang/String;

    return-object v0
.end method

.method public getResult()Lcom/sec/android/app/samsungapps/vlibrary/doc/IDDuplicationCheckResult;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/IDDuplicationCheckInfo;->mResult:Lcom/sec/android/app/samsungapps/vlibrary/doc/IDDuplicationCheckResult;

    return-object v0
.end method

.method public setResult(Lcom/sec/android/app/samsungapps/vlibrary/doc/IDDuplicationCheckResult;)V
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/IDDuplicationCheckInfo;->mResult:Lcom/sec/android/app/samsungapps/vlibrary/doc/IDDuplicationCheckResult;

    .line 32
    return-void
.end method
