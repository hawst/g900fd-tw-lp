.class public Lcom/sec/android/app/samsungapps/vlibrary2/country/ReqCountrySearchExCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private mError:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 21
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/country/ReqCountrySearchExCommand;Z)V
    .locals 0

    .prologue
    .line 14
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/country/ReqCountrySearchExCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$102(Lcom/sec/android/app/samsungapps/vlibrary2/country/ReqCountrySearchExCommand;Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;)Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;
    .locals 0

    .prologue
    .line 14
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/ReqCountrySearchExCommand;->mError:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/country/ReqCountrySearchExCommand;Z)V
    .locals 0

    .prologue
    .line 14
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/country/ReqCountrySearchExCommand;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method public getError()Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/country/ReqCountrySearchExCommand;->mError:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;

    return-object v0
.end method

.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 4

    .prologue
    .line 25
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getNetHeaderInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/country/k;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/k;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/country/ReqCountrySearchExCommand;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->countrySearchEx(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 42
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 43
    return-void
.end method
