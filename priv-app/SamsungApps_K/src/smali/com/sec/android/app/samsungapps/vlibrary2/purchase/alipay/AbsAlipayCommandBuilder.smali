.class public abstract Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AbsAlipayCommandBuilder;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInstallCheckCommand$IAlipayInstallCheckCommandData;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayUpdateCommand$IAlipayUpdateCommandData;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;


# instance fields
.field _AlipayIntResult:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayIntResult;

.field private _FinalMap:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

.field private _IPurchaseCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseCommandBuilder;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayIntResult;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayIntResult;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AbsAlipayCommandBuilder;->_AlipayIntResult:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayIntResult;

    .line 17
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AbsAlipayCommandBuilder;->_IPurchaseCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseCommandBuilder;

    .line 18
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AbsAlipayCommandBuilder;->_FinalMap:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    .line 19
    return-void
.end method


# virtual methods
.method public checkAlipayInstallCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInstallCheckCommand;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInstallCheckCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInstallCheckCommand$IAlipayInstallCheckCommandData;)V

    return-object v0
.end method

.method public complete()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 3

    .prologue
    .line 32
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AbsAlipayCommandBuilder;->_IPurchaseCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseCommandBuilder;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AbsAlipayCommandBuilder;->_FinalMap:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;)V

    return-object v0
.end method

.method public abstract createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
.end method

.method public createPayment()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 38
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayPayment;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/IAlipayCommandBuilder;)V

    return-object v0
.end method

.method public getAlipayInitResult()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayIntResult;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AbsAlipayCommandBuilder;->_AlipayIntResult:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayIntResult;

    return-object v0
.end method

.method protected abstract getInstallerFactory()Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;
.end method

.method public init()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 3

    .prologue
    .line 22
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInitCommand;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AbsAlipayCommandBuilder;->_AlipayIntResult:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayIntResult;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AbsAlipayCommandBuilder;->_IPurchaseCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseCommandBuilder;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayInitCommand;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayIntResult;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseCommandBuilder;)V

    return-object v0
.end method

.method public installAlipay()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 3

    .prologue
    .line 52
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayUpdateCommand;

    const-string v1, "com.alipay.android.app"

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AbsAlipayCommandBuilder;->getInstallerFactory()Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;

    move-result-object v2

    invoke-direct {v0, v1, p0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayUpdateCommand;-><init>(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayUpdateCommand$IAlipayUpdateCommandData;Lcom/sec/android/app/samsungapps/vlibrary3/installer/InstallerFactory;)V

    .line 53
    return-object v0
.end method

.method public abstract onNotifyAlipay(Z)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method

.method public abstract process()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.end method
