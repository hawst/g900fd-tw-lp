.class public final enum Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;

.field public static final enum CARD_ERROR_5017:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;

.field public static final enum DELETE_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;

.field public static final enum DELETE_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;

.field public static final enum USER_AGREE:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;

.field public static final enum USER_DISAGREE:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 14
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;

    const-string v1, "CARD_ERROR_5017"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;->CARD_ERROR_5017:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;

    .line 15
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;

    const-string v1, "USER_AGREE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;->USER_AGREE:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;

    .line 16
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;

    const-string v1, "USER_DISAGREE"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;->USER_DISAGREE:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;

    .line 17
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;

    const-string v1, "DELETE_FAILED"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;->DELETE_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;

    .line 18
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;

    const-string v1, "DELETE_SUCCESS"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;->DELETE_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;

    .line 12
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;->CARD_ERROR_5017:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;->USER_AGREE:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;->USER_DISAGREE:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;->DELETE_FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;->DELETE_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;

    return-object v0
.end method
