.class public Lcom/sec/android/app/samsungapps/MobileSecurePayer;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field static a:Ljava/lang/String;


# instance fields
.field final b:Ljava/lang/Integer;

.field c:Lcom/alipay/android/app/IAlixPay;

.field d:Z

.field e:Landroid/app/Activity;

.field private f:Landroid/content/ServiceConnection;

.field private g:Lcom/alipay/android/app/IRemoteServiceCallback;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-string v0, "MobileSecurePayer"

    sput-object v0, Lcom/sec/android/app/samsungapps/MobileSecurePayer;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/MobileSecurePayer;->b:Ljava/lang/Integer;

    .line 30
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/MobileSecurePayer;->c:Lcom/alipay/android/app/IAlixPay;

    .line 31
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/MobileSecurePayer;->d:Z

    .line 33
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/MobileSecurePayer;->e:Landroid/app/Activity;

    .line 35
    new-instance v0, Lcom/sec/android/app/samsungapps/fd;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/fd;-><init>(Lcom/sec/android/app/samsungapps/MobileSecurePayer;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/MobileSecurePayer;->f:Landroid/content/ServiceConnection;

    .line 135
    new-instance v0, Lcom/sec/android/app/samsungapps/ff;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/ff;-><init>(Lcom/sec/android/app/samsungapps/MobileSecurePayer;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/MobileSecurePayer;->g:Lcom/alipay/android/app/IRemoteServiceCallback;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/MobileSecurePayer;)Lcom/alipay/android/app/IRemoteServiceCallback;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/MobileSecurePayer;->g:Lcom/alipay/android/app/IRemoteServiceCallback;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/MobileSecurePayer;)Landroid/content/ServiceConnection;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/MobileSecurePayer;->f:Landroid/content/ServiceConnection;

    return-object v0
.end method


# virtual methods
.method public pay(Ljava/lang/String;Landroid/os/Handler;ILandroid/app/Activity;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 57
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/MobileSecurePayer;->d:Z

    if-eqz v1, :cond_0

    .line 58
    const/4 v0, 0x0

    .line 128
    :goto_0
    return v0

    .line 60
    :cond_0
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/MobileSecurePayer;->d:Z

    .line 63
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/MobileSecurePayer;->e:Landroid/app/Activity;

    .line 66
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/MobileSecurePayer;->c:Lcom/alipay/android/app/IAlixPay;

    if-nez v1, :cond_1

    .line 68
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/MobileSecurePayer;->e:Landroid/app/Activity;

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/alipay/android/app/IAlixPay;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/MobileSecurePayer;->f:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2, v3, v0}, Landroid/app/Activity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 73
    :cond_1
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/sec/android/app/samsungapps/fe;

    invoke-direct {v2, p0, p3, p2, p1}, Lcom/sec/android/app/samsungapps/fe;-><init>(Lcom/sec/android/app/samsungapps/MobileSecurePayer;ILandroid/os/Handler;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method
