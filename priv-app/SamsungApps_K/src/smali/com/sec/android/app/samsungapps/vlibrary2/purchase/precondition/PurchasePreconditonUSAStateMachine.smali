.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditonUSAStateMachine;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;
.source "ProGuard"


# static fields
.field private static instance:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditonUSAStateMachine;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;-><init>()V

    .line 24
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditonUSAStateMachine;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditonUSAStateMachine;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditonUSAStateMachine;

    if-nez v0, :cond_0

    .line 16
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditonUSAStateMachine;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditonUSAStateMachine;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditonUSAStateMachine;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditonUSAStateMachine;

    .line 18
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditonUSAStateMachine;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditonUSAStateMachine;

    return-object v0
.end method


# virtual methods
.method protected entry(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 2

    .prologue
    .line 72
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/d;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 91
    :goto_0
    :pswitch_0
    return-void

    .line 75
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Action;->SHOW_POPUP:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 78
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Action;->SEND_SIG_FAIL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 79
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditonUSAStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 84
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Action;->ACTION_REGISTER_CREDIT_CARD:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 87
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Action;->SEND_SIG_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    .line 88
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditonUSAStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 72
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method public execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;)Z
    .locals 2

    .prologue
    .line 28
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/d;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 67
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 31
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/d;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 34
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditonUSAStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 37
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$State;->ASK_USER:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditonUSAStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 40
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditonUSAStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 45
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/d;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 48
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditonUSAStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 51
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$State;->REGISTER_CREDIT_CARD:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditonUSAStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 56
    :pswitch_7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/d;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    goto :goto_0

    .line 59
    :pswitch_8
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditonUSAStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 62
    :pswitch_9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditonUSAStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 28
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_4
        :pswitch_7
    .end packed-switch

    .line 31
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 45
    :pswitch_data_2
    .packed-switch 0x4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 56
    :pswitch_data_3
    .packed-switch 0x6
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public bridge synthetic execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 9
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/PurchasePreconditonUSAStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/precondition/IPurchasePreconditionEvent$Event;)Z

    move-result v0

    return v0
.end method

.method protected exit(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 0

    .prologue
    .line 97
    return-void
.end method
