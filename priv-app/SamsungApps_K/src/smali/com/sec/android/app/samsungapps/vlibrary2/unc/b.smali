.class final Lcom/sec/android/app/samsungapps/vlibrary2/unc/b;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# instance fields
.field a:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;

.field b:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

.field final synthetic c:Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;)V
    .locals 2

    .prologue
    .line 134
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/b;->c:Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/b;->c:Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->_ItemCount:I
    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/unc/UpgradeListManager;)I

    move-result v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/b;->a:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;

    .line 137
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/b;->b:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    return-void
.end method


# virtual methods
.method public final a()Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/b;->a:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;

    return-object v0
.end method

.method public final addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/b;->b:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/b;->b:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 159
    :cond_0
    return-void
.end method

.method public final clearContainer()V
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/b;->a:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->clear()V

    .line 164
    return-void
.end method

.method public final closeMap()V
    .locals 2

    .prologue
    .line 145
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/b;->b:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    if-eqz v0, :cond_0

    .line 147
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/b;->b:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 148
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/b;->a:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->add(Ljava/lang/Object;)Z

    .line 150
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/b;->b:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 151
    return-void
.end method

.method public final findString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 178
    const/4 v0, 0x0

    return-object v0
.end method

.method public final openMap()V
    .locals 1

    .prologue
    .line 140
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/b;->b:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 141
    return-void
.end method

.method public final setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/b;->a:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->setHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V

    .line 184
    return-void
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/b;->a:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;

    if-nez v0, :cond_0

    .line 170
    const/4 v0, 0x0

    .line 172
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/unc/b;->a:Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->size()I

    move-result v0

    goto :goto_0
.end method
