.class public Lcom/sec/android/app/samsungapps/autoupdateservice/AutoUpdateService;
.super Landroid/app/Service;
.source "ProGuard"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method static synthetic a(Landroid/content/Context;I)V
    .locals 5

    .prologue
    .line 21
    new-instance v0, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0801ab

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "samsungapps://GoUpdates/"

    const v3, 0x4e8efc6

    const-string v4, "SamsungApps"

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/notification/CNotificationManager;->registerPushNotify(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/autoupdateservice/AutoUpdateService;)V
    .locals 1

    .prologue
    .line 21
    const-string v0, "AutoUpdate StopSelf"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/autoupdateservice/AutoUpdateService;->stopSelf()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    return-object v0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2

    .prologue
    .line 31
    :try_start_0
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getAutoUpdateManager()Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/autoupdateservice/a;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/autoupdateservice/a;-><init>(Lcom/sec/android/app/samsungapps/autoupdateservice/AutoUpdateService;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager$IAutoUpdateManagerObserver;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/AutoUpdateManager;->execute()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    .line 37
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    return v0

    :catch_0
    move-exception v0

    goto :goto_0

    .line 36
    :catch_1
    move-exception v0

    goto :goto_0
.end method
