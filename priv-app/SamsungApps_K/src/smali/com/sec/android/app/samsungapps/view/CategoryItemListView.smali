.class public Lcom/sec/android/app/samsungapps/view/CategoryItemListView;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field a:Landroid/widget/AdapterView$OnItemClickListener;

.field private b:Landroid/content/Context;

.field private c:Lcom/sec/android/app/samsungapps/view/CategoryItemListView$ICategoryData;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

.field private e:Lcom/sec/android/app/samsungapps/view/CategoryItemListAdapter;

.field private f:Ljava/util/ArrayList;

.field private g:Landroid/widget/ListView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/ListView;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->b:Landroid/content/Context;

    .line 34
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->c:Lcom/sec/android/app/samsungapps/view/CategoryItemListView$ICategoryData;

    .line 35
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

    .line 36
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->e:Lcom/sec/android/app/samsungapps/view/CategoryItemListAdapter;

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->f:Ljava/util/ArrayList;

    .line 172
    new-instance v0, Lcom/sec/android/app/samsungapps/view/g;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/view/g;-><init>(Lcom/sec/android/app/samsungapps/view/CategoryItemListView;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->a:Landroid/widget/AdapterView$OnItemClickListener;

    .line 46
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->b:Landroid/content/Context;

    .line 47
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->g:Landroid/widget/ListView;

    .line 48
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCategoryContainer()Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

    new-instance v0, Lcom/sec/android/app/samsungapps/view/CategoryItemListAdapter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->f:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/view/CategoryItemListAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->e:Lcom/sec/android/app/samsungapps/view/CategoryItemListAdapter;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->g:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->e:Lcom/sec/android/app/samsungapps/view/CategoryItemListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->g:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->a:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 50
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->g:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 51
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->c(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/view/CategoryItemListView;)V
    .locals 5

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->getRoot()Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->getRoot()Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->needSubCategory()Z

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->f:Ljava/util/ArrayList;

    new-instance v4, Lcom/sec/android/app/samsungapps/view/h;

    invoke-direct {v4, p0, v0, v2}, Lcom/sec/android/app/samsungapps/view/h;-><init>(Lcom/sec/android/app/samsungapps/view/CategoryItemListView;Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;Z)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->c:Lcom/sec/android/app/samsungapps/view/CategoryItemListView$ICategoryData;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->c:Lcom/sec/android/app/samsungapps/view/CategoryItemListView$ICategoryData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/view/CategoryItemListView$ICategoryData;->stop()V

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->e:Lcom/sec/android/app/samsungapps/view/CategoryItemListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/CategoryItemListAdapter;->notifyDataSetInvalidated()V

    :cond_2
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/view/CategoryItemListView;Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;)V
    .locals 2

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->b:Landroid/content/Context;

    const-class v1, Lcom/sec/android/app/samsungapps/CategorySubListActivity;

    invoke-static {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->startActivityWithObject(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/view/CategoryItemListView;Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->b(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;)V

    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/view/CategoryItemListView;)Lcom/sec/android/app/samsungapps/view/CategoryItemListView$ICategoryData;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->c:Lcom/sec/android/app/samsungapps/view/CategoryItemListView$ICategoryData;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/view/CategoryItemListView;Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;)V
    .locals 2

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->b:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->startCategorizedListActivity(Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->d:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->setSel(Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;Z)Z

    :cond_0
    return-void
.end method

.method private b(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->c:Lcom/sec/android/app/samsungapps/view/CategoryItemListView$ICategoryData;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->c:Lcom/sec/android/app/samsungapps/view/CategoryItemListView$ICategoryData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/view/CategoryItemListView$ICategoryData;->stop()V

    .line 108
    :cond_0
    if-eqz p1, :cond_2

    .line 109
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->size()I

    move-result v3

    move v1, v2

    .line 111
    :goto_0
    if-ge v1, v3, :cond_1

    .line 112
    invoke-virtual {p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    .line 113
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->f:Ljava/util/ArrayList;

    new-instance v5, Lcom/sec/android/app/samsungapps/view/h;

    invoke-direct {v5, p0, v0, v2}, Lcom/sec/android/app/samsungapps/view/h;-><init>(Lcom/sec/android/app/samsungapps/view/CategoryItemListView;Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;Z)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 111
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 115
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->e:Lcom/sec/android/app/samsungapps/view/CategoryItemListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/CategoryItemListAdapter;->notifyDataSetInvalidated()V

    .line 117
    :cond_2
    return-void
.end method

.method private static c(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 146
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->upLevelCategoryName:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    .line 155
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 154
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/view/CategoryItemListView;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->f:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public load()V
    .locals 3

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->c:Lcom/sec/android/app/samsungapps/view/CategoryItemListView$ICategoryData;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getCategoryCommandBuilder()Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/CategoryCommandBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/CategoryCommandBuilder;->loadAllCategory()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->b:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/view/e;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/view/e;-><init>(Lcom/sec/android/app/samsungapps/view/CategoryItemListView;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 85
    :cond_0
    return-void
.end method

.method public load(Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;)V
    .locals 2

    .prologue
    .line 89
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->c:Lcom/sec/android/app/samsungapps/view/CategoryItemListView$ICategoryData;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->c:Lcom/sec/android/app/samsungapps/view/CategoryItemListView$ICategoryData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/view/CategoryItemListView$ICategoryData;->stop()V

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 93
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->isNull([Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 94
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->getChildList()Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    move-result-object v0

    .line 95
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->b(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    .line 102
    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    .line 101
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public load(Ljava/lang/String;Lcom/sec/android/app/samsungapps/view/CategoryItemListView$ICallback;)V
    .locals 4

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    .line 121
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v1

    .line 122
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    const-string v3, "0"

    invoke-direct {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;-><init>(Ljava/lang/String;)V

    .line 123
    new-instance v3, Lcom/sec/android/app/samsungapps/view/f;

    invoke-direct {v3, p0, v2, p2}, Lcom/sec/android/app/samsungapps/view/f;-><init>(Lcom/sec/android/app/samsungapps/view/CategoryItemListView;Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;Lcom/sec/android/app/samsungapps/view/CategoryItemListView$ICallback;)V

    invoke-virtual {v1, p1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->contentCategoryListSearch(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v1

    .line 139
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->b:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 140
    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 141
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->a:Landroid/widget/AdapterView$OnItemClickListener;

    .line 69
    return-void
.end method

.method public setData(Lcom/sec/android/app/samsungapps/view/CategoryItemListView$ICategoryData;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->c:Lcom/sec/android/app/samsungapps/view/CategoryItemListView$ICategoryData;

    .line 55
    return-void
.end method

.method public startCategorizedListActivity(Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;)V
    .locals 3

    .prologue
    .line 202
    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->b:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 203
    const-string v0, "_listType"

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->Category:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 204
    const/high16 v0, 0x20000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 205
    const-string v0, "_titleText"

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 206
    const-string v0, "_category"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 207
    const-string v0, "_buttonState"

    sget-object v2, Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;->ALL_PAID_FREE_NEW:Lcom/sec/android/app/samsungapps/view/ListViewButtonStates;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 209
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/CategoryItemListView;->b:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/CommonActivity;->commonStartActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 210
    return-void
.end method
