.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum BUY:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

.field public static final enum CANCELLABLE:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

.field public static final enum CANCEL_DISABLED:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

.field public static final enum DOWNLOAD_COMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

.field public static final enum GET:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

.field public static final enum GOOGLE_BUY:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

.field public static final enum GOOGLE_GET:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

.field public static final enum INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

.field public static final enum LAUNCHABLE:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

.field public static final enum LAUNCH_DISABLED:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

.field public static final enum SEE_THIS_APP_IN_GEAR_MANAGER:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

.field public static final enum UPDATABLE:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 13
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    const-string v1, "LAUNCH_DISABLED"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->LAUNCH_DISABLED:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    const-string v1, "LAUNCHABLE"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->LAUNCHABLE:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    const-string v1, "GET"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->GET:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    const-string v1, "BUY"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->BUY:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    const-string v1, "UPDATABLE"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->UPDATABLE:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    const-string v1, "GOOGLE_GET"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->GOOGLE_GET:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    const-string v1, "GOOGLE_BUY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->GOOGLE_BUY:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    const-string v1, "DOWNLOAD_COMPLETED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->DOWNLOAD_COMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    const-string v1, "CANCEL_DISABLED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->CANCEL_DISABLED:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    const-string v1, "CANCELLABLE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->CANCELLABLE:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    const-string v1, "INSTALL"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    const-string v1, "SEE_THIS_APP_IN_GEAR_MANAGER"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->SEE_THIS_APP_IN_GEAR_MANAGER:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    .line 11
    const/16 v0, 0xc

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->LAUNCH_DISABLED:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->LAUNCHABLE:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->GET:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->BUY:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->UPDATABLE:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->GOOGLE_GET:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->GOOGLE_BUY:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->DOWNLOAD_COMPLETED:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->CANCEL_DISABLED:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->CANCELLABLE:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->INSTALL:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->SEE_THIS_APP_IN_GEAR_MANAGER:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;

    return-object v0
.end method
