.class public Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;


# instance fields
.field private _Context:Landroid/content/Context;

.field private _ILoadingDialogCreator:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;

.field private _InvalidCardCheckerObserver:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker$InvalidCardCheckerObserver;

.field private _State:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$State;

.field private dlg:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

.field private mNotiPopupFactory:Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$State;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;->_State:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$State;

    .line 27
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;->_ILoadingDialogCreator:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;

    .line 28
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;->mNotiPopupFactory:Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;

    .line 29
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;->_Context:Landroid/content/Context;

    .line 30
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;->sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;)V

    return-void
.end method

.method private requestDeleteCard()V
    .locals 3

    .prologue
    .line 121
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/c;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->resetEasybuySetting(Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 135
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 136
    return-void
.end method

.method private sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;)V
    .locals 1

    .prologue
    .line 50
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCreditCardStateMachine;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCreditCardStateMachine;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCreditCardStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;)Z

    .line 51
    return-void
.end method

.method private sendSigCardDeleted()V
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;->_InvalidCardCheckerObserver:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker$InvalidCardCheckerObserver;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;->_InvalidCardCheckerObserver:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker$InvalidCardCheckerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker$InvalidCardCheckerObserver;->onCardDeleted()V

    .line 118
    :cond_0
    return-void
.end method

.method private showAskDeleteCardPopup()V
    .locals 2

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;->mNotiPopupFactory:Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;->_Context:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;->createNotiPopup(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopup;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/b;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;)V

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopup;->askDeleteCreditCard(Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupResponse;)V

    .line 111
    return-void
.end method

.method private startLoading()V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;->_ILoadingDialogCreator:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialogCreator;->createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;->dlg:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;->dlg:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->startLoading()V

    .line 96
    return-void
.end method

.method private stopLoading()V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;->dlg:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;->dlg:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->endLoading()V

    .line 89
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;->dlg:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    .line 91
    :cond_0
    return-void
.end method


# virtual methods
.method public execute()V
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;->CARD_ERROR_5017:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;->sendEvent(Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Event;)V

    .line 43
    return-void
.end method

.method public getState()Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$State;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;->_State:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$State;

    return-object v0
.end method

.method public bridge synthetic getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;->getState()Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$State;

    move-result-object v0

    return-object v0
.end method

.method public onAction(Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;)V
    .locals 2

    .prologue
    .line 65
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/d;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 83
    :goto_0
    return-void

    .line 68
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;->requestDeleteCard()V

    goto :goto_0

    .line 71
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;->sendSigCardDeleted()V

    goto :goto_0

    .line 74
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;->showAskDeleteCardPopup()V

    goto :goto_0

    .line 77
    :pswitch_3
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;->startLoading()V

    goto :goto_0

    .line 80
    :pswitch_4
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;->stopLoading()V

    goto :goto_0

    .line 65
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public bridge synthetic onAction(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 19
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;->onAction(Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$Action;)V

    return-void
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker$InvalidCardCheckerObserver;)V
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;->_InvalidCardCheckerObserver:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker$InvalidCardCheckerObserver;

    .line 35
    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$State;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;->_State:Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$State;

    .line 56
    return-void
.end method

.method public bridge synthetic setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 19
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$State;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardChecker;->setState(Lcom/sec/android/app/samsungapps/vlibrary2/creditcard/InvalidCardState$State;)V

    return-void
.end method
