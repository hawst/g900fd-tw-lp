.class final Lcom/sec/android/app/samsungapps/commands/ag;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/CheckBox;

.field final synthetic b:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

.field final synthetic c:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;Landroid/widget/CheckBox;Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;)V
    .locals 0

    .prologue
    .line 299
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/commands/ag;->c:Lcom/sec/android/app/samsungapps/commands/CCheckAppUpgradeCommandBuilder;

    iput-object p2, p0, Lcom/sec/android/app/samsungapps/commands/ag;->a:Landroid/widget/CheckBox;

    iput-object p3, p0, Lcom/sec/android/app/samsungapps/commands/ag;->b:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 302
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/ag;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 303
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/ag;->a:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 304
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/ag;->b:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    if-eqz v0, :cond_0

    .line 305
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/ag;->b:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v1, "update_self_setting"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 313
    :cond_0
    :goto_0
    return-void

    .line 308
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/ag;->a:Landroid/widget/CheckBox;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 309
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/ag;->b:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/ag;->b:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v1, "update_self_setting"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0
.end method
