.class public Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;
.super Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;
.source "ProGuard"


# instance fields
.field private a:I


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;Landroid/content/Context;Landroid/view/View;Landroid/graphics/drawable/Drawable;Ljava/lang/String;ZILcom/sec/android/app/samsungapps/view/IContentArrayAdapter;I)V
    .locals 0

    .prologue
    .line 22
    invoke-direct/range {p0 .. p8}, Lcom/sec/android/app/samsungapps/viewholder/ContentListViewHolder;-><init>(Lcom/sec/android/app/samsungapps/viewholder/ContentListViewInfo;Landroid/content/Context;Landroid/view/View;Landroid/graphics/drawable/Drawable;Ljava/lang/String;ZILcom/sec/android/app/samsungapps/view/IContentArrayAdapter;)V

    .line 23
    iput p9, p0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->a:I

    .line 24
    return-void
.end method


# virtual methods
.method public displayNormalItem()V
    .locals 0

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->showProductName()V

    .line 30
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->showProductImg()V

    .line 31
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->showCategory()V

    .line 32
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->showProductType()V

    .line 33
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->showAdultIcon()V

    .line 34
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->showProductnormalPrice()V

    .line 35
    return-void
.end method

.method public showCommonStatus()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->deletecheckbox:Landroid/widget/CheckedTextView;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->deletecheckbox:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setVisibility(I)V

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->layout_list_itemly_rightly:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->layout_list_itemly_rightly:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 132
    :cond_1
    return-void
.end method

.method public showProductName()V
    .locals 9

    .prologue
    const v8, 0x7f02012d

    const v7, 0x7f020124

    const/16 v3, 0x8

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 40
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->google_tencent_icon:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->google_tencent_icon:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 44
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->textname:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 46
    iget v0, p0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->_Position:I

    add-int/lit8 v0, v0, 0x1

    .line 48
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "[S_I_N_A]"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 50
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[S_I_N_A]"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 51
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->textname:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->_Context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.feature.hovering_ui"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-ne v0, v5, :cond_2

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->textname:Landroid/widget/TextView;

    new-instance v1, Lcom/sec/android/app/samsungapps/viewholder/i;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/viewholder/i;-><init>(Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 121
    :cond_2
    return-void

    .line 53
    :cond_3
    iget v1, p0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->a:I

    if-ne v1, v5, :cond_6

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->textname:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->product_count:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->product_count:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 60
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->isLinkApp()Z

    move-result v0

    if-ne v0, v5, :cond_1

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->google_tencent_icon:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->_Context:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->CHINA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->checkCountry(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->google_tencent_icon:Landroid/widget/ImageView;

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 70
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->google_tencent_icon:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 68
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->google_tencent_icon:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 76
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->textname:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->product_count:Landroid/widget/TextView;

    if-eqz v1, :cond_7

    .line 78
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->product_count:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%d"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->product_count:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 82
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->isLinkApp()Z

    move-result v0

    if-ne v0, v5, :cond_1

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->google_tencent_icon:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->_Context:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->CHINA:Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->checkCountry(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->google_tencent_icon:Landroid/widget/ImageView;

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 92
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->google_tencent_icon:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 90
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/MainContentListViewHolder;->google_tencent_icon:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2
.end method
