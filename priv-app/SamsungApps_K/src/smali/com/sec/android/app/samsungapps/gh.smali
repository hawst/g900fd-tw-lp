.class final Lcom/sec/android/app/samsungapps/gh;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/widget/interfaces/IPurchasedWidgetListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)V
    .locals 0

    .prologue
    .line 451
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAllCheckClick(Z)V
    .locals 2

    .prologue
    .line 506
    if-eqz p1, :cond_0

    .line 507
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->d(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->getPurchasedList()Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->g(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->selectAll(Landroid/content/Context;)Z

    .line 512
    :goto_0
    if-eqz p1, :cond_1

    .line 513
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->deletionModeActionItemEnable(Z)V

    .line 517
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->c(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getSelectedCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->setUpPopupMenu(I)V

    .line 518
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->c(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->refreshArrayAdapter()V

    .line 519
    return-void

    .line 509
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->d(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->getPurchasedList()Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->deSelectAll()Z

    goto :goto_0

    .line 515
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->deletionModeActionItemEnable(Z)V

    goto :goto_1
.end method

.method public final onChangeActionBar(I)V
    .locals 1

    .prologue
    .line 501
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->changeActionBar(I)V

    .line 502
    return-void
.end method

.method public final onChangeActionItem(IZ)V
    .locals 2

    .prologue
    .line 524
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 525
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    const v1, 0xa000f

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->enableActionItem(IZ)V

    .line 526
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    const v1, 0xa0011

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->enableActionItem(IZ)V

    .line 530
    :goto_0
    return-void

    .line 528
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    const v1, 0xa0010

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->enableActionItem(IZ)V

    goto :goto_0
.end method

.method public final onClickTab(I)V
    .locals 2

    .prologue
    .line 490
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->c(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->isLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 491
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->c(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getAdapter()Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    move-result-object v0

    if-nez v0, :cond_1

    .line 497
    :cond_0
    :goto_0
    return-void

    .line 494
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->c(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->onWidgetViewState(I)V

    .line 495
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->c(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->updateWidget()V

    goto :goto_0
.end method

.method public final onDataLoadCompleted()V
    .locals 4

    .prologue
    const v3, 0xa0010

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 476
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->setEnableButton(IZ)V

    .line 477
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;

    move-result-object v0

    invoke-virtual {v0, v2, v1}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->setEnableButton(IZ)V

    .line 478
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->f(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->getCommandType()I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 479
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->c(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->isShowDeleteIcon()Z

    move-result v1

    invoke-virtual {v0, v3, v1}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->enableActionItem(IZ)V

    .line 480
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->c(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getAdapter()Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->isCheckDeletionMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 481
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->c(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getSelectedCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->setUpPopupMenu(I)V

    .line 486
    :cond_0
    :goto_0
    return-void

    .line 484
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v1}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->enableActionItem(IZ)V

    goto :goto_0
.end method

.method public final onDataLoadingMore()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x2

    .line 464
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->f(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->getCommandType()I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 465
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->setEnableButton(IZ)V

    .line 469
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->f(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->getCommandType()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 470
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    const v1, 0xa0010

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->c(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->isShowDeleteIcon()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->enableActionItem(IZ)V

    .line 472
    :cond_0
    return-void

    .line 467
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->setEnableButton(IZ)V

    goto :goto_0
.end method

.method public final refreshActionBar(Z)V
    .locals 2

    .prologue
    .line 548
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->f(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->f(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedWidgetHelper;->getCommandType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 549
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    const v1, 0xa0010

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->enableActionItem(IZ)V

    .line 551
    :cond_0
    return-void
.end method

.method public final setPurchasedMode(I)V
    .locals 1

    .prologue
    .line 455
    const/16 v0, 0x12

    if-ne p1, v0, :cond_0

    .line 456
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->displayDeleteScreen()V

    .line 460
    :goto_0
    return-void

    .line 458
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->displayNormalScreen()V

    goto :goto_0
.end method

.method public final setTopButtonLayoutVisible(Z)V
    .locals 1

    .prologue
    .line 543
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->setHideButtonLayout(Z)V

    .line 544
    return-void
.end method

.method public final updatePopupButtonText(II)V
    .locals 1

    .prologue
    .line 534
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    if-eqz v0, :cond_0

    .line 535
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->setUpPopupMenu(I)V

    .line 536
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gh;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-static {v0, p1}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->a(Lcom/sec/android/app/samsungapps/PurchasedListActivity;I)I

    .line 539
    :cond_0
    return-void
.end method
