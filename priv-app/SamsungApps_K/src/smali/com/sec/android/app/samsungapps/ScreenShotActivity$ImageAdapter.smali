.class public Lcom/sec/android/app/samsungapps/ScreenShotActivity$ImageAdapter;
.super Landroid/widget/BaseAdapter;
.source "ProGuard"


# instance fields
.field a:Ljava/util/ArrayList;

.field final synthetic b:Lcom/sec/android/app/samsungapps/ScreenShotActivity;

.field private c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/ScreenShotActivity;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 175
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity$ImageAdapter;->b:Lcom/sec/android/app/samsungapps/ScreenShotActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 172
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity$ImageAdapter;->a:Ljava/util/ArrayList;

    .line 176
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity$ImageAdapter;->c:Landroid/content/Context;

    .line 177
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity$ImageAdapter;->b:Lcom/sec/android/app/samsungapps/ScreenShotActivity;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;->getCount()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity$ImageAdapter;->b:Lcom/sec/android/app/samsungapps/ScreenShotActivity;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;->getURL(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 191
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v2, -0x1

    const/4 v3, 0x1

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity$ImageAdapter;->c:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 199
    if-eqz v0, :cond_0

    .line 200
    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    .line 205
    :cond_0
    if-nez p2, :cond_2

    .line 207
    new-instance v0, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity$ImageAdapter;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;-><init>(Landroid/content/Context;)V

    .line 210
    :goto_0
    check-cast v0, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    .line 212
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity$ImageAdapter;->b:Lcom/sec/android/app/samsungapps/ScreenShotActivity;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getNetAPI()Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setNetAPI(Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;)V

    .line 213
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity$ImageAdapter;->b:Lcom/sec/android/app/samsungapps/ScreenShotActivity;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;->getURL(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setURL(Ljava/lang/String;)V

    .line 214
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 215
    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setAdjustViewBounds(Z)V

    .line 216
    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setScrollbarFadingEnabled(Z)V

    .line 217
    new-instance v1, Landroid/widget/Gallery$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/Gallery$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 219
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity$ImageAdapter;->b:Lcom/sec/android/app/samsungapps/ScreenShotActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    .line 221
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/ScreenShotActivity$ImageAdapter;->b:Lcom/sec/android/app/samsungapps/ScreenShotActivity;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/ScreenShotActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;->getScreenShot()Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;->getOrientation(I)I

    move-result v2

    .line 223
    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    packed-switch v1, :pswitch_data_0

    .line 240
    :goto_1
    return-object v0

    .line 226
    :pswitch_0
    if-eq v2, v3, :cond_1

    .line 227
    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    goto :goto_1

    .line 233
    :pswitch_1
    if-ne v2, v3, :cond_1

    .line 234
    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    goto :goto_1

    .line 236
    :cond_1
    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    goto :goto_1

    :cond_2
    move-object v0, p2

    goto :goto_0

    .line 223
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
