.class final Lcom/sec/android/app/samsungapps/vlibrary2/permission/a;
.super Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionRequestor;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/a;->a:Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionRequestor;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected final onResult(ZLjava/util/HashMap;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/a;->a:Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;->isCanceled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/a;->a:Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;->onFinalResult(Z)V
    invoke-static {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;Z)V

    .line 46
    :cond_0
    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Ljava/util/HashMap;->size()I

    move-result v0

    if-eqz v0, :cond_2

    .line 48
    if-eqz p3, :cond_1

    .line 50
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/a;->a:Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;->_PermissionData:Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;->access$100(Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/PermissionData;->setPermissionInfoList(Ljava/util/HashMap;)V

    .line 52
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/a;->a:Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;->onFinalResult(Z)V
    invoke-static {v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;Z)V

    .line 63
    :goto_0
    return-void

    .line 56
    :cond_2
    if-eqz p4, :cond_3

    invoke-virtual {p4}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError;->getType()Lcom/sec/android/app/samsungapps/vlibrary/net/NetError$ErrorType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/net/NetError$ErrorType;->EXCEPTION:Lcom/sec/android/app/samsungapps/vlibrary/net/NetError$ErrorType;

    if-ne v0, v1, :cond_3

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/a;->a:Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;->onFinalResult(Z)V
    invoke-static {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;->access$300(Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;Z)V

    goto :goto_0

    .line 60
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/permission/a;->a:Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;->onFinalResult(Z)V
    invoke-static {v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;->access$400(Lcom/sec/android/app/samsungapps/vlibrary2/permission/GetPermissionInfoCommand;Z)V

    goto :goto_0
.end method
