.class final Lcom/sec/android/app/samsungapps/commands/ba;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/commands/DelayedNotiCommand$IDelayedNotiCommandInfo;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/commands/ba;->a:Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getMessage()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 86
    const v0, 0x7f08034f

    .line 88
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/commands/ba;->a:Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;->b(Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getDetail()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getOptBillInfoList()Lcom/sec/android/app/samsungapps/vlibrary/doc/DirectBillingOptList;

    move-result-object v1

    .line 89
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DirectBillingOptList;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 94
    invoke-virtual {v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DirectBillingOptList;->getAt(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/DirectBillingOperator;

    move-result-object v1

    .line 95
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DirectBillingOperator;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "SFR"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-ne v2, v6, :cond_1

    .line 97
    const v0, 0x7f08034c

    .line 105
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/commands/ba;->a:Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;->b(Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getBuyPrice()D

    move-result-wide v1

    .line 106
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/commands/ba;->a:Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;->b(Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getCurrencyUnit()Ljava/lang/String;

    move-result-object v3

    .line 107
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/commands/ba;->a:Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;->a(Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v4

    invoke-virtual {v4, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getFormattedPrice(DLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 109
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/commands/ba;->a:Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;->a(Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 110
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/commands/ba;->a:Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;->a(Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f08034d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v6, [Ljava/lang/Object;

    aput-object v1, v2, v5

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 112
    return-object v0

    .line 99
    :cond_1
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DirectBillingOperator;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "BOG"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v6, :cond_0

    .line 101
    const v0, 0x7f08034e

    goto :goto_0
.end method

.method public final getNegativeLabel()Ljava/lang/String;
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/ba;->a:Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;->a(Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f08023d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getPositiveLabel()Ljava/lang/String;
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/ba;->a:Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;->a(Lcom/sec/android/app/samsungapps/commands/DirectBillingCommandBuilder;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0802ef

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
