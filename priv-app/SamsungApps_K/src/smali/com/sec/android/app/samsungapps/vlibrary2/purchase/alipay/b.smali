.class final Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/b;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/b;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceiveResult(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V
    .locals 2

    .prologue
    .line 50
    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/b;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 52
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/b;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;->onFinalResult(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;Z)V

    .line 59
    :cond_0
    :goto_0
    return-void

    .line 55
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/b;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;->_TimeoutRetrier:Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;->access$100(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;)Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/TimeoutRetrier;->retry()Z

    move-result v0

    if-nez v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/b;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;->onFinalResult(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/alipay/AlipayCompleteCommand;Z)V

    goto :goto_0
.end method
