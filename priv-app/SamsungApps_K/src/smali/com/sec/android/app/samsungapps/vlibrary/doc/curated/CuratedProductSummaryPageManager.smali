.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/SimpleFSM;
.source "ProGuard"


# instance fields
.field private mContext:Landroid/content/Context;

.field mCuratedProductListInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;

.field mHandler:Landroid/os/Handler;

.field private mObserver:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$ICurateSummaryPageManagerObserver;

.field p:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/SimpleFSM;-><init>()V

    .line 35
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->mHandler:Landroid/os/Handler;

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->p:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    .line 37
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->mCuratedProductListInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;

    .line 208
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->sendRequest()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->loadCache()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;)Ljava/io/File;
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->createFile()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;Ljava/io/File;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->save(Ljava/io/File;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->onReceiveSuccess()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->onReceiveFailed()V

    return-void
.end method

.method private createFile()Ljava/io/File;
    .locals 2

    .prologue
    .line 218
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->mContext:Landroid/content/Context;

    const-string v1, "curatedsummaryoutput.xml"

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/filewrite/FileCreator;->internalStorage(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 219
    return-object v0
.end method

.method private fileExist()Z
    .locals 1

    .prologue
    .line 249
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->createFile()Ljava/io/File;

    move-result-object v0

    .line 250
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    return v0
.end method

.method private loadCache()V
    .locals 1

    .prologue
    .line 255
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->parseCachedXml()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->onCacheLoadSuccess()V

    .line 263
    :goto_0
    return-void

    .line 261
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->onCahceLoadFailed()V

    goto :goto_0
.end method

.method private loadXml(Ljava/io/File;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 297
    const-string v3, ""

    .line 298
    const/4 v1, 0x0

    .line 300
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 301
    :try_start_1
    new-instance v1, Ljava/io/ObjectInputStream;

    invoke-direct {v1, v2}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 302
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    .line 303
    :try_start_2
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    .line 311
    :goto_0
    if-eqz v2, :cond_0

    .line 314
    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 319
    :cond_0
    :goto_1
    return-object v0

    .line 304
    :catch_0
    move-exception v0

    move-object v2, v3

    :goto_2
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v0, v2

    move-object v2, v1

    .line 310
    goto :goto_0

    .line 307
    :catch_1
    move-exception v0

    move-object v2, v1

    :goto_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v3

    goto :goto_0

    .line 315
    :catch_2
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 307
    :catch_3
    move-exception v0

    goto :goto_3

    :catch_4
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    goto :goto_3

    .line 304
    :catch_5
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    goto :goto_2

    :catch_6
    move-exception v1

    move-object v4, v1

    move-object v1, v2

    move-object v2, v0

    move-object v0, v4

    goto :goto_2
.end method

.method private notifyFailed()V
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$ICurateSummaryPageManagerObserver;

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$ICurateSummaryPageManagerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$ICurateSummaryPageManagerObserver;->onLoadingFailed()V

    .line 166
    :cond_0
    return-void
.end method

.method private notifySuccess()V
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$ICurateSummaryPageManagerObserver;

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$ICurateSummaryPageManagerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$ICurateSummaryPageManagerObserver;->onLoadingCompleted()V

    .line 159
    :cond_0
    return-void
.end method

.method private onCacheLoadSuccess()V
    .locals 2

    .prologue
    .line 323
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/e;->a:[I

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->mState:Ljava/lang/Object;

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 334
    :goto_0
    return-void

    .line 326
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->notifySuccess()V

    .line 327
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/d;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 323
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch
.end method

.method private onCahceLoadFailed()V
    .locals 2

    .prologue
    .line 349
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/e;->a:[I

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->mState:Ljava/lang/Object;

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 353
    :goto_0
    return-void

    .line 352
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;->LOADING:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 349
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch
.end method

.method private onReceiveFailed()V
    .locals 2

    .prologue
    .line 177
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/e;->a:[I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 183
    :goto_0
    :pswitch_0
    return-void

    .line 182
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;->LOAD_FAILED:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 177
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private onReceiveSuccess()V
    .locals 2

    .prologue
    .line 193
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/e;->a:[I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 200
    :goto_0
    :pswitch_0
    return-void

    .line 199
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;->LOAD_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 193
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private parseCachedXml()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 268
    :try_start_0
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;-><init>()V

    .line 269
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLParser;

    invoke-direct {v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLParser;-><init>()V

    .line 271
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->createFile()Ljava/io/File;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->loadXml(Ljava/io/File;)Ljava/lang/String;

    move-result-object v3

    .line 272
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    .line 292
    :cond_0
    :goto_0
    return v0

    .line 276
    :cond_1
    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLParser;->parseXML(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/xml/result/ResponseParseResult;

    move-result-object v2

    .line 277
    if-eqz v2, :cond_0

    .line 278
    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListCreator;

    invoke-direct {v3, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListCreator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;)V

    .line 280
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->mCuratedProductListInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;

    .line 281
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator;

    invoke-direct {v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator$IListListMapContainer;)V

    .line 283
    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator;->onReceiveParsingResult(Lcom/sec/android/app/samsungapps/vlibrary/xml/result/IResponseParseResult;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    .line 284
    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0

    .line 291
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method private save(Ljava/io/File;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 224
    const/4 v2, 0x0

    .line 226
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 227
    :try_start_1
    new-instance v0, Ljava/io/ObjectOutputStream;

    invoke-direct {v0, v1}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 228
    invoke-virtual {v0, p2}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 229
    invoke-virtual {v0}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    .line 237
    :goto_0
    if-eqz v1, :cond_0

    .line 240
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 245
    :cond_0
    :goto_1
    return-void

    .line 230
    :catch_0
    move-exception v0

    move-object v1, v2

    :goto_2
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 233
    :catch_1
    move-exception v0

    move-object v1, v2

    :goto_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 241
    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 233
    :catch_3
    move-exception v0

    goto :goto_3

    .line 230
    :catch_4
    move-exception v0

    goto :goto_2
.end method

.method private sendRequest()V
    .locals 6

    .prologue
    .line 133
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;-><init>()V

    .line 134
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListCreator;

    invoke-direct {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListCreator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;)V

    .line 135
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v2

    const/4 v3, 0x1

    const/16 v4, 0xf

    new-instance v5, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/c;

    invoke-direct {v5, p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;)V

    invoke-virtual {v2, v3, v4, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->curatedProductSummary(IILcom/sec/android/app/samsungapps/vlibrary2/responseparser/ListMapContainerGenerator$IListListMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->p:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    .line 151
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->p:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 152
    return-void
.end method

.method private showLoading()V
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$ICurateSummaryPageManagerObserver;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$ICurateSummaryPageManagerObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$ICurateSummaryPageManagerObserver;->onLoading()V

    .line 173
    :cond_0
    return-void
.end method


# virtual methods
.method protected entry()V
    .locals 2

    .prologue
    .line 64
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/e;->a:[I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 105
    :goto_0
    :pswitch_0
    return-void

    .line 69
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->showLoading()V

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/a;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 79
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->notifyFailed()V

    goto :goto_0

    .line 82
    :pswitch_3
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->notifySuccess()V

    goto :goto_0

    .line 85
    :pswitch_4
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->fileExist()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;->CHACHE_LOADING:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 91
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;->LOADING:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 95
    :pswitch_5
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->showLoading()V

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/b;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 64
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method protected exit()V
    .locals 1

    .prologue
    .line 109
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/e;->a:[I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;->ordinal()I

    .line 112
    return-void
.end method

.method protected getIdleState()Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;
    .locals 1

    .prologue
    .line 124
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;->IDLE:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    return-object v0
.end method

.method protected bridge synthetic getIdleState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->getIdleState()Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    move-result-object v0

    return-object v0
.end method

.method public getInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->mCuratedProductListInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductListInfo;

    return-object v0
.end method

.method public load()V
    .locals 2

    .prologue
    .line 47
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/e;->a:[I

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 54
    :goto_0
    return-void

    .line 50
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;->CHECK_CACHE:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 53
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;->LOADING:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$State;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 47
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setObserver(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$ICurateSummaryPageManagerObserver;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->mContext:Landroid/content/Context;

    .line 42
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager;->mObserver:Lcom/sec/android/app/samsungapps/vlibrary/doc/curated/CuratedProductSummaryPageManager$ICurateSummaryPageManagerObserver;

    .line 43
    return-void
.end method
