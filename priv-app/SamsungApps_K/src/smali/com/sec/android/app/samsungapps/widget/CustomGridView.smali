.class public Lcom/sec/android/app/samsungapps/widget/CustomGridView;
.super Landroid/widget/GridView;
.source "ProGuard"


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/widget/CustomGridView$OnScrollMoreCheckListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0, p1}, Landroid/widget/GridView;-><init>(Landroid/content/Context;)V

    .line 13
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Landroid/widget/GridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/GridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    return-void
.end method


# virtual methods
.method public moreScroll()V
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/CustomGridView;->a:Lcom/sec/android/app/samsungapps/widget/CustomGridView$OnScrollMoreCheckListener;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/CustomGridView$OnScrollMoreCheckListener;->moreScroll()V

    .line 37
    return-void
.end method

.method public onMeasure(II)V
    .locals 2

    .prologue
    .line 28
    const v0, 0x1fffffff

    const/high16 v1, -0x80000000

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 29
    invoke-super {p0, p1, v0}, Landroid/widget/GridView;->onMeasure(II)V

    .line 31
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/CustomGridView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 32
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/CustomGridView;->getMeasuredHeight()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 33
    return-void
.end method

.method public setOnScrollMoreCheckListener(Lcom/sec/android/app/samsungapps/widget/CustomGridView$OnScrollMoreCheckListener;)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/CustomGridView;->a:Lcom/sec/android/app/samsungapps/widget/CustomGridView$OnScrollMoreCheckListener;

    .line 44
    return-void
.end method
