.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum CHECK_TIMER_OVER:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;

.field public static final enum DIRECT_EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;

.field public static final enum EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;

.field public static final enum NO_TIMED_OUT:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;

.field public static final enum REQUEST_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;

.field public static final enum REQUEST_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;

.field public static final enum TIMED_OUT:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 25
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;

    const-string v1, "EXECUTE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;

    const-string v1, "REQUEST_FAILED"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;->REQUEST_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;

    const-string v1, "REQUEST_SUCCESS"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;->REQUEST_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;

    const-string v1, "NO_TIMED_OUT"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;->NO_TIMED_OUT:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;

    const-string v1, "TIMED_OUT"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;->TIMED_OUT:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;

    const-string v1, "CHECK_TIMER_OVER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;->CHECK_TIMER_OVER:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;

    const-string v1, "DIRECT_EXECUTE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;->DIRECT_EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;

    .line 23
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;->REQUEST_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;->REQUEST_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;->NO_TIMED_OUT:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;->TIMED_OUT:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;->CHECK_TIMER_OVER:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;->DIRECT_EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerStateMachine$Event;

    return-object v0
.end method
