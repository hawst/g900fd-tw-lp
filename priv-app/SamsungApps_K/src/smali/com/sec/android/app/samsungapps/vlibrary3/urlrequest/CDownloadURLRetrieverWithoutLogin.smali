.class public Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverWithoutLogin;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetreiver;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieveResult;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field private c:Landroid/content/Context;

.field private d:Z

.field private e:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverWithoutLogin;->d:Z

    .line 63
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverWithoutLogin;->e:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

    .line 22
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverWithoutLogin;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 23
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverWithoutLogin;->c:Landroid/content/Context;

    .line 24
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 4

    .prologue
    .line 88
    const/4 v0, 0x0

    .line 90
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaInstalledInfo;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverWithoutLogin;->c:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverWithoutLogin;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaInstalledInfo;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    .line 92
    :try_start_0
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaInstalledInfo;->getVersionCodeString()Ljava/lang/String;
    :try_end_0
    .catch Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 96
    :goto_0
    return-object v0

    .line 93
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/deltadownload/DeltaException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverWithoutLogin;)V
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverWithoutLogin;->a:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieveResult;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverWithoutLogin;->a:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieveResult;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieveResult;->onURLSucceed()V

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverWithoutLogin;)V
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverWithoutLogin;->a:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieveResult;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverWithoutLogin;->a:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieveResult;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieveResult;->onURLFailed()V

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverWithoutLogin;)Z
    .locals 1

    .prologue
    .line 14
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverWithoutLogin;->d:Z

    return v0
.end method


# virtual methods
.method public execute()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 35
    iget-boolean v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverWithoutLogin;->d:Z

    if-nez v2, :cond_0

    .line 36
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverWithoutLogin;->d:Z

    .line 37
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverWithoutLogin;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getLoadType()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "2"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v0

    :goto_0
    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverWithoutLogin;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getOrderID()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    :goto_1
    if-eqz v0, :cond_3

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverWithoutLogin;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getOrderID()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverWithoutLogin;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getOrderItemSeq()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverWithoutLogin;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverWithoutLogin;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverWithoutLogin;->e:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

    new-instance v6, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/k;

    invoke-direct {v6, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/k;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverWithoutLogin;)V

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->download(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverWithoutLogin;->c:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 43
    :cond_0
    :goto_2
    return-void

    :cond_1
    move v2, v1

    .line 37
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    .line 40
    :cond_3
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverWithoutLogin;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverWithoutLogin;->e:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/j;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/j;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverWithoutLogin;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->downloadForRestore(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverWithoutLogin;->c:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    goto :goto_2
.end method

.method public getURLResult()Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverWithoutLogin;->e:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/URLResult;

    return-object v0
.end method

.method public setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieveResult;)V
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/CDownloadURLRetrieverWithoutLogin;->a:Lcom/sec/android/app/samsungapps/vlibrary3/urlrequest/DownloadURLRetrieveResult;

    .line 29
    return-void
.end method
