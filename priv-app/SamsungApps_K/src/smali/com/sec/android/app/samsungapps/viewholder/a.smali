.class final Lcom/sec/android/app/samsungapps/viewholder/a;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/viewholder/a;->a:Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const v6, 0x7f0c01b5

    const/16 v5, 0x9

    const/4 v2, 0x1

    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 62
    invoke-virtual {p2, v3}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    if-ne v0, v4, :cond_1

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/a;->a:Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;->_Context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "pen_hovering"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 64
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v5, :cond_0

    .line 65
    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 66
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/a;->a:Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;->_Context:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewholder/a;->a:Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-static {v1, p1, v2, v0, v4}, Lcom/sec/android/app/samsungapps/view/SamsungAppsHoveringView;->setBubbleHovering(Landroid/content/Context;Landroid/view/View;Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;Landroid/widget/ScrollView;I)V

    .line 79
    :cond_0
    :goto_0
    return v3

    .line 69
    :cond_1
    invoke-virtual {p2, v3}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 70
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v5, :cond_2

    .line 71
    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 72
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/a;->a:Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;->_Context:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewholder/a;->a:Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;

    iget-object v2, v2, Lcom/sec/android/app/samsungapps/viewholder/BigBannerContentHolder;->_Content:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-static {v1, p1, v2, v0, v4}, Lcom/sec/android/app/samsungapps/view/SamsungAppsHoveringView;->setBubbleHovering(Landroid/content/Context;Landroid/view/View;Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;Landroid/widget/ScrollView;I)V

    goto :goto_0

    .line 74
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    goto :goto_0
.end method
