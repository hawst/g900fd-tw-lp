.class public Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field protected _ContentDetailRelated:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;

.field protected _IGetDetailRelatedCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand$IGetDetailRelatedCommandData;

.field _receiver:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand$IGetDetailRelatedCommandData;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;->_ContentDetailRelated:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;

    .line 51
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/content/h;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/h;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;->_receiver:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    .line 21
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;->_IGetDetailRelatedCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand$IGetDetailRelatedCommandData;

    .line 22
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;Z)V
    .locals 0

    .prologue
    .line 16
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;Z)V
    .locals 0

    .prologue
    .line 16
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;Z)V
    .locals 0

    .prologue
    .line 16
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 4

    .prologue
    .line 26
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;->_IGetDetailRelatedCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand$IGetDetailRelatedCommandData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand$IGetDetailRelatedCommandData;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;->_ContentDetailRelated:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;

    .line 28
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;->_IGetDetailRelatedCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand$IGetDetailRelatedCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand$IGetDetailRelatedCommandData;->getContent()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    .line 29
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getLoadType()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getLoadType()Ljava/lang/String;

    move-result-object v1

    const-string v2, "2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 34
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;->_ContentDetailRelated:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;->_receiver:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->productDetailRelated(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 41
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 42
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 49
    :goto_1
    return-void

    .line 38
    :cond_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;->_ContentDetailRelated:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailRelated;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;->_receiver:Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->guidProductDetailRelated(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 44
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 47
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/content/GetDetailRelatedCommand;->onFinalResult(Z)V

    goto :goto_1
.end method
