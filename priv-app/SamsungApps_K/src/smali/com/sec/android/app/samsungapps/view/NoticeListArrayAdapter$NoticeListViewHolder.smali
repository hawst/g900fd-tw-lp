.class public Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter$NoticeListViewHolder;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;Landroid/content/Context;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 67
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter$NoticeListViewHolder;->a:Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    const v0, 0x7f0c01c5

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter$NoticeListViewHolder;->b:Landroid/widget/TextView;

    .line 70
    const v0, 0x7f0c01c6

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter$NoticeListViewHolder;->c:Landroid/widget/TextView;

    .line 71
    return-void
.end method


# virtual methods
.method public setNotice(Lcom/sec/android/app/samsungapps/vlibrary/doc/Notice;)V
    .locals 2

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter$NoticeListViewHolder;->b:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter$NoticeListViewHolder;->b:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notice;->getNoticeTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter$NoticeListViewHolder;->b:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter$NoticeListViewHolder;->a:Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;->a(Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notice;->getNoticeDate()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/util/AppsDateFormat;->getSystemDateItem(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 82
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter$NoticeListViewHolder;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    :cond_1
    return-void
.end method
