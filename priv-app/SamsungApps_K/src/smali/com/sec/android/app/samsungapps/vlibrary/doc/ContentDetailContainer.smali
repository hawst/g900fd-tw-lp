.class public abstract Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;
.super Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;
.source "ProGuard"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;
.implements Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;
.implements Lcom/sec/android/app/samsungapps/vlibrary/purchase/Purchasable;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContent;


# static fields
.field private static final ADP_PACKAGE_NAME:Ljava/lang/String; = "com.adpreference.adp"

.field private static final SPP_APK_PKGNAME:Ljava/lang/String; = "com.sec.spp.push"

.field private static final serialVersionUID:J = -0x6f0d750e29c68c01L


# instance fields
.field private transient _IContentDetailOverview:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

.field private transient _IContentDetailRelated:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailRelated;

.field private _ObserverList:Ljava/util/ArrayList;

.field private transient _Permission:Lcom/sec/android/app/samsungapps/vlibrary3/doc/Permission;

.field private _bAlreadyLoginAfterUpdated:Z

.field private _bAlreadyPurchasedIsChangedToTrueAfterGetDetail:Z

.field public _tag:I

.field public bAppType:Ljava/lang/String;

.field public bGearVersion:Ljava/lang/String;

.field public categoryID:Ljava/lang/String;

.field public categoryID2:Ljava/lang/String;

.field public categoryName2:Ljava/lang/String;

.field public categoryPath2:Ljava/lang/String;

.field downloadedSize:J

.field protected transient mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

.field mDownloadURI:Ljava/lang/String;

.field private transient mIContentDetailMain:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

.field private mNeedToUpdate:I

.field protected mStrGUID:Ljava/lang/String;

.field oldProgress:J

.field public restrictedAge:I

.field public srchClickURL:Ljava/lang/String;

.field public versionCode:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 76
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->mStrGUID:Ljava/lang/String;

    .line 46
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->_bAlreadyPurchasedIsChangedToTrueAfterGetDetail:Z

    .line 48
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->_bAlreadyLoginAfterUpdated:Z

    .line 286
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->mNeedToUpdate:I

    .line 429
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->oldProgress:J

    .line 450
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->downloadedSize:J

    .line 658
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->_ObserverList:Ljava/util/ArrayList;

    .line 78
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 71
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->mStrGUID:Ljava/lang/String;

    .line 46
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->_bAlreadyPurchasedIsChangedToTrueAfterGetDetail:Z

    .line 48
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->_bAlreadyLoginAfterUpdated:Z

    .line 286
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->mNeedToUpdate:I

    .line 429
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->oldProgress:J

    .line 450
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->downloadedSize:J

    .line 658
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->_ObserverList:Ljava/util/ArrayList;

    .line 72
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-static {p1, v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;->readClass(Landroid/os/Parcel;Ljava/lang/Class;Ljava/lang/Object;)Z

    .line 73
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 81
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->mStrGUID:Ljava/lang/String;

    .line 46
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->_bAlreadyPurchasedIsChangedToTrueAfterGetDetail:Z

    .line 48
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->_bAlreadyLoginAfterUpdated:Z

    .line 286
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->mNeedToUpdate:I

    .line 429
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->oldProgress:J

    .line 450
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->downloadedSize:J

    .line 658
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->_ObserverList:Ljava/util/ArrayList;

    .line 82
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    const/4 v1, 0x1

    invoke-static {p1, v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->mappingClass(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 83
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->_ObserverList:Ljava/util/ArrayList;

    return-object v0
.end method

.method private notifyDetailUpdate()V
    .locals 2

    .prologue
    .line 105
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->_Handler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/n;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/n;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 115
    return-void
.end method

.method private stringCompare(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 403
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 405
    if-nez p2, :cond_1

    .line 410
    :cond_0
    :goto_0
    return v0

    .line 408
    :cond_1
    invoke-virtual {p1, p2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public addObserver(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer$IContentObserver;)V
    .locals 1

    .prologue
    .line 662
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->_ObserverList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 663
    return-void
.end method

.method public compareByGUID(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 416
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->stringCompare(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public compareByProductID(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 422
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->stringCompare(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public deleteFile(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 383
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterInfo;Landroid/content/Context;)V

    .line 384
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->delete()Z

    move-result v0

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    return v0
.end method

.method public downloadEnded(Z)V
    .locals 0

    .prologue
    .line 449
    return-void
.end method

.method public getAbsPath(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 472
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterInfo;Landroid/content/Context;)V

    .line 473
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->getAbsPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getChinaPrepaidCardInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;
    .locals 1

    .prologue
    .line 344
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 345
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getChinaPrepaidCardInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;

    move-result-object v0

    .line 347
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getConvertedFileName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 200
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    if-eqz v0, :cond_1

    .line 202
    const-string v0, ".apk"

    .line 203
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->isWGTOnly()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 205
    const-string v0, ".wgt"

    .line 207
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getConvertedFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 209
    :goto_0
    return-object v0

    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method public getCurrencyUnit()Ljava/lang/String;
    .locals 1

    .prologue
    .line 353
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 354
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->currencyUnit:Ljava/lang/String;

    .line 356
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getCyberCashInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;
    .locals 1

    .prologue
    .line 362
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 363
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getCyberCashInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/CyberCashInfo;

    move-result-object v0

    .line 365
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    return-object v0
.end method

.method public getDetailOverview()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;
    .locals 1

    .prologue
    .line 557
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->_IContentDetailOverview:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    return-object v0
.end method

.method public getDetailRelated()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailRelated;
    .locals 1

    .prologue
    .line 552
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->_IContentDetailRelated:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailRelated;

    return-object v0
.end method

.method public getDownloadURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->mDownloadURI:Ljava/lang/String;

    return-object v0
.end method

.method public getDownloadableContent()Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;
    .locals 0

    .prologue
    .line 466
    return-object p0
.end method

.method public getDownloadedSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;
    .locals 3

    .prologue
    .line 489
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    iget-wide v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->downloadedSize:J

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;-><init>(J)V

    return-object v0
.end method

.method public getGUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->GUID:Ljava/lang/String;

    .line 150
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->mStrGUID:Ljava/lang/String;

    goto :goto_0
.end method

.method public getLoadType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 227
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 228
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getLoadType()Ljava/lang/String;

    move-result-object v0

    .line 230
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "2"

    goto :goto_0
.end method

.method public getNeedToUpdate()I
    .locals 1

    .prologue
    .line 294
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->mNeedToUpdate:I

    return v0
.end method

.method public getNormalPrice()D
    .locals 2

    .prologue
    .line 318
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-nez v0, :cond_0

    .line 319
    const-wide/16 v0, 0x0

    .line 321
    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getNormalPrice()D

    move-result-wide v0

    goto :goto_0
.end method

.method public getOrderID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    if-nez v0, :cond_0

    .line 162
    const/4 v0, 0x0

    .line 164
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getOrderID()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getOrderItemSeq()Ljava/lang/String;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    if-nez v0, :cond_0

    .line 169
    const/4 v0, 0x0

    .line 171
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getOrderItemSeq()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getPaymentPrice()D
    .locals 2

    .prologue
    .line 309
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-nez v0, :cond_0

    .line 310
    const-wide/16 v0, 0x0

    .line 312
    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getPaymentPrice()D

    move-result-wide v0

    goto :goto_0
.end method

.method public getPermission()Lcom/sec/android/app/samsungapps/vlibrary3/doc/Permission;
    .locals 1

    .prologue
    .line 655
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->_Permission:Lcom/sec/android/app/samsungapps/vlibrary3/doc/Permission;

    return-object v0
.end method

.method public abstract getProductID()Ljava/lang/String;
.end method

.method public getProductImageURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 327
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 328
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->productImgUrl:Ljava/lang/String;

    .line 330
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getProductName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 236
    const-string v0, ""

    .line 237
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 239
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->productName:Ljava/lang/String;

    .line 241
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getPurchased()Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 243
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getPurchased()Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;->productName:Ljava/lang/String;

    .line 246
    :cond_1
    if-eqz v0, :cond_2

    const-string v1, "[S_I_N_A]"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 248
    const-string v1, "[S_I_N_A]"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 250
    :cond_2
    if-eqz v0, :cond_3

    const-string v1, "_terminated"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 252
    const-string v1, "_terminated"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 254
    :cond_3
    if-nez v0, :cond_4

    .line 256
    const-string v0, ""

    .line 258
    :cond_4
    return-object v0
.end method

.method public getProgressPercentage()I
    .locals 6

    .prologue
    .line 479
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getRealContentSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->getSize()J

    move-result-wide v0

    .line 480
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 481
    const/4 v0, 0x0

    .line 483
    :goto_0
    return v0

    :cond_0
    iget-wide v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->downloadedSize:J

    const-wide/16 v4, 0x64

    mul-long/2addr v2, v4

    div-long v0, v2, v0

    long-to-int v0, v0

    goto :goto_0
.end method

.method public getPurchased()Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    return-object v0
.end method

.method public getRealContentSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;
    .locals 3

    .prologue
    .line 191
    const-wide/16 v0, 0x0

    .line 192
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    if-eqz v2, :cond_0

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getRealContentSize()J

    move-result-wide v0

    .line 196
    :cond_0
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    invoke-direct {v2, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;-><init>(J)V

    return-object v2
.end method

.method public getRestrictedAge()I
    .locals 1

    .prologue
    .line 562
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->restrictedAge:I

    return v0
.end method

.method public getSaveFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 427
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getConvertedFileName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->isProductIDValid()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v0

    .line 130
    :goto_0
    return-object v0

    .line 127
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->isGUIDValid()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 128
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 130
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getVGUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 633
    const-string v0, ""

    return-object v0
.end method

.method public getVaverageRating()F
    .locals 1

    .prologue
    .line 623
    const/4 v0, 0x0

    return v0
.end method

.method public getVcategoryID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 568
    const-string v0, ""

    return-object v0
.end method

.method public getVcategoryID2()Ljava/lang/String;
    .locals 1

    .prologue
    .line 578
    const-string v0, ""

    return-object v0
.end method

.method public getVcategoryName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 573
    const-string v0, ""

    return-object v0
.end method

.method public getVcategoryName2()Ljava/lang/String;
    .locals 1

    .prologue
    .line 583
    const-string v0, ""

    return-object v0
.end method

.method public getVcontentType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 628
    const-string v0, ""

    return-object v0
.end method

.method public getVcurrencyUnit()Ljava/lang/String;
    .locals 1

    .prologue
    .line 603
    const-string v0, ""

    return-object v0
.end method

.method public getVdiscountFlag()Z
    .locals 1

    .prologue
    .line 618
    const/4 v0, 0x0

    return v0
.end method

.method public getVdiscountPrice()D
    .locals 2

    .prologue
    .line 613
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getVersionCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 687
    const/4 v0, 0x0

    return-object v0
.end method

.method public getVprice()D
    .locals 2

    .prologue
    .line 608
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getVproductID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 588
    const-string v0, ""

    return-object v0
.end method

.method public getVproductImgUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 598
    const-string v0, ""

    return-object v0
.end method

.method public getVproductName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 593
    const-string v0, ""

    return-object v0
.end method

.method public hasOrderID()Z
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    if-nez v0, :cond_0

    .line 155
    const/4 v0, 0x0

    .line 157
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->hasOrderID()Z

    move-result v0

    goto :goto_0
.end method

.method public isAD()Z
    .locals 1

    .prologue
    .line 398
    const-string v0, "com.adpreference.adp"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->compareByGUID(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isAlreadyLoginAfterUpdated()Z
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->_bAlreadyLoginAfterUpdated:Z

    return v0
.end method

.method public isAlreadyPurchasedChangedToTrueFromFalse()Z
    .locals 1

    .prologue
    .line 222
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->_bAlreadyPurchasedIsChangedToTrueAfterGetDetail:Z

    return v0
.end method

.method public isCompletelyDownloaded(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 459
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/download/IFileWriterInfo;Landroid/content/Context;)V

    .line 460
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/FileWriter;->isComplete()Z

    move-result v0

    return v0
.end method

.method public isFreeContent()Z
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    if-nez v0, :cond_0

    .line 215
    const/4 v0, 0x0

    .line 217
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isFreeContent()Z

    move-result v0

    goto :goto_0
.end method

.method public isGUIDValid()Z
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->mStrGUID:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->mStrGUID:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final isInstalled(Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;)Z
    .locals 1

    .prologue
    .line 511
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isKNOXApp()Z
    .locals 1

    .prologue
    .line 645
    const/4 v0, 0x0

    return v0
.end method

.method public isLinkApp()Z
    .locals 1

    .prologue
    .line 639
    const/4 v0, 0x0

    return v0
.end method

.method public isOldVersionInstalled(Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;)Z
    .locals 1

    .prologue
    .line 535
    const/4 v0, 0x0

    return v0
.end method

.method public final isPackageInstalled(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 499
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 500
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isProductIDValid()Z
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x1

    return v0
.end method

.method public isSINAContent()Z
    .locals 1

    .prologue
    .line 530
    const/4 v0, 0x0

    return v0
.end method

.method public isSPP()Z
    .locals 1

    .prologue
    .line 393
    const-string v0, "com.sec.spp.push"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->compareByGUID(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected isUpdatable(Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;)Z
    .locals 1

    .prologue
    .line 521
    const/4 v0, 0x0

    return v0
.end method

.method public isWGTInAPK()Z
    .locals 1

    .prologue
    .line 681
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/gear2/Gear2AppType;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/gear2/Gear2AppType;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    .line 682
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/gear2/Gear2AppType;->isWGTInApkContent()Z

    move-result v0

    return v0
.end method

.method public isWGTOnly()Z
    .locals 1

    .prologue
    .line 676
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/gear2/Gear2AppType;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/gear2/Gear2AppType;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    .line 677
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/gear2/Gear2AppType;->isWGTContent()Z

    move-result v0

    return v0
.end method

.method public purchaseSuccess()V
    .locals 0

    .prologue
    .line 278
    return-void
.end method

.method public removeObserver(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer$IContentObserver;)V
    .locals 1

    .prologue
    .line 667
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->_ObserverList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 668
    return-void
.end method

.method public setAlreadyLoginAfterUpdated(Z)V
    .locals 0

    .prologue
    .line 57
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->_bAlreadyLoginAfterUpdated:Z

    .line 58
    return-void
.end method

.method public setContentSize(J)V
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    iput-wide p1, v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->contentsSize:J

    .line 185
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    iput-wide p1, v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->realContentsSize:J

    .line 187
    :cond_0
    return-void
.end method

.method public setDetailMain(Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;)V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->release()V

    .line 100
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    .line 101
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->notifyDetailUpdate()V

    .line 102
    return-void
.end method

.method public setDetailOverview(Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;)V
    .locals 0

    .prologue
    .line 543
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->_IContentDetailOverview:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    .line 544
    return-void
.end method

.method public setDetailRelated(Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailRelated;)V
    .locals 0

    .prologue
    .line 539
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->_IContentDetailRelated:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailRelated;

    .line 540
    return-void
.end method

.method public setDownloadURI(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 372
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->mDownloadURI:Ljava/lang/String;

    .line 373
    return-void
.end method

.method public setGUID(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->mStrGUID:Ljava/lang/String;

    .line 140
    return-void
.end method

.method public setIContentDetailMain(Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;)V
    .locals 0

    .prologue
    .line 547
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->mIContentDetailMain:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;

    .line 548
    return-void
.end method

.method public setNeedToUpdate(I)V
    .locals 0

    .prologue
    .line 290
    iput p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->mNeedToUpdate:I

    .line 291
    return-void
.end method

.method public setOrderID(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->mContentDetail:Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    iput-object p1, v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->orderID:Ljava/lang/String;

    .line 179
    :cond_0
    return-void
.end method

.method public setOrderIDSeq(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 336
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 337
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    iput-object p1, v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->orderItemSeq:Ljava/lang/String;

    .line 339
    :cond_0
    return-void
.end method

.method public setPermission(Lcom/sec/android/app/samsungapps/vlibrary3/doc/Permission;)V
    .locals 0

    .prologue
    .line 650
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->_Permission:Lcom/sec/android/app/samsungapps/vlibrary3/doc/Permission;

    .line 651
    return-void
.end method

.method public updateDownloadedSize(J)V
    .locals 4

    .prologue
    .line 433
    iput-wide p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->downloadedSize:J

    .line 438
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProgressPercentage()I

    move-result v0

    int-to-long v0, v0

    .line 439
    iget-wide v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->oldProgress:J

    cmp-long v2, v2, v0

    if-eqz v2, :cond_0

    .line 441
    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->oldProgress:J

    .line 443
    :cond_0
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 62
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-static {p1, v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;->writeClass(Landroid/os/Parcel;Ljava/lang/Class;Ljava/lang/Object;)Z

    .line 63
    return-void
.end method
