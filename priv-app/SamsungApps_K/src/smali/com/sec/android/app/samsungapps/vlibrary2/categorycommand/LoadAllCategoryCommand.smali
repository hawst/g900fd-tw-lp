.class public Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/LoadAllCategoryCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _CategoryContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

.field private _ICategoryCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/ICategoryCommandBuilder;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/ICategoryCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 17
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/LoadAllCategoryCommand;->_CategoryContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

    .line 18
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/LoadAllCategoryCommand;->_ICategoryCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/ICategoryCommandBuilder;

    .line 19
    return-void
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 3

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/LoadAllCategoryCommand;->_ICategoryCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/ICategoryCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/ICategoryCommandBuilder;->loadRootCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/LoadAllCategoryCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/a;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/LoadAllCategoryCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 29
    return-void
.end method

.method protected onAfterRootCategoryLoaded(Z)V
    .locals 3

    .prologue
    .line 33
    if-nez p1, :cond_0

    .line 35
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/LoadAllCategoryCommand;->onFinalResult(Z)V

    .line 62
    :goto_0
    return-void

    .line 39
    :cond_0
    const/4 v0, 0x0

    .line 43
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/LoadAllCategoryCommand;->_CategoryContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->getRoot()Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->findCategoryNeedingChild()Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 50
    :goto_1
    if-nez v0, :cond_1

    .line 52
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/LoadAllCategoryCommand;->onFinalResult(Z)V

    goto :goto_0

    .line 47
    :catch_0
    move-exception v1

    const-string v1, "_CategoryContainer is null Or _CategoryContainer.getRoot() returns null"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    goto :goto_1

    .line 55
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/LoadAllCategoryCommand;->_ICategoryCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/ICategoryCommandBuilder;

    invoke-interface {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/ICategoryCommandBuilder;->loadSubCatagory(Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/LoadAllCategoryCommand;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/b;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/categorycommand/LoadAllCategoryCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0
.end method
