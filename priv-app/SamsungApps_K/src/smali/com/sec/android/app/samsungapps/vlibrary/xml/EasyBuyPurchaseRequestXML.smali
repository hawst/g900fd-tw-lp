.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/EasyBuyPurchaseRequestXML;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
.source "ProGuard"


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardParam;ZI)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 10
    const-string v2, "EasybuyPurchase"

    const-string v3, "3010"

    move-object v0, p0

    move-object v1, p1

    move v4, p4

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 12
    const-string v0, "productID"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardParam;->getProduct()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/EasyBuyPurchaseRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isIran()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 16
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 17
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getIMSI()Ljava/lang/String;

    move-result-object v0

    .line 18
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_3

    .line 20
    :cond_0
    const-string v0, "ipAddress"

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getIMEI()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/EasyBuyPurchaseRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    :cond_1
    :goto_0
    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardParam;->getAppliedCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 30
    const-string v0, "couponIssuedSEQ"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardParam;->getAppliedCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->getCouponSeq()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/EasyBuyPurchaseRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    :goto_1
    const-string v0, "imei"

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getIMEI()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/xml/EasyBuyPurchaseRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 38
    const-string v0, "guid"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardParam;->getProduct()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;->getGUID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/EasyBuyPurchaseRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardParam;->getSellingPrice()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_5

    .line 43
    const-string v0, "paymentAmountPrice"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/EasyBuyPurchaseRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    :goto_2
    const-string v1, "testPurchaseYn"

    if-eqz p3, :cond_6

    const-string v0, "Y"

    :goto_3
    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/EasyBuyPurchaseRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardParam;->hasCVC()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 54
    const-string v0, "cvc"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardParam;->getCVC()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/EasyBuyPurchaseRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    :goto_4
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/EasyBuyPurchaseRequestXML;->addUserID()V

    .line 61
    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardParam;->getAppliedGiftCard()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 63
    const-string v0, "giftCardCode"

    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardParam;->getAppliedGiftCard()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;->getGiftCardCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/EasyBuyPurchaseRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    :cond_2
    return-void

    .line 24
    :cond_3
    const-string v1, "ipAddress"

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/EasyBuyPurchaseRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 34
    :cond_4
    const-string v0, "couponIssuedSEQ"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/EasyBuyPurchaseRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 47
    :cond_5
    invoke-interface {p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/globalcreditcard/IGCreditCardParam;->getSellingPrice()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v0

    .line 48
    const-string v1, "paymentAmountPrice"

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/EasyBuyPurchaseRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 50
    :cond_6
    const-string v0, "N"

    goto :goto_3

    .line 58
    :cond_7
    const-string v0, "cvc"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/EasyBuyPurchaseRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4
.end method

.method private addUserID()V
    .locals 3

    .prologue
    .line 69
    const-string v0, "userID"

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/EasyBuyPurchaseRequestXML;->getUserID()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/EasyBuyPurchaseRequestXML;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 70
    return-void
.end method

.method private getUserID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/EasyBuyPurchaseRequestXML;->isLogedIn()Z

    move-result v0

    if-nez v0, :cond_0

    .line 76
    const-string v0, ""

    .line 78
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;->userID:Ljava/lang/String;

    goto :goto_0
.end method

.method private isLogedIn()Z
    .locals 1

    .prologue
    .line 83
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getLoginInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
