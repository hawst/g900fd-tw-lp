.class public Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;
.source "ProGuard"


# static fields
.field private static a:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;-><init>()V

    .line 22
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine;

    if-nez v0, :cond_0

    .line 36
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine;

    .line 38
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine;

    return-object v0
.end method


# virtual methods
.method protected entry(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 3

    .prologue
    .line 104
    const-string v0, "UnifiedBillingStateMachine"

    const-string v1, "entry"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 105
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/d;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 124
    :goto_0
    :pswitch_0
    return-void

    .line 109
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;->SEND_INIT_REQUEST:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 112
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;->START_ACTIVIY:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 115
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;->SEND_COMPLETE:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 118
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;->SIG_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 121
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;->SIG_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 105
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;)Z
    .locals 3

    .prologue
    .line 44
    const-string v0, "UnifiedBillingStateMachine"

    const-string v1, "execute"

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine;->dump(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 45
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/d;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 99
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 47
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/d;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 50
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$State;->REQUESTING:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 55
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/d;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 58
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$State;->START_ACITIVTY:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 61
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$State;->FAILURE:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 66
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/d;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    goto :goto_0

    .line 69
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$State;->UP_PAYMENT_WAITING:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 74
    :pswitch_7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/d;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_4

    goto :goto_0

    .line 77
    :pswitch_8
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$State;->FAILURE:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 80
    :pswitch_9
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$State;->SENDING_COMPLETE:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 83
    :pswitch_a
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$State;->FAILURE:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 88
    :pswitch_b
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/d;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_5

    goto :goto_0

    .line 91
    :pswitch_c
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 94
    :pswitch_d
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$State;->FAILURE:Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 45
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_5
        :pswitch_7
        :pswitch_b
    .end packed-switch

    .line 47
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch

    .line 55
    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 66
    :pswitch_data_3
    .packed-switch 0x4
        :pswitch_6
    .end packed-switch

    .line 74
    :pswitch_data_4
    .packed-switch 0x5
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch

    .line 88
    :pswitch_data_5
    .packed-switch 0x8
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method public bridge synthetic execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 9
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UnifiedBillingStateMachine$Event;)Z

    move-result v0

    return v0
.end method

.method protected exit(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 0

    .prologue
    .line 128
    return-void
.end method
