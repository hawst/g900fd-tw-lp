.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final ORIENTATION_LANDSCAPE:I = 0x1

.field public static final ORIENTATION_PORTRAIT:I = 0x2

.field public static final ORIENTATION_UNKNOWN:I = 0x0

.field private static final serialVersionUID:J = 0x6d3a55b300e05e39L


# instance fields
.field private bCalculated:Z

.field public imgCount:I

.field resolutionArray:Ljava/util/ArrayList;

.field public screenShotImgURL:Ljava/lang/String;

.field public screenShotResolution:Ljava/lang/String;

.field url:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;->imgCount:I

    .line 31
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;->bCalculated:Z

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;->url:Ljava/util/ArrayList;

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;->resolutionArray:Ljava/util/ArrayList;

    .line 38
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;->imgCount:I

    .line 31
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;->bCalculated:Z

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;->url:Ljava/util/ArrayList;

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;->resolutionArray:Ljava/util/ArrayList;

    .line 42
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;->screenShotImgURL:Ljava/lang/String;

    .line 43
    iput p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;->imgCount:I

    .line 44
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;->screenShotResolution:Ljava/lang/String;

    .line 46
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;->calculate()V

    .line 47
    return-void
.end method

.method private decideOrientation()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;->screenShotResolution:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 133
    :cond_0
    return-void

    .line 105
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;->screenShotResolution:Ljava/lang/String;

    const-string v2, "\\|"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    move v0, v1

    .line 107
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 109
    aget-object v3, v2, v0

    const-string v4, "x"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 110
    array-length v4, v3

    if-ge v4, v7, :cond_2

    .line 112
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;->resolutionArray:Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 107
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 116
    :cond_2
    const/4 v4, 0x0

    :try_start_0
    aget-object v4, v3, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 121
    const/4 v5, 0x1

    aget-object v3, v3, v5

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 127
    if-le v4, v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;->resolutionArray:Ljava/util/ArrayList;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 123
    :catch_0
    move-exception v3

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;->resolutionArray:Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 128
    :cond_3
    if-le v3, v4, :cond_4

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;->resolutionArray:Ljava/util/ArrayList;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 129
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;->resolutionArray:Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method


# virtual methods
.method public calculate()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 56
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;->bCalculated:Z

    if-eqz v0, :cond_1

    .line 78
    :cond_0
    return-void

    .line 60
    :cond_1
    iput-boolean v8, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;->bCalculated:Z

    .line 62
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;->decideOrientation()V

    move v0, v1

    .line 64
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;->getImgCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 66
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;->screenShotImgURL:Ljava/lang/String;

    const-string v3, "SHOT1"

    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v5, "SHOT%d"

    new-array v6, v8, [Ljava/lang/Object;

    add-int/lit8 v7, v0, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v1

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 67
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;->getOrientation(I)I

    move-result v3

    if-ne v3, v8, :cond_2

    .line 69
    const-string v3, "_P"

    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    .line 70
    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    .line 72
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v3, v3, 0x2

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 75
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;->url:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 64
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getImageURL(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;->url:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 89
    const/4 v0, 0x0

    .line 91
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;->url:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getImgCount()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;->imgCount:I

    return v0
.end method

.method public getOrientation(I)I
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;->resolutionArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 138
    const/4 v0, 0x0

    .line 140
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;->resolutionArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method
