.class public Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;
.super Lcom/sec/android/app/samsungapps/widget/CommonWidget;
.source "ProGuard"


# static fields
.field public static final WIDGET_PRODUCTLIST1:I = 0x0

.field public static final WIDGET_PRODUCTLIST2:I = 0x1

.field public static final WIDGET_TAG:I = 0x2


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

.field private b:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

.field private d:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;

.field private e:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field private f:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;

.field private g:Lcom/sec/android/app/samsungapps/widget/interfaces/IRetryContentDetail;

.field private h:Z

.field private i:I

.field private j:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;)V

    .line 29
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->h:Z

    .line 30
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->i:I

    .line 31
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->j:I

    .line 35
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->a(Landroid/content/Context;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 39
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->h:Z

    .line 30
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->i:I

    .line 31
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->j:I

    .line 40
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->a(Landroid/content/Context;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 44
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->h:Z

    .line 30
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->i:I

    .line 31
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->j:I

    .line 45
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->a(Landroid/content/Context;)V

    .line 46
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->mContext:Landroid/content/Context;

    .line 50
    const v0, 0x7f040039

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->initView(Landroid/content/Context;I)V

    .line 52
    const v0, 0x7f0c010a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->a:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->a:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->setListType(I)V

    .line 54
    const v0, 0x7f0c010b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->b:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->b:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->setListType(I)V

    .line 57
    const v0, 0x7f0c010c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->d:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;

    .line 58
    return-void
.end method


# virtual methods
.method public loadWidget()V
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->g:Lcom/sec/android/app/samsungapps/widget/interfaces/IRetryContentDetail;

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->g:Lcom/sec/android/app/samsungapps/widget/interfaces/IRetryContentDetail;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/widget/interfaces/IRetryContentDetail;->onClickRetryBtn(I)V

    .line 185
    :cond_0
    return-void
.end method

.method public loadWidget(I)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 139
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->a:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->b:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->d:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;

    if-nez v0, :cond_1

    .line 178
    :cond_0
    :goto_0
    return-void

    .line 143
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 175
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->isLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailRelated()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailRelated;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 176
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->updateWidget()V

    goto :goto_0

    .line 145
    :pswitch_0
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->h:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->f:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->f:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 146
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->a:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->setWidgetState(I)V

    .line 147
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->a:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->loadWidget()V

    goto :goto_1

    .line 149
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->a:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->setVisibility(I)V

    goto :goto_1

    .line 154
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->f:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->f:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->b:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->setWidgetState(I)V

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->b:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->loadWidget()V

    goto :goto_1

    .line 158
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->b:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->setVisibility(I)V

    goto :goto_1

    .line 163
    :pswitch_2
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->h:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailRelated()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailRelated;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailRelated()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailRelated;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailRelated;->getVsellerTag()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailRelated()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailRelated;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailRelated;->getVsellerTag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_4

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->d:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->setWidgetState(I)V

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->d:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->loadWidget()V

    goto/16 :goto_1

    .line 170
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->d:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->setVisibility(I)V

    goto/16 :goto_1

    .line 143
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 219
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 221
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->a:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->b:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->d:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 224
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 226
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->i:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->setHeightRelatedLayout(I)V

    .line 233
    :cond_0
    :goto_0
    return-void

    .line 230
    :cond_1
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->j:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->setHeightRelatedLayout(I)V

    goto :goto_0
.end method

.method public refreshWidget()V
    .locals 0

    .prologue
    .line 204
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->a:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->a:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->release()V

    .line 119
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->a:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->b:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    if-eqz v0, :cond_1

    .line 122
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->b:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->release()V

    .line 123
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->b:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    .line 125
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->d:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;

    if-eqz v0, :cond_2

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->d:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->release()V

    .line 127
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->d:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;

    .line 130
    :cond_2
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 131
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->f:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;

    .line 132
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->g:Lcom/sec/android/app/samsungapps/widget/interfaces/IRetryContentDetail;

    .line 134
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->release()V

    .line 135
    return-void
.end method

.method public setHeightRelatedLayout(I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 236
    const/4 v0, 0x2

    new-array v1, v0, [I

    .line 237
    const v0, 0x7f0c010d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 238
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    .line 239
    const/high16 v3, 0x41a00000    # 20.0f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 241
    if-gtz p1, :cond_0

    .line 242
    if-eqz v0, :cond_1

    .line 245
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->getLocationOnScreen([I)V

    .line 246
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    aget v1, v1, v4

    sub-int p1, v3, v1

    .line 247
    if-lez p1, :cond_1

    .line 248
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v4, :cond_3

    .line 249
    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->j:I

    if-lez v1, :cond_2

    .line 250
    sub-int/2addr p1, v2

    .line 251
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->i:I

    .line 258
    :cond_0
    :goto_0
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setHeight(I)V

    .line 262
    :cond_1
    return-void

    .line 253
    :cond_2
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->i:I

    goto :goto_0

    .line 256
    :cond_3
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->j:I

    goto :goto_0
.end method

.method public setIRetryContentDetail(Lcom/sec/android/app/samsungapps/widget/interfaces/IRetryContentDetail;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->g:Lcom/sec/android/app/samsungapps/widget/interfaces/IRetryContentDetail;

    .line 62
    return-void
.end method

.method public setNeedLoad(Z)V
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->a:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->b:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->d:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->a:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->setNeedLoad(Z)V

    .line 210
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->b:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->setNeedLoad(Z)V

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->d:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->setNeedLoad(Z)V

    .line 213
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->setNeedLoad(Z)V

    .line 214
    return-void
.end method

.method public setWidgetData(ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v1, 0x0

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->a:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->b:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->d:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;

    if-nez v0, :cond_1

    .line 107
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, p2

    .line 69
    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-object v0, p3

    .line 70
    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->f:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->e:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isLinkApp()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->h:Z

    .line 78
    packed-switch p1, :pswitch_data_0

    .line 95
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->a:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->isLoaded()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->b:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->isLoaded()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->d:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->isLoaded()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 97
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->setWidgetState(I)V

    goto :goto_0

    .line 80
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->a:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    invoke-virtual {v0, p2, p3}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->setWidgetData(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->a:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->setWidgetState(I)V

    goto :goto_1

    .line 85
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->b:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    invoke-virtual {v0, p2, p3}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->setWidgetData(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->b:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->setWidgetState(I)V

    goto :goto_1

    .line 90
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->d:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->setWidgetData(Ljava/lang/Object;)V

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->d:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->setWidgetState(I)V

    goto :goto_1

    .line 98
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->a:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->getWidgetState()I

    move-result v0

    if-eq v0, v3, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->b:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->getWidgetState()I

    move-result v0

    if-eq v0, v3, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->d:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->getWidgetState()I

    move-result v0

    if-ne v0, v3, :cond_4

    .line 101
    :cond_3
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->setWidgetState(I)V

    goto :goto_0

    .line 102
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->a:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->getWidgetState()I

    move-result v0

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->b:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->getWidgetState()I

    move-result v0

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->d:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->getWidgetState()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 105
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->setWidgetState(I)V

    goto/16 :goto_0

    .line 78
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setWidgetData(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 112
    return-void
.end method

.method public updateWidget()V
    .locals 4

    .prologue
    const v3, 0x7f0c010d

    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 189
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->setNeedLoad(Z)V

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->a:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->b:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailProductListWidget;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->d:Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/text/ContentDetailTagWidget;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 194
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->setHeightRelatedLayout(I)V

    .line 195
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 199
    :goto_0
    return-void

    .line 197
    :cond_0
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
