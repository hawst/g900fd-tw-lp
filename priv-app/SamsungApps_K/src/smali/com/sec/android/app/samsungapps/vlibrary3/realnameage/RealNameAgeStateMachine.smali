.class public Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine;
.super Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;
.source "ProGuard"


# static fields
.field private static a:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/StateMachine;-><init>()V

    .line 19
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine;

    if-nez v0, :cond_0

    .line 31
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine;

    .line 33
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine;->a:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine;

    return-object v0
.end method


# virtual methods
.method protected entry(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 2

    .prologue
    .line 78
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/f;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 93
    :goto_0
    :pswitch_0
    return-void

    .line 82
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Action;->INVOKE_VIEW:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 87
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 90
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Action;->NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Action;

    invoke-interface {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->onAction(Ljava/lang/Object;)V

    goto :goto_0

    .line 78
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;)Z
    .locals 2

    .prologue
    .line 39
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/f;->b:[I

    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$State;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 67
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 42
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/f;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 45
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$State;->INVOKE_VIEW:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 50
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/f;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 53
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$State;->WAIT_RESULT:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 58
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/f;->a:[I

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    goto :goto_0

    .line 61
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$State;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 64
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$State;->FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$State;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)V

    goto :goto_0

    .line 39
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_4
    .end packed-switch

    .line 42
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch

    .line 50
    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_3
    .end packed-switch

    .line 58
    :pswitch_data_3
    .packed-switch 0x3
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public bridge synthetic execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 7
    check-cast p2, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine;->execute(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeStateMachine$Event;)Z

    move-result v0

    return v0
.end method

.method protected exit(Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;)V
    .locals 0

    .prologue
    .line 99
    return-void
.end method
