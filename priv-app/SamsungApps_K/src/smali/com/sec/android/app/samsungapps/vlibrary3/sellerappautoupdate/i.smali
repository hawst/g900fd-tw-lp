.class final Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/i;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/Downloader$IDownloadSingleItemResult;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpd;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpd;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/i;->a:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDownloadCanceled()V
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/i;->a:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpd;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;->DOWNLOAD_FAILED_OR_CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpd;->a(Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpd;Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;)V

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/i;->a:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpd;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpd;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/Downloader;

    .line 133
    return-void
.end method

.method public final onDownloadFailed()V
    .locals 2

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/i;->a:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpd;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;->DOWNLOAD_FAILED_OR_CANCELED:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpd;->a(Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpd;Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/i;->a:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpd;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpd;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/Downloader;

    .line 127
    return-void
.end method

.method public final onDownloadSuccess()V
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/i;->a:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpd;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;->DOWNLOAD_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpd;->a(Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpd;Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpdStateMachine$Event;)V

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/i;->a:Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpd;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/sec/android/app/samsungapps/vlibrary3/sellerappautoupdate/SellerSingleAppAutoUpd;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/download/Downloader;

    .line 121
    return-void
.end method

.method public final onInstallFailedWithErrCode(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 145
    return-void
.end method

.method public final onPaymentSuccess()V
    .locals 0

    .prologue
    .line 151
    return-void
.end method

.method public final onProgress(JJ)V
    .locals 0

    .prologue
    .line 115
    return-void
.end method

.method public final onStateChanged()V
    .locals 0

    .prologue
    .line 139
    return-void
.end method
