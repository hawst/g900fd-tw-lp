.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

.field private _IPSMSInitCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitCommand$IPSMSInitCommandData;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitCommand$IPSMSInitCommandData;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitCommand;->_IPSMSInitCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitCommand$IPSMSInitCommandData;

    .line 26
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitCommand$IPSMSInitCommandData;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitCommand;->_IPSMSInitCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitCommand$IPSMSInitCommandData;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitCommand;Z)V
    .locals 0

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitCommand;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 3

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitCommand;->_IPSMSInitCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitCommand$IPSMSInitCommandData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitCommand$IPSMSInitCommandData;->createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    .line 40
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitCommand;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->startLoading()V

    .line 41
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/k;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/k;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitCommand;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitCommand;->_IPSMSInitCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitCommand$IPSMSInitCommandData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitCommand$IPSMSInitCommandData;->getInitResultMap()Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/l;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/l;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitCommand;)V

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitCommand;->requestInit(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSInitParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    .line 55
    return-void
.end method

.method public requestInit(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSInitParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V
    .locals 2

    .prologue
    .line 30
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->InitiatePSMSPurchaseNS(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/IPSMSInitParam;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 32
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/psms/PSMSInitCommand;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 34
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 35
    return-void
.end method
