.class public Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;
.super Lcom/sec/android/app/samsungapps/widget/CommonWidget;
.source "ProGuard"


# instance fields
.field a:Landroid/text/TextWatcher;

.field private b:Landroid/content/Context;

.field private d:Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;

.field private e:Lcom/sec/android/app/samsungapps/widget/interfaces/ICommentDetailWidgetClickListener;

.field private final f:Ljava/lang/String;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/EditText;

.field private i:Landroid/widget/RatingBar;

.field private j:Landroid/widget/Toast;

.field private k:Landroid/view/View;

.field private l:Landroid/widget/ScrollView;

.field private m:Ljava/util/ArrayList;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Z

.field private q:I

.field private r:I

.field private s:Landroid/widget/RatingBar$OnRatingBarChangeListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 54
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;)V

    .line 34
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->b:Landroid/content/Context;

    .line 35
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->d:Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;

    .line 36
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->e:Lcom/sec/android/app/samsungapps/widget/interfaces/ICommentDetailWidgetClickListener;

    .line 37
    const-string v0, "ReviewDetailWidget"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->f:Ljava/lang/String;

    .line 39
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->g:Landroid/widget/TextView;

    .line 40
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->h:Landroid/widget/EditText;

    .line 41
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->i:Landroid/widget/RatingBar;

    .line 42
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->j:Landroid/widget/Toast;

    .line 44
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->l:Landroid/widget/ScrollView;

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->m:Ljava/util/ArrayList;

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->n:Ljava/lang/String;

    .line 47
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->o:Ljava/lang/String;

    .line 48
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->p:Z

    .line 49
    iput v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->q:I

    .line 50
    iput v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->r:I

    .line 151
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/detail/q;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/detail/q;-><init>(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->s:Landroid/widget/RatingBar$OnRatingBarChangeListener;

    .line 183
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/detail/r;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/detail/r;-><init>(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->a:Landroid/text/TextWatcher;

    .line 55
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->b:Landroid/content/Context;

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->b:Landroid/content/Context;

    const v1, 0x7f0400dc

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->initView(Landroid/content/Context;I)V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 60
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->b:Landroid/content/Context;

    .line 35
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->d:Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;

    .line 36
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->e:Lcom/sec/android/app/samsungapps/widget/interfaces/ICommentDetailWidgetClickListener;

    .line 37
    const-string v0, "ReviewDetailWidget"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->f:Ljava/lang/String;

    .line 39
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->g:Landroid/widget/TextView;

    .line 40
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->h:Landroid/widget/EditText;

    .line 41
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->i:Landroid/widget/RatingBar;

    .line 42
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->j:Landroid/widget/Toast;

    .line 44
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->l:Landroid/widget/ScrollView;

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->m:Ljava/util/ArrayList;

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->n:Ljava/lang/String;

    .line 47
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->o:Ljava/lang/String;

    .line 48
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->p:Z

    .line 49
    iput v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->q:I

    .line 50
    iput v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->r:I

    .line 151
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/detail/q;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/detail/q;-><init>(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->s:Landroid/widget/RatingBar$OnRatingBarChangeListener;

    .line 183
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/detail/r;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/detail/r;-><init>(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->a:Landroid/text/TextWatcher;

    .line 61
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->b:Landroid/content/Context;

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->b:Landroid/content/Context;

    const v1, 0x7f0400dc

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->initView(Landroid/content/Context;I)V

    .line 63
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;I)I
    .locals 0

    .prologue
    .line 33
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->q:I

    return p1
.end method

.method private a(Ljava/lang/String;)I
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 266
    .line 267
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v4, v2

    move v1, v2

    .line 269
    :goto_0
    if-ge v4, v5, :cond_2

    .line 270
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 271
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 273
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    move v3, v1

    move v1, v2

    .line 274
    :goto_1
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    const/4 v7, -0x1

    if-eq v1, v7, :cond_1

    .line 275
    add-int/lit8 v3, v3, 0x1

    .line 276
    add-int/2addr v1, v6

    goto :goto_1

    :cond_0
    move v3, v1

    .line 269
    :cond_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move v1, v3

    goto :goto_0

    .line 280
    :cond_2
    return v1
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->h:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->j:Landroid/widget/Toast;

    return-object p1
.end method

.method private a()V
    .locals 6

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 484
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0800e4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0800e5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 486
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->i:Landroid/widget/RatingBar;

    invoke-virtual {v1}, Landroid/widget/RatingBar;->getRating()F

    move-result v1

    .line 487
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->i:Landroid/widget/RatingBar;

    if-eqz v2, :cond_0

    .line 488
    cmpl-float v2, v1, v3

    if-lez v2, :cond_1

    .line 489
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->i:Landroid/widget/RatingBar;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0800d0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    float-to-int v1, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/RatingBar;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 497
    :cond_0
    :goto_0
    return-void

    .line 490
    :cond_1
    cmpl-float v1, v1, v3

    if-nez v1, :cond_2

    .line 491
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->i:Landroid/widget/RatingBar;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0800c9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/RatingBar;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 493
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->i:Landroid/widget/RatingBar;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080095

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/RatingBar;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;Ljava/lang/String;)V
    .locals 3

    .prologue
    const v2, 0xa000d

    .line 33
    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->a(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->getRating()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->e:Lcom/sec/android/app/samsungapps/widget/interfaces/ICommentDetailWidgetClickListener;

    const/4 v1, 0x1

    invoke-interface {v0, v2, v1}, Lcom/sec/android/app/samsungapps/widget/interfaces/ICommentDetailWidgetClickListener;->onChangeActionItem(IZ)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->e:Lcom/sec/android/app/samsungapps/widget/interfaces/ICommentDetailWidgetClickListener;

    const/4 v1, 0x0

    invoke-interface {v0, v2, v1}, Lcom/sec/android/app/samsungapps/widget/interfaces/ICommentDetailWidgetClickListener;->onChangeActionItem(IZ)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->b:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->n:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->a(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/widget/ScrollView;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->l:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->o:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->a()V

    return-void
.end method

.method static synthetic e(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->g:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic f(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->o:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->n:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->r:I

    return v0
.end method

.method static synthetic i(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->h:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->a(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->r:I

    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->r:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->h:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->h:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const-class v2, Landroid/text/style/ForegroundColorSpan;

    invoke-interface {v0, v3, v1, v2}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/ForegroundColorSpan;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->h:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    iput-boolean v7, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->p:Z

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->h:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->h:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->d:Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->getProhibitWords()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->d:Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->getProhibitWords()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->h:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v3

    :goto_1
    if-ge v2, v4, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "<font color=#ff0000>%s</font>"

    new-array v5, v7, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->m:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v5, v3

    invoke-static {v0, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_1

    :cond_2
    iput-boolean v7, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->p:Z

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->h:Landroid/widget/EditText;

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_2
.end method

.method static synthetic j(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->p:Z

    return v0
.end method

.method static synthetic k(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Z
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->p:Z

    return v0
.end method

.method static synthetic l(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->q:I

    return v0
.end method

.method static synthetic m(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/widget/Toast;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->j:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic n(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->d:Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;

    return-object v0
.end method

.method static synthetic o(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)V
    .locals 3

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->b:Landroid/content/Context;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080178

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(Ljava/lang/CharSequence;)V

    const-string v1, "Reviews are already registered. Go to the screen for modifying the review."

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0802ef

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/widget/detail/w;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/widget/detail/w;-><init>(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08023d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/widget/detail/x;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/widget/detail/x;-><init>(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setNegativeButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->show()V

    :cond_0
    return-void
.end method

.method static synthetic p(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Landroid/widget/RatingBar;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->i:Landroid/widget/RatingBar;

    return-object v0
.end method

.method static synthetic q(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->m:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic r(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)Lcom/sec/android/app/samsungapps/widget/interfaces/ICommentDetailWidgetClickListener;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->e:Lcom/sec/android/app/samsungapps/widget/interfaces/ICommentDetailWidgetClickListener;

    return-object v0
.end method


# virtual methods
.method public getRating()I
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->i:Landroid/widget/RatingBar;

    invoke-virtual {v0}, Landroid/widget/RatingBar;->getRating()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public getReview()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->h:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public loadWidget()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->d:Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;

    if-eqz v0, :cond_0

    .line 91
    const v0, 0x7f0c02ec

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->k:Landroid/view/View;

    .line 92
    const v0, 0x7f0c02eb

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->l:Landroid/widget/ScrollView;

    .line 93
    const v0, 0x7f0c02ee

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->h:Landroid/widget/EditText;

    .line 94
    const v0, 0x7f0c02ef

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->g:Landroid/widget/TextView;

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->h:Landroid/widget/EditText;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0801fd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->l:Landroid/widget/ScrollView;

    invoke-virtual {v0, v4}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->h:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/detail/o;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/detail/o;-><init>(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->h:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/detail/p;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/detail/p;-><init>(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 139
    const v0, 0x7f0c02ed

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RatingBar;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->i:Landroid/widget/RatingBar;

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->i:Landroid/widget/RatingBar;

    invoke-virtual {v0, v4}, Landroid/widget/RatingBar;->setIsIndicator(Z)V

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->i:Landroid/widget/RatingBar;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->s:Landroid/widget/RatingBar$OnRatingBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/RatingBar;->setOnRatingBarChangeListener(Landroid/widget/RatingBar$OnRatingBarChangeListener;)V

    .line 142
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->updateWidget()V

    .line 143
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->resizeEditor()V

    .line 149
    :goto_0
    return-void

    .line 147
    :cond_0
    invoke-virtual {p0, v4}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->setVisibleRetry(I)Z

    goto :goto_0
.end method

.method public onCancelReview()V
    .locals 3

    .prologue
    .line 435
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->b:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->h:Landroid/widget/EditText;

    if-nez v0, :cond_1

    .line 442
    :cond_0
    :goto_0
    return-void

    .line 438
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->b:Landroid/content/Context;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 439
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->h:Landroid/widget/EditText;

    if-eqz v1, :cond_0

    .line 440
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->h:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_0
.end method

.method public onSaveReview()V
    .locals 4

    .prologue
    .line 382
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->b:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->h:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->i:Landroid/widget/RatingBar;

    if-nez v0, :cond_1

    .line 432
    :cond_0
    :goto_0
    return-void

    .line 386
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->h:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 389
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->i:Landroid/widget/RatingBar;

    invoke-virtual {v1}, Landroid/widget/RatingBar;->getRating()F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 391
    if-eqz v1, :cond_0

    .line 395
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->i:Landroid/widget/RatingBar;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/RatingBar;->setIsIndicator(Z)V

    .line 396
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->h:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->q:I

    .line 397
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->d:Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;

    new-instance v3, Lcom/sec/android/app/samsungapps/widget/detail/u;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/widget/detail/u;-><init>(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)V

    invoke-virtual {v2, v0, v1, v3}, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->sendRequest(Ljava/lang/String;ILcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0
.end method

.method public refreshWidget()V
    .locals 0

    .prologue
    .line 349
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 359
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->h:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 360
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->h:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 363
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->m:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 364
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 367
    :cond_1
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->b:Landroid/content/Context;

    .line 368
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->d:Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;

    .line 369
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->e:Lcom/sec/android/app/samsungapps/widget/interfaces/ICommentDetailWidgetClickListener;

    .line 370
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->g:Landroid/widget/TextView;

    .line 371
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->i:Landroid/widget/RatingBar;

    .line 372
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->j:Landroid/widget/Toast;

    .line 373
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->l:Landroid/widget/ScrollView;

    .line 374
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->h:Landroid/widget/EditText;

    .line 375
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->m:Ljava/util/ArrayList;

    .line 376
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->removeAllViews()V

    .line 377
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->release()V

    .line 378
    return-void
.end method

.method public resizeEditor()V
    .locals 3

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->k:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->g:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->h:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->l:Landroid/widget/ScrollView;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->l:Landroid/widget/ScrollView;

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->k:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->g:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getHeight()I

    move-result v2

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    .line 82
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070025

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 83
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->h:Landroid/widget/EditText;

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setMaxHeight(I)V

    .line 85
    :cond_0
    return-void
.end method

.method public setWidgetClickListener(Lcom/sec/android/app/samsungapps/widget/interfaces/ICommentDetailWidgetClickListener;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->e:Lcom/sec/android/app/samsungapps/widget/interfaces/ICommentDetailWidgetClickListener;

    .line 68
    return-void
.end method

.method public setWidgetData(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 353
    check-cast p1, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;

    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->d:Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;

    .line 354
    return-void
.end method

.method public updateWidget()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->b:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->h:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->d:Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;

    if-nez v0, :cond_1

    .line 181
    :cond_0
    :goto_0
    return-void

    .line 165
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->h:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->d:Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->getOldComment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->i:Landroid/widget/RatingBar;

    const-wide/high16 v1, 0x3fe0000000000000L    # 0.5

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->d:Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->getOldRating()D

    move-result-wide v3

    mul-double/2addr v1, v3

    double-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/RatingBar;->setRating(F)V

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->h:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->h:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->d:Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/CommentDetailWidgetHelper;->getOldComment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->h:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->a:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->h:Landroid/widget/EditText;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/text/InputFilter;

    new-instance v2, Lcom/sec/android/app/samsungapps/widget/detail/t;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/widget/detail/t;-><init>(Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;)V

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->g:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->h:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->length()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/140"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->h:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    const/16 v1, 0x8c

    if-ne v0, v1, :cond_2

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->g:Landroid/widget/TextView;

    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 178
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->e:Lcom/sec/android/app/samsungapps/widget/interfaces/ICommentDetailWidgetClickListener;

    const v1, 0xa000d

    invoke-interface {v0, v1, v5}, Lcom/sec/android/app/samsungapps/widget/interfaces/ICommentDetailWidgetClickListener;->onChangeActionItem(IZ)V

    .line 179
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->setVisibleLoading(I)Z

    .line 180
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/widget/detail/ReviewDetailWidget;->a()V

    goto/16 :goto_0
.end method
