.class final Lcom/sec/android/app/samsungapps/vlibrary2/purchase/h;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;)V
    .locals 0

    .prologue
    .line 343
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/h;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCommandResult(Z)V
    .locals 2

    .prologue
    .line 347
    if-eqz p1, :cond_0

    .line 349
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/h;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->onFinalResult(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->access$700(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;Z)V

    .line 352
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/h;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_IPurchaseView:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseView;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->access$600(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseView;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseView;->onPurchaseCompleted(Z)V

    .line 353
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/h;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->getSelMethod()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_PSMS:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/h;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->getSelMethod()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_CHINA_ALIPAY:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/h;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->getSelMethod()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_KOREA_INICIS:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/h;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->getSelMethod()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_KOREA_UPOINT:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/h;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->getSelMethod()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->_KOREA_DANAL:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    if-ne v0, v1, :cond_2

    :cond_1
    if-nez p1, :cond_2

    .line 359
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/h;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_PurchaseInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;
    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;->getProduct()Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/primitiveobjects2/ObjectHavingProductID;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/download/downloadstate/DLStateQueue;->setDownloadFailed(Ljava/lang/String;)Z

    .line 360
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/h;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->_IPurchaseView:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseView;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->access$600(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseView;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/IPurchaseView;->killView()V

    .line 361
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/h;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->onFinalResult(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;->access$800(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseCommand;Z)V

    .line 363
    :cond_2
    return-void
.end method
