.class public Lcom/sec/android/app/samsungapps/tobelog/LogBody;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/tobelog/ILogBody;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field public bannerId:Ljava/lang/String;

.field public bannerIndex:I

.field public bannerType:Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;

.field public categoryId:Ljava/lang/String;

.field public connectionType:Lcom/sec/android/app/samsungapps/tobelog/LogBody$NetworkType;

.field public contentId:Ljava/lang/String;

.field public curatedSetIndex:I

.field public entryPoint:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

.field public eventCount:I

.field public latitude:D

.field public longtitude:D

.field public mcc:I

.field public mnc:Ljava/lang/String;

.field public pageInTimeStamp:J

.field public pageTimeDuration:J

.field public previousPageId:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

.field public pushMessageStatus:Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;

.field public searchKeyWord:Ljava/lang/String;

.field public storeType:Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->eventCount:I

    .line 55
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a:Ljava/lang/String;

    .line 56
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->b:Ljava/lang/String;

    .line 57
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0xb6

    const/4 v2, 0x1

    .line 250
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 252
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    .line 253
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 254
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    .line 255
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 256
    iget v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->eventCount:I

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;IZ)V

    .line 258
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/StringBuilder;D)V
    .locals 2

    .prologue
    .line 490
    const-wide/16 v0, 0x0

    cmpl-double v0, p1, v0

    if-nez v0, :cond_0

    .line 495
    :goto_0
    return-void

    .line 494
    :cond_0
    invoke-virtual {p0, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private static a(Ljava/lang/StringBuilder;IZ)V
    .locals 0

    .prologue
    .line 482
    if-nez p2, :cond_0

    if-nez p1, :cond_0

    .line 487
    :goto_0
    return-void

    .line 486
    :cond_0
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private static a(Ljava/lang/StringBuilder;J)V
    .locals 2

    .prologue
    .line 473
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 474
    const-wide/16 v0, 0x3e8

    div-long/2addr p1, v0

    .line 477
    :cond_0
    invoke-virtual {p0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 479
    return-void
.end method

.method private static a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 460
    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 461
    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/tobelog/LogBodyDataException;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogBodyDataException;-><init>()V

    throw v0

    .line 464
    :cond_1
    if-nez p1, :cond_2

    .line 469
    :goto_0
    return-void

    .line 468
    :cond_2
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private b()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0xb6

    const/4 v2, 0x1

    .line 306
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 308
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    .line 309
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 310
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    .line 311
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 312
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->previousPageId:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->value()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    .line 313
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 314
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->contentId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    .line 315
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 316
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->storeType:Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    .line 317
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 318
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->entryPoint:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->code()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    .line 319
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 320
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->categoryId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    .line 321
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 322
    iget v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->curatedSetIndex:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;IZ)V

    .line 324
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private c()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/16 v3, 0xb6

    .line 328
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 330
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a:Ljava/lang/String;

    invoke-static {v2, v0, v4}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    .line 331
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 332
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->b:Ljava/lang/String;

    invoke-static {v2, v0, v4}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    .line 333
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 334
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->contentId:Ljava/lang/String;

    invoke-static {v2, v0, v5}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    .line 335
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 336
    iget v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->bannerIndex:I

    invoke-static {v2, v0, v4}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;IZ)V

    .line 337
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 338
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->bannerType:Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    invoke-static {v2, v0, v4}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    .line 339
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 340
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->bannerId:Ljava/lang/String;

    invoke-static {v2, v0, v4}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    .line 341
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 342
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->pushMessageStatus:Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;

    if-nez v0, :cond_1

    :goto_1
    invoke-static {v2, v1, v5}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    .line 343
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 344
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->categoryId:Ljava/lang/String;

    invoke-static {v2, v0, v5}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    .line 345
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 346
    iget v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->curatedSetIndex:I

    invoke-static {v2, v0, v5}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;IZ)V

    .line 348
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 338
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->bannerType:Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 342
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->pushMessageStatus:Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;->name()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method private d()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/16 v3, 0xb6

    .line 352
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 354
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a:Ljava/lang/String;

    invoke-static {v2, v0, v5}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    .line 355
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 356
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->b:Ljava/lang/String;

    invoke-static {v2, v0, v5}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    .line 357
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 358
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->contentId:Ljava/lang/String;

    invoke-static {v2, v0, v5}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    .line 359
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 360
    iget v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->bannerIndex:I

    invoke-static {v2, v0, v4}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;IZ)V

    .line 361
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 362
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->bannerType:Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    invoke-static {v2, v0, v4}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    .line 363
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 364
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->bannerId:Ljava/lang/String;

    invoke-static {v2, v0, v4}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    .line 365
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 366
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->pushMessageStatus:Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;

    if-nez v0, :cond_1

    :goto_1
    invoke-static {v2, v1, v4}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    .line 367
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 368
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->categoryId:Ljava/lang/String;

    invoke-static {v2, v0, v5}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    .line 369
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 370
    iget v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->curatedSetIndex:I

    invoke-static {v2, v0, v4}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;IZ)V

    .line 372
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 362
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->bannerType:Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 366
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->pushMessageStatus:Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;->name()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method private e()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/16 v3, 0xb6

    .line 376
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 378
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a:Ljava/lang/String;

    invoke-static {v2, v0, v5}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    .line 379
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 380
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->b:Ljava/lang/String;

    invoke-static {v2, v0, v5}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    .line 381
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 382
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->contentId:Ljava/lang/String;

    invoke-static {v2, v0, v5}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    .line 383
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 384
    iget v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->bannerIndex:I

    invoke-static {v2, v0, v4}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;IZ)V

    .line 385
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 386
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->bannerType:Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    invoke-static {v2, v0, v4}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    .line 387
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 388
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->bannerId:Ljava/lang/String;

    invoke-static {v2, v0, v4}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    .line 389
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 390
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->pushMessageStatus:Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;

    if-nez v0, :cond_1

    :goto_1
    invoke-static {v2, v1, v4}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    .line 391
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 392
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->categoryId:Ljava/lang/String;

    invoke-static {v2, v0, v4}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    .line 393
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 394
    iget v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->curatedSetIndex:I

    invoke-static {v2, v0, v4}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;IZ)V

    .line 396
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 386
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->bannerType:Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 390
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->pushMessageStatus:Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;->name()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method


# virtual methods
.method public getPageTimeDuration()J
    .locals 2

    .prologue
    .line 499
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->pageTimeDuration:J

    return-wide v0
.end method

.method public increaseEventCount()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->eventCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->eventCount:I

    .line 62
    iget v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->eventCount:I

    return v0
.end method

.method public sumPageTimeDuration(J)J
    .locals 2

    .prologue
    .line 504
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->pageTimeDuration:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->pageTimeDuration:J

    .line 505
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->pageTimeDuration:J

    return-wide v0
.end method

.method public toString(Lcom/sec/android/app/samsungapps/tobelog/ILogEvent;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x1

    const/4 v3, 0x0

    const/16 v4, 0xb6

    .line 68
    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->PAGE_PV_TIME:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 70
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a:Ljava/lang/String;

    invoke-static {v1, v2, v5}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->previousPageId:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    if-nez v2, :cond_0

    :goto_0
    invoke-static {v1, v0, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->b:Ljava/lang/String;

    invoke-static {v1, v0, v5}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->categoryId:Ljava/lang/String;

    invoke-static {v1, v0, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->contentId:Ljava/lang/String;

    invoke-static {v1, v0, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->pageTimeDuration:J

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;J)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->eventCount:I

    invoke-static {v1, v0, v5}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;IZ)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 206
    :goto_1
    return-object v0

    .line 70
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->previousPageId:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->value()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 72
    :cond_1
    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_PURCHASED_LIST_BUTTON:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 74
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 76
    :cond_2
    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_WISH_LIST_MENU:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 78
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 80
    :cond_3
    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_PURCHASED_LIST_MENU:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 82
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 84
    :cond_4
    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_PAYMENT_METHOD_MENU:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 86
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 88
    :cond_5
    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_ANNOUNCEMENT_MENU:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 90
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 92
    :cond_6
    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_SETTINGS_MENU:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 94
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 96
    :cond_7
    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_HELP_MENU:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 98
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 100
    :cond_8
    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_MORE_CURATED_SET:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 102
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a:Ljava/lang/String;

    invoke-static {v2, v1, v5}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->b:Ljava/lang/String;

    invoke-static {v2, v1, v5}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->contentId:Ljava/lang/String;

    invoke-static {v2, v1, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->bannerIndex:I

    invoke-static {v2, v1, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;IZ)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->bannerType:Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;

    if-nez v1, :cond_9

    move-object v1, v0

    :goto_2
    invoke-static {v2, v1, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->bannerId:Ljava/lang/String;

    invoke-static {v2, v1, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->pushMessageStatus:Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;

    if-nez v1, :cond_a

    :goto_3
    invoke-static {v2, v0, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->categoryId:Ljava/lang/String;

    invoke-static {v2, v0, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->curatedSetIndex:I

    invoke-static {v2, v0, v5}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;IZ)V

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_9
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->bannerType:Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;->name()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->pushMessageStatus:Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 104
    :cond_b
    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_MORE_TOP_GAMES:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 106
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 108
    :cond_c
    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_MORE_TOP_PAID:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 110
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 112
    :cond_d
    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_MORE_TOP_FREE:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 114
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 116
    :cond_e
    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_MORE_TOP_NEW:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 118
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 120
    :cond_f
    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->MOVE_OUT_FROM_MAIN_WITHOUT_ACTION:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 122
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 124
    :cond_10
    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->APP_START:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 126
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a:Ljava/lang/String;

    invoke-static {v0, v1, v5}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->b:Ljava/lang/String;

    invoke-static {v0, v1, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->mcc:I

    invoke-static {v0, v1, v5}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;IZ)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->mnc:Ljava/lang/String;

    invoke-static {v0, v1, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->latitude:D

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;D)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->longtitude:D

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;D)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->connectionType:Lcom/sec/android/app/samsungapps/tobelog/LogBody$NetworkType;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/tobelog/LogBody$NetworkType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->entryPoint:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->code()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 128
    :cond_11
    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->APP_FINISH:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 130
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a:Ljava/lang/String;

    invoke-static {v0, v1, v5}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->b:Ljava/lang/String;

    invoke-static {v0, v1, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->mcc:I

    invoke-static {v0, v1, v5}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;IZ)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->mnc:Ljava/lang/String;

    invoke-static {v0, v1, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->latitude:D

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;D)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->longtitude:D

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;D)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->connectionType:Lcom/sec/android/app/samsungapps/tobelog/LogBody$NetworkType;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/tobelog/LogBody$NetworkType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->entryPoint:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->code()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 132
    :cond_12
    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->PAGE_PV_PATH_TIME:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 134
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a:Ljava/lang/String;

    invoke-static {v0, v1, v5}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->previousPageId:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->value()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->b:Ljava/lang/String;

    invoke-static {v0, v1, v5}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->categoryId:Ljava/lang/String;

    invoke-static {v0, v1, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->contentId:Ljava/lang/String;

    invoke-static {v0, v1, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->pageTimeDuration:J

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;J)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->eventCount:I

    invoke-static {v0, v1, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;IZ)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 136
    :cond_13
    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_BUY_DOWNLOAD_BUTTON:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 138
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->b()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 140
    :cond_14
    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->PURCHASE_COMPLETE:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 142
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->b()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 144
    :cond_15
    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_BIG_BANNER:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 146
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->c()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 148
    :cond_16
    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_MIDDLE_BANNER:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 150
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->c()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 152
    :cond_17
    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_BOTTOM_BANNER:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_18

    .line 154
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->c()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 156
    :cond_18
    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_SHARE_BUTTON:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 158
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->d()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 160
    :cond_19
    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_WISH_LIST_BUTTON:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 162
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->d()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 164
    :cond_1a
    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_LIKE_BUTTON:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 166
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->d()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 168
    :cond_1b
    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_DISLIKE_BUTTON:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 170
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->d()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 172
    :cond_1c
    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_OVERVIEW_TAB:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 174
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->d()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 176
    :cond_1d
    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_REVIEW_TAB:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 178
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->d()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 180
    :cond_1e
    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_RELATED_TAB:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 182
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->d()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 184
    :cond_1f
    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_MORE_REVIEW:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_20

    .line 186
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->e()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 188
    :cond_20
    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_MORE_FROM_THIS_DEVELOPER:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_21

    .line 190
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->e()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 192
    :cond_21
    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_SEARCH_WITH_KEYWORD:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_22

    .line 194
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a:Ljava/lang/String;

    invoke-static {v0, v1, v5}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->b:Ljava/lang/String;

    invoke-static {v0, v1, v5}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->searchKeyWord:Ljava/lang/String;

    invoke-static {v0, v1, v5}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 196
    :cond_22
    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_PUSH_SETTING_ON_OFF:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_25

    .line 198
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a:Ljava/lang/String;

    invoke-static {v2, v1, v5}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->b:Ljava/lang/String;

    invoke-static {v2, v1, v5}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->contentId:Ljava/lang/String;

    invoke-static {v2, v1, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->bannerIndex:I

    invoke-static {v2, v1, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;IZ)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->bannerType:Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;

    if-nez v1, :cond_23

    move-object v1, v0

    :goto_4
    invoke-static {v2, v1, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->bannerId:Ljava/lang/String;

    invoke-static {v2, v1, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->pushMessageStatus:Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;

    if-nez v1, :cond_24

    :goto_5
    invoke-static {v2, v0, v5}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->categoryId:Ljava/lang/String;

    invoke-static {v2, v0, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->curatedSetIndex:I

    invoke-static {v2, v0, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a(Ljava/lang/StringBuilder;IZ)V

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_23
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->bannerType:Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;->name()Ljava/lang/String;

    move-result-object v1

    goto :goto_4

    :cond_24
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->pushMessageStatus:Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 200
    :cond_25
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_PUSH_MESSAGE:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 202
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->a()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 206
    :cond_26
    const-string v0, ""

    goto/16 :goto_1
.end method
