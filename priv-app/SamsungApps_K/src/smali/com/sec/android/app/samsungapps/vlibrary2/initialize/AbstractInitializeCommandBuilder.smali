.class public abstract Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AbstractInitializeCommandBuilder;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializeCommandBuilder;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/initialize/InitializeCommand$IInitializeCommandData;


# instance fields
.field private _Context:Landroid/content/Context;

.field private _IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;

.field private _ICheckAppUpgradeCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ICheckAppUpgradeCommandBuilder;

.field private _ICountrySearchCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/country/ICountrySearchCommandBuilder;

.field private _IDisclaimerCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/IDisclaimerCommandBuilder;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/country/ICountrySearchCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ICheckAppUpgradeCommandBuilder;Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/IDisclaimerCommandBuilder;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AbstractInitializeCommandBuilder;->_Context:Landroid/content/Context;

    .line 24
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AbstractInitializeCommandBuilder;->_IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;

    .line 25
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AbstractInitializeCommandBuilder;->_ICheckAppUpgradeCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ICheckAppUpgradeCommandBuilder;

    .line 26
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AbstractInitializeCommandBuilder;->_ICountrySearchCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/country/ICountrySearchCommandBuilder;

    .line 27
    iput-object p5, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AbstractInitializeCommandBuilder;->_IDisclaimerCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/IDisclaimerCommandBuilder;

    .line 28
    return-void
.end method


# virtual methods
.method public autoLogin()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AbstractInitializeCommandBuilder;->_IAccountCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;->autoLogin()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    return-object v0
.end method

.method public checkAppUpgradeCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AbstractInitializeCommandBuilder;->_ICheckAppUpgradeCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ICheckAppUpgradeCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/checkappupgrade/ICheckAppUpgradeCommandBuilder;->validateCheckAppUpgrade()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    return-object v0
.end method

.method public countrySearch()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AbstractInitializeCommandBuilder;->_ICountrySearchCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/country/ICountrySearchCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/ICountrySearchCommandBuilder;->countrySearchCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    return-object v0
.end method

.method protected getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AbstractInitializeCommandBuilder;->_Context:Landroid/content/Context;

    return-object v0
.end method

.method public isReqCountrySearchExecuted()Z
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AbstractInitializeCommandBuilder;->_ICountrySearchCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/country/ICountrySearchCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/ICountrySearchCommandBuilder;->isReqCountrySearchExectuted()Z

    move-result v0

    return v0
.end method

.method public validateDisclaimer()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AbstractInitializeCommandBuilder;->_IDisclaimerCommandBuilder:Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/IDisclaimerCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/discalimer/IDisclaimerCommandBuilder;->validateDisclaimer()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    return-object v0
.end method
