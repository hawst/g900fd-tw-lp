.class public Lcom/sec/android/app/samsungapps/NoticeListViewActivity;
.super Lcom/sec/android/app/samsungapps/ActionBarActivity;
.source "ProGuard"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList$NoticeListObserver;


# static fields
.field private static j:I

.field private static k:I


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;

.field private d:I

.field private e:I

.field private f:Landroid/view/View;

.field private g:Landroid/view/View;

.field private h:Landroid/widget/ListView;

.field private i:Z

.field private l:Landroid/content/Context;

.field private m:Landroid/view/View;

.field private n:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->j:I

    .line 45
    const/16 v0, 0xf

    sput v0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->k:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 33
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;-><init>()V

    .line 35
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->a:Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;

    .line 36
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;

    .line 37
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;

    .line 38
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->d:I

    .line 39
    const/16 v0, 0xf

    iput v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->e:I

    .line 40
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->f:Landroid/view/View;

    .line 41
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->g:Landroid/view/View;

    .line 42
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->h:Landroid/widget/ListView;

    .line 43
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->i:Z

    .line 46
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->l:Landroid/content/Context;

    .line 47
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->m:Landroid/view/View;

    .line 48
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->n:Z

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/NoticeListViewActivity;)Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->a:Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;

    iget v1, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->d:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->setStartPosition(I)V

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;

    iget v1, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->e:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->setEndPosition(I)V

    .line 116
    return-void
.end method

.method private a(Z)V
    .locals 5

    .prologue
    const v4, 0x7f0c0177

    const v3, 0x7f0c007f

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 271
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->f:Landroid/view/View;

    if-nez v0, :cond_0

    .line 295
    :goto_0
    return-void

    .line 274
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 275
    if-eqz p1, :cond_1

    .line 277
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->i:Z

    .line 278
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->f:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 279
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->f:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->g:Landroid/view/View;

    const v1, 0x7f0c0086

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 281
    new-instance v1, Lcom/sec/android/app/samsungapps/fj;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/fj;-><init>(Lcom/sec/android/app/samsungapps/NoticeListViewActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 292
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->f:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 293
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->f:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/NoticeListViewActivity;)Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;

    return-object v0
.end method

.method private b()V
    .locals 5

    .prologue
    .line 177
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;->getNoticeArray()Ljava/util/ArrayList;

    move-result-object v1

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;->getNoticeArray()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 179
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 181
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->add(Ljava/lang/Object;)Z

    .line 179
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 184
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;->getNoticeArray()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 190
    :goto_1
    return-void

    .line 187
    :catch_0
    move-exception v0

    .line 188
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "NoticeListViewActivity::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/NoticeListViewActivity;)I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->e:I

    return v0
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/NoticeListViewActivity;)V
    .locals 3

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->d:I

    iget v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->e:I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->getTotalCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    sget v1, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->j:I

    add-int/2addr v1, v0

    sget v2, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->k:I

    add-int/2addr v0, v2

    iput v1, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->d:I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->getTotalCount()I

    move-result v1

    if-le v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->getTotalCount()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->e:I

    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->a()V

    :cond_0
    return-void

    :cond_1
    iput v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->e:I

    goto :goto_0
.end method

.method static synthetic e(Lcom/sec/android/app/samsungapps/NoticeListViewActivity;)V
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->addObserver(Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList$NoticeListObserver;)V

    return-void
.end method

.method static synthetic f(Lcom/sec/android/app/samsungapps/NoticeListViewActivity;)V
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->loadMoreNoticeList()V

    return-void
.end method

.method static synthetic g(Lcom/sec/android/app/samsungapps/NoticeListViewActivity;)Landroid/view/View;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->f:Landroid/view/View;

    return-object v0
.end method

.method static synthetic h(Lcom/sec/android/app/samsungapps/NoticeListViewActivity;)Z
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->i:Z

    return v0
.end method


# virtual methods
.method public moreNoticeLoading()V
    .locals 1

    .prologue
    .line 248
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->a(Z)V

    .line 250
    return-void
.end method

.method public moreNoticeLoadingCompleted(Z)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 255
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->removeObserver(Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList$NoticeListObserver;)V

    .line 257
    if-eqz p1, :cond_1

    .line 258
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;->getNoticeArray()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 259
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->b()V

    .line 260
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->f:Landroid/view/View;

    const v1, 0x7f0c007f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 261
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->f:Landroid/view/View;

    const v1, 0x7f0c0177

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->f:Landroid/view/View;

    const v1, 0x7f0c016a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 267
    :cond_0
    :goto_0
    return-void

    .line 265
    :cond_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->a(Z)V

    goto :goto_0
.end method

.method public noticeLoadingCompleted(Z)V
    .locals 3

    .prologue
    .line 196
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->n:Z

    if-eqz v0, :cond_1

    .line 229
    :cond_0
    :goto_0
    return-void

    .line 199
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->removeObserver(Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList$NoticeListObserver;)V

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->getTotalCount()I

    move-result v0

    if-eqz v0, :cond_2

    .line 202
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->setVisibleEmpty(Z)Z

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->h:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 204
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->a:Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;

    if-nez v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;->getNoticeArray()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 209
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->b()V

    .line 210
    new-instance v0, Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;

    const v1, 0x7f04007a

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->a:Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->h:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->a:Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 212
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->a:Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 229
    :catch_0
    move-exception v0

    goto :goto_0

    .line 218
    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->setVisibleEmpty(Z)Z

    .line 219
    const v0, 0x7f080158

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->setEmptyText(Ljava/lang/String;)V

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->h:Landroid/widget/ListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const v7, 0x7f0c0180

    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 55
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 56
    iput-object p0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->l:Landroid/content/Context;

    .line 57
    const-string v0, "layout_inflater"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 58
    const v1, 0x7f040059

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->g:Landroid/view/View;

    .line 60
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->g:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->setMainViewAndEmptyView(Landroid/view/View;)V

    .line 66
    :goto_0
    const v0, 0x7f08018c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v4, v3, [I

    const v0, 0xa0008

    aput v0, v4, v6

    const v5, 0xb0001

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    .line 69
    const-string v0, "layout_inflater"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f040054

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->m:Landroid/view/View;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;->getNoticeList()Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->addObserver(Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList$NoticeListObserver;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->g:Landroid/view/View;

    const v1, 0x7f0c016a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->f:Landroid/view/View;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->g:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->h:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->h:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->h:Landroid/widget/ListView;

    new-instance v1, Lcom/sec/android/app/samsungapps/fi;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/fi;-><init>(Lcom/sec/android/app/samsungapps/NoticeListViewActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->a()V

    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->i:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, v3}, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->a(Z)V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->getTotalCount()I

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->g:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->g:Landroid/view/View;

    const v1, 0x7f0c007f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->loadNoticeList()V

    .line 70
    :cond_2
    return-void

    .line 63
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->g:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->setMainView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 69
    :cond_4
    invoke-virtual {p0, v6}, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->setVisibleEmpty(Z)Z

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->g:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->a:Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->h:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->a:Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->a:Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;->notifyDataSetChanged()V

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 336
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->n:Z

    .line 337
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onDestroy()V

    .line 339
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->c:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeList;->clear()V

    .line 343
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->a:Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;

    if-eqz v0, :cond_1

    .line 345
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->a:Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;->clear()V

    .line 346
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->a:Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;

    .line 348
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;

    if-eqz v0, :cond_3

    .line 350
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;->mNoticeArrayList:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 352
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;->mNoticeArrayList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 354
    :cond_2
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->b:Lcom/sec/android/app/samsungapps/vlibrary/doc/NoticeContainer;

    .line 356
    :cond_3
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->a:Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;

    invoke-virtual {v0, p3}, Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notice;

    .line 133
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notice;->getNoticeID()Ljava/lang/String;

    move-result-object v0

    .line 134
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->a:Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/view/NoticeListArrayAdapter;->notifyDataSetChanged()V

    .line 135
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->l:Landroid/content/Context;

    const/16 v2, 0x1d

    invoke-static {v1, v0, v2}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Ljava/lang/String;I)V

    .line 138
    return-void
.end method

.method public onSamsungAppsCreateOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;)Z
    .locals 1

    .prologue
    .line 324
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onSamsungAppsCreateOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;)Z

    .line 329
    const v0, 0x7f0c0004

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->removeItem(I)Z

    .line 331
    const/4 v0, 0x1

    return v0
.end method

.method public setEmptyText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 237
    const v0, 0x7f0c0083

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/NoticeListViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 238
    if-eqz v0, :cond_0

    .line 240
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 242
    :cond_0
    return-void
.end method
