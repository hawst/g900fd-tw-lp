.class public Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static a:Ljava/lang/String;

.field private static b:Ljava/lang/String;

.field private static c:I

.field private static d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 24
    sput-object v1, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->a:Ljava/lang/String;

    .line 25
    sput-object v1, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->b:Ljava/lang/String;

    .line 26
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->c:I

    .line 27
    sput-object v1, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createLogDataForDetailPageView(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/tobelog/LogData;
    .locals 2

    .prologue
    .line 108
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->PRODUCT_DETAIL:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->PAGE_PV_PATH_TIME:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    if-eqz p0, :cond_0

    :goto_0
    invoke-static {v0, v1, p0, p1, p2}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->createLogDataForPageView(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;Lcom/sec/android/app/samsungapps/tobelog/LogPage;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/tobelog/LogData;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object p0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->EMPTY:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    goto :goto_0
.end method

.method public static createLogDataForPageView(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;Lcom/sec/android/app/samsungapps/tobelog/LogPage;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/tobelog/LogData;
    .locals 5

    .prologue
    .line 128
    const/4 v0, 0x0

    .line 131
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->getInstance()Lcom/sec/android/app/samsungapps/tobelog/LogManager;

    move-result-object v1

    .line 132
    new-instance v2, Lcom/sec/android/app/samsungapps/tobelog/LogBody;

    sget-object v3, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->a:Ljava/lang/String;

    sget-object v4, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->b:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    iput-object p2, v2, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->previousPageId:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    .line 134
    iput-object p3, v2, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->categoryId:Ljava/lang/String;

    .line 135
    iput-object p4, v2, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->contentId:Ljava/lang/String;

    .line 136
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iput-wide v3, v2, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->pageInTimeStamp:J

    .line 137
    invoke-static {}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->getUserId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, p0, p1, v3, v2}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->createLogData(Lcom/sec/android/app/samsungapps/tobelog/ILogPage;Lcom/sec/android/app/samsungapps/tobelog/ILogEvent;Ljava/lang/String;Lcom/sec/android/app/samsungapps/tobelog/ILogBody;)Lcom/sec/android/app/samsungapps/tobelog/LogData;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 143
    :goto_0
    return-object v0

    .line 138
    :catch_0
    move-exception v1

    .line 139
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception on ToBeLogWrapper.createLogDataForPageView : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getUserId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 233
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v1

    .line 234
    const/4 v0, 0x0

    .line 235
    if-eqz v1, :cond_0

    .line 236
    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getUserID()Ljava/lang/String;

    move-result-object v0

    .line 238
    :cond_0
    return-object v0
.end method

.method public static initialize(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 34
    :try_start_0
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getIMEI()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->a:Ljava/lang/String;

    .line 35
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMCC()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->c:I

    .line 36
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMNC()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->d:Ljava/lang/String;

    .line 37
    sget v0, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->c:I

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/MccTable;->countryCodeForMcc(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->b:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 42
    :goto_0
    return-void

    .line 38
    :catch_0
    move-exception v0

    .line 39
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Exception on ToBeLogWrapper.initialize : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendAppStartAndFinishLog(Landroid/content/Context;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;)V
    .locals 6

    .prologue
    .line 46
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->STAFF_PICKS:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    .line 47
    instance-of v1, p0, Lcom/sec/android/app/samsungapps/viewpager/ViewPagerMainView;

    if-eqz v1, :cond_1

    .line 48
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->STAFF_PICKS:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    .line 74
    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sendAppStartAndFinishLog - pageId : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", eventId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", serviceCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V

    .line 78
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->getInstance()Lcom/sec/android/app/samsungapps/tobelog/LogManager;

    move-result-object v1

    .line 79
    new-instance v2, Lcom/sec/android/app/samsungapps/tobelog/LogBody;

    sget-object v3, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->a:Ljava/lang/String;

    sget-object v4, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->b:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    invoke-static {}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->getUserId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, p1, v3, v2}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->createLogData(Lcom/sec/android/app/samsungapps/tobelog/ILogPage;Lcom/sec/android/app/samsungapps/tobelog/ILogEvent;Ljava/lang/String;Lcom/sec/android/app/samsungapps/tobelog/ILogBody;)Lcom/sec/android/app/samsungapps/tobelog/LogData;

    move-result-object v3

    .line 82
    sget v0, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->c:I

    iput v0, v2, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->mcc:I

    .line 83
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->d:Ljava/lang/String;

    iput-object v0, v2, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->mnc:Ljava/lang/String;

    .line 85
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDeviceInfoLoader()Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->getConnectedNetworkType()I

    move-result v0

    .line 86
    const/4 v4, 0x1

    if-ne v0, v4, :cond_5

    .line 88
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody$NetworkType;->WIFI:Lcom/sec/android/app/samsungapps/tobelog/LogBody$NetworkType;

    .line 93
    :goto_1
    const-wide/16 v4, 0x0

    iput-wide v4, v2, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->latitude:D

    .line 94
    const-wide/16 v4, 0x0

    iput-wide v4, v2, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->longtitude:D

    .line 95
    iput-object v0, v2, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->connectionType:Lcom/sec/android/app/samsungapps/tobelog/LogBody$NetworkType;

    .line 96
    iput-object p2, v2, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->entryPoint:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    .line 98
    invoke-virtual {v1, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->putNormalLog(Lcom/sec/android/app/samsungapps/tobelog/LogData;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 104
    :goto_2
    return-void

    .line 49
    :cond_1
    instance-of v1, p0, Lcom/sec/android/app/samsungapps/CategoryListActivity;

    if-eqz v1, :cond_2

    .line 50
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CATEGORY_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    goto :goto_0

    .line 51
    :cond_2
    instance-of v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    if-eqz v1, :cond_3

    .line 52
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->PRODUCT_DETAIL:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    goto :goto_0

    .line 53
    :cond_3
    instance-of v1, p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;

    if-eqz v1, :cond_0

    .line 54
    check-cast p0, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/CategorizedListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 55
    const/4 v1, 0x0

    .line 57
    if-eqz v0, :cond_6

    .line 58
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 59
    if-eqz v0, :cond_6

    .line 60
    const-string v2, "_listType"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 61
    if-eqz v0, :cond_6

    .line 62
    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    .line 67
    :goto_3
    if-eqz v0, :cond_4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->ProductSetList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    if-ne v0, v1, :cond_4

    .line 68
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CONTENTS_SET_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    goto/16 :goto_0

    .line 70
    :cond_4
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->CATEGORY_PRODUCT_LIST:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    goto/16 :goto_0

    .line 90
    :cond_5
    :try_start_1
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody$NetworkType;->MOBILE:Lcom/sec/android/app/samsungapps/tobelog/LogBody$NetworkType;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 100
    :catch_0
    move-exception v0

    .line 101
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Exception on ToBeLogWrapper.sendAppStartAndFinishLog : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V

    goto :goto_2

    :cond_6
    move-object v0, v1

    goto :goto_3
.end method

.method public static sendClickPurchaseLog(Lcom/sec/android/app/samsungapps/tobelog/LogEvent;Lcom/sec/android/app/samsungapps/tobelog/LogPage;Ljava/lang/String;Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 173
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->getInstance()Lcom/sec/android/app/samsungapps/tobelog/LogManager;

    move-result-object v0

    .line 174
    new-instance v1, Lcom/sec/android/app/samsungapps/tobelog/LogBody;

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->a:Ljava/lang/String;

    sget-object v3, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->b:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    iput-object p1, v1, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->previousPageId:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    .line 176
    iput-object p2, v1, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->contentId:Ljava/lang/String;

    .line 177
    iput-object p3, v1, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->storeType:Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;

    .line 178
    iput-object p4, v1, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->entryPoint:Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    .line 179
    iput-object p5, v1, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->categoryId:Ljava/lang/String;

    .line 181
    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->PRODUCT_DETAIL:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    invoke-static {}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->getUserId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, p0, v3, v1}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->createLogData(Lcom/sec/android/app/samsungapps/tobelog/ILogPage;Lcom/sec/android/app/samsungapps/tobelog/ILogEvent;Ljava/lang/String;Lcom/sec/android/app/samsungapps/tobelog/ILogBody;)Lcom/sec/android/app/samsungapps/tobelog/LogData;

    move-result-object v1

    .line 184
    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->putNormalLog(Lcom/sec/android/app/samsungapps/tobelog/LogData;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 190
    :goto_0
    return-void

    .line 185
    :catch_0
    move-exception v0

    .line 186
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Exception on ToBeLogWrapper.sendClickDownloadButtonLog : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendMainPageViewLog(Lcom/sec/android/app/samsungapps/tobelog/LogData;)V
    .locals 5

    .prologue
    .line 148
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogData;->getBody()Lcom/sec/android/app/samsungapps/tobelog/ILogBody;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;

    .line 149
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->pageInTimeStamp:J

    sub-long/2addr v1, v3

    iput-wide v1, v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->pageTimeDuration:J

    .line 150
    invoke-static {}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->getInstance()Lcom/sec/android/app/samsungapps/tobelog/LogManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->putSummaryLog(Lcom/sec/android/app/samsungapps/tobelog/LogData;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 155
    :goto_0
    return-void

    .line 151
    :catch_0
    move-exception v0

    .line 152
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Exception on ToBeLogWrapper.sendMainPageViewLog : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendNormalClickLog(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;Ljava/lang/String;ILcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;Ljava/lang/String;Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 195
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->getInstance()Lcom/sec/android/app/samsungapps/tobelog/LogManager;

    move-result-object v0

    .line 196
    new-instance v1, Lcom/sec/android/app/samsungapps/tobelog/LogBody;

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->a:Ljava/lang/String;

    sget-object v3, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->b:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    iput-object p2, v1, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->contentId:Ljava/lang/String;

    .line 198
    iput p3, v1, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->bannerIndex:I

    .line 199
    iput-object p4, v1, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->bannerType:Lcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;

    .line 200
    iput-object p5, v1, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->bannerId:Ljava/lang/String;

    .line 201
    iput-object p6, v1, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->pushMessageStatus:Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;

    .line 202
    iput-object p7, v1, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->categoryId:Ljava/lang/String;

    .line 203
    iput p8, v1, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->curatedSetIndex:I

    .line 205
    invoke-static {}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->getUserId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p0, p1, v2, v1}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->createLogData(Lcom/sec/android/app/samsungapps/tobelog/ILogPage;Lcom/sec/android/app/samsungapps/tobelog/ILogEvent;Ljava/lang/String;Lcom/sec/android/app/samsungapps/tobelog/ILogBody;)Lcom/sec/android/app/samsungapps/tobelog/LogData;

    move-result-object v1

    .line 207
    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->putNormalLog(Lcom/sec/android/app/samsungapps/tobelog/LogData;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 213
    :goto_0
    return-void

    .line 208
    :catch_0
    move-exception v0

    .line 209
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Exception on ToBeLogWrapper.sendNormalClickLog : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendNormalPageViewLog(Lcom/sec/android/app/samsungapps/tobelog/LogData;)V
    .locals 5

    .prologue
    .line 117
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/tobelog/LogData;->getBody()Lcom/sec/android/app/samsungapps/tobelog/ILogBody;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;

    .line 118
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->pageInTimeStamp:J

    sub-long/2addr v1, v3

    iput-wide v1, v0, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->pageTimeDuration:J

    .line 119
    invoke-static {}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->getInstance()Lcom/sec/android/app/samsungapps/tobelog/LogManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->putNormalLog(Lcom/sec/android/app/samsungapps/tobelog/LogData;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 124
    :goto_0
    return-void

    .line 120
    :catch_0
    move-exception v0

    .line 121
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Exception on ToBeLogWrapper.sendDetailPageViewLog : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendSearchLog(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 218
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->getInstance()Lcom/sec/android/app/samsungapps/tobelog/LogManager;

    move-result-object v0

    .line 219
    new-instance v1, Lcom/sec/android/app/samsungapps/tobelog/LogBody;

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->a:Ljava/lang/String;

    sget-object v3, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->b:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    iput-object p2, v1, Lcom/sec/android/app/samsungapps/tobelog/LogBody;->searchKeyWord:Ljava/lang/String;

    .line 222
    invoke-static {}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->getUserId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p0, p1, v2, v1}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->createLogData(Lcom/sec/android/app/samsungapps/tobelog/ILogPage;Lcom/sec/android/app/samsungapps/tobelog/ILogEvent;Ljava/lang/String;Lcom/sec/android/app/samsungapps/tobelog/ILogBody;)Lcom/sec/android/app/samsungapps/tobelog/LogData;

    move-result-object v1

    .line 224
    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->putNormalLog(Lcom/sec/android/app/samsungapps/tobelog/LogData;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 230
    :goto_0
    return-void

    .line 225
    :catch_0
    move-exception v0

    .line 226
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Exception on ToBeLogWrapper.sendSearchLog : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendSummaryNormalLog(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;)V
    .locals 5

    .prologue
    .line 160
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->getInstance()Lcom/sec/android/app/samsungapps/tobelog/LogManager;

    move-result-object v0

    .line 161
    invoke-static {}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->getUserId()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/tobelog/LogBody;

    sget-object v3, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->a:Ljava/lang/String;

    sget-object v4, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->b:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/samsungapps/tobelog/LogBody;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, p0, p1, v1, v2}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->createLogData(Lcom/sec/android/app/samsungapps/tobelog/ILogPage;Lcom/sec/android/app/samsungapps/tobelog/ILogEvent;Ljava/lang/String;Lcom/sec/android/app/samsungapps/tobelog/ILogBody;)Lcom/sec/android/app/samsungapps/tobelog/LogData;

    move-result-object v1

    .line 162
    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/LogManager;->putSummaryLog(Lcom/sec/android/app/samsungapps/tobelog/LogData;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 168
    :goto_0
    return-void

    .line 163
    :catch_0
    move-exception v0

    .line 164
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Exception on ToBeLogWrapper.sendNormalLog : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/LogForLog;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method
