.class public final enum Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

.field public static final enum CURATED_PRODUCTLIST:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

.field public static final enum Category:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

.field public static final enum ContentCategoryProductList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

.field public static final enum FeaturedHot:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

.field public static final enum ProductSetList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

.field public static final enum Search:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

.field public static final enum TopAll:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

.field public static final enum TopFree:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

.field public static final enum TopNew:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

.field public static final enum TopPaid:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

.field public static final enum customerAlsoBought:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

.field public static final enum deprecated_purchased:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

.field public static final enum deprecated_upgradeList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

.field public static final enum etc:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

.field public static final enum wishList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 23
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    const-string v1, "FeaturedHot"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->FeaturedHot:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    .line 24
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    const-string v1, "TopAll"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopAll:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    .line 25
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    const-string v1, "TopPaid"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopPaid:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    .line 26
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    const-string v1, "TopFree"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopFree:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    .line 27
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    const-string v1, "TopNew"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopNew:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    .line 28
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    const-string v1, "Category"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->Category:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    .line 29
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    const-string v1, "Search"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->Search:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    .line 30
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    const-string v1, "deprecated_purchased"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->deprecated_purchased:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    .line 31
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    const-string v1, "ProductSetList"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->ProductSetList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    .line 32
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    const-string v1, "etc"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->etc:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    .line 33
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    const-string v1, "customerAlsoBought"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->customerAlsoBought:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    .line 34
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    const-string v1, "ContentCategoryProductList"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->ContentCategoryProductList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    .line 35
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    const-string v1, "deprecated_upgradeList"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->deprecated_upgradeList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    .line 36
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    const-string v1, "wishList"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->wishList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    .line 37
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    const-string v1, "CURATED_PRODUCTLIST"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->CURATED_PRODUCTLIST:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    .line 21
    const/16 v0, 0xf

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->FeaturedHot:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopAll:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopPaid:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopFree:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->TopNew:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->Category:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->Search:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->deprecated_purchased:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->ProductSetList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->etc:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->customerAlsoBought:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->ContentCategoryProductList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->deprecated_upgradeList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->wishList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->CURATED_PRODUCTLIST:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    return-object v0
.end method
