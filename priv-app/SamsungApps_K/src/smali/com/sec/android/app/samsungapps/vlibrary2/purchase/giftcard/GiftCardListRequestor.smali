.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListRequestor;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field _ActiveGiftCardList:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;

.field _ExpiredGiftCardList:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;

.field protected _IGiftCardReceiver:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardListReceiver;

.field _bExpiredGiftCardResult:Z

.field _bGiftCardResult:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardListReceiver;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 21
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 15
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListRequestor;->_ActiveGiftCardList:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;

    .line 16
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListRequestor;->_ExpiredGiftCardList:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;

    .line 17
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListRequestor;->_bGiftCardResult:Z

    .line 18
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListRequestor;->_bExpiredGiftCardResult:Z

    .line 22
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListRequestor;->_IGiftCardReceiver:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardListReceiver;

    .line 23
    return-void
.end method

.method private checkResult()V
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListRequestor;->iscompleteReceive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListRequestor;->_IGiftCardReceiver:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardListReceiver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardListReceiver;->complete()V

    .line 42
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListRequestor;->onFinalResult(Z)V

    .line 44
    :cond_0
    return-void
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListRequestor;->_IGiftCardReceiver:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardListReceiver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardListReceiver;->clear()V

    .line 28
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;

    const/high16 v1, 0x10000

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListRequestor;->_ActiveGiftCardList:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;

    .line 29
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListRequestor;->requestActiveGiftCardList()V

    .line 30
    return-void
.end method

.method protected iscompleteReceive()Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListRequestor;->_bGiftCardResult:Z

    return v0
.end method

.method protected onReceiveActiveGiftCardList(Z)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 49
    if-eqz p1, :cond_1

    .line 51
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListRequestor;->_bGiftCardResult:Z

    .line 52
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListRequestor;->_ActiveGiftCardList:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    .line 54
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListRequestor;->_IGiftCardReceiver:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardListReceiver;

    invoke-interface {v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardListReceiver;->add(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;)V

    goto :goto_0

    .line 56
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListRequestor;->checkResult()V

    .line 63
    :goto_1
    return-void

    .line 60
    :cond_1
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListRequestor;->_bGiftCardResult:Z

    .line 61
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListRequestor;->onFinalResult(Z)V

    goto :goto_1
.end method

.method protected onReceiveExpiredGiftCardList(Z)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 67
    if-eqz p1, :cond_1

    .line 69
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListRequestor;->_bExpiredGiftCardResult:Z

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListRequestor;->_ExpiredGiftCardList:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    .line 72
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListRequestor;->_IGiftCardReceiver:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardListReceiver;

    invoke-interface {v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCardListReceiver;->add(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;)V

    goto :goto_0

    .line 74
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListRequestor;->checkResult()V

    .line 81
    :goto_1
    return-void

    .line 78
    :cond_1
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListRequestor;->_bExpiredGiftCardResult:Z

    .line 79
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListRequestor;->onFinalResult(Z)V

    goto :goto_1
.end method

.method protected requestActiveGiftCardList()V
    .locals 4

    .prologue
    .line 85
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    const-string v1, "A"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListRequestor;->_ActiveGiftCardList:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/d;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardListRequestor;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->giftCardList(Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 92
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 93
    return-void
.end method
