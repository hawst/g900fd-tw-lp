.class public Lcom/sec/android/app/samsungapps/interimpage/InterimViewCreateMediator;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private a:Landroid/content/Context;

.field private b:I

.field private c:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/interimpage/InterimViewCreateMediator;->a:Landroid/content/Context;

    .line 19
    iput p2, p0, Lcom/sec/android/app/samsungapps/interimpage/InterimViewCreateMediator;->b:I

    .line 20
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/interimpage/InterimViewCreateMediator;->c:Landroid/view/LayoutInflater;

    .line 21
    return-void
.end method


# virtual methods
.method public createView(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 25
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/interimpage/InterimViewCreateMediator;->c:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/sec/android/app/samsungapps/interimpage/InterimViewCreateMediator;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 26
    const v1, 0x1020016

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 27
    iget-object v1, p1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->productName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 28
    return-object v2
.end method
