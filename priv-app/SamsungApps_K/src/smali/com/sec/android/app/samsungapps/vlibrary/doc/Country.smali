.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/price/ICurrencyFormatInfo;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# static fields
.field private static final AUSTRIA:I = 0xe8

.field private static final BRAZIL:I = 0x2d4

.field private static final BULGARIA:I = 0x11c

.field private static final CHINA:I = 0x1cc

.field private static final CHINA2:I = 0x1cd

.field private static final CROATIA:I = 0xdb

.field private static final CZECH:I = 0xe6

.field private static final DENMARK:I = 0xee

.field private static final FINLAND:I = 0xf4

.field private static final FRANCE:I = 0xd0

.field private static final GERMAN:I = 0x106

.field private static final GREECE:I = 0xca

.field private static final INDIA:I = 0x194

.field private static final IRAN:I = 0x1b0

.field private static final IRELAND:I = 0x110

.field private static final ITALY:I = 0xde

.field private static final KOREA:I = 0x1c2

.field private static final LITHUANIA:I = 0xf6

.field private static final NKOREA:I = 0x3e8

.field private static final NORWAY:I = 0xf2

.field private static final POLAND:I = 0x104

.field private static final ROMANIA:I = 0xe2

.field private static final RUSSIA:I = 0xfa

.field private static final SINGAPORE:I = 0x20d

.field private static final SLOVAKIA:I = 0xe7

.field private static final SWEDEN:I = 0xf0

.field private static final TEST_STORE:I = 0x3e8

.field private static final TURKEY:I = 0x11e

.field private static final UKRAINE:I = 0xff

.field private static final UNC_STORE:Ljava/lang/String; = "PPC"

.field private static final USA:I = 0x136


# instance fields
.field public MCC:Ljava/lang/String;

.field MNC:Ljava/lang/String;

.field private bNeedClearDisclaimer:Z

.field private bNeedUpdate:Z

.field public contentSizeLimitation:I

.field public countryCode:Ljava/lang/String;

.field public countryName:Ljava/lang/String;

.field public countryUrl:Ljava/lang/String;

.field public currencyUnitDivision:Z

.field public currencyUnitHasPenny:Z

.field public currencyUnitPrecedes:Z

.field public description:Ljava/lang/String;

.field public freeStoreClsf:I

.field public freeTabClsf:I

.field private hubURL:Ljava/lang/String;

.field lang:Ljava/lang/String;

.field mCSC:Ljava/lang/String;

.field mCheckAppUpgrade:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

.field private mDataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

.field mDeviceInfoLoader:Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

.field private mQAURL:Ljava/lang/String;

.field map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

.field public offset:I

.field public realCountryCode:Ljava/lang/String;

.field public snsVal:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string v0, "EN"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->lang:Ljava/lang/String;

    .line 23
    const-string v0, "00"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MNC:Ljava/lang/String;

    .line 25
    const-string v0, "XEU"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mCSC:Ljava/lang/String;

    .line 26
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->countryUrl:Ljava/lang/String;

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->hubURL:Ljava/lang/String;

    .line 29
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->countryCode:Ljava/lang/String;

    .line 30
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->countryName:Ljava/lang/String;

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    .line 37
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->realCountryCode:Ljava/lang/String;

    .line 56
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mCheckAppUpgrade:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    .line 57
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mDeviceInfoLoader:Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->bNeedClearDisclaimer:Z

    .line 664
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 104
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->createURL()V

    .line 105
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string v0, "EN"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->lang:Ljava/lang/String;

    .line 23
    const-string v0, "00"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MNC:Ljava/lang/String;

    .line 25
    const-string v0, "XEU"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mCSC:Ljava/lang/String;

    .line 26
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->countryUrl:Ljava/lang/String;

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->hubURL:Ljava/lang/String;

    .line 29
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->countryCode:Ljava/lang/String;

    .line 30
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->countryName:Ljava/lang/String;

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    .line 37
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->realCountryCode:Ljava/lang/String;

    .line 56
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mCheckAppUpgrade:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    .line 57
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mDeviceInfoLoader:Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->bNeedClearDisclaimer:Z

    .line 664
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 97
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->createURL()V

    .line 98
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mDeviceInfoLoader:Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    .line 99
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mDataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    .line 100
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->load()V

    .line 101
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string v0, "EN"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->lang:Ljava/lang/String;

    .line 23
    const-string v0, "00"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MNC:Ljava/lang/String;

    .line 25
    const-string v0, "XEU"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mCSC:Ljava/lang/String;

    .line 26
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->countryUrl:Ljava/lang/String;

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->hubURL:Ljava/lang/String;

    .line 29
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->countryCode:Ljava/lang/String;

    .line 30
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->countryName:Ljava/lang/String;

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    .line 37
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->realCountryCode:Ljava/lang/String;

    .line 56
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mCheckAppUpgrade:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    .line 57
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mDeviceInfoLoader:Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->bNeedClearDisclaimer:Z

    .line 664
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 108
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    const/4 v1, 0x1

    invoke-virtual {p1, v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->mappingClass(Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 109
    return-void
.end method

.method private checkCSC(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 366
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mCSC:Ljava/lang/String;

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isSameStr(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 371
    :goto_0
    return-void

    .line 369
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mCSC:Ljava/lang/String;

    .line 370
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->bNeedUpdate:Z

    goto :goto_0
.end method

.method private checkMCC(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 336
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isValidString(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isValidString(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 337
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isSameStr(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 355
    :cond_0
    :goto_0
    return-void

    .line 340
    :cond_1
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    .line 341
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->bNeedUpdate:Z

    .line 342
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->disclaimerNeedClear()V

    goto :goto_0

    .line 345
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isValidString(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isValidString(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 348
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isValidString(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isValidString(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 349
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    .line 350
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->bNeedUpdate:Z

    goto :goto_0

    .line 353
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->disclaimerNeedClear()V

    .line 354
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->bNeedUpdate:Z

    goto :goto_0
.end method

.method private checkMNC(Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/MNCInfo;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 374
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MNC:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/MNCInfo;->getMNC()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isSameStr(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 389
    :cond_0
    :goto_0
    return-void

    .line 380
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/MNCInfo;->isDefaultValue()Z

    move-result v0

    if-nez v0, :cond_2

    .line 381
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/MNCInfo;->getMNC()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MNC:Ljava/lang/String;

    .line 382
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->bNeedUpdate:Z

    goto :goto_0

    .line 384
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MNC:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isValidString(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 385
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/MNCInfo;->getMNC()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MNC:Ljava/lang/String;

    .line 386
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->bNeedUpdate:Z

    goto :goto_0
.end method

.method private disclaimerNeedClear()V
    .locals 1

    .prologue
    .line 358
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->bNeedClearDisclaimer:Z

    .line 359
    return-void
.end method

.method private getChinaHubUrl()Ljava/lang/String;
    .locals 4

    .prologue
    .line 187
    const-string v0, "%s://%s-%s.%s.%s/%s.%s"

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "http"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "cn"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "ms"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "samsungapps"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "com"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "ods"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "as"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getDeviceCSC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mDeviceInfoLoader:Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->readCSC()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getDeviceMCC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mDeviceInfoLoader:Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->readMCC()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getDeviceMNC()Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/MNCInfo;
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mDeviceInfoLoader:Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->readMNC()Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/MNCInfo;

    move-result-object v0

    return-object v0
.end method

.method private getInt(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 575
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 577
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getString(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 583
    :try_start_0
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 586
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v0, ""

    goto :goto_0
.end method

.method private huburl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 168
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getSAConfig()Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getHubUrl()Ljava/lang/String;

    move-result-object v0

    .line 169
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 183
    :goto_0
    return-object v0

    .line 179
    :cond_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CSC;->isChinaVendor()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 180
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getChinaHubUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 183
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->hubURL:Ljava/lang/String;

    goto :goto_0
.end method

.method private isSameStr(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 304
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isValidString(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isValidString(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 305
    invoke-virtual {p1, p2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 307
    :cond_0
    return v0
.end method

.method private isValidString(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 300
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private loadFromDevice()V
    .locals 3

    .prologue
    .line 323
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->bNeedClearDisclaimer:Z

    .line 324
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mDeviceInfoLoader:Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    if-eqz v0, :cond_0

    .line 325
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getDeviceMCC()Ljava/lang/String;

    move-result-object v0

    .line 326
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getDeviceMNC()Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/MNCInfo;

    move-result-object v1

    .line 327
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getDeviceCSC()Ljava/lang/String;

    move-result-object v2

    .line 329
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->checkMCC(Ljava/lang/String;)V

    .line 330
    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->checkMNC(Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/MNCInfo;)V

    .line 331
    invoke-direct {p0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->checkCSC(Ljava/lang/String;)V

    .line 333
    :cond_0
    return-void
.end method

.method private setFreeStore(I)V
    .locals 1

    .prologue
    .line 472
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getCheckAppUpgradeResult()Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    move-result-object v0

    iput p1, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->freeStoreClsf:I

    .line 473
    iput p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->freeStoreClsf:I

    .line 474
    return-void
.end method


# virtual methods
.method public ableToUseGlobalCreditCard()Z
    .locals 1

    .prologue
    .line 601
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMCC()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 603
    sparse-switch v0, :sswitch_data_0

    .line 608
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 606
    :sswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 603
    nop

    :sswitch_data_0
    .sparse-switch
        0x1c2 -> :sswitch_0
        0x1cc -> :sswitch_0
    .end sparse-switch
.end method

.method public addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 683
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    if-eqz v0, :cond_0

    .line 684
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 686
    :cond_0
    return-void
.end method

.method public clearContainer()V
    .locals 0

    .prologue
    .line 692
    return-void
.end method

.method public clearUpdateField()V
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->bNeedUpdate:Z

    .line 123
    return-void
.end method

.method public closeMap()V
    .locals 3

    .prologue
    .line 674
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    if-eqz v0, :cond_0

    .line 675
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, p0, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;->mappingClass(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 677
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 678
    return-void
.end method

.method public createURL()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 633
    const-string v0, "%s://%s-%s.%s.%s/%s.%s"

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "http"

    aput-object v2, v1, v4

    const-string v2, "hub"

    aput-object v2, v1, v5

    const-string v2, "odc"

    aput-object v2, v1, v6

    const-string v2, "samsungapps"

    aput-object v2, v1, v7

    const-string v2, "com"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "ods"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "as"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->hubURL:Ljava/lang/String;

    .line 643
    const-string v0, "%s://%s-%s.%s.%s/%s.%s"

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "http"

    aput-object v2, v1, v4

    const-string v2, "qa"

    aput-object v2, v1, v5

    const-string v2, "odc"

    aput-object v2, v1, v6

    const-string v2, "samsungapps"

    aput-object v2, v1, v7

    const-string v2, "com"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "ods"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "as"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mQAURL:Ljava/lang/String;

    .line 644
    return-void
.end method

.method public findString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 703
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCSC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mCSC:Ljava/lang/String;

    return-object v0
.end method

.method public getCheckAppUpgradeResult()Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;
    .locals 1

    .prologue
    .line 535
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mCheckAppUpgrade:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    if-nez v0, :cond_0

    .line 536
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mCheckAppUpgrade:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    .line 538
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mCheckAppUpgrade:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    return-object v0
.end method

.method public getCountryCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 652
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->countryCode:Ljava/lang/String;

    return-object v0
.end method

.method public getCountryNameFromSIM(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 519
    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 520
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v1

    .line 523
    const/4 v2, 0x5

    if-ne v1, v2, :cond_2

    .line 524
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v0

    .line 528
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 529
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CountryNameGetter;->findISOCountryNameByMCC(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 531
    :cond_1
    :goto_0
    return-object v0

    .line 526
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CountryNameGetter;->findISOCountryNameByMCC(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getCurrencyUnitDivision()Z
    .locals 1

    .prologue
    .line 724
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->currencyUnitDivision:Z

    return v0
.end method

.method public getCurrencyUnitHasPenny()Z
    .locals 1

    .prologue
    .line 719
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->currencyUnitHasPenny:Z

    return v0
.end method

.method public getCurrencyUnitPrecedes()Z
    .locals 1

    .prologue
    .line 714
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->currencyUnitPrecedes:Z

    return v0
.end method

.method public getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;
    .locals 1

    .prologue
    .line 570
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mDataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    return-object v0
.end method

.method public getDefaultLocaleString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 515
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDeviceInfoLoader()Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;
    .locals 1

    .prologue
    .line 565
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mDeviceInfoLoader:Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    return-object v0
.end method

.method public getFormattedPrice(DLjava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 403
    double-to-int v0, p1

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 405
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->currencyUnitHasPenny:Z

    if-eqz v1, :cond_0

    .line 408
    :try_start_0
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "0.00"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 409
    invoke-virtual {v0, p1, p2}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 414
    :cond_0
    :goto_0
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->currencyUnitDivision:Z

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x3

    if-le v1, v2, :cond_2

    .line 416
    :try_start_1
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "#,###"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 417
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->currencyUnitHasPenny:Z

    if-eqz v1, :cond_1

    .line 419
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "#,##0.00"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 421
    :cond_1
    invoke-virtual {v0, p1, p2}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 426
    :cond_2
    :goto_1
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->currencyUnitPrecedes:Z

    if-eqz v1, :cond_3

    .line 427
    const-string v1, "%s%s"

    new-array v2, v5, [Ljava/lang/Object;

    aput-object p3, v2, v3

    aput-object v0, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 432
    :goto_2
    return-object v0

    .line 411
    :catch_0
    move-exception v0

    const-string v0, " "

    goto :goto_0

    .line 423
    :catch_1
    move-exception v0

    const-string v0, " "

    goto :goto_1

    .line 430
    :cond_3
    const-string v1, "%s%s"

    new-array v2, v5, [Ljava/lang/Object;

    aput-object v0, v2, v3

    aput-object p3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public getHubURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 199
    sget-boolean v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->_useFixedURL:Z

    if-eqz v0, :cond_0

    .line 200
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->_FixedHubURL:Ljava/lang/String;

    .line 202
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->huburl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getLang()Ljava/lang/String;
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    const-string v1, "404"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    const-string v1, "405"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 128
    :cond_0
    :try_start_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->lang:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 133
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->lang:Ljava/lang/String;

    return-object v0

    .line 130
    :catch_0
    move-exception v0

    const-string v0, "EN"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->lang:Ljava/lang/String;

    goto :goto_0
.end method

.method public getMCC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    return-object v0
.end method

.method public getMNC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MNC:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MNC:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 142
    :cond_0
    const-string v0, "00"

    .line 144
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MNC:Ljava/lang/String;

    goto :goto_0
.end method

.method public getQaURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mQAURL:Ljava/lang/String;

    return-object v0
.end method

.method public getRealCountryCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 728
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->realCountryCode:Ljava/lang/String;

    return-object v0
.end method

.method public getSingUpLimitationAge()I
    .locals 1

    .prologue
    .line 489
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMCC()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 491
    sparse-switch v0, :sswitch_data_0

    .line 511
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 493
    :sswitch_0
    const/16 v0, 0xd

    goto :goto_0

    .line 496
    :sswitch_1
    const/16 v0, 0xe

    goto :goto_0

    .line 509
    :sswitch_2
    const/16 v0, 0x12

    goto :goto_0

    .line 491
    :sswitch_data_0
    .sparse-switch
        0xca -> :sswitch_2
        0xdb -> :sswitch_2
        0xe2 -> :sswitch_2
        0xe6 -> :sswitch_2
        0xe7 -> :sswitch_2
        0xee -> :sswitch_2
        0xf0 -> :sswitch_2
        0xf2 -> :sswitch_2
        0xf4 -> :sswitch_2
        0xf6 -> :sswitch_1
        0x110 -> :sswitch_2
        0x11c -> :sswitch_2
        0x136 -> :sswitch_0
        0x1b0 -> :sswitch_2
        0x1c2 -> :sswitch_1
    .end sparse-switch
.end method

.method public getURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    sget-boolean v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->_useFixedURL:Z

    if-eqz v0, :cond_0

    .line 157
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->_FixedURL:Ljava/lang/String;

    .line 164
    :goto_0
    return-object v0

    .line 159
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->countryUrl:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->countryUrl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->countryUrl:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->countryUrl:Ljava/lang/String;

    goto :goto_0

    .line 163
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->huburl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 164
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->huburl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public isBrazill()Z
    .locals 2

    .prologue
    .line 266
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    const/16 v1, 0x2d4

    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 267
    const/4 v0, 0x1

    .line 269
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isChina()Z
    .locals 2

    .prologue
    .line 596
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMCC()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 597
    const/16 v1, 0x1cc

    if-eq v0, v1, :cond_0

    const/16 v1, 0x1cd

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFaceBookSupported()Z
    .locals 1

    .prologue
    .line 619
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mCheckAppUpgrade:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    if-eqz v0, :cond_0

    .line 620
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mCheckAppUpgrade:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    iget v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->snsVal:I

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->snsVal:I

    .line 622
    :cond_0
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->snsVal:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFrance()Z
    .locals 2

    .prologue
    .line 273
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    const/16 v1, 0xd0

    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 274
    const/4 v0, 0x1

    .line 276
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFreeStore()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 465
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mCheckAppUpgrade:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mCheckAppUpgrade:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    iget v2, v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->freeStoreClsf:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_2

    .line 466
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mCheckAppUpgrade:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    iget v2, v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->freeStoreClsf:I

    if-nez v2, :cond_1

    .line 468
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 466
    goto :goto_0

    .line 468
    :cond_2
    iget v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->freeStoreClsf:I

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public isGermany()Z
    .locals 2

    .prologue
    .line 280
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    const/16 v1, 0x106

    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 281
    const/4 v0, 0x1

    .line 283
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isIndia()Z
    .locals 2

    .prologue
    .line 210
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    const/16 v1, 0x194

    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 211
    const/4 v0, 0x1

    .line 213
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isIran()Z
    .locals 2

    .prologue
    .line 244
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    const/16 v1, 0x1b0

    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 245
    const/4 v0, 0x1

    .line 247
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isKorea()Z
    .locals 2

    .prologue
    .line 237
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    const/16 v1, 0x1c2

    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 238
    const/4 v0, 0x1

    .line 240
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNotUsingGooglePlayCountry()Z
    .locals 2

    .prologue
    .line 227
    const-string v0, "460"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "461"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "417"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "634"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "432"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "414"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "368"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 229
    :cond_0
    const/4 v0, 0x1

    .line 232
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPaidTabFirst()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 477
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mCheckAppUpgrade:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mCheckAppUpgrade:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    iget v2, v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->freeTabClsf:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_2

    .line 478
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mCheckAppUpgrade:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    iget v2, v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->freeTabClsf:I

    if-ne v2, v0, :cond_1

    .line 480
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 478
    goto :goto_0

    .line 480
    :cond_2
    iget v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->freeTabClsf:I

    if-eq v2, v0, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public isPoland()Z
    .locals 2

    .prologue
    .line 258
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMCC()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 259
    const/16 v1, 0x104

    if-ne v0, v1, :cond_0

    .line 260
    const/4 v0, 0x1

    .line 262
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTestStore()Z
    .locals 2

    .prologue
    .line 251
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    const/16 v1, 0x3e8

    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 252
    const/4 v0, 0x1

    .line 254
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTurkey()Z
    .locals 2

    .prologue
    .line 288
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    const/16 v1, 0x11e

    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 289
    const/4 v0, 0x1

    .line 291
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTwiterSupported()Z
    .locals 1

    .prologue
    .line 612
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mCheckAppUpgrade:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    if-eqz v0, :cond_0

    .line 613
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mCheckAppUpgrade:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    iget v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->snsVal:I

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->snsVal:I

    .line 615
    :cond_0
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->snsVal:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isUS()Z
    .locals 2

    .prologue
    .line 217
    const-string v0, "310"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "311"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "312"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "313"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "314"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "315"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "316"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 219
    :cond_0
    const/4 v0, 0x1

    .line 222
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isUkraine()Z
    .locals 2

    .prologue
    .line 591
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMCC()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 592
    const/16 v1, 0xff

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isUncStore()Z
    .locals 2

    .prologue
    .line 295
    const-string v0, "PPC"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->countryCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isYouTubeSupported()Z
    .locals 1

    .prologue
    .line 626
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mCheckAppUpgrade:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    if-eqz v0, :cond_0

    .line 627
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mCheckAppUpgrade:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    iget v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->snsVal:I

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->snsVal:I

    .line 629
    :cond_0
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->snsVal:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load()V
    .locals 0

    .prologue
    .line 117
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->loadFromDB()V

    .line 118
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->loadFromDevice()V

    .line 119
    return-void
.end method

.method protected loadFromDB()V
    .locals 1

    .prologue
    .line 451
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mDataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->getLastMCC()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    .line 452
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mDataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->getLastMNC()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MNC:Ljava/lang/String;

    .line 453
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mDataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->getLastCSC()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mCSC:Ljava/lang/String;

    .line 454
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mDataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->getURL()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->countryUrl:Ljava/lang/String;

    .line 455
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mDataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->getUnitDivision()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->currencyUnitDivision:Z

    .line 456
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mDataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->getCurrencyUnitHasPenny()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->currencyUnitHasPenny:Z

    .line 457
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mDataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->getCurrencyUnitPrecedes()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->currencyUnitPrecedes:Z

    .line 458
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mDataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->getFreeStoreClsf()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->setFreeStore(I)V

    .line 459
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mDataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->getCountryCode()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->countryCode:Ljava/lang/String;

    .line 460
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mDataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->getRealCountryCode()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->realCountryCode:Ljava/lang/String;

    .line 461
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mDataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->getFreeTabClsf()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->freeTabClsf:I

    .line 462
    return-void
.end method

.method public needClearDisclaimer()Z
    .locals 1

    .prologue
    .line 362
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->bNeedClearDisclaimer:Z

    return v0
.end method

.method public needUpdate()Z
    .locals 1

    .prologue
    .line 392
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isTestMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 393
    const/4 v0, 0x0

    .line 399
    :goto_0
    return v0

    .line 396
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->countryUrl:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->countryUrl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->realCountryCode:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->realCountryCode:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 397
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 399
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->bNeedUpdate:Z

    goto :goto_0
.end method

.method public openMap()V
    .locals 1

    .prologue
    .line 668
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->map:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 669
    return-void
.end method

.method public requestContryInfo(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V
    .locals 3

    .prologue
    .line 542
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    const/4 v1, 0x1

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/u;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/u;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->countrySearch(ZLcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 558
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 559
    return-void
.end method

.method public save()V
    .locals 2

    .prologue
    .line 436
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mDataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MCC:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeLastMCC(Ljava/lang/String;)V

    .line 437
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mDataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->MNC:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeLastMNC(Ljava/lang/String;)V

    .line 438
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mDataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mCSC:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeLastCSC(Ljava/lang/String;)V

    .line 439
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mDataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->countryUrl:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeURL(Ljava/lang/String;)V

    .line 440
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mDataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->getURL()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->countryUrl:Ljava/lang/String;

    .line 441
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mDataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->currencyUnitDivision:Z

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeUnitDivision(Z)V

    .line 442
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mDataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->currencyUnitHasPenny:Z

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeCurrencyUnitHasPenny(Z)V

    .line 443
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mDataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->currencyUnitPrecedes:Z

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeCurrencyUnitPrecedes(Z)V

    .line 444
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mDataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isFreeStore()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeFreeStore(I)V

    .line 445
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mDataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->countryCode:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeCountryCode(Ljava/lang/String;)V

    .line 446
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mDataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->realCountryCode:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->writeRealCountryCode(Ljava/lang/String;)V

    .line 447
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mDataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->freeTabClsf:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;->setFreeTabClsf(I)V

    .line 448
    return-void

    .line 444
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setCheckAppUpgradeResult(Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;)V
    .locals 0

    .prologue
    .line 657
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mCheckAppUpgrade:Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    .line 658
    return-void
.end method

.method public setLoaders(Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;)V
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mDeviceInfoLoader:Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    .line 113
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->mDataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    .line 114
    return-void
.end method

.method public setNeedClearDisclaimer(Z)V
    .locals 0

    .prologue
    .line 661
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->bNeedClearDisclaimer:Z

    .line 662
    return-void
.end method

.method public setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 710
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 697
    const/4 v0, 0x0

    return v0
.end method
