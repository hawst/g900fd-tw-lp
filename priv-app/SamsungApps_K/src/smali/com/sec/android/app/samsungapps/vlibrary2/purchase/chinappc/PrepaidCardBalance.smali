.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PrepaidCardBalance;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# instance fields
.field private _Balance:D

.field private _CardNumber:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private validString(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 59
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 11

    .prologue
    const/4 v1, 0x2

    const/4 v4, 0x0

    const/4 v2, 0x1

    .line 28
    const-string v0, "balanceInfo"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 30
    invoke-direct {p0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PrepaidCardBalance;->validString(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 32
    const-string v0, ";"

    invoke-virtual {p2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 33
    array-length v0, v5

    if-nez v0, :cond_1

    .line 55
    :cond_0
    return-void

    .line 38
    :cond_1
    array-length v0, v5

    if-le v0, v2, :cond_2

    move v0, v1

    :goto_0
    move v3, v4

    .line 43
    :goto_1
    if-ge v3, v0, :cond_0

    .line 45
    aget-object v6, v5, v3

    const-string v7, "@"

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 46
    array-length v7, v6

    if-lt v7, v1, :cond_0

    .line 50
    iget-wide v7, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PrepaidCardBalance;->_Balance:D

    aget-object v9, v6, v2

    invoke-static {v9}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v9

    add-double/2addr v7, v9

    iput-wide v7, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PrepaidCardBalance;->_Balance:D

    .line 51
    aget-object v6, v6, v4

    iput-object v6, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PrepaidCardBalance;->_CardNumber:Ljava/lang/String;

    .line 43
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public clearContainer()V
    .locals 0

    .prologue
    .line 65
    return-void
.end method

.method public closeMap()V
    .locals 0

    .prologue
    .line 24
    return-void
.end method

.method public findString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    return-object v0
.end method

.method public getBalance()D
    .locals 2

    .prologue
    .line 79
    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PrepaidCardBalance;->_Balance:D

    return-wide v0
.end method

.method public getCardNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PrepaidCardBalance;->_CardNumber:Ljava/lang/String;

    return-object v0
.end method

.method public openMap()V
    .locals 2

    .prologue
    .line 18
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PrepaidCardBalance;->_Balance:D

    .line 19
    return-void
.end method

.method public setBalance(D)V
    .locals 0

    .prologue
    .line 89
    iput-wide p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PrepaidCardBalance;->_Balance:D

    .line 90
    return-void
.end method

.method public setCardNumber(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/chinappc/PrepaidCardBalance;->_CardNumber:Ljava/lang/String;

    .line 95
    return-void
.end method

.method public setResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 101
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    return v0
.end method
