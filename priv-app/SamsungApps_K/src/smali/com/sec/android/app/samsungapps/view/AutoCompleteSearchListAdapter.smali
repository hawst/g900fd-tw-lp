.class public Lcom/sec/android/app/samsungapps/view/AutoCompleteSearchListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ProGuard"


# instance fields
.field protected final mContext:Landroid/content/Context;

.field protected final mKeyword:Ljava/lang/String;

.field protected final mVi:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 26
    const v0, 0x7f04005c

    invoke-direct {p0, p1, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 27
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/AutoCompleteSearchListAdapter;->mContext:Landroid/content/Context;

    .line 28
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/view/AutoCompleteSearchListAdapter;->mKeyword:Ljava/lang/String;

    .line 29
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/view/AutoCompleteSearchListAdapter;->mVi:Landroid/view/LayoutInflater;

    .line 30
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 35
    if-nez p2, :cond_0

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/AutoCompleteSearchListAdapter;->mVi:Landroid/view/LayoutInflater;

    const v1, 0x7f04005c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 40
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/view/AutoCompleteSearchListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResult;

    .line 45
    const v1, 0x7f0c0160

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0c0187

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iget-object v3, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResult;->isUserSearchHistory:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_2

    const v3, 0x7f02010e

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/AutoCompleteSearchListAdapter;->mKeyword:Ljava/lang/String;

    if-nez v2, :cond_3

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResult;->keyword:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 47
    :cond_1
    :goto_1
    return-object p2

    .line 45
    :cond_2
    const v3, 0x7f02010f

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/view/AutoCompleteSearchListAdapter;->mKeyword:Ljava/lang/String;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AutoCompleteSearchResult;->keyword:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->textToHighlight(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
