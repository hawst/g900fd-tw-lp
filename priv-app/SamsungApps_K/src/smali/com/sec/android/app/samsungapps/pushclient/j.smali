.class final Lcom/sec/android/app/samsungapps/pushclient/j;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field final synthetic b:Lcom/sec/android/app/samsungapps/pushclient/PushService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/pushclient/PushService;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V
    .locals 0

    .prologue
    .line 1402
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/pushclient/j;->b:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    iput-object p2, p0, Lcom/sec/android/app/samsungapps/pushclient/j;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCommandResult(Z)V
    .locals 3

    .prologue
    .line 1406
    const-string v0, "doProductDetail()"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1410
    if-eqz p1, :cond_2

    .line 1420
    const-string v0, "######## ProductDetail result ######## "

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1421
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "productID \t\t: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/j;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->productID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1422
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "productName \t\t: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/j;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->productName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1423
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "orderID \t\t\t: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/j;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->orderID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1424
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "contentsSize \t\t: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/j;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    iget-wide v1, v1, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->contentsSize:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1425
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "orderItemSeq \t\t: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/j;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->orderItemSeq:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1426
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "packageName \t\t: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/j;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->GUID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1427
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "version \t\t\t: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/j;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->version:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1428
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "contentURL \t\t: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/j;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->contentURL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1429
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "realContentsSize \t: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/j;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    iget-wide v1, v1, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->realContentsSize:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1430
    const-string v0, "################################### "

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1436
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/j;->b:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    .line 1437
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/j;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->GUID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->isPackageInstalled(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 1442
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/j;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->GUID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->getPackageInfo(Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 1445
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/j;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->version:Ljava/lang/String;

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->compareVersion(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;

    move-result-object v0

    .line 1448
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;->leftlower:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;->same:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager$VersionCompareResult;

    if-ne v0, v1, :cond_1

    .line 1451
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Exist Package : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/j;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->GUID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->c(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1453
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/j;->b:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/j;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->productID:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->d(Lcom/sec/android/app/samsungapps/pushclient/PushService;Ljava/lang/String;)V

    .line 1474
    :goto_0
    return-void

    .line 1464
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/j;->b:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/pushclient/j;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->productID:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->d(Lcom/sec/android/app/samsungapps/pushclient/PushService;Ljava/lang/String;)V

    goto :goto_0

    .line 1471
    :cond_2
    const-string v0, "ERROR() : doProductDetail()"

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/pushclient/b;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1472
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/pushclient/j;->b:Lcom/sec/android/app/samsungapps/pushclient/PushService;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/pushclient/PushService;->k(Lcom/sec/android/app/samsungapps/pushclient/PushService;)V

    goto :goto_0
.end method
