.class public Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;


# instance fields
.field private final MAX_BIG_BANNER_WIDTH:I

.field private final MAX_SMALL_BANNER_WIDTH:I

.field private final MIN_BIG_BANNER_WIDTH:I

.field private final MIN_SMALL_BANNER_WIDTH:I

.field private final ORG_BIG_BANNER_HEIGHT:I

.field private final ORG_BIG_BANNER_WIDTH:I

.field private final ORG_SMALL_BANNER_HEIGHT:I

.field private final ORG_SMALL_BANNER_WIDTH:I

.field _BigbannerHeight:I

.field _BigbannerWidth:I

.field _Context:Landroid/content/Context;

.field _SmallbannerHeight:I

.field _SmallbannerWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/16 v1, 0xf0

    const/16 v5, 0x2d0

    const/16 v4, 0x168

    const/16 v3, 0x140

    const/16 v2, 0xa0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const/16 v0, 0x438

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;->ORG_BIG_BANNER_WIDTH:I

    .line 13
    const/16 v0, 0x288

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;->ORG_BIG_BANNER_HEIGHT:I

    .line 14
    iput v5, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;->MAX_BIG_BANNER_WIDTH:I

    .line 15
    iput v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;->MIN_BIG_BANNER_WIDTH:I

    .line 16
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;->ORG_SMALL_BANNER_WIDTH:I

    .line 17
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;->ORG_SMALL_BANNER_HEIGHT:I

    .line 18
    iput v4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;->MAX_SMALL_BANNER_WIDTH:I

    .line 19
    iput v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;->MIN_SMALL_BANNER_WIDTH:I

    .line 22
    const/16 v0, 0x36c

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;->_BigbannerWidth:I

    .line 23
    const/16 v0, 0x288

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;->_BigbannerHeight:I

    .line 24
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;->_SmallbannerWidth:I

    .line 25
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;->_SmallbannerHeight:I

    .line 32
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;->_Context:Landroid/content/Context;

    .line 34
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isTablet()Z

    move-result v0

    if-nez v0, :cond_2

    .line 35
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;->_Context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;->_Context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 37
    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;->_BigbannerWidth:I

    .line 38
    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;->_BigbannerWidth:I

    if-le v1, v5, :cond_3

    .line 39
    iput v5, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;->_BigbannerWidth:I

    .line 43
    :cond_0
    :goto_0
    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;->_BigbannerWidth:I

    mul-int/lit16 v1, v1, 0x288

    div-int/lit16 v1, v1, 0x438

    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;->_BigbannerHeight:I

    .line 45
    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;->_SmallbannerWidth:I

    .line 46
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;->_SmallbannerWidth:I

    if-le v0, v4, :cond_4

    .line 47
    iput v4, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;->_SmallbannerWidth:I

    .line 51
    :cond_1
    :goto_1
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;->_SmallbannerWidth:I

    mul-int/lit16 v0, v0, 0xf0

    div-int/lit16 v0, v0, 0x1f4

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;->_SmallbannerHeight:I

    .line 53
    :cond_2
    return-void

    .line 40
    :cond_3
    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;->_BigbannerWidth:I

    if-ge v1, v3, :cond_0

    .line 41
    iput v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;->_BigbannerWidth:I

    goto :goto_0

    .line 48
    :cond_4
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;->_SmallbannerWidth:I

    if-ge v0, v2, :cond_1

    .line 49
    iput v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;->_SmallbannerWidth:I

    goto :goto_1
.end method


# virtual methods
.method public getHeight()I
    .locals 1

    .prologue
    .line 68
    const/16 v0, 0x87

    return v0
.end method

.method public getHeight(Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;)I
    .locals 2

    .prologue
    .line 105
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/a;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 126
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;->getHeight()I

    move-result v0

    .line 130
    :goto_0
    return v0

    .line 110
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;->getHeight()I

    move-result v0

    goto :goto_0

    .line 114
    :pswitch_1
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;->_BigbannerHeight:I

    goto :goto_0

    .line 118
    :pswitch_2
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;->_SmallbannerHeight:I

    goto :goto_0

    .line 122
    :pswitch_3
    const/16 v0, 0x320

    .line 123
    goto :goto_0

    .line 105
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 58
    const/16 v0, 0x87

    return v0
.end method

.method public getWidth(Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;)I
    .locals 2

    .prologue
    .line 74
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/a;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/ImageResolutionType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 95
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;->getWidth()I

    move-result v0

    .line 99
    :goto_0
    return v0

    .line 79
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;->getWidth()I

    move-result v0

    goto :goto_0

    .line 83
    :pswitch_1
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;->_BigbannerWidth:I

    goto :goto_0

    .line 87
    :pswitch_2
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;->_SmallbannerWidth:I

    goto :goto_0

    .line 91
    :pswitch_3
    const/16 v0, 0x1e0

    .line 92
    goto :goto_0

    .line 74
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
