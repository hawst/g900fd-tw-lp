.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/SearchContentQuery;
.super Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;
.source "ProGuard"


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->Search:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)V

    .line 9
    return-void
.end method


# virtual methods
.method protected createContentListWithID()Lcom/sec/android/app/samsungapps/vlibrary/doc/subclass/ContentListWithID;
    .locals 4

    .prologue
    .line 14
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/subclass/ContentListWithID;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/SearchContentList;

    invoke-direct {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SearchContentList;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SearchContentQuery;->mSortOrder:Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SortOrder;->getno()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SearchContentQuery;->getPaidTypeFilter()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/subclass/ContentListWithID;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;II)V

    return-object v0
.end method

.method public getSearchContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/SearchContentList;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SearchContentQuery;->getContentList()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentList;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/SearchContentList;

    return-object v0
.end method
