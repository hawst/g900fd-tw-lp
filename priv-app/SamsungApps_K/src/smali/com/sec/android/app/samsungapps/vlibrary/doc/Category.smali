.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;
.super Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;
.source "ProGuard"


# instance fields
.field private _bCompleted:Z

.field private _bSelected:Z

.field private _mSibling:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

.field public categoryDescription:Ljava/lang/String;

.field public categoryID:Ljava/lang/String;

.field public categoryImgUrl:Ljava/lang/String;

.field public categoryLevel:I

.field public categoryName:Ljava/lang/String;

.field public categoryShopID:Ljava/lang/String;

.field public categorySortString:Ljava/lang/String;

.field mCategoryList:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

.field public newCategoryYn:Ljava/lang/String;

.field public sortOptionYN:Z

.field public specialCategoryYn:Ljava/lang/String;

.field public subLevelCategory:I

.field public upLevelCategoryID:Ljava/lang/String;

.field public upLevelCategoryName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 80
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;-><init>()V

    .line 6
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryID:Ljava/lang/String;

    .line 7
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryName:Ljava/lang/String;

    .line 11
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryShopID:Ljava/lang/String;

    .line 15
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->upLevelCategoryID:Ljava/lang/String;

    .line 17
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->upLevelCategoryName:Ljava/lang/String;

    .line 22
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->subLevelCategory:I

    .line 30
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categorySortString:Ljava/lang/String;

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryImgUrl:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryDescription:Ljava/lang/String;

    .line 33
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->newCategoryYn:Ljava/lang/String;

    .line 34
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->specialCategoryYn:Ljava/lang/String;

    .line 36
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->_bCompleted:Z

    .line 37
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->mCategoryList:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    .line 39
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->_mSibling:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    .line 81
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    const/4 v1, 0x1

    invoke-static {p1, v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->mappingClass(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;Ljava/lang/Class;Ljava/lang/Object;Z)Z

    .line 82
    const-string v0, "sortOptionYN"

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 83
    if-eqz v0, :cond_0

    .line 84
    const-string v1, "Y"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->sortOptionYN:Z

    .line 86
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 73
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;-><init>()V

    .line 6
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryID:Ljava/lang/String;

    .line 7
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryName:Ljava/lang/String;

    .line 11
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryShopID:Ljava/lang/String;

    .line 15
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->upLevelCategoryID:Ljava/lang/String;

    .line 17
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->upLevelCategoryName:Ljava/lang/String;

    .line 22
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->subLevelCategory:I

    .line 30
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categorySortString:Ljava/lang/String;

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryImgUrl:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryDescription:Ljava/lang/String;

    .line 33
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->newCategoryYn:Ljava/lang/String;

    .line 34
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->specialCategoryYn:Ljava/lang/String;

    .line 36
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->_bCompleted:Z

    .line 37
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->mCategoryList:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    .line 39
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->_mSibling:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    .line 74
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryID:Ljava/lang/String;

    .line 75
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryName:Ljava/lang/String;

    .line 76
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryImgUrl:Ljava/lang/String;

    .line 77
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 46
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ObjectCreatedFromMap;-><init>()V

    .line 6
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryID:Ljava/lang/String;

    .line 7
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryName:Ljava/lang/String;

    .line 11
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryShopID:Ljava/lang/String;

    .line 15
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->upLevelCategoryID:Ljava/lang/String;

    .line 17
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->upLevelCategoryName:Ljava/lang/String;

    .line 22
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->subLevelCategory:I

    .line 30
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categorySortString:Ljava/lang/String;

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryImgUrl:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryDescription:Ljava/lang/String;

    .line 33
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->newCategoryYn:Ljava/lang/String;

    .line 34
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->specialCategoryYn:Ljava/lang/String;

    .line 36
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->_bCompleted:Z

    .line 37
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->mCategoryList:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    .line 39
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->_mSibling:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    .line 47
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryID:Ljava/lang/String;

    .line 48
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryName:Ljava/lang/String;

    .line 49
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryShopID:Ljava/lang/String;

    .line 50
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->upLevelCategoryID:Ljava/lang/String;

    .line 51
    iput p5, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->subLevelCategory:I

    .line 52
    iput p6, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryLevel:I

    .line 53
    iput-object p7, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categorySortString:Ljava/lang/String;

    .line 54
    iput-object p8, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryImgUrl:Ljava/lang/String;

    .line 55
    iput-object p9, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryDescription:Ljava/lang/String;

    .line 56
    iput-object p10, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->newCategoryYn:Ljava/lang/String;

    .line 57
    return-void
.end method


# virtual methods
.method public clearSel()V
    .locals 1

    .prologue
    .line 198
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->_bSelected:Z

    .line 199
    return-void
.end method

.method public compareID(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 159
    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryID:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 162
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryID:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public compareParentID(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 166
    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->upLevelCategoryID:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 169
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->upLevelCategoryID:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public findCategory(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->mCategoryList:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    if-eqz v0, :cond_1

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->mCategoryList:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->findCategory(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    move-result-object v0

    .line 142
    if-eqz v0, :cond_1

    move-object p0, v0

    .line 149
    :cond_0
    :goto_0
    return-object p0

    .line 146
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->compareID(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 149
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public findCategoryNeedingChild()Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;
    .locals 1

    .prologue
    .line 240
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->isTerminalNode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 241
    const/4 p0, 0x0

    .line 246
    :cond_0
    :goto_0
    return-object p0

    .line 243
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->mCategoryList:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->mCategoryList:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->findCategoryNeedingChild()Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    move-result-object p0

    goto :goto_0
.end method

.method public get(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->mCategoryList:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->mCategoryList:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    .line 194
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getChild(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->mCategoryList:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->mCategoryList:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;

    .line 123
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getChildCount()I
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->mCategoryList:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    if-nez v0, :cond_0

    .line 133
    const/4 v0, 0x0

    .line 135
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->mCategoryList:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getChildList()Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->mCategoryList:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    return-object v0
.end method

.method public getImgURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryImgUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryName:Ljava/lang/String;

    return-object v0
.end method

.method public getNewCategoryYn()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->newCategoryYn:Ljava/lang/String;

    return-object v0
.end method

.method public getSibling()Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->_mSibling:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    return-object v0
.end method

.method public isComplete()Z
    .locals 1

    .prologue
    .line 174
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->needSubCategory()Z

    move-result v0

    if-nez v0, :cond_0

    .line 175
    const/4 v0, 0x1

    .line 177
    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->_bCompleted:Z

    goto :goto_0
.end method

.method public isKNOXCategory()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 61
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryName:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryName:Ljava/lang/String;

    const-string v2, "Samsung KNOX"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 69
    :cond_0
    :goto_0
    return v0

    .line 65
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryName:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryName:Ljava/lang/String;

    const-string v2, "KNOX"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 69
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSelected()Z
    .locals 1

    .prologue
    .line 203
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->_bSelected:Z

    return v0
.end method

.method public isTerminalNode()Z
    .locals 1

    .prologue
    .line 187
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->needSubCategory()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public needSubCategory()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 115
    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->subLevelCategory:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public selCategory(Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 207
    if-ne p0, p1, :cond_0

    .line 209
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->_bSelected:Z

    .line 210
    invoke-virtual {p2, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->addSelectedItem(Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;)V

    .line 222
    :goto_0
    return v0

    .line 213
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->mCategoryList:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    if-eqz v1, :cond_1

    .line 215
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->mCategoryList:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;->selCategory(Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 217
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->_bSelected:Z

    .line 218
    invoke-virtual {p2, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->addSelectedItem(Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;)V

    goto :goto_0

    .line 222
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setChild(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->mCategoryList:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    .line 91
    return-void
.end method

.method public setComplete(Z)V
    .locals 0

    .prologue
    .line 182
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->_bCompleted:Z

    .line 183
    return-void
.end method

.method public setNewCategoryYn(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 226
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->newCategoryYn:Ljava/lang/String;

    .line 227
    return-void
.end method

.method public setSibling(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;)V
    .locals 0

    .prologue
    .line 231
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->_mSibling:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryList;

    .line 232
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Category;->categoryName:Ljava/lang/String;

    return-object v0
.end method
