.class public Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/CPreloadUpdateManagerFactory;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManagerFactory;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerFactory;

.field private b:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/PreloadAppUpdaterFactory;

.field private c:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckManagerFactory;

.field private d:Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;

.field private e:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SettingsProvider;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckManagerFactory;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerFactory;Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/PreloadAppUpdaterFactory;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SettingsProvider;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/CPreloadUpdateManagerFactory;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerFactory;

    .line 25
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/CPreloadUpdateManagerFactory;->b:Lcom/sec/android/app/samsungapps/vlibrary3/preloadappupdater/PreloadAppUpdaterFactory;

    .line 26
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/CPreloadUpdateManagerFactory;->c:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckManagerFactory;

    .line 27
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/CPreloadUpdateManagerFactory;->d:Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;

    .line 28
    iput-object p5, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/CPreloadUpdateManagerFactory;->e:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SettingsProvider;

    .line 29
    return-void
.end method


# virtual methods
.method public createPreloadUpdateManager(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;
    .locals 7

    .prologue
    .line 34
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/CPreloadUpdateManagerFactory;->a:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerFactory;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/CPreloadUpdateManagerFactory;->c:Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckManagerFactory;

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/CPreloadUpdateManagerFactory;->d:Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;

    iget-object v6, p0, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/CPreloadUpdateManagerFactory;->e:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SettingsProvider;

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/vlibrary3/preloadupdate/PreloadUpdateManager;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/trigger/AutoUpdateTriggerFactory;Lcom/sec/android/app/samsungapps/vlibrary3/fotaagreement/FOTAAgreementCheckManagerFactory;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SettingsProvider;)V

    .line 36
    return-object v0
.end method
