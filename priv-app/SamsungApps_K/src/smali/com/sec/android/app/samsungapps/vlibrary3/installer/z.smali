.class final Lcom/sec/android/app/samsungapps/vlibrary3/installer/z;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/z;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onForegroundInstalling()V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/z;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/z;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;->onForegroundInstalling()V

    .line 91
    :cond_0
    return-void
.end method

.method public final onInstallFailed()V
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/z;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/z;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;->onInstallFailed()V

    .line 98
    :cond_0
    return-void
.end method

.method public final onInstallFailed(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/z;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/z;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;->onInstallFailed(Ljava/lang/String;)V

    .line 84
    :cond_0
    return-void
.end method

.method public final onInstallSuccess()V
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/z;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/installer/z;->a:Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;->a(Lcom/sec/android/app/samsungapps/vlibrary3/installer/SigCheckerForInstaller;)Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/installer/Installer$IInstallManagerObserver;->onInstallSuccess()V

    .line 105
    :cond_0
    return-void
.end method
