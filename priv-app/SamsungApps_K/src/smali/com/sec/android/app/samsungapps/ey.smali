.class final Lcom/sec/android/app/samsungapps/ey;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/initialize/deeplink/DeepLinkInitializeContext$IDeepLinkInitObserver;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/DeepLinker;

.field final synthetic b:Lcom/sec/android/app/samsungapps/Main;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/Main;Lcom/sec/android/app/samsungapps/DeepLinker;)V
    .locals 0

    .prologue
    .line 284
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/ey;->b:Lcom/sec/android/app/samsungapps/Main;

    iput-object p2, p0, Lcom/sec/android/app/samsungapps/ey;->a:Lcom/sec/android/app/samsungapps/DeepLinker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDeeplinkInitFailed(Z)V
    .locals 2

    .prologue
    .line 297
    if-eqz p1, :cond_0

    .line 298
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ey;->b:Lcom/sec/android/app/samsungapps/Main;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ey;->a:Lcom/sec/android/app/samsungapps/DeepLinker;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/Main;->a(Lcom/sec/android/app/samsungapps/Main;Lcom/sec/android/app/samsungapps/DeepLinker;)V

    .line 302
    :goto_0
    return-void

    .line 301
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ey;->b:Lcom/sec/android/app/samsungapps/Main;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/Main;->finish()V

    goto :goto_0
.end method

.method public final onDeeplinkInitSuccess()V
    .locals 1

    .prologue
    .line 288
    sget-object v0, Lcom/sec/android/app/samsungapps/CommonActivity;->mCurActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->setInitialized(Landroid/content/Context;)V

    .line 289
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ey;->a:Lcom/sec/android/app/samsungapps/DeepLinker;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/DeepLinker;->runDeepLink()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ey;->b:Lcom/sec/android/app/samsungapps/Main;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/Main;->finish()V

    .line 293
    :cond_0
    return-void
.end method
