.class public Lcom/sec/android/app/samsungapps/helper/NetworkDownloadSizeLimitConditionalPopup;
.super Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;
.source "ProGuard"


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;-><init>(Landroid/content/Context;)V

    .line 24
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/helper/NetworkDownloadSizeLimitConditionalPopup;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 25
    return-void
.end method

.method private a()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/helper/NetworkDownloadSizeLimitConditionalPopup;->_Context:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDeviceInfoLoader()Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    move-result-object v2

    .line 96
    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->getNetwrokType()I

    move-result v0

    .line 98
    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->isWibroConnected()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->isWIFIConnected()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 100
    :cond_0
    const/4 v0, 0x4

    .line 104
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/helper/NetworkDownloadSizeLimitConditionalPopup;->_Context:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCheckAppUpgradeResult()Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    move-result-object v2

    .line 105
    if-nez v2, :cond_2

    .line 107
    const-string v0, "error"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->err(Ljava/lang/String;)V

    .line 132
    :goto_0
    return v1

    .line 111
    :cond_2
    packed-switch v0, :pswitch_data_0

    move v0, v1

    :cond_3
    :goto_1
    move v1, v0

    .line 132
    goto :goto_0

    .line 114
    :pswitch_0
    iget v0, v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->_2gLimit:I

    goto :goto_1

    .line 117
    :pswitch_1
    iget v0, v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->_25gLimit:I

    goto :goto_1

    .line 120
    :pswitch_2
    iget v0, v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->_3gLimit:I

    goto :goto_1

    .line 123
    :pswitch_3
    iget v0, v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->_4gLimit:I

    .line 124
    if-nez v0, :cond_3

    .line 125
    iget v0, v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->_3gLimit:I

    goto :goto_1

    .line 129
    :pswitch_4
    iget v0, v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;->_wifiLimit:I

    goto :goto_1

    .line 111
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public getLimitSize()J
    .locals 2

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/helper/NetworkDownloadSizeLimitConditionalPopup;->a()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method protected matchCondition()Z
    .locals 3

    .prologue
    .line 70
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/helper/NetworkDownloadSizeLimitConditionalPopup;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getRealContentSize()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/helper/NetworkDownloadSizeLimitConditionalPopup;->overDownloadLimitation(J)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    const/4 v0, 0x1

    .line 79
    :goto_0
    return v0

    .line 75
    :catch_0
    move-exception v0

    .line 77
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "NetworkDownloadSizeLimitConditionalPopup::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 79
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onInvokePopup(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 31
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/helper/NetworkDownloadSizeLimitConditionalPopup;->getLimitSize()J

    move-result-wide v0

    .line 34
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/helper/NetworkDownloadSizeLimitConditionalPopup;->_Context:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/helper/NetworkDownloadSizeLimitConditionalPopup;->_Context:Landroid/content/Context;

    const v4, 0x7f080159

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/android/app/samsungapps/uiutil/StringUtil;->replaceChineseString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 39
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/helper/NetworkDownloadSizeLimitConditionalPopup;->_Context:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->createInfoDialog(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    move-result-object v0

    .line 41
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/helper/NetworkDownloadSizeLimitConditionalPopup;->_Context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08023d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/helper/j;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/helper/j;-><init>(Lcom/sec/android/app/samsungapps/helper/NetworkDownloadSizeLimitConditionalPopup;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setNegativeButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 49
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/helper/NetworkDownloadSizeLimitConditionalPopup;->_Context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0802ef

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/helper/k;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/helper/k;-><init>(Lcom/sec/android/app/samsungapps/helper/NetworkDownloadSizeLimitConditionalPopup;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setPositiveButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 56
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    move-result v0

    if-nez v0, :cond_0

    .line 57
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/helper/NetworkDownloadSizeLimitConditionalPopup;->userAgree(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/helper/NetworkDownloadSizeLimitConditionalPopup;->invokeCompleted()V

    .line 65
    return-void

    .line 59
    :catch_0
    move-exception v0

    .line 61
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "NetworkDownloadSizeLimitConditionalPopup::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 62
    invoke-virtual {p0, v6}, Lcom/sec/android/app/samsungapps/helper/NetworkDownloadSizeLimitConditionalPopup;->userAgree(Z)V

    goto :goto_0
.end method

.method public overDownloadLimitation(J)Z
    .locals 2

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/helper/NetworkDownloadSizeLimitConditionalPopup;->a()I

    move-result v0

    .line 85
    if-eqz v0, :cond_0

    mul-int/lit16 v0, v0, 0x400

    mul-int/lit16 v0, v0, 0x400

    int-to-long v0, v0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 87
    const/4 v0, 0x1

    .line 90
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
