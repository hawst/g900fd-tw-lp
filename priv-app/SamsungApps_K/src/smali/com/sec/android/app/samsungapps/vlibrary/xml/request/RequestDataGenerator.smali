.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestDataGenerator;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private data:Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestData;

.field private mContext:Landroid/content/Context;

.field private mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

.field private mRequestInfo:Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestInfo;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestInfo;Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestData;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestData;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestDataGenerator;->data:Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestData;

    .line 20
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestDataGenerator;->mContext:Landroid/content/Context;

    .line 21
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestDataGenerator;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    .line 22
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestDataGenerator;->mRequestInfo:Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestInfo;

    .line 23
    return-void
.end method

.method private setRequestHeader(Ljava/util/HashMap;Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestInfo;)V
    .locals 3

    .prologue
    .line 27
    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestInfo;->toMap()Ljava/util/HashMap;

    move-result-object v0

    .line 28
    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 30
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 32
    :cond_0
    return-void
.end method

.method private setSamsungProtocol(Ljava/util/HashMap;)V
    .locals 4

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestDataGenerator;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IBaseContextUtil;->getFakeModel(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 37
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestDataGenerator;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/IBaseContextUtil;->getGearOSVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 39
    const-string v2, "networkType"

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestDataGenerator;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getNetInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/NetworkInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetworkInfo;->getNetworkType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    const-string v2, "version2"

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestDataGenerator;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getPlatformKeyVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    const-string v2, "lang"

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestDataGenerator;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getLang()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    const-string v2, "openApiVersion"

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestDataGenerator;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getSamsungApps()Lcom/sec/android/app/samsungapps/vlibrary/doc/SamsungApps;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SamsungApps;->getOpenAPIVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    .line 46
    const-string v2, "deviceModel"

    invoke-virtual {p1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    :goto_0
    const-string v0, "mcc"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestDataGenerator;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMCC()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    const-string v0, "mnc"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestDataGenerator;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMNC()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    const-string v0, "csc"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestDataGenerator;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getCSC()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    const-string v0, "odcVersion"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestDataGenerator;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getSamsungApps()Lcom/sec/android/app/samsungapps/vlibrary/doc/SamsungApps;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SamsungApps;->getODCVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    const-string v0, "gosversion"

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    :cond_0
    const-string v0, "version"

    const-string v1, "4.0"

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    const-string v0, "filter"

    const-string v1, "1"

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    return-void

    .line 50
    :cond_1
    const-string v0, "deviceModel"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/request/RequestDataGenerator;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getModelName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
