.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodSelectable;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/primitives/selectable/ISelectable;


# instance fields
.field private _PaymentMethodSpec:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

.field private _bSelected:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodSelectable;->_PaymentMethodSpec:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    .line 12
    return-void
.end method


# virtual methods
.method public getID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodSelectable;->_PaymentMethodSpec:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPaymentMethodSpec()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodSelectable;->_PaymentMethodSpec:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    return-object v0
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x1

    return v0
.end method

.method public isSelected()Z
    .locals 1

    .prologue
    .line 23
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodSelectable;->_bSelected:Z

    return v0
.end method

.method public setSelect(Z)V
    .locals 0

    .prologue
    .line 28
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/PaymentMethodSelectable;->_bSelected:Z

    .line 29
    return-void
.end method
