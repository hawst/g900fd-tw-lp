.class public Lcom/sec/android/app/samsungapps/ConcreteViewCaller;
.super Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ViewCaller;
.source "ProGuard"


# instance fields
.field a:Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ViewCaller;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ConcreteViewCaller;->a:Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;

    return-void
.end method


# virtual methods
.method public dismissDialog()V
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConcreteViewCaller;->a:Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;

    if-eqz v0, :cond_0

    .line 31
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConcreteViewCaller;->a:Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;->hide()V

    .line 34
    :cond_0
    return-void
.end method

.method public showAddCommentView(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;)Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView;
    .locals 3

    .prologue
    .line 21
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/samsungapps/ReviewDetailActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 22
    const-string v1, "productid"

    iget-object v2, p2, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->productID:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 23
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 24
    const/4 v0, 0x0

    return-object v0
.end method

.method public showNotificationPopup(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;Z)Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalView;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x5

    const/4 v1, 0x0

    .line 40
    if-nez p2, :cond_1

    .line 93
    :cond_0
    return-object v8

    .line 45
    :cond_1
    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;->size()I

    move-result v3

    .line 46
    if-lez v3, :cond_0

    .line 48
    new-instance v4, Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;

    invoke-direct {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;-><init>()V

    invoke-virtual {p2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;->size()I

    move-result v5

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_3

    invoke-virtual {p2, v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->getNotificationType()I

    move-result v6

    if-ne v6, v7, :cond_2

    invoke-virtual {v4, v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;->add(ILjava/lang/Object;)V

    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    invoke-virtual {v4, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 49
    :cond_3
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;

    invoke-direct {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;-><init>()V

    .line 51
    :goto_2
    if-ge v1, v3, :cond_5

    .line 53
    invoke-virtual {v4, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    .line 54
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->checkUserAlreadyCheck()Z

    move-result v5

    if-nez v5, :cond_4

    .line 55
    invoke-virtual {v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;->add(Ljava/lang/Object;)Z

    .line 51
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 59
    :cond_5
    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 61
    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_3
    if-ltz v1, :cond_0

    .line 63
    invoke-virtual {v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;

    .line 65
    if-eqz v0, :cond_6

    .line 67
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->getNotificationType()I

    move-result v3

    if-ne v3, v7, :cond_7

    .line 69
    if-nez p3, :cond_6

    .line 71
    new-instance v3, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;

    invoke-direct {v3, p1, v0}, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;)V

    iput-object v3, p0, Lcom/sec/android/app/samsungapps/ConcreteViewCaller;->a:Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConcreteViewCaller;->a:Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;->show()Z

    .line 61
    :cond_6
    :goto_4
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_3

    .line 75
    :cond_7
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;->getNotificationType()I

    move-result v3

    const/4 v4, 0x6

    if-ne v3, v4, :cond_8

    .line 77
    if-eqz p3, :cond_6

    .line 79
    new-instance v3, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;

    invoke-direct {v3, p1, v0}, Lcom/sec/android/app/samsungapps/GiftCardNotificationDialogBuilder;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;)V

    iput-object v3, p0, Lcom/sec/android/app/samsungapps/ConcreteViewCaller;->a:Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConcreteViewCaller;->a:Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;->show()Z

    goto :goto_4

    .line 85
    :cond_8
    new-instance v3, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;

    invoke-direct {v3, p1, v0}, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/Notification;)V

    iput-object v3, p0, Lcom/sec/android/app/samsungapps/ConcreteViewCaller;->a:Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ConcreteViewCaller;->a:Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/NotificationDialogBuilder;->show()Z

    goto :goto_4
.end method
