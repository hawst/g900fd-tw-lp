.class public Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field public static final HOVERING_NONE:I = 0x0

.field public static final HOVERING_TOOLTIP:I = 0x1

.field public static final HOVERING_USER_CUSTOM:I = 0x3

.field public static final HOVERING_WIDGET_DEFAULT:I = 0x2

.field private static final SETTING_SYSTEM_FINGER_AIR_VIEW:Ljava/lang/String; = "finger_air_view"

.field private static final TAG:Ljava/lang/String; = "HoveringManager"


# instance fields
.field protected mContext:Landroid/content/Context;

.field protected mCustomView:Landroid/view/View;

.field protected mHoveringType:I

.field protected mViewToBeHovered:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mContext:Landroid/content/Context;

    .line 20
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    .line 21
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mCustomView:Landroid/view/View;

    .line 22
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mHoveringType:I

    .line 41
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mContext:Landroid/content/Context;

    .line 42
    iput p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mHoveringType:I

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mContext:Landroid/content/Context;

    .line 20
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    .line 21
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mCustomView:Landroid/view/View;

    .line 22
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mHoveringType:I

    .line 87
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mContext:Landroid/content/Context;

    .line 88
    iput p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mHoveringType:I

    .line 92
    :try_start_0
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mCustomView:Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 98
    :goto_0
    return-void

    .line 94
    :catch_0
    move-exception v0

    .line 96
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HoveringManagerConstuctor::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;ILandroid/view/View;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mContext:Landroid/content/Context;

    .line 20
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    .line 21
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mCustomView:Landroid/view/View;

    .line 22
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mHoveringType:I

    .line 53
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mContext:Landroid/content/Context;

    .line 54
    iput p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mHoveringType:I

    .line 55
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/View;I)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mContext:Landroid/content/Context;

    .line 20
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    .line 21
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mCustomView:Landroid/view/View;

    .line 22
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mHoveringType:I

    .line 66
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mContext:Landroid/content/Context;

    .line 67
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mHoveringType:I

    .line 68
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    .line 72
    :try_start_0
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mCustomView:Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 78
    :goto_0
    return-void

    .line 74
    :catch_0
    move-exception v0

    .line 76
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HoveringManagerConstuctor::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static isEnabledAirView(Landroid/content/ContentResolver;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 294
    .line 296
    if-nez p0, :cond_0

    .line 298
    const-string v0, "HoveringManagerisEnabledAirView::ContentResolver is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 313
    :goto_0
    return v1

    .line 304
    :cond_0
    :try_start_0
    const-string v2, "finger_air_view"

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 305
    if-ne v2, v0, :cond_1

    :goto_1
    move v1, v0

    .line 313
    goto :goto_0

    :cond_1
    move v0, v1

    .line 305
    goto :goto_1

    .line 307
    :catch_0
    move-exception v0

    .line 309
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "HoveringManagerisEnabledAirView::"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Error;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    move v0, v1

    goto :goto_1
.end method

.method public static removeHovering(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 279
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0}, Landroid/view/View;->setHoverPopupType(I)V
    :try_end_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    .line 285
    :goto_0
    return-void

    .line 281
    :catch_0
    move-exception v0

    .line 283
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HoveringManagerremoveHovering::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Error;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public getCustomView()Landroid/view/View;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mCustomView:Landroid/view/View;

    return-object v0
.end method

.method public isEnabledAirView()Z
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 324
    const-string v0, "HoveringManagerisEnabledAirView::Context is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 325
    const/4 v0, 0x0

    .line 328
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->isEnabledAirView(Landroid/content/ContentResolver;)Z

    move-result v0

    goto :goto_0
.end method

.method public setViewToBeHovered(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    .line 116
    return-void
.end method

.method public showAirView()V
    .locals 3

    .prologue
    .line 192
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mHoveringType:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setHoverPopupType(I)V

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mCustomView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setContent(Landroid/view/View;)V

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setGuideLineEnabled(Z)V

    .line 195
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setGuideLineFadeOffset(I)V

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    const/4 v1, 0x0

    const/16 v2, -0x14

    invoke-virtual {v0, v1, v2}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    const/16 v1, 0x3031

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V
    :try_end_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    .line 203
    :goto_0
    return-void

    .line 199
    :catch_0
    move-exception v0

    .line 201
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HoveringManagershowAirView::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Error;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public showAirView(III)V
    .locals 3

    .prologue
    .line 215
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mHoveringType:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setHoverPopupType(I)V

    .line 216
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mCustomView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setContent(Landroid/view/View;)V

    .line 217
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setGuideLineEnabled(Z)V

    .line 218
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setGuideLineFadeOffset(I)V

    .line 219
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V
    :try_end_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    .line 226
    :goto_0
    return-void

    .line 222
    :catch_0
    move-exception v0

    .line 224
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HoveringManagershowAirView::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Error;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public showAirView(IIIZ)V
    .locals 3

    .prologue
    .line 239
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mHoveringType:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setHoverPopupType(I)V

    .line 240
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mCustomView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setContent(Landroid/view/View;)V

    .line 241
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    invoke-virtual {v0, p4}, Landroid/widget/HoverPopupWindow;->setGuideLineEnabled(Z)V

    .line 242
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setGuideLineFadeOffset(I)V

    .line 243
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 244
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V
    :try_end_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    .line 250
    :goto_0
    return-void

    .line 246
    :catch_0
    move-exception v0

    .line 248
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HoveringManagershowAirView::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Error;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public showAirView(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 262
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mHoveringType:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setHoverPopupType(I)V

    .line 263
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    .line 269
    :goto_0
    return-void

    .line 265
    :catch_0
    move-exception v0

    .line 267
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HoveringManagershowAirView::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Error;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public showCustomPopupWindow()V
    .locals 3

    .prologue
    .line 126
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mHoveringType:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setHoverPopupType(I)V

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mCustomView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setContent(Landroid/view/View;)V

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setGuideLineEnabled(Z)V

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setGuideLineFadeOffset(I)V

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    const/4 v1, 0x0

    const/16 v2, -0x14

    invoke-virtual {v0, v1, v2}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    const/16 v1, 0x3031

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V
    :try_end_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    :goto_0
    return-void

    .line 133
    :catch_0
    move-exception v0

    .line 135
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HoveringManagershowCustomPopupWindow::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Error;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public showCustomPopupWindowWithOffset(III)V
    .locals 3

    .prologue
    .line 150
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mHoveringType:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setHoverPopupType(I)V

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mCustomView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setContent(Landroid/view/View;)V

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setGuideLineEnabled(Z)V

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setGuideLineFadeOffset(I)V

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V
    :try_end_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    .line 161
    :goto_0
    return-void

    .line 157
    :catch_0
    move-exception v0

    .line 159
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HoveringManagershowCustomPopupWindowWithOffset::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Error;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public showToolTip(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 174
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    iget v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mHoveringType:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setHoverPopupType(I)V

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    const/16 v1, 0x12c

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setHoverDetectTime(I)V

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    const/16 v1, 0x3035

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/hovering/HoveringManager;->mViewToBeHovered:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    .line 183
    :goto_0
    return-void

    .line 179
    :catch_0
    move-exception v0

    .line 181
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HoveringManagershowToolTip::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Error;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method
