.class public Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/view/InfLoadingState;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

.field private b:Landroid/widget/AbsListView;

.field private c:Landroid/content/Context;

.field private d:Lcom/sec/android/app/samsungapps/view/ViewList;

.field private e:Lcom/sec/android/app/samsungapps/view/ViewList;

.field private f:Lcom/sec/android/app/samsungapps/view/ViewList;

.field private g:Lcom/sec/android/app/samsungapps/view/ViewList;

.field private h:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/AbsListView;Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    .line 26
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->c:Landroid/content/Context;

    .line 44
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b:Landroid/widget/AbsListView;

    .line 45
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->c:Landroid/content/Context;

    .line 46
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->h:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;

    .line 47
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;)Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 236
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->f:Lcom/sec/android/app/samsungapps/view/ViewList;

    if-eqz v0, :cond_0

    .line 237
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->f:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 239
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->d:Lcom/sec/android/app/samsungapps/view/ViewList;

    if-eqz v0, :cond_1

    .line 240
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->d:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 242
    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;)Landroid/widget/AbsListView;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b:Landroid/widget/AbsListView;

    return-object v0
.end method

.method private b()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 245
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->g:Lcom/sec/android/app/samsungapps/view/ViewList;

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->g:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 248
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->d:Lcom/sec/android/app/samsungapps/view/ViewList;

    if-eqz v0, :cond_1

    .line 249
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->d:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 251
    :cond_1
    return-void
.end method


# virtual methods
.method public clearAdapterItem()V
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->clear()V

    .line 208
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->setLoadingEmptyView()V

    .line 210
    :cond_0
    return-void
.end method

.method protected empty()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 197
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    if-nez v1, :cond_1

    .line 200
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->getCount()I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public release()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    if-eqz v0, :cond_1

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b:Landroid/widget/AbsListView;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b:Landroid/widget/AbsListView;

    invoke-virtual {v1}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->setIndexOfFirstItem(I)V

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->release()V

    .line 104
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b:Landroid/widget/AbsListView;

    if-eqz v0, :cond_2

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b:Landroid/widget/AbsListView;

    invoke-virtual {v0, v2}, Landroid/widget/AbsListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b:Landroid/widget/AbsListView;

    invoke-virtual {v0, v2}, Landroid/widget/AbsListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 109
    :cond_2
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    .line 110
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->c:Landroid/content/Context;

    .line 111
    return-void
.end method

.method public setArrayAdapter(Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;)V
    .locals 2

    .prologue
    .line 71
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b:Landroid/widget/AbsListView;

    if-nez v0, :cond_1

    .line 90
    :cond_0
    :goto_0
    return-void

    .line 75
    :cond_1
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->setInfLoadingState(Lcom/sec/android/app/samsungapps/view/InfLoadingState;)V

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    new-instance v1, Lcom/sec/android/app/samsungapps/viewpager/n;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/viewpager/n;-><init>(Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->setDisplayHandler(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter$OnDisplayHandler;)V

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->c:Landroid/content/Context;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->c:Landroid/content/Context;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b:Landroid/widget/AbsListView;

    instance-of v0, v0, Landroid/widget/ListView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b:Landroid/widget/AbsListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b:Landroid/widget/AbsListView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->getIndexOfFirstItem()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setSelection(I)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b:Landroid/widget/AbsListView;

    new-instance v1, Lcom/sec/android/app/samsungapps/viewpager/o;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/viewpager/o;-><init>(Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;)V

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b:Landroid/widget/AbsListView;

    new-instance v1, Lcom/sec/android/app/samsungapps/viewpager/p;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/viewpager/p;-><init>(Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;)V

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b:Landroid/widget/AbsListView;

    instance-of v0, v0, Lcom/sec/android/app/samsungapps/widget/CustomGridView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b:Landroid/widget/AbsListView;

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/CustomGridView;

    new-instance v1, Lcom/sec/android/app/samsungapps/viewpager/q;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/viewpager/q;-><init>(Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/CustomGridView;->setOnScrollMoreCheckListener(Lcom/sec/android/app/samsungapps/widget/CustomGridView$OnScrollMoreCheckListener;)V

    .line 85
    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->isLoading()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->isSearchList()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->isCompleteEmptyList()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->setNoDataEmptyView()V

    goto :goto_0

    .line 83
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_1
.end method

.method public setEmptyViews(Lcom/sec/android/app/samsungapps/view/ViewList;Lcom/sec/android/app/samsungapps/view/ViewList;Lcom/sec/android/app/samsungapps/view/ViewList;Lcom/sec/android/app/samsungapps/view/ViewList;)V
    .locals 0

    .prologue
    .line 219
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->d:Lcom/sec/android/app/samsungapps/view/ViewList;

    .line 220
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->e:Lcom/sec/android/app/samsungapps/view/ViewList;

    .line 221
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->f:Lcom/sec/android/app/samsungapps/view/ViewList;

    .line 222
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->g:Lcom/sec/android/app/samsungapps/view/ViewList;

    .line 224
    return-void
.end method

.method public setListView(Landroid/widget/AbsListView;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b:Landroid/widget/AbsListView;

    .line 51
    return-void
.end method

.method public setLoadingEmptyView()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 335
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b:Landroid/widget/AbsListView;

    if-nez v0, :cond_1

    .line 348
    :cond_0
    :goto_0
    return-void

    .line 339
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->f:Lcom/sec/android/app/samsungapps/view/ViewList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->d:Lcom/sec/android/app/samsungapps/view/ViewList;

    if-eqz v0, :cond_0

    .line 340
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->e:Lcom/sec/android/app/samsungapps/view/ViewList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->e:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->d:Lcom/sec/android/app/samsungapps/view/ViewList;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->d:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 341
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b()V

    .line 342
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->empty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 343
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->f:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 344
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->d:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 346
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b:Landroid/widget/AbsListView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->f:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->getTopView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setEmptyView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public setNoDataEmptyView()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 256
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b:Landroid/widget/AbsListView;

    if-nez v0, :cond_0

    .line 291
    :goto_0
    return-void

    .line 261
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->e:Lcom/sec/android/app/samsungapps/view/ViewList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->d:Lcom/sec/android/app/samsungapps/view/ViewList;

    if-eqz v0, :cond_1

    .line 263
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->a()V

    .line 264
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b()V

    .line 266
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->empty()Z

    move-result v0

    if-ne v0, v2, :cond_1

    .line 268
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->isLoading()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->isSearchList()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->isCompleteEmptyList()Z

    move-result v0

    if-ne v0, v2, :cond_2

    .line 273
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->e:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 274
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->d:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 276
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b:Landroid/widget/AbsListView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->e:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->getTopView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setEmptyView(Landroid/view/View;)V

    .line 290
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b:Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->invalidate()V

    goto :goto_0

    .line 280
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->d:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 281
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->g:Lcom/sec/android/app/samsungapps/view/ViewList;

    if-eqz v0, :cond_1

    .line 283
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->g:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 284
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b:Landroid/widget/AbsListView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->g:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->getTopView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setEmptyView(Landroid/view/View;)V

    goto :goto_1
.end method

.method public setNoDataEmptyView(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 295
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b:Landroid/widget/AbsListView;

    if-nez v0, :cond_0

    .line 331
    :goto_0
    return-void

    .line 300
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->e:Lcom/sec/android/app/samsungapps/view/ViewList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->d:Lcom/sec/android/app/samsungapps/view/ViewList;

    if-eqz v0, :cond_2

    .line 302
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->a()V

    .line 303
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b()V

    .line 305
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->empty()Z

    move-result v0

    if-ne v0, v1, :cond_2

    .line 307
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->isLoading()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->isSearchList()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->a:Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/BaseContentArrayAdapter;->isCompleteEmptyList()Z

    move-result v0

    if-ne v0, v1, :cond_4

    .line 312
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->e:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ViewList;->getTopView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_1

    instance-of v1, v0, Landroid/widget/TextView;

    if-nez v1, :cond_3

    .line 313
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->e:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 314
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->d:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 316
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b:Landroid/widget/AbsListView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->e:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->getTopView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setEmptyView(Landroid/view/View;)V

    .line 330
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b:Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->invalidate()V

    goto :goto_0

    .line 312
    :cond_3
    sget-object v1, Lcom/sec/android/app/samsungapps/viewpager/r;->a:[I

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->h:Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController$ButtonType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_1

    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0801d0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0801c8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0801c9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :pswitch_3
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0801ca

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 318
    :cond_4
    if-nez p1, :cond_2

    .line 320
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->d:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 321
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->g:Lcom/sec/android/app/samsungapps/view/ViewList;

    if-eqz v0, :cond_2

    .line 323
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->g:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/view/ViewList;->setVisibility(I)V

    .line 324
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b:Landroid/widget/AbsListView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->g:Lcom/sec/android/app/samsungapps/view/ViewList;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/view/ViewList;->getTopView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setEmptyView(Landroid/view/View;)V

    goto :goto_2

    .line 312
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setSelection(I)V
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b:Landroid/widget/AbsListView;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b:Landroid/widget/AbsListView;

    invoke-virtual {v0, p1}, Landroid/widget/AbsListView;->setSelection(I)V

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b:Landroid/widget/AbsListView;

    new-instance v1, Lcom/sec/android/app/samsungapps/viewpager/m;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/viewpager/m;-><init>(Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;I)V

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->post(Ljava/lang/Runnable;)Z

    .line 65
    :cond_0
    return-void
.end method

.method public setVisibility(I)V
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b:Landroid/widget/AbsListView;

    if-eqz v0, :cond_0

    .line 214
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewpager/ListFragmentController;->b:Landroid/widget/AbsListView;

    invoke-virtual {v0, p1}, Landroid/widget/AbsListView;->setVisibility(I)V

    .line 216
    :cond_0
    return-void
.end method
