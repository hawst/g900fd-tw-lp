.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/EasyBuyPurchaseParser;
.super Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;
.source "ProGuard"


# instance fields
.field mDownloadableContent:Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;)V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/PostProcessor;-><init>()V

    .line 6
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/EasyBuyPurchaseParser;->mDownloadableContent:Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;

    .line 9
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/EasyBuyPurchaseParser;->mDownloadableContent:Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;

    .line 10
    return-void
.end method


# virtual methods
.method protected onCreateObject(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 3

    .prologue
    .line 15
    const-string v0, "orderID"

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 17
    if-eqz v0, :cond_0

    .line 19
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/EasyBuyPurchaseParser;->mDownloadableContent:Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;

    invoke-interface {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;->setOrderID(Ljava/lang/String;)V

    .line 21
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/EasyBuyPurchaseParser;->mDownloadableContent:Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;

    const-string v1, "contentsSize"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->getInt(Ljava/lang/String;I)I

    move-result v1

    int-to-long v1, v1

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;->setContentSize(J)V

    .line 22
    const-string v0, "downloadUri"

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 23
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/EasyBuyPurchaseParser;->mDownloadableContent:Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;

    invoke-interface {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/downloadmanager/DownloadInf;->setDownloadURI(Ljava/lang/String;)V

    .line 24
    return-void
.end method

.method protected onPostParseError()V
    .locals 0

    .prologue
    .line 30
    return-void
.end method

.method protected onPostParseResponseHeader(Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;)V
    .locals 0

    .prologue
    .line 35
    return-void
.end method
