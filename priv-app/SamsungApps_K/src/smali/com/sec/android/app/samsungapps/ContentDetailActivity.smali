.class public Lcom/sec/android/app/samsungapps/ContentDetailActivity;
.super Lcom/sec/android/app/samsungapps/ActionBarActivity;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer$IContentObserver;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail$IDetailView;
.implements Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
.implements Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;
.implements Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker$AbuseContentBlockerObserver;
.implements Lcom/sec/android/app/samsungapps/widget/interfaces/IRetryContentDetail;


# static fields
.field public static final WIDGET_MAINVIEW:I = 0x4

.field public static final WIDGET_OVERVIEW:I = 0x0

.field public static final WIDGET_RELATED:I = 0x2

.field public static final WIDGET_REVIEW:I = 0x1


# instance fields
.field private A:Z

.field private B:Z

.field private C:Z

.field private D:Z

.field private E:Z

.field private F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

.field private G:Ljava/lang/String;

.field private H:Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/ContentDetailCommandBuilder;

.field private I:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;

.field private J:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;

.field private K:Lcom/sec/android/app/samsungapps/vlibrary2/like/IContentLikeCommandBuilder;

.field private L:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;

.field private M:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

.field private N:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

.field private O:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

.field private P:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

.field private Q:Landroid/nfc/NfcAdapter;

.field private R:I

.field private S:J

.field private T:J

.field private U:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;

.field private V:Lcom/sec/android/app/samsungapps/bu;

.field private W:[I

.field private X:[I

.field private Y:[I

.field private Z:Z

.field a:Z

.field private aa:Z

.field private ab:Z

.field b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

.field c:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailWidgetHelper;

.field d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

.field e:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailMainWidgetClickListener;

.field f:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

.field g:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailOverviewWidgetClickListener;

.field h:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailReviewsWidgetClickListener;

.field i:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailExpertReviewWidgetClickListener;

.field j:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailUserReviewWidgetClickListener;

.field k:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;

.field l:Ljava/util/ArrayList;

.field m:Lcom/sec/android/app/samsungapps/test/ContentDetailOverviewFragment;

.field protected mDetailView:Landroid/view/View;

.field n:Lcom/sec/android/app/samsungapps/test/ContentDetailReviewsFragment;

.field o:Lcom/sec/android/app/samsungapps/test/ContentDetailRelatedFragment;

.field p:Lcom/sec/android/app/samsungapps/tobelog/LogData;

.field q:Z

.field r:Z

.field s:Z

.field public shareOption:I

.field t:Landroid/os/Handler;

.field u:Lcom/sec/android/app/samsungapps/LoadingDialog;

.field private v:Landroid/content/Context;

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x3

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 109
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;-><init>()V

    .line 114
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->v:Landroid/content/Context;

    .line 115
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->mDetailView:Landroid/view/View;

    .line 119
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a:Z

    .line 121
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->x:Z

    .line 122
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->y:Z

    .line 123
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->z:Z

    .line 124
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->A:Z

    .line 125
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->B:Z

    .line 126
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->C:Z

    .line 127
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->D:Z

    .line 128
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->E:Z

    .line 130
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 131
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->G:Ljava/lang/String;

    .line 133
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->H:Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/ContentDetailCommandBuilder;

    .line 146
    iput v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->shareOption:I

    .line 147
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->Q:Landroid/nfc/NfcAdapter;

    .line 148
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->R:I

    .line 158
    iput-wide v4, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->S:J

    .line 159
    iput-wide v4, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->T:J

    .line 173
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->k:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;

    .line 180
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->p:Lcom/sec/android/app/samsungapps/tobelog/LogData;

    .line 182
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->U:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;

    .line 184
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->V:Lcom/sec/android/app/samsungapps/bu;

    .line 186
    new-array v0, v3, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->W:[I

    .line 190
    new-array v0, v3, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->X:[I

    .line 194
    new-array v0, v3, [I

    fill-array-data v0, :array_2

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->Y:[I

    .line 334
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->q:Z

    .line 1401
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->r:Z

    .line 1402
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->s:Z

    .line 1506
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->ab:Z

    .line 1840
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->t:Landroid/os/Handler;

    .line 2318
    new-instance v0, Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/LoadingDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->u:Lcom/sec/android/app/samsungapps/LoadingDialog;

    .line 3029
    return-void

    .line 186
    :array_0
    .array-data 4
        0x7f0c00bb
        0x7f0c00be
        0x7f0c00c1
    .end array-data

    .line 190
    :array_1
    .array-data 4
        0x7f0c00bc
        0x7f0c00bf
        0x7f0c00c2
    .end array-data

    .line 194
    :array_2
    .array-data 4
        0x7f0c00bd
        0x7f0c00c0
        0x7f0c00c3
    .end array-data
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/ContentDetailActivity;J)J
    .locals 0

    .prologue
    .line 109
    iput-wide p1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->T:J

    return-wide p1
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/ContentDetailActivity;Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->M:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    return-object p1
.end method

.method private a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2364
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2365
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2366
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2367
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, p1, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 2369
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 2370
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 2372
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 2374
    iget-object v7, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v7, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 2375
    new-instance v8, Landroid/content/Intent;

    const-string v9, "android.intent.action.SEND"

    invoke-direct {v8, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2376
    const-string v9, "text/plain"

    invoke-virtual {v8, v9}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 2377
    const-string v9, "android.intent.extra.TEXT"

    invoke-virtual {v8, v9, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2378
    const-string v9, "android.intent.extra.SUBJECT"

    invoke-virtual {v8, v9, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2379
    new-instance v9, Landroid/content/ComponentName;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v9, v7, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v9}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 2380
    invoke-virtual {v8, v7}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2382
    const-string v0, ".chaton"

    invoke-virtual {v7, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, ".groupcast"

    invoke-virtual {v7, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    :goto_1
    if-eqz v0, :cond_2

    .line 2384
    invoke-interface {v3, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    move v0, v2

    .line 2382
    goto :goto_1

    .line 2386
    :cond_2
    const-string v0, ".allshare"

    invoke-virtual {v7, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    if-eqz v0, :cond_4

    .line 2388
    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    move v0, v2

    .line 2386
    goto :goto_2

    .line 2392
    :cond_4
    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2396
    :cond_5
    invoke-interface {v3, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2400
    invoke-interface {v3, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2405
    return-object v3
.end method

.method private a(Ljava/util/List;)Ljava/util/List;
    .locals 5

    .prologue
    .line 2428
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 2444
    :cond_0
    return-object p1

    .line 2432
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v0}, Ljava/text/Collator;->getInstance(Ljava/util/Locale;)Ljava/text/Collator;

    move-result-object v3

    .line 2434
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2435
    add-int/lit8 v0, v1, 0x1

    move v2, v0

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 2436
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 2437
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 2438
    invoke-virtual {v3, v4, v0}, Ljava/text/Collator;->equals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2439
    invoke-interface {p1, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2435
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2434
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 1606
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->l:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 1630
    :goto_0
    return-void

    .line 1609
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->getWidget()Lcom/sec/android/app/samsungapps/widget/CommonWidget;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->getWidgetState()I

    move-result v0

    .line 1610
    if-eqz v0, :cond_1

    .line 1611
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->g()V

    .line 1615
    :goto_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 1618
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d()V

    goto :goto_0

    .line 1613
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->l:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->V:Lcom/sec/android/app/samsungapps/bu;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/bu;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->getWidget()Lcom/sec/android/app/samsungapps/widget/CommonWidget;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->onWidgetSetViewState()V

    goto :goto_1

    .line 1623
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->e()V

    goto :goto_0

    .line 1628
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->f()V

    goto :goto_0

    .line 1615
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(IZ)V
    .locals 4

    .prologue
    .line 581
    const v0, 0x7f0c00a4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 582
    const v1, 0x7f0c00a6

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 583
    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    .line 584
    if-nez p2, :cond_0

    .line 585
    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 587
    :cond_0
    new-instance v1, Lcom/sec/android/app/samsungapps/aj;

    invoke-direct {v1, p0, p2, p1}, Lcom/sec/android/app/samsungapps/aj;-><init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;ZI)V

    const-wide/16 v2, 0x19

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/ScrollView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 601
    :cond_1
    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x1

    .line 1125
    if-nez p1, :cond_1

    .line 1127
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->finish()V

    .line 1174
    :cond_0
    :goto_0
    return-void

    .line 1131
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "cdcontainer"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 1132
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "fakeModel"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->G:Ljava/lang/String;

    .line 1134
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    if-eqz v0, :cond_0

    .line 1138
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->aa:Z

    .line 1140
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "deepLinkAppName"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1142
    if-eqz v0, :cond_2

    const-string v1, "GameHub"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v6, :cond_2

    .line 1144
    iput-boolean v6, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->aa:Z

    .line 1148
    :cond_2
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->Z:Z

    .line 1150
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1152
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "NOACCOUNT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->Z:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1159
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->addObserver(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer$IContentObserver;)V

    .line 1160
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->L:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;

    if-eqz v0, :cond_4

    .line 1161
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->L:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->onDestroy()V

    .line 1163
    :cond_4
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->v:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v5

    invoke-virtual {v5, p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstallChecker(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;)V

    invoke-direct {v0, p0, v1, v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/ButtonStaticStateManager;Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->L:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;

    .line 1164
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    invoke-static {}, Lcom/sec/android/app/samsungapps/initializer/Global;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1166
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->startInitialize()V

    goto/16 :goto_0

    .line 1154
    :catch_0
    move-exception v0

    .line 1155
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ContentDetailTest::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_1

    .line 1169
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    .line 1170
    invoke-direct {p0, v6}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a(Z)V

    .line 1173
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->initialized()V

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/ContentDetailActivity;IZ)V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a(IZ)V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/ContentDetailActivity;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V
    .locals 3

    .prologue
    .line 1276
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 1277
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->notiPopup()Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;

    move-result-object v2

    invoke-direct {v0, p0, v1, p0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail$IDetailView;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->I:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;

    .line 1279
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->I:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->createWishItemCommandBuilder(Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;)Lcom/sec/android/app/samsungapps/commands/WishItemCommandBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->J:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;

    .line 1280
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/like/ContentLikeCommandBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->I:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/like/ContentLikeCommandBuilder;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->K:Lcom/sec/android/app/samsungapps/vlibrary2/like/IContentLikeCommandBuilder;

    .line 1281
    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    const v3, 0x7f0c00a1

    const/4 v2, 0x0

    .line 1221
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->w:Z

    if-eqz v0, :cond_1

    .line 1272
    :cond_0
    :goto_0
    return-void

    .line 1226
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->v:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/view/restrictedappcheckutil/RestrictedAppCheckUtil;->isAdultBlur(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1228
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1235
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->I:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;

    if-eqz v0, :cond_0

    .line 1236
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->A:Z

    .line 1237
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->z:Z

    .line 1238
    if-eqz p1, :cond_2

    .line 1239
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->i()V

    .line 1241
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->I:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;->updateDetailViewAfterGetDetail()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/ak;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/ak;-><init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0

    .line 1232
    :cond_3
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Z
    .locals 1

    .prologue
    .line 109
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->w:Z

    return v0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/ContentDetailActivity;I)Z
    .locals 1

    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->b(I)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/ContentDetailActivity;Z)Z
    .locals 0

    .prologue
    .line 109
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->x:Z

    return p1
.end method

.method private b()I
    .locals 1

    .prologue
    .line 609
    const v0, 0x7f0c00a4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 610
    if-eqz v0, :cond_0

    .line 611
    invoke-virtual {v0}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v0

    .line 613
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/ContentDetailActivity;Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->N:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    return-object p1
.end method

.method private b(Ljava/util/List;)Ljava/util/List;
    .locals 6

    .prologue
    .line 2449
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 2469
    :cond_0
    :goto_0
    return-object p1

    .line 2454
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v0}, Ljava/text/Collator;->getInstance(Ljava/util/Locale;)Ljava/text/Collator;

    move-result-object v3

    .line 2458
    if-eqz p1, :cond_0

    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    add-int/lit8 v0, v2, 0x1

    move v1, v0

    :goto_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v4, v0}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v5, v0}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_2

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {p1, v2, v4}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p1, v1, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2462
    :catch_0
    move-exception v0

    const-string v0, "ContentDetailTest::doSortByAppNameOrder::NullPointerException is occured."

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 2464
    :catch_1
    move-exception v0

    .line 2466
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ContentDetailTest::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/ContentDetailActivity;I)V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a(I)V

    return-void
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/ContentDetailActivity;Z)V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a(Z)V

    return-void
.end method

.method private b(Z)V
    .locals 1

    .prologue
    .line 1393
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    if-eqz v0, :cond_0

    .line 1394
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->buttonListEnable(Z)V

    .line 1396
    :cond_0
    return-void
.end method

.method private b(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1638
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->w:Z

    if-ne v2, v0, :cond_2

    :cond_0
    move v0, v1

    .line 1641
    :cond_1
    :goto_0
    return v0

    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->V:Lcom/sec/android/app/samsungapps/bu;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/bu;->a()I

    move-result v2

    if-eq p1, v2, :cond_1

    move v0, v1

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Z
    .locals 1

    .prologue
    .line 109
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->x:Z

    return v0
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 619
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailWidgetHelper;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailWidgetHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailWidgetHelper;

    .line 620
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailWidgetHelper;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailWidgetHelper;->setContentDetailContainer(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    .line 622
    new-instance v0, Lcom/sec/android/app/samsungapps/ap;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/ap;-><init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->e:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailMainWidgetClickListener;

    .line 623
    new-instance v0, Lcom/sec/android/app/samsungapps/ar;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/ar;-><init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->g:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailOverviewWidgetClickListener;

    .line 624
    new-instance v0, Lcom/sec/android/app/samsungapps/as;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/as;-><init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailReviewsWidgetClickListener;

    .line 625
    new-instance v0, Lcom/sec/android/app/samsungapps/av;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/av;-><init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->i:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailExpertReviewWidgetClickListener;

    .line 626
    new-instance v0, Lcom/sec/android/app/samsungapps/ax;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/ax;-><init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->j:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailUserReviewWidgetClickListener;

    .line 627
    return-void
.end method

.method static synthetic c(Lcom/sec/android/app/samsungapps/ContentDetailActivity;Z)Z
    .locals 0

    .prologue
    .line 109
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->C:Z

    return p1
.end method

.method static synthetic d(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->O:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 705
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->m:Lcom/sec/android/app/samsungapps/test/ContentDetailOverviewFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/test/ContentDetailOverviewFragment;->getWidget()Lcom/sec/android/app/samsungapps/widget/CommonWidget;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;

    .line 708
    if-nez v0, :cond_1

    .line 754
    :cond_0
    :goto_0
    return-void

    .line 713
    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->getNeedLoad()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->getWidgetState()I

    move-result v1

    if-eq v1, v2, :cond_0

    .line 718
    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->setIRetryContentDetail(Lcom/sec/android/app/samsungapps/widget/interfaces/IRetryContentDetail;)V

    .line 720
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->b(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 722
    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->onWidgetSetViewState(I)V

    .line 724
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->I:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;

    if-eqz v1, :cond_0

    .line 726
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->I:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;->getDetailOverview()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/aw;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/app/samsungapps/aw;-><init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;)V

    invoke-virtual {v1, p0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0
.end method

.method static synthetic e(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->M:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    return-object v0
.end method

.method private e()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 757
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->n:Lcom/sec/android/app/samsungapps/test/ContentDetailReviewsFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/test/ContentDetailReviewsFragment;->getWidget()Lcom/sec/android/app/samsungapps/widget/CommonWidget;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;

    .line 760
    if-nez v0, :cond_1

    .line 841
    :cond_0
    :goto_0
    return-void

    .line 765
    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->getNeedLoad()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->getWidgetState()I

    move-result v1

    if-eq v1, v4, :cond_0

    .line 769
    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->setIRetryContentDetail(Lcom/sec/android/app/samsungapps/widget/interfaces/IRetryContentDetail;)V

    .line 771
    invoke-direct {p0, v4}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->b(I)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->isLoaded()Z

    move-result v1

    if-nez v1, :cond_2

    .line 773
    invoke-virtual {v0, v4}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->onWidgetSetViewState(I)V

    .line 776
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->O:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->O:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;->getProductID()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_4

    .line 777
    :cond_3
    new-instance v1, Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;-><init>(Ljava/lang/String;I)V

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->O:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    .line 780
    const-string v1, "create _CommentListCommandBuilder"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    .line 783
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->setWidgetData(Ljava/lang/Object;)V

    .line 785
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->O:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;->loadComment()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/bk;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/app/samsungapps/bk;-><init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;)V

    invoke-virtual {v1, p0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 813
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->P:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    if-nez v1, :cond_5

    .line 814
    new-instance v1, Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v4}, Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;-><init>(Ljava/lang/String;I)V

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->P:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    .line 817
    const-string v1, "create _CommentListCommandBuilder"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    .line 819
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->P:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;->loadExpertCommend()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/bo;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/app/samsungapps/bo;-><init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;)V

    invoke-virtual {v1, p0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0
.end method

.method static synthetic f(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->P:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    return-object v0
.end method

.method private f()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 845
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->o:Lcom/sec/android/app/samsungapps/test/ContentDetailRelatedFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/test/ContentDetailRelatedFragment;->getWidget()Lcom/sec/android/app/samsungapps/widget/CommonWidget;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;

    .line 848
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    if-nez v1, :cond_1

    .line 973
    :cond_0
    :goto_0
    return-void

    .line 853
    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->getNeedLoad()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->getWidgetState()I

    move-result v1

    if-eq v1, v3, :cond_0

    .line 858
    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->setIRetryContentDetail(Lcom/sec/android/app/samsungapps/widget/interfaces/IRetryContentDetail;)V

    .line 860
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->H:Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/ContentDetailCommandBuilder;

    if-nez v1, :cond_2

    .line 861
    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/ContentDetailCommandBuilder;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/ContentDetailCommandBuilder;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailMain;)V

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->H:Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/ContentDetailCommandBuilder;

    .line 865
    :cond_2
    invoke-direct {p0, v4}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->b(I)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->isLoaded()Z

    move-result v1

    if-nez v1, :cond_3

    .line 867
    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->onWidgetSetViewState(I)V

    .line 871
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 873
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->I:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;

    if-eqz v1, :cond_5

    .line 875
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->I:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;->getDetailRelated()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/bp;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/app/samsungapps/bp;-><init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;)V

    invoke-virtual {v1, p0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 907
    :cond_5
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->categoryID:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 909
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->H:Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/ContentDetailCommandBuilder;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/ContentDetailCommandBuilder;->getCategoryProductListCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/bq;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/app/samsungapps/bq;-><init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;)V

    invoke-virtual {v1, p0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 941
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->sellerID:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 943
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->H:Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/ContentDetailCommandBuilder;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/ContentDetailCommandBuilder;->getSellerProductListCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/br;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/app/samsungapps/br;-><init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;)V

    invoke-virtual {v1, p0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto/16 :goto_0

    .line 899
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    const/4 v2, 0x0

    invoke-virtual {v0, v4, v1, v2}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->setWidgetData(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 903
    invoke-virtual {v0, v4}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->loadWidget(I)V

    goto :goto_1

    .line 933
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getCategoryProductList()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;

    move-result-object v2

    invoke-virtual {v0, v3, v1, v2}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->setWidgetData(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 937
    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->loadWidget(I)V

    goto :goto_2

    .line 967
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getSellerProductList()Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentList;

    move-result-object v2

    invoke-virtual {v0, v5, v1, v2}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->setWidgetData(ILjava/lang/Object;Ljava/lang/Object;)V

    .line 971
    invoke-virtual {v0, v5}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailRelatedWidget;->loadWidget(I)V

    goto/16 :goto_0
.end method

.method static synthetic g(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->N:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    return-object v0
.end method

.method private g()V
    .locals 5

    .prologue
    .line 997
    const v0, 0x7f0c00aa

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 998
    const v0, 0x7f0c0082

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 999
    if-eqz v0, :cond_0

    .line 1000
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->v:Landroid/content/Context;

    const-string v3, "isa_samsungapps_icon_dummy"

    const-string v4, "drawable"

    invoke-static {v1, v3, v4}, Lcom/sec/android/app/samsungapps/uiutil/KnoxResourceManager;->findResource(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1003
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1004
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 1005
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->getWidget()Lcom/sec/android/app/samsungapps/widget/CommonWidget;

    move-result-object v0

    const v4, 0x7f0c00a6

    invoke-virtual {p0, v4}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v0, v4, v2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->setEmptyView(Landroid/view/View;Landroid/view/View;)V

    .line 1004
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1008
    :cond_1
    return-void
.end method

.method static synthetic h(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->L:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;

    return-object v0
.end method

.method private h()V
    .locals 1

    .prologue
    .line 1033
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    if-eqz v0, :cond_0

    .line 1034
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->refreshWidget()V

    .line 1036
    :cond_0
    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1102
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    if-eqz v0, :cond_0

    .line 1103
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    invoke-virtual {v0, v1, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->onWidgetViewState(IZ)V

    .line 1105
    :cond_0
    return-void
.end method

.method static synthetic i(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V
    .locals 4

    .prologue
    .line 109
    const/4 v0, 0x1

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->needToLoginToDownload()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    iget-boolean v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->Z:Z

    if-nez v1, :cond_0

    if-nez v0, :cond_1

    :cond_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/helper/DownloadHelpFacade;->getInstance()Lcom/sec/android/app/samsungapps/helper/DownloadHelpFacade;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/helper/DownloadHelpFacade;->createNoAccountBasedDownloadHelperFactory(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/helper/DownloadHelperFactory;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    iget-boolean v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->aa:Z

    invoke-interface {v0, p0, v1, v2}, Lcom/sec/android/app/samsungapps/helper/DownloadHelperFactory;->createDownloadCmdManager(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Z)Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->k:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;

    :goto_1
    return-void

    :catch_0
    move-exception v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ContentDetailTest::Exception::"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/sec/android/app/samsungapps/helper/DownloadHelpFacade;->getInstance()Lcom/sec/android/app/samsungapps/helper/DownloadHelpFacade;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/helper/DownloadHelpFacade;->createDownloadHelperFactory(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/helper/DownloadHelperFactory;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    iget-boolean v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->aa:Z

    invoke-interface {v0, p0, v1, v2}, Lcom/sec/android/app/samsungapps/helper/DownloadHelperFactory;->createDownloadCmdManager(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Z)Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->k:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;

    goto :goto_1
.end method

.method static synthetic j(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->U:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;

    return-object v0
.end method

.method private j()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1446
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->ab:Z

    if-ne v0, v2, :cond_0

    .line 1470
    :goto_0
    return-void

    .line 1449
    :cond_0
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->ab:Z

    .line 1451
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    if-eqz v0, :cond_1

    .line 1452
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->buttonListEnable(Z)V

    .line 1454
    :cond_1
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->createLogin(Z)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/am;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/am;-><init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0
.end method

.method private k()V
    .locals 3

    .prologue
    .line 1646
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->n:Lcom/sec/android/app/samsungapps/test/ContentDetailReviewsFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->n:Lcom/sec/android/app/samsungapps/test/ContentDetailReviewsFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/test/ContentDetailReviewsFragment;->getWidget()Lcom/sec/android/app/samsungapps/widget/CommonWidget;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1647
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->n:Lcom/sec/android/app/samsungapps/test/ContentDetailReviewsFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/test/ContentDetailReviewsFragment;->getWidget()Lcom/sec/android/app/samsungapps/widget/CommonWidget;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;

    .line 1649
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->O:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->P:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    .line 1663
    :cond_0
    :goto_0
    return-void

    .line 1652
    :cond_1
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->initReviewState(I)V

    .line 1653
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->setNeedLoad(Z)V

    .line 1654
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->O:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;->getCommentList()Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->clear()V

    .line 1655
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->P:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;->getCommentList()Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;->clear()V

    .line 1656
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1658
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->hasMyComment()Z

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->hasMyRating()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->updateWriteBtnState(ZZ)V

    .line 1661
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->e()V

    goto :goto_0
.end method

.method static synthetic k(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V
    .locals 1

    .prologue
    .line 109
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->b(Z)V

    return-void
.end method

.method private l()V
    .locals 7

    .prologue
    const/4 v4, 0x3

    const/4 v2, 0x0

    const v1, 0x7f0802a4

    const v5, 0xb0001

    const/4 v3, 0x1

    .line 2191
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2192
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x2

    new-array v4, v0, [I

    fill-array-data v4, :array_0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    .line 2314
    :goto_0
    return-void

    .line 2199
    :cond_0
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isUncStore()Z

    move-result v0

    if-eq v0, v3, :cond_1

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isTestMode()Z

    move-result v0

    if-ne v0, v3, :cond_2

    .line 2201
    :cond_1
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v4, v3, [I

    const/4 v0, 0x0

    const v6, 0xa0008

    aput v6, v4, v0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    goto :goto_0

    .line 2206
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isPurchased()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isPackageInstalled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isPrePostApp()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2208
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isLinkApp()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->G:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 2209
    :cond_4
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x2

    new-array v4, v0, [I

    fill-array-data v4, :array_1

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    goto :goto_0

    .line 2215
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->v:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->isKitkatMenuAvailable(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 2216
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2217
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x2

    new-array v4, v0, [I

    fill-array-data v4, :array_2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    goto/16 :goto_0

    .line 2223
    :cond_6
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v4, v4, [I

    fill-array-data v4, :array_3

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    goto/16 :goto_0

    .line 2231
    :cond_7
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x2

    new-array v4, v0, [I

    fill-array-data v4, :array_4

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    goto/16 :goto_0

    .line 2239
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getWishState()Lcom/sec/android/app/samsungapps/vlibrary/doc/WishState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishState;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 2240
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isLinkApp()Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->G:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 2241
    :cond_9
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v4, v4, [I

    fill-array-data v4, :array_5

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    goto/16 :goto_0

    .line 2248
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->v:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->isKitkatMenuAvailable(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 2249
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2250
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v4, v4, [I

    fill-array-data v4, :array_6

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    goto/16 :goto_0

    .line 2257
    :cond_b
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x4

    new-array v4, v0, [I

    fill-array-data v4, :array_7

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    goto/16 :goto_0

    .line 2266
    :cond_c
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v4, v4, [I

    fill-array-data v4, :array_8

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    goto/16 :goto_0

    .line 2275
    :cond_d
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isLinkApp()Z

    move-result v0

    if-nez v0, :cond_e

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->G:Ljava/lang/String;

    if-eqz v0, :cond_f

    .line 2276
    :cond_e
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v4, v4, [I

    fill-array-data v4, :array_9

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    goto/16 :goto_0

    .line 2283
    :cond_f
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->v:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->isKitkatMenuAvailable(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 2284
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-nez v0, :cond_10

    .line 2285
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x4

    new-array v4, v0, [I

    fill-array-data v4, :array_a

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    goto/16 :goto_0

    .line 2301
    :cond_10
    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v4, v4, [I

    fill-array-data v4, :array_b

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    goto/16 :goto_0

    .line 2192
    :array_0
    .array-data 4
        0xa0005
        0xa0008
    .end array-data

    .line 2209
    :array_1
    .array-data 4
        0xa0005
        0xa0008
    .end array-data

    .line 2217
    :array_2
    .array-data 4
        0xa0005
        0xa0008
    .end array-data

    .line 2223
    :array_3
    .array-data 4
        0xa0013
        0xa0005
        0xa0008
    .end array-data

    .line 2231
    :array_4
    .array-data 4
        0xa0005
        0xa0008
    .end array-data

    .line 2241
    :array_5
    .array-data 4
        0xa0016
        0xa0005
        0xa0008
    .end array-data

    .line 2250
    :array_6
    .array-data 4
        0xa0016
        0xa0005
        0xa0008
    .end array-data

    .line 2257
    :array_7
    .array-data 4
        0xa0013
        0xa0016
        0xa0005
        0xa0008
    .end array-data

    .line 2266
    :array_8
    .array-data 4
        0xa0016
        0xa0005
        0xa0008
    .end array-data

    .line 2276
    :array_9
    .array-data 4
        0xa0015
        0xa0005
        0xa0008
    .end array-data

    .line 2285
    :array_a
    .array-data 4
        0xa0013
        0xa0015
        0xa0005
        0xa0008
    .end array-data

    .line 2301
    :array_b
    .array-data 4
        0xa0015
        0xa0005
        0xa0008
    .end array-data
.end method

.method static synthetic l(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getWishState()Lcom/sec/android/app/samsungapps/vlibrary/doc/WishState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishState;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishState;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->hasWishListID()Z

    move-result v0

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->J:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->J:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;->removeWishCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/ao;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/ao;-><init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, v2}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->b(Z)V

    iput-boolean v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->ab:Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->J:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->J:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;->addToWishCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/an;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/an;-><init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->PRODUCT_DETAIL:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_WISH_LIST_BUTTON:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v2

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getVcategoryID()Ljava/lang/String;

    move-result-object v7

    move-object v5, v4

    move-object v6, v4

    move v8, v3

    invoke-static/range {v0 .. v8}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendNormalClickLog(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;Ljava/lang/String;ILcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;Ljava/lang/String;Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private m()V
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 2502
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->K:Lcom/sec/android/app/samsungapps/vlibrary2/like/IContentLikeCommandBuilder;

    if-nez v0, :cond_1

    .line 2537
    :cond_0
    :goto_0
    return-void

    .line 2505
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->K:Lcom/sec/android/app/samsungapps/vlibrary2/like/IContentLikeCommandBuilder;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/like/IContentLikeCommandBuilder;->registerContentLikeUnlike()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/ba;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/ba;-><init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 2526
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    if-eqz v0, :cond_0

    .line 2527
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->hasLike()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2528
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->PRODUCT_DETAIL:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_LIKE_BUTTON:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v2

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getVcategoryID()Ljava/lang/String;

    move-result-object v7

    move-object v5, v4

    move-object v6, v4

    move v8, v3

    invoke-static/range {v0 .. v8}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendNormalClickLog(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;Ljava/lang/String;ILcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;Ljava/lang/String;Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;Ljava/lang/String;I)V

    goto :goto_0

    .line 2532
    :cond_2
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->PRODUCT_DETAIL:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_DISLIKE_BUTTON:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v2

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getVcategoryID()Ljava/lang/String;

    move-result-object v7

    move-object v5, v4

    move-object v6, v4

    move v8, v3

    invoke-static/range {v0 .. v8}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendNormalClickLog(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;Ljava/lang/String;ILcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;Ljava/lang/String;Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method static synthetic m(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Z
    .locals 1

    .prologue
    .line 109
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->ab:Z

    return v0
.end method

.method static synthetic n(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->J:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;

    return-object v0
.end method

.method private n()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 3072
    sget-object v0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->mCurActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isUncStore()Z

    move-result v0

    if-eq v0, v1, :cond_0

    sget-object v0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->mCurActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v0

    if-ne v0, v1, :cond_1

    .line 3124
    :cond_0
    return-void

    .line 3077
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->W:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, v1, v0

    .line 3078
    sget-object v4, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->mCurActivity:Landroid/app/Activity;

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isIran()Z

    move-result v4

    if-eqz v4, :cond_3

    const v4, 0x7f0c00be

    if-ne v3, v4, :cond_3

    .line 3080
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 3081
    if-eqz v3, :cond_2

    .line 3082
    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 3077
    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3086
    :cond_3
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/samsungapps/bm;

    invoke-direct {v4, p0}, Lcom/sec/android/app/samsungapps/bm;-><init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method static synthetic o(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->v:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic p(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Z
    .locals 1

    .prologue
    .line 109
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->A:Z

    return v0
.end method

.method static synthetic q(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->j()V

    return-void
.end method

.method static synthetic r(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V
    .locals 3

    .prologue
    .line 109
    :try_start_0
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    const-string v1, "com.samsung.android.app.watchmanager"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;->getLaunchIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "packageName"

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getGUID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ContentDetailTest::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ContentDetailTest::Error::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Error;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic s(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->L:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->isExecutable()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->launch(Landroid/content/Context;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_BUY_DOWNLOAD_BUTTON:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getPreviousPage()Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->isLinkApp()Z

    move-result v3

    if-ne v3, v6, :cond_3

    sget-object v3, Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;->GOOGLE:Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;

    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->getEntryPoint()Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getVcategoryID()Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendClickPurchaseLog(Lcom/sec/android/app/samsungapps/tobelog/LogEvent;Lcom/sec/android/app/samsungapps/tobelog/LogPage;Ljava/lang/String;Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;Lcom/sec/android/app/samsungapps/tobelog/ServiceCode;Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isLinkApp()Z

    move-result v0

    if-ne v0, v6, :cond_4

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getVContentURL()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/LogSender;->sendDownloadClickLog(Lcom/sec/android/app/samsungapps/vlibrary/doc/AbstractContentDetail;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ActivityNotFoundException::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    sget-object v3, Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;->SAMSUNG:Lcom/sec/android/app/samsungapps/tobelog/LogBody$StoreType;

    goto :goto_1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_4
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->k:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->execute()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    goto/16 :goto_0

    :catch_2
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ContentDetailTest::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method static synthetic t(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/bu;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->V:Lcom/sec/android/app/samsungapps/bu;

    return-object v0
.end method

.method static synthetic u(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)I
    .locals 1

    .prologue
    .line 109
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->b()I

    move-result v0

    return v0
.end method

.method static synthetic v(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Z
    .locals 1

    .prologue
    .line 109
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->C:Z

    return v0
.end method

.method static synthetic w(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V
    .locals 4

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getGUID()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getVProductName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "http://www.samsungapps.com/appquery/appDetail.as?appId="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "&cntyTxt="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMCC()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "&equipID="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDeviceInfoLoader()Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->getModelName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.SEND"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "android.intent.category.DEFAULT"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "android.intent.extra.TEXT"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "android.intent.extra.SUBJECT"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "text/plain"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    invoke-direct {p0, v2, v0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    const v2, 0x7f080141

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "android.intent.extra.INITIAL_INTENTS"

    const/4 v0, 0x0

    new-array v0, v0, [Landroid/os/Parcelable;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/os/Parcelable;

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    const/high16 v0, 0x20000000

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method static synthetic x(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 109
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->S:J

    iget-wide v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->S:J

    iget-wide v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->T:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x7d0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->r:Z

    if-ne v0, v4, :cond_0

    iput-boolean v5, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->r:Z

    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->r:Z

    if-eq v0, v4, :cond_1

    iput-boolean v4, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->r:Z

    invoke-direct {p0, v5}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->b(Z)V

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->isLogedIn()Z

    move-result v0

    if-ne v0, v4, :cond_2

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->m()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/samsungapps/initializer/Global;->createLogin(Z)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/al;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/al;-><init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0
.end method

.method static synthetic y(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->n:Lcom/sec/android/app/samsungapps/test/ContentDetailReviewsFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->n:Lcom/sec/android/app/samsungapps/test/ContentDetailReviewsFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/test/ContentDetailReviewsFragment;->getWidget()Lcom/sec/android/app/samsungapps/widget/CommonWidget;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->n:Lcom/sec/android/app/samsungapps/test/ContentDetailReviewsFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/test/ContentDetailReviewsFragment;->getWidget()Lcom/sec/android/app/samsungapps/widget/CommonWidget;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->onWidgetSetViewState(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public _createMimeRecord(Ljava/lang/String;[B)Landroid/nfc/NdefRecord;
    .locals 4

    .prologue
    .line 2633
    const/4 v0, 0x0

    .line 2634
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x9

    if-lt v1, v2, :cond_0

    .line 2636
    const-string v0, "US-ASCII"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    .line 2647
    :goto_0
    new-instance v1, Landroid/nfc/NdefRecord;

    const/4 v2, 0x2

    const/4 v3, 0x0

    new-array v3, v3, [B

    invoke-direct {v1, v2, v0, v3, p2}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    .line 2650
    return-object v1

    .line 2641
    :cond_0
    :try_start_0
    const-string v1, "US-ASCII"

    invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 2642
    :catch_0
    move-exception v1

    .line 2643
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ContentDetailTest::UnsupportedEncodingException::"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method final a()V
    .locals 2

    .prologue
    .line 696
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    if-eqz v0, :cond_0

    .line 697
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailWidgetHelper;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->setWidgetData(Ljava/lang/Object;)V

    .line 698
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->e:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailMainWidgetClickListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->setMainWidgetListener(Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailMainWidgetClickListener;)V

    .line 699
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->setIRetryContentDetail(Lcom/sec/android/app/samsungapps/widget/interfaces/IRetryContentDetail;)V

    .line 701
    :cond_0
    return-void
.end method

.method public closeWindowByLogedOut()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2990
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->w:Z

    if-nez v0, :cond_0

    .line 2992
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a:Z

    if-nez v0, :cond_1

    .line 2994
    const-string v0, "closeWindowByLogedOut() called in the background!!"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    .line 2995
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->D:Z

    .line 3021
    :cond_0
    :goto_0
    return-void

    .line 2998
    :cond_1
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->D:Z

    .line 3001
    :try_start_0
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->v:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;)V

    .line 3002
    const v1, 0x7f0801ba

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 3003
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setCancelable(Z)V

    .line 3004
    const v1, 0x7f08013e

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 3007
    const v1, 0x7f0802ef

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/bl;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/bl;-><init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 3014
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 3015
    :catch_0
    move-exception v0

    .line 3017
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ContentDetailTest::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 3018
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->finish()V

    goto :goto_0
.end method

.method public closeWindowByRestrictedContent()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2964
    :try_start_0
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->v:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;)V

    .line 2965
    const v1, 0x7f0801ba

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 2966
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setCancelable(Z)V

    .line 2967
    const v1, 0x7f08013e

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 2973
    const v1, 0x7f0802ef

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/bj;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/bj;-><init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 2980
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2986
    :goto_0
    return-void

    .line 2981
    :catch_0
    move-exception v0

    .line 2983
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ContentDetailTest::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 2984
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->U:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->onUserAgree(Z)V

    goto :goto_0
.end method

.method public closeWindowByRestrictedContentUnder19()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2941
    :try_start_0
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->v:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;)V

    .line 2942
    const v1, 0x7f0801ba

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 2943
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setCancelable(Z)V

    .line 2944
    const v1, 0x7f080149

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 2945
    const v1, 0x7f0802ef

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/bi;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/bi;-><init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 2952
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2958
    :goto_0
    return-void

    .line 2953
    :catch_0
    move-exception v0

    .line 2955
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ContentDetailTest::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 2956
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->U:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->onUserAgree(Z)V

    goto :goto_0
.end method

.method public closeWindowWithoutNotice()V
    .locals 0

    .prologue
    .line 3025
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->finish()V

    .line 3026
    return-void
.end method

.method public createNfcAdapter()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2562
    invoke-static {p0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->Q:Landroid/nfc/NfcAdapter;

    .line 2564
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->Q:Landroid/nfc/NfcAdapter;

    if-eqz v0, :cond_0

    .line 2567
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->Q:Landroid/nfc/NfcAdapter;

    new-instance v1, Lcom/sec/android/app/samsungapps/bb;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/bb;-><init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V

    new-array v2, v3, [Landroid/app/Activity;

    invoke-virtual {v0, v1, p0, v2}, Landroid/nfc/NfcAdapter;->setNdefPushMessageCallback(Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    .line 2604
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->Q:Landroid/nfc/NfcAdapter;

    new-instance v1, Lcom/sec/android/app/samsungapps/bc;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/bc;-><init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V

    new-array v2, v3, [Landroid/app/Activity;

    invoke-virtual {v0, v1, p0, v2}, Landroid/nfc/NfcAdapter;->setOnNdefPushCompleteCallback(Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    .line 2623
    :cond_0
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 342
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->q:Z

    .line 344
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public endLoading()V
    .locals 1

    .prologue
    .line 2327
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->u:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->end()V

    .line 2328
    return-void
.end method

.method public getLastCustomNonConfigurationInstance()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 430
    new-instance v0, Lcom/sec/android/app/samsungapps/ContentDetailActivity$ContentDetailRestoreDataObject;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity$ContentDetailRestoreDataObject;-><init>()V

    .line 432
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity$ContentDetailRestoreDataObject;->setRestoreData(Z)V

    .line 434
    return-object v0
.end method

.method public handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1973
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->w:Z

    if-eqz v0, :cond_0

    move v0, v1

    .line 2083
    :goto_0
    return v0

    .line 1977
    :cond_0
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->E:Z

    .line 1979
    sget-object v0, Lcom/sec/android/app/samsungapps/bn;->c:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 2083
    :cond_1
    :goto_1
    :pswitch_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z

    move-result v0

    goto :goto_0

    :pswitch_1
    move-object v0, p1

    .line 1981
    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;

    .line 1982
    sget-object v3, Lcom/sec/android/app/samsungapps/bn;->a:[I

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;->getAccountEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;->ordinal()I

    move-result v0

    aget v0, v3, v0

    packed-switch v0, :pswitch_data_1

    goto :goto_1

    .line 1984
    :pswitch_2
    const/16 v0, 0x11

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;I)V

    move v0, v2

    .line 1985
    goto :goto_0

    .line 1989
    :pswitch_3
    invoke-direct {p0, v2}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a(Z)V

    .line 1992
    invoke-direct {p0, v2}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->b(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1993
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->k()V

    :cond_2
    move v0, v1

    .line 1996
    goto :goto_0

    .line 1998
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getPurchased()Lcom/sec/android/app/samsungapps/vlibrary/doc/Purchased;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2000
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->finish()V

    move v0, v1

    .line 2001
    goto :goto_0

    .line 2003
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->U:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;

    if-eqz v0, :cond_4

    .line 2005
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->U:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->onLogedOut()V

    .line 2007
    :cond_4
    invoke-direct {p0, v2}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a(Z)V

    .line 2008
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z

    move v0, v1

    .line 2009
    goto :goto_0

    .line 2016
    :pswitch_5
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getExtraData()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v3

    if-eqz v0, :cond_5

    if-nez v3, :cond_6

    :cond_5
    :goto_2
    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->setProgressStateNormal()V

    goto :goto_1

    :cond_6
    invoke-virtual {v0, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_5

    move v1, v2

    goto :goto_2

    .line 2019
    :pswitch_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->v:Landroid/content/Context;

    const/16 v1, 0xc

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;I)V

    move v0, v2

    .line 2020
    goto/16 :goto_0

    .line 2022
    :pswitch_7
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->addSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;)V

    .line 2023
    const/16 v0, 0xf

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;I)V

    move v0, v2

    .line 2024
    goto/16 :goto_0

    .line 2027
    :pswitch_8
    sget-object v0, Lcom/sec/android/app/samsungapps/bn;->b:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getUpdateAppEvent()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/UpdateAppEvent;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/UpdateAppEvent;->getUpdateAppEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/UpdateAppEvent$UpdateAppEventType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/UpdateAppEvent$UpdateAppEventType;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_2

    goto/16 :goto_1

    .line 2030
    :pswitch_9
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z

    move v0, v1

    .line 2031
    goto/16 :goto_0

    .line 2041
    :pswitch_a
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->E:Z

    .line 2043
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getExtraData()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2044
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    if-eqz v3, :cond_1

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2046
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->V:Lcom/sec/android/app/samsungapps/bu;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/bu;->a()I

    move-result v0

    if-ne v0, v2, :cond_7

    .line 2047
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a()V

    .line 2048
    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a(Z)V

    .line 2051
    :cond_7
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a:Z

    if-eqz v0, :cond_8

    .line 2053
    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a(Z)V

    goto/16 :goto_1

    .line 2057
    :cond_8
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->z:Z

    .line 2058
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->B:Z

    goto/16 :goto_1

    .line 2064
    :pswitch_b
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a:Z

    if-eqz v0, :cond_9

    .line 2066
    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a(Z)V

    goto/16 :goto_1

    .line 2070
    :cond_9
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->B:Z

    goto/16 :goto_1

    .line 2075
    :pswitch_c
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getExtraData()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 2076
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getOrderID()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getOrderID()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2078
    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    goto/16 :goto_1

    .line 1979
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch

    .line 1982
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 2027
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_9
    .end packed-switch
.end method

.method public hideProgressBar()V
    .locals 1

    .prologue
    .line 2708
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    if-eqz v0, :cond_0

    .line 2710
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->hideProgressBar()V

    .line 2712
    :cond_0
    return-void
.end method

.method public myReviewCommand()V
    .locals 3

    .prologue
    .line 1844
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    if-nez v0, :cond_0

    .line 1883
    :goto_0
    return-void

    .line 1848
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->O:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    if-nez v0, :cond_1

    .line 1849
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->O:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    .line 1851
    const-string v0, "create _CommentListCommandBuilder"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    .line 1854
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->O:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;->myReviewCommand()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->v:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/at;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/at;-><init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0
.end method

.method public notifyProgress(JJ)V
    .locals 1

    .prologue
    .line 2716
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    if-eqz v0, :cond_0

    .line 2718
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->notifyProgress(JJ)V

    .line 2720
    :cond_0
    return-void
.end method

.method public notifyProgressIndeterminated()V
    .locals 1

    .prologue
    .line 2724
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    if-eqz v0, :cond_0

    .line 2726
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->notifyProgressIndeterminated()V

    .line 2727
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->L:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->onDetailLoaded()V

    .line 2729
    :cond_0
    return-void
.end method

.method public notifyProgressText(Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonProgressText;)V
    .locals 1

    .prologue
    .line 2749
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    if-eqz v0, :cond_0

    .line 2751
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->notifyProgressText(Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonProgressText;)V

    .line 2753
    :cond_0
    return-void
.end method

.method public onActionItemActionBar(ILandroid/view/View;)V
    .locals 1

    .prologue
    .line 455
    sparse-switch p1, :sswitch_data_0

    .line 470
    :goto_0
    return-void

    .line 458
    :sswitch_0
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;I)V

    goto :goto_0

    .line 462
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->e:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailMainWidgetClickListener;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailMainWidgetClickListener;->onClickShareBtn()V

    goto :goto_0

    .line 467
    :sswitch_2
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->j()V

    goto :goto_0

    .line 455
    nop

    :sswitch_data_0
    .sparse-switch
        0xa0005 -> :sswitch_0
        0xa0013 -> :sswitch_1
        0xa0015 -> :sswitch_2
        0xa0016 -> :sswitch_2
    .end sparse-switch
.end method

.method public onAskPopupForValidateUserAge()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2905
    :try_start_0
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->v:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;)V

    .line 2906
    const v1, 0x7f080142

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 2908
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setCancelable(Z)V

    .line 2909
    const v1, 0x7f080144

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 2911
    const v1, 0x7f080145

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/bg;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/bg;-><init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 2920
    const v1, 0x7f08023d

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/bh;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/bh;-><init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setNegativeButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 2929
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2935
    :goto_0
    return-void

    .line 2930
    :catch_0
    move-exception v0

    .line 2932
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ContentDetailTest::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 2933
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->U:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->onUserAgree(Z)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 349
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->w:Z

    .line 350
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onBackPressed()V

    .line 351
    return-void
.end method

.method public onClickRetryBtn(I)V
    .locals 1

    .prologue
    .line 2542
    packed-switch p1, :pswitch_data_0

    .line 2557
    :goto_0
    :pswitch_0
    return-void

    .line 2544
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d()V

    goto :goto_0

    .line 2547
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->e()V

    goto :goto_0

    .line 2550
    :pswitch_3
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->f()V

    goto :goto_0

    .line 2553
    :pswitch_4
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a()V

    .line 2554
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a(Z)V

    goto :goto_0

    .line 2542
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 384
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->b()I

    move-result v0

    .line 387
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 388
    if-lez v0, :cond_0

    .line 389
    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a(IZ)V

    .line 392
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->V:Lcom/sec/android/app/samsungapps/bu;

    if-eqz v0, :cond_1

    .line 394
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->V:Lcom/sec/android/app/samsungapps/bu;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/bu;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->showBgImage(I)V

    .line 397
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 399
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->V:Lcom/sec/android/app/samsungapps/bu;

    if-eqz v0, :cond_2

    .line 400
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->l:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->V:Lcom/sec/android/app/samsungapps/bu;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/bu;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->getWidget()Lcom/sec/android/app/samsungapps/widget/CommonWidget;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->getWidgetState()I

    move-result v0

    .line 401
    if-eqz v0, :cond_3

    .line 402
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->g()V

    .line 403
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->l:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->V:Lcom/sec/android/app/samsungapps/bu;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/bu;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->getWidget()Lcom/sec/android/app/samsungapps/widget/CommonWidget;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->onWidgetSetViewState()V

    .line 420
    :cond_2
    :goto_0
    return-void

    .line 407
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->setButtonTextSize()V

    goto :goto_0

    .line 411
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->mDetailView:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 413
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->x:Z

    if-eqz v0, :cond_5

    .line 414
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->onWidgetViewState(IZ)V

    goto :goto_0

    .line 416
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->i()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 202
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 203
    iput-object p0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->v:Landroid/content/Context;

    .line 204
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 205
    iput-boolean v5, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->x:Z

    .line 206
    iput-boolean v4, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->C:Z

    .line 207
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->R:I

    .line 208
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/realname/CRealNameAgeCheckerFactory;->createRealNameAgeConfirm(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;

    invoke-direct {v3}, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;-><init>()V

    invoke-direct {v0, p0, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary3/realnameage/RealNameAgeConfirm;Lcom/sec/android/app/samsungapps/vlibrary2/account/IAccountCommandBuilder;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->U:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;

    .line 209
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->U:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker$AbuseContentBlockerObserver;)V

    .line 210
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getLastCustomNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/ContentDetailActivity$ContentDetailRestoreDataObject;

    .line 211
    if-eqz v0, :cond_0

    .line 212
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity$ContentDetailRestoreDataObject;->getRestoreData()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->y:Z

    .line 215
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->v:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 216
    const v2, 0x7f040027

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->mDetailView:Landroid/view/View;

    .line 217
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->mDetailView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->setContentView(Landroid/view/View;)V

    .line 219
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    sget-object v2, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->PRODUCT_DETAIL:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->setCurrentPage(Lcom/sec/android/app/samsungapps/tobelog/LogPage;)V

    .line 220
    invoke-direct {p0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a(Landroid/content/Intent;)V

    .line 222
    const v0, 0x7f0c0039

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    .line 224
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c()V

    .line 225
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->v:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    .line 226
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->l:Ljava/util/ArrayList;

    .line 227
    new-instance v0, Lcom/sec/android/app/samsungapps/bu;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/bu;-><init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->V:Lcom/sec/android/app/samsungapps/bu;

    .line 228
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->n()V

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->finish()V

    .line 231
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->l:Ljava/util/ArrayList;

    if-nez v0, :cond_5

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->finish()V

    .line 232
    :goto_1
    invoke-virtual {p0, v4}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->onWidgetSetViewState(I)V

    .line 235
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_2

    .line 237
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->createNfcAdapter()V

    .line 239
    :cond_2
    if-eqz p1, :cond_3

    .line 240
    const-string v0, "current_tab"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->R:I

    .line 243
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->mBaseHandle:Lcom/sec/android/app/samsungapps/base/BaseHandle;

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->PRODUCT_DETAIL:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/base/BaseHandle;->setCurrentPage(Lcom/sec/android/app/samsungapps/tobelog/LogPage;)V

    .line 246
    return-void

    .line 230
    :cond_4
    const v0, 0x7f0c00a5

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->getWidgetState()I

    move-result v1

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->onWidgetViewState(IZ)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->O:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    if-nez v0, :cond_1

    new-instance v0, Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->O:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    const-string v0, "create _CommentListCommandBuilder"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 231
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    const v0, 0x7f0c00a7

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/test/ContentDetailOverviewFragment;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->m:Lcom/sec/android/app/samsungapps/test/ContentDetailOverviewFragment;

    const v0, 0x7f0c00a8

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/test/ContentDetailRelatedFragment;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->o:Lcom/sec/android/app/samsungapps/test/ContentDetailRelatedFragment;

    const v0, 0x7f0c00a9

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/test/ContentDetailReviewsFragment;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->n:Lcom/sec/android/app/samsungapps/test/ContentDetailReviewsFragment;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->l:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->m:Lcom/sec/android/app/samsungapps/test/ContentDetailOverviewFragment;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->l:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->n:Lcom/sec/android/app/samsungapps/test/ContentDetailReviewsFragment;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->l:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->o:Lcom/sec/android/app/samsungapps/test/ContentDetailRelatedFragment;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    invoke-virtual {v2, v0}, Landroid/support/v4/app/FragmentTransaction;->show(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    invoke-virtual {v2, v0}, Landroid/support/v4/app/FragmentTransaction;->hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    invoke-virtual {v2, v0}, Landroid/support/v4/app/FragmentTransaction;->hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->g()V

    goto/16 :goto_1
.end method

.method public onDestroy()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 501
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->w:Z

    .line 502
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->C:Z

    .line 503
    iput-wide v4, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->S:J

    .line 504
    iput-wide v4, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->T:J

    .line 505
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->r:Z

    .line 506
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->D:Z

    .line 507
    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->E:Z

    .line 509
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->L:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;

    if-eqz v1, :cond_0

    .line 511
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->L:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->onDestroy()V

    .line 512
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->L:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;

    .line 514
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->k:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;

    if-eqz v1, :cond_1

    .line 516
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->k:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;->onDestroy()V

    .line 517
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->k:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;

    .line 519
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    if-eqz v1, :cond_2

    .line 520
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->release()V

    .line 523
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->l:Ljava/util/ArrayList;

    if-eqz v1, :cond_4

    .line 525
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->l:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    .line 526
    :goto_0
    if-ge v1, v2, :cond_3

    .line 527
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->release()V

    .line 526
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 529
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 530
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->l:Ljava/util/ArrayList;

    .line 531
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->m:Lcom/sec/android/app/samsungapps/test/ContentDetailOverviewFragment;

    .line 532
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->n:Lcom/sec/android/app/samsungapps/test/ContentDetailReviewsFragment;

    .line 533
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->o:Lcom/sec/android/app/samsungapps/test/ContentDetailRelatedFragment;

    .line 535
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    if-eqz v0, :cond_5

    .line 537
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->removeObserver(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer$IContentObserver;)V

    .line 539
    :cond_5
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 541
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailWidgetHelper;

    if-eqz v0, :cond_6

    .line 542
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailWidgetHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailWidgetHelper;->release()V

    .line 543
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailWidgetHelper;

    .line 545
    :cond_6
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailData;

    iput-object v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->e:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailMainWidgetClickListener;

    iput-object v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->f:Lcom/sec/android/app/samsungapps/vlibrary2/doc/IContentDetailOverview;

    iput-object v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->g:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailOverviewWidgetClickListener;

    iput-object v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailReviewsWidgetClickListener;

    iput-object v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->i:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailExpertReviewWidgetClickListener;

    iput-object v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->j:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailUserReviewWidgetClickListener;

    .line 547
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->v:Landroid/content/Context;

    .line 548
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->mDetailView:Landroid/view/View;

    .line 549
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->I:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;

    .line 550
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->H:Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/ContentDetailCommandBuilder;

    .line 551
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->J:Lcom/sec/android/app/samsungapps/vlibrary2/wishlist/IWishItemCommandBuilder;

    .line 552
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->K:Lcom/sec/android/app/samsungapps/vlibrary2/like/IContentLikeCommandBuilder;

    .line 553
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->O:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    .line 554
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->P:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    .line 555
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->M:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    .line 556
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->N:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentList;

    .line 558
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->Q:Landroid/nfc/NfcAdapter;

    .line 559
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->u:Lcom/sec/android/app/samsungapps/LoadingDialog;

    .line 561
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->U:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;

    if-eqz v0, :cond_7

    .line 563
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->U:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->setObserver(Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker$AbuseContentBlockerObserver;)V

    .line 564
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->U:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;

    .line 567
    :cond_7
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->V:Lcom/sec/android/app/samsungapps/bu;

    .line 568
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->W:[I

    .line 569
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->X:[I

    .line 570
    iput-object v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->Y:[I

    .line 572
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onDestroy()V

    .line 573
    return-void
.end method

.method public onDetailLoading(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V
    .locals 1

    .prologue
    .line 2104
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->mDetailView:Landroid/view/View;

    if-nez v0, :cond_0

    .line 2108
    :cond_0
    return-void
.end method

.method public onDetailUpdated()V
    .locals 2

    .prologue
    .line 2757
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2759
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->onDetailUpdated(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Z)V

    .line 2765
    :cond_0
    :goto_0
    return-void

    .line 2761
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2763
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->onDetailUpdated(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Z)V

    goto :goto_0
.end method

.method public onDetailUpdated(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Z)V
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 2112
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->A:Z

    .line 2113
    iput-wide v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->S:J

    .line 2114
    iput-wide v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->T:J

    .line 2115
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->r:Z

    .line 2117
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onDetailUpdated() - Success: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->d(Ljava/lang/String;)V

    .line 2118
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->w:Z

    if-eqz v0, :cond_1

    .line 2142
    :cond_0
    :goto_0
    return-void

    .line 2121
    :cond_1
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    if-eqz v0, :cond_0

    .line 2125
    if-eqz p2, :cond_2

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2127
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2130
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->l()V

    .line 2131
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->U:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->onContentDetailLoaded(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    goto :goto_0

    .line 2133
    :cond_2
    const-string v0, "onDetailUpdated() - Oops, We don\'t data!"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->d(Ljava/lang/String;)V

    .line 2134
    if-nez p2, :cond_3

    .line 2135
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->onWidgetSetViewState(I)V

    goto :goto_0

    .line 2137
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->mDetailView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2138
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->onWidgetSetViewState(I)V

    goto :goto_0
.end method

.method public onNavigationActionBar(ILandroid/view/View;)V
    .locals 2

    .prologue
    .line 2658
    packed-switch p1, :pswitch_data_0

    .line 2682
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onNavigationActionBar(ILandroid/view/View;)V

    .line 2684
    :goto_0
    return-void

    .line 2666
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "deepLinkAppName"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2667
    if-eqz v0, :cond_0

    const-string v1, "GameHub"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2669
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2670
    const-string v1, "com.sec.everglades.game.launch"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2671
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2672
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->commonStartActivity(Landroid/content/Intent;)V

    .line 2673
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->onBackPressed()V

    goto :goto_0

    .line 2677
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onNavigationActionBar(ILandroid/view/View;)V

    goto :goto_0

    .line 2658
    :pswitch_data_0
    .packed-switch 0xa0000
        :pswitch_0
    .end packed-switch
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 372
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 373
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->k:Lcom/sec/android/app/samsungapps/vlibrary3/downloadcommandmgr/DownloadCmdManager;

    .line 374
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->reInit(Landroid/content/Intent;)V

    .line 375
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 286
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a:Z

    .line 287
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onPause()V

    .line 290
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->p:Lcom/sec/android/app/samsungapps/tobelog/LogData;

    if-eqz v0, :cond_0

    .line 291
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->p:Lcom/sec/android/app/samsungapps/tobelog/LogData;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendNormalPageViewLog(Lcom/sec/android/app/samsungapps/tobelog/LogData;)V

    .line 292
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->p:Lcom/sec/android/app/samsungapps/tobelog/LogData;

    .line 295
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->PRODUCT_DETAIL:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->MOVE_OUT_FROM_MAIN_WITHOUT_ACTION:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendSummaryNormalLog(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;)V

    .line 296
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 300
    iput-boolean v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a:Z

    .line 301
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onResume()V

    .line 302
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->l()V

    .line 304
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->D:Z

    if-eqz v0, :cond_0

    .line 306
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->closeWindowByLogedOut()V

    .line 308
    :cond_0
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->D:Z

    .line 310
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    if-eqz v0, :cond_2

    .line 311
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    .line 312
    if-eqz v0, :cond_2

    .line 313
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    if-eqz v0, :cond_2

    .line 314
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->refreshWidget()V

    .line 315
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->m:Lcom/sec/android/app/samsungapps/test/ContentDetailOverviewFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/test/ContentDetailOverviewFragment;->getWidget()Lcom/sec/android/app/samsungapps/widget/CommonWidget;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;

    .line 317
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->isScreenshotLoaded()Z

    move-result v1

    if-nez v1, :cond_1

    .line 318
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->onResumeOverView()V

    .line 320
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->L:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;

    if-eqz v0, :cond_2

    .line 321
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->L:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->onDetailLoaded()V

    .line 327
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->B:Z

    if-ne v0, v3, :cond_3

    .line 328
    invoke-direct {p0, v2}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a(Z)V

    .line 331
    :cond_3
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->q:Z

    .line 332
    return-void
.end method

.method public onSamsungAppsClickedOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;)V
    .locals 2

    .prologue
    .line 475
    sget-object v0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->mCurActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isUncStore()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 477
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 497
    :goto_0
    return-void

    .line 480
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/samsungapps/AboutActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 481
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 482
    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->commonStartActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    goto :goto_0

    .line 488
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    .line 495
    :cond_1
    :goto_1
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onSamsungAppsClickedOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;)V

    goto :goto_0

    .line 490
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->e:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailMainWidgetClickListener;

    if-eqz v0, :cond_1

    .line 491
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->e:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailMainWidgetClickListener;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailMainWidgetClickListener;->onClickShareBtn()V

    goto :goto_1

    .line 477
    :pswitch_data_0
    .packed-switch 0x7f0c000e
        :pswitch_0
    .end packed-switch

    .line 488
    :pswitch_data_1
    .packed-switch 0x7f0c000f
        :pswitch_1
    .end packed-switch
.end method

.method public onSamsungAppsCreateOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;)Z
    .locals 7

    .prologue
    const v1, 0x7f0c000f

    const v3, 0x7f08034b

    const v4, 0x7f020068

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 440
    sget-object v0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->mCurActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isUncStore()Z

    move-result v0

    if-ne v0, v6, :cond_0

    .line 442
    const v1, 0x7f0c000e

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f080116

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0200c3

    move-object v0, p1

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(IILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    move v0, v6

    .line 449
    :goto_0
    return v0

    .line 448
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isUncStore()Z

    move-result v0

    if-eq v0, v6, :cond_2

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isTestMode()Z

    move-result v0

    if-eq v0, v6, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isPurchased()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isPackageInstalled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isLinkApp()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->G:Ljava/lang/String;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->v:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->isKitkatMenuAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v0, p1

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(IILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    .line 449
    :cond_2
    :goto_1
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onSamsungAppsCreateOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;)Z

    move-result v0

    goto :goto_0

    .line 448
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getWishState()Lcom/sec/android/app/samsungapps/vlibrary/doc/WishState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/WishState;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isLinkApp()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->G:Ljava/lang/String;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->v:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->isKitkatMenuAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v0, p1

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(IILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isLinkApp()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->G:Ljava/lang/String;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->v:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->isKitkatMenuAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v0, p1

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->addItem(IILjava/lang/String;IZ)Lcom/sec/android/app/samsungapps/widget/SamsungAppsMenuItem;

    goto :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 424
    const-string v0, "current_tab"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->V:Lcom/sec/android/app/samsungapps/bu;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/bu;->a()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 425
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 426
    return-void
.end method

.method public onShowNeedToLoginToCheckRestrictedApp()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2857
    :try_start_0
    new-instance v0, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->v:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;-><init>(Landroid/content/Context;)V

    .line 2858
    const v1, 0x7f0802b5

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 2859
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setCancelable(Z)V

    .line 2860
    const v1, 0x7f08014a

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 2863
    const v1, 0x7f08023d

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/bd;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/bd;-><init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setNegativeButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 2871
    const v1, 0x7f0802b5

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/be;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/be;-><init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->setPositiveButton(Ljava/lang/CharSequence;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 2893
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2899
    :goto_0
    return-void

    .line 2894
    :catch_0
    move-exception v0

    .line 2896
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ContentDetailTest::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 2897
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->U:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/AbuseContentBlocker;->onUserAgree(Z)V

    goto :goto_0
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 355
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->w:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    if-eqz v0, :cond_0

    .line 356
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    if-eqz v0, :cond_0

    .line 357
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->m:Lcom/sec/android/app/samsungapps/test/ContentDetailOverviewFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/test/ContentDetailOverviewFragment;->getWidget()Lcom/sec/android/app/samsungapps/widget/CommonWidget;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;

    .line 359
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->onStopOverView()V

    .line 362
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onStop()V

    .line 363
    return-void
.end method

.method public onWidgetSetViewState(I)V
    .locals 12

    .prologue
    const v11, 0x7f0c0084

    const v10, 0x7f0c0080

    const v9, 0x7f0c007f

    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 1049
    const v0, 0x7f0c00a3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 1050
    const v0, 0x7f0c00a2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 1051
    const v0, 0x7f0c0082

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 1052
    const v1, 0x7f0c0083

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1053
    if-eqz v0, :cond_0

    .line 1054
    iget-object v4, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->v:Landroid/content/Context;

    const-string v5, "isa_samsungapps_icon_dummy"

    const-string v6, "drawable"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/samsungapps/uiutil/KnoxResourceManager;->findResource(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1056
    :cond_0
    if-eqz v1, :cond_1

    .line 1057
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->v:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f080286

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1059
    :cond_1
    if-eqz v2, :cond_2

    if-nez v3, :cond_3

    .line 1093
    :cond_2
    :goto_0
    return-void

    .line 1062
    :cond_3
    if-nez p1, :cond_4

    .line 1063
    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1064
    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1066
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1067
    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1068
    const v0, 0x7f0c007e

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1069
    invoke-virtual {v3, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1070
    invoke-virtual {v3, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1071
    invoke-virtual {v3, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1072
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 1074
    :pswitch_0
    invoke-virtual {v3, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1077
    :pswitch_1
    invoke-virtual {v3, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1080
    :pswitch_2
    invoke-virtual {v3, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1081
    const v0, 0x7f0c0086

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 1082
    new-instance v1, Lcom/sec/android/app/samsungapps/bs;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/bs;-><init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 1072
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public reInit(Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 249
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->x:Z

    .line 250
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a(Landroid/content/Intent;)V

    .line 251
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->l:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 282
    :cond_0
    :goto_0
    return-void

    .line 255
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->m:Lcom/sec/android/app/samsungapps/test/ContentDetailOverviewFragment;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->m:Lcom/sec/android/app/samsungapps/test/ContentDetailOverviewFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/test/ContentDetailOverviewFragment;->getWidget()Lcom/sec/android/app/samsungapps/widget/CommonWidget;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 256
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->m:Lcom/sec/android/app/samsungapps/test/ContentDetailOverviewFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/test/ContentDetailOverviewFragment;->getWidget()Lcom/sec/android/app/samsungapps/widget/CommonWidget;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->setScreenshotNeedLoad()V

    .line 258
    :cond_2
    iput-boolean v4, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->y:Z

    .line 259
    iput-object v5, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->O:Lcom/sec/android/app/samsungapps/commands/CommentListCommandBuilder;

    .line 261
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c()V

    .line 263
    invoke-virtual {p0, v4}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->onWidgetSetViewState(I)V

    .line 264
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->setNeedLoad(Z)V

    .line 265
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    invoke-virtual {v0, v4, v4}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->onWidgetViewState(IZ)V

    .line 266
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->n()V

    .line 268
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    .line 269
    :goto_1
    if-ge v1, v3, :cond_3

    .line 270
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/viewpager/CommonFragment;->getWidget()Lcom/sec/android/app/samsungapps/widget/CommonWidget;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->setNeedLoad(Z)V

    .line 269
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 272
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 274
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 276
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentTransaction;->show(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentTransaction;->hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 278
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->l:Ljava/util/ArrayList;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentTransaction;->hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 279
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 280
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->g()V

    .line 281
    iput-object v5, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->H:Lcom/sec/android/app/samsungapps/vlibrary2/contentdetailcommand/ContentDetailCommandBuilder;

    goto :goto_0
.end method

.method public setDeleteButton(Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DeleteButtonState;)V
    .locals 1

    .prologue
    .line 2741
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    if-eqz v0, :cond_0

    .line 2743
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->setDeleteButton(Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DeleteButtonState;)V

    .line 2745
    :cond_0
    return-void
.end method

.method public setDetailButton(Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;)V
    .locals 1

    .prologue
    .line 2733
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    if-eqz v0, :cond_0

    .line 2735
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->setDetailButton(Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/IDetailButtonDisplay$DetailButtonState;)V

    .line 2737
    :cond_0
    return-void
.end method

.method public showBgImage(I)V
    .locals 10

    .prologue
    const v9, 0x7f0800d8

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 3128
    sget-object v0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->mCurActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isUncStore()Z

    move-result v0

    if-eq v0, v7, :cond_0

    sget-object v0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->mCurActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v0

    if-ne v0, v7, :cond_2

    .line 3130
    :cond_0
    const v0, 0x7f0c00ba

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 3131
    if-eqz v0, :cond_1

    .line 3133
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 3158
    :cond_1
    return-void

    :cond_2
    move v1, v2

    .line 3138
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->W:[I

    array-length v0, v0

    if-ge v1, v0, :cond_1

    .line 3139
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->Y:[I

    aget v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 3140
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->X:[I

    aget v3, v3, v1

    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 3141
    if-eqz v0, :cond_3

    if-eqz v3, :cond_3

    .line 3142
    const v4, 0x401d70a4    # 2.46f

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b008c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v0, v4, v8, v8, v5}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 3143
    if-ne v1, p1, :cond_4

    .line 3144
    invoke-static {v0, v7}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->setTitleColor(Landroid/widget/TextView;Z)V

    .line 3145
    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 3146
    sget-object v3, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 3147
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setSelected(Z)V

    .line 3148
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->v:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->v:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0800d9

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 3138
    :cond_3
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 3150
    :cond_4
    invoke-static {v0, v2}, Lcom/sec/android/app/samsungapps/uiutil/UiUtil;->setTitleColor(Landroid/widget/TextView;Z)V

    .line 3151
    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 3152
    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 3153
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSelected(Z)V

    .line 3154
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->v:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->v:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0800da

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public showDetailnotRestrictedContent()V
    .locals 7

    .prologue
    const/4 v6, -0x1

    const/4 v2, 0x0

    const/4 v5, 0x1

    .line 2784
    const-string v0, "onDetailUpdated() - We have data!"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->d(Ljava/lang/String;)V

    .line 2785
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->x:Z

    if-nez v0, :cond_0

    .line 2786
    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->onWidgetSetViewState(I)V

    .line 2787
    iput-boolean v5, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->x:Z

    .line 2790
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    if-eqz v0, :cond_1

    .line 2792
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailWidgetHelper;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->setWidgetData(Ljava/lang/Object;)V

    .line 2793
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->onWidgetViewState(I)V

    .line 2794
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->updateWidget()V

    .line 2799
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->s:Z

    if-eqz v0, :cond_2

    .line 2801
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->s:Z

    .line 2802
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->m()V

    .line 2805
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->L:Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/btnmanager/DetailButtonManager;->onDetailLoaded()V

    .line 2811
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->l()V

    .line 2813
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->isLogedIn()Z

    move-result v0

    if-ne v0, v5, :cond_3

    .line 2814
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0, v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->setAlreadyLoginAfterUpdated(Z)V

    .line 2816
    :cond_3
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->x:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->y:Z

    if-eq v0, v5, :cond_4

    iget v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->R:I

    if-eq v0, v6, :cond_a

    .line 2818
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a()V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->m:Lcom/sec/android/app/samsungapps/test/ContentDetailOverviewFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/test/ContentDetailOverviewFragment;->getWidget()Lcom/sec/android/app/samsungapps/widget/CommonWidget;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->setScreenshotNeedLoad()V

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d()V

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/initializer/Global;->notiPopup()Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;

    move-result-object v3

    invoke-direct {v0, p0, v1, p0, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/ContentCommandBuilder;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;Lcom/sec/android/app/samsungapps/vlibrary2/content/UpdateDetailViewAfterGetDetail$IDetailView;Lcom/sec/android/app/samsungapps/vlibrary3/popup/INotiPopupFactory;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->I:Lcom/sec/android/app/samsungapps/vlibrary2/contentcommand/IContentCommandBuilder;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->n:Lcom/sec/android/app/samsungapps/test/ContentDetailReviewsFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/test/ContentDetailReviewsFragment;->getWidget()Lcom/sec/android/app/samsungapps/widget/CommonWidget;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->h:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailReviewsWidgetClickListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->setReviewsWidgetListener(Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailReviewsWidgetClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->n:Lcom/sec/android/app/samsungapps/test/ContentDetailReviewsFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/test/ContentDetailReviewsFragment;->getWidget()Lcom/sec/android/app/samsungapps/widget/CommonWidget;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->expertReviewWidget:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->i:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailExpertReviewWidgetClickListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailExpertReviewWidget;->setReviewsWidgetListener(Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailExpertReviewWidgetClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->n:Lcom/sec/android/app/samsungapps/test/ContentDetailReviewsFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/test/ContentDetailReviewsFragment;->getWidget()Lcom/sec/android/app/samsungapps/widget/CommonWidget;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;->userReviewWidget:Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->j:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailUserReviewWidgetClickListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/list/ContentDetailUserReviewWidget;->setReviewsWidgetListener(Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailUserReviewWidgetClickListener;)V

    .line 2819
    iget v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->R:I

    if-ltz v0, :cond_6

    .line 2820
    iget v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->R:I

    if-lez v0, :cond_5

    .line 2821
    iget v3, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->R:I

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->l:Ljava/util/ArrayList;

    if-nez v0, :cond_7

    .line 2823
    :cond_5
    :goto_0
    iput v6, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->R:I

    .line 2841
    :cond_6
    :goto_1
    iput-boolean v5, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->z:Z

    .line 2842
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->B:Z

    .line 2843
    iput-boolean v2, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->y:Z

    .line 2851
    return-void

    .line 2821
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v4

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->V:Lcom/sec/android/app/samsungapps/bu;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/bu;->a(I)V

    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->showBgImage(I)V

    move v1, v2

    :goto_2
    const/4 v0, 0x3

    if-ge v1, v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    invoke-virtual {v4, v0}, Landroid/support/v4/app/FragmentTransaction;->hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_8
    const/4 v0, 0x2

    if-le v3, v0, :cond_9

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    invoke-virtual {v4, v0}, Landroid/support/v4/app/FragmentTransaction;->show(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    :goto_3
    invoke-direct {p0, v3}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->a(I)V

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_0

    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    invoke-virtual {v4, v0}, Landroid/support/v4/app/FragmentTransaction;->show(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    goto :goto_3

    .line 2827
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailWidgetHelper;

    if-eqz v0, :cond_b

    .line 2828
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailWidgetHelper;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailWidgetHelper;->onRefreshContainer(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V

    .line 2830
    :cond_b
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->z:Z

    if-ne v0, v5, :cond_f

    .line 2831
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->n:Lcom/sec/android/app/samsungapps/test/ContentDetailReviewsFragment;

    if-eqz v0, :cond_c

    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->E:Z

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->V:Lcom/sec/android/app/samsungapps/bu;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/bu;->a()I

    move-result v0

    if-ne v0, v5, :cond_c

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->n:Lcom/sec/android/app/samsungapps/test/ContentDetailReviewsFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/test/ContentDetailReviewsFragment;->getWidget()Lcom/sec/android/app/samsungapps/widget/CommonWidget;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailReviewsWidget;

    if-eqz v0, :cond_c

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->k()V

    :cond_c
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->h()V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->F:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->isPurchased()Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->m:Lcom/sec/android/app/samsungapps/test/ContentDetailOverviewFragment;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->m:Lcom/sec/android/app/samsungapps/test/ContentDetailOverviewFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/test/ContentDetailOverviewFragment;->getWidget()Lcom/sec/android/app/samsungapps/widget/CommonWidget;

    move-result-object v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->m:Lcom/sec/android/app/samsungapps/test/ContentDetailOverviewFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/test/ContentDetailOverviewFragment;->getWidget()Lcom/sec/android/app/samsungapps/widget/CommonWidget;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;

    invoke-virtual {v0, v5}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailOverviewWidget;->setNeedLoad(Z)V

    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d()V

    .line 2832
    :cond_d
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->onWidgetViewState(I)V

    .line 2837
    :cond_e
    :goto_4
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->ab:Z

    if-nez v0, :cond_6

    .line 2838
    invoke-direct {p0, v5}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->b(Z)V

    goto/16 :goto_1

    .line 2834
    :cond_f
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->h()V

    goto :goto_4
.end method

.method public showProgressBar()V
    .locals 1

    .prologue
    .line 2700
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    if-eqz v0, :cond_0

    .line 2702
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->d:Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/detail/ContentDetailMainWidget;->showProgressBar()V

    .line 2704
    :cond_0
    return-void
.end method

.method protected startInitialize()V
    .locals 2

    .prologue
    .line 1192
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->initializer(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializerFactory;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/bt;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/bt;-><init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V

    invoke-interface {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializerFactory;->createInitializer(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializer$IIntializerObserver;)Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializer;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/IInitializer;->execute()V

    .line 1217
    return-void
.end method

.method public startLoading()V
    .locals 1

    .prologue
    .line 2322
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->u:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->start()V

    .line 2323
    return-void
.end method
