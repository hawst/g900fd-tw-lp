.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private final COUNT_LIST_PER_ITEM:I

.field private _Context:Landroid/content/Context;

.field private _IListViewState:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;

.field private _PurchasedList:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;ILcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;)V
    .locals 2

    .prologue
    const/16 v1, 0xf

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->COUNT_LIST_PER_ITEM:I

    .line 27
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->_Context:Landroid/content/Context;

    .line 28
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->_IListViewState:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;

    .line 29
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    invoke-direct {v0, v1, p3, p4}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;-><init>(IILcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->_PurchasedList:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    .line 30
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;)Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;
    .locals 1

    .prologue
    .line 12
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->_IListViewState:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;

    return-object v0
.end method

.method private requestPurchasedList()V
    .locals 3

    .prologue
    .line 49
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->_Context:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->_PurchasedList:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;)V

    .line 50
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/c;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/c;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListRequestManager;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 71
    return-void
.end method


# virtual methods
.method public getPurchasedList()Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->_PurchasedList:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    return-object v0
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 113
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->_IListViewState:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;

    .line 114
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->_Context:Landroid/content/Context;

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->_PurchasedList:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->_PurchasedList:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->clear()V

    .line 119
    :cond_0
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->_PurchasedList:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    .line 120
    return-void
.end method

.method public requestMore()V
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->_IListViewState:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;->STATE_LOADINGMORE:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;->setState(Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;)V

    .line 103
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->requestPurchasedList()V

    .line 104
    return-void
.end method

.method public requestPurchaseHistHide()V
    .locals 3

    .prologue
    .line 75
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseHistHideRequestManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->_Context:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->_PurchasedList:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseHistHideRequestManager;-><init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;)V

    .line 76
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->_Context:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/d;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/d;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchaseHistHideRequestManager;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 98
    return-void
.end method

.method public setIListViewStateManager(Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;)V
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->_IListViewState:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;

    .line 35
    return-void
.end method

.method public start()V
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->_IListViewState:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;->STATE_LOADING:Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager;->setState(Lcom/sec/android/app/samsungapps/vlibrary2/contentlistcommand/IListViewStateManager$IListViewState;)V

    .line 40
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->_PurchasedList:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->_PurchasedList:Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedList;->clear()V

    .line 44
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->requestPurchasedList()V

    .line 45
    return-void
.end method
