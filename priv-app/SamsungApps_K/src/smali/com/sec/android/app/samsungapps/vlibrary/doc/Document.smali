.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field public static _FixedHubURL:Ljava/lang/String;

.field public static _FixedURL:Ljava/lang/String;

.field public static _Handler:Landroid/os/Handler;

.field public static _useFixedURL:Z

.field public static _useSSL:Z

.field private static bUnifiedBillingCondition:Z

.field private static instance:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

.field public static mCategoryContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;


# instance fields
.field private _Context:Landroid/content/Context;

.field _IImageResolution:Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

.field _ItemCountPerPage:I

.field private _KNOXAPI:Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

.field private _MigrationProceeder:Lcom/sec/android/app/samsungapps/vlibrary/viewInteractor/InfCheckMigrationRequired;

.field private _bTablet:Z

.field mAccountInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

.field mBannerList:Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;

.field mCacheTimer:Lcom/sec/android/app/samsungapps/vlibrary/doc/j;

.field mCategoryContents:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

.field private mConfig:Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

.field mContentRepository:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentRepository;

.field private mDataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

.field private mDeepLink:Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

.field private mDeviceInfoLoader:Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

.field mFeaturedHot:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

.field mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

.field mPurchasedContents:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

.field mRequestBuilder:Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

.field private mRequestBuilderFactory:Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilderFactory;

.field private mSappsConfig:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

.field mSearch:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

.field mTestMode:Lcom/sec/android/app/samsungapps/vlibrary/etc/TestMode;

.field mTopAll:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

.field mTopFree:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

.field mTopNew:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

.field mTopPaid:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

.field mViewCaller:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ViewCaller;

.field net:Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

.field notifications:Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->instance:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    .line 43
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mCategoryContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

    .line 67
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->_useSSL:Z

    .line 68
    sput-boolean v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->_useFixedURL:Z

    .line 69
    const-string v0, "http://10.240.58.126"

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->_FixedHubURL:Ljava/lang/String;

    .line 70
    const-string v0, "http://10.240.58.126"

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->_FixedURL:Ljava/lang/String;

    .line 86
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->_Handler:Landroid/os/Handler;

    .line 263
    sput-boolean v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->bUnifiedBillingCondition:Z

    return-void
.end method

.method protected constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mAccountInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    .line 37
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    .line 39
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->net:Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    .line 40
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentRepository;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentRepository;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mContentRepository:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentRepository;

    .line 45
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->createFeaturedHot()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mFeaturedHot:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    .line 46
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->createTopAll()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mTopAll:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    .line 47
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->createTopFree()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mTopFree:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    .line 48
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->createTopPaid()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mTopPaid:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    .line 49
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->createTopNew()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mTopNew:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    .line 50
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->createSearch()Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mSearch:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    .line 51
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mCategoryContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->createCategory(Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryIDGetter;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mCategoryContents:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    .line 54
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->_IImageResolution:Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    .line 56
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mViewCaller:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ViewCaller;

    .line 58
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mBannerList:Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;

    .line 59
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->notifications:Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;

    .line 64
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/j;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/j;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mCacheTimer:Lcom/sec/android/app/samsungapps/vlibrary/doc/j;

    .line 65
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->_Context:Landroid/content/Context;

    .line 72
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mConfig:Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    .line 81
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->_KNOXAPI:Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    .line 398
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mDataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    .line 487
    const/16 v0, 0x19

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->_ItemCountPerPage:I

    .line 108
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;
    .locals 1

    .prologue
    .line 99
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->instance:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    if-nez v0, :cond_0

    .line 101
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->instance:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    .line 103
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->instance:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    return-object v0
.end method

.method public static isAliveMain()Z
    .locals 1

    .prologue
    .line 122
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->instance:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isExistDocumentInstance()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 127
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->instance:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    if-nez v1, :cond_1

    .line 157
    :cond_0
    :goto_0
    return v0

    .line 132
    :cond_1
    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->instance:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->net:Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->instance:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mContentRepository:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentRepository;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->instance:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mCategoryContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->instance:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mFeaturedHot:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->instance:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mTopNew:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->instance:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mTopAll:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->instance:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mTopFree:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->instance:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mTopPaid:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->instance:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mSearch:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->instance:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mCategoryContents:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->instance:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mBannerList:Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->instance:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->notifications:Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->instance:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mCacheTimer:Lcom/sec/android/app/samsungapps/vlibrary/doc/j;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->instance:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mTestMode:Lcom/sec/android/app/samsungapps/vlibrary/etc/TestMode;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->instance:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mDeviceInfoLoader:Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->instance:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mDataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->instance:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mDataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->instance:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    iget-object v1, v1, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mAccountInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    if-eqz v1, :cond_0

    .line 157
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isUnifiedBillingCondition()Z
    .locals 1

    .prologue
    .line 275
    sget-boolean v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->bUnifiedBillingCondition:Z

    return v0
.end method

.method private isUnifiedBillingUsingCondition(Landroid/content/Context;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 267
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isUnifiedBillingSupported()Z

    move-result v1

    if-nez v1, :cond_1

    .line 270
    :cond_0
    :goto_0
    return v0

    :cond_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_0

    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isSAccountVersionHigherThan1_3_001(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public checkCountry(Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;)Z
    .locals 1

    .prologue
    .line 299
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    .line 300
    if-nez v0, :cond_0

    .line 302
    const/4 v0, 0x0

    .line 304
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMCC()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/country/CountryCode;->equalMCC(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public clearCache()V
    .locals 2

    .prologue
    .line 318
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mCategoryContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;->clear()V

    .line 320
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mFeaturedHot:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->clear()V

    .line 321
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mTopNew:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->clear()V

    .line 322
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mTopAll:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->clear()V

    .line 323
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mTopFree:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->clear()V

    .line 324
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mTopPaid:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->clear()V

    .line 325
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mSearch:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->clear()V

    .line 326
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mCategoryContents:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->clear()V

    .line 327
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mPurchasedContents:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    if-eqz v0, :cond_0

    .line 329
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mPurchasedContents:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;->clear()V

    .line 331
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mBannerList:Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;->clear()V

    .line 332
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mCacheTimer:Lcom/sec/android/app/samsungapps/vlibrary/doc/j;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/j;->c()V

    .line 333
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->notifications:Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;->clear()V

    .line 334
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mAccountInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    if-eqz v0, :cond_1

    .line 336
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mAccountInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->setLoginInfo(Lcom/sec/android/app/samsungapps/vlibrary/doc/LoginInfo;)V

    .line 338
    :cond_1
    return-void
.end method

.method public clearFileCache()V
    .locals 2

    .prologue
    .line 342
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/etc/CacheManager;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->_Context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/CacheManager;-><init>(Landroid/content/Context;)V

    .line 343
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/etc/CacheManager;->ClearFileCache()V

    .line 344
    return-void
.end method

.method public createDeepLink(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 437
    invoke-static {p1}, Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;->createDeepLink(Landroid/content/Intent;)Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mDeepLink:Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    .line 438
    return-void
.end method

.method public findContent(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mContentRepository:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentRepository;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentRepository;->findByID(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    move-result-object v0

    return-object v0
.end method

.method public getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;
    .locals 1

    .prologue
    .line 349
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mAccountInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    return-object v0
.end method

.method public getBannerList()Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;
    .locals 1

    .prologue
    .line 401
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mBannerList:Lcom/sec/android/app/samsungapps/vlibrary/doc/BannerList;

    return-object v0
.end method

.method public getCategoryContainer()Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;
    .locals 1

    .prologue
    .line 395
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mCategoryContainer:Lcom/sec/android/app/samsungapps/vlibrary/doc/CategoryContainer;

    return-object v0
.end method

.method public getCheckAppUpgradeResult()Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    if-nez v0, :cond_1

    .line 454
    :cond_0
    const/4 v0, 0x0

    .line 456
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getCheckAppUpgradeResult()Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;

    move-result-object v0

    goto :goto_0
.end method

.method public getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mConfig:Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    return-object v0
.end method

.method public getContentQuery(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;
    .locals 2

    .prologue
    .line 380
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/w;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 390
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 382
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mFeaturedHot:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    goto :goto_0

    .line 383
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mTopAll:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    goto :goto_0

    .line 384
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mTopFree:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    goto :goto_0

    .line 385
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mTopPaid:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    goto :goto_0

    .line 386
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mTopNew:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    goto :goto_0

    .line 387
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mSearch:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    goto :goto_0

    .line 388
    :pswitch_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mCategoryContents:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery;

    goto :goto_0

    .line 380
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;
    .locals 1

    .prologue
    .line 416
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    if-nez v0, :cond_0

    .line 418
    const/4 v0, 0x0

    .line 420
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    goto :goto_0
.end method

.method public getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;
    .locals 1

    .prologue
    .line 432
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mDataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    return-object v0
.end method

.method public getDeepLink()Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;
    .locals 1

    .prologue
    .line 442
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mDeepLink:Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    return-object v0
.end method

.method public getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;
    .locals 1

    .prologue
    .line 425
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    if-nez v0, :cond_0

    .line 427
    const/4 v0, 0x0

    .line 429
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v0

    goto :goto_0
.end method

.method public getDeviceInfoLoader()Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mDeviceInfoLoader:Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    return-object v0
.end method

.method public getDeviceType()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document$DeviceType;
    .locals 2

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isTablet()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 91
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document$DeviceType;->tablet:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document$DeviceType;

    .line 93
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document$DeviceType;->phone:Lcom/sec/android/app/samsungapps/vlibrary/doc/Document$DeviceType;

    goto :goto_0
.end method

.method public getIMEI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v0

    .line 113
    if-nez v0, :cond_0

    .line 115
    const-string v0, "000000000"

    .line 117
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getIMEI()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getImageResolution()Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;
    .locals 1

    .prologue
    .line 294
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->_IImageResolution:Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    return-object v0
.end method

.method public getInitialContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->_Context:Landroid/content/Context;

    return-object v0
.end method

.method public getItemCountPerPage()I
    .locals 1

    .prologue
    .line 495
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->_ItemCountPerPage:I

    return v0
.end method

.method public getKnoxAPI()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;
    .locals 1

    .prologue
    .line 506
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->_KNOXAPI:Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    return-object v0
.end method

.method public getMCC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 309
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    .line 310
    if-nez v0, :cond_0

    .line 312
    const/4 v0, 0x0

    .line 314
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMCC()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getNetAPI()Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->net:Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    return-object v0
.end method

.method public getNetHeaderInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    return-object v0
.end method

.method public getNotificationList()Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;
    .locals 1

    .prologue
    .line 461
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->notifications:Lcom/sec/android/app/samsungapps/vlibrary/doc/NotificationList;

    return-object v0
.end method

.method public getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;
    .locals 2

    .prologue
    .line 367
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mRequestBuilder:Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    if-nez v0, :cond_0

    .line 369
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mRequestBuilderFactory:Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilderFactory;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getNetHeaderInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilderFactory;->createRequestBuilder(Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;)Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mRequestBuilder:Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    .line 371
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mRequestBuilder:Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    return-object v0
.end method

.method public getSAConfig()Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mSappsConfig:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    return-object v0
.end method

.method public getTestMode()Lcom/sec/android/app/samsungapps/vlibrary/etc/TestMode;
    .locals 1

    .prologue
    .line 484
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mTestMode:Lcom/sec/android/app/samsungapps/vlibrary/etc/TestMode;

    return-object v0
.end method

.method public getViewCaller()Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ViewCaller;
    .locals 1

    .prologue
    .line 406
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mViewCaller:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ViewCaller;

    return-object v0
.end method

.method public init(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SamsungAccountExistChecker;Lcom/sec/android/app/samsungapps/vlibrary/viewInteractor/InfCheckMigrationRequired;Lcom/sec/android/app/samsungapps/vlibrary/doc/IVersionProvider;Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilderFactory;)V
    .locals 11

    .prologue
    .line 179
    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-virtual/range {v0 .. v10}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->init(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SamsungAccountExistChecker;Lcom/sec/android/app/samsungapps/vlibrary/viewInteractor/InfCheckMigrationRequired;Lcom/sec/android/app/samsungapps/vlibrary/doc/IVersionProvider;ZLcom/sec/android/app/samsungapps/vlibrary/util/IConfig;Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilderFactory;)V

    .line 181
    return-void
.end method

.method public init(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SamsungAccountExistChecker;Lcom/sec/android/app/samsungapps/vlibrary/viewInteractor/InfCheckMigrationRequired;Lcom/sec/android/app/samsungapps/vlibrary/doc/IVersionProvider;ZLcom/sec/android/app/samsungapps/vlibrary/util/IConfig;Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilderFactory;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 186
    iput-object p9, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mConfig:Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    .line 187
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mRequestBuilder:Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    .line 189
    iput-object p6, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->_MigrationProceeder:Lcom/sec/android/app/samsungapps/vlibrary/viewInteractor/InfCheckMigrationRequired;

    .line 190
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->_KNOXAPI:Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->_KNOXAPI:Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;->release()V

    .line 193
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->_KNOXAPI:Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    .line 195
    :cond_0
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXAPI;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXAPI;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->_KNOXAPI:Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/IKNOXAPI;

    .line 196
    iput-boolean p8, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->_bTablet:Z

    .line 197
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->_Context:Landroid/content/Context;

    .line 198
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "cache expired check "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 199
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mCacheTimer:Lcom/sec/android/app/samsungapps/vlibrary/doc/j;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/j;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mCacheTimer:Lcom/sec/android/app/samsungapps/vlibrary/doc/j;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/j;->c()V

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mCacheTimer:Lcom/sec/android/app/samsungapps/vlibrary/doc/j;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/j;->b()V

    .line 205
    :cond_1
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mSappsConfig:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    .line 207
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mTestMode:Lcom/sec/android/app/samsungapps/vlibrary/etc/TestMode;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mTestMode:Lcom/sec/android/app/samsungapps/vlibrary/etc/TestMode;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/etc/TestMode;->isTestMode()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 209
    const/4 v0, 0x1

    .line 211
    :goto_0
    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/etc/TestMode;

    invoke-direct {v2}, Lcom/sec/android/app/samsungapps/vlibrary/etc/TestMode;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mTestMode:Lcom/sec/android/app/samsungapps/vlibrary/etc/TestMode;

    .line 212
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mTestMode:Lcom/sec/android/app/samsungapps/vlibrary/etc/TestMode;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/etc/TestMode;->isTestMode()Z

    move-result v2

    if-eq v0, v2, :cond_2

    .line 214
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->clearCache()V

    .line 217
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->net:Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->updateHttpClient()V

    .line 218
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mDeviceInfoLoader:Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    .line 219
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mDataExchanger:Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    .line 220
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mDeviceInfoLoader:Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    invoke-direct {v0, v2, p3, p7}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;Lcom/sec/android/app/samsungapps/vlibrary/doc/IVersionProvider;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    .line 221
    iput-object p10, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mRequestBuilderFactory:Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilderFactory;

    .line 224
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->_bTablet:Z

    if-eqz v0, :cond_7

    .line 226
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/TabletImageResolution;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->_Context:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/TabletImageResolution;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->_IImageResolution:Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    .line 233
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mAccountInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    if-nez v0, :cond_8

    .line 235
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    invoke-direct {v0, p3, p5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SamsungAccountExistChecker;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mAccountInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    .line 241
    :goto_2
    sget-boolean v0, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXAPI;->bNeedClearCache:Z

    if-eqz v0, :cond_3

    .line 243
    sput-boolean v1, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXAPI;->bNeedClearCache:Z

    .line 244
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->clearCache()V

    .line 246
    :cond_3
    invoke-interface {p4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->isExistSaconfig()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 248
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->clearCache()V

    .line 251
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->needUpdate()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 253
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->clearCache()V

    .line 255
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mCacheTimer:Lcom/sec/android/app/samsungapps/vlibrary/doc/j;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/j;->d()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 257
    const-string v0, "cache expired"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 258
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->clearCache()V

    .line 260
    :cond_6
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isUnifiedBillingUsingCondition(Landroid/content/Context;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->bUnifiedBillingCondition:Z

    .line 261
    return-void

    .line 230
    :cond_7
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->_Context:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/HHPImageResolution;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->_IImageResolution:Lcom/sec/android/app/samsungapps/vlibrary2/imageResolution/IImageResolution;

    goto :goto_1

    .line 239
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mAccountInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    invoke-virtual {v0, p3, p5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->setReference(Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/SamsungAccountExistChecker;)V

    goto :goto_2

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method

.method public isCountryListSearchMode()Z
    .locals 1

    .prologue
    .line 475
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mTestMode:Lcom/sec/android/app/samsungapps/vlibrary/etc/TestMode;

    if-nez v0, :cond_0

    .line 477
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/etc/TestMode;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/etc/TestMode;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mTestMode:Lcom/sec/android/app/samsungapps/vlibrary/etc/TestMode;

    .line 479
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mTestMode:Lcom/sec/android/app/samsungapps/vlibrary/etc/TestMode;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/etc/TestMode;->isCountryListSearchMode()Z

    move-result v0

    return v0
.end method

.method public isLogedIn()Z
    .locals 1

    .prologue
    .line 510
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->_isLogedIn()Z

    move-result v0

    return v0
.end method

.method public isTablet()Z
    .locals 1

    .prologue
    .line 163
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->_bTablet:Z

    return v0
.end method

.method public isTestMode()Z
    .locals 1

    .prologue
    .line 466
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mTestMode:Lcom/sec/android/app/samsungapps/vlibrary/etc/TestMode;

    if-nez v0, :cond_0

    .line 468
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/etc/TestMode;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/etc/TestMode;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mTestMode:Lcom/sec/android/app/samsungapps/vlibrary/etc/TestMode;

    .line 470
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mTestMode:Lcom/sec/android/app/samsungapps/vlibrary/etc/TestMode;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/etc/TestMode;->isTestMode()Z

    move-result v0

    return v0
.end method

.method public sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->net:Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 377
    return-void
.end method

.method public setCheckAppUpgradeResult(Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;)V
    .locals 1

    .prologue
    .line 500
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mNetHeaderInfo:Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->setCheckAppUpgradeResult(Lcom/sec/android/app/samsungapps/vlibrary/doc/CheckAppUpgradeResult;)V

    .line 502
    return-void
.end method

.method public setConfig(Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;)V
    .locals 0

    .prologue
    .line 173
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mConfig:Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    .line 174
    return-void
.end method

.method public setDeeplink(Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;)V
    .locals 0

    .prologue
    .line 447
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mDeepLink:Lcom/sec/android/app/samsungapps/vlibrary/etc/DeepLink;

    .line 448
    return-void
.end method

.method public setItemCountPerPage(I)V
    .locals 0

    .prologue
    .line 490
    iput p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->_ItemCountPerPage:I

    .line 491
    return-void
.end method

.method public setViewCaller(Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ViewCaller;)V
    .locals 0

    .prologue
    .line 411
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->mViewCaller:Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ViewCaller;

    .line 412
    return-void
.end method
