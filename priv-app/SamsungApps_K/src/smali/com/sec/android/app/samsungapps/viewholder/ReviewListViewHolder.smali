.class public Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field a:Landroid/widget/LinearLayout;

.field b:Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;

.field public btn_deleteBtn:Landroid/widget/Button;

.field public btn_editBtn:Landroid/widget/Button;

.field public cafeName:Ljava/lang/String;

.field public cafeUrl:Ljava/lang/String;

.field public date:Ljava/lang/String;

.field public ly_btnLayout:Landroid/widget/LinearLayout;

.field public ly_expert_link:Landroid/widget/LinearLayout;

.field public ly_expert_main:Landroid/widget/LinearLayout;

.field public ly_reviewSellerLayout:Landroid/widget/RelativeLayout;

.field public ly_reviewSellerLayoutContainer:Landroid/widget/LinearLayout;

.field public mContext:Landroid/content/Context;

.field public mType:I

.field public mView:Landroid/view/View;

.field public name:Ljava/lang/String;

.field public ratingCntr:F

.field public rating_ratingBar:Landroid/widget/RatingBar;

.field public review:Ljava/lang/String;

.field public sellerName:Ljava/lang/String;

.field public sellerReview:Ljava/lang/String;

.field public sellerdate:Ljava/lang/String;

.field public sellername:Ljava/lang/String;

.field public sellerreview:Ljava/lang/String;

.field public tv_cafeName:Landroid/widget/TextView;

.field public tv_cafeUrl:Landroid/widget/TextView;

.field public tv_date:Landroid/widget/TextView;

.field public tv_expertReview:Landroid/widget/TextView;

.field public tv_name:Landroid/widget/TextView;

.field public tv_review:Landroid/widget/TextView;

.field public tv_sellerDate:Landroid/widget/TextView;

.field public tv_sellerLayoutTitle:Landroid/widget/TextView;

.field public tv_sellerName:Landroid/widget/TextView;

.field public tv_sellerReview:Landroid/widget/TextView;

.field public v_itemDivider:Landroid/view/View;

.field public v_itemGap:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;Landroid/content/Context;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->mView:Landroid/view/View;

    .line 63
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->b:Lcom/sec/android/app/samsungapps/widget/detail/ReviewListArrayAdapter;

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->mView:Landroid/view/View;

    const v1, 0x7f0c0118

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->tv_name:Landroid/widget/TextView;

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->mView:Landroid/view/View;

    const v1, 0x7f0c0119

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->tv_date:Landroid/widget/TextView;

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->mView:Landroid/view/View;

    const v1, 0x7f0c0127

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->tv_review:Landroid/widget/TextView;

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->mView:Landroid/view/View;

    const v1, 0x7f0c0124

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->a:Landroid/widget/LinearLayout;

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->mView:Landroid/view/View;

    const v1, 0x7f0c0128

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->ly_btnLayout:Landroid/widget/LinearLayout;

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->mView:Landroid/view/View;

    const v1, 0x7f0c012b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->ly_reviewSellerLayoutContainer:Landroid/widget/LinearLayout;

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->mView:Landroid/view/View;

    const v1, 0x7f0c0122

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->v_itemDivider:Landroid/view/View;

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->mView:Landroid/view/View;

    const v1, 0x7f0c0125

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->v_itemGap:Landroid/view/View;

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->mView:Landroid/view/View;

    const v1, 0x7f0c0129

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->btn_editBtn:Landroid/widget/Button;

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->mView:Landroid/view/View;

    const v1, 0x7f0c012a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->btn_deleteBtn:Landroid/widget/Button;

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->mView:Landroid/view/View;

    const v1, 0x7f0c0126

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RatingBar;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->rating_ratingBar:Landroid/widget/RatingBar;

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->mView:Landroid/view/View;

    const v1, 0x7f0c012d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->tv_sellerName:Landroid/widget/TextView;

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->mView:Landroid/view/View;

    const v1, 0x7f0c012e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->tv_sellerDate:Landroid/widget/TextView;

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->mView:Landroid/view/View;

    const v1, 0x7f0c012f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->tv_sellerReview:Landroid/widget/TextView;

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->mView:Landroid/view/View;

    const v1, 0x7f0c011a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->tv_expertReview:Landroid/widget/TextView;

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->mView:Landroid/view/View;

    const v1, 0x7f0c0117

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->ly_expert_main:Landroid/widget/LinearLayout;

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->mView:Landroid/view/View;

    const v1, 0x7f0c0116

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->ly_expert_link:Landroid/widget/LinearLayout;

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->mView:Landroid/view/View;

    const v1, 0x7f0c011b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->tv_cafeName:Landroid/widget/TextView;

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->mView:Landroid/view/View;

    const v1, 0x7f0c011c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->tv_cafeUrl:Landroid/widget/TextView;

    .line 89
    return-void
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->tv_name:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->tv_date:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->date:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->tv_review:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->review:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    :goto_0
    return-void

    .line 122
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->tv_expertReview:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->review:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public setHolder(IZZ)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->mView:Landroid/view/View;

    if-nez v0, :cond_0

    .line 114
    :goto_0
    return-void

    .line 96
    :cond_0
    const/4 v0, 0x2

    if-ne p1, v0, :cond_3

    .line 98
    if-eqz p3, :cond_1

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->tv_sellerDate:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->sellerdate:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->tv_sellerReview:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->sellerreview:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->ly_reviewSellerLayoutContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 103
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->a(I)V

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->rating_ratingBar:Landroid/widget/RatingBar;

    iget v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->ratingCntr:F

    invoke-virtual {v0, v1}, Landroid/widget/RatingBar;->setRating(F)V

    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->ly_btnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 105
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->ly_reviewSellerLayoutContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 104
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->ly_btnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1

    .line 111
    :cond_3
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->a(I)V

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->tv_cafeName:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->cafeName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->tv_cafeUrl:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "<u>"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/viewholder/ReviewListViewHolder;->cafeUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</u>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
