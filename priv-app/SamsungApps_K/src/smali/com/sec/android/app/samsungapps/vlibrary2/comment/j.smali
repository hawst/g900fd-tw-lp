.class final Lcom/sec/android/app/samsungapps/vlibrary2/comment/j;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/xml/CommentRequestXML$IModifyCommentInfo;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/j;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getCommentID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/j;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->mCommentID:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->access$500(Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getCommentText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/j;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->mICommentModifyView:Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$ICommentModifyView;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->access$300(Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$ICommentModifyView;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$ICommentModifyView;->getCommentText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getProductID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/j;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->mIModifyCommentCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$IModifyCommentCommandData;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->access$200(Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$IModifyCommentCommandData;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$IModifyCommentCommandData;->getProductID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getRatingValue()I
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/j;->a:Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->mICommentModifyView:Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$ICommentModifyView;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;->access$300(Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$ICommentModifyView;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/ModifyCommentCommand$ICommentModifyView;->getRatingValue()I

    move-result v0

    return v0
.end method
