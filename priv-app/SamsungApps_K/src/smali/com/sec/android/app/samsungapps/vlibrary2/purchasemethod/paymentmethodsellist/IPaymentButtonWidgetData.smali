.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/paymentmethodsellist/IPaymentButtonWidgetData;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract cancel()Z
.end method

.method public abstract getDiscountPrice()Ljava/lang/String;
.end method

.method public abstract getNormalPrice()Ljava/lang/String;
.end method

.method public abstract isDiscounted()Z
.end method

.method public abstract isFreeProduct()Z
.end method

.method public abstract isKorea()Z
.end method

.method public abstract isPaymentAbleCondition()Z
.end method

.method public abstract payment()Z
.end method

.method public abstract release()V
.end method
