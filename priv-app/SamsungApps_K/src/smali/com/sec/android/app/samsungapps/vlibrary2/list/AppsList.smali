.class public Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;
.super Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;
.source "ProGuard"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x77e80bb5ed367d9eL


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;-><init>(I)V

    .line 19
    return-void
.end method


# virtual methods
.method public append(Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 24
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;->isIntersect(Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 40
    :cond_0
    :goto_0
    return-void

    .line 29
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;->getListHeaderResponse()Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->getEndNumber()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->setEndNumber(I)V

    .line 30
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;->getListHeaderResponse()Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->getListHeaderResponse()Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;->append(Lcom/sec/android/app/samsungapps/vlibrary2/baselist/ListHeaderResponse;)V

    .line 32
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/baselist/BaseList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 34
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/list/AppsList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 36
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method
