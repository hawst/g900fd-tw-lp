.class public Lcom/sec/android/app/samsungapps/helper/FreeStorageWarnConditionPopup;
.super Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;
.source "ProGuard"


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/conditionalpopup/ConditionalPopup;-><init>(Landroid/content/Context;)V

    .line 23
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/helper/FreeStorageWarnConditionPopup;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    .line 24
    return-void
.end method


# virtual methods
.method protected matchCondition()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 56
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/helper/FreeStorageWarnConditionPopup;->a:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getDetailMain()Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/doc/ContentDetailMain;->getRealContentSize()J

    move-result-wide v2

    const-wide/16 v4, 0x4

    mul-long/2addr v4, v2

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/helper/FreeStorageWarnConditionPopup;->_Context:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v2

    if-nez v2, :cond_0

    const-string v2, "error"

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->err(Ljava/lang/String;)V

    const-wide/16 v2, 0x0

    :goto_0
    cmp-long v2, v4, v2

    if-lez v2, :cond_1

    move v2, v0

    :goto_1
    if-eqz v2, :cond_2

    .line 65
    :goto_2
    return v0

    .line 56
    :cond_0
    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getStorage()Lcom/sec/android/app/samsungapps/vlibrary/doc/Storage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Storage;->getAvailableInternalMemorySize()J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1

    .line 61
    :catch_0
    move-exception v0

    .line 63
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "FreeStorageWarnConditionPopup::Exception::"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    :cond_2
    move v0, v1

    .line 65
    goto :goto_2
.end method

.method protected onInvokePopup(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 30
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/helper/FreeStorageWarnConditionPopup;->_Context:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/helper/FreeStorageWarnConditionPopup;->_Context:Landroid/content/Context;

    const v2, 0x7f080168

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->createInfoDialog(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/app/samsungapps/CustomDialogBuilder;

    move-result-object v0

    .line 34
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/helper/FreeStorageWarnConditionPopup;->_Context:Landroid/content/Context;

    const v2, 0x7f0802ef

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/helper/h;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/helper/h;-><init>(Lcom/sec/android/app/samsungapps/helper/FreeStorageWarnConditionPopup;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->setPositiveButton(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;)V

    .line 41
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/CustomDialogBuilder;->show()Z

    move-result v0

    if-nez v0, :cond_0

    .line 43
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/helper/FreeStorageWarnConditionPopup;->userAgree(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/helper/FreeStorageWarnConditionPopup;->invokeCompleted()V

    .line 51
    return-void

    .line 45
    :catch_0
    move-exception v0

    .line 47
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FreeStorageWarnConditionPopup::Exception::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 48
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/helper/FreeStorageWarnConditionPopup;->userAgree(Z)V

    goto :goto_0
.end method
