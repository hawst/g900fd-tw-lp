.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum DOWNLOAD_EX_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

.field public static final enum DOWNLOAD_EX_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

.field public static final enum EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

.field public static final enum NOT_AVAILABLE_UPDATE_BECAUSEOF_NETWORKSTATE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

.field public static final enum UPDATE_CHECK_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

.field public static final enum UPDATE_CHECK_SUCCESS_AND_NEED_UPDATE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

.field public static final enum UPDATE_CHECK_SUCCESS_AND_NO_NEED_UPDATE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

.field public static final enum UPDATE_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

.field public static final enum UPDATE_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 11
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    const-string v1, "EXECUTE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    const-string v1, "UPDATE_CHECK_FAILED"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;->UPDATE_CHECK_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    const-string v1, "UPDATE_CHECK_SUCCESS_AND_NO_NEED_UPDATE"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;->UPDATE_CHECK_SUCCESS_AND_NO_NEED_UPDATE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    const-string v1, "UPDATE_CHECK_SUCCESS_AND_NEED_UPDATE"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;->UPDATE_CHECK_SUCCESS_AND_NEED_UPDATE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    const-string v1, "DOWNLOAD_EX_FAILED"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;->DOWNLOAD_EX_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    const-string v1, "DOWNLOAD_EX_SUCCESS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;->DOWNLOAD_EX_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    const-string v1, "UPDATE_SUCCESS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;->UPDATE_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    const-string v1, "UPDATE_FAILED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;->UPDATE_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    const-string v1, "NOT_AVAILABLE_UPDATE_BECAUSEOF_NETWORKSTATE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;->NOT_AVAILABLE_UPDATE_BECAUSEOF_NETWORKSTATE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    .line 10
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;->EXECUTE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;->UPDATE_CHECK_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;->UPDATE_CHECK_SUCCESS_AND_NO_NEED_UPDATE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;->UPDATE_CHECK_SUCCESS_AND_NEED_UPDATE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;->DOWNLOAD_EX_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;->DOWNLOAD_EX_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;->UPDATE_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;->UPDATE_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;->NOT_AVAILABLE_UPDATE_BECAUSEOF_NETWORKSTATE:Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/autoupdate/SelfUpdateStateMachine$Event;

    return-object v0
.end method
