.class public interface abstract Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;
.super Ljava/lang/Object;
.source "ProGuard"


# virtual methods
.method public abstract getAutoUpdateDefaultInterval()I
.end method

.method public abstract getCSC()Ljava/lang/String;
.end method

.method public abstract getMCC()Ljava/lang/String;
.end method

.method public abstract getMNC()Ljava/lang/String;
.end method

.method public abstract getModel()Ljava/lang/String;
.end method

.method public abstract isGameHubSupported()Z
.end method

.method public abstract isGeoIpBasedCountrySearch()Z
.end method

.method public abstract isKnoxMode()Z
.end method

.method public abstract isLogMode()Z
.end method

.method public abstract isReleaseMode()Z
.end method

.method public abstract isSamsungAccountUsed()Z
.end method

.method public abstract isSamsungUpdateMode()Z
.end method

.method public abstract isSlienceInstallSupported()Z
.end method

.method public abstract isUnifiedBillingSupported()Z
.end method

.method public abstract isUsingApkVersionUnifiedBilling()Z
.end method
