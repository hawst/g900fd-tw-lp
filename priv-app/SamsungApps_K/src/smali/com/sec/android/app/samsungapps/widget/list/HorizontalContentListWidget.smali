.class public Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;
.super Lcom/sec/android/app/samsungapps/widget/CommonWidget;
.source "ProGuard"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

.field private b:Lcom/sec/android/app/samsungapps/widget/interfaces/IHorizontalContentListWidgetClickListener;

.field private d:Lcom/sec/android/app/samsungapps/widget/interfaces/IHorizontalContentListWidgetData;

.field private e:Ljava/util/ArrayList;

.field private f:I

.field private g:Landroid/widget/GridView;

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;)V

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->e:Ljava/util/ArrayList;

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->h:Z

    .line 62
    const v0, 0x7f040070

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->initView(Landroid/content/Context;I)V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->e:Ljava/util/ArrayList;

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->h:Z

    .line 67
    const v0, 0x7f040070

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->initView(Landroid/content/Context;I)V

    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->e:Ljava/util/ArrayList;

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->h:Z

    .line 72
    const v0, 0x7f040070

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->initView(Landroid/content/Context;I)V

    .line 73
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;)Lcom/sec/android/app/samsungapps/widget/interfaces/IHorizontalContentListWidgetClickListener;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IHorizontalContentListWidgetClickListener;

    return-object v0
.end method

.method private a(Z)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 131
    const v0, 0x7f0c01a7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 132
    const v1, 0x7f0c01a9

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 133
    if-eqz p1, :cond_0

    .line 135
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 136
    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    .line 137
    invoke-virtual {v0, v2}, Landroid/view/View;->setFocusable(Z)V

    .line 138
    new-instance v2, Lcom/sec/android/app/samsungapps/widget/list/o;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/widget/list/o;-><init>(Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 144
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0800d6

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 150
    :goto_0
    return-void

    .line 146
    :cond_0
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 147
    invoke-virtual {v0, v3}, Landroid/view/View;->setClickable(Z)V

    .line 148
    invoke-virtual {v0, v3}, Landroid/view/View;->setFocusable(Z)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;Z)Z
    .locals 0

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->h:Z

    return p1
.end method


# virtual methods
.method public displayTitle()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 310
    const v0, 0x7f0c01a8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 311
    const v1, 0x7f0c004d

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 313
    if-eqz v0, :cond_0

    iget v2, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->f:I

    if-ne v2, v4, :cond_0

    .line 314
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->d:Lcom/sec/android/app/samsungapps/widget/interfaces/IHorizontalContentListWidgetData;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/widget/interfaces/IHorizontalContentListWidgetData;->isFranceStore()Z

    move-result v2

    if-ne v2, v4, :cond_1

    .line 315
    const v2, 0x7f0200f5

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 316
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 327
    :cond_0
    :goto_0
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->f:I

    packed-switch v0, :pswitch_data_0

    .line 349
    :goto_1
    :pswitch_0
    return-void

    .line 317
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->d:Lcom/sec/android/app/samsungapps/widget/interfaces/IHorizontalContentListWidgetData;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/widget/interfaces/IHorizontalContentListWidgetData;->isGermanyStore()Z

    move-result v2

    if-ne v2, v4, :cond_2

    .line 318
    const v2, 0x7f0200f6

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 319
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 323
    :cond_2
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 329
    :pswitch_1
    const v0, 0x7f0801ef

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 332
    :pswitch_2
    const v0, 0x7f08021e

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 335
    :pswitch_3
    const v0, 0x7f0801f0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 338
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getListTitle()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getListTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 343
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getListTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 347
    :cond_3
    const v0, 0x7f0801f6

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 327
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public getHorizontalContentList()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 286
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->e:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getNumColumns()I
    .locals 4

    .prologue
    const v3, 0x7f0d0001

    .line 290
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 292
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 293
    const v2, 0x7f070027

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 294
    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/2addr v0, v1

    .line 298
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 299
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 301
    :cond_0
    return v0
.end method

.method public loadWidget()V
    .locals 3

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->d:Lcom/sec/android/app/samsungapps/widget/interfaces/IHorizontalContentListWidgetData;

    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->f:I

    new-instance v2, Lcom/sec/android/app/samsungapps/widget/list/n;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/widget/list/n;-><init>(Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;)V

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/samsungapps/widget/interfaces/IHorizontalContentListWidgetData;->sendRequest(ILcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    .line 128
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 385
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 386
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->refreshWidget()V

    .line 387
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->configurationListener:Lcom/sec/android/app/samsungapps/widget/CommonWidget$ConfChangedCompleteListener;

    if-eqz v0, :cond_0

    .line 388
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->configurationListener:Lcom/sec/android/app/samsungapps/widget/CommonWidget$ConfChangedCompleteListener;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget$ConfChangedCompleteListener;->configurationChangedComplete()V

    .line 390
    :cond_0
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IHorizontalContentListWidgetClickListener;

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->d:Lcom/sec/android/app/samsungapps/widget/interfaces/IHorizontalContentListWidgetData;

    invoke-interface {v0, p3}, Lcom/sec/android/app/samsungapps/widget/interfaces/IHorizontalContentListWidgetData;->getContent(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    move-result-object v0

    .line 156
    if-eqz v0, :cond_0

    .line 157
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IHorizontalContentListWidgetClickListener;

    invoke-interface {v1, v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IHorizontalContentListWidgetClickListener;->onContentClick(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;)V

    .line 161
    :cond_0
    return-void
.end method

.method public refreshItemColumns()V
    .locals 2

    .prologue
    .line 364
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->g:Landroid/widget/GridView;

    if-nez v0, :cond_0

    .line 381
    :goto_0
    return-void

    .line 369
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->g:Landroid/widget/GridView;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->getNumColumns()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 371
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->g:Landroid/widget/GridView;

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/list/p;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/list/p;-><init>(Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_0
.end method

.method public refreshWidget()V
    .locals 0

    .prologue
    .line 357
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->updateWidget()V

    .line 358
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 94
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->release()V

    .line 95
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IHorizontalContentListWidgetClickListener;

    .line 96
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->d:Lcom/sec/android/app/samsungapps/widget/interfaces/IHorizontalContentListWidgetData;

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->a:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->a:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->release()V

    .line 102
    :cond_0
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->a:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 109
    :cond_1
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->e:Ljava/util/ArrayList;

    .line 110
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->g:Landroid/widget/GridView;

    .line 111
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->removeAllViews()V

    .line 112
    return-void
.end method

.method public setAdapter(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->a:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    .line 86
    return-void
.end method

.method public setGridView()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 421
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->g:Landroid/widget/GridView;

    if-nez v0, :cond_0

    .line 430
    :goto_0
    return-void

    .line 425
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070028

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 426
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070026

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 428
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->g:Landroid/widget/GridView;

    invoke-virtual {v2, v0}, Landroid/widget/GridView;->setHorizontalSpacing(I)V

    .line 429
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->g:Landroid/widget/GridView;

    invoke-virtual {v0, v1, v3, v1, v3}, Landroid/widget/GridView;->setPadding(IIII)V

    goto :goto_0
.end method

.method public setListener(Lcom/sec/android/app/samsungapps/widget/interfaces/IHorizontalContentListWidgetClickListener;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->b:Lcom/sec/android/app/samsungapps/widget/interfaces/IHorizontalContentListWidgetClickListener;

    .line 77
    return-void
.end method

.method public setType(I)V
    .locals 0

    .prologue
    .line 89
    iput p1, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->f:I

    .line 90
    return-void
.end method

.method public setVisibleNodata(I)Z
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 394
    const v0, 0x7f0c0084

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 396
    const v0, 0x7f0c007f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 397
    const v0, 0x7f0c0080

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 398
    const v0, 0x7f0c007e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 400
    const v0, 0x7f0c0083

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 401
    const v1, 0x7f0c0081

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 403
    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->f:I

    sparse-switch v1, :sswitch_data_0

    .line 414
    const v1, 0x7f0801d0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 417
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 405
    :sswitch_0
    const v1, 0x7f0801c8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 408
    :sswitch_1
    const v1, 0x7f0801c9

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 411
    :sswitch_2
    const v1, 0x7f0801ca

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 403
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x4 -> :sswitch_1
        0x8 -> :sswitch_2
    .end sparse-switch
.end method

.method public setWidgetData(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 81
    check-cast p1, Lcom/sec/android/app/samsungapps/widget/interfaces/IHorizontalContentListWidgetData;

    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->d:Lcom/sec/android/app/samsungapps/widget/interfaces/IHorizontalContentListWidgetData;

    .line 82
    return-void
.end method

.method public updateWidget()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, -0x1

    const/16 v9, 0x8

    const/4 v6, 0x0

    .line 165
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->d:Lcom/sec/android/app/samsungapps/widget/interfaces/IHorizontalContentListWidgetData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 281
    :cond_0
    :goto_0
    return-void

    .line 173
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 178
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->h:Z

    if-eqz v0, :cond_0

    .line 184
    invoke-direct {p0, v6}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->a(Z)V

    .line 186
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->d:Lcom/sec/android/app/samsungapps/widget/interfaces/IHorizontalContentListWidgetData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IHorizontalContentListWidgetData;->getContentCount()I

    move-result v0

    if-lez v0, :cond_c

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->d:Lcom/sec/android/app/samsungapps/widget/interfaces/IHorizontalContentListWidgetData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IHorizontalContentListWidgetData;->getContentCount()I

    move-result v1

    move v0, v6

    .line 189
    :goto_1
    if-ge v0, v1, :cond_5

    .line 191
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->d:Lcom/sec/android/app/samsungapps/widget/interfaces/IHorizontalContentListWidgetData;

    invoke-interface {v2, v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IHorizontalContentListWidgetData;->getContent(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    move-result-object v2

    .line 193
    if-eqz v2, :cond_4

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;->isRemoveContent(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 195
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->e:Ljava/util/ArrayList;

    if-eqz v3, :cond_3

    .line 197
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->e:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 189
    :cond_3
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 200
    :cond_4
    if-eqz v2, :cond_3

    .line 201
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "JunosPulse checked : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->i(Ljava/lang/String;)V

    goto :goto_2

    .line 206
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v1, v0

    .line 208
    :goto_3
    const v0, 0x7f0c01aa

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 209
    const v2, 0x7f0c0142

    invoke-virtual {p0, v2}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 210
    const v3, 0x7f0c01ab

    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 211
    const v4, 0x7f0c01a6

    invoke-virtual {p0, v4}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 212
    iget-object v5, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/samsungapps/uiutil/DeviceResolution;->getDisplayMetrics(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v5

    .line 214
    iget v7, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->f:I

    packed-switch v7, :pswitch_data_0

    .line 231
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->getNumColumns()I

    move-result v0

    .line 235
    :goto_4
    iget v5, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->f:I

    if-ne v5, v11, :cond_8

    .line 237
    invoke-virtual {v4, v9}, Landroid/view/View;->setVisibility(I)V

    .line 238
    invoke-virtual {v2, v9}, Landroid/view/View;->setVisibility(I)V

    .line 239
    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 252
    :goto_5
    if-le v1, v0, :cond_a

    .line 253
    invoke-direct {p0, v11}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->a(Z)V

    .line 254
    add-int/lit8 v1, v1, -0x1

    :goto_6
    if-lt v1, v0, :cond_a

    .line 255
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->e:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 254
    add-int/lit8 v1, v1, -0x1

    goto :goto_6

    :cond_6
    move v1, v6

    .line 206
    goto :goto_3

    .line 220
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->getNumColumns()I

    move-result v7

    if-le v1, v7, :cond_7

    if-eqz v5, :cond_7

    .line 222
    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v8, 0x438e0000    # 284.0f

    iget v5, v5, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v5, v8

    float-to-int v5, v5

    invoke-direct {v7, v10, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 228
    :goto_7
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->getNumColumns()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    .line 229
    goto :goto_4

    .line 226
    :cond_7
    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v8, 0x430e0000    # 142.0f

    iget v5, v5, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v5, v8

    float-to-int v5, v5

    invoke-direct {v7, v10, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_7

    .line 243
    :cond_8
    iget v5, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->f:I

    const/4 v7, 0x2

    if-ne v5, v7, :cond_9

    .line 244
    invoke-virtual {v4, v9}, Landroid/view/View;->setVisibility(I)V

    .line 248
    :goto_8
    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 249
    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_5

    .line 246
    :cond_9
    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_8

    .line 259
    :cond_a
    const v0, 0x7f0c00ee

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->g:Landroid/widget/GridView;

    .line 260
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->g:Landroid/widget/GridView;

    invoke-virtual {v0, v6}, Landroid/widget/GridView;->setFocusableInTouchMode(Z)V

    .line 261
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->setGridView()V

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->a:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    if-nez v0, :cond_b

    .line 263
    new-instance v0, Lcom/sec/android/app/samsungapps/view/MainContentListItemArrayAdapter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->mContext:Landroid/content/Context;

    const v2, 0x7f040069

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->e:Ljava/util/ArrayList;

    const/16 v4, 0x63

    iget v5, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->f:I

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/view/MainContentListItemArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;II)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->setAdapter(Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;)V

    .line 264
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->g:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->a:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 269
    :goto_9
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->refreshItemColumns()V

    .line 270
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->g:Landroid/widget/GridView;

    invoke-virtual {v0, v6}, Landroid/widget/GridView;->setVisibility(I)V

    .line 271
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->g:Landroid/widget/GridView;

    invoke-virtual {v0, p0}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 272
    invoke-virtual {p0, v9}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->setVisibleLoading(I)Z

    .line 279
    :goto_a
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->displayTitle()V

    goto/16 :goto_0

    .line 266
    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->a:Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ContentArrayAdapter;->notifyDataSetChanged()V

    goto :goto_9

    .line 276
    :cond_c
    invoke-virtual {p0, v6}, Lcom/sec/android/app/samsungapps/widget/list/HorizontalContentListWidget;->setVisibleNodata(I)Z

    goto :goto_a

    .line 214
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
