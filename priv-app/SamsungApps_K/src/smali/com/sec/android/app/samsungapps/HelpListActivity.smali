.class public Lcom/sec/android/app/samsungapps/HelpListActivity;
.super Lcom/sec/android/app/samsungapps/ActionBarActivity;
.source "ProGuard"


# instance fields
.field a:Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;-><init>()V

    .line 15
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/HelpListActivity;->a:Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;

    .line 16
    const-string v0, "HelpListActivity"

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/HelpListActivity;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 21
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 23
    const v0, 0x7f08021b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/HelpListActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const v3, 0xb0001

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/HelpListActivity;->setActionBarConfiguration(Ljava/lang/String;Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;ZI)V

    .line 24
    const v0, 0x7f040056

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/HelpListActivity;->setMainView(I)V

    .line 25
    const v0, 0x7f0c017a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/HelpListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/HelpListActivity;->a:Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/HelpListActivity;->a:Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/HelpListActivity;->a:Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;->loadWidget()V

    .line 26
    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/HelpListActivity;->a:Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;

    if-eqz v0, :cond_0

    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/HelpListActivity;->a:Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;->release()V

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/HelpListActivity;->a:Lcom/sec/android/app/samsungapps/widget/list/HelpListWidget;

    .line 43
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onDestroy()V

    .line 44
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 31
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onResume()V

    .line 32
    return-void
.end method
