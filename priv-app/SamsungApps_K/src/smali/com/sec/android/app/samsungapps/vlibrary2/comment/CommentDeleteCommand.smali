.class public Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentDeleteCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _ICommentDeleteCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentDeleteCommand$ICommentDeleteCommandData;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentDeleteCommand$ICommentDeleteCommandData;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentDeleteCommand;->_ICommentDeleteCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentDeleteCommand$ICommentDeleteCommandData;

    .line 18
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentDeleteCommand;Z)V
    .locals 0

    .prologue
    .line 12
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentDeleteCommand;->onFinalResult(Z)V

    return-void
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 4

    .prologue
    .line 22
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentDeleteCommand;->_ICommentDeleteCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentDeleteCommand$ICommentDeleteCommandData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentDeleteCommand$ICommentDeleteCommandData;->getProductID()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentDeleteCommand;->_ICommentDeleteCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentDeleteCommand$ICommentDeleteCommandData;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentDeleteCommand$ICommentDeleteCommandData;->getDeleteCommentID()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/comment/e;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/comment/e;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/comment/CommentDeleteCommand;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->commentDelete(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    .line 29
    return-void
.end method
