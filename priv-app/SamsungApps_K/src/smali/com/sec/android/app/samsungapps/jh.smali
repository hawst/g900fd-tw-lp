.class final Lcom/sec/android/app/samsungapps/jh;
.super Landroid/content/BroadcastReceiver;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)V
    .locals 0

    .prologue
    .line 1561
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/jh;->a:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1565
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1566
    const-string v1, "extra.push.status"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1567
    const-string v2, "extra.push.err.code"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1569
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/jh;->a:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/Context;

    move-result-object v3

    if-nez v3, :cond_1

    .line 1647
    :cond_0
    :goto_0
    return-void

    .line 1573
    :cond_1
    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/jh;->a:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v4}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;-><init>(Landroid/content/Context;)V

    .line 1575
    if-eqz v0, :cond_2

    if-nez v1, :cond_3

    .line 1577
    :cond_2
    const-string v0, "SettingListActivityHelper::getSppResultReceiver::Not Ready Object"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 1581
    :cond_3
    const-string v4, "intent.action.push_connFailure"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-ne v4, v5, :cond_7

    .line 1583
    const-string v0, "SUCCESS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1585
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jh;->a:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v0, v2}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;Ljava/lang/String;)V

    .line 1633
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jh;->a:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->l(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1635
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jh;->a:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    if-eqz v0, :cond_5

    .line 1636
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jh;->a:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->end()V

    .line 1639
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jh;->a:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jh;->a:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1640
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jh;->a:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->b:Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;->dismiss()V

    .line 1643
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jh;->a:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1644
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jh;->a:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/jh;->a:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->g(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/BroadcastReceiver;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    goto :goto_0

    .line 1588
    :cond_7
    const-string v4, "intent.action.push_reg"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-ne v4, v5, :cond_a

    .line 1593
    const-string v0, "SUCCESS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 1596
    const-string v0, "W5000"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jh;->a:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->i(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)I

    move-result v0

    if-gtz v0, :cond_8

    .line 1599
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jh;->a:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v0, v5}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;Z)Z

    .line 1600
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jh;->a:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->j(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)I

    .line 1601
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jh;->a:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v0, v3}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;)V

    goto :goto_1

    .line 1605
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jh;->a:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->k(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)I

    .line 1606
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jh;->a:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v0, v6}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;Z)Z

    .line 1607
    const-string v0, "notify_store_activities_setting"

    const-string v1, "1"

    invoke-virtual {v3, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setSharedConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 1609
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jh;->a:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v0, v2}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1616
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jh;->a:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v0, v6}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;Z)Z

    goto/16 :goto_1

    .line 1619
    :cond_a
    const-string v4, "intent.action.push_dereg"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v5, :cond_4

    .line 1624
    const-string v0, "SUCCESS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1626
    const-string v0, "notify_store_activities_setting"

    const-string v1, "0"

    invoke-virtual {v3, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setSharedConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 1628
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jh;->a:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v0, v2}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;Ljava/lang/String;)V

    goto/16 :goto_1
.end method
