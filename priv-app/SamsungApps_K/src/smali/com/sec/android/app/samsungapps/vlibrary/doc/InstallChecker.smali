.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/doc/IInstallChecker;


# instance fields
.field private appManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;->appManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    .line 12
    return-void
.end method


# virtual methods
.method public isInstalled(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;->appManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->isInstalled(Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;)Z

    move-result v0

    return v0
.end method

.method public isOldVersionInstalled(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;->appManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->isOldVersionInstalled(Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;)Z

    move-result v0

    return v0
.end method

.method public isUpdatable(Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;)Z
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/InstallChecker;->appManager:Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->isUpdatable(Lcom/sec/android/app/samsungapps/vlibrary/util/AppManager;)Z

    move-result v0

    return v0
.end method
