.class final Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/d;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletExecutor$ISamsungWalletExecData;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/d;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getPrice()Ljava/lang/String;
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/d;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;

    # getter for: Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;->_ISamsungWalletPaymentData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment$ISamsungWalletPaymentData;
    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;->access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment$ISamsungWalletPaymentData;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment$ISamsungWalletPaymentData;->getBuyPrice()D

    move-result-wide v0

    double-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getReturnParams()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    const-string v0, "returnParams"

    return-object v0
.end method

.method public final getTimeout()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    const-string v0, "30"

    return-object v0
.end method

.method public final getTransactionID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/d;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;->initResult:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletInitResult;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletInitResult;->getPaymentID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getWK()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/d;->a:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletPayment;->initResult:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletInitResult;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/samsungwallet/SamsungWalletInitResult;->sWalletKey:Ljava/lang/String;

    return-object v0
.end method
