.class public Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;
.super Lcom/sec/android/app/samsungapps/ActionBarActivity;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/NotiDialogObserver;
.implements Lcom/sec/android/app/samsungapps/uieventmanager/UIEventObserver;
.implements Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;


# static fields
.field public static mUpdateAppCount:I


# instance fields
.field a:Landroid/content/Context;

.field b:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;

.field private c:Lcom/sec/android/app/samsungapps/LoadingDialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;-><init>()V

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;->a:Landroid/content/Context;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x1

    .line 50
    sget-object v0, Lcom/sec/android/app/samsungapps/fz;->c:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getMyTabEvent()Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent;->getMyTabEventType()Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/uieventmanager/MyTabEvent$MyTabEventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const/16 v0, 0x23

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;I)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;-><init>()V

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isIran()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/sec/android/app/samsungapps/commands/IranDebitCommandBuilder;

    invoke-direct {v0, v2, v2}, Lcom/sec/android/app/samsungapps/commands/IranDebitCommandBuilder;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/PurchaseInfo;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/commands/IranDebitCommandBuilder;->registerIranCreditCard()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    :goto_1
    new-instance v1, Lcom/sec/android/app/samsungapps/fy;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/fy;-><init>(Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;)V

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0

    :cond_0
    invoke-virtual {v0, v5}, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;->createRegisterCreditCard(Z)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    goto :goto_1

    :pswitch_2
    const/16 v0, 0x21

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;I)V

    goto :goto_0

    :pswitch_3
    const/16 v0, 0x22

    invoke-static {p0, v0}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;I)V

    goto :goto_0

    :pswitch_4
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UPBillingConditionChecker;

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UPBillingConditionChecker;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/unifiedbilling/UPBillingConditionChecker;->isUPApkCondition()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;->a:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingApkCreditCardActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_1
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    new-instance v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    invoke-direct {v1}, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;-><init>()V

    sget-object v2, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->SAC_CLIENT_ID:Ljava/lang/String;

    iput-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->appServiceID:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getRealCountryCode()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->country:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "_"

    const-string v4, "-"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    iput-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->language:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getSAConfig()Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getBillingServerType()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/sec/android/app/samsungapps/unfiedbilling/UnifiedBillingActivity;->getUPServerURL(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->upServerURL:Ljava/lang/String;

    new-instance v2, Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    invoke-direct {v2}, Lcom/samsungosp/billingup/client/requestparam/UserInfo;-><init>()V

    iput-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    iget-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getUserID()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->userID:Ljava/lang/String;

    iget-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getEmailID()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->userEmail:Ljava/lang/String;

    iget-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    sget-object v3, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/Common;->SAC_CLIENT_ID:Ljava/lang/String;

    iput-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->authAppID:Ljava/lang/String;

    iget-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    sget-object v3, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;->_Token:Ljava/lang/String;

    iput-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->accessToken:Ljava/lang/String;

    new-instance v2, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    invoke-direct {v2}, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;-><init>()V

    iput-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    iget-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getModelName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->deviceID:Ljava/lang/String;

    :try_start_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/knoxMode/KNOXUtil;->isKnox2Mode()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDevice()Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->getModelName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_KNOX"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->deviceID:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    :cond_2
    :goto_2
    iget-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getIMEI()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->deviceUID:Ljava/lang/String;

    iget-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    const-string v3, "M|640|480"

    iput-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    iget-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMCC()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->mcc:Ljava/lang/String;

    iget-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getMNC()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->mnc:Ljava/lang/String;

    iget-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getCSC()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->csc:Ljava/lang/String;

    iget-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDeviceInfoLoader()Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->getOpenApiVersion()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->osVersion:Ljava/lang/String;

    iget-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getDeviceInfoLoader()Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->loadODCVersion()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->appVersion:Ljava/lang/String;

    :try_start_1
    invoke-static {v1, p0}, Lcom/samsungosp/billingup/client/UnifiedPayment;->makeCreditCardIntent(Lcom/samsungosp/billingup/client/requestparam/CreditCardData;Landroid/content/Context;)Landroid/content/Intent;
    :try_end_1
    .catch Lcom/samsungosp/billingup/client/util/UnifiedPaymentException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    invoke-virtual {p0, v0, v5}, Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;->startActivityForResult(Landroid/content/Intent;I)V

    const v0, 0x7f050007

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;->overridePendingTransition(II)V

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;->finish()V

    goto/16 :goto_0

    :catch_1
    move-exception v2

    goto :goto_2

    :catch_2
    move-exception v2

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z
    .locals 2

    .prologue
    .line 185
    sget-object v0, Lcom/sec/android/app/samsungapps/fz;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent$EventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 205
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->handleSystemEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEvent;Z)Z

    move-result v0

    :goto_1
    return v0

    :pswitch_0
    move-object v0, p1

    .line 188
    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;

    .line 189
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;->getAccountEventType()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;->LogedOut:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent$AccountEventType;

    if-ne v0, v1, :cond_1

    .line 190
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;->finish()V

    .line 191
    const/4 v0, 0x0

    goto :goto_1

    .line 195
    :cond_1
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;

    invoke-static {p1}, Lcom/sec/android/app/samsungapps/CommonActivity;->handleAccountEvent(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/AccountEvent;)Z

    move-result v0

    goto :goto_1

    .line 198
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;->b:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;

    if-eqz v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;->b:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->reArrangeMenuItem()V

    goto :goto_0

    .line 185
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public handleUIEvent(Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V
    .locals 4

    .prologue
    .line 211
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getEventType()Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;->MyTabEvent:Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    if-ne v0, v1, :cond_0

    .line 213
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->createLogin(Z)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/fx;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/fx;-><init>(Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;)V

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 229
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getEventType()Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;->ContentDisplayEvent:Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent$UIEventType;

    if-ne v0, v1, :cond_1

    .line 230
    sget-object v0, Lcom/sec/android/app/samsungapps/fz;->b:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getContentDisplayEvent()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->getContentDisplayEventType()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent$ContentDisplayEventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 236
    :cond_1
    :goto_0
    return-void

    .line 235
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;->ProductSetList:Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getContentDisplayEvent()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEvent;->getContentDisplayEvent()Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/uieventmanager/ContentDisplayEvent;->getID()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x4

    invoke-static {p0, v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/CommonActivity;->show(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentListQuery$QueryType;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 230
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 170
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 171
    packed-switch p2, :pswitch_data_0

    .line 180
    :goto_0
    return-void

    .line 174
    :pswitch_0
    const-string v0, "Calling UnifiedBilling CreditCard Activity succeeded."

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 177
    :pswitch_1
    const-string v0, "Buyer cancels."

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 171
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const v1, 0x7f08021a

    const v5, 0xb0003

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 104
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 105
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->addSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V

    .line 108
    iput-object p0, p0, Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;->a:Landroid/content/Context;

    .line 109
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getConfig()Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/IConfig;->isSamsungUpdateMode()Z

    move-result v0

    if-ne v0, v3, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->isFreeStore()Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    move-object v4, v2

    .line 114
    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;->setActionBarConfiguration(ILcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    .line 122
    :goto_0
    const v0, 0x7f04008b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;->setMainView(I)V

    .line 123
    const v0, 0x7f0c0205

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;->b:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;

    .line 125
    new-instance v0, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;-><init>()V

    .line 126
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;->createForceLogin()Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/fw;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/fw;-><init>(Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;)V

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    .line 151
    return-void

    .line 117
    :cond_0
    invoke-virtual {p0, v3}, Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;->setMenuIconWhite(Z)V

    .line 118
    new-array v4, v3, [I

    const/4 v0, 0x0

    const v6, 0xa0008

    aput v6, v4, v0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;->setActionBarConfiguration(ILcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar$onClickListener;Z[II)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 59
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventManager;->removeSystemEventObserver(Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/SystemEventObserver;)V

    .line 60
    invoke-static {}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->getInstance()Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->removeObserver(Lcom/sec/android/app/samsungapps/uieventmanager/UIEventObserver;)Z

    .line 61
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onDestroy()V

    .line 62
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 72
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 73
    return-void
.end method

.method public onNotiDialogReceive(Lcom/sec/android/app/samsungapps/CommonDialogInterface;I)I
    .locals 1

    .prologue
    .line 375
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/CommonDialogInterface;->getNotiType()I

    move-result v0

    .line 376
    packed-switch v0, :pswitch_data_0

    .line 394
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 379
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 381
    invoke-static {p0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->removeCard()Z

    .line 382
    new-instance v0, Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/LoadingDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;->c:Lcom/sec/android/app/samsungapps/LoadingDialog;

    .line 383
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;->c:Lcom/sec/android/app/samsungapps/LoadingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/LoadingDialog;->start()V

    goto :goto_0

    .line 388
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;->b:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;

    if-eqz v0, :cond_0

    .line 389
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;->b:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->reArrangeMenuItem()V

    goto :goto_0

    .line 376
    :pswitch_data_0
    .packed-switch 0xff2a
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 154
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onPause()V

    .line 155
    invoke-static {}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->getInstance()Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->removeObserver(Lcom/sec/android/app/samsungapps/uieventmanager/UIEventObserver;)Z

    .line 156
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 161
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onResume()V

    .line 162
    invoke-static {}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->getInstance()Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/samsungapps/uieventmanager/UIEventManager;->addObserver(Lcom/sec/android/app/samsungapps/uieventmanager/UIEventObserver;)V

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;->b:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/PaymentMethodsListActivityR;->b:Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PaymentMethodListWidget;->reArrangeMenuItem()V

    .line 166
    :cond_0
    return-void
.end method

.method public onSamsungAppsOpenOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;)Z
    .locals 1

    .prologue
    .line 78
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/ActionBarActivity;->onSamsungAppsOpenOptionMenu(Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;)Z

    .line 79
    const v0, 0x7f0c000b

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->removeItem(I)Z

    .line 80
    const v0, 0x7f0c0003

    invoke-virtual {p1, v0}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsOptionMenu;->removeItem(I)Z

    .line 81
    const/4 v0, 0x1

    return v0
.end method
