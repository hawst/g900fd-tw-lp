.class public final enum Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum CHECK_COUNTRY_INFO:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;

.field public static final enum NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;

.field public static final enum NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;

.field public static final enum REQ_COUNTRY_SEARCH:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;

.field public static final enum REQ_NETWORK_STATE_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;

.field private static final synthetic a:[Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 12
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;

    const-string v1, "REQ_NETWORK_STATE_CHECK"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;->REQ_NETWORK_STATE_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;

    const-string v1, "NOTIFY_FAILED"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;

    const-string v1, "CHECK_COUNTRY_INFO"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;->CHECK_COUNTRY_INFO:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;

    const-string v1, "REQ_COUNTRY_SEARCH"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;->REQ_COUNTRY_SEARCH:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;

    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;

    const-string v1, "NOTIFY_SUCCESS"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;->NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;

    .line 10
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;->REQ_NETWORK_STATE_CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;->NOTIFY_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;->CHECK_COUNTRY_INFO:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;->REQ_COUNTRY_SEARCH:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;->NOTIFY_SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;->a:[Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary3/initialize/IInitializeManagerState$Action;

    return-object v0
.end method
