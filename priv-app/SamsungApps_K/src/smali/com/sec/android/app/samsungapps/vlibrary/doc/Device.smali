.class public Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static final SHIP_BUILD:Z

.field public static final defaultIMEI:Ljava/lang/String; = "000000000"


# instance fields
.field private IMSI:Ljava/lang/String;

.field private bSupportPhone:Z

.field private mBlackTheme:Z

.field private mDeviceInfoLoader:Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

.field private mDeviceMCC:Ljava/lang/String;

.field private mDeviceMCCAvailable:Z

.field mStorage:Lcom/sec/android/app/samsungapps/vlibrary/doc/Storage;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 157
    const-string v0, "true"

    const-string v1, "ro.product_ship"

    const-string v2, "false"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->SHIP_BUILD:Z

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Storage;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Storage;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->mStorage:Lcom/sec/android/app/samsungapps/vlibrary/doc/Storage;

    .line 19
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->mBlackTheme:Z

    .line 24
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->mDeviceMCCAvailable:Z

    .line 28
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->mDeviceInfoLoader:Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    .line 29
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->load()V

    .line 30
    return-void
.end method

.method public static Is3GAvailable(Landroid/content/Context;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 140
    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 142
    if-nez v0, :cond_0

    .line 154
    :goto_0
    return v1

    .line 147
    :cond_0
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 149
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    move v1, v0

    .line 154
    goto :goto_0

    :cond_1
    move v0, v1

    .line 149
    goto :goto_1

    .line 150
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static IsWifiAvailable(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 122
    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 124
    if-nez v0, :cond_0

    .line 136
    :goto_0
    return v2

    .line 129
    :cond_0
    const/4 v3, 0x1

    :try_start_0
    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 131
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    move v2, v0

    .line 136
    goto :goto_0

    :cond_1
    move v0, v2

    .line 131
    goto :goto_1

    .line 132
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static isNoShipTestMode(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 178
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->isShipMode()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->readTestMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    const/4 v0, 0x1

    .line 181
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isShipMode()Z
    .locals 1

    .prologue
    .line 161
    sget-boolean v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->SHIP_BUILD:Z

    return v0
.end method

.method private static readTestMode(Landroid/content/Context;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 166
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "test_samsungapps"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 168
    if-ne v2, v0, :cond_0

    .line 174
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 168
    goto :goto_0

    :catch_0
    move-exception v0

    :goto_1
    move v0, v1

    .line 174
    goto :goto_0

    .line 173
    :catch_1
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public getIMEI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->mDeviceInfoLoader:Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->getIMEI()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIMSI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->mDeviceInfoLoader:Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->getIMSI()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLocalIpAddress()Ljava/lang/String;
    .locals 4

    .prologue
    .line 106
    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 107
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/NetworkInterface;

    .line 108
    invoke-virtual {v0}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetAddress;

    .line 110
    invoke-virtual {v0}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v3

    if-nez v3, :cond_1

    .line 111
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 118
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMCC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->mDeviceMCC:Ljava/lang/String;

    return-object v0
.end method

.method public getMSISDN()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->mDeviceInfoLoader:Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->getMSISDN()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getModelName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->mDeviceInfoLoader:Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->getModelName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getStorage()Lcom/sec/android/app/samsungapps/vlibrary/doc/Storage;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->mStorage:Lcom/sec/android/app/samsungapps/vlibrary/doc/Storage;

    return-object v0
.end method

.method public iSPhoneSupported()Z
    .locals 1

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->bSupportPhone:Z

    return v0
.end method

.method public isBlackTheme()Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->mBlackTheme:Z

    return v0
.end method

.method public isDeviceMCCValid()Z
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->mDeviceMCC:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->mDeviceMCC:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMCCAvailable()Z
    .locals 1

    .prologue
    .line 100
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->mDeviceMCCAvailable:Z

    return v0
.end method

.method protected load()V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->mDeviceInfoLoader:Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->doesSupportPhoneFeature()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->bSupportPhone:Z

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->mDeviceInfoLoader:Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->isBlackTheme()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->mBlackTheme:Z

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->mDeviceInfoLoader:Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->getIMSI()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->IMSI:Ljava/lang/String;

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->mDeviceInfoLoader:Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;->readMCC()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->mDeviceMCC:Ljava/lang/String;

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->mDeviceMCC:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->mDeviceMCC:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Device;->mDeviceMCCAvailable:Z

    .line 75
    :cond_0
    return-void
.end method
