.class public Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field observers:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;->observers:Ljava/util/ArrayList;

    .line 77
    return-void
.end method


# virtual methods
.method public addObserver(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 24
    monitor-enter p0

    .line 25
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;->observers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 27
    if-ne v1, p1, :cond_0

    .line 29
    monitor-exit p0

    .line 33
    :goto_0
    return-void

    .line 32
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;->observers:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 33
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 48
    monitor-enter p0

    .line 50
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;->observers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 51
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getCloneList()Ljava/util/ArrayList;
    .locals 3

    .prologue
    .line 11
    monitor-enter p0

    .line 12
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 13
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;->observers:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 15
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 19
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 18
    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0
.end method

.method public hasObserver(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 64
    monitor-enter p0

    .line 66
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;->observers:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 68
    const/4 v0, 0x0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 72
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    monitor-exit p0

    goto :goto_0

    .line 74
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public removeObserver(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 38
    monitor-enter p0

    .line 40
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;->observers:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 42
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 43
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public runEach(Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList$Callback;)V
    .locals 2

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList;->getCloneList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 58
    invoke-interface {p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/ObserverList$Callback;->eachElement(Ljava/lang/Object;)V

    goto :goto_0

    .line 60
    :cond_0
    return-void
.end method
