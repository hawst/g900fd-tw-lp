.class public Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IRequestInfo;
.implements Lcom/sec/android/app/samsungapps/vlibrary/xmlbase/IXMLGenerator;


# instance fields
.field _IConnectURLGetter:Lcom/sec/android/app/samsungapps/vlibrary2/xmlheader/IURLGetter;

.field _IRequestHeaderCreator:Lcom/sec/android/app/samsungapps/vlibrary2/xmlheader/CRequestHeaderCreator;

.field private _ReturnMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

.field bSSL:Z

.field private mGearOSVersion:Ljava/lang/String;

.field private mReqID:Ljava/lang/String;

.field private mReqName:Ljava/lang/String;

.field private mTransactionID:I

.field paramvaluelist:Ljava/util/HashMap;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;Ljava/lang/String;Ljava/lang/String;IZZ)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->paramvaluelist:Ljava/util/HashMap;

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->mReqName:Ljava/lang/String;

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->mReqID:Ljava/lang/String;

    .line 27
    iput v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->mTransactionID:I

    .line 28
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->bSSL:Z

    .line 29
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->_IRequestHeaderCreator:Lcom/sec/android/app/samsungapps/vlibrary2/xmlheader/CRequestHeaderCreator;

    .line 30
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->_IConnectURLGetter:Lcom/sec/android/app/samsungapps/vlibrary2/xmlheader/IURLGetter;

    .line 39
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->mReqName:Ljava/lang/String;

    .line 40
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->mReqID:Ljava/lang/String;

    .line 41
    iput p4, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->mTransactionID:I

    .line 43
    iput-boolean p6, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->bSSL:Z

    .line 44
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/xmlheader/CRequestHeaderCreator;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/xmlheader/CRequestHeaderCreator;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->_IRequestHeaderCreator:Lcom/sec/android/app/samsungapps/vlibrary2/xmlheader/CRequestHeaderCreator;

    .line 45
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/xmlheader/CURLGetter;

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/xmlheader/CURLGetter;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->_IConnectURLGetter:Lcom/sec/android/app/samsungapps/vlibrary2/xmlheader/IURLGetter;

    .line 46
    return-void
.end method

.method private createXML(Z)Ljava/lang/String;
    .locals 8

    .prologue
    .line 117
    :try_start_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    .line 119
    invoke-static {}, Landroid/util/Xml;->newSerializer()Lorg/xmlpull/v1/XmlSerializer;

    move-result-object v2

    .line 120
    new-instance v3, Ljava/io/StringWriter;

    invoke-direct {v3}, Ljava/io/StringWriter;-><init>()V

    .line 128
    invoke-interface {v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/Writer;)V

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->_IRequestHeaderCreator:Lcom/sec/android/app/samsungapps/vlibrary2/xmlheader/CRequestHeaderCreator;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->mGearOSVersion:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/xmlheader/CRequestHeaderCreator;->writeOpenSamsungHeader(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V

    .line 133
    const-string v0, ""

    const-string v1, "request"

    invoke-interface {v2, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 134
    const-string v0, ""

    const-string v1, "name"

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->mReqName:Ljava/lang/String;

    invoke-interface {v2, v0, v1, v4}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 135
    const-string v0, ""

    const-string v1, "id"

    iget-object v4, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->mReqID:Ljava/lang/String;

    invoke-interface {v2, v0, v1, v4}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 136
    const-string v0, ""

    const-string v1, "numParam"

    const-string v4, "%d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->paramvaluelist:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v0, v1, v4}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 137
    const-string v0, ""

    const-string v1, "transactionId"

    const-string v4, "%d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget v7, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->mTransactionID:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v0, v1, v4}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 139
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->paramvaluelist:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 141
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 142
    const-string v5, ""

    const-string v6, "param"

    invoke-interface {v2, v5, v6}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 143
    const-string v5, ""

    const-string v6, "name"

    invoke-interface {v2, v5, v6, v1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 145
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/e;

    .line 146
    if-eqz p1, :cond_0

    iget-boolean v1, v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/e;->b:Z

    if-eqz v1, :cond_0

    .line 147
    const-string v0, "XXX"

    invoke-interface {v2, v0}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 151
    :goto_1
    const-string v0, ""

    const-string v1, "param"

    invoke-interface {v2, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 163
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 165
    const-string v0, ""

    :goto_2
    return-object v0

    .line 149
    :cond_0
    :try_start_1
    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/e;->a:Ljava/lang/String;

    invoke-interface {v2, v0}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_1

    .line 154
    :cond_1
    const-string v0, ""

    const-string v1, "request"

    invoke-interface {v2, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->_IRequestHeaderCreator:Lcom/sec/android/app/samsungapps/vlibrary2/xmlheader/CRequestHeaderCreator;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/xmlheader/CRequestHeaderCreator;->writeCloseSamsungHeader(Lorg/xmlpull/v1/XmlSerializer;)V

    .line 159
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    .line 160
    invoke-virtual {v3}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    .line 161
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_2
.end method


# virtual methods
.method public addParam(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 184
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    return-void
.end method

.method public addParam(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 179
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 180
    return-void
.end method

.method public addParam(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 171
    if-nez p2, :cond_0

    .line 172
    const-string p2, ""

    .line 174
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->paramvaluelist:Ljava/util/HashMap;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary/xml/e;

    invoke-direct {v1, p2, p3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/e;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    return-void
.end method

.method protected addPredeployed()V
    .locals 2

    .prologue
    .line 213
    const-string v0, "predeployed"

    const-string v1, "1"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->addParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    return-void
.end method

.method public createStrStrMap()Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;
    .locals 4

    .prologue
    .line 97
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->_ReturnMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->_ReturnMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 101
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->_ReturnMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;->put(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->_ReturnMap:Lcom/sec/android/app/samsungapps/vlibrary/xml/StrStrMap;

    return-object v0
.end method

.method public getLogXML()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->createXML(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getReqID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->mReqID:Ljava/lang/String;

    return-object v0
.end method

.method public getReqName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->mReqName:Ljava/lang/String;

    return-object v0
.end method

.method public getTransactionID()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->mTransactionID:I

    return v0
.end method

.method public getURL()Ljava/lang/String;
    .locals 3

    .prologue
    .line 199
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->_IConnectURLGetter:Lcom/sec/android/app/samsungapps/vlibrary2/xmlheader/IURLGetter;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/xmlheader/IURLGetter;->getURL()Ljava/lang/String;

    move-result-object v0

    .line 200
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->isSSL()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 202
    const-string v1, "http:"

    const-string v2, "https:"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 204
    :cond_0
    return-object v0
.end method

.method public getValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->paramvaluelist:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/e;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/vlibrary/xml/e;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getXML()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->createXML(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isSSL()Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->bSSL:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->_useSSL:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public keySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->paramvaluelist:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public setFakeModel(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->_IRequestHeaderCreator:Lcom/sec/android/app/samsungapps/vlibrary2/xmlheader/CRequestHeaderCreator;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/xmlheader/CRequestHeaderCreator;->setFakeModel(Ljava/lang/String;)V

    .line 111
    return-void
.end method

.method public setGearOSVersion(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 218
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->mGearOSVersion:Ljava/lang/String;

    .line 219
    return-void
.end method

.method public setReqID(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->mReqID:Ljava/lang/String;

    .line 66
    return-void
.end method

.method public setReqName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->mReqName:Ljava/lang/String;

    .line 61
    return-void
.end method

.method public setTransactionID(I)V
    .locals 0

    .prologue
    .line 80
    iput p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/xml/XMLGenerator;->mTransactionID:I

    .line 81
    return-void
.end method

.method protected toStr(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 209
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
