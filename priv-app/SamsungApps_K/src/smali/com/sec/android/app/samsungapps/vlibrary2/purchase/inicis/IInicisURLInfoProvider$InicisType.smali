.class public final enum Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;

.field public static final enum INICIS_CREDITCARD:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;

.field public static final enum INICIS_PHONEBILL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;

.field public static final enum INICIS_UPOINT:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 15
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;

    const-string v1, "INICIS_UPOINT"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;->INICIS_UPOINT:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;

    .line 16
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;

    const-string v1, "INICIS_CREDITCARD"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;->INICIS_CREDITCARD:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;

    .line 17
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;

    const-string v1, "INICIS_PHONEBILL"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;->INICIS_PHONEBILL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;

    .line 13
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;->INICIS_UPOINT:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;->INICIS_CREDITCARD:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;->INICIS_PHONEBILL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;->$VALUES:[Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;

    invoke-virtual {v0}, [Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/samsungapps/vlibrary2/purchase/inicis/IInicisURLInfoProvider$InicisType;

    return-object v0
.end method
