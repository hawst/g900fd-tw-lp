.class public Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;


# static fields
.field public static final AGREED:Ljava/lang/String; = "1"

.field public static final DEFAULT_ENABLE_TENCENT:I = 0x1

.field public static final DISAGREED:Ljava/lang/String; = "0"


# instance fields
.field db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

.field private mSappsConfig:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

.field sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->mSappsConfig:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    .line 32
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    .line 33
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    invoke-direct {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    .line 34
    return-void
.end method

.method private getBool(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 328
    if-nez p1, :cond_1

    .line 338
    :cond_0
    :goto_0
    return v0

    .line 331
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    .line 332
    const-string v3, "Y"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v0, v1

    .line 333
    goto :goto_0

    .line 335
    :cond_2
    const-string v3, "1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 336
    goto :goto_0
.end method

.method private getInt(Ljava/lang/String;)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 314
    if-nez p1, :cond_0

    .line 322
    :goto_0
    return v0

    .line 318
    :cond_0
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 322
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private isValidString(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 139
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public clearAllDB()V
    .locals 1

    .prologue
    .line 684
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->clearAllDB()Z

    .line 685
    return-void
.end method

.method public get25GLimit()I
    .locals 2

    .prologue
    .line 469
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->getDbData(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->getInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public get2GLimit()I
    .locals 2

    .prologue
    .line 463
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->getDbData(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->getInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public get3GLimit()I
    .locals 2

    .prologue
    .line 475
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->getDbData(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->getInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public get4GLimit()I
    .locals 2

    .prologue
    .line 481
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->getDbData(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->getInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getClientType()Ljava/lang/String;
    .locals 2

    .prologue
    .line 689
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->getDbData(I)Ljava/lang/String;

    move-result-object v0

    .line 690
    if-nez v0, :cond_0

    .line 691
    const-string v0, ""

    .line 693
    :cond_0
    return-object v0
.end method

.method public getCountryCode()Ljava/lang/String;
    .locals 2

    .prologue
    .line 534
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v1, 0x35

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->getDbData(I)Ljava/lang/String;

    move-result-object v0

    .line 535
    if-nez v0, :cond_0

    .line 536
    const-string v0, ""

    .line 538
    :cond_0
    return-object v0
.end method

.method public getCurrencyUnitHasPenny()Z
    .locals 3

    .prologue
    .line 185
    const-string v0, "1"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v2, 0x36

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->getDbData(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getCurrencyUnitPrecedes()Z
    .locals 3

    .prologue
    .line 193
    const-string v0, "1"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v2, 0x34

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->getDbData(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    const/4 v0, 0x1

    .line 196
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDisclaimerAgreed()Z
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v1, "DisclaimerSkip"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getSharedConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDisclaimerVersion()Ljava/lang/String;
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->mSappsConfig:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->mSappsConfig:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->isExistSaconfig()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->mSappsConfig:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getDisclaimerVersion()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->isValidString(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->mSappsConfig:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getDisclaimerVersion()Ljava/lang/String;

    move-result-object v0

    .line 134
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v1, "DisclaimerVersion"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getSharedConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getEmailID()Ljava/lang/String;
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->getDbData(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEnableTencent()I
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 766
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v2, "EnableTencent"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 767
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 776
    :cond_0
    :goto_0
    return v0

    .line 771
    :cond_1
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 776
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public getFlexibleTabClsf()I
    .locals 2

    .prologue
    .line 389
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v1, 0x49

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->getDbData(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->getInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getFlexibleTabName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 401
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v1, 0x4a

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->getDbData(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFreeStoreClsf()I
    .locals 4

    .prologue
    const/4 v0, -0x1

    .line 288
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v2, 0x24

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->getDbData(I)Ljava/lang/String;

    move-result-object v1

    .line 289
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-gtz v2, :cond_1

    .line 309
    :cond_0
    :goto_0
    return v0

    .line 297
    :cond_1
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    goto :goto_0

    .line 301
    :catch_0
    move-exception v1

    const-string v1, "ConcreteDataExchanger::isFreeStore::NumberFormatException"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 303
    :catch_1
    move-exception v1

    .line 305
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ConcreteDataExchanger::isFreeStore:: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getFreeTabClsf()I
    .locals 3

    .prologue
    .line 357
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v1, 0x3a

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->getDbData(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    .line 365
    :goto_0
    return v0

    .line 360
    :catch_0
    move-exception v0

    const-string v0, "ConcreteDataExchange::getFreeTabClsf::NumberFormatException"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    .line 365
    :goto_1
    const/4 v0, -0x1

    goto :goto_0

    .line 361
    :catch_1
    move-exception v0

    .line 363
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ConcreteDataExchange::getFreeTabClsf::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public getHeaderInfinityVersion()I
    .locals 2

    .prologue
    .line 344
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v1, 0x25

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->getDbData(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->getInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getLastCSC()Ljava/lang/String;
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v1, 0x2b

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->getDbData(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLastCachedTime()Ljava/util/Date;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 548
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v2, "CachedTime"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 549
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 566
    :cond_0
    :goto_0
    return-object v0

    .line 553
    :cond_1
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "EEE, dd MMM yyyy HH:mm:ss Z"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 554
    const-string v3, "GMT"

    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 559
    :try_start_0
    invoke-virtual {v2, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 561
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_0
.end method

.method public getLastMCC()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->getDbData(I)Ljava/lang/String;

    move-result-object v0

    .line 82
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 84
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const-string v2, ""

    invoke-virtual {v1, v3, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->setDbData(ILjava/lang/String;)Z

    .line 85
    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->writeLastMCC(Ljava/lang/String;)V

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->getDbData(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLastMNC()Ljava/lang/String;
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v1, 0x2a

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->getDbData(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLastPhoneNumber()Ljava/lang/String;
    .locals 2

    .prologue
    .line 215
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v1, "LASTPHONENUMBER"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 216
    if-nez v0, :cond_0

    .line 217
    const-string v0, ""

    .line 219
    :cond_0
    return-object v0
.end method

.method public getLastPurchaseMethodType()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 266
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v2, "PurchaseMethod"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 267
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 279
    :cond_0
    :goto_0
    return-object v0

    .line 274
    :cond_1
    :try_start_0
    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->valueOf(Ljava/lang/String;)Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 275
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getLatestSamsungAppsVersion()Ljava/lang/String;
    .locals 2

    .prologue
    .line 438
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v1, "samsungAppsLatestVersion"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMinPrice()D
    .locals 5

    .prologue
    .line 709
    const-wide/16 v0, 0x0

    .line 711
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v3, "minPriceCreditCard"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 712
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 716
    :try_start_0
    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 724
    :cond_0
    :goto_0
    return-wide v0

    .line 720
    :catch_0
    move-exception v3

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ConcreteDataExchange::getMinPrice::NumberFormatException::"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getPushNotiSuccess()Z
    .locals 2

    .prologue
    .line 526
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v1, 0x4d

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->getDbData(I)Ljava/lang/String;

    move-result-object v0

    .line 527
    if-eqz v0, :cond_0

    const-string v1, "Y"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 528
    const/4 v0, 0x1

    .line 530
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPushRegID()Ljava/lang/String;
    .locals 2

    .prologue
    .line 230
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v1, 0x47

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->getDbData(I)Ljava/lang/String;

    move-result-object v0

    .line 231
    return-object v0
.end method

.method public getRealCountryCode()Ljava/lang/String;
    .locals 2

    .prologue
    .line 780
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v1, "real_country_code"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSNSVal()I
    .locals 2

    .prologue
    .line 377
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v1, 0x3b

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->getDbData(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->getInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getURL()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x1

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->mSappsConfig:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->mSappsConfig:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->isUsingStageURL()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->mSappsConfig:Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/SAppsConfig;->getStageDataHostURL()Ljava/lang/String;

    move-result-object v0

    .line 149
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 162
    :goto_0
    return-object v0

    .line 156
    :cond_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isTestMode()Z

    move-result v0

    if-ne v0, v4, :cond_1

    .line 158
    const-string v0, "%s://%s-%s.%s.%s/%s.%s"

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "http"

    aput-object v3, v1, v2

    const-string v2, "qa"

    aput-object v2, v1, v4

    const/4 v2, 0x2

    const-string v3, "odc"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "samsungapps"

    aput-object v3, v1, v2

    const-string v2, "com"

    aput-object v2, v1, v5

    const/4 v2, 0x5

    const-string v3, "ods"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "as"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 162
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    invoke-virtual {v0, v5}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->getDbData(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getUnitDivision()Z
    .locals 3

    .prologue
    .line 175
    const-string v0, "1"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v2, 0x3c

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->getDbData(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getUpdatableCount()I
    .locals 4

    .prologue
    .line 744
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v1, "BadgeCount"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 745
    const/4 v0, 0x0

    .line 746
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 750
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 757
    :cond_0
    :goto_0
    return v0

    .line 754
    :catch_0
    move-exception v2

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ConcreteDataExchange::getUpdatableCount::NumberFormatException::"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/AppsLog;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getWifiLimit()I
    .locals 2

    .prologue
    .line 487
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->getDbData(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->getInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public isADchecked()Z
    .locals 3

    .prologue
    .line 251
    const-string v0, "Y"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v2, "DF_AD_SETTING"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isAllowAllHost()Z
    .locals 3

    .prologue
    .line 450
    const-string v0, "Y"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v2, "allowAllHost"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isChinaChargePopupNeverShowAgain()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 614
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v2, "ChinaChargePopupShowAgain"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 615
    const-string v2, "1"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v0, :cond_0

    .line 619
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isChinaConnectWlanWarnDoNotShowAgain()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 659
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v2, "ChinaConnectWLANNeverShow"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 660
    const-string v2, "1"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v0, :cond_0

    .line 664
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isChinaDataChargeWarnDoNotShowAgain()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 637
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v2, "ChinaDataChargeNeverShow"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 638
    const-string v2, "1"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-ne v1, v0, :cond_0

    .line 642
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDialogCheckedDontDisplay(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 200
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 201
    const-string v1, "Y"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isNameAuthExecuted()Z
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    return v0
.end method

.method public isPushSvcTurnedOn()Z
    .locals 2

    .prologue
    .line 242
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v1, 0x45

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->getDbData(I)Ljava/lang/String;

    move-result-object v0

    .line 243
    const-string v1, "N"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 244
    const/4 v0, 0x0

    .line 246
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isSuggestAllShareContentChecked()Z
    .locals 2

    .prologue
    .line 598
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v1, "SuggestAllShareContent"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 599
    const-string v1, "N"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 600
    const/4 v0, 0x0

    .line 602
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public needAlipayUpdate()Z
    .locals 3

    .prologue
    .line 413
    const-string v0, "Y"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v2, "needAlipayUpdate"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public needSamsungAppsUpdate()Z
    .locals 3

    .prologue
    .line 426
    const-string v0, "Y"

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v2, "needSamsungAppsUpdate"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public readServerLoadLevel()I
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 582
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v2, "serverLoadLevel"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->getConfigItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 583
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 592
    :cond_0
    :goto_0
    return v0

    .line 587
    :cond_1
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 592
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public setDisclaimerAgreed(Z)V
    .locals 3

    .prologue
    .line 114
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v2, "DisclaimerSkip"

    if-eqz p1, :cond_0

    const-string v0, "1"

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setSharedConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 116
    return-void

    .line 114
    :cond_0
    const-string v0, "0"

    goto :goto_0
.end method

.method public setDisclaimerVersion(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v1, "DisclaimerVersion"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setSharedConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 125
    return-void
.end method

.method public setFreeTabClsf(I)V
    .locals 3

    .prologue
    .line 371
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v1, 0x3a

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->setDbData(ILjava/lang/String;)Z

    .line 372
    return-void
.end method

.method public setLatestSamsungAppsVersion(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 444
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v1, "samsungAppsLatestVersion"

    if-nez p1, :cond_0

    const-string p1, ""

    :cond_0
    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 445
    return-void
.end method

.method public setNameAutheExecuted()V
    .locals 0

    .prologue
    .line 38
    return-void
.end method

.method public setPushNotiSuccess()V
    .locals 3

    .prologue
    .line 522
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v1, 0x4d

    const-string v2, "Y"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->setDbData(ILjava/lang/String;)Z

    .line 523
    return-void
.end method

.method public write25GLimit(I)V
    .locals 3

    .prologue
    .line 499
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v1, 0x8

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->setDbData(ILjava/lang/String;)Z

    .line 500
    return-void
.end method

.method public write2GLimit(I)V
    .locals 3

    .prologue
    .line 493
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/4 v1, 0x7

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->setDbData(ILjava/lang/String;)Z

    .line 494
    return-void
.end method

.method public write3GLimit(I)V
    .locals 3

    .prologue
    .line 505
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v1, 0x9

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->setDbData(ILjava/lang/String;)Z

    .line 506
    return-void
.end method

.method public write4GLimit(I)V
    .locals 3

    .prologue
    .line 511
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v1, 0xa

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->setDbData(ILjava/lang/String;)Z

    .line 512
    return-void
.end method

.method public writeADChecked(Z)V
    .locals 3

    .prologue
    .line 256
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v2, "DF_AD_SETTING"

    if-eqz p1, :cond_0

    const-string v0, "Y"

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 257
    return-void

    .line 256
    :cond_0
    const-string v0, "N"

    goto :goto_0
.end method

.method public writeAlipayUpdate(Z)V
    .locals 3

    .prologue
    .line 420
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v2, "needAlipayUpdate"

    if-eqz p1, :cond_0

    const-string v0, "Y"

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 421
    return-void

    .line 420
    :cond_0
    const-string v0, "N"

    goto :goto_0
.end method

.method public writeAllowAllHost(Z)V
    .locals 3

    .prologue
    .line 457
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v2, "allowAllHost"

    if-eqz p1, :cond_0

    const-string v0, "Y"

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 458
    return-void

    .line 457
    :cond_0
    const-string v0, "N"

    goto :goto_0
.end method

.method public writeChinaChargePopupNeverShowAgain(Z)V
    .locals 3

    .prologue
    .line 625
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 627
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v1, "ChinaChargePopupShowAgain"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 633
    :goto_0
    return-void

    .line 631
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v1, "ChinaChargePopupShowAgain"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public writeChinaConnectWlanWarnDoNotShowAgain(Z)V
    .locals 3

    .prologue
    .line 669
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 671
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v1, "ChinaConnectWLANNeverShow"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 677
    :goto_0
    return-void

    .line 675
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v1, "ChinaConnectWLANNeverShow"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public writeChinaDataChargeWarnDoNotShowAgain(Z)V
    .locals 3

    .prologue
    .line 647
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 649
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v1, "ChinaDataChargeNeverShow"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 655
    :goto_0
    return-void

    .line 653
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v1, "ChinaDataChargeNeverShow"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public writeClientType(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 699
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->setDbData(ILjava/lang/String;)Z

    .line 700
    return-void
.end method

.method public writeCountryCode(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 543
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v1, 0x35

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->setDbData(ILjava/lang/String;)Z

    .line 544
    return-void
.end method

.method public writeCurrencyUnitHasPenny(Z)V
    .locals 3

    .prologue
    .line 179
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v2, 0x36

    if-eqz p1, :cond_0

    const-string v0, "1"

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->setDbData(ILjava/lang/String;)Z

    .line 181
    return-void

    .line 179
    :cond_0
    const-string v0, "0"

    goto :goto_0
.end method

.method public writeCurrencyUnitPrecedes(Z)V
    .locals 3

    .prologue
    .line 189
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v2, 0x34

    if-eqz p1, :cond_0

    const-string v0, "1"

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->setDbData(ILjava/lang/String;)Z

    .line 190
    return-void

    .line 189
    :cond_0
    const-string v0, "0"

    goto :goto_0
.end method

.method public writeDialogDontDisplayCheck(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 205
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v1, "Y"

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 206
    return-void
.end method

.method public writeEmailID(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v1, 0x10

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->setDbData(ILjava/lang/String;)Z

    .line 59
    return-void
.end method

.method public writeEnableTencent(I)V
    .locals 3

    .prologue
    .line 762
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v1, "EnableTencent"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 763
    return-void
.end method

.method public writeFlexibleTabClsf(I)V
    .locals 3

    .prologue
    .line 395
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v1, 0x49

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->setDbData(ILjava/lang/String;)Z

    .line 396
    return-void
.end method

.method public writeFlexibleTabName(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 407
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v1, 0x4a

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->setDbData(ILjava/lang/String;)Z

    .line 408
    return-void
.end method

.method public writeFreeStore(I)V
    .locals 3

    .prologue
    .line 283
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v1, 0x24

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->setDbData(ILjava/lang/String;)Z

    .line 284
    return-void
.end method

.method public writeHeaderInfinityVersion(I)V
    .locals 3

    .prologue
    .line 350
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v1, 0x25

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->setDbData(ILjava/lang/String;)Z

    .line 351
    return-void
.end method

.method public writeLastCSC(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v1, 0x2b

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->setDbData(ILjava/lang/String;)Z

    .line 75
    return-void
.end method

.method public writeLastCachedTime(Ljava/util/Date;)V
    .locals 3

    .prologue
    .line 571
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "EEE, dd MMM yyyy HH:mm:ss Z"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 572
    const-string v1, "GMT"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 573
    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 574
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v2, "CachedTime"

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 575
    return-void
.end method

.method public writeLastMCC(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v1, 0x29

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->setDbData(ILjava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v1, "SelectedMcc"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setSharedConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 96
    :cond_0
    return-void
.end method

.method public writeLastMNC(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v1, 0x2a

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->setDbData(ILjava/lang/String;)Z

    .line 111
    return-void
.end method

.method public writeLastPhoneNumber(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 210
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v1, "LASTPHONENUMBER"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 211
    return-void
.end method

.method public writeLastPurchaseMethodType(Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;)V
    .locals 3

    .prologue
    .line 261
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v1, "PurchaseMethod"

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 262
    return-void
.end method

.method public writeMinPrice(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 734
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v1, "minPriceCreditCard"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 735
    return-void
.end method

.method public writePushRegID(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 224
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v1, 0x47

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->setDbData(ILjava/lang/String;)Z

    .line 226
    return-void
.end method

.method public writePushSvcOnOffState(Z)V
    .locals 3

    .prologue
    .line 236
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v2, 0x45

    if-eqz p1, :cond_0

    const-string v0, "Y"

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->setDbData(ILjava/lang/String;)Z

    .line 238
    return-void

    .line 236
    :cond_0
    const-string v0, "N"

    goto :goto_0
.end method

.method public writeRealCountryCode(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 784
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v1, "real_country_code"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 786
    return-void
.end method

.method public writeSNSVal(I)V
    .locals 3

    .prologue
    .line 383
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v1, 0x3b

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->setDbData(ILjava/lang/String;)Z

    .line 384
    return-void
.end method

.method public writeSamsungAppsUpdate(Z)V
    .locals 3

    .prologue
    .line 432
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v2, "needSamsungAppsUpdate"

    if-eqz p1, :cond_0

    const-string v0, "Y"

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 433
    return-void

    .line 432
    :cond_0
    const-string v0, "N"

    goto :goto_0
.end method

.method public writeServerLoadLeverl(I)V
    .locals 3

    .prologue
    .line 578
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v1, "serverLoadLevel"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 579
    return-void
.end method

.method public writeSuggestAllShareContentChecked(Z)V
    .locals 3

    .prologue
    .line 608
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->sharedpreference:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;

    const-string v2, "SuggestAllShareContent"

    if-eqz p1, :cond_0

    const-string v0, "Y"

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/AppsSharedPreference;->setConfigItem(Ljava/lang/String;Ljava/lang/String;)Z

    .line 609
    return-void

    .line 608
    :cond_0
    const-string v0, "N"

    goto :goto_0
.end method

.method public writeURL(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/4 v1, 0x4

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->setDbData(ILjava/lang/String;)Z

    .line 167
    return-void
.end method

.method public writeUnitDivision(Z)V
    .locals 3

    .prologue
    .line 170
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v2, 0x3c

    if-eqz p1, :cond_0

    const-string v0, "1"

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->setDbData(ILjava/lang/String;)Z

    .line 172
    return-void

    .line 170
    :cond_0
    const-string v0, "0"

    goto :goto_0
.end method

.method public writeWifiLimit(I)V
    .locals 3

    .prologue
    .line 517
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/ConcreteDataExchanger;->db:Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;

    const/16 v1, 0xb

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/DbCommon;->setDbData(ILjava/lang/String;)Z

    .line 518
    return-void
.end method
