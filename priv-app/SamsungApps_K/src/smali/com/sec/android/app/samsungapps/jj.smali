.class final Lcom/sec/android/app/samsungapps/jj;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/widget/PurchaseProtectionArrayAdapter;

.field final synthetic b:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;Lcom/sec/android/app/samsungapps/widget/PurchaseProtectionArrayAdapter;)V
    .locals 0

    .prologue
    .line 1752
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/jj;->b:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    iput-object p2, p0, Lcom/sec/android/app/samsungapps/jj;->a:Lcom/sec/android/app/samsungapps/widget/PurchaseProtectionArrayAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1757
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jj;->b:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1812
    :goto_0
    return-void

    .line 1761
    :cond_0
    if-ne p2, v1, :cond_2

    .line 1763
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jj;->b:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->isLogedIn()Z

    move-result v0

    if-ne v0, v1, :cond_1

    .line 1765
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jj;->b:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/helper/CDownloadPrecheckerFactory;->createPwordConfirm(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;

    move-result-object v0

    .line 1766
    new-instance v1, Lcom/sec/android/app/samsungapps/jk;

    invoke-direct {v1, p0, p2}, Lcom/sec/android/app/samsungapps/jk;-><init>(Lcom/sec/android/app/samsungapps/jj;I)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->addObserver(Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager$IPwordConfirmObserver;)V

    .line 1786
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->check()V

    goto :goto_0

    .line 1790
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jj;->b:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/initializer/Global;->createLogin(Z)Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/jj;->b:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/jl;

    invoke-direct {v2, p0, p2}, Lcom/sec/android/app/samsungapps/jl;-><init>(Lcom/sec/android/app/samsungapps/jj;I)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;->execute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V

    goto :goto_0

    .line 1807
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jj;->b:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(I)V

    .line 1809
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jj;->a:Lcom/sec/android/app/samsungapps/widget/PurchaseProtectionArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchaseProtectionArrayAdapter;->notifyDataSetChanged()V

    .line 1810
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/jj;->b:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/jj;->b:Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->c(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;)Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;->a(Lcom/sec/android/app/samsungapps/SettingsListActivityHelper;Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;)V

    goto :goto_0
.end method
