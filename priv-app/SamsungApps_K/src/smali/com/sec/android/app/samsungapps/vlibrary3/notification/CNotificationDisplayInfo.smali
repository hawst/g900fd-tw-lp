.class public Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/notification/INotificationDisplayInfo;


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

.field f:I

.field g:Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

.field h:Z

.field i:Z

.field private j:Z

.field private k:Ljava/lang/String;

.field private l:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;ZZZLjava/lang/String;Z)V
    .locals 4

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;->a:Ljava/lang/String;

    .line 30
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;->b:Ljava/lang/String;

    .line 31
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;->c:Ljava/lang/String;

    .line 32
    iput-object p5, p0, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;->e:Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    .line 33
    iput-object p6, p0, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    .line 34
    iput-object p4, p0, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;->d:Ljava/lang/String;

    .line 36
    iput-boolean p7, p0, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;->h:Z

    .line 37
    iput-boolean p8, p0, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;->i:Z

    .line 38
    iput-boolean p9, p0, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;->j:Z

    .line 40
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->getSize()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;->e:Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->getSize()J

    move-result-wide v0

    const-wide/16 v2, 0x64

    mul-long/2addr v0, v2

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;->getSize()J

    move-result-wide v2

    div-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;->f:I

    .line 44
    :cond_0
    iput-object p10, p0, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;->k:Ljava/lang/String;

    .line 45
    iput-boolean p11, p0, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;->l:Z

    .line 46
    return-void
.end method


# virtual methods
.method public dontOpenDetailPage()Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;->j:Z

    return v0
.end method

.method public getDownloadProgress()I
    .locals 1

    .prologue
    .line 92
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;->f:I

    return v0
.end method

.method public getDownloadedSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;->e:Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    return-object v0
.end method

.method public getFakeModel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;->k:Ljava/lang/String;

    return-object v0
.end method

.method public getGUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getLoadType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getNotificationID()I
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public getProductID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getProductName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getRealContentSize()Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;->g:Lcom/sec/android/app/samsungapps/vlibrary/doc/primitiveTypes/FileSize;

    return-object v0
.end method

.method public isAD()Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;->i:Z

    return v0
.end method

.method public isCancellable()Z
    .locals 1

    .prologue
    .line 118
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;->l:Z

    return v0
.end method

.method public isSPP()Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;->h:Z

    return v0
.end method

.method public setDownloadProgress(I)V
    .locals 0

    .prologue
    .line 97
    iput p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/notification/CNotificationDisplayInfo;->f:I

    .line 98
    return-void
.end method
