.class public Lcom/sec/android/app/samsungapps/vlibrary/net/RequestURL;
.super Lcom/sec/android/app/samsungapps/vlibrary/net/RequestGET;
.source "ProGuard"


# instance fields
.field handler:Landroid/os/Handler;

.field mHttpGet:Lorg/apache/http/client/methods/HttpGet;

.field mResponse:Ljava/lang/String;

.field mURL:Ljava/lang/String;

.field retryCount:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 33
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestGET;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestURL;->mURL:Ljava/lang/String;

    .line 26
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestURL;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    .line 27
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestURL;->mResponse:Ljava/lang/String;

    .line 28
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestURL;->retryCount:I

    .line 146
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/net/f;

    invoke-direct {v0, p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/f;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestURL;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestURL;->handler:Landroid/os/Handler;

    .line 34
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestURL;->mURL:Ljava/lang/String;

    .line 35
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary/net/RequestURL;Z)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestURL;->notifyURLResult(Z)V

    return-void
.end method

.method private notifyURLResult(Z)V
    .locals 1

    .prologue
    .line 160
    if-nez p1, :cond_0

    .line 162
    const-string v0, "notifyURLResult result fail"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 164
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, p0, p1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestURL;->notify(Lcom/sec/android/app/samsungapps/vlibrary/net/Request;ZLcom/sec/android/app/samsungapps/vlibrary/net/NetError;)V

    .line 165
    return-void
.end method


# virtual methods
.method public cancel()Z
    .locals 1

    .prologue
    .line 202
    const/4 v0, 0x0

    return v0
.end method

.method public getHttpGet()Lorg/apache/http/client/methods/HttpGet;
    .locals 3

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestURL;->mURL:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestURL;->mURL:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestURL;->mURL:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 51
    :cond_0
    const-string v0, "mURL is not valid. HttpGet is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 52
    const/4 v0, 0x0

    .line 66
    :goto_0
    return-object v0

    .line 55
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestURL;->mURL:Ljava/lang/String;

    const/16 v1, 0x20

    const/16 v2, 0x2b

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    .line 59
    :try_start_0
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestURL;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 66
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestURL;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    goto :goto_0

    .line 62
    :catch_0
    move-exception v0

    .line 64
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RequestURL::getHttpGet::IllegalArgumentException, "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public getResponse()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestURL;->mResponse:Ljava/lang/String;

    return-object v0
.end method

.method public getURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestURL;->mURL:Ljava/lang/String;

    return-object v0
.end method

.method protected handleResultInUIThread()V
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestURL;->handler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 144
    return-void
.end method

.method public isCanceled()Z
    .locals 1

    .prologue
    .line 211
    const/4 v0, 0x0

    return v0
.end method

.method public isSucceed()Z
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestURL;->mResponse:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 182
    const/4 v0, 0x0

    .line 184
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public needRetry()Z
    .locals 2

    .prologue
    .line 192
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestURL;->retryCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestURL;->retryCount:I

    .line 193
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestURL;->retryCount:I

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onResult(Lorg/apache/http/HttpResponse;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 74
    if-nez p1, :cond_0

    .line 76
    const-string v0, "RequestURL response result is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 77
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestURL;->handleResultInUIThread()V

    .line 139
    :goto_0
    return-void

    .line 81
    :cond_0
    const/4 v0, 0x0

    .line 84
    :try_start_0
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v1

    .line 93
    new-instance v2, Ljava/io/BufferedInputStream;

    const/16 v3, 0x800

    invoke-direct {v2, v1, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 94
    new-instance v1, Lorg/apache/http/util/ByteArrayBuffer;

    const/16 v3, 0x800

    invoke-direct {v1, v3}, Lorg/apache/http/util/ByteArrayBuffer;-><init>(I)V

    .line 95
    const/16 v3, 0x800

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 103
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    .line 110
    :goto_1
    const/4 v4, 0x0

    const/16 v5, 0x800

    :try_start_1
    invoke-virtual {v2, v3, v4, v5}, Ljava/io/BufferedInputStream;->read([BII)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v4

    .line 118
    const/4 v5, -0x1

    if-eq v4, v5, :cond_1

    .line 120
    invoke-virtual {v1, v3, v6, v4}, Lorg/apache/http/util/ByteArrayBuffer;->append([BII)V

    goto :goto_1

    .line 97
    :catch_0
    move-exception v1

    .line 99
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "RequestURL::onResult::IOException, "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    .line 137
    :goto_2
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestURL;->mResponse:Ljava/lang/String;

    .line 138
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestURL;->handleResultInUIThread()V

    goto :goto_0

    .line 112
    :catch_1
    move-exception v2

    .line 114
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "RequestURL::onResult::IOException, "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    .line 126
    :cond_1
    invoke-virtual {v1}, Lorg/apache/http/util/ByteArrayBuffer;->length()I

    move-result v2

    if-nez v2, :cond_2

    .line 128
    const-string v1, "RequestURL::onResult::ByteArrayBuffer length is zero"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->e(Ljava/lang/String;)V

    goto :goto_2

    .line 133
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-virtual {v1}, Lorg/apache/http/util/ByteArrayBuffer;->toByteArray()[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    goto :goto_2
.end method
