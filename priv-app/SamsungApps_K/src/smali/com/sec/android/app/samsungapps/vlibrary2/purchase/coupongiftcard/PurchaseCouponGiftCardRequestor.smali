.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/PurchaseCouponGiftCardRequestor;
.super Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;
.source "ProGuard"


# instance fields
.field private _IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftCardReceiver;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardRequestor;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftCardReceiver;)V

    .line 30
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/PurchaseCouponGiftCardRequestor;->_IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

    .line 31
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/PurchaseCouponGiftCardRequestor;)Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/PurchaseCouponGiftCardRequestor;->_IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

    return-object v0
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 2

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/PurchaseCouponGiftCardRequestor;->_ICouponGiftCardReceiver:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftCardReceiver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftCardReceiver;->clear()V

    .line 36
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/PurchaseCouponGiftCardRequestor;->_CouponContainer:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;

    .line 37
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;

    const/high16 v1, 0x10000

    invoke-direct {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/PurchaseCouponGiftCardRequestor;->_ActiveGiftCardList:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/GiftCardList;

    .line 38
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/PurchaseCouponGiftCardRequestor;->requestActiveGiftCardList()V

    .line 39
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/PurchaseCouponGiftCardRequestor;->requestCouponList()V

    .line 40
    return-void
.end method

.method protected iscompleteReceive()Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/PurchaseCouponGiftCardRequestor;->_bCouponResult:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/PurchaseCouponGiftCardRequestor;->_bGiftCardResult:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected requestCouponList()V
    .locals 4

    .prologue
    .line 60
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/f;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/f;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/PurchaseCouponGiftCardRequestor;)V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/PurchaseCouponGiftCardRequestor;->_CouponContainer:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/CouponContainer;

    new-instance v3, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/g;

    invoke-direct {v3, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/g;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/PurchaseCouponGiftCardRequestor;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->purchaseCouponList(Lcom/sec/android/app/samsungapps/vlibrary2/requestbuilder/IPurchaseCouponQueryCondition;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 79
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/PurchaseCouponGiftCardRequestor;->_Context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;->applyFakeModelIfNeeds(Landroid/content/Context;)V

    .line 81
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 82
    return-void
.end method
