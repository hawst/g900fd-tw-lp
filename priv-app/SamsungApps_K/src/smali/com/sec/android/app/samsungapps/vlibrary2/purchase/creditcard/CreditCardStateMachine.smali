.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static instance:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine;


# instance fields
.field private _State:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;->BEGIN:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine;->_State:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    .line 180
    return-void
.end method

.method private entryAction(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;)V
    .locals 2

    .prologue
    .line 136
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/e;->b:[I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine;->_State:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 168
    :goto_0
    :pswitch_0
    return-void

    .line 140
    :pswitch_1
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->requestCardSort()V

    .line 141
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->requestCardStateInfo()V

    .line 142
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->startLoading()V

    goto :goto_0

    .line 147
    :pswitch_2
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->endLoading()V

    .line 148
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->invokeView()V

    goto :goto_0

    .line 153
    :pswitch_3
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->endLoading()V

    .line 154
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->sendFailSignal()V

    goto :goto_0

    .line 159
    :pswitch_4
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->startLoading()V

    goto :goto_0

    .line 164
    :pswitch_5
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->sendRegisterSuccessSignal()V

    goto :goto_0

    .line 136
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method private exitAction(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;)V
    .locals 2

    .prologue
    .line 172
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/e;->b:[I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine;->_State:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 178
    :goto_0
    return-void

    .line 175
    :pswitch_0
    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;->endLoading()V

    goto :goto_0

    .line 172
    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_0
    .end packed-switch
.end method

.method public static getInstance()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine;

    if-nez v0, :cond_0

    .line 11
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine;-><init>()V

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine;

    .line 13
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine;->instance:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine;

    return-object v0
.end method

.method private trans(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;)V
    .locals 0

    .prologue
    .line 129
    invoke-direct {p0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine;->exitAction(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;)V

    .line 130
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine;->_State:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    .line 131
    invoke-direct {p0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine;->entryAction(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;)V

    .line 132
    return-void
.end method


# virtual methods
.method public execute(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;)V
    .locals 3

    .prologue
    .line 19
    const-string v0, "hcjo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine;->_State:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 20
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/e;->b:[I

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine;->_State:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 125
    :goto_0
    return-void

    .line 26
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/e;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 29
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;->READY:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    invoke-direct {p0, v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine;->trans(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;)V

    goto :goto_0

    .line 36
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/e;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 39
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;->RECEIVED_CARDSORT_STATE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    invoke-direct {p0, v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine;->trans(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;)V

    goto :goto_0

    .line 42
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;->RECIVED_STATELIST_STATE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    invoke-direct {p0, v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine;->trans(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;)V

    goto :goto_0

    .line 49
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/e;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    goto :goto_0

    .line 52
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;->INVOKE_VIEW_STATE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    invoke-direct {p0, v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine;->trans(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;)V

    goto :goto_0

    .line 55
    :pswitch_7
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;->REGISTERFAIL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    invoke-direct {p0, v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine;->trans(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;)V

    goto :goto_0

    .line 62
    :pswitch_8
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/e;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_4

    :pswitch_9
    goto :goto_0

    .line 65
    :pswitch_a
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;->INVOKE_VIEW_STATE:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    invoke-direct {p0, v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine;->trans(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;)V

    goto :goto_0

    .line 68
    :pswitch_b
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;->REGISTERFAIL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    invoke-direct {p0, v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine;->trans(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;)V

    goto :goto_0

    .line 75
    :pswitch_c
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/e;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_5

    goto :goto_0

    .line 79
    :pswitch_d
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;->REGISTERFAIL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    invoke-direct {p0, v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine;->trans(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;)V

    goto :goto_0

    .line 84
    :pswitch_e
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;->WAITING_USER_SUBMIT:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    invoke-direct {p0, v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine;->trans(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;)V

    goto/16 :goto_0

    .line 92
    :pswitch_f
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/e;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_6

    goto/16 :goto_0

    .line 96
    :pswitch_10
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;->REGISTERFAIL:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    invoke-direct {p0, v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine;->trans(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;)V

    goto/16 :goto_0

    .line 101
    :pswitch_11
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;->REGISTERING:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    invoke-direct {p0, v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine;->trans(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;)V

    goto/16 :goto_0

    .line 109
    :pswitch_12
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/e;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent;->getEventType()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardEvent$CreditCardEventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_7

    goto/16 :goto_0

    .line 113
    :pswitch_13
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;->WAITING_USER_SUBMIT:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    invoke-direct {p0, v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine;->trans(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;)V

    goto/16 :goto_0

    .line 118
    :pswitch_14
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;->SUCCESS:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;

    invoke-direct {p0, v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine;->trans(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateMachine$CreditCardState;Lcom/sec/android/app/samsungapps/vlibrary2/purchase/creditcard/CreditCardStateContext;)V

    goto/16 :goto_0

    .line 20
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_5
        :pswitch_8
        :pswitch_c
        :pswitch_f
        :pswitch_12
    .end packed-switch

    .line 26
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch

    .line 36
    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 49
    :pswitch_data_3
    .packed-switch 0x3
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 62
    :pswitch_data_4
    .packed-switch 0x2
        :pswitch_a
        :pswitch_9
        :pswitch_b
    .end packed-switch

    .line 75
    :pswitch_data_5
    .packed-switch 0x5
        :pswitch_d
        :pswitch_e
    .end packed-switch

    .line 92
    :pswitch_data_6
    .packed-switch 0x7
        :pswitch_10
        :pswitch_11
    .end packed-switch

    .line 109
    :pswitch_data_7
    .packed-switch 0x9
        :pswitch_13
        :pswitch_14
    .end packed-switch
.end method
