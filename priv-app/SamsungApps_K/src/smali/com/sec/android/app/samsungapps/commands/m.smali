.class final Lcom/sec/android/app/samsungapps/commands/m;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;)V
    .locals 0

    .prologue
    .line 158
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/commands/m;->a:Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final invoke(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/commands/m;->a:Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/commands/AccountCommandBuilder;->isTokenBasedSamsungAccountInstalled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    const-class v0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountNewVersionHandlerActivity;

    invoke-static {p1, v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->startActivityWithObject(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/Object;)V

    .line 170
    :goto_0
    return-void

    .line 168
    :cond_0
    const-class v0, Lcom/sec/android/app/samsungapps/invisibleActivity/SamsungAccountHandlerActivity;

    invoke-static {p1, v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary2/activityobjectlinker/ActivityObjectLinker;->startActivityWithObject(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0
.end method
