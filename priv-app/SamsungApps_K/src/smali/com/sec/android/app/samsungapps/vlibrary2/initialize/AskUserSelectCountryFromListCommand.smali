.class public Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _CountryListMap:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/CountryListMap;

.field private _IAskUserSelectCountryFromListCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand$IAskUserSelectCountryFromListCommandData;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand$IAskUserSelectCountryFromListCommandData;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 18
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/CountryListMap;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/CountryListMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand;->_CountryListMap:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/CountryListMap;

    .line 22
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand;->_IAskUserSelectCountryFromListCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand$IAskUserSelectCountryFromListCommandData;

    .line 23
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand;Z)V
    .locals 0

    .prologue
    .line 16
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand;->_Context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand;)Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand$IAskUserSelectCountryFromListCommandData;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand;->_IAskUserSelectCountryFromListCommandData:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand$IAskUserSelectCountryFromListCommandData;

    return-object v0
.end method


# virtual methods
.method public getCountryListMap()Lcom/sec/android/app/samsungapps/vlibrary2/initialize/CountryListMap;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand;->_CountryListMap:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/CountryListMap;

    return-object v0
.end method

.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 0

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand;->requestAvailableCountryList()V

    .line 28
    return-void
.end method

.method protected requestAvailableCountryList()V
    .locals 3

    .prologue
    .line 32
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand;->_CountryListMap:Lcom/sec/android/app/samsungapps/vlibrary2/initialize/CountryListMap;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/b;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->availableCountryList(Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 44
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 45
    return-void
.end method

.method public selectCountry(Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;)V
    .locals 2

    .prologue
    .line 49
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getCountry()Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;

    move-result-object v0

    .line 50
    if-nez v0, :cond_0

    .line 52
    const-string v0, "error"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->err(Ljava/lang/String;)V

    .line 58
    :goto_0
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getNetHeaderInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/NetHeaderInfo;->selectCountry(Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;)V

    .line 59
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary2/initialize/AskUserSelectCountryFromListCommand;->onFinalResult(Z)V

    .line 60
    return-void

    .line 56
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getDeviceInfoLoader()Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->getDataExchanger()Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Country;->setLoaders(Lcom/sec/android/app/samsungapps/vlibrary/doc/DeviceInfoLoader;Lcom/sec/android/app/samsungapps/vlibrary/doc/DataExchanger;)V

    goto :goto_0
.end method
