.class public Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary3/statemachine/IStateContext;


# static fields
.field private static c:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$State;

.field private static e:J


# instance fields
.field a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

.field b:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;

.field private d:Landroid/os/Handler;

.field private f:Landroid/content/Context;

.field private g:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

.field private h:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 23
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$State;->NOT_CONFIRMED:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$State;

    sput-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$State;

    .line 25
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->e:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->d:Landroid/os/Handler;

    .line 106
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    .line 198
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;

    invoke-direct {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->b:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;

    .line 33
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->f:Landroid/content/Context;

    .line 34
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->g:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    .line 35
    iput-object p3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->h:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    .line 36
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;)Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->h:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Event;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Event;)V

    return-void
.end method

.method private a(Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Event;)V
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->d:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/a;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Event;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 47
    return-void
.end method


# virtual methods
.method public addObserver(Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager$IPwordConfirmObserver;)V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;->add(Ljava/lang/Object;)Z

    .line 111
    return-void
.end method

.method public cancelConfirm()V
    .locals 1

    .prologue
    .line 245
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Event;->P2CONFIRM_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Event;)V

    .line 246
    return-void
.end method

.method public check()V
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Event;->CHECK:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Event;)V

    .line 52
    return-void
.end method

.method public confirmPassword(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 161
    const-string v0, ""

    .line 162
    if-eqz p1, :cond_0

    .line 164
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 166
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->f:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isExistSamsungAccount(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->f:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/account/SamsungAccountValidator;->isSAccountVersionHigherThan1_3_001(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 168
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getEmailID()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/d;

    invoke-direct {v2, v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->b:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/CMapContainer;

    new-instance v4, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/b;

    invoke-direct {v4, p0}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/b;-><init>(Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;)V

    invoke-virtual {v0, v2, v1, v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->login(Lcom/sec/android/app/samsungapps/vlibrary2/account/ILoginParam;ZLcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->h:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->h:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->startLoading()V

    .line 174
    :cond_1
    :goto_0
    return-void

    .line 172
    :cond_2
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getAccountInfo()Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/AccountInfo;->getPassword()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Event;->P2CONFIRMED:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Event;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;->clone()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager$IPwordConfirmObserver;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager$IPwordConfirmObserver;->onInvalidPassword()V

    goto :goto_1
.end method

.method public getState()Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$State;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$State;

    return-object v0
.end method

.method public bridge synthetic getState()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->getState()Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$State;

    move-result-object v0

    return-object v0
.end method

.method public onAction(Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Action;)V
    .locals 3

    .prologue
    .line 66
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/c;->a:[I

    invoke-virtual {p1}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Action;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 86
    :cond_0
    :goto_0
    return-void

    .line 69
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->onPasswordConfirm()V

    goto :goto_0

    .line 72
    :pswitch_1
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->e:J

    goto :goto_0

    .line 75
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;->clone()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager$IPwordConfirmObserver;

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager$IPwordConfirmObserver;->onConfirmResult(Z)V

    goto :goto_1

    .line 79
    :pswitch_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->e:J

    goto :goto_0

    .line 82
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;->clone()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager$IPwordConfirmObserver;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager$IPwordConfirmObserver;->onConfirmResult(Z)V

    goto :goto_2

    .line 66
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public bridge synthetic onAction(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Action;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->onAction(Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Action;)V

    return-void
.end method

.method public onConfirmPasswordFailed()V
    .locals 1

    .prologue
    .line 155
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Event;->P2CONFIRM_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Event;)V

    .line 156
    return-void
.end method

.method public onConfirmPasswordSuccess()V
    .locals 1

    .prologue
    .line 150
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Event;->P2CONFIRMED:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Event;)V

    .line 151
    return-void
.end method

.method public onLogedOut()V
    .locals 1

    .prologue
    .line 120
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Event;->LOGED_OUT:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Event;)V

    .line 121
    return-void
.end method

.method public onManualLogedInForPayment()V
    .locals 1

    .prologue
    .line 125
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Event;->LOGED_IN_MANUAL_FOR_PAYMENT:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Event;)V

    .line 126
    return-void
.end method

.method protected onPasswordConfirm()V
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->g:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->g:Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->f:Landroid/content/Context;

    invoke-interface {v0, v1, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/viewinvoker/IViewInvoker;->invoke(Landroid/content/Context;Ljava/lang/Object;)V

    .line 138
    :goto_0
    return-void

    .line 136
    :cond_0
    sget-object v0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Event;->P2CONFIRM_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Event;

    invoke-direct {p0, v0}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->a(Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$Event;)V

    goto :goto_0
.end method

.method public removeObserver(Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager$IPwordConfirmObserver;)V
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->a:Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/primitives/ThreadSafeArrayList;->remove(Ljava/lang/Object;)Z

    .line 116
    return-void
.end method

.method public setState(Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$State;)V
    .locals 0

    .prologue
    .line 56
    sput-object p1, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->c:Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$State;

    .line 57
    return-void
.end method

.method public bridge synthetic setState(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$State;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmManager;->setState(Lcom/sec/android/app/samsungapps/vlibrary3/account/passwordconfirm/PwordConfirmStateMachine$State;)V

    return-void
.end method
