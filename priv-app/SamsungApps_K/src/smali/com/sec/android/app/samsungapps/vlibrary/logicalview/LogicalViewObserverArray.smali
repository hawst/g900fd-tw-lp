.class public Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalViewObserverArray;
.super Ljava/util/ArrayList;
.source "ProGuard"


# static fields
.field private static final serialVersionUID:J = -0x5b0aa3aceb1c31a2L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    return-void
.end method


# virtual methods
.method public getEnum()[Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalViewObserver;
    .locals 5

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalViewObserverArray;->size()I

    move-result v0

    .line 16
    if-nez v0, :cond_0

    .line 17
    const/4 v0, 0x0

    .line 25
    :goto_0
    return-object v0

    .line 19
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalViewObserverArray;->size()I

    move-result v0

    new-array v3, v0, [Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalViewObserver;

    .line 20
    const/4 v0, 0x0

    .line 21
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalViewObserverArray;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/logicalview/LogicalViewObserver;

    .line 23
    add-int/lit8 v2, v1, 0x1

    aput-object v0, v3, v1

    move v1, v2

    goto :goto_1

    :cond_1
    move-object v0, v3

    .line 25
    goto :goto_0
.end method
