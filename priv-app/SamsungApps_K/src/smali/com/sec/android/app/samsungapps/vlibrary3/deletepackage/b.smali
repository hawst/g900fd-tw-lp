.class final Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/b;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/concreteloader/OnPackageDeleted;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;)V
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/b;->a:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final packageDeleted(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 108
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/b;->a:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;->DELETE_SUCCEED:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;->a(Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;)V

    .line 116
    :goto_0
    return-void

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/b;->a:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;->DELETE_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;->a(Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;)V

    goto :goto_0
.end method

.method public final packageDeleted(Z)V
    .locals 2

    .prologue
    .line 120
    if-eqz p1, :cond_0

    .line 122
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/b;->a:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;->DELETE_SUCCEED:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;->a(Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;)V

    .line 129
    :goto_0
    return-void

    .line 126
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/b;->a:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;

    sget-object v1, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;->DELETE_FAILED:Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;

    invoke-static {v0, v1}, Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;->a(Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackage;Lcom/sec/android/app/samsungapps/vlibrary3/deletepackage/DeletePackageStateMachine$Event;)V

    goto :goto_0
.end method
