.class public Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private mScreenShot:Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;

.field private mSel:I


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;I)V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;->mScreenShot:Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;

    .line 11
    iput p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;->mSel:I

    .line 12
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;->mScreenShot:Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;->getImgCount()I

    move-result v0

    return v0
.end method

.method public getScreenShot()Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;->mScreenShot:Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;

    return-object v0
.end method

.method public getSel()I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;->mSel:I

    return v0
.end method

.method public getURL(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;->mScreenShot:Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ScreenShot;->getImageURL(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setSel(I)V
    .locals 0

    .prologue
    .line 31
    iput p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;->mSel:I

    .line 32
    return-void
.end method
