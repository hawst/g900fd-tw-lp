.class public Lcom/sec/android/app/samsungapps/view/ScreenShotPagerAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "ProGuard"


# instance fields
.field private a:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;

.field private b:Landroid/app/Activity;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 20
    check-cast p1, Landroid/app/Activity;

    iput-object p1, p0, Lcom/sec/android/app/samsungapps/view/ScreenShotPagerAdapter;->b:Landroid/app/Activity;

    .line 21
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/view/ScreenShotPagerAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;

    .line 22
    return-void
.end method


# virtual methods
.method public destroyItem(Landroid/view/View;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 50
    check-cast p1, Landroid/support/v4/view/ViewPager;

    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/support/v4/view/ViewPager;->removeView(Landroid/view/View;)V

    .line 51
    return-void
.end method

.method public finishUpdate(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 61
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ScreenShotPagerAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;->getCount()I

    move-result v0

    return v0
.end method

.method public instantiateItem(Landroid/view/View;I)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 31
    new-instance v1, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ScreenShotPagerAdapter;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;-><init>(Landroid/content/Context;)V

    .line 32
    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setFocusable(Z)V

    .line 33
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ScreenShotPagerAdapter;->b:Landroid/app/Activity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/initializer/Global;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/initializer/Global;->getDocument()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getNetAPI()Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setNetAPI(Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;)V

    .line 34
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ScreenShotPagerAdapter;->a:Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/samsungapps/vlibrary/eventmanager/eventInterface/ScreenShotInterface;->getURL(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setURL(Ljava/lang/String;)V

    .line 35
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 36
    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setAdjustViewBounds(Z)V

    .line 37
    invoke-virtual {v1, v2}, Lcom/sec/android/app/samsungapps/view/CacheWebImageView;->setScrollbarFadingEnabled(Z)V

    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ScreenShotPagerAdapter;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/view/ScreenShotPagerAdapter;->b:Landroid/app/Activity;

    check-cast v0, Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;

    invoke-virtual {v0, v2, v1, p2}, Lcom/sec/android/app/samsungapps/ScreenShotViewPagerActivity;->setImageScaleType(Landroid/content/res/Configuration;Lcom/sec/android/app/samsungapps/view/CacheWebImageView;I)V

    .line 43
    check-cast p1, Landroid/support/v4/view/ViewPager;

    const/4 v0, 0x0

    invoke-virtual {p1, v1, v0}, Landroid/support/v4/view/ViewPager;->addView(Landroid/view/View;I)V

    .line 45
    return-object v1
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 55
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public restoreState(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
    .locals 0

    .prologue
    .line 58
    return-void
.end method

.method public saveState()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    return-object v0
.end method

.method public startUpdate(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 60
    return-void
.end method
