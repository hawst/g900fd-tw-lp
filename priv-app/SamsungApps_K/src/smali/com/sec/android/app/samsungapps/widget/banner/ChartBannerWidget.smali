.class public Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;
.super Lcom/sec/android/app/samsungapps/widget/CommonWidget;
.source "ProGuard"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field public static final CHART_FIRST_ITEM_ROUND:I = 0xb

.field public static final CHART_ITEM_ROUND:I = 0x10

.field public static final COLUMNS_NUM_LANDSCAPE:I = 0x6

.field public static final COLUMNS_NUM_PORTRAIT:I = 0x3


# instance fields
.field a:Landroid/content/Context;

.field b:Z

.field private d:Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;

.field private e:Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerWidgetData;

.field private f:Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerClickListener;

.field private g:Landroid/widget/GridView;

.field private h:I

.field private i:I

.field private j:I

.field private k:Z

.field private l:Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;

.field private m:Ljava/util/ArrayList;

.field private n:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 69
    invoke-direct {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;)V

    .line 46
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->h:I

    .line 47
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->i:I

    .line 48
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->j:I

    .line 49
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->k:Z

    .line 50
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->l:Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->m:Ljava/util/ArrayList;

    .line 54
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->n:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 55
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->b:Z

    .line 70
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->a:Landroid/content/Context;

    .line 71
    const v0, 0x7f040068

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->initView(Landroid/content/Context;I)V

    .line 72
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 76
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->h:I

    .line 47
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->i:I

    .line 48
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->j:I

    .line 49
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->k:Z

    .line 50
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->l:Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->m:Ljava/util/ArrayList;

    .line 54
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->n:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 55
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->b:Z

    .line 77
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->a:Landroid/content/Context;

    .line 78
    const v0, 0x7f040068

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->initView(Landroid/content/Context;I)V

    .line 79
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 83
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->h:I

    .line 47
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->i:I

    .line 48
    iput v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->j:I

    .line 49
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->k:Z

    .line 50
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->l:Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->m:Ljava/util/ArrayList;

    .line 54
    iput-object v2, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->n:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 55
    iput-boolean v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->b:Z

    .line 84
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->a:Landroid/content/Context;

    .line 85
    const v0, 0x7f040068

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->initView(Landroid/content/Context;I)V

    .line 86
    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;)Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerClickListener;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->f:Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerClickListener;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;Z)Z
    .locals 0

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->k:Z

    return p1
.end method

.method static synthetic b(Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;)Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->n:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    return-object v0
.end method

.method public static calcChartBannerSize(Landroid/content/Context;I)I
    .locals 4

    .prologue
    .line 408
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 409
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    .line 410
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 412
    const v3, 0x7f070005

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 413
    iget v3, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 417
    iget v0, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    .line 418
    const/4 v0, 0x6

    .line 423
    :goto_0
    sub-int v2, v3, p1

    div-int v0, v2, v0

    .line 425
    if-le v0, v1, :cond_0

    move v0, v1

    .line 429
    :cond_0
    return v0

    .line 420
    :cond_1
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public static getRoundedCornerBitmap(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 481
    if-nez p0, :cond_0

    .line 483
    const-string v0, "BigBannerContentHolder::getRoundedCornerBitmap:: bitmap is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->err(Ljava/lang/String;)V

    .line 484
    const/4 v0, 0x0

    .line 504
    :goto_0
    return-object v0

    .line 487
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 488
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 490
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 492
    new-instance v3, Landroid/graphics/Rect;

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-direct {v3, v6, v6, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 493
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4, v3}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 494
    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 497
    invoke-virtual {v1, v6, v6, v6, v6}, Landroid/graphics/Canvas;->drawARGB(IIII)V

    .line 498
    const v5, -0xbdbdbe

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 499
    invoke-virtual {v1, v4, p1, p1, v2}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 501
    new-instance v4, Landroid/graphics/PorterDuffXfermode;

    sget-object v5, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v4, v5}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 502
    invoke-virtual {v1, p0, v3, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0
.end method


# virtual methods
.method public getAdapter()Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->d:Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;

    return-object v0
.end method

.method public getWidgetData()Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerWidgetData;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->e:Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerWidgetData;

    return-object v0
.end method

.method public initHover()V
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->d:Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->d:Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;->isHovered()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 90
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->updateWidget()V

    .line 92
    :cond_0
    return-void
.end method

.method public loadWidget()V
    .locals 2

    .prologue
    .line 238
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->e:Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerWidgetData;

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->e:Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerWidgetData;

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/banner/o;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/banner/o;-><init>(Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;)V

    invoke-interface {v0, v1}, Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerWidgetData;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)V

    .line 253
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 403
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 404
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->refreshWidget()V

    .line 405
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    .prologue
    .line 259
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->f:Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerClickListener;

    if-eqz v0, :cond_1

    .line 260
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->h:I

    if-ge p3, v0, :cond_2

    .line 261
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 262
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductID()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 263
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->f:Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerClickListener;

    invoke-interface {v1, v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerClickListener;->onContentClick(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;)V

    .line 270
    :cond_0
    :goto_0
    const v0, 0x7f0c0192

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 271
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 273
    :cond_1
    return-void

    .line 266
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->b:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 267
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->f:Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerClickListener;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerClickListener;->onMoreClick()V

    goto :goto_0
.end method

.method public orientationsize()V
    .locals 10

    .prologue
    const v9, 0x7f0c0142

    .line 363
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 364
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 365
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    .line 367
    const v3, 0x7f07001c

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 368
    const v4, 0x7f07001d

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 369
    const v5, 0x7f07001e

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 370
    const v6, 0x7f070005

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 371
    const v7, 0x7f07001f

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 372
    const v8, 0x7f070021

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 374
    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    sub-int/2addr v1, v3

    sub-int/2addr v1, v4

    sub-int/2addr v1, v5

    sub-int v0, v1, v0

    .line 376
    iget v1, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 377
    invoke-virtual {p0, v9}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 378
    invoke-virtual {p0, v9}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    sub-int/2addr v0, v1

    .line 383
    :goto_0
    div-int v1, v0, v6

    .line 384
    mul-int v2, v1, v6

    add-int/lit8 v3, v1, -0x1

    mul-int/2addr v3, v7

    add-int/2addr v2, v3

    if-ge v0, v2, :cond_2

    .line 385
    add-int/lit8 v0, v1, -0x1

    .line 388
    :goto_1
    mul-int v1, v0, v6

    add-int/lit8 v2, v0, -0x1

    mul-int/2addr v2, v7

    add-int/2addr v1, v2

    .line 390
    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->j:I

    .line 391
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->j:I

    mul-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->h:I

    .line 393
    iget v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->i:I

    .line 394
    const v0, 0x7f0c00ee

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    .line 395
    if-eqz v0, :cond_0

    .line 396
    invoke-virtual {v0}, Landroid/widget/GridView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 398
    :cond_0
    return-void

    .line 380
    :cond_1
    invoke-virtual {p0, v9}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public refreshWidget()V
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 278
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 282
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->e:Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerWidgetData;

    if-nez v0, :cond_1

    .line 359
    :cond_0
    :goto_0
    return-void

    .line 287
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->e:Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerWidgetData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerWidgetData;->getContentCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 289
    invoke-virtual {p0, v8}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->setVisibleNodata(I)Z

    goto :goto_0

    .line 293
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 294
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->orientationsize()V

    .line 295
    iput-boolean v8, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->b:Z

    .line 297
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->e:Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerWidgetData;

    invoke-interface {v0, v8}, Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerWidgetData;->getContent(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->n:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 299
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->n:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    if-eqz v0, :cond_3

    .line 300
    const v0, 0x7f0c01a0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->l:Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;

    .line 301
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->l:Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->n:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->setContent(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;)V

    .line 302
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->l:Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->loadWidget()V

    .line 303
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->l:Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->refreshWidget()V

    .line 305
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->e:Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerWidgetData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerWidgetData;->getContentCount()I

    move-result v1

    move v0, v7

    .line 306
    :goto_1
    if-ge v0, v1, :cond_4

    .line 308
    iget v2, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->i:I

    if-ne v0, v2, :cond_5

    .line 310
    iput-boolean v7, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->b:Z

    .line 325
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 326
    add-int/lit8 v1, v0, 0x1

    iget v2, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->i:I

    if-gt v1, v2, :cond_8

    .line 327
    add-int/lit8 v0, v0, 0x1

    :goto_2
    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->i:I

    if-ge v0, v1, :cond_8

    .line 328
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->m:Ljava/util/ArrayList;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-direct {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;-><init>()V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 327
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 314
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->e:Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerWidgetData;

    invoke-interface {v2, v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerWidgetData;->getContent(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    move-result-object v2

    .line 316
    if-eqz v2, :cond_7

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->a:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;->isRemoveContent(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 317
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->m:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 306
    :cond_6
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 319
    :cond_7
    if-eqz v2, :cond_6

    .line 320
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "JunosPulse checked : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->i(Ljava/lang/String;)V

    goto :goto_3

    .line 332
    :cond_8
    new-instance v0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->a:Landroid/content/Context;

    const v2, 0x7f040067

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->m:Ljava/util/ArrayList;

    const/16 v4, 0x63

    iget v5, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->h:I

    iget-boolean v6, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->b:Z

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;IIZ)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->setAdapter(Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;)V

    .line 333
    const v0, 0x7f0c00ee

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->g:Landroid/widget/GridView;

    .line 334
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->g:Landroid/widget/GridView;

    invoke-virtual {v0, v8}, Landroid/widget/GridView;->setFocusableInTouchMode(Z)V

    .line 335
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->g:Landroid/widget/GridView;

    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->j:I

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 336
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->g:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->d:Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 337
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->g:Landroid/widget/GridView;

    invoke-virtual {v0, p0}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 339
    const v0, 0x7f0c019d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 340
    const v1, 0x7f0c019e

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 341
    if-eqz v0, :cond_9

    iget-boolean v2, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->b:Z

    if-ne v2, v7, :cond_9

    .line 342
    invoke-virtual {v0, v7}, Landroid/view/View;->setFocusable(Z)V

    .line 343
    invoke-virtual {v0, v8}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 344
    new-instance v2, Lcom/sec/android/app/samsungapps/widget/banner/p;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/widget/banner/p;-><init>(Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 352
    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    .line 357
    :goto_4
    const v0, 0x7f0c019f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 358
    invoke-virtual {p0, v9}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->setVisibleLoading(I)Z

    goto/16 :goto_0

    .line 354
    :cond_9
    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 218
    invoke-super {p0}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->release()V

    .line 219
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->m:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 221
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->m:Ljava/util/ArrayList;

    .line 223
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->d:Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;

    if-eqz v0, :cond_1

    .line 224
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->d:Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;

    .line 226
    :cond_1
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->e:Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerWidgetData;

    .line 227
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->f:Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerClickListener;

    .line 228
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->n:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 229
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->l:Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;

    if-eqz v0, :cond_2

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->l:Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->release()V

    .line 231
    iput-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->l:Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;

    .line 233
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->removeAllViews()V

    .line 234
    return-void
.end method

.method public setAdapter(Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;)V
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->d:Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;

    .line 113
    return-void
.end method

.method public setListener(Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerClickListener;)V
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->f:Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerClickListener;

    .line 97
    return-void
.end method

.method public setVisibleNodata(I)Z
    .locals 2

    .prologue
    .line 460
    invoke-super {p0, p1}, Lcom/sec/android/app/samsungapps/widget/CommonWidget;->setVisibleNodata(I)Z

    .line 462
    if-nez p1, :cond_1

    .line 463
    const v0, 0x7f0c0083

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 464
    if-eqz v0, :cond_0

    .line 465
    const v1, 0x7f080214

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 468
    :cond_0
    const v0, 0x7f0c0081

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 469
    if-eqz v0, :cond_1

    .line 470
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 473
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public setWidgetData(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 102
    check-cast p1, Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerWidgetData;

    iput-object p1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->e:Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerWidgetData;

    .line 103
    return-void
.end method

.method public updateWidget()V
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 122
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 126
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->e:Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerWidgetData;

    if-nez v0, :cond_1

    .line 213
    :cond_0
    :goto_0
    return-void

    .line 131
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->e:Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerWidgetData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerWidgetData;->getContentCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 133
    invoke-virtual {p0, v8}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->setVisibleNodata(I)Z

    goto :goto_0

    .line 137
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 138
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->orientationsize()V

    .line 139
    iput-boolean v8, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->b:Z

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->e:Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerWidgetData;

    invoke-interface {v0, v8}, Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerWidgetData;->getContent(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->n:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->n:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    if-eqz v0, :cond_3

    .line 144
    const v0, 0x7f0c01a0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->l:Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->l:Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->n:Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->setContent(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;)V

    .line 146
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->l:Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->loadWidget()V

    .line 149
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->e:Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerWidgetData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerWidgetData;->getContentCount()I

    move-result v1

    move v0, v7

    .line 150
    :goto_1
    if-ge v0, v1, :cond_4

    .line 152
    iget v2, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->i:I

    if-ne v0, v2, :cond_5

    .line 154
    iput-boolean v7, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->b:Z

    .line 169
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 170
    add-int/lit8 v1, v0, 0x1

    iget v2, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->i:I

    if-gt v1, v2, :cond_8

    .line 171
    add-int/lit8 v0, v0, 0x1

    :goto_2
    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->i:I

    if-ge v0, v1, :cond_8

    .line 172
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->m:Ljava/util/ArrayList;

    new-instance v2, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    invoke-direct {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;-><init>()V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 171
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 158
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->e:Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerWidgetData;

    invoke-interface {v2, v0}, Lcom/sec/android/app/samsungapps/widget/interfaces/IChartBannerWidgetData;->getContent(I)Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;

    move-result-object v2

    .line 160
    if-eqz v2, :cond_7

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->a:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/android/app/samsungapps/vlibrary2/content/JunosPulseChecker;->isRemoveContent(Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 161
    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->m:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 150
    :cond_6
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 163
    :cond_7
    if-eqz v2, :cond_6

    .line 164
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "JunosPulse checked : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Content;->getProductName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->i(Ljava/lang/String;)V

    goto :goto_3

    .line 176
    :cond_8
    new-instance v0, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->a:Landroid/content/Context;

    const v2, 0x7f040067

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->m:Ljava/util/ArrayList;

    const/16 v4, 0x63

    iget v5, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->h:I

    iget-boolean v6, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->b:Z

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;IIZ)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->setAdapter(Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;)V

    .line 177
    const v0, 0x7f0c00ee

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->g:Landroid/widget/GridView;

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->g:Landroid/widget/GridView;

    invoke-virtual {v0, v8}, Landroid/widget/GridView;->setFocusableInTouchMode(Z)V

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->g:Landroid/widget/GridView;

    iget v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->j:I

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->g:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->d:Lcom/sec/android/app/samsungapps/view/ChartArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->g:Landroid/widget/GridView;

    invoke-virtual {v0, p0}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 183
    const v0, 0x7f0c019d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 184
    const v1, 0x7f0c019e

    invoke-virtual {p0, v1}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 185
    if-eqz v0, :cond_9

    iget-boolean v2, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->b:Z

    if-ne v2, v7, :cond_9

    .line 186
    invoke-virtual {v0, v7}, Landroid/view/View;->setFocusable(Z)V

    .line 187
    invoke-virtual {v0, v8}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 188
    new-instance v2, Lcom/sec/android/app/samsungapps/widget/banner/m;

    invoke-direct {v2, p0}, Lcom/sec/android/app/samsungapps/widget/banner/m;-><init>(Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 196
    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    .line 197
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0800d6

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 202
    :goto_4
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->l:Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;

    new-instance v1, Lcom/sec/android/app/samsungapps/widget/banner/n;

    invoke-direct {v1, p0}, Lcom/sec/android/app/samsungapps/widget/banner/n;-><init>(Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerFirstItemWidget;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 211
    const v0, 0x7f0c019f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 212
    invoke-virtual {p0, v9}, Lcom/sec/android/app/samsungapps/widget/banner/ChartBannerWidget;->setVisibleLoading(I)Z

    goto/16 :goto_0

    .line 199
    :cond_9
    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4
.end method
