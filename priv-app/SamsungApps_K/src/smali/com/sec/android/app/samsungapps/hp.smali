.class final Lcom/sec/android/app/samsungapps/hp;
.super Landroid/content/BroadcastReceiver;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/SMS;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/SMS;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/hp;->a:Lcom/sec/android/app/samsungapps/SMS;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 77
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 78
    if-nez v0, :cond_1

    .line 90
    :cond_0
    :goto_0
    return-void

    .line 81
    :cond_1
    const-string v1, "ACTION_MSG_SENT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "android.provider.Telephony.SMS_RECEIVED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    :cond_2
    const-string v0, "PSMS : sms onReceive"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/hp;->a:Lcom/sec/android/app/samsungapps/SMS;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/SMS;->e:Landroid/app/PendingIntent;

    invoke-virtual {v0}, Landroid/app/PendingIntent;->cancel()V

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/hp;->a:Lcom/sec/android/app/samsungapps/SMS;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/SMS;->f:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/hp;->a:Lcom/sec/android/app/samsungapps/SMS;

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/SMS;->a(Lcom/sec/android/app/samsungapps/SMS;)Lcom/sec/android/app/samsungapps/hp;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/hp;->a:Lcom/sec/android/app/samsungapps/SMS;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/SMS;->b(Lcom/sec/android/app/samsungapps/SMS;)Lcom/sec/android/app/samsungapps/hp;

    .line 88
    iget-object v1, p0, Lcom/sec/android/app/samsungapps/hp;->a:Lcom/sec/android/app/samsungapps/SMS;

    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/hp;->getResultCode()I

    move-result v0

    const/4 v2, -0x1

    if-ne v0, v2, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/SMS;->notifySendResult(Z)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method
