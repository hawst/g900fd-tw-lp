.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/ICouponGiftcardContainer;


# instance fields
.field private _ICoupon:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

.field private _IGiftCard:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

.field private _IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

.field private _bEnabled:Z

.field private _bSelect:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;->_IGiftCard:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    .line 10
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;->_ICoupon:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    .line 11
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;->_bSelect:Z

    .line 12
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;->_bEnabled:Z

    .line 23
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;->_ICoupon:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    .line 24
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;->_IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

    .line 25
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;->_IGiftCard:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    .line 10
    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;->_ICoupon:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    .line 11
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;->_bSelect:Z

    .line 12
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;->_bEnabled:Z

    .line 17
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;->_IGiftCard:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    .line 18
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;->_IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

    .line 19
    return-void
.end method


# virtual methods
.method public getCoupon()Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;->_ICoupon:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    return-object v0
.end method

.method public getGiftCard()Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;->_IGiftCard:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    return-object v0
.end method

.method public getID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;->isCoupon()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;->_ICoupon:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;->getCouponID()Ljava/lang/String;

    move-result-object v0

    .line 93
    :goto_0
    return-object v0

    .line 89
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;->isGiftCard()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;->_IGiftCard:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;->getGiftCardCode()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 93
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCoupon()Z
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;->_ICoupon:Lcom/sec/android/app/samsungapps/vlibrary2/coupon/ICoupon;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;->_IPaymentInfo:Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasemethod/IPaymentInfo;->getSelPaymentMethodSpec()Lcom/sec/android/app/samsungapps/vlibrary2/primitives/PaymentMethodSpec;

    move-result-object v0

    if-nez v0, :cond_0

    .line 35
    const/4 v0, 0x0

    .line 55
    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;->_bEnabled:Z

    goto :goto_0
.end method

.method public isGiftCard()Z
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;->_IGiftCard:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/IGiftCard;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSelected()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;->_bSelect:Z

    return v0
.end method

.method public setEnabled(Z)V
    .locals 0

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;->_bEnabled:Z

    .line 30
    return-void
.end method

.method public setSelect(Z)V
    .locals 0

    .prologue
    .line 80
    iput-boolean p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/coupongiftcard/CouponGiftCardSelectable;->_bSelect:Z

    .line 81
    return-void
.end method
