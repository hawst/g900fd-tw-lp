.class public Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadRunner;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/vlibrary/net/threads/RequestQueueConsumer$IRequestQueueConsumerData;


# instance fields
.field private _RequestQueue:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestQueue;

.field private _RequestQueueConsumer:Lcom/sec/android/app/samsungapps/vlibrary/net/threads/RequestQueueConsumer;

.field private _Thread:Ljava/lang/Thread;

.field private _ThreadRunnerLifeListener:Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadRunner$IThreadRunnerLifeListener;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadRunner$IThreadRunnerLifeListener;Lcom/sec/android/app/samsungapps/vlibrary/net/RequestQueue;Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p2, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadRunner;->_RequestQueue:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestQueue;

    .line 17
    new-instance v0, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/RequestQueueConsumer;

    invoke-direct {v0, p3, p0}, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/RequestQueueConsumer;-><init>(Lcom/sec/android/app/samsungapps/vlibrary/net/NetAPI;Lcom/sec/android/app/samsungapps/vlibrary/net/threads/RequestQueueConsumer$IRequestQueueConsumerData;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadRunner;->_RequestQueueConsumer:Lcom/sec/android/app/samsungapps/vlibrary/net/threads/RequestQueueConsumer;

    .line 18
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadRunner;->_ThreadRunnerLifeListener:Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadRunner$IThreadRunnerLifeListener;

    .line 19
    return-void
.end method


# virtual methods
.method public onDestroy()V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadRunner;->_ThreadRunnerLifeListener:Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadRunner$IThreadRunnerLifeListener;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadRunner$IThreadRunnerLifeListener;->onThreadRunnerDestroyed()V

    .line 35
    return-void
.end method

.method public start()V
    .locals 2

    .prologue
    .line 23
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadRunner;->_RequestQueueConsumer:Lcom/sec/android/app/samsungapps/vlibrary/net/threads/RequestQueueConsumer;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadRunner;->_Thread:Ljava/lang/Thread;

    .line 24
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadRunner;->_Thread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 25
    return-void
.end method

.method public take()Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary/net/threads/ThreadRunner;->_RequestQueue:Lcom/sec/android/app/samsungapps/vlibrary/net/RequestQueue;

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/net/RequestQueue;->take()Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;

    move-result-object v0

    return-object v0
.end method
