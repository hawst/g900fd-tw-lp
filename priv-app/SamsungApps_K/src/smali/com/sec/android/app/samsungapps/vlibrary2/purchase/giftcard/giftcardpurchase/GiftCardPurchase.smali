.class public Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchase;
.super Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;
.source "ProGuard"


# instance fields
.field private _IGiftCardPurchaseData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchase$IGiftCardPurchaseData;

.field private _ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

.field private _IMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchase$IGiftCardPurchaseData;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommand;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchase;->_IGiftCardPurchaseData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchase$IGiftCardPurchaseData;

    .line 23
    invoke-interface {p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchase$IGiftCardPurchaseData;->getFinalResultMap()Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchase;->_IMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    .line 24
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchase;)Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchase;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchase;Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;)Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;
    .locals 0

    .prologue
    .line 15
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchase;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchase;Z)V
    .locals 0

    .prologue
    .line 15
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchase;->onFinalResult(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchase;Z)V
    .locals 0

    .prologue
    .line 15
    invoke-virtual {p0, p1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchase;->onFinalResult(Z)V

    return-void
.end method

.method private request()V
    .locals 5

    .prologue
    .line 38
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getRequestBuilder()Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchase;->_IGiftCardPurchaseData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchase$IGiftCardPurchaseData;

    invoke-interface {v1}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchase$IGiftCardPurchaseData;->getProductID()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchase;->_IGiftCardPurchaseData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchase$IGiftCardPurchaseData;

    invoke-interface {v2}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchase$IGiftCardPurchaseData;->getGiftCardCode()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchase;->_IMapContainer:Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;

    new-instance v4, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/a;

    invoke-direct {v4, p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/a;-><init>(Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchase;)V

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/app/samsungapps/vlibrary/xml/RequestBuilder;->giftCardPurchase(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/samsungapps/vlibrary2/responseparser/IMapContainer;Lcom/sec/android/app/samsungapps/vlibrary/net/NetResultReceiver;)Lcom/sec/android/app/samsungapps/vlibrary/net/RequestPOST;

    move-result-object v0

    .line 58
    invoke-static {}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->getInstance()Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/samsungapps/vlibrary/doc/Document;->sendRequest(Lcom/sec/android/app/samsungapps/vlibrary/net/IRequest;)V

    .line 59
    return-void
.end method


# virtual methods
.method protected impExecute(Landroid/content/Context;Lcom/sec/android/app/samsungapps/vlibrary2/command/ICommandResultReceiver;)V
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchase;->_IGiftCardPurchaseData:Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchase$IGiftCardPurchaseData;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchase$IGiftCardPurchaseData;->createLoadingDialog()Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchase;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    .line 29
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchase;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    if-eqz v0, :cond_0

    .line 31
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchase;->_ILoadingDialog:Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;

    invoke-interface {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/loading/ILoadingDialog;->startLoading()V

    .line 33
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchase/giftcard/giftcardpurchase/GiftCardPurchase;->request()V

    .line 34
    return-void
.end method
