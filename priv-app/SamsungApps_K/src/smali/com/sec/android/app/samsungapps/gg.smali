.class final Lcom/sec/android/app/samsungapps/gg;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog$onClickListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)V
    .locals 0

    .prologue
    .line 321
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/gg;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Lcom/sec/android/app/samsungapps/widget/SamsungAppsDialog;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 325
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gg;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->c(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getAdapter()Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gg;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->c(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->getAdapter()Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/view/PurchasedListAdapter;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    .line 327
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gg;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->d(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/vlibrary2/purchasedList/PurchasedListManager;->requestPurchaseHistHide()V

    .line 328
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gg;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->mActionBar:Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/samsungapps/widget/SamsungAppsActionBar;->showOptionItem(Z)V

    .line 329
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gg;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->setHideCheckAllButton()V

    .line 330
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gg;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->e(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/widget/PurchasedListTopButton;->setHideButtonLayout(Z)V

    .line 331
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gg;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    const v1, 0xa0012

    invoke-virtual {v0, v1}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->changeActionBar(I)V

    .line 332
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gg;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    const v1, 0xa0010

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->enableActionItem(IZ)V

    .line 333
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/gg;->a:Lcom/sec/android/app/samsungapps/PurchasedListActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/PurchasedListActivity;->c(Lcom/sec/android/app/samsungapps/PurchasedListActivity;)Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/widget/list/PurchasedListWidget;->onWidgetViewState(I)V

    .line 335
    :cond_0
    return-void
.end method
