.class final Lcom/sec/android/app/samsungapps/bm;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)V
    .locals 0

    .prologue
    .line 3086
    iput-object p1, p0, Lcom/sec/android/app/samsungapps/bm;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 3090
    .line 3091
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 3120
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/bm;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    iget-object v0, v0, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->e:Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailMainWidgetClickListener;

    invoke-interface {v0, v3}, Lcom/sec/android/app/samsungapps/widget/interfaces/IContentDetailMainWidgetClickListener;->onClickTabBtn(I)V

    .line 3121
    return-void

    .line 3094
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/bm;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->showBgImage(I)V

    .line 3095
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/bm;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3096
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->PRODUCT_DETAIL:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_OVERVIEW_TAB:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/bm;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v2

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/bm;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v5}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getVcategoryID()Ljava/lang/String;

    move-result-object v7

    move-object v5, v4

    move-object v6, v4

    move v8, v3

    invoke-static/range {v0 .. v8}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendNormalClickLog(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;Ljava/lang/String;ILcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;Ljava/lang/String;Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;Ljava/lang/String;I)V

    goto :goto_0

    .line 3103
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/bm;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-virtual {v0, v9}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->showBgImage(I)V

    .line 3104
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/bm;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 3105
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->PRODUCT_DETAIL:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_REVIEW_TAB:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/bm;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v2

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/bm;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v5}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getVcategoryID()Ljava/lang/String;

    move-result-object v7

    move-object v5, v4

    move-object v6, v4

    move v8, v3

    invoke-static/range {v0 .. v8}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendNormalClickLog(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;Ljava/lang/String;ILcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;Ljava/lang/String;Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;Ljava/lang/String;I)V

    move v3, v9

    goto :goto_0

    .line 3112
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/bm;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-virtual {v0, v10}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->showBgImage(I)V

    .line 3113
    iget-object v0, p0, Lcom/sec/android/app/samsungapps/bm;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 3114
    sget-object v0, Lcom/sec/android/app/samsungapps/tobelog/LogPage;->PRODUCT_DETAIL:Lcom/sec/android/app/samsungapps/tobelog/LogPage;

    sget-object v1, Lcom/sec/android/app/samsungapps/tobelog/LogEvent;->CLICK_RELATED_TAB:Lcom/sec/android/app/samsungapps/tobelog/LogEvent;

    iget-object v2, p0, Lcom/sec/android/app/samsungapps/bm;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v2}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getProductID()Ljava/lang/String;

    move-result-object v2

    iget-object v5, p0, Lcom/sec/android/app/samsungapps/bm;->a:Lcom/sec/android/app/samsungapps/ContentDetailActivity;

    invoke-static {v5}, Lcom/sec/android/app/samsungapps/ContentDetailActivity;->c(Lcom/sec/android/app/samsungapps/ContentDetailActivity;)Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/samsungapps/vlibrary/doc/ContentDetailContainer;->getVcategoryID()Ljava/lang/String;

    move-result-object v7

    move-object v5, v4

    move-object v6, v4

    move v8, v3

    invoke-static/range {v0 .. v8}, Lcom/sec/android/app/samsungapps/tobelog/ToBeLogWrapper;->sendNormalClickLog(Lcom/sec/android/app/samsungapps/tobelog/LogPage;Lcom/sec/android/app/samsungapps/tobelog/LogEvent;Ljava/lang/String;ILcom/sec/android/app/samsungapps/tobelog/LogBody$BannerType;Ljava/lang/String;Lcom/sec/android/app/samsungapps/tobelog/LogBody$PushStatus;Ljava/lang/String;I)V

    :cond_1
    move v3, v10

    goto/16 :goto_0

    :cond_2
    move v3, v9

    goto/16 :goto_0

    .line 3091
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0c00bb -> :sswitch_0
        0x7f0c00be -> :sswitch_1
        0x7f0c00c1 -> :sswitch_2
    .end sparse-switch
.end method
