.class public Lcom/sec/android/app/samsungapps/uiutil/DeviceResolution;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field public static final LIMIT_TABLET_MIN_HEIGHT:I = 0x400

.field public static final LIMIT_TABLET_MIN_WIDTH:I = 0x258

.field private static a:I

.field private static b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 17
    sput v0, Lcom/sec/android/app/samsungapps/uiutil/DeviceResolution;->a:I

    .line 18
    sput v0, Lcom/sec/android/app/samsungapps/uiutil/DeviceResolution;->b:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDensity(Landroid/content/Context;)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 81
    .line 83
    if-nez p0, :cond_0

    .line 112
    :goto_0
    return v1

    .line 90
    :cond_0
    const-string v0, "window"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 91
    if-nez v0, :cond_1

    .line 93
    const-string v0, "getDensity::WindowManager is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    move v0, v1

    :goto_1
    move v1, v0

    .line 112
    goto :goto_0

    .line 97
    :cond_1
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 98
    if-nez v0, :cond_2

    .line 100
    const-string v0, "getDensity::Display is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    move v0, v1

    .line 101
    goto :goto_1

    .line 104
    :cond_2
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 106
    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 108
    iget v0, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    goto :goto_1
.end method

.method public static getDisplayMetrics(Landroid/content/Context;)Landroid/util/DisplayMetrics;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 117
    .line 119
    if-nez p0, :cond_0

    .line 144
    :goto_0
    return-object v1

    .line 125
    :cond_0
    const-string v0, "window"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 126
    if-nez v0, :cond_1

    .line 128
    const-string v0, "getDensity::WindowManager is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    move-object v0, v1

    :goto_1
    move-object v1, v0

    .line 144
    goto :goto_0

    .line 132
    :cond_1
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    .line 133
    if-nez v2, :cond_2

    .line 135
    const-string v0, "getDensity::Display is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    move-object v0, v1

    .line 136
    goto :goto_1

    .line 139
    :cond_2
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 140
    invoke-virtual {v2, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    goto :goto_1
.end method

.method public static getHeight()I
    .locals 1

    .prologue
    .line 196
    sget v0, Lcom/sec/android/app/samsungapps/uiutil/DeviceResolution;->b:I

    return v0
.end method

.method public static getHeightPixel(Landroid/app/Activity;)I
    .locals 4

    .prologue
    .line 30
    const/4 v0, 0x0

    .line 34
    if-nez p0, :cond_0

    .line 36
    const-string v1, "getHeightPixel::activity is null"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    .line 76
    :goto_0
    return v0

    .line 40
    :cond_0
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 41
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    .line 42
    if-nez v2, :cond_1

    .line 44
    const-string v1, "getHeightPixel::Window is null"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 48
    :cond_1
    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 50
    iget v1, v1, Landroid/graphics/Rect;->top:I

    .line 52
    const v3, 0x1020002

    invoke-virtual {v2, v3}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 53
    if-nez v2, :cond_2

    .line 55
    const-string v1, "getHeightPixel::View is null"

    invoke-static {v1}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 59
    :cond_2
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v0

    .line 60
    sub-int v2, v0, v1

    .line 65
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/uiutil/DeviceResolution;->getDisplayMetrics(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v3

    .line 66
    const/16 v0, 0x30

    .line 67
    if-eqz v3, :cond_3

    .line 69
    iget v0, v3, Landroid/util/DisplayMetrics;->density:F

    float-to-int v0, v0

    mul-int/lit8 v0, v0, 0x30

    .line 72
    :cond_3
    add-int/2addr v1, v2

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public static getWidth()I
    .locals 1

    .prologue
    .line 191
    sget v0, Lcom/sec/android/app/samsungapps/uiutil/DeviceResolution;->a:I

    return v0
.end method

.method public static setResolution(II)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 178
    if-eq p0, v0, :cond_0

    .line 180
    sput p0, Lcom/sec/android/app/samsungapps/uiutil/DeviceResolution;->a:I

    .line 183
    :cond_0
    if-eq p1, v0, :cond_1

    .line 185
    sput p1, Lcom/sec/android/app/samsungapps/uiutil/DeviceResolution;->b:I

    .line 187
    :cond_1
    return-void
.end method

.method public static setResolution(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 149
    if-nez p0, :cond_0

    .line 174
    :goto_0
    return-void

    .line 156
    :cond_0
    const-string v0, "window"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 157
    if-nez v0, :cond_1

    .line 159
    const-string v0, "constructer::WindowManager is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 163
    :cond_1
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 164
    if-nez v0, :cond_2

    .line 166
    const-string v0, "constructer::Display is null"

    invoke-static {v0}, Lcom/sec/android/app/samsungapps/vlibrary/util/Loger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 170
    :cond_2
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v1

    sput v1, Lcom/sec/android/app/samsungapps/uiutil/DeviceResolution;->a:I

    .line 171
    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    sput v0, Lcom/sec/android/app/samsungapps/uiutil/DeviceResolution;->b:I

    goto :goto_0
.end method
