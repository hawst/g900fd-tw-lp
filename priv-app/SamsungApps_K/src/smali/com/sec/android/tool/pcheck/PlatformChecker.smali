.class public Lcom/sec/android/tool/pcheck/PlatformChecker;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static d:Lcom/sec/android/tool/pcheck/PlatformChecker;


# instance fields
.field private c:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 17
    new-array v0, v4, [Ljava/lang/String;

    .line 18
    const-string v1, "+uMpjjXqpsp6zydT2bn4xtPgZ1kGMqdw96T48d7a77I="

    aput-object v1, v0, v2

    .line 19
    const-string v1, "IaAPuAkDREGD4tPYwnUH0tHcYgp2GykATCvSpM2m+Wk="

    aput-object v1, v0, v3

    .line 17
    sput-object v0, Lcom/sec/android/tool/pcheck/PlatformChecker;->a:[Ljava/lang/String;

    .line 22
    new-array v0, v4, [Ljava/lang/String;

    .line 23
    const-string v1, "HW2cvdpQwYWjlUPWCe9XXv2E4YDUhhVfToG3SOkKqDg="

    aput-object v1, v0, v2

    .line 24
    const-string v1, "sQdbX+sU0IdnB2wic5nAD5TnVd46A+H/5dqacw20lJU="

    aput-object v1, v0, v3

    .line 22
    sput-object v0, Lcom/sec/android/tool/pcheck/PlatformChecker;->b:[Ljava/lang/String;

    .line 15
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/sec/android/tool/pcheck/PlatformChecker;->c:Landroid/content/Context;

    .line 39
    return-void
.end method

.method private static a(Landroid/content/pm/Signature;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 133
    invoke-virtual {p0}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v0

    .line 137
    :try_start_0
    invoke-static {v0}, Ljavax/security/cert/X509Certificate;->getInstance([B)Ljavax/security/cert/X509Certificate;

    move-result-object v0

    .line 138
    invoke-virtual {v0}, Ljavax/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v0

    instance-of v2, v0, Ljava/security/interfaces/RSAPublicKey;

    if-eqz v2, :cond_0

    check-cast v0, Ljava/security/interfaces/RSAPublicKey;

    invoke-interface {v0}, Ljava/security/interfaces/RSAPublicKey;->getModulus()Ljava/math/BigInteger;

    move-result-object v0

    const/16 v2, 0x10

    invoke-virtual {v0, v2}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/tool/pcheck/PlatformChecker;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 144
    :goto_0
    return-object v0

    .line 138
    :cond_0
    instance-of v2, v0, Ljava/security/interfaces/DSAPublicKey;

    if-eqz v2, :cond_1

    check-cast v0, Ljava/security/interfaces/DSAPublicKey;

    invoke-interface {v0}, Ljava/security/interfaces/DSAPublicKey;->getY()Ljava/math/BigInteger;

    move-result-object v0

    const/16 v2, 0x10

    invoke-virtual {v0, v2}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/tool/pcheck/PlatformChecker;->c(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 140
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 42
    invoke-direct {p0, p1}, Lcom/sec/android/tool/pcheck/PlatformChecker;->b(Ljava/lang/String;)[Landroid/content/pm/Signature;

    move-result-object v2

    .line 45
    if-eqz v2, :cond_0

    array-length v1, v2

    if-nez v1, :cond_1

    .line 56
    :cond_0
    return-object v0

    .line 49
    :cond_1
    array-length v3, v2

    const/4 v1, 0x0

    .line 52
    :goto_0
    if-ge v1, v3, :cond_0

    .line 49
    aget-object v0, v2, v1

    .line 50
    invoke-static {v0}, Lcom/sec/android/tool/pcheck/PlatformChecker;->a(Landroid/content/pm/Signature;)Ljava/lang/String;

    move-result-object v0

    .line 51
    if-nez v0, :cond_0

    .line 52
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)[B
    .locals 2

    .prologue
    .line 182
    :try_start_0
    invoke-static {p0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    .line 186
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/MessageDigest;->digest([B)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 191
    :goto_0
    return-object v0

    .line 188
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/lang/String;)[Landroid/content/pm/Signature;
    .locals 3

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/android/tool/pcheck/PlatformChecker;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 119
    const/4 v0, 0x0

    .line 123
    const/16 v2, 0x40

    .line 122
    :try_start_0
    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 124
    iget-object v0, v1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 129
    :goto_0
    return-object v0

    .line 125
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 170
    const-string v0, "SHA256"

    invoke-static {v0, p0}, Lcom/sec/android/tool/pcheck/PlatformChecker;->a(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v0

    .line 172
    new-instance v1, Ljava/lang/String;

    .line 176
    const/4 v2, 0x0

    .line 175
    invoke-static {v0, v2}, Lorg/apache/commons/codec/binary/Base64;->encodeBase64([BZ)[B

    move-result-object v0

    .line 174
    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V

    .line 178
    return-object v1
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/tool/pcheck/PlatformChecker;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/sec/android/tool/pcheck/PlatformChecker;->d:Lcom/sec/android/tool/pcheck/PlatformChecker;

    if-nez v0, :cond_0

    .line 32
    new-instance v0, Lcom/sec/android/tool/pcheck/PlatformChecker;

    invoke-direct {v0, p0}, Lcom/sec/android/tool/pcheck/PlatformChecker;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/tool/pcheck/PlatformChecker;->d:Lcom/sec/android/tool/pcheck/PlatformChecker;

    .line 34
    :cond_0
    sget-object v0, Lcom/sec/android/tool/pcheck/PlatformChecker;->d:Lcom/sec/android/tool/pcheck/PlatformChecker;

    return-object v0
.end method


# virtual methods
.method public getAppsType(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 81
    const-string v0, ""

    .line 83
    invoke-direct {p0, p1}, Lcom/sec/android/tool/pcheck/PlatformChecker;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 84
    sget-object v2, Lcom/sec/android/tool/pcheck/PlatformChecker;->b:[Ljava/lang/String;

    aget-object v2, v2, v3

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 89
    const-string v0, "self1"

    .line 113
    :cond_0
    :goto_0
    return-object v0

    .line 91
    :cond_1
    sget-object v2, Lcom/sec/android/tool/pcheck/PlatformChecker;->b:[Ljava/lang/String;

    aget-object v2, v2, v4

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 96
    const-string v0, "self2"

    goto :goto_0

    .line 98
    :cond_2
    sget-object v2, Lcom/sec/android/tool/pcheck/PlatformChecker;->a:[Ljava/lang/String;

    aget-object v2, v2, v3

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 103
    const-string v0, "plat1"

    goto :goto_0

    .line 105
    :cond_3
    sget-object v2, Lcom/sec/android/tool/pcheck/PlatformChecker;->a:[Ljava/lang/String;

    aget-object v2, v2, v4

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 110
    const-string v0, "plat3"

    goto :goto_0
.end method

.method public getType()I
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 60
    .line 62
    const-string v2, "android"

    invoke-direct {p0, v2}, Lcom/sec/android/tool/pcheck/PlatformChecker;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 63
    sget-object v3, Lcom/sec/android/tool/pcheck/PlatformChecker;->a:[Ljava/lang/String;

    aget-object v3, v3, v1

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 69
    :goto_0
    return v0

    .line 65
    :cond_0
    sget-object v3, Lcom/sec/android/tool/pcheck/PlatformChecker;->a:[Ljava/lang/String;

    aget-object v0, v3, v0

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 66
    const/4 v0, 0x3

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method
