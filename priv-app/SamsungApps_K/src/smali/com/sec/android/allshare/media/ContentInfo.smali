.class public Lcom/sec/android/allshare/media/ContentInfo;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private a:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 137
    new-instance v0, Lcom/sec/android/allshare/media/a;

    invoke-direct {v0}, Lcom/sec/android/allshare/media/a;-><init>()V

    sput-object v0, Lcom/sec/android/allshare/media/ContentInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 26
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/allshare/media/ContentInfo;->a:J

    .line 128
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 125
    invoke-direct {p0, p1}, Lcom/sec/android/allshare/media/ContentInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Lcom/sec/android/allshare/media/ContentInfo$Builder;)V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-static {p1}, Lcom/sec/android/allshare/media/ContentInfo$Builder;->a(Lcom/sec/android/allshare/media/ContentInfo$Builder;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/allshare/media/ContentInfo;->a:J

    .line 39
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/allshare/media/ContentInfo$Builder;B)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/sec/android/allshare/media/ContentInfo;-><init>(Lcom/sec/android/allshare/media/ContentInfo$Builder;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 104
    const/4 v0, 0x0

    return v0
.end method

.method public getStartingPosition()J
    .locals 2

    .prologue
    .line 48
    iget-wide v0, p0, Lcom/sec/android/allshare/media/ContentInfo;->a:J

    return-wide v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 116
    iget-wide v0, p0, Lcom/sec/android/allshare/media/ContentInfo;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 117
    return-void
.end method
