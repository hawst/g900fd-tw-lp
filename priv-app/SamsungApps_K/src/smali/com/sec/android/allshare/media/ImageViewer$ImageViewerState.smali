.class public final enum Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum BUFFERING:Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

.field public static final enum CONTENT_CHANGED:Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

.field public static final enum SHOWING:Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

.field public static final enum STOPPED:Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

.field public static final enum UNKNOWN:Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

.field private static final synthetic a:[Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 38
    new-instance v0, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    const-string v1, "STOPPED"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;-><init>(Ljava/lang/String;I)V

    .line 42
    sput-object v0, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;->STOPPED:Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    .line 43
    new-instance v0, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    const-string v1, "BUFFERING"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;-><init>(Ljava/lang/String;I)V

    .line 50
    sput-object v0, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;->BUFFERING:Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    .line 51
    new-instance v0, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    const-string v1, "SHOWING"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;-><init>(Ljava/lang/String;I)V

    .line 55
    sput-object v0, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;->SHOWING:Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    .line 56
    new-instance v0, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    const-string v1, "CONTENT_CHANGED"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;-><init>(Ljava/lang/String;I)V

    .line 66
    sput-object v0, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;->CONTENT_CHANGED:Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    .line 67
    new-instance v0, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;-><init>(Ljava/lang/String;I)V

    .line 73
    sput-object v0, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;->UNKNOWN:Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    .line 36
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    sget-object v1, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;->STOPPED:Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;->BUFFERING:Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;->SHOWING:Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;->CONTENT_CHANGED:Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;->UNKNOWN:Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;->a:[Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;->a:[Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
