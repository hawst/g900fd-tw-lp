.class public final enum Lcom/sec/android/allshare/Device$DeviceDomain;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum LOCAL_NETWORK:Lcom/sec/android/allshare/Device$DeviceDomain;

.field public static final enum MY_DEVICE:Lcom/sec/android/allshare/Device$DeviceDomain;

.field public static final enum REMOTE_NETWORK:Lcom/sec/android/allshare/Device$DeviceDomain;

.field public static final enum UNKNOWN:Lcom/sec/android/allshare/Device$DeviceDomain;

.field private static final synthetic a:[Lcom/sec/android/allshare/Device$DeviceDomain;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 40
    new-instance v0, Lcom/sec/android/allshare/Device$DeviceDomain;

    const-string v1, "MY_DEVICE"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/Device$DeviceDomain;-><init>(Ljava/lang/String;I)V

    .line 43
    sput-object v0, Lcom/sec/android/allshare/Device$DeviceDomain;->MY_DEVICE:Lcom/sec/android/allshare/Device$DeviceDomain;

    .line 44
    new-instance v0, Lcom/sec/android/allshare/Device$DeviceDomain;

    const-string v1, "LOCAL_NETWORK"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/allshare/Device$DeviceDomain;-><init>(Ljava/lang/String;I)V

    .line 47
    sput-object v0, Lcom/sec/android/allshare/Device$DeviceDomain;->LOCAL_NETWORK:Lcom/sec/android/allshare/Device$DeviceDomain;

    .line 48
    new-instance v0, Lcom/sec/android/allshare/Device$DeviceDomain;

    const-string v1, "REMOTE_NETWORK"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/allshare/Device$DeviceDomain;-><init>(Ljava/lang/String;I)V

    .line 51
    sput-object v0, Lcom/sec/android/allshare/Device$DeviceDomain;->REMOTE_NETWORK:Lcom/sec/android/allshare/Device$DeviceDomain;

    .line 52
    new-instance v0, Lcom/sec/android/allshare/Device$DeviceDomain;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/allshare/Device$DeviceDomain;-><init>(Ljava/lang/String;I)V

    .line 55
    sput-object v0, Lcom/sec/android/allshare/Device$DeviceDomain;->UNKNOWN:Lcom/sec/android/allshare/Device$DeviceDomain;

    .line 38
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/allshare/Device$DeviceDomain;

    sget-object v1, Lcom/sec/android/allshare/Device$DeviceDomain;->MY_DEVICE:Lcom/sec/android/allshare/Device$DeviceDomain;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/allshare/Device$DeviceDomain;->LOCAL_NETWORK:Lcom/sec/android/allshare/Device$DeviceDomain;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/allshare/Device$DeviceDomain;->REMOTE_NETWORK:Lcom/sec/android/allshare/Device$DeviceDomain;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/allshare/Device$DeviceDomain;->UNKNOWN:Lcom/sec/android/allshare/Device$DeviceDomain;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/allshare/Device$DeviceDomain;->a:[Lcom/sec/android/allshare/Device$DeviceDomain;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/allshare/Device$DeviceDomain;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/android/allshare/Device$DeviceDomain;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/allshare/Device$DeviceDomain;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/allshare/Device$DeviceDomain;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/android/allshare/Device$DeviceDomain;->a:[Lcom/sec/android/allshare/Device$DeviceDomain;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/android/allshare/Device$DeviceDomain;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
