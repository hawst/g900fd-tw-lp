.class public Lcom/sec/android/allshare/Item$LocalContentBuilder;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static synthetic d:[I


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 249
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 251
    iput-object p1, p0, Lcom/sec/android/allshare/Item$LocalContentBuilder;->a:Ljava/lang/String;

    .line 252
    iput-object p2, p0, Lcom/sec/android/allshare/Item$LocalContentBuilder;->b:Ljava/lang/String;

    .line 253
    return-void
.end method

.method private static synthetic a()[I
    .locals 3

    .prologue
    .line 237
    sget-object v0, Lcom/sec/android/allshare/Item$LocalContentBuilder;->d:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/allshare/Item$MediaType;->values()[Lcom/sec/android/allshare/Item$MediaType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/allshare/Item$MediaType;->ITEM_AUDIO:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/allshare/Item$MediaType;->ITEM_FOLDER:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/android/allshare/Item$MediaType;->ITEM_IMAGE:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    :try_start_3
    sget-object v1, Lcom/sec/android/allshare/Item$MediaType;->ITEM_UNKNOWN:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_4
    :try_start_4
    sget-object v1, Lcom/sec/android/allshare/Item$MediaType;->ITEM_VIDEO:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_5
    sput-object v0, Lcom/sec/android/allshare/Item$LocalContentBuilder;->d:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_5

    :catch_1
    move-exception v1

    goto :goto_4

    :catch_2
    move-exception v1

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_1
.end method


# virtual methods
.method public build()Lcom/sec/android/allshare/Item;
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 276
    iget-object v0, p0, Lcom/sec/android/allshare/Item$LocalContentBuilder;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/Item$LocalContentBuilder;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 302
    :cond_0
    :goto_0
    return-object v2

    .line 281
    :cond_1
    iget-object v0, p0, Lcom/sec/android/allshare/Item$LocalContentBuilder;->a:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    move v0, v7

    :goto_1
    if-eqz v0, :cond_0

    .line 286
    iget-object v0, p0, Lcom/sec/android/allshare/Item$LocalContentBuilder;->a:Ljava/lang/String;

    const-string v1, "content:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 289
    new-instance v0, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;

    sget-object v1, Lcom/sec/android/allshare/ah;->c:Lcom/sec/android/allshare/ah;

    iget-object v2, p0, Lcom/sec/android/allshare/Item$LocalContentBuilder;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/allshare/Item$LocalContentBuilder;->c:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/allshare/Item$LocalContentBuilder;->b:Ljava/lang/String;

    move v5, v7

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;-><init>(Lcom/sec/android/allshare/ah;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;B)V

    move-object v2, v0

    goto :goto_0

    .line 281
    :cond_3
    const-string v1, "content:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-static {}, Lcom/sec/android/allshare/ServiceConnector;->a()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-ne v1, v6, :cond_4

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    move v0, v6

    goto :goto_1

    :cond_4
    if-eqz v0, :cond_5

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_5
    move v0, v7

    goto :goto_1

    :cond_6
    const-string v1, "file://"

    const-string v3, "file:"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :cond_7
    const-string v1, "/data/data"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_8

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_8

    move v0, v6

    goto :goto_1

    :cond_8
    move v0, v7

    goto :goto_1

    .line 292
    :cond_9
    iget-object v0, p0, Lcom/sec/android/allshare/Item$LocalContentBuilder;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/allshare/Item;->a(Ljava/lang/String;)Lcom/sec/android/allshare/Item$MediaType;

    move-result-object v0

    .line 293
    invoke-static {}, Lcom/sec/android/allshare/Item$LocalContentBuilder;->a()[I

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    .line 300
    :pswitch_0
    new-instance v0, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;

    sget-object v1, Lcom/sec/android/allshare/ah;->c:Lcom/sec/android/allshare/ah;

    iget-object v2, p0, Lcom/sec/android/allshare/Item$LocalContentBuilder;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/allshare/Item$LocalContentBuilder;->c:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/allshare/Item$LocalContentBuilder;->b:Ljava/lang/String;

    move v5, v7

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;-><init>(Lcom/sec/android/allshare/ah;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;B)V

    move-object v2, v0

    goto/16 :goto_0

    .line 293
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public setTitle(Ljava/lang/String;)Lcom/sec/android/allshare/Item$LocalContentBuilder;
    .locals 0

    .prologue
    .line 263
    iput-object p1, p0, Lcom/sec/android/allshare/Item$LocalContentBuilder;->c:Ljava/lang/String;

    .line 264
    return-object p0
.end method
