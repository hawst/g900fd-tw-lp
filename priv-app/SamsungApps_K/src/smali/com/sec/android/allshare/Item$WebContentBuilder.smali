.class public Lcom/sec/android/allshare/Item$WebContentBuilder;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static synthetic d:[I


# instance fields
.field private a:Landroid/net/Uri;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 628
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 618
    iput-object v0, p0, Lcom/sec/android/allshare/Item$WebContentBuilder;->a:Landroid/net/Uri;

    .line 619
    iput-object v0, p0, Lcom/sec/android/allshare/Item$WebContentBuilder;->b:Ljava/lang/String;

    .line 620
    iput-object v0, p0, Lcom/sec/android/allshare/Item$WebContentBuilder;->c:Ljava/lang/String;

    .line 630
    iput-object p1, p0, Lcom/sec/android/allshare/Item$WebContentBuilder;->a:Landroid/net/Uri;

    .line 631
    iput-object p2, p0, Lcom/sec/android/allshare/Item$WebContentBuilder;->b:Ljava/lang/String;

    .line 632
    return-void
.end method

.method private static synthetic a()[I
    .locals 3

    .prologue
    .line 616
    sget-object v0, Lcom/sec/android/allshare/Item$WebContentBuilder;->d:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/allshare/Item$MediaType;->values()[Lcom/sec/android/allshare/Item$MediaType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/allshare/Item$MediaType;->ITEM_AUDIO:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/allshare/Item$MediaType;->ITEM_FOLDER:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/android/allshare/Item$MediaType;->ITEM_IMAGE:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    :try_start_3
    sget-object v1, Lcom/sec/android/allshare/Item$MediaType;->ITEM_UNKNOWN:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_4
    :try_start_4
    sget-object v1, Lcom/sec/android/allshare/Item$MediaType;->ITEM_VIDEO:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_5
    sput-object v0, Lcom/sec/android/allshare/Item$WebContentBuilder;->d:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_5

    :catch_1
    move-exception v1

    goto :goto_4

    :catch_2
    move-exception v1

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_1
.end method


# virtual methods
.method public build()Lcom/sec/android/allshare/Item;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 657
    iget-object v1, p0, Lcom/sec/android/allshare/Item$WebContentBuilder;->a:Landroid/net/Uri;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/allshare/Item$WebContentBuilder;->b:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 674
    :cond_0
    :goto_0
    return-object v0

    .line 660
    :cond_1
    iget-object v1, p0, Lcom/sec/android/allshare/Item$WebContentBuilder;->a:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    .line 661
    if-eqz v1, :cond_0

    const-string v2, "content"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "file"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 664
    iget-object v1, p0, Lcom/sec/android/allshare/Item$WebContentBuilder;->b:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/allshare/Item;->a(Ljava/lang/String;)Lcom/sec/android/allshare/Item$MediaType;

    move-result-object v1

    .line 665
    invoke-static {}, Lcom/sec/android/allshare/Item$WebContentBuilder;->a()[I

    move-result-object v2

    invoke-virtual {v1}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 672
    :pswitch_0
    new-instance v0, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;

    sget-object v1, Lcom/sec/android/allshare/ah;->b:Lcom/sec/android/allshare/ah;

    iget-object v2, p0, Lcom/sec/android/allshare/Item$WebContentBuilder;->a:Landroid/net/Uri;

    iget-object v3, p0, Lcom/sec/android/allshare/Item$WebContentBuilder;->c:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/allshare/Item$WebContentBuilder;->b:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;-><init>(Lcom/sec/android/allshare/ah;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;B)V

    goto :goto_0

    .line 665
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public setTitle(Ljava/lang/String;)Lcom/sec/android/allshare/Item$WebContentBuilder;
    .locals 0

    .prologue
    .line 644
    iput-object p1, p0, Lcom/sec/android/allshare/Item$WebContentBuilder;->c:Ljava/lang/String;

    .line 645
    return-object p0
.end method
