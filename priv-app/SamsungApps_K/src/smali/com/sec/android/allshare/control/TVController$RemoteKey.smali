.class public final enum Lcom/sec/android/allshare/control/TVController$RemoteKey;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum KEY_0:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_1:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_2:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_3:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_4:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_5:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_6:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_7:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_8:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_9:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_BLUE:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_CHDOWN:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_CHUP:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_CH_LIST:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_CONTENTS:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_DASH:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_DOWN:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_ENTER:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_EXIT:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_FF:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_GREEN:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_INFO:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_LEFT:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_MENU:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_MUTE:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_PAUSE:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_PLAY:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_POWEROFF:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_PRECH:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_REC:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_RED:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_RETURN:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_REWIND:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_RIGHT:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_SOURCE:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_STOP:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_TOOLS:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_UP:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_VOLDOWN:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_VOLUP:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field public static final enum KEY_YELLOW:Lcom/sec/android/allshare/control/TVController$RemoteKey;

.field private static final synthetic a:[Lcom/sec/android/allshare/control/TVController$RemoteKey;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 34
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_POWEROFF"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 35
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_POWEROFF:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 36
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_SOURCE"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 37
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_SOURCE:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 38
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_1"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 39
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_1:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 40
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_2"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 41
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_2:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 42
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_3"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 43
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_3:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 44
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_4"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 45
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_4:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 46
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_5"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 47
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_5:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 48
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_6"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 49
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_6:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 50
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_7"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 51
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_7:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 52
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_8"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 53
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_8:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 54
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_9"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 55
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_9:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 56
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_DASH"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 57
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_DASH:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 58
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_0"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 59
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_0:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 60
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_PRECH"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 61
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_PRECH:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 62
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_VOLUP"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 63
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_VOLUP:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 64
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_VOLDOWN"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 65
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_VOLDOWN:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 66
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_MUTE"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 67
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_MUTE:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 68
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_CH_LIST"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 69
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_CH_LIST:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 70
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_CHUP"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 71
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_CHUP:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 72
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_CHDOWN"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 73
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_CHDOWN:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 74
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_CONTENTS"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 75
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_CONTENTS:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 76
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_MENU"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 77
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_MENU:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 78
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_TOOLS"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 79
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_TOOLS:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 80
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_UP"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 81
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_UP:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 82
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_INFO"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 83
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_INFO:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 84
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_LEFT"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 85
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_LEFT:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 86
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_ENTER"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 87
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_ENTER:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 88
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_RIGHT"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 89
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_RIGHT:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 90
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_RETURN"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 91
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_RETURN:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 92
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_DOWN"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 93
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_DOWN:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 94
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_EXIT"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 95
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_EXIT:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 96
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_RED"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 97
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_RED:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 98
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_GREEN"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 99
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_GREEN:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 100
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_YELLOW"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 101
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_YELLOW:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 102
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_BLUE"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 103
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_BLUE:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 104
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_REWIND"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 105
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_REWIND:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 106
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_PAUSE"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 107
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_PAUSE:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 108
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_FF"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 109
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_FF:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 110
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_REC"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 111
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_REC:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 112
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_PLAY"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 113
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_PLAY:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 114
    new-instance v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    const-string v1, "KEY_STOP"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$RemoteKey;-><init>(Ljava/lang/String;I)V

    .line 115
    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_STOP:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    .line 32
    const/16 v0, 0x29

    new-array v0, v0, [Lcom/sec/android/allshare/control/TVController$RemoteKey;

    sget-object v1, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_POWEROFF:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_SOURCE:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_1:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_2:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_3:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_4:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_5:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_6:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_7:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_8:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_9:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_DASH:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_0:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_PRECH:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_VOLUP:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_VOLDOWN:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_MUTE:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_CH_LIST:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_CHUP:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_CHDOWN:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_CONTENTS:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_MENU:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_TOOLS:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_UP:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_INFO:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_LEFT:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_ENTER:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_RIGHT:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_RETURN:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_DOWN:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_EXIT:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_RED:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_GREEN:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_YELLOW:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_BLUE:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_REWIND:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_PAUSE:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_FF:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_REC:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_PLAY:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_STOP:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->a:[Lcom/sec/android/allshare/control/TVController$RemoteKey;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/allshare/control/TVController$RemoteKey;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/allshare/control/TVController$RemoteKey;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/android/allshare/control/TVController$RemoteKey;->a:[Lcom/sec/android/allshare/control/TVController$RemoteKey;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/android/allshare/control/TVController$RemoteKey;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
