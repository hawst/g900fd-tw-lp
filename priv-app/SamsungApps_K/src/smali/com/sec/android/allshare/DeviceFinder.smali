.class public abstract Lcom/sec/android/allshare/DeviceFinder;
.super Ljava/lang/Object;
.source "ProGuard"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    return-void
.end method


# virtual methods
.method public abstract getDevice(Ljava/lang/String;Lcom/sec/android/allshare/Device$DeviceType;)Lcom/sec/android/allshare/Device;
.end method

.method public abstract getDevices(Lcom/sec/android/allshare/Device$DeviceDomain;Lcom/sec/android/allshare/Device$DeviceType;)Ljava/util/ArrayList;
.end method

.method public abstract getDevices(Lcom/sec/android/allshare/Device$DeviceType;)Ljava/util/ArrayList;
.end method

.method public abstract getDevices(Lcom/sec/android/allshare/Device$DeviceType;Ljava/lang/String;)Ljava/util/ArrayList;
.end method

.method public abstract refresh()V
.end method

.method public abstract setDeviceFinderEventListener(Lcom/sec/android/allshare/Device$DeviceType;Lcom/sec/android/allshare/DeviceFinder$IDeviceFinderEventListener;)V
.end method
