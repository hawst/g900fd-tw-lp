.class final Lcom/sec/android/allshare/ap;
.super Lcom/sec/android/allshare/e;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/allshare/an;


# direct methods
.method constructor <init>(Lcom/sec/android/allshare/an;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/allshare/ap;->a:Lcom/sec/android/allshare/an;

    .line 122
    invoke-direct {p0, p2}, Lcom/sec/android/allshare/e;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 4

    .prologue
    .line 128
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 129
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v1

    .line 130
    const-string v2, "BUNDLE_ENUM_ERROR"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/allshare/ERROR;->valueOf(Ljava/lang/String;)Lcom/sec/android/allshare/ERROR;

    move-result-object v2

    .line 132
    const-string v3, "com.sec.android.allshare.action.ACTION_RECEIVER_RECEIVE_BY_ITEM"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 134
    const-string v1, "BUNDLE_PARCELABLE_ITEM"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 135
    iget-object v1, p0, Lcom/sec/android/allshare/ap;->a:Lcom/sec/android/allshare/an;

    invoke-static {v0}, Lcom/sec/android/allshare/an;->a(Landroid/os/Bundle;)Lcom/sec/android/allshare/Item;

    move-result-object v0

    .line 138
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/allshare/ap;->a:Lcom/sec/android/allshare/an;

    invoke-static {v1}, Lcom/sec/android/allshare/an;->b(Lcom/sec/android/allshare/an;)Lcom/sec/android/allshare/media/Receiver$IReceiverResponseListener;

    move-result-object v1

    invoke-interface {v1, v0, v2}, Lcom/sec/android/allshare/media/Receiver$IReceiverResponseListener;->onReceiveResponseReceived(Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/ERROR;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    .line 169
    :cond_0
    :goto_0
    return-void

    .line 140
    :catch_0
    move-exception v0

    .line 142
    const-string v1, "ReceiverImpl"

    const-string v2, "mResponseHandler ACTION_RECEIVER_RECEIVE_BY_ITEM Exception"

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 144
    :catch_1
    move-exception v0

    .line 146
    const-string v1, "ReceiverImpl"

    const-string v2, "mResponseHandler ACTION_RECEIVER_RECEIVE_BY_ITEM Error"

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Error;)V

    goto :goto_0

    .line 150
    :cond_1
    const-string v3, "com.sec.android.allshare.action.ACTION_RECEIVER_CANCEL_BY_ITEM"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 152
    const-string v1, "BUNDLE_PARCELABLE_ITEM"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 153
    iget-object v1, p0, Lcom/sec/android/allshare/ap;->a:Lcom/sec/android/allshare/an;

    invoke-static {v0}, Lcom/sec/android/allshare/an;->a(Landroid/os/Bundle;)Lcom/sec/android/allshare/Item;

    move-result-object v0

    .line 156
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/allshare/ap;->a:Lcom/sec/android/allshare/an;

    invoke-static {v1}, Lcom/sec/android/allshare/an;->b(Lcom/sec/android/allshare/an;)Lcom/sec/android/allshare/media/Receiver$IReceiverResponseListener;

    move-result-object v1

    invoke-interface {v1, v0, v2}, Lcom/sec/android/allshare/media/Receiver$IReceiverResponseListener;->onCancelResponseReceived(Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/ERROR;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Error; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_0

    .line 158
    :catch_2
    move-exception v0

    .line 160
    const-string v1, "ReceiverImpl"

    const-string v2, "mResponseHandler ACTION_RECEIVER_CANCEL_BY_ITEM Exception"

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 162
    :catch_3
    move-exception v0

    .line 164
    const-string v1, "ReceiverImpl"

    const-string v2, "mResponseHandler ACTION_RECEIVER_CANCEL_BY_ITEM Error"

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Error;)V

    goto :goto_0
.end method
