.class final Lcom/sec/android/allshare/h;
.super Lcom/sec/android/allshare/d;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/allshare/g;


# direct methods
.method constructor <init>(Lcom/sec/android/allshare/g;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/allshare/h;->a:Lcom/sec/android/allshare/g;

    .line 113
    invoke-direct {p0, p2}, Lcom/sec/android/allshare/d;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 7

    .prologue
    .line 120
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getEventID()Ljava/lang/String;

    move-result-object v3

    .line 122
    const/4 v1, 0x0

    .line 126
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/allshare/h;->a:Lcom/sec/android/allshare/g;

    invoke-static {v0}, Lcom/sec/android/allshare/g;->a(Lcom/sec/android/allshare/g;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/allshare/DeviceFinder$IDeviceFinderEventListener;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v2, v0

    .line 133
    :goto_0
    invoke-static {}, Lcom/sec/android/allshare/g;->a()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/allshare/Device$DeviceType;

    .line 134
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v3

    .line 135
    const-string v1, "BUNDLE_STRING_TYPE"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 136
    const-string v1, "BUNDLE_PARCELABLE_DEVICE"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    .line 138
    if-nez v1, :cond_1

    .line 191
    :cond_0
    :goto_1
    return-void

    .line 128
    :catch_0
    move-exception v0

    .line 130
    const-string v2, "DeviceFinderImpl"

    const-string v4, "mEventHandler.handleEventMessage : Exception"

    invoke-static {v2, v4, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    move-object v2, v1

    goto :goto_0

    .line 141
    :cond_1
    iget-object v5, p0, Lcom/sec/android/allshare/h;->a:Lcom/sec/android/allshare/g;

    invoke-static {v5, v1, v0}, Lcom/sec/android/allshare/g;->a(Lcom/sec/android/allshare/g;Landroid/os/Bundle;Lcom/sec/android/allshare/Device$DeviceType;)Lcom/sec/android/allshare/Device;

    move-result-object v5

    .line 142
    if-eqz v5, :cond_0

    .line 145
    const-string v6, "ADDED"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 149
    if-eqz v2, :cond_0

    .line 151
    :try_start_1
    sget-object v1, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v2, v0, v5, v1}, Lcom/sec/android/allshare/DeviceFinder$IDeviceFinderEventListener;->onDeviceAdded(Lcom/sec/android/allshare/Device$DeviceType;Lcom/sec/android/allshare/Device;Lcom/sec/android/allshare/ERROR;)V

    .line 152
    const-string v0, "DeviceFinderImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[ADDED] "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/sec/android/allshare/Device;->getDeviceType()Lcom/sec/android/allshare/Device$DeviceType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v5}, Lcom/sec/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Error; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_1

    .line 155
    :catch_1
    move-exception v0

    .line 157
    const-string v1, "DeviceFinderImpl"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 159
    :catch_2
    move-exception v0

    .line 161
    const-string v1, "DeviceFinderImpl"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Error;)V

    goto :goto_1

    .line 165
    :cond_2
    const-string v6, "REMOVED"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 169
    :try_start_2
    iget-object v4, p0, Lcom/sec/android/allshare/h;->a:Lcom/sec/android/allshare/g;

    invoke-static {v4, v1, v0}, Lcom/sec/android/allshare/g;->b(Lcom/sec/android/allshare/g;Landroid/os/Bundle;Lcom/sec/android/allshare/Device$DeviceType;)V

    .line 170
    const-string v1, "BUNDLE_ENUM_ERROR"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/allshare/ERROR;->valueOf(Ljava/lang/String;)Lcom/sec/android/allshare/ERROR;

    move-result-object v1

    .line 171
    if-eqz v2, :cond_0

    .line 173
    invoke-interface {v2, v0, v5, v1}, Lcom/sec/android/allshare/DeviceFinder$IDeviceFinderEventListener;->onDeviceRemoved(Lcom/sec/android/allshare/Device$DeviceType;Lcom/sec/android/allshare/Device;Lcom/sec/android/allshare/ERROR;)V

    .line 174
    const-string v0, "DeviceFinderImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[REMOVED] "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/sec/android/allshare/Device;->getDeviceType()Lcom/sec/android/allshare/Device$DeviceType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v5}, Lcom/sec/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Error; {:try_start_2 .. :try_end_2} :catch_4

    goto/16 :goto_1

    .line 177
    :catch_3
    move-exception v0

    .line 179
    const-string v1, "DeviceFinderImpl"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto/16 :goto_1

    .line 181
    :catch_4
    move-exception v0

    .line 183
    const-string v1, "DeviceFinderImpl"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Error;)V

    goto/16 :goto_1
.end method
