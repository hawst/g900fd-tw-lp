.class final Lcom/sec/android/allshare/s;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/allshare/w;


# instance fields
.field final synthetic a:Lcom/sec/android/allshare/p;


# direct methods
.method constructor <init>(Lcom/sec/android/allshare/p;)V
    .locals 0

    .prologue
    .line 530
    iput-object p1, p0, Lcom/sec/android/allshare/s;->a:Lcom/sec/android/allshare/p;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/sec/android/allshare/media/FileReceiver$IFileReceiverProgressUpdateEventListener;Landroid/os/Bundle;)V
    .locals 9

    .prologue
    .line 536
    sget-object v0, Lcom/sec/android/allshare/ERROR;->FAIL:Lcom/sec/android/allshare/ERROR;

    .line 538
    const-string v0, "BUNDLE_LONG_FILE_PROGRESS"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    .line 539
    const-string v0, "BUNDLE_LONG_FILE_SIZE"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v5

    .line 540
    const-string v0, "BUNDLE_STRING_FILE_PATH"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 541
    const-string v1, "BUNDLE_STRING_SESSIONID"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 543
    const-string v1, "BUNDLE_ENUM_ERROR"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 544
    if-nez v1, :cond_0

    .line 545
    sget-object v8, Lcom/sec/android/allshare/ERROR;->FAIL:Lcom/sec/android/allshare/ERROR;

    .line 549
    :goto_0
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 551
    iget-object v0, p0, Lcom/sec/android/allshare/s;->a:Lcom/sec/android/allshare/p;

    invoke-static {v0}, Lcom/sec/android/allshare/p;->a(Lcom/sec/android/allshare/p;)Lcom/sec/android/allshare/n;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/allshare/n;->a(Lcom/sec/android/allshare/n;)Lcom/sec/android/allshare/media/FileReceiver;

    move-result-object v1

    move-object v0, p1

    invoke-interface/range {v0 .. v8}, Lcom/sec/android/allshare/media/FileReceiver$IFileReceiverProgressUpdateEventListener;->onProgressUpdated(Lcom/sec/android/allshare/media/FileReceiver;Ljava/lang/String;JJLjava/io/File;Lcom/sec/android/allshare/ERROR;)V

    .line 552
    return-void

    .line 547
    :cond_0
    invoke-static {v1}, Lcom/sec/android/allshare/ERROR;->valueOf(Ljava/lang/String;)Lcom/sec/android/allshare/ERROR;

    move-result-object v8

    goto :goto_0
.end method
