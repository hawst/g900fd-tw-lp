.class public Lcom/sec/android/allshare/TVControllerImpl$TVInformationImpl;
.super Lcom/sec/android/allshare/control/TVController$TVInformation;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/allshare/TVControllerImpl;

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/allshare/TVControllerImpl;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2124
    iput-object p1, p0, Lcom/sec/android/allshare/TVControllerImpl$TVInformationImpl;->a:Lcom/sec/android/allshare/TVControllerImpl;

    .line 2123
    invoke-direct {p0, p1}, Lcom/sec/android/allshare/control/TVController$TVInformation;-><init>(Lcom/sec/android/allshare/control/TVController;)V

    .line 2119
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl$TVInformationImpl;->c:Ljava/lang/String;

    .line 2120
    iput-boolean v1, p0, Lcom/sec/android/allshare/TVControllerImpl$TVInformationImpl;->d:Z

    .line 2121
    iput-boolean v1, p0, Lcom/sec/android/allshare/TVControllerImpl$TVInformationImpl;->e:Z

    .line 2126
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/allshare/TVControllerImpl;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 2129
    iput-object p1, p0, Lcom/sec/android/allshare/TVControllerImpl$TVInformationImpl;->a:Lcom/sec/android/allshare/TVControllerImpl;

    .line 2128
    invoke-direct {p0, p1}, Lcom/sec/android/allshare/control/TVController$TVInformation;-><init>(Lcom/sec/android/allshare/control/TVController;)V

    .line 2119
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/allshare/TVControllerImpl$TVInformationImpl;->c:Ljava/lang/String;

    .line 2120
    iput-boolean v2, p0, Lcom/sec/android/allshare/TVControllerImpl$TVInformationImpl;->d:Z

    .line 2121
    iput-boolean v2, p0, Lcom/sec/android/allshare/TVControllerImpl$TVInformationImpl;->e:Z

    .line 2130
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v3

    .line 2137
    :try_start_0
    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, p2}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-interface {v3, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 2138
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v1

    move v2, v1

    move-object v1, v0

    .line 2140
    :goto_0
    if-ne v2, v5, :cond_0

    .line 2178
    :goto_1
    return-void

    .line 2142
    :cond_0
    packed-switch v2, :pswitch_data_0

    .line 2163
    :cond_1
    :goto_2
    :pswitch_0
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v2

    goto :goto_0

    .line 2148
    :pswitch_1
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 2152
    :pswitch_2
    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 2154
    if-eqz v1, :cond_1

    const-string v2, "SupportTVVersion"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    iput-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl$TVInformationImpl;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/allshare/TVControllerImpl$TVInformationImpl;->c:Ljava/lang/String;

    const-string v4, "2012"

    invoke-virtual {v2, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-ltz v2, :cond_1

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/allshare/TVControllerImpl$TVInformationImpl;->d:Z
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_2

    .line 2166
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    goto :goto_1

    .line 2159
    :pswitch_3
    :try_start_1
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v0

    goto :goto_2

    .line 2170
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 2174
    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1

    .line 2142
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2200
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl$TVInformationImpl;->c:Ljava/lang/String;

    return-object v0
.end method

.method public isSupportMouse()Z
    .locals 1

    .prologue
    .line 2211
    iget-boolean v0, p0, Lcom/sec/android/allshare/TVControllerImpl$TVInformationImpl;->e:Z

    return v0
.end method

.method public isSupportWebSharing()Z
    .locals 1

    .prologue
    .line 2206
    iget-boolean v0, p0, Lcom/sec/android/allshare/TVControllerImpl$TVInformationImpl;->d:Z

    return v0
.end method
