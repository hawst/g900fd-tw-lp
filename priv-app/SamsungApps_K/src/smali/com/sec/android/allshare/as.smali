.class final Lcom/sec/android/allshare/as;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/os/Handler$Callback;


# static fields
.field private static synthetic c:[I


# instance fields
.field private a:Lcom/sec/android/allshare/ServiceConnector$IServiceConnectEventListener;

.field private b:Lcom/sec/android/allshare/ax;


# direct methods
.method constructor <init>(Lcom/sec/android/allshare/ServiceConnector$IServiceConnectEventListener;Landroid/content/Context;Lcom/sec/android/allshare/at;)V
    .locals 1

    .prologue
    .line 181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 183
    iput-object p1, p0, Lcom/sec/android/allshare/as;->a:Lcom/sec/android/allshare/ServiceConnector$IServiceConnectEventListener;

    .line 184
    new-instance v0, Lcom/sec/android/allshare/ax;

    invoke-direct {v0, p2, p3}, Lcom/sec/android/allshare/ax;-><init>(Landroid/content/Context;Lcom/sec/android/allshare/at;)V

    iput-object v0, p0, Lcom/sec/android/allshare/as;->b:Lcom/sec/android/allshare/ax;

    .line 1
    return-void
.end method

.method private static synthetic a()[I
    .locals 3

    .prologue
    .line 181
    sget-object v0, Lcom/sec/android/allshare/as;->c:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;->values()[Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;->ALLSHARE_SERVICE_CONNECTED:Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;

    invoke-virtual {v1}, Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_1

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;->ALLSHARE_SERVICE_DISCONNECTED:Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;

    invoke-virtual {v1}, Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_0

    :goto_2
    sput-object v0, Lcom/sec/android/allshare/as;->c:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_1
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)Z
    .locals 3

    .prologue
    .line 190
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v0, v0, Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;

    if-nez v0, :cond_1

    .line 191
    :cond_0
    const/4 v0, 0x0

    .line 231
    :goto_0
    return v0

    .line 193
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;

    .line 195
    invoke-static {}, Lcom/sec/android/allshare/as;->a()[I

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 231
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 198
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/allshare/as;->b:Lcom/sec/android/allshare/ax;

    sget-object v1, Lcom/sec/android/allshare/ServiceConnector$ServiceState;->ENABLED:Lcom/sec/android/allshare/ServiceConnector$ServiceState;

    iput-object v1, v0, Lcom/sec/android/allshare/ax;->a:Lcom/sec/android/allshare/ServiceConnector$ServiceState;

    .line 201
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/allshare/as;->a:Lcom/sec/android/allshare/ServiceConnector$IServiceConnectEventListener;

    iget-object v1, p0, Lcom/sec/android/allshare/as;->b:Lcom/sec/android/allshare/ax;

    sget-object v2, Lcom/sec/android/allshare/ServiceConnector$ServiceState;->ENABLED:Lcom/sec/android/allshare/ServiceConnector$ServiceState;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/ServiceConnector$IServiceConnectEventListener;->onCreated(Lcom/sec/android/allshare/ServiceProvider;Lcom/sec/android/allshare/ServiceConnector$ServiceState;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 203
    :catch_0
    move-exception v0

    .line 205
    const-string v1, "ServiceConnector"

    const-string v2, "handleMessage Exception"

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 207
    :catch_1
    move-exception v0

    .line 209
    const-string v1, "ServiceConnector"

    const-string v2, "handleMessage Error"

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Error;)V

    goto :goto_1

    .line 214
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/allshare/as;->b:Lcom/sec/android/allshare/ax;

    sget-object v1, Lcom/sec/android/allshare/ServiceConnector$ServiceState;->DISABLED:Lcom/sec/android/allshare/ServiceConnector$ServiceState;

    iput-object v1, v0, Lcom/sec/android/allshare/ax;->a:Lcom/sec/android/allshare/ServiceConnector$ServiceState;

    .line 217
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/allshare/as;->a:Lcom/sec/android/allshare/ServiceConnector$IServiceConnectEventListener;

    iget-object v1, p0, Lcom/sec/android/allshare/as;->b:Lcom/sec/android/allshare/ax;

    invoke-interface {v0, v1}, Lcom/sec/android/allshare/ServiceConnector$IServiceConnectEventListener;->onDeleted(Lcom/sec/android/allshare/ServiceProvider;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Error; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_1

    .line 219
    :catch_2
    move-exception v0

    .line 221
    const-string v1, "ServiceConnector"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 223
    :catch_3
    move-exception v0

    .line 225
    const-string v1, "ServiceConnector"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Error;)V

    goto :goto_1

    .line 195
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
