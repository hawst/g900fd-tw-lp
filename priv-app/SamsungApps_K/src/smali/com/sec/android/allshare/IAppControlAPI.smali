.class final Lcom/sec/android/allshare/IAppControlAPI;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field a:Lcom/sec/android/allshare/bd;

.field b:Lcom/sec/android/allshare/bc;

.field public c:Ljava/util/concurrent/ExecutorService;

.field private d:Ljava/lang/String;

.field private e:I

.field private f:Ljava/lang/String;

.field private g:Z

.field private h:Ljava/lang/Object;

.field private i:Lcom/sec/android/allshare/IAppControlAPI$IControlEventListener;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/allshare/IAppControlAPI;->d:Ljava/lang/String;

    .line 50
    iput-object v1, p0, Lcom/sec/android/allshare/IAppControlAPI;->a:Lcom/sec/android/allshare/bd;

    .line 51
    iput-object v1, p0, Lcom/sec/android/allshare/IAppControlAPI;->b:Lcom/sec/android/allshare/bc;

    .line 53
    iput v2, p0, Lcom/sec/android/allshare/IAppControlAPI;->e:I

    .line 56
    iput-boolean v2, p0, Lcom/sec/android/allshare/IAppControlAPI;->g:Z

    .line 57
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/allshare/IAppControlAPI;->h:Ljava/lang/Object;

    .line 59
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/allshare/IAppControlAPI;->c:Ljava/util/concurrent/ExecutorService;

    .line 71
    iput-object v1, p0, Lcom/sec/android/allshare/IAppControlAPI;->i:Lcom/sec/android/allshare/IAppControlAPI$IControlEventListener;

    .line 93
    return-void
.end method

.method static synthetic a(Lcom/sec/android/allshare/IAppControlAPI;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/allshare/IAppControlAPI;->h:Ljava/lang/Object;

    return-object v0
.end method

.method private a(Landroid/os/Message;)V
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/allshare/IAppControlAPI;->a:Lcom/sec/android/allshare/bd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/IAppControlAPI;->a:Lcom/sec/android/allshare/bd;

    iget-object v0, v0, Lcom/sec/android/allshare/bd;->o:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/sec/android/allshare/IAppControlAPI;->a:Lcom/sec/android/allshare/bd;

    iget-object v0, v0, Lcom/sec/android/allshare/bd;->o:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 146
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/sec/android/allshare/IAppControlAPI;)V
    .locals 2

    .prologue
    .line 236
    iget-object v0, p0, Lcom/sec/android/allshare/IAppControlAPI;->b:Lcom/sec/android/allshare/bc;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/IAppControlAPI;->a:Lcom/sec/android/allshare/bd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/IAppControlAPI;->a:Lcom/sec/android/allshare/bd;

    new-instance v1, Lcom/sec/android/allshare/bc;

    invoke-direct {v1, v0}, Lcom/sec/android/allshare/bc;-><init>(Lcom/sec/android/allshare/bd;)V

    iput-object v1, p0, Lcom/sec/android/allshare/IAppControlAPI;->b:Lcom/sec/android/allshare/bc;

    iget-object v0, p0, Lcom/sec/android/allshare/IAppControlAPI;->b:Lcom/sec/android/allshare/bc;

    iget-object v1, p0, Lcom/sec/android/allshare/IAppControlAPI;->i:Lcom/sec/android/allshare/IAppControlAPI$IControlEventListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/bc;->a(Lcom/sec/android/allshare/IAppControlAPI$IControlEventListener;)V

    iget-object v0, p0, Lcom/sec/android/allshare/IAppControlAPI;->b:Lcom/sec/android/allshare/bc;

    invoke-virtual {v0}, Lcom/sec/android/allshare/bc;->start()V

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/sec/android/allshare/IAppControlAPI;)V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/allshare/IAppControlAPI;->g:Z

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 75
    new-instance v0, Lcom/sec/android/allshare/EventSync;

    invoke-direct {v0}, Lcom/sec/android/allshare/EventSync;-><init>()V

    .line 76
    const/16 v1, 0x270f

    iput v1, v0, Lcom/sec/android/allshare/EventSync;->a:I

    .line 78
    iget-object v1, p0, Lcom/sec/android/allshare/IAppControlAPI;->i:Lcom/sec/android/allshare/IAppControlAPI$IControlEventListener;

    if-eqz v1, :cond_0

    .line 80
    iget-object v1, p0, Lcom/sec/android/allshare/IAppControlAPI;->i:Lcom/sec/android/allshare/IAppControlAPI$IControlEventListener;

    invoke-interface {v1, v0}, Lcom/sec/android/allshare/IAppControlAPI$IControlEventListener;->controlEvent(Lcom/sec/android/allshare/EventSync;)V

    .line 82
    :cond_0
    return-void
.end method

.method public final a(IDIII)V
    .locals 3

    .prologue
    .line 460
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 462
    const/4 v1, 0x6

    iput v1, v0, Landroid/os/Message;->what:I

    .line 464
    new-instance v1, Lcom/sec/android/allshare/EventTouch;

    invoke-direct {v1}, Lcom/sec/android/allshare/EventTouch;-><init>()V

    .line 465
    iput p1, v1, Lcom/sec/android/allshare/EventTouch;->g:I

    .line 466
    double-to-int v2, p2

    iput v2, v1, Lcom/sec/android/allshare/EventTouch;->h:I

    .line 467
    iput p4, v1, Lcom/sec/android/allshare/EventTouch;->i:I

    .line 468
    iput p5, v1, Lcom/sec/android/allshare/EventTouch;->a:I

    .line 469
    iput p6, v1, Lcom/sec/android/allshare/EventTouch;->b:I

    .line 471
    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 473
    invoke-direct {p0, v0}, Lcom/sec/android/allshare/IAppControlAPI;->a(Landroid/os/Message;)V

    .line 475
    return-void
.end method

.method public final a(IIIII)V
    .locals 2

    .prologue
    .line 438
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 440
    const/4 v1, 0x5

    iput v1, v0, Landroid/os/Message;->what:I

    .line 442
    new-instance v1, Lcom/sec/android/allshare/EventTouch;

    invoke-direct {v1}, Lcom/sec/android/allshare/EventTouch;-><init>()V

    .line 443
    iput p1, v1, Lcom/sec/android/allshare/EventTouch;->g:I

    .line 444
    iput p2, v1, Lcom/sec/android/allshare/EventTouch;->a:I

    .line 445
    iput p3, v1, Lcom/sec/android/allshare/EventTouch;->b:I

    .line 446
    iput p4, v1, Lcom/sec/android/allshare/EventTouch;->c:I

    .line 447
    iput p5, v1, Lcom/sec/android/allshare/EventTouch;->d:I

    .line 448
    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 450
    invoke-direct {p0, v0}, Lcom/sec/android/allshare/IAppControlAPI;->a(Landroid/os/Message;)V

    .line 451
    return-void
.end method

.method public final a(Lcom/sec/android/allshare/IAppControlAPI$IControlEventListener;)V
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/sec/android/allshare/IAppControlAPI;->i:Lcom/sec/android/allshare/IAppControlAPI$IControlEventListener;

    .line 87
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 405
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 406
    iput v1, v0, Landroid/os/Message;->what:I

    .line 407
    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 408
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 410
    invoke-direct {p0, v0}, Lcom/sec/android/allshare/IAppControlAPI;->a(Landroid/os/Message;)V

    .line 411
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 187
    iput-boolean v0, p0, Lcom/sec/android/allshare/IAppControlAPI;->g:Z

    .line 189
    new-instance v1, Lcom/sec/android/allshare/NetworkSocketInfo;

    invoke-direct {v1}, Lcom/sec/android/allshare/NetworkSocketInfo;-><init>()V

    .line 192
    iput-object p2, p0, Lcom/sec/android/allshare/IAppControlAPI;->d:Ljava/lang/String;

    iput v0, p0, Lcom/sec/android/allshare/IAppControlAPI;->e:I

    .line 193
    iput-object p1, v1, Lcom/sec/android/allshare/NetworkSocketInfo;->e:Ljava/lang/String;

    .line 195
    iput-object p2, v1, Lcom/sec/android/allshare/NetworkSocketInfo;->b:Ljava/lang/String;

    .line 196
    const v2, 0xd6d8

    iput v2, v1, Lcom/sec/android/allshare/NetworkSocketInfo;->c:I

    .line 197
    iput v0, v1, Lcom/sec/android/allshare/NetworkSocketInfo;->a:I

    .line 199
    iput-object p3, p0, Lcom/sec/android/allshare/IAppControlAPI;->f:Ljava/lang/String;

    new-instance v2, Lcom/sec/android/allshare/bd;

    invoke-direct {v2, p0}, Lcom/sec/android/allshare/bd;-><init>(Lcom/sec/android/allshare/IAppControlAPI;)V

    iput-object v2, p0, Lcom/sec/android/allshare/IAppControlAPI;->a:Lcom/sec/android/allshare/bd;

    iget-object v2, p0, Lcom/sec/android/allshare/IAppControlAPI;->a:Lcom/sec/android/allshare/bd;

    invoke-virtual {v2}, Lcom/sec/android/allshare/bd;->start()V

    .line 201
    iget-object v2, p0, Lcom/sec/android/allshare/IAppControlAPI;->a:Lcom/sec/android/allshare/bd;

    if-eqz v2, :cond_0

    .line 204
    iget-object v2, p0, Lcom/sec/android/allshare/IAppControlAPI;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v3, Lcom/sec/android/allshare/z;

    invoke-direct {v3, p0, v1}, Lcom/sec/android/allshare/z;-><init>(Lcom/sec/android/allshare/IAppControlAPI;Lcom/sec/android/allshare/NetworkSocketInfo;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 228
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 415
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 416
    const/16 v1, 0xe

    iput v1, v0, Landroid/os/Message;->what:I

    .line 417
    const/4 v1, 0x0

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 418
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 420
    invoke-direct {p0, v0}, Lcom/sec/android/allshare/IAppControlAPI;->a(Landroid/os/Message;)V

    .line 421
    return-void
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 249
    iget-boolean v1, p0, Lcom/sec/android/allshare/IAppControlAPI;->g:Z

    if-eqz v1, :cond_0

    .line 262
    :goto_0
    return v0

    .line 254
    :cond_0
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    if-eqz v1, :cond_1

    iput v0, v1, Landroid/os/Message;->what:I

    :cond_1
    invoke-direct {p0, v1}, Lcom/sec/android/allshare/IAppControlAPI;->a(Landroid/os/Message;)V

    iput-object v2, p0, Lcom/sec/android/allshare/IAppControlAPI;->b:Lcom/sec/android/allshare/bc;

    iput v0, p0, Lcom/sec/android/allshare/IAppControlAPI;->e:I

    .line 256
    iget-object v1, p0, Lcom/sec/android/allshare/IAppControlAPI;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 258
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/sec/android/allshare/IAppControlAPI;->a:Lcom/sec/android/allshare/bd;

    .line 259
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/allshare/IAppControlAPI;->b:Lcom/sec/android/allshare/bc;

    .line 256
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 262
    const/4 v0, 0x1

    goto :goto_0

    .line 256
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 356
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 357
    const/16 v1, 0x9

    iput v1, v0, Landroid/os/Message;->what:I

    .line 358
    const/4 v1, 0x0

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 359
    invoke-direct {p0, v0}, Lcom/sec/android/allshare/IAppControlAPI;->a(Landroid/os/Message;)V

    .line 360
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 369
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 370
    const/16 v1, 0x34

    iput v1, v0, Landroid/os/Message;->what:I

    .line 371
    invoke-direct {p0, v0}, Lcom/sec/android/allshare/IAppControlAPI;->a(Landroid/os/Message;)V

    .line 372
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 480
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 481
    const/16 v1, 0x8

    iput v1, v0, Landroid/os/Message;->what:I

    .line 483
    invoke-direct {p0, v0}, Lcom/sec/android/allshare/IAppControlAPI;->a(Landroid/os/Message;)V

    .line 484
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 488
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 489
    const/16 v1, 0xf

    iput v1, v0, Landroid/os/Message;->what:I

    .line 491
    invoke-direct {p0, v0}, Lcom/sec/android/allshare/IAppControlAPI;->a(Landroid/os/Message;)V

    .line 492
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 496
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 497
    const/16 v1, 0x10

    iput v1, v0, Landroid/os/Message;->what:I

    .line 499
    invoke-direct {p0, v0}, Lcom/sec/android/allshare/IAppControlAPI;->a(Landroid/os/Message;)V

    .line 500
    return-void
.end method
