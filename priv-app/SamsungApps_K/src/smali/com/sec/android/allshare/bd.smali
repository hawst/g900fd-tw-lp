.class final Lcom/sec/android/allshare/bd;
.super Ljava/lang/Thread;
.source "ProGuard"


# instance fields
.field public a:Ljava/net/Socket;

.field b:I

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field f:Ljava/lang/String;

.field g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field i:Ljava/lang/String;

.field j:Ljava/nio/ByteBuffer;

.field k:Ljava/io/OutputStream;

.field l:Ljava/io/DataOutputStream;

.field public m:Ljava/util/concurrent/ExecutorService;

.field n:Lcom/sec/android/allshare/IAppControlAPI;

.field public o:Landroid/os/Handler;

.field private p:I

.field private q:Ljava/net/SocketAddress;


# direct methods
.method public constructor <init>(Lcom/sec/android/allshare/IAppControlAPI;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 880
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 782
    const/16 v0, 0x7e4

    iput v0, p0, Lcom/sec/android/allshare/bd;->p:I

    .line 784
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/allshare/bd;->b:I

    .line 788
    iput-object v1, p0, Lcom/sec/android/allshare/bd;->c:Ljava/lang/String;

    .line 789
    iput-object v1, p0, Lcom/sec/android/allshare/bd;->d:Ljava/lang/String;

    .line 790
    iput-object v1, p0, Lcom/sec/android/allshare/bd;->e:Ljava/lang/String;

    .line 791
    iput-object v1, p0, Lcom/sec/android/allshare/bd;->f:Ljava/lang/String;

    .line 792
    iput-object v1, p0, Lcom/sec/android/allshare/bd;->g:Ljava/lang/String;

    .line 793
    iput-object v1, p0, Lcom/sec/android/allshare/bd;->h:Ljava/lang/String;

    .line 794
    iput-object v1, p0, Lcom/sec/android/allshare/bd;->i:Ljava/lang/String;

    .line 798
    iput-object v1, p0, Lcom/sec/android/allshare/bd;->k:Ljava/io/OutputStream;

    .line 799
    iput-object v1, p0, Lcom/sec/android/allshare/bd;->l:Ljava/io/DataOutputStream;

    .line 802
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/allshare/bd;->m:Ljava/util/concurrent/ExecutorService;

    .line 804
    iput-object v1, p0, Lcom/sec/android/allshare/bd;->n:Lcom/sec/android/allshare/IAppControlAPI;

    .line 1398
    iput-object v1, p0, Lcom/sec/android/allshare/bd;->o:Landroid/os/Handler;

    .line 883
    iput-object p1, p0, Lcom/sec/android/allshare/bd;->n:Lcom/sec/android/allshare/IAppControlAPI;

    .line 884
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/allshare/bd;->f:Ljava/lang/String;

    .line 885
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 928
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 929
    add-int/lit8 v1, v0, 0x2

    add-int/lit8 v1, v1, 0x5

    .line 931
    const-string v2, "TVMessageSender"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "sendDTVKeyboardEnd targetLen "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 933
    iget-object v2, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 934
    iget-object v2, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 935
    iget-object v2, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    int-to-short v0, v0

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 936
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    iget-object v2, p0, Lcom/sec/android/allshare/bd;->i:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 937
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 939
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 944
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->l:Ljava/io/DataOutputStream;

    iget-object v2, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Ljava/io/DataOutputStream;->write([BII)V

    .line 945
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->l:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 952
    :goto_0
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 953
    return-void

    .line 947
    :catch_0
    move-exception v0

    .line 950
    const-string v1, "TVMessageSender"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final a(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1258
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 1259
    add-int/lit8 v1, v0, 0x6

    add-int/lit8 v1, v1, 0x5

    .line 1266
    iget-object v2, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 1267
    iget-object v2, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 1268
    iget-object v2, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    int-to-short v0, v0

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1269
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    iget-object v2, p0, Lcom/sec/android/allshare/bd;->i:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 1270
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    const/4 v2, 0x6

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1273
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    const/16 v2, 0xb

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1276
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1277
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 1281
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->l:Ljava/io/DataOutputStream;

    iget-object v2, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Ljava/io/DataOutputStream;->write([BII)V

    .line 1282
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->l:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1289
    :goto_0
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 1291
    return-void

    .line 1284
    :catch_0
    move-exception v0

    .line 1287
    const-string v1, "TVMessageSender"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final a(IIIII)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1021
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 1022
    add-int/lit8 v1, v0, 0x16

    add-int/lit8 v1, v1, 0x5

    .line 1025
    iget-object v2, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 1026
    iget-object v2, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 1027
    iget-object v2, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    int-to-short v0, v0

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1028
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    iget-object v2, p0, Lcom/sec/android/allshare/bd;->i:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 1029
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    const/16 v2, 0x16

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1031
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1034
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1035
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1036
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1037
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1038
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p5}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1040
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 1044
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->l:Ljava/io/DataOutputStream;

    iget-object v2, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Ljava/io/DataOutputStream;->write([BII)V

    .line 1045
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->l:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1052
    :goto_0
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 1054
    return-void

    .line 1047
    :catch_0
    move-exception v0

    .line 1050
    const-string v1, "TVMessageSender"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final a(IIIIII)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1363
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 1364
    add-int/lit8 v1, v0, 0x1a

    add-int/lit8 v1, v1, 0x5

    .line 1367
    iget-object v2, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 1368
    iget-object v2, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 1369
    iget-object v2, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    int-to-short v0, v0

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1370
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    iget-object v2, p0, Lcom/sec/android/allshare/bd;->i:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 1371
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    const/16 v2, 0x1a

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1373
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    const/16 v2, 0x11

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1376
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1377
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1378
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1379
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1380
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p5}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1381
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p6}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1382
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 1386
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->l:Ljava/io/DataOutputStream;

    iget-object v2, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Ljava/io/DataOutputStream;->write([BII)V

    .line 1387
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->l:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1394
    :goto_0
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 1396
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(ILjava/lang/String;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1189
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->a:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isConnected()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1248
    :cond_0
    :goto_0
    return-void

    .line 1192
    :cond_1
    if-eqz p2, :cond_0

    .line 1195
    :try_start_0
    const-string v0, "UNICODE"

    .line 1199
    if-ne p1, v7, :cond_3

    .line 1200
    const-string v0, "UTF-8"

    .line 1206
    :cond_2
    :goto_1
    invoke-virtual {p2, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1215
    if-eqz v0, :cond_0

    .line 1217
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    .line 1218
    iget-object v2, p0, Lcom/sec/android/allshare/bd;->i:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    .line 1219
    add-int/lit8 v3, v1, 0x4

    .line 1220
    add-int v4, v3, v2

    add-int/lit8 v4, v4, 0x5

    .line 1222
    iget-object v5, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 1223
    iget-object v5, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 1224
    iget-object v5, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    int-to-short v2, v2

    invoke-virtual {v5, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1226
    iget-object v2, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    iget-object v5, p0, Lcom/sec/android/allshare/bd;->i:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 1227
    iget-object v2, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    int-to-short v3, v3

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1228
    iget-object v2, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v7}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1231
    iget-object v2, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    int-to-short v1, v1

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1232
    iget-object v1, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 1233
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 1237
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->l:Ljava/io/DataOutputStream;

    iget-object v1, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v4}, Ljava/io/DataOutputStream;->write([BII)V

    .line 1238
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->l:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1246
    :goto_2
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    goto :goto_0

    .line 1201
    :cond_3
    const/4 v1, 0x2

    if-ne p1, v1, :cond_2

    .line 1202
    :try_start_2
    const-string v0, "UTF-16"
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 1208
    :catch_0
    move-exception v0

    .line 1211
    const-string v1, "TVMessageSender"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 1240
    :catch_1
    move-exception v0

    .line 1243
    const-string v1, "TVMessageSender"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_2
.end method

.method public final a(Lcom/sec/android/allshare/NetworkSocketInfo;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 808
    iget v0, p1, Lcom/sec/android/allshare/NetworkSocketInfo;->a:I

    iput v0, p0, Lcom/sec/android/allshare/bd;->b:I

    .line 809
    iget-object v0, p1, Lcom/sec/android/allshare/NetworkSocketInfo;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/allshare/bd;->c:Ljava/lang/String;

    .line 810
    iget v0, p1, Lcom/sec/android/allshare/NetworkSocketInfo;->c:I

    iput v0, p0, Lcom/sec/android/allshare/bd;->p:I

    .line 811
    iget-object v0, p1, Lcom/sec/android/allshare/NetworkSocketInfo;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/allshare/bd;->h:Ljava/lang/String;

    .line 814
    iget-object v0, p1, Lcom/sec/android/allshare/NetworkSocketInfo;->e:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 816
    const-string v0, "00-24-54-92-0A-44"

    iput-object v0, p0, Lcom/sec/android/allshare/bd;->e:Ljava/lang/String;

    .line 823
    :goto_0
    iget v0, p0, Lcom/sec/android/allshare/bd;->b:I

    if-ne v0, v3, :cond_1

    .line 825
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->a:Ljava/net/Socket;

    if-eqz v0, :cond_0

    .line 829
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->a:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 836
    :goto_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/allshare/bd;->a:Ljava/net/Socket;

    .line 839
    :cond_0
    const/16 v0, 0x1000

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    .line 841
    new-instance v0, Ljava/net/Socket;

    invoke-direct {v0}, Ljava/net/Socket;-><init>()V

    iput-object v0, p0, Lcom/sec/android/allshare/bd;->a:Ljava/net/Socket;

    .line 842
    new-instance v0, Ljava/net/InetSocketAddress;

    iget-object v1, p0, Lcom/sec/android/allshare/bd;->c:Ljava/lang/String;

    iget v2, p0, Lcom/sec/android/allshare/bd;->p:I

    invoke-direct {v0, v1, v2}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/sec/android/allshare/bd;->q:Ljava/net/SocketAddress;

    .line 846
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->a:Ljava/net/Socket;

    iget-object v1, p0, Lcom/sec/android/allshare/bd;->q:Ljava/net/SocketAddress;

    const/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    .line 847
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->a:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 849
    const-string v0, "TVMessageSender"

    const-string v1, "initSender : mSocket is connected."

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 851
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 852
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->a:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->getLocalAddress()Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/InetAddress;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/allshare/bd;->d:Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 856
    :try_start_2
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->a:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/allshare/bd;->k:Ljava/io/OutputStream;

    .line 857
    new-instance v0, Ljava/io/DataOutputStream;

    iget-object v1, p0, Lcom/sec/android/allshare/bd;->k:Ljava/io/OutputStream;

    invoke-direct {v0, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lcom/sec/android/allshare/bd;->l:Ljava/io/DataOutputStream;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 878
    :cond_1
    :goto_2
    return-void

    .line 820
    :cond_2
    iget-object v0, p1, Lcom/sec/android/allshare/NetworkSocketInfo;->e:Ljava/lang/String;

    const/16 v1, 0x3a

    const/16 v2, 0x2d

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/allshare/bd;->e:Ljava/lang/String;

    goto :goto_0

    .line 831
    :catch_0
    move-exception v0

    .line 834
    const-string v1, "TVMessageSender"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 859
    :catch_1
    move-exception v0

    .line 862
    :try_start_3
    const-string v1, "TVMessageSender"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_2

    .line 871
    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 868
    :cond_3
    :try_start_4
    const-string v0, "TVMessageSender"

    const-string v1, "initSender : mSocket connecting is failed."

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1145
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->a:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isConnected()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1185
    :cond_0
    :goto_0
    return-void

    .line 1148
    :cond_1
    if-eqz p1, :cond_0

    .line 1152
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v0, v6}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    .line 1154
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    .line 1155
    iget-object v2, p0, Lcom/sec/android/allshare/bd;->i:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    .line 1156
    add-int/lit8 v3, v1, 0x5

    .line 1157
    add-int v4, v3, v2

    add-int/lit8 v4, v4, 0x5

    .line 1159
    iget-object v5, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 1160
    iget-object v5, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 1161
    iget-object v5, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    int-to-short v2, v2

    invoke-virtual {v5, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1162
    iget-object v2, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    iget-object v5, p0, Lcom/sec/android/allshare/bd;->i:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 1164
    iget-object v2, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    int-to-short v3, v3

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1165
    iget-object v2, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v6}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1168
    iget-object v2, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    int-to-byte v3, p2

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 1169
    iget-object v2, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    int-to-short v1, v1

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1170
    iget-object v1, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 1171
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 1175
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->l:Ljava/io/DataOutputStream;

    iget-object v1, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v4}, Ljava/io/DataOutputStream;->write([BII)V

    .line 1176
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->l:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1184
    :goto_1
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    goto :goto_0

    .line 1178
    :catch_0
    move-exception v0

    .line 1181
    const-string v1, "TVMessageSender"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public final b()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 958
    new-instance v0, Ljava/lang/StringBuffer;

    const/16 v1, 0x1000

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 959
    const-string v1, "omnia.iapp.samsung"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 960
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/allshare/bd;->i:Ljava/lang/String;

    .line 964
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v0, v10}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    .line 967
    iget-object v1, p0, Lcom/sec/android/allshare/bd;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-static {v1, v10}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    .line 971
    iget-object v2, p0, Lcom/sec/android/allshare/bd;->f:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-static {v2, v10}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v2

    .line 977
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    .line 978
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    .line 979
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    .line 980
    iget-object v6, p0, Lcom/sec/android/allshare/bd;->i:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    .line 982
    add-int v7, v3, v4

    add-int/2addr v7, v5

    add-int/lit8 v7, v7, 0x8

    .line 983
    add-int v8, v7, v6

    add-int/lit8 v8, v8, 0x5

    .line 985
    iget-object v9, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 986
    iget-object v9, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v9, v10}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 987
    iget-object v9, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    int-to-short v6, v6

    invoke-virtual {v9, v6}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 988
    iget-object v6, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    iget-object v9, p0, Lcom/sec/android/allshare/bd;->i:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->getBytes()[B

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 989
    iget-object v6, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    int-to-short v7, v7

    invoke-virtual {v6, v7}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 991
    iget-object v6, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    const/16 v7, 0x64

    invoke-virtual {v6, v7}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 994
    iget-object v6, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    int-to-short v3, v3

    invoke-virtual {v6, v3}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 995
    iget-object v3, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 996
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    int-to-short v3, v4

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 997
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 998
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    int-to-short v1, v5

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 999
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 1000
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 1004
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->l:Ljava/io/DataOutputStream;

    iget-object v1, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v8}, Ljava/io/DataOutputStream;->write([BII)V

    .line 1005
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->l:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1013
    :goto_0
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 1014
    return-void

    .line 1007
    :catch_0
    move-exception v0

    .line 1010
    const-string v1, "TVMessageSender"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final b(IIIII)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1058
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 1059
    add-int/lit8 v1, v0, 0x1e

    add-int/lit8 v1, v1, 0x5

    .line 1062
    iget-object v2, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 1063
    iget-object v2, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 1064
    iget-object v2, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    int-to-short v0, v0

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1065
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    iget-object v2, p0, Lcom/sec/android/allshare/bd;->i:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 1066
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    const/16 v2, 0x1e

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1068
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    const/4 v2, 0x7

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1071
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1072
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1073
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1074
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1075
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p5}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1076
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    const/16 v2, 0xa

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1077
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 1081
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 1085
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->l:Ljava/io/DataOutputStream;

    iget-object v2, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Ljava/io/DataOutputStream;->write([BII)V

    .line 1086
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->l:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1093
    :goto_0
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 1095
    return-void

    .line 1088
    :catch_0
    move-exception v0

    .line 1091
    const-string v1, "TVMessageSender"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1295
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->i:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1323
    :goto_0
    return-void

    .line 1298
    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 1299
    add-int/lit8 v1, v0, 0x2

    add-int/lit8 v1, v1, 0x5

    .line 1303
    iget-object v2, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 1304
    iget-object v2, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 1305
    iget-object v2, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    int-to-short v0, v0

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1306
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    iget-object v2, p0, Lcom/sec/android/allshare/bd;->i:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 1307
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1309
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    const/16 v2, 0xf

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1314
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->l:Ljava/io/DataOutputStream;

    iget-object v2, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Ljava/io/DataOutputStream;->write([BII)V

    .line 1315
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->l:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1322
    :goto_1
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    goto :goto_0

    .line 1317
    :catch_0
    move-exception v0

    .line 1320
    const-string v1, "TVMessageSender"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public final d()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1327
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->i:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1357
    :goto_0
    return-void

    .line 1332
    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 1333
    add-int/lit8 v1, v0, 0x2

    add-int/lit8 v1, v1, 0x5

    .line 1337
    iget-object v2, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 1338
    iget-object v2, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 1339
    iget-object v2, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    int-to-short v0, v0

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1340
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    iget-object v2, p0, Lcom/sec/android/allshare/bd;->i:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 1341
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1343
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    const/16 v2, 0x10

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 1348
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->l:Ljava/io/DataOutputStream;

    iget-object v2, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Ljava/io/DataOutputStream;->write([BII)V

    .line 1349
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->l:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1356
    :goto_1
    iget-object v0, p0, Lcom/sec/android/allshare/bd;->j:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public final run()V
    .locals 1

    .prologue
    .line 890
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 893
    new-instance v0, Lcom/sec/android/allshare/be;

    invoke-direct {v0, p0}, Lcom/sec/android/allshare/be;-><init>(Lcom/sec/android/allshare/bd;)V

    iput-object v0, p0, Lcom/sec/android/allshare/bd;->o:Landroid/os/Handler;

    .line 895
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 897
    return-void
.end method
