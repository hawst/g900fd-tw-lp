.class final Lcom/sec/android/allshare/am;
.super Lcom/sec/android/allshare/e;
.source "ProGuard"


# static fields
.field private static synthetic b:[I


# instance fields
.field final synthetic a:Lcom/sec/android/allshare/ProviderImpl;


# direct methods
.method constructor <init>(Lcom/sec/android/allshare/ProviderImpl;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/allshare/am;->a:Lcom/sec/android/allshare/ProviderImpl;

    .line 162
    invoke-direct {p0, p2}, Lcom/sec/android/allshare/e;-><init>(Landroid/os/Looper;)V

    return-void
.end method

.method private static synthetic a()[I
    .locals 3

    .prologue
    .line 162
    sget-object v0, Lcom/sec/android/allshare/am;->b:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/allshare/Item$MediaType;->values()[Lcom/sec/android/allshare/Item$MediaType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/allshare/Item$MediaType;->ITEM_AUDIO:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/allshare/Item$MediaType;->ITEM_FOLDER:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/android/allshare/Item$MediaType;->ITEM_IMAGE:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    :try_start_3
    sget-object v1, Lcom/sec/android/allshare/Item$MediaType;->ITEM_UNKNOWN:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_4
    :try_start_4
    sget-object v1, Lcom/sec/android/allshare/Item$MediaType;->ITEM_VIDEO:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_5
    sput-object v0, Lcom/sec/android/allshare/am;->b:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_5

    :catch_1
    move-exception v1

    goto :goto_4

    :catch_2
    move-exception v1

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 12

    .prologue
    .line 167
    if-nez p1, :cond_1

    .line 286
    :cond_0
    :goto_0
    return-void

    .line 169
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v7

    .line 170
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v8

    .line 171
    if-eqz v7, :cond_0

    if-eqz v8, :cond_0

    .line 174
    const-string v0, "BUNDLE_PARCELABLE_CONTENT_BUNDLE_ARRAYLIST"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 175
    if-nez v0, :cond_2

    .line 176
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 178
    :cond_2
    const-string v1, "BUNDLE_INT_STARTINDEX"

    invoke-virtual {v8, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 179
    const-string v1, "BUNDLE_INT_REQUESTCOUNT"

    invoke-virtual {v8, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 180
    const-string v1, "BUNDLE_BOOLEAN_ENDOFITEM"

    invoke-virtual {v8, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 181
    const-string v1, "BUNDLE_ENUM_ERROR"

    invoke-virtual {v8, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/allshare/ERROR;->valueOf(Ljava/lang/String;)Lcom/sec/android/allshare/ERROR;

    move-result-object v6

    .line 182
    const-string v1, "BUNDLE_STRING_ITEM_TYPE_ARRAYLIST"

    invoke-virtual {v8, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v9

    .line 184
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 186
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .line 187
    :cond_3
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_5

    .line 213
    const-string v0, "com.sec.android.allshare.action.ACTION_PROVIDER_REQUEST_SEARCHCRITERIA_ITEMS"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 215
    const-string v0, "BUNDLE_STRING_SEARCHSTRING"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 216
    iget-object v4, p0, Lcom/sec/android/allshare/am;->a:Lcom/sec/android/allshare/ProviderImpl;

    invoke-static {v4}, Lcom/sec/android/allshare/ProviderImpl;->b(Lcom/sec/android/allshare/ProviderImpl;)Lcom/sec/android/allshare/media/Provider$IProviderSearchResponseListener;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 222
    :try_start_0
    new-instance v4, Lcom/sec/android/allshare/media/SearchCriteria$Builder;

    invoke-direct {v4}, Lcom/sec/android/allshare/media/SearchCriteria$Builder;-><init>()V

    .line 223
    invoke-virtual {v4, v0}, Lcom/sec/android/allshare/media/SearchCriteria$Builder;->setKeyword(Ljava/lang/String;)Lcom/sec/android/allshare/media/SearchCriteria$Builder;

    move-result-object v4

    .line 225
    if-eqz v9, :cond_4

    .line 227
    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_9

    .line 234
    :cond_4
    invoke-virtual {v4}, Lcom/sec/android/allshare/media/SearchCriteria$Builder;->build()Lcom/sec/android/allshare/media/SearchCriteria;

    move-result-object v4

    .line 235
    iget-object v0, p0, Lcom/sec/android/allshare/am;->a:Lcom/sec/android/allshare/ProviderImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/ProviderImpl;->b(Lcom/sec/android/allshare/ProviderImpl;)Lcom/sec/android/allshare/media/Provider$IProviderSearchResponseListener;

    move-result-object v0

    invoke-interface/range {v0 .. v6}, Lcom/sec/android/allshare/media/Provider$IProviderSearchResponseListener;->onSearchResponseReceived(Ljava/util/ArrayList;IILcom/sec/android/allshare/media/SearchCriteria;ZLcom/sec/android/allshare/ERROR;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_0

    .line 237
    :catch_0
    move-exception v0

    .line 239
    const-string v1, "ProviderImpl"

    const-string v2, "mAllShareRespHandler.handleResponseMessage Exception"

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 186
    :cond_5
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 189
    if-nez v0, :cond_6

    sget-object v4, Lcom/sec/android/allshare/Item$MediaType;->ITEM_UNKNOWN:Lcom/sec/android/allshare/Item$MediaType;

    .line 190
    :goto_3
    if-eqz v4, :cond_3

    .line 191
    invoke-static {}, Lcom/sec/android/allshare/am;->a()[I

    move-result-object v11

    .line 193
    invoke-virtual {v4}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v4

    aget v4, v11, v4

    packed-switch v4, :pswitch_data_0

    goto/16 :goto_0

    .line 205
    :pswitch_0
    new-instance v4, Lcom/sec/android/allshare/FolderItemImpl;

    invoke-direct {v4, v0}, Lcom/sec/android/allshare/FolderItemImpl;-><init>(Landroid/os/Bundle;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 189
    :cond_6
    const-string v4, "BUNDLE_STRING_ITEM_TYPE"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_7

    sget-object v4, Lcom/sec/android/allshare/Item$MediaType;->ITEM_UNKNOWN:Lcom/sec/android/allshare/Item$MediaType;

    goto :goto_3

    :cond_7
    invoke-static {v4}, Lcom/sec/android/allshare/Item$MediaType;->valueOf(Ljava/lang/String;)Lcom/sec/android/allshare/Item$MediaType;

    move-result-object v11

    if-nez v11, :cond_8

    sget-object v4, Lcom/sec/android/allshare/Item$MediaType;->ITEM_UNKNOWN:Lcom/sec/android/allshare/Item$MediaType;

    goto :goto_3

    :cond_8
    invoke-static {v4}, Lcom/sec/android/allshare/Item$MediaType;->valueOf(Ljava/lang/String;)Lcom/sec/android/allshare/Item$MediaType;

    move-result-object v4

    goto :goto_3

    .line 196
    :pswitch_1
    new-instance v4, Lcom/sec/android/allshare/AudioItemImpl;

    invoke-direct {v4, v0}, Lcom/sec/android/allshare/AudioItemImpl;-><init>(Landroid/os/Bundle;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 199
    :pswitch_2
    new-instance v4, Lcom/sec/android/allshare/ImageItemImpl;

    invoke-direct {v4, v0}, Lcom/sec/android/allshare/ImageItemImpl;-><init>(Landroid/os/Bundle;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 202
    :pswitch_3
    new-instance v4, Lcom/sec/android/allshare/VideoItemImpl;

    invoke-direct {v4, v0}, Lcom/sec/android/allshare/VideoItemImpl;-><init>(Landroid/os/Bundle;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 227
    :cond_9
    :try_start_1
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 229
    invoke-static {v0}, Lcom/sec/android/allshare/Item$MediaType;->valueOf(Ljava/lang/String;)Lcom/sec/android/allshare/Item$MediaType;

    move-result-object v0

    .line 230
    invoke-virtual {v4, v0}, Lcom/sec/android/allshare/media/SearchCriteria$Builder;->addItemType(Lcom/sec/android/allshare/Item$MediaType;)Lcom/sec/android/allshare/media/SearchCriteria$Builder;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Error; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_2

    .line 241
    :catch_1
    move-exception v0

    .line 243
    const-string v1, "ProviderImpl"

    const-string v2, "mAllShareRespHandler.handleResponseMessage Error"

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Error;)V

    goto/16 :goto_0

    .line 248
    :cond_a
    const-string v0, "com.sec.android.allshare.action.ACTION_PROVIDER_REQUEST_ITEMS"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 250
    const-string v0, "BUNDLE_PARCELABLE_FOLDERITEM"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 251
    const/4 v4, 0x0

    .line 253
    if-eqz v0, :cond_c

    .line 255
    const-string v4, "BUNDLE_STRING_OBJECT_ID"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 257
    if-eqz v4, :cond_b

    const-string v7, "0"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 259
    :cond_b
    new-instance v4, Lcom/sec/android/allshare/ProviderImpl$RootFolderItem;

    invoke-direct {v4, v0}, Lcom/sec/android/allshare/ProviderImpl$RootFolderItem;-><init>(Landroid/os/Bundle;)V

    .line 267
    :cond_c
    :goto_4
    iget-object v0, p0, Lcom/sec/android/allshare/am;->a:Lcom/sec/android/allshare/ProviderImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/ProviderImpl;->c(Lcom/sec/android/allshare/ProviderImpl;)Lcom/sec/android/allshare/media/Provider$IProviderBrowseResponseListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 273
    :try_start_2
    iget-object v0, p0, Lcom/sec/android/allshare/am;->a:Lcom/sec/android/allshare/ProviderImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/ProviderImpl;->c(Lcom/sec/android/allshare/ProviderImpl;)Lcom/sec/android/allshare/media/Provider$IProviderBrowseResponseListener;

    move-result-object v0

    invoke-interface/range {v0 .. v6}, Lcom/sec/android/allshare/media/Provider$IProviderBrowseResponseListener;->onBrowseResponseReceived(Ljava/util/ArrayList;IILcom/sec/android/allshare/Item;ZLcom/sec/android/allshare/ERROR;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Error; {:try_start_2 .. :try_end_2} :catch_3

    goto/16 :goto_0

    .line 275
    :catch_2
    move-exception v0

    .line 277
    const-string v1, "ProviderImpl"

    const-string v2, "mAllShareRespHandler.handleResponseMessage Exception"

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 263
    :cond_d
    new-instance v4, Lcom/sec/android/allshare/FolderItemImpl;

    invoke-direct {v4, v0}, Lcom/sec/android/allshare/FolderItemImpl;-><init>(Landroid/os/Bundle;)V

    goto :goto_4

    .line 279
    :catch_3
    move-exception v0

    .line 281
    const-string v1, "ProviderImpl"

    const-string v2, "mAllShareRespHandler.handleResponseMessage Error"

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Error;)V

    goto/16 :goto_0

    .line 193
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
