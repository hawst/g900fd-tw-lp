.class final Lcom/sec/android/allshare/be;
.super Landroid/os/Handler;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/allshare/bd;


# direct methods
.method constructor <init>(Lcom/sec/android/allshare/bd;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/allshare/be;->a:Lcom/sec/android/allshare/bd;

    .line 1402
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    .line 1408
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 1556
    :cond_0
    :goto_0
    return-void

    .line 1414
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/allshare/be;->a:Lcom/sec/android/allshare/bd;

    iget-object v0, v0, Lcom/sec/android/allshare/bd;->a:Ljava/net/Socket;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/be;->a:Lcom/sec/android/allshare/bd;

    iget-object v0, v0, Lcom/sec/android/allshare/bd;->a:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1416
    const-string v0, "TVMessageSender"

    const-string v1, "sendDTVKeyboardEnd"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1417
    iget-object v0, p0, Lcom/sec/android/allshare/be;->a:Lcom/sec/android/allshare/bd;

    invoke-virtual {v0}, Lcom/sec/android/allshare/bd;->a()V

    goto :goto_0

    .line 1422
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/allshare/be;->a:Lcom/sec/android/allshare/bd;

    iget-object v0, v0, Lcom/sec/android/allshare/bd;->a:Ljava/net/Socket;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/allshare/be;->a:Lcom/sec/android/allshare/bd;

    iget-object v0, v0, Lcom/sec/android/allshare/bd;->a:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1424
    iget-object v0, p0, Lcom/sec/android/allshare/be;->a:Lcom/sec/android/allshare/bd;

    invoke-virtual {v0}, Lcom/sec/android/allshare/bd;->b()V

    goto :goto_0

    .line 1428
    :cond_1
    const-string v0, "TVMessageSender"

    const-string v1, "CONTROLLER_EVENT_DEVICE_AUTHENTICATION : socket is not connected"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1429
    iget-object v0, p0, Lcom/sec/android/allshare/be;->a:Lcom/sec/android/allshare/bd;

    iget-object v0, v0, Lcom/sec/android/allshare/bd;->n:Lcom/sec/android/allshare/IAppControlAPI;

    invoke-virtual {v0}, Lcom/sec/android/allshare/IAppControlAPI;->a()V

    goto :goto_0

    .line 1434
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/allshare/be;->a:Lcom/sec/android/allshare/bd;

    iget v0, v0, Lcom/sec/android/allshare/bd;->b:I

    if-ne v0, v1, :cond_0

    .line 1436
    iget-object v0, p0, Lcom/sec/android/allshare/be;->a:Lcom/sec/android/allshare/bd;

    iget-object v0, v0, Lcom/sec/android/allshare/bd;->a:Ljava/net/Socket;

    if-eqz v0, :cond_0

    .line 1440
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/allshare/be;->a:Lcom/sec/android/allshare/bd;

    iget-object v0, v0, Lcom/sec/android/allshare/bd;->a:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1447
    :goto_1
    iget-object v0, p0, Lcom/sec/android/allshare/be;->a:Lcom/sec/android/allshare/bd;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/sec/android/allshare/bd;->a:Ljava/net/Socket;

    goto :goto_0

    .line 1442
    :catch_0
    move-exception v0

    .line 1445
    const-string v1, "TVMessageSender"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 1452
    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/allshare/be;->a:Lcom/sec/android/allshare/bd;

    iget v0, v0, Lcom/sec/android/allshare/bd;->b:I

    if-ne v0, v1, :cond_0

    .line 1454
    iget-object v0, p0, Lcom/sec/android/allshare/be;->a:Lcom/sec/android/allshare/bd;

    iget-object v0, v0, Lcom/sec/android/allshare/bd;->a:Ljava/net/Socket;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/allshare/be;->a:Lcom/sec/android/allshare/bd;

    iget-object v0, v0, Lcom/sec/android/allshare/bd;->a:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1456
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-nez v0, :cond_0

    .line 1458
    iget-object v1, p0, Lcom/sec/android/allshare/be;->a:Lcom/sec/android/allshare/bd;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/allshare/bd;->a(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 1463
    :cond_2
    const-string v0, "TVMessageSender"

    const-string v1, "CONTROLLER_EVENT_REMOTECONTROL_KEY : socket is not connected"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1469
    :sswitch_4
    iget-object v0, p0, Lcom/sec/android/allshare/be;->a:Lcom/sec/android/allshare/bd;

    iget v0, v0, Lcom/sec/android/allshare/bd;->b:I

    if-ne v0, v1, :cond_0

    .line 1472
    iget-object v0, p0, Lcom/sec/android/allshare/be;->a:Lcom/sec/android/allshare/bd;

    iget-object v0, v0, Lcom/sec/android/allshare/bd;->a:Ljava/net/Socket;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/allshare/be;->a:Lcom/sec/android/allshare/bd;

    iget-object v0, v0, Lcom/sec/android/allshare/bd;->a:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1474
    iget-object v1, p0, Lcom/sec/android/allshare/be;->a:Lcom/sec/android/allshare/bd;

    iget v2, p1, Landroid/os/Message;->arg1:I

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/allshare/bd;->a(ILjava/lang/String;)V

    goto/16 :goto_0

    .line 1478
    :cond_3
    const-string v0, "TVMessageSender"

    const-string v1, "CONTROLLER_EVENT_KEYBOARD_INPUT : socket is not connected"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1484
    :sswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object v5, v0

    check-cast v5, Lcom/sec/android/allshare/EventTouch;

    .line 1486
    iget-object v0, p0, Lcom/sec/android/allshare/be;->a:Lcom/sec/android/allshare/bd;

    iget v0, v0, Lcom/sec/android/allshare/bd;->b:I

    if-ne v0, v1, :cond_0

    .line 1488
    iget-object v0, p0, Lcom/sec/android/allshare/be;->a:Lcom/sec/android/allshare/bd;

    iget-object v0, v0, Lcom/sec/android/allshare/bd;->a:Ljava/net/Socket;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/allshare/be;->a:Lcom/sec/android/allshare/bd;

    iget-object v0, v0, Lcom/sec/android/allshare/bd;->a:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1490
    iget-object v0, p0, Lcom/sec/android/allshare/be;->a:Lcom/sec/android/allshare/bd;

    iget v1, v5, Lcom/sec/android/allshare/EventTouch;->g:I

    iget v2, v5, Lcom/sec/android/allshare/EventTouch;->h:I

    iget v3, v5, Lcom/sec/android/allshare/EventTouch;->i:I

    iget v4, v5, Lcom/sec/android/allshare/EventTouch;->a:I

    iget v5, v5, Lcom/sec/android/allshare/EventTouch;->b:I

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/allshare/bd;->a(IIIII)V

    goto/16 :goto_0

    .line 1494
    :cond_4
    const-string v0, "TVMessageSender"

    const-string v1, "CONTROLLER_EVENT_SEMANTIC : socket is not connected"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1499
    :sswitch_6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object v5, v0

    check-cast v5, Lcom/sec/android/allshare/EventTouch;

    .line 1501
    iget-object v0, p0, Lcom/sec/android/allshare/be;->a:Lcom/sec/android/allshare/bd;

    iget v0, v0, Lcom/sec/android/allshare/bd;->b:I

    if-ne v0, v1, :cond_0

    .line 1503
    iget-object v0, p0, Lcom/sec/android/allshare/be;->a:Lcom/sec/android/allshare/bd;

    iget-object v0, v0, Lcom/sec/android/allshare/bd;->a:Ljava/net/Socket;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/allshare/be;->a:Lcom/sec/android/allshare/bd;

    iget-object v0, v0, Lcom/sec/android/allshare/bd;->a:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1505
    iget-object v0, p0, Lcom/sec/android/allshare/be;->a:Lcom/sec/android/allshare/bd;

    iget v1, v5, Lcom/sec/android/allshare/EventTouch;->g:I

    iget v2, v5, Lcom/sec/android/allshare/EventTouch;->a:I

    iget v3, v5, Lcom/sec/android/allshare/EventTouch;->b:I

    iget v4, v5, Lcom/sec/android/allshare/EventTouch;->c:I

    iget v5, v5, Lcom/sec/android/allshare/EventTouch;->d:I

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/allshare/bd;->b(IIIII)V

    goto/16 :goto_0

    .line 1509
    :cond_5
    const-string v0, "TVMessageSender"

    const-string v1, "CONTROLLER_EVENT_GESTURE : socket is not connected"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1516
    :sswitch_7
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object v6, v0

    check-cast v6, Lcom/sec/android/allshare/EventMouse;

    .line 1517
    iget-object v0, p0, Lcom/sec/android/allshare/be;->a:Lcom/sec/android/allshare/bd;

    iget v0, v0, Lcom/sec/android/allshare/bd;->b:I

    if-ne v0, v1, :cond_0

    .line 1519
    iget-object v0, p0, Lcom/sec/android/allshare/be;->a:Lcom/sec/android/allshare/bd;

    iget-object v0, v0, Lcom/sec/android/allshare/bd;->a:Ljava/net/Socket;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/allshare/be;->a:Lcom/sec/android/allshare/bd;

    iget-object v0, v0, Lcom/sec/android/allshare/bd;->a:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1521
    iget-object v0, p0, Lcom/sec/android/allshare/be;->a:Lcom/sec/android/allshare/bd;

    iget v1, v6, Lcom/sec/android/allshare/EventMouse;->a:I

    iget v2, v6, Lcom/sec/android/allshare/EventMouse;->b:I

    iget v3, v6, Lcom/sec/android/allshare/EventMouse;->c:I

    iget v4, v6, Lcom/sec/android/allshare/EventMouse;->d:I

    iget v5, v6, Lcom/sec/android/allshare/EventMouse;->e:I

    iget v6, v6, Lcom/sec/android/allshare/EventMouse;->f:I

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/allshare/bd;->a(IIIIII)V

    goto/16 :goto_0

    .line 1525
    :cond_6
    const-string v0, "TVMessageSender"

    const-string v1, "CONTROLLER_EVENT_MOUSE : socket is not connected"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1532
    :sswitch_8
    iget-object v0, p0, Lcom/sec/android/allshare/be;->a:Lcom/sec/android/allshare/bd;

    iget-object v0, v0, Lcom/sec/android/allshare/bd;->a:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1534
    iget-object v0, p0, Lcom/sec/android/allshare/be;->a:Lcom/sec/android/allshare/bd;

    invoke-virtual {v0}, Lcom/sec/android/allshare/bd;->c()V

    goto/16 :goto_0

    .line 1538
    :cond_7
    const-string v0, "TVMessageSender"

    const-string v1, "CONTROLLER_EVENT_MOUSE_CREATE : socket is not connected"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1543
    :sswitch_9
    iget-object v0, p0, Lcom/sec/android/allshare/be;->a:Lcom/sec/android/allshare/bd;

    iget-object v0, v0, Lcom/sec/android/allshare/bd;->a:Ljava/net/Socket;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/allshare/be;->a:Lcom/sec/android/allshare/bd;

    iget-object v0, v0, Lcom/sec/android/allshare/bd;->a:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1545
    iget-object v0, p0, Lcom/sec/android/allshare/be;->a:Lcom/sec/android/allshare/bd;

    invoke-virtual {v0}, Lcom/sec/android/allshare/bd;->d()V

    goto/16 :goto_0

    .line 1549
    :cond_8
    const-string v0, "TVMessageSender"

    const-string v1, "CONTROLLER_EVENT_MOUSE_DESTROY : socket is not connected"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1555
    :sswitch_a
    iget-object v0, p0, Lcom/sec/android/allshare/be;->a:Lcom/sec/android/allshare/bd;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/bd;->a(I)V

    goto/16 :goto_0

    .line 1408
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_2
        0x1 -> :sswitch_4
        0x2 -> :sswitch_7
        0x5 -> :sswitch_6
        0x6 -> :sswitch_5
        0x8 -> :sswitch_0
        0x9 -> :sswitch_a
        0xe -> :sswitch_3
        0xf -> :sswitch_8
        0x10 -> :sswitch_9
        0x34 -> :sswitch_1
    .end sparse-switch
.end method
