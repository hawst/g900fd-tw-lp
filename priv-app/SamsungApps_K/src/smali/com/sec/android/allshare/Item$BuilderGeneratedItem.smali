.class Lcom/sec/android/allshare/Item$BuilderGeneratedItem;
.super Lcom/sec/android/allshare/Item;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/allshare/iface/IBundleHolder;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static synthetic e:[I


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Lcom/sec/android/allshare/ah;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 550
    new-instance v0, Lcom/sec/android/allshare/af;

    invoke-direct {v0}, Lcom/sec/android/allshare/af;-><init>()V

    sput-object v0, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 368
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 565
    invoke-direct {p0}, Lcom/sec/android/allshare/Item;-><init>()V

    .line 373
    sget-object v0, Lcom/sec/android/allshare/ah;->d:Lcom/sec/android/allshare/ah;

    iput-object v0, p0, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->d:Lcom/sec/android/allshare/ah;

    .line 567
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->a:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->b:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->c:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-static {v0}, Lcom/sec/android/allshare/ah;->valueOf(Ljava/lang/String;)Lcom/sec/android/allshare/ah;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->d:Lcom/sec/android/allshare/ah;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 568
    :goto_0
    return-void

    .line 567
    :catch_0
    move-exception v0

    sget-object v0, Lcom/sec/android/allshare/ah;->d:Lcom/sec/android/allshare/ah;

    iput-object v0, p0, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->d:Lcom/sec/android/allshare/ah;

    goto :goto_0
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 565
    invoke-direct {p0, p1}, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Lcom/sec/android/allshare/ah;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 383
    invoke-direct {p0}, Lcom/sec/android/allshare/Item;-><init>()V

    .line 373
    sget-object v0, Lcom/sec/android/allshare/ah;->d:Lcom/sec/android/allshare/ah;

    iput-object v0, p0, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->d:Lcom/sec/android/allshare/ah;

    .line 385
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->a:Ljava/lang/String;

    .line 386
    iput-object p4, p0, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->b:Ljava/lang/String;

    .line 387
    iput-object p3, p0, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->c:Ljava/lang/String;

    .line 388
    iput-object p1, p0, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->d:Lcom/sec/android/allshare/ah;

    .line 389
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/allshare/ah;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;B)V
    .locals 0

    .prologue
    .line 383
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;-><init>(Lcom/sec/android/allshare/ah;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Lcom/sec/android/allshare/ah;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 375
    invoke-direct {p0}, Lcom/sec/android/allshare/Item;-><init>()V

    .line 373
    sget-object v0, Lcom/sec/android/allshare/ah;->d:Lcom/sec/android/allshare/ah;

    iput-object v0, p0, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->d:Lcom/sec/android/allshare/ah;

    .line 377
    iput-object p2, p0, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->a:Ljava/lang/String;

    .line 378
    iput-object p4, p0, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->b:Ljava/lang/String;

    .line 379
    iput-object p3, p0, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->c:Ljava/lang/String;

    .line 380
    iput-object p1, p0, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->d:Lcom/sec/android/allshare/ah;

    .line 381
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/allshare/ah;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;B)V
    .locals 0

    .prologue
    .line 375
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;-><init>(Lcom/sec/android/allshare/ah;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private static synthetic a()[I
    .locals 3

    .prologue
    .line 368
    sget-object v0, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->e:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/allshare/ah;->values()[Lcom/sec/android/allshare/ah;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/allshare/ah;->c:Lcom/sec/android/allshare/ah;

    invoke-virtual {v1}, Lcom/sec/android/allshare/ah;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_3

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/allshare/ah;->a:Lcom/sec/android/allshare/ah;

    invoke-virtual {v1}, Lcom/sec/android/allshare/ah;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_2

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/android/allshare/ah;->d:Lcom/sec/android/allshare/ah;

    invoke-virtual {v1}, Lcom/sec/android/allshare/ah;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1

    :goto_3
    :try_start_3
    sget-object v1, Lcom/sec/android/allshare/ah;->b:Lcom/sec/android/allshare/ah;

    invoke-virtual {v1}, Lcom/sec/android/allshare/ah;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_0

    :goto_4
    sput-object v0, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->e:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_4

    :catch_1
    move-exception v1

    goto :goto_3

    :catch_2
    move-exception v1

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 520
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 573
    if-ne p0, p1, :cond_1

    .line 574
    const/4 v0, 0x1

    .line 586
    :cond_0
    :goto_0
    return v0

    .line 575
    :cond_1
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;

    if-eqz v1, :cond_0

    .line 578
    check-cast p1, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;

    .line 580
    invoke-virtual {p0}, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->getURI()Landroid/net/Uri;

    move-result-object v1

    if-nez v1, :cond_2

    .line 582
    invoke-virtual {p1}, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->getURI()Landroid/net/Uri;

    move-result-object v1

    if-nez v1, :cond_0

    .line 586
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->getURI()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->getURI()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public getAlbumTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 448
    const-string v0, ""

    return-object v0
.end method

.method public getArtist()Ljava/lang/String;
    .locals 1

    .prologue
    .line 454
    const-string v0, ""

    return-object v0
.end method

.method public getBundle()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 434
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 435
    const-string v1, "BUNDLE_STRING_ITEM_TYPE"

    invoke-virtual {p0}, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->getType()Lcom/sec/android/allshare/Item$MediaType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/allshare/Item$MediaType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    const-string v1, "BUNDLE_STRING_ITEM_TITLE"

    iget-object v2, p0, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    const-string v1, "BUNDLE_STRING_FILEPATH"

    iget-object v2, p0, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    const-string v1, "BUNDLE_PARCELABLE_ITEM_URI"

    invoke-virtual {p0}, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->getURI()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 439
    const-string v1, "BUNDLE_STRING_ITEM_MIMETYPE"

    iget-object v2, p0, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    const-string v1, "BUNDLE_STRING_ITEM_CONSTRUCTOR_KEY"

    iget-object v2, p0, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->d:Lcom/sec/android/allshare/ah;

    invoke-virtual {v2}, Lcom/sec/android/allshare/ah;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    return-object v0
.end method

.method public getContentBuildType()Lcom/sec/android/allshare/Item$ContentBuildType;
    .locals 2

    .prologue
    .line 592
    invoke-static {}, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->a()[I

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->d:Lcom/sec/android/allshare/ah;

    invoke-virtual {v1}, Lcom/sec/android/allshare/ah;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 605
    sget-object v0, Lcom/sec/android/allshare/Item$ContentBuildType;->UNKNOWN:Lcom/sec/android/allshare/Item$ContentBuildType;

    :goto_0
    return-object v0

    .line 595
    :pswitch_0
    sget-object v0, Lcom/sec/android/allshare/Item$ContentBuildType;->LOCAL:Lcom/sec/android/allshare/Item$ContentBuildType;

    goto :goto_0

    .line 597
    :pswitch_1
    sget-object v0, Lcom/sec/android/allshare/Item$ContentBuildType;->PROVIDER:Lcom/sec/android/allshare/Item$ContentBuildType;

    goto :goto_0

    .line 599
    :pswitch_2
    sget-object v0, Lcom/sec/android/allshare/Item$ContentBuildType;->WEB:Lcom/sec/android/allshare/Item$ContentBuildType;

    goto :goto_0

    .line 601
    :pswitch_3
    sget-object v0, Lcom/sec/android/allshare/Item$ContentBuildType;->UNKNOWN:Lcom/sec/android/allshare/Item$ContentBuildType;

    goto :goto_0

    .line 592
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public getDate()Ljava/util/Date;
    .locals 1

    .prologue
    .line 394
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDuration()J
    .locals 2

    .prologue
    .line 466
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public getExtension()Ljava/lang/String;
    .locals 1

    .prologue
    .line 514
    const-string v0, ""

    return-object v0
.end method

.method public getFileSize()J
    .locals 2

    .prologue
    .line 502
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public getGenre()Ljava/lang/String;
    .locals 1

    .prologue
    .line 460
    const-string v0, ""

    return-object v0
.end method

.method public getLocation()Landroid/location/Location;
    .locals 1

    .prologue
    .line 490
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMimetype()Ljava/lang/String;
    .locals 1

    .prologue
    .line 508
    iget-object v0, p0, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getResolution()Ljava/lang/String;
    .locals 1

    .prologue
    .line 484
    const-string v0, ""

    return-object v0
.end method

.method public getSubtitle()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 496
    const/4 v0, 0x0

    return-object v0
.end method

.method public getThumbnail()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 478
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 400
    iget-object v0, p0, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lcom/sec/android/allshare/Item$MediaType;
    .locals 1

    .prologue
    .line 406
    iget-object v0, p0, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/allshare/Item;->a(Ljava/lang/String;)Lcom/sec/android/allshare/Item$MediaType;

    move-result-object v0

    return-object v0
.end method

.method public getURI()Landroid/net/Uri;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 412
    iget-object v1, p0, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 427
    :goto_0
    return-object v0

    .line 417
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 418
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    .line 420
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 421
    :cond_1
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->a:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 423
    goto :goto_0

    .line 427
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public isRootFolder()Z
    .locals 1

    .prologue
    .line 472
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 526
    iget-object v0, p0, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 527
    iget-object v0, p0, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 528
    iget-object v0, p0, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 529
    iget-object v0, p0, Lcom/sec/android/allshare/Item$BuilderGeneratedItem;->d:Lcom/sec/android/allshare/ah;

    invoke-virtual {v0}, Lcom/sec/android/allshare/ah;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 530
    return-void
.end method
