.class final Lcom/sec/android/allshare/TVControllerImpl;
.super Lcom/sec/android/allshare/control/TVController;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/allshare/IAppControlAPI$IControlEventListener;
.implements Lcom/sec/android/allshare/iface/IHandlerHolder;


# instance fields
.field a:Lcom/sec/android/allshare/e;

.field private b:Lcom/sec/android/allshare/IAllShareConnector;

.field private c:Lcom/sec/android/allshare/j;

.field private d:Lcom/sec/android/allshare/control/TVController$IEventListener;

.field private e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

.field private f:Lcom/sec/android/allshare/IAppControlAPI;

.field private g:Ljava/lang/String;

.field private h:Z

.field private i:Lcom/sec/android/allshare/control/TVController$TVInformation;

.field private j:Landroid/os/Handler;

.field private k:Lcom/sec/android/allshare/d;

.field private l:I

.field private m:I


# direct methods
.method constructor <init>(Lcom/sec/android/allshare/IAllShareConnector;Lcom/sec/android/allshare/j;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 74
    invoke-direct {p0}, Lcom/sec/android/allshare/control/TVController;-><init>()V

    .line 55
    iput-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    .line 56
    iput-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->c:Lcom/sec/android/allshare/j;

    .line 58
    iput-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->d:Lcom/sec/android/allshare/control/TVController$IEventListener;

    .line 59
    iput-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    .line 61
    iput-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->f:Lcom/sec/android/allshare/IAppControlAPI;

    .line 68
    iput-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->g:Ljava/lang/String;

    .line 70
    iput-boolean v2, p0, Lcom/sec/android/allshare/TVControllerImpl;->h:Z

    .line 72
    iput-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->i:Lcom/sec/android/allshare/control/TVController$TVInformation;

    .line 110
    new-instance v0, Lcom/sec/android/allshare/ay;

    invoke-static {}, Lcom/sec/android/allshare/ServiceConnector;->b()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/allshare/ay;-><init>(Lcom/sec/android/allshare/TVControllerImpl;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->j:Landroid/os/Handler;

    .line 229
    new-instance v0, Lcom/sec/android/allshare/az;

    invoke-static {}, Lcom/sec/android/allshare/ServiceConnector;->b()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/allshare/az;-><init>(Lcom/sec/android/allshare/TVControllerImpl;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->k:Lcom/sec/android/allshare/d;

    .line 257
    new-instance v0, Lcom/sec/android/allshare/ba;

    invoke-static {}, Lcom/sec/android/allshare/ServiceConnector;->b()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/allshare/ba;-><init>(Lcom/sec/android/allshare/TVControllerImpl;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->a:Lcom/sec/android/allshare/e;

    .line 903
    iput v2, p0, Lcom/sec/android/allshare/TVControllerImpl;->l:I

    .line 904
    iput v2, p0, Lcom/sec/android/allshare/TVControllerImpl;->m:I

    .line 76
    if-nez p1, :cond_0

    .line 78
    const-string v0, "TVControllerImpl"

    const-string v1, "Connection FAIL: AllShare Service Connector does not exist"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    :goto_0
    return-void

    .line 82
    :cond_0
    invoke-static {}, Lcom/sec/android/allshare/ServiceConnector;->a()Landroid/content/Context;

    move-result-object v0

    .line 84
    if-eqz v0, :cond_1

    .line 86
    invoke-static {}, Lcom/sec/android/allshare/AVDUtil;->isAVD()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 87
    const-string v0, "00-24-54-92-0A-44"

    iput-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->g:Ljava/lang/String;

    .line 94
    :cond_1
    :goto_1
    iput-object p1, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    .line 95
    iput-object p2, p0, Lcom/sec/android/allshare/TVControllerImpl;->c:Lcom/sec/android/allshare/j;

    .line 97
    new-instance v0, Lcom/sec/android/allshare/IAppControlAPI;

    invoke-direct {v0}, Lcom/sec/android/allshare/IAppControlAPI;-><init>()V

    iput-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->f:Lcom/sec/android/allshare/IAppControlAPI;

    .line 99
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    invoke-virtual {p2}, Lcom/sec/android/allshare/j;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/allshare/TVControllerImpl;->k:Lcom/sec/android/allshare/d;

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/android/allshare/IAllShareConnector;->a(Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/allshare/d;)Z

    goto :goto_0

    .line 89
    :cond_2
    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 90
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->g:Ljava/lang/String;

    goto :goto_1
.end method

.method static synthetic a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 732
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->f:Lcom/sec/android/allshare/IAppControlAPI;

    if-eqz v0, :cond_0

    .line 734
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->f:Lcom/sec/android/allshare/IAppControlAPI;

    .line 735
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->f:Lcom/sec/android/allshare/IAppControlAPI;

    invoke-virtual {v0}, Lcom/sec/android/allshare/IAppControlAPI;->g()V

    .line 736
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->f:Lcom/sec/android/allshare/IAppControlAPI;

    invoke-virtual {v0}, Lcom/sec/android/allshare/IAppControlAPI;->b()Z

    .line 739
    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->d:Lcom/sec/android/allshare/control/TVController$IEventListener;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->h:Z

    if-eqz v0, :cond_1

    .line 741
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->d:Lcom/sec/android/allshare/control/TVController$IEventListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IEventListener;->onDisconnected(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    .line 744
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->h:Z

    .line 745
    return-void
.end method

.method static synthetic a(Lcom/sec/android/allshare/TVControllerImpl;Lcom/sec/android/allshare/control/TVController$TVInformation;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/android/allshare/TVControllerImpl;->i:Lcom/sec/android/allshare/control/TVController$TVInformation;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/allshare/TVControllerImpl;Z)V
    .locals 0

    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/sec/android/allshare/TVControllerImpl;->h:Z

    return-void
.end method

.method static synthetic b(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IEventListener;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->d:Lcom/sec/android/allshare/control/TVController$IEventListener;

    return-object v0
.end method

.method private b()Z
    .locals 2

    .prologue
    .line 2221
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->i:Lcom/sec/android/allshare/control/TVController$TVInformation;

    if-eqz v0, :cond_0

    .line 2223
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->i:Lcom/sec/android/allshare/control/TVController$TVInformation;

    invoke-virtual {v0}, Lcom/sec/android/allshare/control/TVController$TVInformation;->getVersion()Ljava/lang/String;

    move-result-object v0

    .line 2225
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 2227
    const-string v1, "2012"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 2228
    if-ltz v0, :cond_0

    .line 2230
    const/4 v0, 0x1

    .line 2235
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/IAppControlAPI;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->f:Lcom/sec/android/allshare/IAppControlAPI;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/android/allshare/TVControllerImpl;)V
    .locals 4

    .prologue
    .line 799
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->a()V

    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/TVControllerImpl;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "com.sec.android.allshare.action.ACTION_CONTROL_TV_GET_DTV_INIT_INFO"

    invoke-virtual {v0, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v2, p0, Lcom/sec/android/allshare/TVControllerImpl;->a:Lcom/sec/android/allshare/e;

    invoke-interface {v1, v0, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    goto :goto_0
.end method

.method static synthetic e(Lcom/sec/android/allshare/TVControllerImpl;)V
    .locals 0

    .prologue
    .line 730
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->a()V

    return-void
.end method

.method static synthetic f(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$TVInformation;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->i:Lcom/sec/android/allshare/control/TVController$TVInformation;

    return-object v0
.end method


# virtual methods
.method public final browserScrollDown()V
    .locals 4

    .prologue
    .line 2010
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2013
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->a()V

    .line 2015
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 2017
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onBrowserScrollDownResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    .line 2060
    :cond_1
    :goto_0
    return-void

    .line 2023
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->h:Z

    if-nez v0, :cond_3

    .line 2025
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 2027
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->INVALID_DEVICE:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onBrowserScrollDownResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 2033
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2035
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 2037
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->FEATURE_NOT_SUPPORTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onBrowserScrollDownResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 2052
    :cond_4
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 2053
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2055
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/TVControllerImpl;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2056
    const-string v2, "com.sec.android.allshare.action.ACTION_CONTROL_TV_BROWSER_SCROLLDOWN"

    invoke-virtual {v0, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 2057
    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 2059
    iget-object v1, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v2, p0, Lcom/sec/android/allshare/TVControllerImpl;->a:Lcom/sec/android/allshare/e;

    invoke-interface {v1, v0, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    goto :goto_0
.end method

.method public final browserScrollUp()V
    .locals 4

    .prologue
    .line 2065
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2068
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->a()V

    .line 2070
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 2072
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onBrowserScrollUpResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    .line 2115
    :cond_1
    :goto_0
    return-void

    .line 2078
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->h:Z

    if-nez v0, :cond_3

    .line 2080
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 2082
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->INVALID_DEVICE:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onBrowserScrollUpResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 2088
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2090
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 2092
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->FEATURE_NOT_SUPPORTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onBrowserScrollUpResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 2107
    :cond_4
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 2108
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2110
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/TVControllerImpl;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2111
    const-string v2, "com.sec.android.allshare.action.ACTION_CONTROL_TV_BROWSER_SCROLLUP"

    invoke-virtual {v0, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 2112
    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 2114
    iget-object v1, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v2, p0, Lcom/sec/android/allshare/TVControllerImpl;->a:Lcom/sec/android/allshare/e;

    invoke-interface {v1, v0, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    goto :goto_0
.end method

.method public final browserZoomDefault()V
    .locals 4

    .prologue
    .line 1755
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1758
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->a()V

    .line 1760
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1762
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onBrowserZoomDefaultResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    .line 1796
    :cond_1
    :goto_0
    return-void

    .line 1768
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->h:Z

    if-nez v0, :cond_3

    .line 1770
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1772
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->INVALID_DEVICE:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onBrowserZoomDefaultResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 1778
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1780
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1782
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->FEATURE_NOT_SUPPORTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onBrowserZoomDefaultResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 1788
    :cond_4
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 1789
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1791
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/TVControllerImpl;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1792
    const-string v2, "com.sec.android.allshare.action.ACTION_CONTROL_TV_REQUESTBROWSER_ZOOMDEFAULT"

    invoke-virtual {v0, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 1793
    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 1795
    iget-object v1, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v2, p0, Lcom/sec/android/allshare/TVControllerImpl;->a:Lcom/sec/android/allshare/e;

    invoke-interface {v1, v0, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    goto :goto_0
.end method

.method public final browserZoomIn()V
    .locals 4

    .prologue
    .line 1662
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1665
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->a()V

    .line 1667
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1669
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onBrowserZoomInResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    .line 1703
    :cond_1
    :goto_0
    return-void

    .line 1675
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->h:Z

    if-nez v0, :cond_3

    .line 1677
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1679
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->INVALID_DEVICE:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onBrowserZoomInResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 1685
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1687
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1689
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->FEATURE_NOT_SUPPORTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onBrowserZoomInResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 1695
    :cond_4
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 1696
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1698
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/TVControllerImpl;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1699
    const-string v2, "com.sec.android.allshare.action.ACTION_CONTROL_TV_REQUESTBROWSER_ZOOMIN"

    invoke-virtual {v0, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 1700
    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 1702
    iget-object v1, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v2, p0, Lcom/sec/android/allshare/TVControllerImpl;->a:Lcom/sec/android/allshare/e;

    invoke-interface {v1, v0, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    goto :goto_0
.end method

.method public final browserZoomOut()V
    .locals 4

    .prologue
    .line 1708
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1711
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->a()V

    .line 1713
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1715
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onBrowserZoomOutResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    .line 1750
    :cond_1
    :goto_0
    return-void

    .line 1721
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->h:Z

    if-nez v0, :cond_3

    .line 1723
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1725
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->INVALID_DEVICE:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onBrowserZoomOutResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 1732
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1734
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1736
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->FEATURE_NOT_SUPPORTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onBrowserZoomOutResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 1742
    :cond_4
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 1743
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1745
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/TVControllerImpl;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1746
    const-string v2, "com.sec.android.allshare.action.ACTION_CONTROL_TV_REQUESTBROWSER_ZOOMOUT"

    invoke-virtual {v0, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 1747
    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 1749
    iget-object v1, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v2, p0, Lcom/sec/android/allshare/TVControllerImpl;->a:Lcom/sec/android/allshare/e;

    invoke-interface {v1, v0, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    goto :goto_0
.end method

.method public final closeWebPage()V
    .locals 4

    .prologue
    .line 1294
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1297
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->a()V

    .line 1299
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1301
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onCloseWebPageResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    .line 1335
    :cond_1
    :goto_0
    return-void

    .line 1307
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->h:Z

    if-nez v0, :cond_3

    .line 1309
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1311
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->INVALID_DEVICE:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onCloseWebPageResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 1317
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1319
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1321
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->FEATURE_NOT_SUPPORTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onCloseWebPageResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 1327
    :cond_4
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 1328
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1330
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/TVControllerImpl;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1331
    const-string v2, "com.sec.android.allshare.action.ACTION_CONTROL_TV_STOP_BROWSER"

    invoke-virtual {v0, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 1332
    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 1334
    iget-object v1, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v2, p0, Lcom/sec/android/allshare/TVControllerImpl;->a:Lcom/sec/android/allshare/e;

    invoke-interface {v1, v0, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    goto :goto_0
.end method

.method public final declared-synchronized connect()V
    .locals 4

    .prologue
    .line 693
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 696
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->a()V

    .line 698
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 700
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onConnectResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 728
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 706
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->f:Lcom/sec/android/allshare/IAppControlAPI;

    if-eqz v0, :cond_1

    .line 708
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->f:Lcom/sec/android/allshare/IAppControlAPI;

    invoke-virtual {v0}, Lcom/sec/android/allshare/IAppControlAPI;->b()Z

    move-result v0

    .line 709
    if-nez v0, :cond_3

    .line 711
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->FAIL:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onConnectResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 693
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 717
    :cond_3
    :try_start_2
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->f:Lcom/sec/android/allshare/IAppControlAPI;

    invoke-virtual {v0, p0}, Lcom/sec/android/allshare/IAppControlAPI;->a(Lcom/sec/android/allshare/IAppControlAPI$IControlEventListener;)V

    .line 718
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->f:Lcom/sec/android/allshare/IAppControlAPI;

    iget-object v1, p0, Lcom/sec/android/allshare/TVControllerImpl;->g:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/allshare/TVControllerImpl;->getIPAdress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/allshare/TVControllerImpl;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/allshare/IAppControlAPI;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 719
    if-nez v0, :cond_1

    .line 721
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->FAIL:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onConnectResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final controlEvent(Lcom/sec/android/allshare/EventSync;)V
    .locals 2

    .prologue
    .line 1218
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->j:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 1220
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->j:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 1222
    if-eqz v0, :cond_0

    .line 1224
    iget v1, p1, Lcom/sec/android/allshare/EventSync;->a:I

    iput v1, v0, Landroid/os/Message;->what:I

    .line 1225
    iget v1, p1, Lcom/sec/android/allshare/EventSync;->b:I

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 1226
    iget v1, p1, Lcom/sec/android/allshare/EventSync;->c:I

    iput v1, v0, Landroid/os/Message;->arg2:I

    .line 1227
    iget-object v1, p1, Lcom/sec/android/allshare/EventSync;->d:Ljava/lang/String;

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1229
    iget-object v1, p0, Lcom/sec/android/allshare/TVControllerImpl;->j:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1232
    :cond_0
    return-void
.end method

.method public final declared-synchronized disconnect()V
    .locals 2

    .prologue
    .line 750
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 752
    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 754
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onDisconnectResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    .line 757
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 783
    :cond_2
    :goto_0
    monitor-exit p0

    return-void

    .line 762
    :cond_3
    :try_start_1
    iget-boolean v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->h:Z

    if-nez v0, :cond_5

    .line 764
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_4

    .line 766
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->INVALID_DEVICE:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onDisconnectResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    .line 769
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 750
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 774
    :cond_5
    :try_start_2
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_2

    .line 776
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->a()V

    .line 778
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onDisconnectResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final getBrowserMode()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1340
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1343
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->a()V

    .line 1345
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1347
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v2, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onGetBrowserModeResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/control/TVController$BrowserMode;Lcom/sec/android/allshare/ERROR;)V

    .line 1381
    :cond_1
    :goto_0
    return-void

    .line 1353
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->h:Z

    if-nez v0, :cond_3

    .line 1355
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1357
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->INVALID_DEVICE:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v2, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onGetBrowserModeResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/control/TVController$BrowserMode;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 1363
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1365
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1367
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->FEATURE_NOT_SUPPORTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v2, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onGetBrowserModeResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/control/TVController$BrowserMode;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 1373
    :cond_4
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 1374
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1376
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/TVControllerImpl;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1377
    const-string v2, "com.sec.android.allshare.action.ACTION_CONTROL_TV_GETBROWSERMODE"

    invoke-virtual {v0, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 1378
    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 1380
    iget-object v1, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v2, p0, Lcom/sec/android/allshare/TVControllerImpl;->a:Lcom/sec/android/allshare/e;

    invoke-interface {v1, v0, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    goto :goto_0
.end method

.method public final getBrowserURL()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1386
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1389
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->a()V

    .line 1391
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1393
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v2, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onGetBrowserURLResponseReceived(Lcom/sec/android/allshare/control/TVController;Ljava/lang/String;Lcom/sec/android/allshare/ERROR;)V

    .line 1427
    :cond_1
    :goto_0
    return-void

    .line 1399
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->h:Z

    if-nez v0, :cond_3

    .line 1401
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1403
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->INVALID_DEVICE:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v2, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onGetBrowserURLResponseReceived(Lcom/sec/android/allshare/control/TVController;Ljava/lang/String;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 1409
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1411
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1413
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->FEATURE_NOT_SUPPORTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v2, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onGetBrowserURLResponseReceived(Lcom/sec/android/allshare/control/TVController;Ljava/lang/String;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 1419
    :cond_4
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 1420
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1422
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/TVControllerImpl;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1423
    const-string v2, "com.sec.android.allshare.action.ACTION_CONTROL_TV_REQUESTCURRENTURL"

    invoke-virtual {v0, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 1424
    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 1426
    iget-object v1, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v2, p0, Lcom/sec/android/allshare/TVControllerImpl;->a:Lcom/sec/android/allshare/e;

    invoke-interface {v1, v0, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    goto :goto_0
.end method

.method public final getDeviceDomain()Lcom/sec/android/allshare/Device$DeviceDomain;
    .locals 1

    .prologue
    .line 675
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->c:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getDeviceDomain()Lcom/sec/android/allshare/Device$DeviceDomain;

    move-result-object v0

    return-object v0
.end method

.method public final getDeviceType()Lcom/sec/android/allshare/Device$DeviceType;
    .locals 1

    .prologue
    .line 629
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->c:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getDeviceType()Lcom/sec/android/allshare/Device$DeviceType;

    move-result-object v0

    return-object v0
.end method

.method public final getID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 663
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->c:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getIPAdress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 635
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->c:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getIPAdress()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getIcon()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 647
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->c:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getIcon()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final getIconList()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 653
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->c:Lcom/sec/android/allshare/j;

    if-nez v0, :cond_0

    .line 655
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 657
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->c:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getIconList()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public final getModelName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 623
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->c:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getModelName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getNIC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 669
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->c:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getNIC()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 641
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->c:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getTVInformation()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1965
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1968
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->a()V

    .line 1970
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1972
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v2, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onGetTVInformationResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/control/TVController$TVInformation;Lcom/sec/android/allshare/ERROR;)V

    .line 2005
    :cond_1
    :goto_0
    return-void

    .line 1978
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->h:Z

    if-nez v0, :cond_3

    .line 1980
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1982
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->INVALID_DEVICE:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v2, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onGetTVInformationResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/control/TVController$TVInformation;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 1989
    :cond_3
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 1990
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1992
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/TVControllerImpl;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1993
    const-string v2, "com.sec.android.allshare.action.ACTION_CONTROL_TV_GET_DTV_INFORMATION"

    invoke-virtual {v0, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 1994
    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 1996
    iget-object v1, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v2, p0, Lcom/sec/android/allshare/TVControllerImpl;->a:Lcom/sec/android/allshare/e;

    invoke-interface {v1, v0, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    goto :goto_0
.end method

.method public final goHomePage()V
    .locals 4

    .prologue
    .line 1432
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1435
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->a()V

    .line 1437
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1439
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onGoHomePageResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    .line 1473
    :cond_1
    :goto_0
    return-void

    .line 1445
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->h:Z

    if-nez v0, :cond_3

    .line 1447
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1449
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->INVALID_DEVICE:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onGoHomePageResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 1455
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1457
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1459
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->FEATURE_NOT_SUPPORTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onGoHomePageResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 1465
    :cond_4
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 1466
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1468
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/TVControllerImpl;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1469
    const-string v2, "com.sec.android.allshare.action.ACTION_CONTROL_TV_GOHOME_BROWSER"

    invoke-virtual {v0, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 1470
    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 1472
    iget-object v1, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v2, p0, Lcom/sec/android/allshare/TVControllerImpl;->a:Lcom/sec/android/allshare/e;

    invoke-interface {v1, v0, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    goto :goto_0
.end method

.method public final goNextWebPage()V
    .locals 4

    .prologue
    .line 1570
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1573
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->a()V

    .line 1575
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1577
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onGoNextPageResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    .line 1611
    :cond_1
    :goto_0
    return-void

    .line 1583
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->h:Z

    if-nez v0, :cond_3

    .line 1585
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1587
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->INVALID_DEVICE:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onGoNextPageResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 1593
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1595
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1597
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->FEATURE_NOT_SUPPORTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onGoNextPageResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 1603
    :cond_4
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 1604
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1606
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/TVControllerImpl;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1607
    const-string v2, "com.sec.android.allshare.action.ACTION_CONTROL_TV_REQUESTBROWSER_NEXTPAGE"

    invoke-virtual {v0, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 1608
    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 1610
    iget-object v1, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v2, p0, Lcom/sec/android/allshare/TVControllerImpl;->a:Lcom/sec/android/allshare/e;

    invoke-interface {v1, v0, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    goto :goto_0
.end method

.method public final goPreviousWebPage()V
    .locals 4

    .prologue
    .line 1616
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1619
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->a()V

    .line 1621
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1623
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onGoPreviousPageResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    .line 1657
    :cond_1
    :goto_0
    return-void

    .line 1629
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->h:Z

    if-nez v0, :cond_3

    .line 1631
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1633
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->INVALID_DEVICE:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onGoPreviousPageResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 1639
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1641
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1643
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->FEATURE_NOT_SUPPORTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onGoPreviousPageResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 1649
    :cond_4
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 1650
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1652
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/TVControllerImpl;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1653
    const-string v2, "com.sec.android.allshare.action.ACTION_CONTROL_TV_REQUESTBROWSER_PREVIOUSPAGE"

    invoke-virtual {v0, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 1654
    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 1656
    iget-object v1, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v2, p0, Lcom/sec/android/allshare/TVControllerImpl;->a:Lcom/sec/android/allshare/e;

    invoke-interface {v1, v0, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    goto :goto_0
.end method

.method public final isConnected()Z
    .locals 1

    .prologue
    .line 788
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 791
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->a()V

    .line 793
    const/4 v0, 0x0

    .line 796
    :goto_0
    return v0

    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->h:Z

    goto :goto_0
.end method

.method public final openWebPage(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1237
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1240
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->a()V

    .line 1242
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1244
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, p1, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onOpenWebPageResponseReceived(Lcom/sec/android/allshare/control/TVController;Ljava/lang/String;Lcom/sec/android/allshare/ERROR;)V

    .line 1289
    :cond_1
    :goto_0
    return-void

    .line 1250
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->h:Z

    if-nez v0, :cond_3

    .line 1252
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1254
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->INVALID_DEVICE:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, p1, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onOpenWebPageResponseReceived(Lcom/sec/android/allshare/control/TVController;Ljava/lang/String;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 1260
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1262
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1264
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->FEATURE_NOT_SUPPORTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, p1, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onOpenWebPageResponseReceived(Lcom/sec/android/allshare/control/TVController;Ljava/lang/String;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 1270
    :cond_4
    if-nez p1, :cond_5

    .line 1272
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1274
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, p1, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onOpenWebPageResponseReceived(Lcom/sec/android/allshare/control/TVController;Ljava/lang/String;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 1280
    :cond_5
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 1281
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1283
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/TVControllerImpl;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1284
    const-string v2, "BUNDLE_STRING_BROWSER_URL"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1286
    const-string v2, "com.sec.android.allshare.action.ACTION_CONTROL_TV_RUN_BROWSER"

    invoke-virtual {v0, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 1287
    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 1288
    iget-object v1, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v2, p0, Lcom/sec/android/allshare/TVControllerImpl;->a:Lcom/sec/android/allshare/e;

    invoke-interface {v1, v0, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    goto :goto_0
.end method

.method public final refreshWebPage()V
    .locals 4

    .prologue
    .line 1478
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1481
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->a()V

    .line 1483
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1485
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onRefreshWebPageResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    .line 1519
    :cond_1
    :goto_0
    return-void

    .line 1491
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->h:Z

    if-nez v0, :cond_3

    .line 1493
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1495
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->INVALID_DEVICE:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onRefreshWebPageResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 1501
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1503
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1505
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->FEATURE_NOT_SUPPORTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onRefreshWebPageResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 1511
    :cond_4
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 1512
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1514
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/TVControllerImpl;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1515
    const-string v2, "com.sec.android.allshare.action.ACTION_CONTROL_TV_REQUESTBROWSER_REFRESH"

    invoke-virtual {v0, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 1516
    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 1518
    iget-object v1, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v2, p0, Lcom/sec/android/allshare/TVControllerImpl;->a:Lcom/sec/android/allshare/e;

    invoke-interface {v1, v0, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    goto :goto_0
.end method

.method public final removeEventHandler()V
    .locals 4

    .prologue
    .line 2241
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    iget-object v2, p0, Lcom/sec/android/allshare/TVControllerImpl;->c:Lcom/sec/android/allshare/j;

    invoke-virtual {v2}, Lcom/sec/android/allshare/j;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/allshare/TVControllerImpl;->k:Lcom/sec/android/allshare/d;

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/android/allshare/IAllShareConnector;->b(Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/allshare/d;)V

    .line 2242
    return-void
.end method

.method public final sendKeyboardEnd()Lcom/sec/android/allshare/ERROR;
    .locals 1

    .prologue
    .line 884
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 887
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->a()V

    .line 889
    sget-object v0, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    .line 900
    :goto_0
    return-object v0

    .line 892
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->h:Z

    if-nez v0, :cond_2

    .line 894
    sget-object v0, Lcom/sec/android/allshare/ERROR;->INVALID_DEVICE:Lcom/sec/android/allshare/ERROR;

    goto :goto_0

    .line 898
    :cond_2
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->f:Lcom/sec/android/allshare/IAppControlAPI;

    invoke-virtual {v0}, Lcom/sec/android/allshare/IAppControlAPI;->e()V

    .line 900
    sget-object v0, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    goto :goto_0
.end method

.method public final sendKeyboardString(Ljava/lang/String;)Lcom/sec/android/allshare/ERROR;
    .locals 1

    .prologue
    .line 858
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 861
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->a()V

    .line 863
    sget-object v0, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    .line 878
    :goto_0
    return-object v0

    .line 866
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->h:Z

    if-nez v0, :cond_2

    .line 868
    sget-object v0, Lcom/sec/android/allshare/ERROR;->INVALID_DEVICE:Lcom/sec/android/allshare/ERROR;

    goto :goto_0

    .line 871
    :cond_2
    if-nez p1, :cond_3

    .line 873
    sget-object v0, Lcom/sec/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/sec/android/allshare/ERROR;

    goto :goto_0

    .line 876
    :cond_3
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->f:Lcom/sec/android/allshare/IAppControlAPI;

    invoke-virtual {v0, p1}, Lcom/sec/android/allshare/IAppControlAPI;->a(Ljava/lang/String;)V

    .line 878
    sget-object v0, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    goto :goto_0
.end method

.method public final sendRemoteKey(Lcom/sec/android/allshare/control/TVController$RemoteKey;)Lcom/sec/android/allshare/ERROR;
    .locals 2

    .prologue
    .line 822
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 825
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->a()V

    .line 827
    sget-object v0, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    .line 852
    :goto_0
    return-object v0

    .line 830
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->h:Z

    if-nez v0, :cond_2

    .line 832
    sget-object v0, Lcom/sec/android/allshare/ERROR;->INVALID_DEVICE:Lcom/sec/android/allshare/ERROR;

    goto :goto_0

    .line 835
    :cond_2
    if-nez p1, :cond_3

    .line 837
    sget-object v0, Lcom/sec/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/sec/android/allshare/ERROR;

    goto :goto_0

    .line 840
    :cond_3
    invoke-virtual {p1}, Lcom/sec/android/allshare/control/TVController$RemoteKey;->toString()Ljava/lang/String;

    move-result-object v0

    .line 841
    const-string v1, "KEY_BLUE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 843
    const-string v0, "KEY_CYAN"

    .line 850
    :cond_4
    :goto_1
    iget-object v1, p0, Lcom/sec/android/allshare/TVControllerImpl;->f:Lcom/sec/android/allshare/IAppControlAPI;

    invoke-virtual {v1, v0}, Lcom/sec/android/allshare/IAppControlAPI;->b(Ljava/lang/String;)V

    .line 852
    sget-object v0, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    goto :goto_0

    .line 845
    :cond_5
    const-string v1, "KEY_DASH"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 847
    const-string v0, "KEY_PLUS100"

    goto :goto_1
.end method

.method public final sendTouchClick()Lcom/sec/android/allshare/ERROR;
    .locals 2

    .prologue
    .line 989
    invoke-static {}, Lcom/sec/android/allshare/AVDUtil;->isAVD()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 990
    sget-object v0, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    .line 1008
    :goto_0
    return-object v0

    .line 993
    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 996
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->a()V

    .line 998
    sget-object v0, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    goto :goto_0

    .line 1001
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->h:Z

    if-nez v0, :cond_3

    .line 1003
    sget-object v0, Lcom/sec/android/allshare/ERROR;->INVALID_DEVICE:Lcom/sec/android/allshare/ERROR;

    goto :goto_0

    .line 1006
    :cond_3
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->f:Lcom/sec/android/allshare/IAppControlAPI;

    sget-object v1, Lcom/sec/android/allshare/control/TVController$RemoteKey;->KEY_ENTER:Lcom/sec/android/allshare/control/TVController$RemoteKey;

    invoke-virtual {v1}, Lcom/sec/android/allshare/control/TVController$RemoteKey;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/IAppControlAPI;->b(Ljava/lang/String;)V

    .line 1008
    sget-object v0, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    goto :goto_0
.end method

.method public final sendTouchDown()Lcom/sec/android/allshare/ERROR;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 909
    invoke-static {}, Lcom/sec/android/allshare/AVDUtil;->isAVD()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 910
    sget-object v0, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    .line 931
    :goto_0
    return-object v0

    .line 913
    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 916
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->a()V

    .line 918
    sget-object v0, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    goto :goto_0

    .line 921
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->h:Z

    if-nez v0, :cond_3

    .line 923
    sget-object v0, Lcom/sec/android/allshare/ERROR;->INVALID_DEVICE:Lcom/sec/android/allshare/ERROR;

    goto :goto_0

    .line 926
    :cond_3
    iput v2, p0, Lcom/sec/android/allshare/TVControllerImpl;->l:I

    .line 927
    iput v2, p0, Lcom/sec/android/allshare/TVControllerImpl;->m:I

    .line 929
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->f:Lcom/sec/android/allshare/IAppControlAPI;

    const/16 v1, 0xb

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/allshare/IAppControlAPI;->a(IIIII)V

    .line 931
    sget-object v0, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    goto :goto_0
.end method

.method public final sendTouchMove(II)Lcom/sec/android/allshare/ERROR;
    .locals 6

    .prologue
    .line 937
    invoke-static {}, Lcom/sec/android/allshare/AVDUtil;->isAVD()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 938
    sget-object v0, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    .line 959
    :goto_0
    return-object v0

    .line 941
    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 944
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->a()V

    .line 946
    sget-object v0, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    goto :goto_0

    .line 949
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->h:Z

    if-nez v0, :cond_3

    .line 951
    sget-object v0, Lcom/sec/android/allshare/ERROR;->INVALID_DEVICE:Lcom/sec/android/allshare/ERROR;

    goto :goto_0

    .line 954
    :cond_3
    iget v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->l:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->l:I

    .line 955
    iget v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->m:I

    add-int/2addr v0, p2

    iput v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->m:I

    .line 957
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->f:Lcom/sec/android/allshare/IAppControlAPI;

    const/16 v1, 0xd

    iget v2, p0, Lcom/sec/android/allshare/TVControllerImpl;->l:I

    iget v3, p0, Lcom/sec/android/allshare/TVControllerImpl;->m:I

    move v4, p1

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/allshare/IAppControlAPI;->a(IIIII)V

    .line 959
    sget-object v0, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    goto :goto_0
.end method

.method public final sendTouchUp()Lcom/sec/android/allshare/ERROR;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 965
    invoke-static {}, Lcom/sec/android/allshare/AVDUtil;->isAVD()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 966
    sget-object v0, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    .line 984
    :goto_0
    return-object v0

    .line 969
    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 972
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->a()V

    .line 974
    sget-object v0, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    goto :goto_0

    .line 977
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->h:Z

    if-nez v0, :cond_3

    .line 979
    sget-object v0, Lcom/sec/android/allshare/ERROR;->INVALID_DEVICE:Lcom/sec/android/allshare/ERROR;

    goto :goto_0

    .line 982
    :cond_3
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->f:Lcom/sec/android/allshare/IAppControlAPI;

    const/16 v1, 0xc

    iget v2, p0, Lcom/sec/android/allshare/TVControllerImpl;->l:I

    iget v3, p0, Lcom/sec/android/allshare/TVControllerImpl;->m:I

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/allshare/IAppControlAPI;->a(IIIII)V

    .line 984
    sget-object v0, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    goto :goto_0
.end method

.method public final setBrowserMode(Lcom/sec/android/allshare/control/TVController$BrowserMode;)V
    .locals 4

    .prologue
    .line 1801
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1804
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->a()V

    .line 1806
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1808
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, p1, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onSetBrowserModeResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/control/TVController$BrowserMode;Lcom/sec/android/allshare/ERROR;)V

    .line 1862
    :cond_1
    :goto_0
    return-void

    .line 1814
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->h:Z

    if-nez v0, :cond_3

    .line 1816
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1818
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->INVALID_DEVICE:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, p1, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onSetBrowserModeResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/control/TVController$BrowserMode;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 1824
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1826
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1828
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->FEATURE_NOT_SUPPORTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, p1, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onSetBrowserModeResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/control/TVController$BrowserMode;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 1834
    :cond_4
    if-nez p1, :cond_5

    .line 1836
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1838
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, p1, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onSetBrowserModeResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/control/TVController$BrowserMode;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 1844
    :cond_5
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 1845
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1847
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/TVControllerImpl;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1848
    const-string v2, "com.sec.android.allshare.action.ACTION_CONTROL_TV_REQUESTBROWSER_MODE"

    invoke-virtual {v0, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 1850
    sget-object v2, Lcom/sec/android/allshare/control/TVController$BrowserMode;->LINK_BROWSING:Lcom/sec/android/allshare/control/TVController$BrowserMode;

    invoke-virtual {p1, v2}, Lcom/sec/android/allshare/control/TVController$BrowserMode;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1852
    const-string v2, "BUNDLE_STRING_BROWSER_INPUT_MODE"

    const-string v3, "SetTabMode"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1859
    :goto_1
    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 1861
    iget-object v1, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v2, p0, Lcom/sec/android/allshare/TVControllerImpl;->a:Lcom/sec/android/allshare/e;

    invoke-interface {v1, v0, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    goto :goto_0

    .line 1856
    :cond_6
    const-string v2, "BUNDLE_STRING_BROWSER_INPUT_MODE"

    const-string v3, "SetPointerMode"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final setEventListener(Lcom/sec/android/allshare/control/TVController$IEventListener;)V
    .locals 0

    .prologue
    .line 681
    iput-object p1, p0, Lcom/sec/android/allshare/TVControllerImpl;->d:Lcom/sec/android/allshare/control/TVController$IEventListener;

    .line 682
    return-void
.end method

.method public final setResponseListener(Lcom/sec/android/allshare/control/TVController$IResponseListener;)V
    .locals 0

    .prologue
    .line 687
    iput-object p1, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    .line 688
    return-void
.end method

.method public final stopWebPageLoading()V
    .locals 4

    .prologue
    .line 1524
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1527
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->a()V

    .line 1529
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1531
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onStopWebPageResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    .line 1565
    :cond_1
    :goto_0
    return-void

    .line 1537
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->h:Z

    if-nez v0, :cond_3

    .line 1539
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1541
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->INVALID_DEVICE:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onStopWebPageResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 1547
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/allshare/TVControllerImpl;->b()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1549
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    if-eqz v0, :cond_1

    .line 1551
    iget-object v0, p0, Lcom/sec/android/allshare/TVControllerImpl;->e:Lcom/sec/android/allshare/control/TVController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->FEATURE_NOT_SUPPORTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onStopWebPageResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 1557
    :cond_4
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 1558
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1560
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/TVControllerImpl;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1561
    const-string v2, "com.sec.android.allshare.action.ACTION_CONTROL_TV_REQUESTBROWSER_STOPPAGE"

    invoke-virtual {v0, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 1562
    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 1564
    iget-object v1, p0, Lcom/sec/android/allshare/TVControllerImpl;->b:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v2, p0, Lcom/sec/android/allshare/TVControllerImpl;->a:Lcom/sec/android/allshare/e;

    invoke-interface {v1, v0, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    goto :goto_0
.end method
