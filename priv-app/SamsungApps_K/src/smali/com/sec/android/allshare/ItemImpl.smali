.class final Lcom/sec/android/allshare/ItemImpl;
.super Lcom/sec/android/allshare/Item;
.source "ProGuard"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static synthetic b:[I


# instance fields
.field private a:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 179
    new-instance v0, Lcom/sec/android/allshare/ai;

    invoke-direct {v0}, Lcom/sec/android/allshare/ai;-><init>()V

    sput-object v0, Lcom/sec/android/allshare/ItemImpl;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 39
    return-void
.end method

.method protected constructor <init>(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/android/allshare/Item;-><init>()V

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/allshare/ItemImpl;->a:Landroid/os/Bundle;

    .line 47
    iput-object p1, p0, Lcom/sec/android/allshare/ItemImpl;->a:Landroid/os/Bundle;

    .line 48
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 171
    invoke-direct {p0}, Lcom/sec/android/allshare/Item;-><init>()V

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/allshare/ItemImpl;->a:Landroid/os/Bundle;

    .line 173
    const-class v0, Landroid/os/Bundle;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    iput-object v0, p0, Lcom/sec/android/allshare/ItemImpl;->a:Landroid/os/Bundle;

    .line 174
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 171
    invoke-direct {p0, p1}, Lcom/sec/android/allshare/ItemImpl;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private static synthetic c()[I
    .locals 3

    .prologue
    .line 39
    sget-object v0, Lcom/sec/android/allshare/ItemImpl;->b:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/allshare/ah;->values()[Lcom/sec/android/allshare/ah;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/allshare/ah;->c:Lcom/sec/android/allshare/ah;

    invoke-virtual {v1}, Lcom/sec/android/allshare/ah;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_3

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/allshare/ah;->a:Lcom/sec/android/allshare/ah;

    invoke-virtual {v1}, Lcom/sec/android/allshare/ah;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_2

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/android/allshare/ah;->d:Lcom/sec/android/allshare/ah;

    invoke-virtual {v1}, Lcom/sec/android/allshare/ah;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1

    :goto_3
    :try_start_3
    sget-object v1, Lcom/sec/android/allshare/ah;->b:Lcom/sec/android/allshare/ah;

    invoke-virtual {v1}, Lcom/sec/android/allshare/ah;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_0

    :goto_4
    sput-object v0, Lcom/sec/android/allshare/ItemImpl;->b:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_4

    :catch_1
    move-exception v1

    goto :goto_3

    :catch_2
    move-exception v1

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_1
.end method


# virtual methods
.method final a()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/allshare/ItemImpl;->a:Landroid/os/Bundle;

    return-object v0
.end method

.method protected final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/allshare/ItemImpl;->a:Landroid/os/Bundle;

    .line 104
    if-nez v0, :cond_1

    .line 105
    const-string v0, ""

    .line 112
    :cond_0
    :goto_0
    return-object v0

    .line 107
    :cond_1
    const-string v1, "BUNDLE_STRING_OBJECT_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 109
    if-nez v0, :cond_0

    .line 110
    const-string v0, ""

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 149
    const/4 v0, 0x0

    return v0
.end method

.method public final getAlbumTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 213
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getArtist()Ljava/lang/String;
    .locals 1

    .prologue
    .line 219
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getContentBuildType()Lcom/sec/android/allshare/Item$ContentBuildType;
    .locals 2

    .prologue
    .line 267
    iget-object v0, p0, Lcom/sec/android/allshare/ItemImpl;->a:Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 268
    sget-object v0, Lcom/sec/android/allshare/Item$ContentBuildType;->UNKNOWN:Lcom/sec/android/allshare/Item$ContentBuildType;

    .line 298
    :goto_0
    return-object v0

    .line 270
    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/ItemImpl;->a:Landroid/os/Bundle;

    const-string v1, "BUNDLE_STRING_ITEM_CONSTRUCTOR_KEY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 272
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 273
    :cond_1
    sget-object v0, Lcom/sec/android/allshare/Item$ContentBuildType;->UNKNOWN:Lcom/sec/android/allshare/Item$ContentBuildType;

    goto :goto_0

    .line 275
    :cond_2
    sget-object v1, Lcom/sec/android/allshare/ah;->d:Lcom/sec/android/allshare/ah;

    .line 278
    :try_start_0
    invoke-static {v0}, Lcom/sec/android/allshare/ah;->valueOf(Ljava/lang/String;)Lcom/sec/android/allshare/ah;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 285
    invoke-static {}, Lcom/sec/android/allshare/ItemImpl;->c()[I

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/android/allshare/ah;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 298
    sget-object v0, Lcom/sec/android/allshare/Item$ContentBuildType;->UNKNOWN:Lcom/sec/android/allshare/Item$ContentBuildType;

    goto :goto_0

    .line 282
    :catch_0
    move-exception v0

    sget-object v0, Lcom/sec/android/allshare/Item$ContentBuildType;->UNKNOWN:Lcom/sec/android/allshare/Item$ContentBuildType;

    goto :goto_0

    .line 288
    :pswitch_0
    sget-object v0, Lcom/sec/android/allshare/Item$ContentBuildType;->LOCAL:Lcom/sec/android/allshare/Item$ContentBuildType;

    goto :goto_0

    .line 290
    :pswitch_1
    sget-object v0, Lcom/sec/android/allshare/Item$ContentBuildType;->PROVIDER:Lcom/sec/android/allshare/Item$ContentBuildType;

    goto :goto_0

    .line 292
    :pswitch_2
    sget-object v0, Lcom/sec/android/allshare/Item$ContentBuildType;->WEB:Lcom/sec/android/allshare/Item$ContentBuildType;

    goto :goto_0

    .line 294
    :pswitch_3
    sget-object v0, Lcom/sec/android/allshare/Item$ContentBuildType;->UNKNOWN:Lcom/sec/android/allshare/Item$ContentBuildType;

    goto :goto_0

    .line 285
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final getDate()Ljava/util/Date;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 68
    iget-object v1, p0, Lcom/sec/android/allshare/ItemImpl;->a:Landroid/os/Bundle;

    if-nez v1, :cond_1

    .line 92
    :cond_0
    :goto_0
    return-object v0

    .line 70
    :cond_1
    iget-object v1, p0, Lcom/sec/android/allshare/ItemImpl;->a:Landroid/os/Bundle;

    const-string v2, "BUNDLE_STRING_ITEM_DATE"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 72
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 77
    :try_start_0
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyy-MM-dd HH:mm:ss"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 78
    invoke-virtual {v2, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v2

    .line 84
    :try_start_1
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyy-MM-dd"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 85
    invoke-virtual {v2, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_1
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_0

    .line 87
    :catch_1
    move-exception v1

    .line 89
    const-string v2, "ItemImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getDate  ParseException: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/allshare/ItemImpl;->a:Landroid/os/Bundle;

    const-string v5, "BUNDLE_STRING_ITEM_TITLE"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final getDuration()J
    .locals 2

    .prologue
    .line 225
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final getExtension()Ljava/lang/String;
    .locals 2

    .prologue
    .line 207
    iget-object v0, p0, Lcom/sec/android/allshare/ItemImpl;->a:Landroid/os/Bundle;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/ItemImpl;->a:Landroid/os/Bundle;

    const-string v1, "BUNDLE_STRING_ITEM_EXTENSION"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getFileSize()J
    .locals 2

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/android/allshare/ItemImpl;->a:Landroid/os/Bundle;

    if-nez v0, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/ItemImpl;->a:Landroid/os/Bundle;

    const-string v1, "BUNDLE_LONG_ITEM_FILE_SIZE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public final getGenre()Ljava/lang/String;
    .locals 1

    .prologue
    .line 231
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getLocation()Landroid/location/Location;
    .locals 1

    .prologue
    .line 237
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getMimetype()Ljava/lang/String;
    .locals 2

    .prologue
    .line 201
    iget-object v0, p0, Lcom/sec/android/allshare/ItemImpl;->a:Landroid/os/Bundle;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/ItemImpl;->a:Landroid/os/Bundle;

    const-string v1, "BUNDLE_STRING_ITEM_MIMETYPE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getResolution()Ljava/lang/String;
    .locals 1

    .prologue
    .line 243
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getSubtitle()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 249
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getThumbnail()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 255
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/allshare/ItemImpl;->a:Landroid/os/Bundle;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/ItemImpl;->a:Landroid/os/Bundle;

    const-string v1, "BUNDLE_STRING_ITEM_TITLE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getType()Lcom/sec/android/allshare/Item$MediaType;
    .locals 1

    .prologue
    .line 118
    sget-object v0, Lcom/sec/android/allshare/Item$MediaType;->ITEM_UNKNOWN:Lcom/sec/android/allshare/Item$MediaType;

    return-object v0
.end method

.method public final getURI()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/allshare/ItemImpl;->a:Landroid/os/Bundle;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    check-cast v0, Landroid/net/Uri;

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/ItemImpl;->a:Landroid/os/Bundle;

    const-string v1, "BUNDLE_PARCELABLE_ITEM_URI"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    goto :goto_0
.end method

.method public final isRootFolder()Z
    .locals 1

    .prologue
    .line 261
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 155
    iget-object v0, p0, Lcom/sec/android/allshare/ItemImpl;->a:Landroid/os/Bundle;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 156
    return-void
.end method
