.class final Lcom/sec/android/allshare/aq;
.super Lcom/sec/android/allshare/screen/ScreenCastManager;
.source "ProGuard"


# static fields
.field private static e:Lcom/sec/android/allshare/aq;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/sec/android/allshare/screen/ScreenCastManager$IScreenCastEventListener;

.field private c:Z

.field private d:Ljava/lang/String;

.field private f:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/allshare/aq;->e:Lcom/sec/android/allshare/aq;

    .line 15
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 88
    invoke-direct {p0}, Lcom/sec/android/allshare/screen/ScreenCastManager;-><init>()V

    .line 19
    iput-object v1, p0, Lcom/sec/android/allshare/aq;->a:Landroid/content/Context;

    .line 21
    iput-object v1, p0, Lcom/sec/android/allshare/aq;->b:Lcom/sec/android/allshare/screen/ScreenCastManager$IScreenCastEventListener;

    .line 23
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/allshare/aq;->c:Z

    .line 25
    iput-object v1, p0, Lcom/sec/android/allshare/aq;->d:Ljava/lang/String;

    .line 32
    new-instance v0, Lcom/sec/android/allshare/ar;

    invoke-direct {v0, p0}, Lcom/sec/android/allshare/ar;-><init>(Lcom/sec/android/allshare/aq;)V

    iput-object v0, p0, Lcom/sec/android/allshare/aq;->f:Landroid/content/BroadcastReceiver;

    .line 90
    iput-object p1, p0, Lcom/sec/android/allshare/aq;->a:Landroid/content/Context;

    .line 92
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 93
    const-string v1, "com.sec.android.allshare.intent.action.CAST_GETSTATE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 94
    const-string v1, "android.net.wifi.p2p.STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 95
    const-string v1, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 97
    iget-object v1, p0, Lcom/sec/android/allshare/aq;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/allshare/aq;->f:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 98
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/sec/android/allshare/IAllShareConnector;)Lcom/sec/android/allshare/aq;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 103
    if-nez p1, :cond_1

    .line 105
    const-string v1, "ScreenCastManagerImpl"

    const-string v2, "Connection FAIL: AllShare Service Connector does not exist"

    invoke-static {v1, v2}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    :cond_0
    :goto_0
    return-object v0

    .line 109
    :cond_1
    if-nez p0, :cond_2

    .line 111
    const-string v1, "ScreenCastManagerImpl"

    const-string v2, "Input Context value is NULL"

    invoke-static {v1, v2}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 115
    :cond_2
    invoke-static {p0}, Lcom/sec/android/allshare/aq;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 120
    sget-object v0, Lcom/sec/android/allshare/aq;->e:Lcom/sec/android/allshare/aq;

    if-nez v0, :cond_3

    .line 121
    new-instance v0, Lcom/sec/android/allshare/aq;

    invoke-direct {v0, p0}, Lcom/sec/android/allshare/aq;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/allshare/aq;->e:Lcom/sec/android/allshare/aq;

    .line 123
    :cond_3
    sget-object v0, Lcom/sec/android/allshare/aq;->e:Lcom/sec/android/allshare/aq;

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/allshare/aq;)Lcom/sec/android/allshare/screen/ScreenCastManager$IScreenCastEventListener;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/allshare/aq;->b:Lcom/sec/android/allshare/screen/ScreenCastManager$IScreenCastEventListener;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/allshare/aq;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/sec/android/allshare/aq;->d:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/allshare/aq;Z)V
    .locals 0

    .prologue
    .line 23
    iput-boolean p1, p0, Lcom/sec/android/allshare/aq;->c:Z

    return-void
.end method

.method private static a(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 130
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 131
    const-string v2, "com.sec.android.app.wfdbroker"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 133
    iget v1, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    .line 135
    const/16 v2, 0x64

    if-lt v1, v2, :cond_0

    .line 136
    const-string v1, "ScreenCastManagerImpl"

    const-string v2, "Screen Share supporting device !!!"

    invoke-static {v1, v2}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    const/4 v0, 0x1

    .line 147
    :goto_0
    return v0

    .line 140
    :cond_0
    const-string v1, "ScreenCastManagerImpl"

    const-string v2, "Screen Share NOT supporting device !!!"

    invoke-static {v1, v2}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 146
    :catch_0
    move-exception v1

    const-string v1, "ScreenCastManagerImpl"

    const-string v2, "Screen Share NOT supporting device !!!"

    invoke-static {v1, v2}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic b()Lcom/sec/android/allshare/aq;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/sec/android/allshare/aq;->e:Lcom/sec/android/allshare/aq;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 238
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/allshare/aq;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/allshare/aq;->f:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 244
    :goto_0
    iput-object v2, p0, Lcom/sec/android/allshare/aq;->b:Lcom/sec/android/allshare/screen/ScreenCastManager$IScreenCastEventListener;

    .line 245
    iput-object v2, p0, Lcom/sec/android/allshare/aq;->f:Landroid/content/BroadcastReceiver;

    .line 246
    sput-object v2, Lcom/sec/android/allshare/aq;->e:Lcom/sec/android/allshare/aq;

    .line 247
    iput-object v2, p0, Lcom/sec/android/allshare/aq;->a:Landroid/content/Context;

    .line 248
    iput-object v2, p0, Lcom/sec/android/allshare/aq;->d:Ljava/lang/String;

    .line 249
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/allshare/aq;->c:Z

    .line 251
    const-string v0, "ScreenCastManagerImpl"

    const-string v1, "destroyInstance is called !!!!!"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    return-void

    .line 241
    :catch_0
    move-exception v0

    const-string v0, "ScreenCastManagerImpl"

    const-string v1, "unregisterReceiver Failed !!!!!"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final activateManagerUI()V
    .locals 3

    .prologue
    .line 160
    iget-object v0, p0, Lcom/sec/android/allshare/aq;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 162
    const-string v0, "ScreenCastManagerImpl"

    const-string v1, "Context is NULL !!!!!"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    :goto_0
    return-void

    .line 166
    :cond_0
    const-string v0, "com.sec.android.allshare.intent.action.CAST_START"

    .line 168
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 169
    const/high16 v2, 0x10020000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 170
    const/high16 v2, 0x4000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 171
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 174
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/allshare/aq;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 177
    :catch_0
    move-exception v0

    const-string v0, "ScreenCastManagerImpl"

    const-string v1, "activateManagerUI Failed !!!!!"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final setScreenCastEventListener(Lcom/sec/android/allshare/screen/ScreenCastManager$IScreenCastEventListener;)V
    .locals 2

    .prologue
    .line 218
    iget-object v0, p0, Lcom/sec/android/allshare/aq;->a:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 220
    const-string v0, "ScreenCastManagerImpl"

    const-string v1, "Context is NULL !!!!!"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    :cond_0
    :goto_0
    return-void

    .line 224
    :cond_1
    iput-object p1, p0, Lcom/sec/android/allshare/aq;->b:Lcom/sec/android/allshare/screen/ScreenCastManager$IScreenCastEventListener;

    .line 226
    iget-object v0, p0, Lcom/sec/android/allshare/aq;->b:Lcom/sec/android/allshare/screen/ScreenCastManager$IScreenCastEventListener;

    if-eqz v0, :cond_0

    .line 228
    iget-boolean v0, p0, Lcom/sec/android/allshare/aq;->c:Z

    if-eqz v0, :cond_2

    .line 229
    iget-object v0, p0, Lcom/sec/android/allshare/aq;->b:Lcom/sec/android/allshare/screen/ScreenCastManager$IScreenCastEventListener;

    sget-object v1, Lcom/sec/android/allshare/aq;->e:Lcom/sec/android/allshare/aq;

    invoke-interface {v0, v1}, Lcom/sec/android/allshare/screen/ScreenCastManager$IScreenCastEventListener;->onStarted(Lcom/sec/android/allshare/screen/ScreenCastManager;)V

    goto :goto_0

    .line 231
    :cond_2
    iget-object v0, p0, Lcom/sec/android/allshare/aq;->b:Lcom/sec/android/allshare/screen/ScreenCastManager$IScreenCastEventListener;

    sget-object v1, Lcom/sec/android/allshare/aq;->e:Lcom/sec/android/allshare/aq;

    invoke-interface {v0, v1}, Lcom/sec/android/allshare/screen/ScreenCastManager$IScreenCastEventListener;->onStopped(Lcom/sec/android/allshare/screen/ScreenCastManager;)V

    goto :goto_0
.end method

.method public final stop()V
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sec/android/allshare/aq;->a:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 186
    const-string v0, "ScreenCastManagerImpl"

    const-string v1, "Context is NULL !!!!!"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    :cond_0
    :goto_0
    return-void

    .line 190
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/allshare/aq;->c:Z

    if-nez v0, :cond_2

    .line 192
    const-string v0, "ScreenCastManagerImpl"

    const-string v1, "Screen Share is already Stopped State !!!!!"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    iget-object v0, p0, Lcom/sec/android/allshare/aq;->b:Lcom/sec/android/allshare/screen/ScreenCastManager$IScreenCastEventListener;

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/sec/android/allshare/aq;->b:Lcom/sec/android/allshare/screen/ScreenCastManager$IScreenCastEventListener;

    sget-object v1, Lcom/sec/android/allshare/aq;->e:Lcom/sec/android/allshare/aq;

    invoke-interface {v0, v1}, Lcom/sec/android/allshare/screen/ScreenCastManager$IScreenCastEventListener;->onStopped(Lcom/sec/android/allshare/screen/ScreenCastManager;)V

    goto :goto_0

    .line 200
    :cond_2
    const-string v0, "com.sec.android.allshare.intent.action.CAST_STOP"

    .line 202
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 203
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 206
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/allshare/aq;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 209
    :catch_0
    move-exception v0

    const-string v0, "ScreenCastManagerImpl"

    const-string v1, "Stop Screen Cast Failed !!!!!"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
