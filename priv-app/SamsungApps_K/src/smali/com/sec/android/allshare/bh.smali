.class final Lcom/sec/android/allshare/bh;
.super Lcom/sec/android/allshare/d;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/allshare/bg;


# direct methods
.method constructor <init>(Lcom/sec/android/allshare/bg;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/allshare/bh;->a:Lcom/sec/android/allshare/bg;

    .line 578
    invoke-direct {p0, p2}, Lcom/sec/android/allshare/d;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 5

    .prologue
    .line 583
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 584
    if-nez v0, :cond_1

    .line 602
    :cond_0
    :goto_0
    return-void

    .line 587
    :cond_1
    const-string v1, "BUNDLE_STRING_MAIN_TV_EVENT_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 588
    const-string v2, "BUNDLE_STRING_MAIN_TV_EVENT_XML"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 589
    const-string v2, "AllShareEventHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[TVControl] mAllShareEvent : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 591
    new-instance v1, Lcom/sec/android/allshare/bk;

    iget-object v2, p0, Lcom/sec/android/allshare/bh;->a:Lcom/sec/android/allshare/bg;

    invoke-direct {v1, v2}, Lcom/sec/android/allshare/bk;-><init>(Lcom/sec/android/allshare/bg;)V

    .line 592
    invoke-virtual {v1, v0}, Lcom/sec/android/allshare/bk;->a(Ljava/lang/String;)V

    .line 594
    invoke-virtual {v1}, Lcom/sec/android/allshare/bk;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PowerOFF"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 596
    iget-object v0, p0, Lcom/sec/android/allshare/bh;->a:Lcom/sec/android/allshare/bg;

    invoke-static {v0}, Lcom/sec/android/allshare/bg;->a(Lcom/sec/android/allshare/bg;)Lcom/sec/android/allshare/media/ViewController$IEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 598
    iget-object v0, p0, Lcom/sec/android/allshare/bh;->a:Lcom/sec/android/allshare/bg;

    invoke-virtual {v0}, Lcom/sec/android/allshare/bg;->disconnect()V

    .line 599
    iget-object v0, p0, Lcom/sec/android/allshare/bh;->a:Lcom/sec/android/allshare/bg;

    invoke-static {v0}, Lcom/sec/android/allshare/bg;->a(Lcom/sec/android/allshare/bg;)Lcom/sec/android/allshare/media/ViewController$IEventListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/allshare/bh;->a:Lcom/sec/android/allshare/bg;

    sget-object v2, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/media/ViewController$IEventListener;->onDisconnected(Lcom/sec/android/allshare/media/ViewController;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0
.end method
