.class final Lcom/sec/android/allshare/bc;
.super Ljava/lang/Thread;
.source "ProGuard"


# instance fields
.field a:Ljava/net/Socket;

.field b:Ljava/nio/ByteBuffer;

.field c:Ljava/nio/channels/ReadableByteChannel;

.field d:[B

.field e:Ljava/nio/CharBuffer;

.field f:Ljava/lang/String;

.field g:I

.field public h:Z

.field i:Lcom/sec/android/allshare/bd;

.field private j:Lcom/sec/android/allshare/IAppControlAPI$IControlEventListener;


# direct methods
.method public constructor <init>(Lcom/sec/android/allshare/bd;)V
    .locals 4

    .prologue
    const/16 v3, 0x200

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 552
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 524
    iput-object v1, p0, Lcom/sec/android/allshare/bc;->a:Ljava/net/Socket;

    .line 528
    new-array v0, v3, [B

    iput-object v0, p0, Lcom/sec/android/allshare/bc;->d:[B

    .line 531
    const-string v0, "live_camera.jpg"

    iput-object v0, p0, Lcom/sec/android/allshare/bc;->f:Ljava/lang/String;

    .line 532
    iput v2, p0, Lcom/sec/android/allshare/bc;->g:I

    .line 533
    iput-boolean v2, p0, Lcom/sec/android/allshare/bc;->h:Z

    .line 534
    iput-object v1, p0, Lcom/sec/android/allshare/bc;->i:Lcom/sec/android/allshare/bd;

    .line 540
    iput-object v1, p0, Lcom/sec/android/allshare/bc;->j:Lcom/sec/android/allshare/IAppControlAPI$IControlEventListener;

    .line 555
    const/16 v0, 0x800

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/allshare/bc;->b:Ljava/nio/ByteBuffer;

    .line 556
    invoke-static {v3}, Ljava/nio/CharBuffer;->allocate(I)Ljava/nio/CharBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/allshare/bc;->e:Ljava/nio/CharBuffer;

    .line 560
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/allshare/bc;->g:I

    .line 562
    iput-object p1, p0, Lcom/sec/android/allshare/bc;->i:Lcom/sec/android/allshare/bd;

    .line 563
    iget-object v0, p1, Lcom/sec/android/allshare/bd;->a:Ljava/net/Socket;

    iput-object v0, p0, Lcom/sec/android/allshare/bc;->a:Ljava/net/Socket;

    .line 564
    return-void
.end method

.method private a(III)V
    .locals 2

    .prologue
    .line 573
    new-instance v0, Lcom/sec/android/allshare/EventSync;

    invoke-direct {v0}, Lcom/sec/android/allshare/EventSync;-><init>()V

    .line 575
    iput p1, v0, Lcom/sec/android/allshare/EventSync;->a:I

    .line 576
    iput p2, v0, Lcom/sec/android/allshare/EventSync;->b:I

    .line 577
    iput p3, v0, Lcom/sec/android/allshare/EventSync;->c:I

    .line 584
    iget-object v1, p0, Lcom/sec/android/allshare/bc;->j:Lcom/sec/android/allshare/IAppControlAPI$IControlEventListener;

    if-eqz v1, :cond_0

    .line 586
    iget-object v1, p0, Lcom/sec/android/allshare/bc;->j:Lcom/sec/android/allshare/IAppControlAPI$IControlEventListener;

    invoke-interface {v1, v0}, Lcom/sec/android/allshare/IAppControlAPI$IControlEventListener;->controlEvent(Lcom/sec/android/allshare/EventSync;)V

    .line 595
    :goto_0
    return-void

    .line 590
    :cond_0
    const-string v0, "TVMessageListener"

    const-string v1, "EventListener is null !!!"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 599
    new-instance v0, Lcom/sec/android/allshare/EventSync;

    invoke-direct {v0}, Lcom/sec/android/allshare/EventSync;-><init>()V

    .line 600
    const/4 v1, 0x3

    iput v1, v0, Lcom/sec/android/allshare/EventSync;->a:I

    .line 601
    iput p1, v0, Lcom/sec/android/allshare/EventSync;->b:I

    .line 602
    const/4 v1, 0x0

    iput v1, v0, Lcom/sec/android/allshare/EventSync;->c:I

    .line 603
    iput-object p2, v0, Lcom/sec/android/allshare/EventSync;->d:Ljava/lang/String;

    .line 609
    iget-object v1, p0, Lcom/sec/android/allshare/bc;->j:Lcom/sec/android/allshare/IAppControlAPI$IControlEventListener;

    if-eqz v1, :cond_0

    .line 611
    iget-object v1, p0, Lcom/sec/android/allshare/bc;->j:Lcom/sec/android/allshare/IAppControlAPI$IControlEventListener;

    invoke-interface {v1, v0}, Lcom/sec/android/allshare/IAppControlAPI$IControlEventListener;->controlEvent(Lcom/sec/android/allshare/EventSync;)V

    .line 619
    :goto_0
    return-void

    .line 615
    :cond_0
    const-string v0, "TVMessageListener"

    const-string v1, "EventListener is null !!!"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/sec/android/allshare/IAppControlAPI$IControlEventListener;)V
    .locals 0

    .prologue
    .line 544
    iput-object p1, p0, Lcom/sec/android/allshare/bc;->j:Lcom/sec/android/allshare/IAppControlAPI$IControlEventListener;

    .line 545
    return-void
.end method

.method public final run()V
    .locals 8

    .prologue
    .line 631
    iget-object v0, p0, Lcom/sec/android/allshare/bc;->a:Ljava/net/Socket;

    if-nez v0, :cond_1

    .line 774
    :cond_0
    :goto_0
    return-void

    .line 633
    :cond_1
    iget-object v0, p0, Lcom/sec/android/allshare/bc;->a:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 638
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/allshare/bc;->a:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Ljava/nio/channels/Channels;->newChannel(Ljava/io/InputStream;)Ljava/nio/channels/ReadableByteChannel;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/allshare/bc;->c:Ljava/nio/channels/ReadableByteChannel;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 651
    :goto_1
    :try_start_1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    if-ne v0, p0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/bc;->c:Ljava/nio/channels/ReadableByteChannel;

    iget-object v1, p0, Lcom/sec/android/allshare/bc;->b:Ljava/nio/ByteBuffer;

    invoke-interface {v0, v1}, Ljava/nio/channels/ReadableByteChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 658
    const-string v1, "TVMessageListener"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TVMessageListener data received "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    iget-object v0, p0, Lcom/sec/android/allshare/bc;->b:Ljava/nio/ByteBuffer;

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 667
    iget-object v0, p0, Lcom/sec/android/allshare/bc;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 669
    iget-object v0, p0, Lcom/sec/android/allshare/bc;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    .line 670
    iget-object v1, p0, Lcom/sec/android/allshare/bc;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v1

    .line 672
    const/16 v2, 0x200

    new-array v2, v2, [B

    .line 673
    iget-object v3, p0, Lcom/sec/android/allshare/bc;->b:Ljava/nio/ByteBuffer;

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4, v1}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 674
    new-instance v3, Ljava/lang/String;

    const/4 v4, 0x0

    invoke-direct {v3, v2, v4, v1}, Ljava/lang/String;-><init>([BII)V

    .line 677
    iget-object v3, p0, Lcom/sec/android/allshare/bc;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v3

    .line 678
    iget-object v4, p0, Lcom/sec/android/allshare/bc;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v4

    .line 680
    const-string v5, "TVMessageListener"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "tvStatus :"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 681
    const-string v0, "TVMessageListener"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "targetLen :"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 682
    const-string v0, "TVMessageListener"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "dataLen :"

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 683
    const-string v0, "TVMessageListener"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "protocolId :"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 685
    sparse-switch v4, :sswitch_data_0

    .line 756
    :goto_2
    iget-object v0, p0, Lcom/sec/android/allshare/bc;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 767
    :catch_0
    move-exception v0

    .line 770
    const-string v1, "TVMessageListener"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 641
    :catch_1
    move-exception v0

    .line 644
    const-string v1, "TVMessageListener"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto/16 :goto_1

    .line 688
    :sswitch_0
    :try_start_2
    const-string v0, "TVMessageListener"

    const-string v1, "IAPP_REMOCON"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 689
    iget-object v0, p0, Lcom/sec/android/allshare/bc;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    .line 692
    const-string v1, "TVMessageListener"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "response :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 700
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, v1, v0, v2}, Lcom/sec/android/allshare/bc;->a(III)V

    goto :goto_2

    .line 704
    :sswitch_1
    const-string v0, "TVMessageListener"

    const-string v1, "IAPP_REMOTE_INPUT_TYPE"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 705
    iget-object v0, p0, Lcom/sec/android/allshare/bc;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    .line 706
    const-string v1, "TVMessageListener"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "remoteType :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 707
    const/16 v1, 0xa

    const/4 v2, 0x0

    invoke-direct {p0, v1, v0, v2}, Lcom/sec/android/allshare/bc;->a(III)V

    goto :goto_2

    .line 711
    :sswitch_2
    const-string v0, "TVMessageListener"

    const-string v1, "IAPP_STATUS"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 712
    iget-object v0, p0, Lcom/sec/android/allshare/bc;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    .line 713
    iget-object v1, p0, Lcom/sec/android/allshare/bc;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v1

    .line 714
    const/16 v2, 0xc8

    invoke-direct {p0, v2, v0, v1}, Lcom/sec/android/allshare/bc;->a(III)V

    goto/16 :goto_2

    .line 718
    :sswitch_3
    const-string v0, "TVMessageListener"

    const-string v1, "IAPP_KEYBOARD_SYNC"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 719
    iget-object v0, p0, Lcom/sec/android/allshare/bc;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    .line 720
    const-string v1, "TVMessageListener"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "stringLength :"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 722
    iget-object v1, p0, Lcom/sec/android/allshare/bc;->b:Ljava/nio/ByteBuffer;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 723
    new-instance v1, Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v0}, Ljava/lang/String;-><init>([BII)V

    .line 725
    const-string v2, "AA=="

    invoke-virtual {v1, v2}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 727
    const-string v1, ""

    invoke-direct {p0, v0, v1}, Lcom/sec/android/allshare/bc;->a(ILjava/lang/String;)V

    goto/16 :goto_2

    .line 731
    :cond_2
    const/4 v2, 0x1

    invoke-static {v1, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    .line 734
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>([B)V

    .line 736
    invoke-direct {p0, v0, v2}, Lcom/sec/android/allshare/bc;->a(ILjava/lang/String;)V

    goto/16 :goto_2

    .line 740
    :sswitch_4
    const-string v0, "TVMessageListener"

    const-string v1, "IAPP_EXIT"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 741
    iget-object v0, p0, Lcom/sec/android/allshare/bc;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    .line 743
    const/16 v1, 0x12c

    const/4 v2, 0x0

    invoke-direct {p0, v1, v0, v2}, Lcom/sec/android/allshare/bc;->a(III)V

    goto/16 :goto_2

    .line 747
    :sswitch_5
    const-string v0, "TVMessageListener"

    const-string v1, "IAPP_AUTHENTICATION"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 748
    iget-object v0, p0, Lcom/sec/android/allshare/bc;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    .line 750
    const/16 v1, 0x64

    const/4 v2, 0x0

    invoke-direct {p0, v1, v0, v2}, Lcom/sec/android/allshare/bc;->a(III)V

    goto/16 :goto_2

    .line 754
    :sswitch_6
    const-string v0, "TVMessageListener"

    const-string v1, "IAPP_AUTHENTICATION_TIMEOUT"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 755
    const/16 v0, 0x65

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/allshare/bc;->a(III)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_2

    .line 685
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x3 -> :sswitch_3
        0xa -> :sswitch_1
        0x64 -> :sswitch_5
        0x65 -> :sswitch_6
        0xc8 -> :sswitch_2
        0x12c -> :sswitch_4
    .end sparse-switch
.end method
