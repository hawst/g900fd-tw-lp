.class Lcom/sec/android/allshare/EventTouch;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:I

.field public j:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1763
    new-instance v0, Lcom/sec/android/allshare/m;

    invoke-direct {v0}, Lcom/sec/android/allshare/m;-><init>()V

    sput-object v0, Lcom/sec/android/allshare/EventTouch;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 1726
    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 1726
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 1743
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1750
    iget v0, p0, Lcom/sec/android/allshare/EventTouch;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1751
    iget v0, p0, Lcom/sec/android/allshare/EventTouch;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1752
    iget v0, p0, Lcom/sec/android/allshare/EventTouch;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1753
    iget v0, p0, Lcom/sec/android/allshare/EventTouch;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1754
    iget v0, p0, Lcom/sec/android/allshare/EventTouch;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1755
    iget v0, p0, Lcom/sec/android/allshare/EventTouch;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1756
    iget v0, p0, Lcom/sec/android/allshare/EventTouch;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1757
    iget v0, p0, Lcom/sec/android/allshare/EventTouch;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1758
    iget v0, p0, Lcom/sec/android/allshare/EventTouch;->i:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1760
    iget-object v0, p0, Lcom/sec/android/allshare/EventTouch;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1761
    return-void
.end method
