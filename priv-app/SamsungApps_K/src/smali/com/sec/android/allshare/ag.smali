.class final Lcom/sec/android/allshare/ag;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static synthetic a:[I

.field private static synthetic b:[I


# direct methods
.method static a(Landroid/os/Bundle;)Lcom/sec/android/allshare/Item;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 31
    if-nez p0, :cond_1

    .line 79
    :cond_0
    :goto_0
    return-object v0

    .line 36
    :cond_1
    const-string v1, "BUNDLE_STRING_ITEM_CONSTRUCTOR_KEY"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 38
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 41
    sget-object v2, Lcom/sec/android/allshare/ah;->d:Lcom/sec/android/allshare/ah;

    .line 44
    :try_start_0
    invoke-static {v1}, Lcom/sec/android/allshare/ah;->valueOf(Ljava/lang/String;)Lcom/sec/android/allshare/ah;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 51
    invoke-static {}, Lcom/sec/android/allshare/ag;->a()[I

    move-result-object v2

    invoke-virtual {v1}, Lcom/sec/android/allshare/ah;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 54
    :pswitch_0
    invoke-static {p0}, Lcom/sec/android/allshare/ag;->b(Landroid/os/Bundle;)Lcom/sec/android/allshare/Item;

    move-result-object v0

    goto :goto_0

    .line 58
    :pswitch_1
    const-string v0, "BUNDLE_PARCELABLE_ITEM_URI"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 59
    const-string v1, "BUNDLE_STRING_ITEM_MIMETYPE"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 60
    new-instance v2, Lcom/sec/android/allshare/Item$WebContentBuilder;

    invoke-direct {v2, v0, v1}, Lcom/sec/android/allshare/Item$WebContentBuilder;-><init>(Landroid/net/Uri;Ljava/lang/String;)V

    .line 61
    const-string v0, "BUNDLE_STRING_ITEM_TITLE"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/android/allshare/Item$WebContentBuilder;->setTitle(Ljava/lang/String;)Lcom/sec/android/allshare/Item$WebContentBuilder;

    move-result-object v0

    .line 63
    invoke-virtual {v0}, Lcom/sec/android/allshare/Item$WebContentBuilder;->build()Lcom/sec/android/allshare/Item;

    move-result-object v0

    goto :goto_0

    .line 68
    :pswitch_2
    const-string v0, "BUNDLE_STRING_FILEPATH"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 69
    const-string v1, "BUNDLE_STRING_ITEM_MIMETYPE"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 70
    new-instance v2, Lcom/sec/android/allshare/Item$LocalContentBuilder;

    invoke-direct {v2, v0, v1}, Lcom/sec/android/allshare/Item$LocalContentBuilder;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    const-string v0, "BUNDLE_STRING_ITEM_TITLE"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/sec/android/allshare/Item$LocalContentBuilder;->setTitle(Ljava/lang/String;)Lcom/sec/android/allshare/Item$LocalContentBuilder;

    move-result-object v0

    .line 72
    invoke-virtual {v0}, Lcom/sec/android/allshare/Item$LocalContentBuilder;->build()Lcom/sec/android/allshare/Item;

    move-result-object v0

    goto :goto_0

    .line 48
    :catch_0
    move-exception v1

    goto :goto_0

    .line 51
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static synthetic a()[I
    .locals 3

    .prologue
    .line 21
    sget-object v0, Lcom/sec/android/allshare/ag;->a:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/allshare/ah;->values()[Lcom/sec/android/allshare/ah;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/allshare/ah;->c:Lcom/sec/android/allshare/ah;

    invoke-virtual {v1}, Lcom/sec/android/allshare/ah;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_3

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/allshare/ah;->a:Lcom/sec/android/allshare/ah;

    invoke-virtual {v1}, Lcom/sec/android/allshare/ah;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_2

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/android/allshare/ah;->d:Lcom/sec/android/allshare/ah;

    invoke-virtual {v1}, Lcom/sec/android/allshare/ah;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1

    :goto_3
    :try_start_3
    sget-object v1, Lcom/sec/android/allshare/ah;->b:Lcom/sec/android/allshare/ah;

    invoke-virtual {v1}, Lcom/sec/android/allshare/ah;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_0

    :goto_4
    sput-object v0, Lcom/sec/android/allshare/ag;->a:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_4

    :catch_1
    move-exception v1

    goto :goto_3

    :catch_2
    move-exception v1

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_1
.end method

.method private static b(Landroid/os/Bundle;)Lcom/sec/android/allshare/Item;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 84
    .line 85
    const-string v1, "BUNDLE_STRING_ITEM_TYPE"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 86
    sget-object v2, Lcom/sec/android/allshare/Item$MediaType;->ITEM_UNKNOWN:Lcom/sec/android/allshare/Item$MediaType;

    .line 89
    :try_start_0
    invoke-static {v1}, Lcom/sec/android/allshare/Item$MediaType;->valueOf(Ljava/lang/String;)Lcom/sec/android/allshare/Item$MediaType;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 96
    invoke-static {}, Lcom/sec/android/allshare/ag;->b()[I

    move-result-object v2

    invoke-virtual {v1}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 115
    :goto_0
    :pswitch_0
    return-object v0

    .line 99
    :pswitch_1
    new-instance v0, Lcom/sec/android/allshare/AudioItemImpl;

    invoke-direct {v0, p0}, Lcom/sec/android/allshare/AudioItemImpl;-><init>(Landroid/os/Bundle;)V

    goto :goto_0

    .line 102
    :pswitch_2
    new-instance v0, Lcom/sec/android/allshare/ImageItemImpl;

    invoke-direct {v0, p0}, Lcom/sec/android/allshare/ImageItemImpl;-><init>(Landroid/os/Bundle;)V

    goto :goto_0

    .line 105
    :pswitch_3
    new-instance v0, Lcom/sec/android/allshare/VideoItemImpl;

    invoke-direct {v0, p0}, Lcom/sec/android/allshare/VideoItemImpl;-><init>(Landroid/os/Bundle;)V

    goto :goto_0

    .line 108
    :pswitch_4
    new-instance v0, Lcom/sec/android/allshare/FolderItemImpl;

    invoke-direct {v0, p0}, Lcom/sec/android/allshare/FolderItemImpl;-><init>(Landroid/os/Bundle;)V

    goto :goto_0

    .line 93
    :catch_0
    move-exception v1

    goto :goto_0

    .line 96
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method private static synthetic b()[I
    .locals 3

    .prologue
    .line 21
    sget-object v0, Lcom/sec/android/allshare/ag;->b:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/allshare/Item$MediaType;->values()[Lcom/sec/android/allshare/Item$MediaType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/allshare/Item$MediaType;->ITEM_AUDIO:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/allshare/Item$MediaType;->ITEM_FOLDER:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/android/allshare/Item$MediaType;->ITEM_IMAGE:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    :try_start_3
    sget-object v1, Lcom/sec/android/allshare/Item$MediaType;->ITEM_UNKNOWN:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_4
    :try_start_4
    sget-object v1, Lcom/sec/android/allshare/Item$MediaType;->ITEM_VIDEO:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_5
    sput-object v0, Lcom/sec/android/allshare/ag;->b:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_5

    :catch_1
    move-exception v1

    goto :goto_4

    :catch_2
    move-exception v1

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_1
.end method
