.class Lcom/sec/android/allshare/NetworkSocketInfo;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:I

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1591
    new-instance v0, Lcom/sec/android/allshare/ak;

    invoke-direct {v0}, Lcom/sec/android/allshare/ak;-><init>()V

    sput-object v0, Lcom/sec/android/allshare/NetworkSocketInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 1567
    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 1567
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 1578
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1584
    iget v0, p0, Lcom/sec/android/allshare/NetworkSocketInfo;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1585
    iget-object v0, p0, Lcom/sec/android/allshare/NetworkSocketInfo;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1586
    iget v0, p0, Lcom/sec/android/allshare/NetworkSocketInfo;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1587
    iget-object v0, p0, Lcom/sec/android/allshare/NetworkSocketInfo;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1588
    iget-object v0, p0, Lcom/sec/android/allshare/NetworkSocketInfo;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1589
    return-void
.end method
