.class final Lcom/sec/android/allshare/an;
.super Lcom/sec/android/allshare/media/Receiver;
.source "ProGuard"


# static fields
.field private static synthetic g:[I


# instance fields
.field a:Lcom/sec/android/allshare/d;

.field b:Lcom/sec/android/allshare/e;

.field private c:Lcom/sec/android/allshare/IAllShareConnector;

.field private d:Lcom/sec/android/allshare/j;

.field private e:Lcom/sec/android/allshare/media/Receiver$IProgressUpdateEventListener;

.field private f:Lcom/sec/android/allshare/media/Receiver$IReceiverResponseListener;


# direct methods
.method constructor <init>(Lcom/sec/android/allshare/IAllShareConnector;Lcom/sec/android/allshare/j;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 45
    invoke-direct {p0}, Lcom/sec/android/allshare/media/Receiver;-><init>()V

    .line 40
    iput-object v0, p0, Lcom/sec/android/allshare/an;->c:Lcom/sec/android/allshare/IAllShareConnector;

    .line 41
    iput-object v0, p0, Lcom/sec/android/allshare/an;->d:Lcom/sec/android/allshare/j;

    .line 42
    iput-object v0, p0, Lcom/sec/android/allshare/an;->e:Lcom/sec/android/allshare/media/Receiver$IProgressUpdateEventListener;

    .line 43
    iput-object v0, p0, Lcom/sec/android/allshare/an;->f:Lcom/sec/android/allshare/media/Receiver$IReceiverResponseListener;

    .line 58
    new-instance v0, Lcom/sec/android/allshare/ao;

    invoke-static {}, Lcom/sec/android/allshare/ServiceConnector;->b()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/allshare/ao;-><init>(Lcom/sec/android/allshare/an;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/allshare/an;->a:Lcom/sec/android/allshare/d;

    .line 122
    new-instance v0, Lcom/sec/android/allshare/ap;

    invoke-static {}, Lcom/sec/android/allshare/ServiceConnector;->b()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/allshare/ap;-><init>(Lcom/sec/android/allshare/an;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/allshare/an;->b:Lcom/sec/android/allshare/e;

    .line 47
    if-nez p1, :cond_0

    .line 49
    const-string v0, "ReceiverImpl"

    const-string v1, "Connection FAIL: AllShare Service Connector does not exist"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    :goto_0
    return-void

    .line 53
    :cond_0
    iput-object p2, p0, Lcom/sec/android/allshare/an;->d:Lcom/sec/android/allshare/j;

    .line 54
    iput-object p1, p0, Lcom/sec/android/allshare/an;->c:Lcom/sec/android/allshare/IAllShareConnector;

    .line 55
    iget-object v0, p0, Lcom/sec/android/allshare/an;->c:Lcom/sec/android/allshare/IAllShareConnector;

    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    invoke-virtual {p2}, Lcom/sec/android/allshare/j;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/allshare/an;->a:Lcom/sec/android/allshare/d;

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/android/allshare/IAllShareConnector;->a(Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/allshare/d;)Z

    goto :goto_0
.end method

.method static a(Landroid/os/Bundle;)Lcom/sec/android/allshare/Item;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 174
    if-nez p0, :cond_1

    .line 191
    :cond_0
    :goto_0
    return-object v0

    .line 176
    :cond_1
    const-string v1, "BUNDLE_STRING_ITEM_TYPE"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 177
    if-eqz v1, :cond_0

    .line 180
    invoke-static {v1}, Lcom/sec/android/allshare/Item$MediaType;->valueOf(Ljava/lang/String;)Lcom/sec/android/allshare/Item$MediaType;

    move-result-object v1

    .line 182
    invoke-static {}, Lcom/sec/android/allshare/an;->b()[I

    move-result-object v2

    invoke-virtual {v1}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 185
    :pswitch_0
    new-instance v0, Lcom/sec/android/allshare/AudioItemImpl;

    invoke-direct {v0, p0}, Lcom/sec/android/allshare/AudioItemImpl;-><init>(Landroid/os/Bundle;)V

    goto :goto_0

    .line 187
    :pswitch_1
    new-instance v0, Lcom/sec/android/allshare/ImageItemImpl;

    invoke-direct {v0, p0}, Lcom/sec/android/allshare/ImageItemImpl;-><init>(Landroid/os/Bundle;)V

    goto :goto_0

    .line 189
    :pswitch_2
    new-instance v0, Lcom/sec/android/allshare/VideoItemImpl;

    invoke-direct {v0, p0}, Lcom/sec/android/allshare/VideoItemImpl;-><init>(Landroid/os/Bundle;)V

    goto :goto_0

    .line 182
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic a(Lcom/sec/android/allshare/an;)Lcom/sec/android/allshare/media/Receiver$IProgressUpdateEventListener;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/allshare/an;->e:Lcom/sec/android/allshare/media/Receiver$IProgressUpdateEventListener;

    return-object v0
.end method

.method private a(Lcom/sec/android/allshare/Item;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 246
    iget-object v0, p0, Lcom/sec/android/allshare/an;->c:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/an;->c:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 248
    :cond_0
    const-string v0, "com.sec.android.allshare.action.ACTION_RECEIVER_RECEIVE_BY_ITEM"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 249
    iget-object v0, p0, Lcom/sec/android/allshare/an;->f:Lcom/sec/android/allshare/media/Receiver$IReceiverResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p1, v1}, Lcom/sec/android/allshare/media/Receiver$IReceiverResponseListener;->onReceiveResponseReceived(Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/ERROR;)V

    .line 283
    :goto_0
    return-void

    .line 251
    :cond_1
    iget-object v0, p0, Lcom/sec/android/allshare/an;->f:Lcom/sec/android/allshare/media/Receiver$IReceiverResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p1, v1}, Lcom/sec/android/allshare/media/Receiver$IReceiverResponseListener;->onCancelResponseReceived(Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 255
    :cond_2
    new-instance v1, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v1}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 256
    invoke-virtual {v1, p2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 258
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 259
    instance-of v0, p1, Lcom/sec/android/allshare/AudioItemImpl;

    if-eqz v0, :cond_3

    .line 260
    const-string v0, "BUNDLE_PARCELABLE_ITEM"

    check-cast p1, Lcom/sec/android/allshare/AudioItemImpl;

    invoke-virtual {p1}, Lcom/sec/android/allshare/AudioItemImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 279
    :goto_1
    const-string v3, "BUNDLE_STRING_ID"

    iget-object v0, p0, Lcom/sec/android/allshare/an;->d:Lcom/sec/android/allshare/j;

    if-nez v0, :cond_8

    const-string v0, ""

    :goto_2
    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    invoke-virtual {v1, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 282
    iget-object v0, p0, Lcom/sec/android/allshare/an;->c:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v2, p0, Lcom/sec/android/allshare/an;->b:Lcom/sec/android/allshare/e;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    goto :goto_0

    .line 261
    :cond_3
    instance-of v0, p1, Lcom/sec/android/allshare/VideoItemImpl;

    if-eqz v0, :cond_4

    .line 262
    const-string v0, "BUNDLE_PARCELABLE_ITEM"

    check-cast p1, Lcom/sec/android/allshare/VideoItemImpl;

    invoke-virtual {p1}, Lcom/sec/android/allshare/VideoItemImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_1

    .line 263
    :cond_4
    instance-of v0, p1, Lcom/sec/android/allshare/ImageItemImpl;

    if-eqz v0, :cond_5

    .line 264
    const-string v0, "BUNDLE_PARCELABLE_ITEM"

    check-cast p1, Lcom/sec/android/allshare/ImageItemImpl;

    invoke-virtual {p1}, Lcom/sec/android/allshare/ImageItemImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_1

    .line 265
    :cond_5
    invoke-virtual {p1}, Lcom/sec/android/allshare/Item;->getContentBuildType()Lcom/sec/android/allshare/Item$ContentBuildType;

    move-result-object v0

    sget-object v3, Lcom/sec/android/allshare/Item$ContentBuildType;->LOCAL:Lcom/sec/android/allshare/Item$ContentBuildType;

    invoke-virtual {v0, v3}, Lcom/sec/android/allshare/Item$ContentBuildType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 267
    const-string v0, "BUNDLE_PARCELABLE_ITEM_URI"

    invoke-virtual {p1}, Lcom/sec/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 268
    const-string v0, "BUNDLE_STRING_ITEM_MIMETYPE"

    invoke-virtual {p1}, Lcom/sec/android/allshare/Item;->getMimetype()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    const-string v0, "BUNDLE_PARCELABLE_ITEM"

    check-cast p1, Lcom/sec/android/allshare/iface/IBundleHolder;

    invoke-interface {p1}, Lcom/sec/android/allshare/iface/IBundleHolder;->getBundle()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_1

    .line 273
    :cond_6
    const-string v0, "com.sec.android.allshare.action.ACTION_RECEIVER_RECEIVE_BY_ITEM"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 274
    iget-object v0, p0, Lcom/sec/android/allshare/an;->f:Lcom/sec/android/allshare/media/Receiver$IReceiverResponseListener;

    sget-object v3, Lcom/sec/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p1, v3}, Lcom/sec/android/allshare/media/Receiver$IReceiverResponseListener;->onReceiveResponseReceived(Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_1

    .line 276
    :cond_7
    iget-object v0, p0, Lcom/sec/android/allshare/an;->f:Lcom/sec/android/allshare/media/Receiver$IReceiverResponseListener;

    sget-object v3, Lcom/sec/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p1, v3}, Lcom/sec/android/allshare/media/Receiver$IReceiverResponseListener;->onCancelResponseReceived(Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_1

    .line 279
    :cond_8
    iget-object v0, p0, Lcom/sec/android/allshare/an;->d:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getID()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method static synthetic b(Lcom/sec/android/allshare/an;)Lcom/sec/android/allshare/media/Receiver$IReceiverResponseListener;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/allshare/an;->f:Lcom/sec/android/allshare/media/Receiver$IReceiverResponseListener;

    return-object v0
.end method

.method private static synthetic b()[I
    .locals 3

    .prologue
    .line 36
    sget-object v0, Lcom/sec/android/allshare/an;->g:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/allshare/Item$MediaType;->values()[Lcom/sec/android/allshare/Item$MediaType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/allshare/Item$MediaType;->ITEM_AUDIO:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/allshare/Item$MediaType;->ITEM_FOLDER:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/android/allshare/Item$MediaType;->ITEM_IMAGE:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    :try_start_3
    sget-object v1, Lcom/sec/android/allshare/Item$MediaType;->ITEM_UNKNOWN:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_4
    :try_start_4
    sget-object v1, Lcom/sec/android/allshare/Item$MediaType;->ITEM_VIDEO:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_5
    sput-object v0, Lcom/sec/android/allshare/an;->g:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_5

    :catch_1
    move-exception v1

    goto :goto_4

    :catch_2
    move-exception v1

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 296
    iget-object v0, p0, Lcom/sec/android/allshare/an;->c:Lcom/sec/android/allshare/IAllShareConnector;

    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    iget-object v2, p0, Lcom/sec/android/allshare/an;->d:Lcom/sec/android/allshare/j;

    invoke-virtual {v2}, Lcom/sec/android/allshare/j;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/allshare/an;->a:Lcom/sec/android/allshare/d;

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/android/allshare/IAllShareConnector;->b(Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/allshare/d;)V

    .line 297
    return-void
.end method

.method public final cancel(Lcom/sec/android/allshare/Item;)V
    .locals 1

    .prologue
    .line 219
    const-string v0, "com.sec.android.allshare.action.ACTION_RECEIVER_CANCEL_BY_ITEM"

    invoke-direct {p0, p1, v0}, Lcom/sec/android/allshare/an;->a(Lcom/sec/android/allshare/Item;Ljava/lang/String;)V

    .line 220
    return-void
.end method

.method public final receive(Lcom/sec/android/allshare/Item;)V
    .locals 1

    .prologue
    .line 213
    const-string v0, "com.sec.android.allshare.action.ACTION_RECEIVER_RECEIVE_BY_ITEM"

    invoke-direct {p0, p1, v0}, Lcom/sec/android/allshare/an;->a(Lcom/sec/android/allshare/Item;Ljava/lang/String;)V

    .line 214
    return-void
.end method

.method public final setProgressUpdateEventListener(Lcom/sec/android/allshare/media/Receiver$IProgressUpdateEventListener;)V
    .locals 0

    .prologue
    .line 225
    iput-object p1, p0, Lcom/sec/android/allshare/an;->e:Lcom/sec/android/allshare/media/Receiver$IProgressUpdateEventListener;

    .line 226
    return-void
.end method

.method public final setReceiverResponseListener(Lcom/sec/android/allshare/media/Receiver$IReceiverResponseListener;)V
    .locals 0

    .prologue
    .line 231
    iput-object p1, p0, Lcom/sec/android/allshare/an;->f:Lcom/sec/android/allshare/media/Receiver$IReceiverResponseListener;

    .line 232
    return-void
.end method
