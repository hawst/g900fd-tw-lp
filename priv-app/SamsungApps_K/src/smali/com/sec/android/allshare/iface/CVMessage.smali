.class public Lcom/sec/android/allshare/iface/CVMessage;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final CVM_TYPE_EVENT:I = 0x4

.field public static final CVM_TYPE_REQUEST:I = 0x2

.field public static final CVM_TYPE_RESPONSE:I = 0x3

.field public static final CVM_TYPE_UNDEF:I = 0x1

.field public static final EVT_MSG_KEY:Ljava/lang/String; = "EVT_MSG_KEY"

.field public static final RES_MSG_KEY:Ljava/lang/String; = "RES_MSG_KEY"


# instance fields
.field private a:J

.field private b:I

.field private c:J

.field private d:Ljava/lang/String;

.field private e:Landroid/os/Bundle;

.field private f:Landroid/os/Messenger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 299
    new-instance v0, Lcom/sec/android/allshare/iface/a;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/a;-><init>()V

    sput-object v0, Lcom/sec/android/allshare/iface/CVMessage;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 36
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 59
    const/4 v0, 0x1

    const-string v1, ""

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/allshare/iface/CVMessage;-><init>(ILjava/lang/String;Landroid/os/Bundle;)V

    .line 60
    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 69
    const-string v0, ""

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;-><init>(ILjava/lang/String;Landroid/os/Bundle;)V

    .line 70
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>(ILjava/lang/String;Landroid/os/Bundle;)V

    .line 81
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lcom/sec/android/allshare/iface/CVMessage;->a:J

    .line 92
    iput p1, p0, Lcom/sec/android/allshare/iface/CVMessage;->b:I

    .line 93
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/allshare/iface/CVMessage;->c:J

    .line 94
    iput-object p2, p0, Lcom/sec/android/allshare/iface/CVMessage;->d:Ljava/lang/String;

    .line 95
    iput-object p3, p0, Lcom/sec/android/allshare/iface/CVMessage;->e:Landroid/os/Bundle;

    .line 96
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/allshare/iface/CVMessage;->f:Landroid/os/Messenger;

    .line 97
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 291
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lcom/sec/android/allshare/iface/CVMessage;->a:J

    .line 293
    invoke-virtual {p0, p1}, Lcom/sec/android/allshare/iface/CVMessage;->readFromParcel(Landroid/os/Parcel;)V

    .line 294
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 291
    invoke-direct {p0, p1}, Lcom/sec/android/allshare/iface/CVMessage;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 256
    const/4 v0, 0x0

    return v0
.end method

.method public final getActionID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sec/android/allshare/iface/CVMessage;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final getBundle()Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 235
    iget-object v0, p0, Lcom/sec/android/allshare/iface/CVMessage;->e:Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 236
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/sec/android/allshare/iface/CVMessage;->e:Landroid/os/Bundle;

    .line 237
    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/iface/CVMessage;->e:Landroid/os/Bundle;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 238
    iget-object v0, p0, Lcom/sec/android/allshare/iface/CVMessage;->e:Landroid/os/Bundle;

    return-object v0
.end method

.method public final getEventID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/sec/android/allshare/iface/CVMessage;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final getMessenger()Landroid/os/Messenger;
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/sec/android/allshare/iface/CVMessage;->f:Landroid/os/Messenger;

    return-object v0
.end method

.method public final getMsgID()J
    .locals 2

    .prologue
    .line 214
    iget-wide v0, p0, Lcom/sec/android/allshare/iface/CVMessage;->c:J

    return-wide v0
.end method

.method public final getMsgType()I
    .locals 1

    .prologue
    .line 204
    iget v0, p0, Lcom/sec/android/allshare/iface/CVMessage;->b:I

    return v0
.end method

.method public final getVersion()J
    .locals 2

    .prologue
    .line 174
    iget-wide v0, p0, Lcom/sec/android/allshare/iface/CVMessage;->a:J

    return-wide v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 279
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/allshare/iface/CVMessage;->a:J

    .line 280
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/allshare/iface/CVMessage;->b:I

    .line 281
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/allshare/iface/CVMessage;->c:J

    .line 282
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/allshare/iface/CVMessage;->d:Ljava/lang/String;

    .line 283
    const-class v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readBundle(Ljava/lang/ClassLoader;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/allshare/iface/CVMessage;->e:Landroid/os/Bundle;

    .line 284
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Messenger;

    iput-object v0, p0, Lcom/sec/android/allshare/iface/CVMessage;->f:Landroid/os/Messenger;

    .line 285
    return-void
.end method

.method public setActionID(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/sec/android/allshare/iface/CVMessage;->d:Ljava/lang/String;

    .line 115
    return-void
.end method

.method public setBundle(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 154
    iput-object p1, p0, Lcom/sec/android/allshare/iface/CVMessage;->e:Landroid/os/Bundle;

    .line 155
    return-void
.end method

.method public setEventID(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, Lcom/sec/android/allshare/iface/CVMessage;->d:Ljava/lang/String;

    .line 125
    return-void
.end method

.method public setMessenger(Landroid/os/Messenger;)V
    .locals 0

    .prologue
    .line 164
    iput-object p1, p0, Lcom/sec/android/allshare/iface/CVMessage;->f:Landroid/os/Messenger;

    .line 165
    return-void
.end method

.method public setMsgID(J)V
    .locals 0

    .prologue
    .line 144
    iput-wide p1, p0, Lcom/sec/android/allshare/iface/CVMessage;->c:J

    .line 145
    return-void
.end method

.method public setMsgType(I)V
    .locals 0

    .prologue
    .line 134
    iput p1, p0, Lcom/sec/android/allshare/iface/CVMessage;->b:I

    .line 135
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 266
    iget-wide v0, p0, Lcom/sec/android/allshare/iface/CVMessage;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 267
    iget v0, p0, Lcom/sec/android/allshare/iface/CVMessage;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 268
    iget-wide v0, p0, Lcom/sec/android/allshare/iface/CVMessage;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 269
    iget-object v0, p0, Lcom/sec/android/allshare/iface/CVMessage;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 270
    iget-object v0, p0, Lcom/sec/android/allshare/iface/CVMessage;->e:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 271
    iget-object v0, p0, Lcom/sec/android/allshare/iface/CVMessage;->f:Landroid/os/Messenger;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 272
    return-void
.end method
