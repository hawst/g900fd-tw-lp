.class final Lcom/sec/android/allshare/p;
.super Lcom/sec/android/allshare/d;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/allshare/n;

.field private b:Ljava/util/HashMap;


# direct methods
.method constructor <init>(Lcom/sec/android/allshare/n;Landroid/os/Looper;)V
    .locals 3

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/allshare/p;->a:Lcom/sec/android/allshare/n;

    .line 504
    invoke-direct {p0, p2}, Lcom/sec/android/allshare/d;-><init>(Landroid/os/Looper;)V

    .line 507
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/allshare/p;->b:Ljava/util/HashMap;

    .line 509
    iget-object v0, p0, Lcom/sec/android/allshare/p;->b:Ljava/util/HashMap;

    const-string v1, "com.sec.android.allshare.event.EVENT_FILE_RECEIVER_PROGRESS"

    new-instance v2, Lcom/sec/android/allshare/s;

    invoke-direct {v2, p0}, Lcom/sec/android/allshare/s;-><init>(Lcom/sec/android/allshare/p;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 510
    iget-object v0, p0, Lcom/sec/android/allshare/p;->b:Ljava/util/HashMap;

    const-string v1, "com.sec.android.allshare.event.EVENT_FILE_RECEIVER_COMPLECTED"

    new-instance v2, Lcom/sec/android/allshare/q;

    invoke-direct {v2, p0}, Lcom/sec/android/allshare/q;-><init>(Lcom/sec/android/allshare/p;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 511
    iget-object v0, p0, Lcom/sec/android/allshare/p;->b:Ljava/util/HashMap;

    const-string v1, "com.sec.android.allshare.event.EVENT_FILE_RECEIVER_FAILED"

    new-instance v2, Lcom/sec/android/allshare/r;

    invoke-direct {v2, p0}, Lcom/sec/android/allshare/r;-><init>(Lcom/sec/android/allshare/p;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/allshare/p;)Lcom/sec/android/allshare/n;
    .locals 1

    .prologue
    .line 504
    iget-object v0, p0, Lcom/sec/android/allshare/p;->a:Lcom/sec/android/allshare/n;

    return-object v0
.end method


# virtual methods
.method final a(Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 3

    .prologue
    .line 519
    iget-object v0, p0, Lcom/sec/android/allshare/p;->a:Lcom/sec/android/allshare/n;

    invoke-static {v0}, Lcom/sec/android/allshare/n;->d(Lcom/sec/android/allshare/n;)Lcom/sec/android/allshare/media/FileReceiver$IFileReceiverProgressUpdateEventListener;

    move-result-object v1

    .line 520
    if-nez v1, :cond_1

    .line 528
    :cond_0
    :goto_0
    return-void

    .line 523
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getEventID()Ljava/lang/String;

    move-result-object v0

    .line 525
    iget-object v2, p0, Lcom/sec/android/allshare/p;->b:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/allshare/w;

    .line 526
    if-eqz v0, :cond_0

    .line 527
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/w;->a(Lcom/sec/android/allshare/media/FileReceiver$IFileReceiverProgressUpdateEventListener;Landroid/os/Bundle;)V

    goto :goto_0
.end method
