.class final Lcom/sec/android/allshare/ar;
.super Landroid/content/BroadcastReceiver;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/allshare/aq;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/allshare/aq;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/allshare/ar;->a:Lcom/sec/android/allshare/aq;

    .line 32
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 34
    const-string v0, "com.sec.android.allshare.intent.action.CAST_GETSTATE"

    iput-object v0, p0, Lcom/sec/android/allshare/ar;->b:Ljava/lang/String;

    .line 35
    const-string v0, "com.sec.android.allshare.intent.extra.CAST_STATE"

    iput-object v0, p0, Lcom/sec/android/allshare/ar;->c:Ljava/lang/String;

    .line 36
    const-string v0, "com.sec.android.allshare.intent.extra.CAST_IPADDR"

    iput-object v0, p0, Lcom/sec/android/allshare/ar;->d:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 41
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 43
    iget-object v1, p0, Lcom/sec/android/allshare/ar;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 45
    iget-object v0, p0, Lcom/sec/android/allshare/ar;->c:Ljava/lang/String;

    invoke-virtual {p2, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 48
    if-eqz v0, :cond_1

    .line 49
    iget-object v0, p0, Lcom/sec/android/allshare/ar;->a:Lcom/sec/android/allshare/aq;

    iget-object v1, p0, Lcom/sec/android/allshare/ar;->d:Ljava/lang/String;

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/aq;->a(Lcom/sec/android/allshare/aq;Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lcom/sec/android/allshare/ar;->a:Lcom/sec/android/allshare/aq;

    invoke-static {v0, v3}, Lcom/sec/android/allshare/aq;->a(Lcom/sec/android/allshare/aq;Z)V

    .line 51
    iget-object v0, p0, Lcom/sec/android/allshare/ar;->a:Lcom/sec/android/allshare/aq;

    invoke-static {v0}, Lcom/sec/android/allshare/aq;->a(Lcom/sec/android/allshare/aq;)Lcom/sec/android/allshare/screen/ScreenCastManager$IScreenCastEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/sec/android/allshare/ar;->a:Lcom/sec/android/allshare/aq;

    invoke-static {v0}, Lcom/sec/android/allshare/aq;->a(Lcom/sec/android/allshare/aq;)Lcom/sec/android/allshare/screen/ScreenCastManager$IScreenCastEventListener;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/allshare/aq;->b()Lcom/sec/android/allshare/aq;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/allshare/screen/ScreenCastManager$IScreenCastEventListener;->onStarted(Lcom/sec/android/allshare/screen/ScreenCastManager;)V

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 55
    :cond_1
    iget-object v0, p0, Lcom/sec/android/allshare/ar;->a:Lcom/sec/android/allshare/aq;

    invoke-static {v0, v2}, Lcom/sec/android/allshare/aq;->a(Lcom/sec/android/allshare/aq;Z)V

    .line 56
    iget-object v0, p0, Lcom/sec/android/allshare/ar;->a:Lcom/sec/android/allshare/aq;

    invoke-static {v0, v4}, Lcom/sec/android/allshare/aq;->a(Lcom/sec/android/allshare/aq;Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lcom/sec/android/allshare/ar;->a:Lcom/sec/android/allshare/aq;

    invoke-static {v0}, Lcom/sec/android/allshare/aq;->a(Lcom/sec/android/allshare/aq;)Lcom/sec/android/allshare/screen/ScreenCastManager$IScreenCastEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/sec/android/allshare/ar;->a:Lcom/sec/android/allshare/aq;

    invoke-static {v0}, Lcom/sec/android/allshare/aq;->a(Lcom/sec/android/allshare/aq;)Lcom/sec/android/allshare/screen/ScreenCastManager$IScreenCastEventListener;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/allshare/aq;->b()Lcom/sec/android/allshare/aq;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/allshare/screen/ScreenCastManager$IScreenCastEventListener;->onStopped(Lcom/sec/android/allshare/screen/ScreenCastManager;)V

    goto :goto_0

    .line 62
    :cond_2
    const-string v1, "android.net.wifi.p2p.STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 65
    const-string v0, "wifi_p2p_state"

    invoke-virtual {p2, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 64
    if-ne v0, v3, :cond_0

    .line 69
    iget-object v0, p0, Lcom/sec/android/allshare/ar;->a:Lcom/sec/android/allshare/aq;

    invoke-static {v0, v2}, Lcom/sec/android/allshare/aq;->a(Lcom/sec/android/allshare/aq;Z)V

    .line 70
    iget-object v0, p0, Lcom/sec/android/allshare/ar;->a:Lcom/sec/android/allshare/aq;

    invoke-static {v0, v4}, Lcom/sec/android/allshare/aq;->a(Lcom/sec/android/allshare/aq;Ljava/lang/String;)V

    goto :goto_0

    .line 73
    :cond_3
    const-string v1, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    const-string v0, "networkInfo"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    .line 77
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/sec/android/allshare/ar;->a:Lcom/sec/android/allshare/aq;

    invoke-static {v0, v2}, Lcom/sec/android/allshare/aq;->a(Lcom/sec/android/allshare/aq;Z)V

    .line 80
    iget-object v0, p0, Lcom/sec/android/allshare/ar;->a:Lcom/sec/android/allshare/aq;

    invoke-static {v0, v4}, Lcom/sec/android/allshare/aq;->a(Lcom/sec/android/allshare/aq;Ljava/lang/String;)V

    goto :goto_0
.end method
