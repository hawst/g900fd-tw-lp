.class final Lcom/sec/android/allshare/bg;
.super Lcom/sec/android/allshare/media/ViewController;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/allshare/iface/IBundleHolder;


# instance fields
.field private A:F

.field private B:F

.field private C:F

.field private D:F

.field private E:F

.field private F:F

.field private G:F

.field private H:I

.field private I:Lcom/sec/android/allshare/d;

.field private J:Lcom/sec/android/allshare/IAppControlAPI$IControlEventListener;

.field private K:Landroid/os/Handler;

.field private a:Lcom/sec/android/allshare/IAppControlAPI;

.field private b:Lcom/sec/android/allshare/IAllShareConnector;

.field private c:Lcom/sec/android/allshare/media/ViewController$IEventListener;

.field private d:Lcom/sec/android/allshare/media/ViewController$IResponseListener;

.field private e:Lcom/sec/android/allshare/j;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:F

.field private j:F

.field private k:F

.field private l:F

.field private m:Z

.field private n:F

.field private o:F

.field private p:F

.field private q:I

.field private r:I

.field private s:I

.field private t:I

.field private u:I

.field private v:I

.field private w:F

.field private x:F

.field private y:F

.field private z:F


# direct methods
.method constructor <init>(Lcom/sec/android/allshare/IAllShareConnector;Lcom/sec/android/allshare/j;II)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 90
    invoke-direct {p0}, Lcom/sec/android/allshare/media/ViewController;-><init>()V

    .line 52
    iput-object v2, p0, Lcom/sec/android/allshare/bg;->a:Lcom/sec/android/allshare/IAppControlAPI;

    .line 53
    iput-object v2, p0, Lcom/sec/android/allshare/bg;->b:Lcom/sec/android/allshare/IAllShareConnector;

    .line 54
    iput-object v2, p0, Lcom/sec/android/allshare/bg;->c:Lcom/sec/android/allshare/media/ViewController$IEventListener;

    .line 55
    iput-object v2, p0, Lcom/sec/android/allshare/bg;->d:Lcom/sec/android/allshare/media/ViewController$IResponseListener;

    .line 57
    iput-object v2, p0, Lcom/sec/android/allshare/bg;->e:Lcom/sec/android/allshare/j;

    .line 59
    iput-object v2, p0, Lcom/sec/android/allshare/bg;->f:Ljava/lang/String;

    .line 60
    iput-object v2, p0, Lcom/sec/android/allshare/bg;->g:Ljava/lang/String;

    .line 61
    iput-object v2, p0, Lcom/sec/android/allshare/bg;->h:Ljava/lang/String;

    .line 62
    iput v0, p0, Lcom/sec/android/allshare/bg;->i:F

    .line 63
    iput v0, p0, Lcom/sec/android/allshare/bg;->j:F

    .line 64
    iput v0, p0, Lcom/sec/android/allshare/bg;->k:F

    .line 65
    iput v0, p0, Lcom/sec/android/allshare/bg;->l:F

    .line 66
    iput-boolean v1, p0, Lcom/sec/android/allshare/bg;->m:Z

    .line 68
    iput v3, p0, Lcom/sec/android/allshare/bg;->n:F

    .line 69
    iput v0, p0, Lcom/sec/android/allshare/bg;->o:F

    .line 70
    iput v0, p0, Lcom/sec/android/allshare/bg;->p:F

    .line 71
    iput v1, p0, Lcom/sec/android/allshare/bg;->q:I

    .line 72
    iput v1, p0, Lcom/sec/android/allshare/bg;->r:I

    .line 73
    iput v1, p0, Lcom/sec/android/allshare/bg;->s:I

    .line 74
    iput v1, p0, Lcom/sec/android/allshare/bg;->t:I

    .line 75
    iput v1, p0, Lcom/sec/android/allshare/bg;->u:I

    .line 76
    iput v1, p0, Lcom/sec/android/allshare/bg;->v:I

    .line 77
    iput v0, p0, Lcom/sec/android/allshare/bg;->w:F

    .line 78
    iput v0, p0, Lcom/sec/android/allshare/bg;->x:F

    .line 79
    iput v0, p0, Lcom/sec/android/allshare/bg;->y:F

    .line 80
    iput v0, p0, Lcom/sec/android/allshare/bg;->z:F

    .line 81
    iput v0, p0, Lcom/sec/android/allshare/bg;->A:F

    .line 82
    iput v0, p0, Lcom/sec/android/allshare/bg;->B:F

    .line 83
    iput v0, p0, Lcom/sec/android/allshare/bg;->C:F

    .line 84
    iput v0, p0, Lcom/sec/android/allshare/bg;->D:F

    .line 85
    iput v0, p0, Lcom/sec/android/allshare/bg;->E:F

    .line 86
    iput v3, p0, Lcom/sec/android/allshare/bg;->F:F

    .line 87
    iput v3, p0, Lcom/sec/android/allshare/bg;->G:F

    .line 88
    iput v1, p0, Lcom/sec/android/allshare/bg;->H:I

    .line 578
    new-instance v0, Lcom/sec/android/allshare/bh;

    invoke-static {}, Lcom/sec/android/allshare/ServiceConnector;->b()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/allshare/bh;-><init>(Lcom/sec/android/allshare/bg;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/allshare/bg;->I:Lcom/sec/android/allshare/d;

    .line 605
    new-instance v0, Lcom/sec/android/allshare/bi;

    invoke-direct {v0, p0}, Lcom/sec/android/allshare/bi;-><init>(Lcom/sec/android/allshare/bg;)V

    iput-object v0, p0, Lcom/sec/android/allshare/bg;->J:Lcom/sec/android/allshare/IAppControlAPI$IControlEventListener;

    .line 627
    new-instance v0, Lcom/sec/android/allshare/bj;

    invoke-direct {v0, p0}, Lcom/sec/android/allshare/bj;-><init>(Lcom/sec/android/allshare/bg;)V

    iput-object v0, p0, Lcom/sec/android/allshare/bg;->K:Landroid/os/Handler;

    .line 92
    if-nez p1, :cond_0

    .line 94
    const-string v0, "ViewControllerImpl"

    const-string v1, "Connection FAIL: AllShare Service Connector does not exist"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    :goto_0
    return-void

    .line 98
    :cond_0
    invoke-static {}, Lcom/sec/android/allshare/ServiceConnector;->a()Landroid/content/Context;

    move-result-object v0

    .line 100
    if-eqz v0, :cond_1

    .line 103
    invoke-static {}, Lcom/sec/android/allshare/AVDUtil;->isAVD()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 104
    const-string v0, "00-24-54-92-0A-44"

    iput-object v0, p0, Lcom/sec/android/allshare/bg;->f:Ljava/lang/String;

    .line 111
    :cond_1
    :goto_1
    iput-object p1, p0, Lcom/sec/android/allshare/bg;->b:Lcom/sec/android/allshare/IAllShareConnector;

    .line 113
    new-instance v0, Lcom/sec/android/allshare/IAppControlAPI;

    invoke-direct {v0}, Lcom/sec/android/allshare/IAppControlAPI;-><init>()V

    iput-object v0, p0, Lcom/sec/android/allshare/bg;->a:Lcom/sec/android/allshare/IAppControlAPI;

    .line 115
    iput-object p2, p0, Lcom/sec/android/allshare/bg;->e:Lcom/sec/android/allshare/j;

    .line 116
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->e:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getIPAdress()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/allshare/bg;->g:Ljava/lang/String;

    .line 117
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->e:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/allshare/bg;->h:Ljava/lang/String;

    .line 119
    int-to-float v0, p3

    iput v0, p0, Lcom/sec/android/allshare/bg;->i:F

    .line 120
    int-to-float v0, p4

    iput v0, p0, Lcom/sec/android/allshare/bg;->j:F

    .line 121
    iget v0, p0, Lcom/sec/android/allshare/bg;->i:F

    iget v1, p0, Lcom/sec/android/allshare/bg;->j:F

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/allshare/bg;->A:F

    .line 124
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->b:Lcom/sec/android/allshare/IAllShareConnector;

    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    iget-object v2, p0, Lcom/sec/android/allshare/bg;->e:Lcom/sec/android/allshare/j;

    invoke-virtual {v2}, Lcom/sec/android/allshare/j;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/allshare/bg;->I:Lcom/sec/android/allshare/d;

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/android/allshare/IAllShareConnector;->a(Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/allshare/d;)Z

    goto :goto_0

    .line 106
    :cond_2
    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 107
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/allshare/bg;->f:Ljava/lang/String;

    goto :goto_1
.end method

.method static synthetic a(Lcom/sec/android/allshare/bg;)Lcom/sec/android/allshare/media/ViewController$IEventListener;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->c:Lcom/sec/android/allshare/media/ViewController$IEventListener;

    return-object v0
.end method

.method private a(FF)V
    .locals 2

    .prologue
    .line 294
    iget v0, p0, Lcom/sec/android/allshare/bg;->p:F

    iget v1, p0, Lcom/sec/android/allshare/bg;->o:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    .line 296
    iget v0, p0, Lcom/sec/android/allshare/bg;->o:F

    mul-float/2addr v0, p2

    iput v0, p0, Lcom/sec/android/allshare/bg;->B:F

    .line 297
    iput p2, p0, Lcom/sec/android/allshare/bg;->C:F

    .line 305
    :goto_0
    return-void

    .line 302
    :cond_0
    iput p1, p0, Lcom/sec/android/allshare/bg;->B:F

    .line 303
    iget v0, p0, Lcom/sec/android/allshare/bg;->o:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/sec/android/allshare/bg;->C:F

    goto :goto_0
.end method

.method static synthetic b(Lcom/sec/android/allshare/bg;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 627
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->K:Landroid/os/Handler;

    return-object v0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->a:Lcom/sec/android/allshare/IAppControlAPI;

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->a:Lcom/sec/android/allshare/IAppControlAPI;

    invoke-virtual {v0}, Lcom/sec/android/allshare/IAppControlAPI;->g()V

    .line 175
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->a:Lcom/sec/android/allshare/IAppControlAPI;

    invoke-virtual {v0}, Lcom/sec/android/allshare/IAppControlAPI;->b()Z

    .line 178
    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->c:Lcom/sec/android/allshare/media/ViewController$IEventListener;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/android/allshare/bg;->m:Z

    if-eqz v0, :cond_1

    .line 180
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->c:Lcom/sec/android/allshare/media/ViewController$IEventListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/media/ViewController$IEventListener;->onDisconnected(Lcom/sec/android/allshare/media/ViewController;Lcom/sec/android/allshare/ERROR;)V

    .line 183
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/allshare/bg;->m:Z

    .line 184
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 315
    invoke-static {}, Lcom/sec/android/allshare/ServiceConnector;->a()Landroid/content/Context;

    move-result-object v0

    .line 316
    if-nez v0, :cond_0

    .line 323
    :goto_0
    return-void

    .line 319
    :cond_0
    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 320
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/sec/android/allshare/bg;->D:F

    .line 321
    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/android/allshare/bg;->E:F

    .line 322
    iget v0, p0, Lcom/sec/android/allshare/bg;->D:F

    iget v1, p0, Lcom/sec/android/allshare/bg;->E:F

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/allshare/bg;->p:F

    goto :goto_0
.end method

.method static synthetic c(Lcom/sec/android/allshare/bg;)V
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/allshare/bg;->m:Z

    return-void
.end method

.method static synthetic d(Lcom/sec/android/allshare/bg;)Lcom/sec/android/allshare/media/ViewController$IResponseListener;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->d:Lcom/sec/android/allshare/media/ViewController$IResponseListener;

    return-object v0
.end method

.method private d()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/high16 v3, 0x40000000    # 2.0f

    .line 379
    iget v0, p0, Lcom/sec/android/allshare/bg;->p:F

    iget v1, p0, Lcom/sec/android/allshare/bg;->o:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    .line 381
    iget v0, p0, Lcom/sec/android/allshare/bg;->D:F

    iget v1, p0, Lcom/sec/android/allshare/bg;->k:F

    iget v2, p0, Lcom/sec/android/allshare/bg;->E:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/allshare/bg;->l:F

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    div-float/2addr v0, v3

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/allshare/bg;->q:I

    .line 382
    iput v4, p0, Lcom/sec/android/allshare/bg;->r:I

    .line 389
    :goto_0
    return-void

    .line 386
    :cond_0
    iput v4, p0, Lcom/sec/android/allshare/bg;->q:I

    .line 387
    iget v0, p0, Lcom/sec/android/allshare/bg;->E:F

    iget v1, p0, Lcom/sec/android/allshare/bg;->l:F

    iget v2, p0, Lcom/sec/android/allshare/bg;->D:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/allshare/bg;->k:F

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    div-float/2addr v0, v3

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/allshare/bg;->r:I

    goto :goto_0
.end method

.method static synthetic e(Lcom/sec/android/allshare/bg;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 725
    iput-boolean v2, p0, Lcom/sec/android/allshare/bg;->m:Z

    iget-object v0, p0, Lcom/sec/android/allshare/bg;->a:Lcom/sec/android/allshare/IAppControlAPI;

    invoke-virtual {v0}, Lcom/sec/android/allshare/IAppControlAPI;->c()V

    iget-object v0, p0, Lcom/sec/android/allshare/bg;->c:Lcom/sec/android/allshare/media/ViewController$IEventListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/bg;->c:Lcom/sec/android/allshare/media/ViewController$IEventListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/media/ViewController$IEventListener;->onConnected(Lcom/sec/android/allshare/media/ViewController;Lcom/sec/android/allshare/ERROR;)V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->d:Lcom/sec/android/allshare/media/ViewController$IResponseListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/allshare/bg;->d:Lcom/sec/android/allshare/media/ViewController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/media/ViewController$IResponseListener;->onConnectResponseReceived(Lcom/sec/android/allshare/media/ViewController;Lcom/sec/android/allshare/ERROR;)V

    :cond_1
    iput-boolean v2, p0, Lcom/sec/android/allshare/bg;->m:Z

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 837
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->b:Lcom/sec/android/allshare/IAllShareConnector;

    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    iget-object v2, p0, Lcom/sec/android/allshare/bg;->e:Lcom/sec/android/allshare/j;

    invoke-virtual {v2}, Lcom/sec/android/allshare/j;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/allshare/bg;->I:Lcom/sec/android/allshare/d;

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/android/allshare/IAllShareConnector;->b(Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/allshare/d;)V

    .line 838
    return-void
.end method

.method public final connect()V
    .locals 4

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->b:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/bg;->b:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 140
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/allshare/bg;->b()V

    .line 142
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->d:Lcom/sec/android/allshare/media/ViewController$IResponseListener;

    if-eqz v0, :cond_1

    .line 144
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->d:Lcom/sec/android/allshare/media/ViewController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/media/ViewController$IResponseListener;->onConnectResponseReceived(Lcom/sec/android/allshare/media/ViewController;Lcom/sec/android/allshare/ERROR;)V

    .line 167
    :cond_1
    :goto_0
    return-void

    .line 150
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/allshare/bg;->m:Z

    if-nez v0, :cond_1

    .line 152
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->a:Lcom/sec/android/allshare/IAppControlAPI;

    invoke-virtual {v0}, Lcom/sec/android/allshare/IAppControlAPI;->b()Z

    move-result v0

    .line 153
    if-nez v0, :cond_3

    .line 155
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->d:Lcom/sec/android/allshare/media/ViewController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->FAIL:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/media/ViewController$IResponseListener;->onConnectResponseReceived(Lcom/sec/android/allshare/media/ViewController;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 160
    :cond_3
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->a:Lcom/sec/android/allshare/IAppControlAPI;

    iget-object v1, p0, Lcom/sec/android/allshare/bg;->J:Lcom/sec/android/allshare/IAppControlAPI$IControlEventListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/IAppControlAPI;->a(Lcom/sec/android/allshare/IAppControlAPI$IControlEventListener;)V

    .line 161
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->a:Lcom/sec/android/allshare/IAppControlAPI;

    iget-object v1, p0, Lcom/sec/android/allshare/bg;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/allshare/bg;->g:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/allshare/bg;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/allshare/IAppControlAPI;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 162
    if-nez v0, :cond_1

    .line 164
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->d:Lcom/sec/android/allshare/media/ViewController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->FAIL:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/media/ViewController$IResponseListener;->onConnectResponseReceived(Lcom/sec/android/allshare/media/ViewController;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0
.end method

.method public final disconnect()V
    .locals 2

    .prologue
    .line 189
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->b:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/bg;->b:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->d:Lcom/sec/android/allshare/media/ViewController$IResponseListener;

    if-eqz v0, :cond_1

    .line 193
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->d:Lcom/sec/android/allshare/media/ViewController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/media/ViewController$IResponseListener;->onDisconnectResponseReceived(Lcom/sec/android/allshare/media/ViewController;Lcom/sec/android/allshare/ERROR;)V

    .line 196
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/allshare/bg;->b()V

    .line 218
    :cond_2
    :goto_0
    return-void

    .line 201
    :cond_3
    iget-boolean v0, p0, Lcom/sec/android/allshare/bg;->m:Z

    if-nez v0, :cond_5

    .line 203
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->d:Lcom/sec/android/allshare/media/ViewController$IResponseListener;

    if-eqz v0, :cond_4

    .line 205
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->d:Lcom/sec/android/allshare/media/ViewController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->INVALID_DEVICE:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/media/ViewController$IResponseListener;->onDisconnectResponseReceived(Lcom/sec/android/allshare/media/ViewController;Lcom/sec/android/allshare/ERROR;)V

    .line 208
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/allshare/bg;->b()V

    goto :goto_0

    .line 212
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/allshare/bg;->b()V

    .line 214
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->d:Lcom/sec/android/allshare/media/ViewController$IResponseListener;

    if-eqz v0, :cond_2

    .line 216
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->d:Lcom/sec/android/allshare/media/ViewController$IResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p0, v1}, Lcom/sec/android/allshare/media/ViewController$IResponseListener;->onDisconnectResponseReceived(Lcom/sec/android/allshare/media/ViewController;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0
.end method

.method public final getBundle()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 223
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getViewHeight()I
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->b:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/bg;->b:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 239
    :cond_0
    const/4 v0, -0x1

    .line 241
    :goto_0
    return v0

    :cond_1
    iget v0, p0, Lcom/sec/android/allshare/bg;->j:F

    float-to-int v0, v0

    goto :goto_0
.end method

.method public final getViewWidth()I
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->b:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/bg;->b:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 230
    :cond_0
    const/4 v0, -0x1

    .line 232
    :goto_0
    return v0

    :cond_1
    iget v0, p0, Lcom/sec/android/allshare/bg;->i:F

    float-to-int v0, v0

    goto :goto_0
.end method

.method public final isConnected()Z
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->b:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/bg;->b:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 248
    :cond_0
    const/4 v0, 0x0

    .line 250
    :goto_0
    return v0

    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/allshare/bg;->m:Z

    goto :goto_0
.end method

.method public final move(IIZ)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 539
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->b:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/bg;->b:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 576
    :cond_0
    :goto_0
    return-void

    .line 542
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/allshare/bg;->m:Z

    if-eqz v0, :cond_0

    .line 544
    invoke-static {}, Lcom/sec/android/allshare/ServiceConnector;->a()Landroid/content/Context;

    move-result-object v0

    .line 550
    if-eqz v0, :cond_0

    .line 553
    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 554
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v1

    .line 555
    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    .line 557
    iget v2, p0, Lcom/sec/android/allshare/bg;->i:F

    int-to-float v1, v1

    div-float v1, v2, v1

    .line 558
    iget v2, p0, Lcom/sec/android/allshare/bg;->j:F

    int-to-float v0, v0

    div-float v0, v2, v0

    .line 560
    int-to-float v2, p1

    mul-float/2addr v1, v2

    float-to-int v2, v1

    .line 561
    int-to-float v1, p2

    mul-float/2addr v0, v1

    float-to-int v3, v0

    .line 564
    if-eqz p3, :cond_2

    .line 566
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->a:Lcom/sec/android/allshare/IAppControlAPI;

    invoke-virtual {v0}, Lcom/sec/android/allshare/IAppControlAPI;->c()V

    .line 567
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->a:Lcom/sec/android/allshare/IAppControlAPI;

    const/16 v1, 0xc

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/allshare/IAppControlAPI;->a(IIIII)V

    .line 568
    :cond_2
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->a:Lcom/sec/android/allshare/IAppControlAPI;

    const/16 v1, 0xd

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/allshare/IAppControlAPI;->a(IIIII)V

    goto :goto_0
.end method

.method public final setEventListener(Lcom/sec/android/allshare/media/ViewController$IEventListener;)V
    .locals 0

    .prologue
    .line 256
    iput-object p1, p0, Lcom/sec/android/allshare/bg;->c:Lcom/sec/android/allshare/media/ViewController$IEventListener;

    .line 257
    return-void
.end method

.method public final setResponseListener(Lcom/sec/android/allshare/media/ViewController$IResponseListener;)V
    .locals 0

    .prologue
    .line 843
    iput-object p1, p0, Lcom/sec/android/allshare/bg;->d:Lcom/sec/android/allshare/media/ViewController$IResponseListener;

    .line 844
    return-void
.end method

.method public final setViewAngle(I)V
    .locals 12

    .prologue
    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    .line 262
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->b:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/bg;->b:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 271
    :cond_0
    :goto_0
    return-void

    .line 265
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/allshare/bg;->m:Z

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->a:Lcom/sec/android/allshare/IAppControlAPI;

    const/16 v1, 0x25

    move v5, v4

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/allshare/IAppControlAPI;->a(IDIII)V

    .line 268
    iget-object v5, p0, Lcom/sec/android/allshare/bg;->a:Lcom/sec/android/allshare/IAppControlAPI;

    const/16 v6, 0x27

    const-wide/high16 v7, 0x4059000000000000L    # 100.0

    move v9, p1

    move v10, v4

    move v11, v4

    invoke-virtual/range {v5 .. v11}, Lcom/sec/android/allshare/IAppControlAPI;->a(IDIII)V

    .line 269
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->a:Lcom/sec/android/allshare/IAppControlAPI;

    const/16 v1, 0x26

    move v5, v4

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/allshare/IAppControlAPI;->a(IDIII)V

    goto :goto_0
.end method

.method public final zoom(IIIIII)V
    .locals 7

    .prologue
    .line 394
    const-string v0, "ViewControllerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[zoom] cx : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " cy : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " zoomPercent : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " angle : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->b:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/bg;->b:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 534
    :cond_0
    :goto_0
    return-void

    .line 399
    :cond_1
    iget v0, p0, Lcom/sec/android/allshare/bg;->k:F

    int-to-float v1, p5

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    iget v0, p0, Lcom/sec/android/allshare/bg;->l:F

    int-to-float v1, p6

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_3

    .line 401
    :cond_2
    const-string v0, "ViewControllerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[zoom] mImageWidth : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/allshare/bg;->k:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mImageHeight : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/allshare/bg;->l:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " sourceWidth : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " sourceHeight : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/allshare/bg;->k:F

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/allshare/bg;->l:F

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/allshare/bg;->n:F

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/allshare/bg;->o:F

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/allshare/bg;->p:F

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/allshare/bg;->q:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/allshare/bg;->r:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/allshare/bg;->s:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/allshare/bg;->t:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/allshare/bg;->u:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/allshare/bg;->v:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/allshare/bg;->w:F

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/allshare/bg;->x:F

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/allshare/bg;->y:F

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/allshare/bg;->z:F

    .line 403
    invoke-direct {p0}, Lcom/sec/android/allshare/bg;->c()V

    .line 404
    int-to-float v0, p5

    iput v0, p0, Lcom/sec/android/allshare/bg;->k:F

    int-to-float v0, p6

    iput v0, p0, Lcom/sec/android/allshare/bg;->l:F

    iget v0, p0, Lcom/sec/android/allshare/bg;->k:F

    iget v1, p0, Lcom/sec/android/allshare/bg;->l:F

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/allshare/bg;->o:F

    .line 405
    iget v0, p0, Lcom/sec/android/allshare/bg;->D:F

    iget v1, p0, Lcom/sec/android/allshare/bg;->E:F

    invoke-direct {p0, v0, v1}, Lcom/sec/android/allshare/bg;->a(FF)V

    .line 407
    const-string v0, "ViewControllerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[zoom] mTvWidth : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/allshare/bg;->i:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mTvHeight : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/allshare/bg;->j:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mImageWidth : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/allshare/bg;->k:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mImageHeight : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/allshare/bg;->l:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    iget v0, p0, Lcom/sec/android/allshare/bg;->j:F

    iget v1, p0, Lcom/sec/android/allshare/bg;->l:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_9

    iget v0, p0, Lcom/sec/android/allshare/bg;->i:F

    iget v1, p0, Lcom/sec/android/allshare/bg;->k:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_9

    .line 411
    iget v0, p0, Lcom/sec/android/allshare/bg;->i:F

    iget v1, p0, Lcom/sec/android/allshare/bg;->k:F

    sub-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/allshare/bg;->y:F

    .line 412
    iget v0, p0, Lcom/sec/android/allshare/bg;->j:F

    iget v1, p0, Lcom/sec/android/allshare/bg;->l:F

    sub-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/allshare/bg;->z:F

    .line 425
    :goto_1
    const-string v0, "ViewControllerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[zoom] mTvOrgX0 : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/allshare/bg;->y:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mTvOrgY0 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/allshare/bg;->z:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mTvOrgX : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/allshare/bg;->w:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mTvOrgY : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/allshare/bg;->x:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 427
    iget v0, p0, Lcom/sec/android/allshare/bg;->y:F

    iput v0, p0, Lcom/sec/android/allshare/bg;->w:F

    .line 428
    iget v0, p0, Lcom/sec/android/allshare/bg;->z:F

    iput v0, p0, Lcom/sec/android/allshare/bg;->x:F

    .line 430
    iput p4, p0, Lcom/sec/android/allshare/bg;->H:I

    .line 432
    invoke-direct {p0}, Lcom/sec/android/allshare/bg;->d()V

    .line 433
    iget v0, p0, Lcom/sec/android/allshare/bg;->q:I

    iput v0, p0, Lcom/sec/android/allshare/bg;->u:I

    .line 434
    iget v0, p0, Lcom/sec/android/allshare/bg;->r:I

    iput v0, p0, Lcom/sec/android/allshare/bg;->v:I

    .line 437
    :cond_3
    iget-boolean v0, p0, Lcom/sec/android/allshare/bg;->m:Z

    if-eqz v0, :cond_0

    .line 439
    invoke-direct {p0}, Lcom/sec/android/allshare/bg;->c()V

    .line 441
    const/4 v0, 0x0

    .line 442
    if-eqz p3, :cond_4

    iget v1, p0, Lcom/sec/android/allshare/bg;->H:I

    if-eq v1, p4, :cond_b

    .line 444
    :cond_4
    const/16 v0, 0x64

    if-ge p3, v0, :cond_5

    .line 445
    const/4 p3, 0x0

    .line 447
    :cond_5
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/allshare/bg;->n:F

    .line 448
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/allshare/bg;->G:F

    .line 449
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/allshare/bg;->F:F

    .line 450
    const/4 v0, 0x1

    .line 451
    iget v1, p0, Lcom/sec/android/allshare/bg;->D:F

    iget v2, p0, Lcom/sec/android/allshare/bg;->E:F

    invoke-direct {p0, v1, v2}, Lcom/sec/android/allshare/bg;->a(FF)V

    .line 453
    invoke-direct {p0}, Lcom/sec/android/allshare/bg;->d()V

    .line 454
    iget v1, p0, Lcom/sec/android/allshare/bg;->q:I

    iput v1, p0, Lcom/sec/android/allshare/bg;->u:I

    .line 455
    iget v1, p0, Lcom/sec/android/allshare/bg;->r:I

    iput v1, p0, Lcom/sec/android/allshare/bg;->v:I

    .line 479
    :cond_6
    :goto_2
    iget v1, p0, Lcom/sec/android/allshare/bg;->B:F

    iget v2, p0, Lcom/sec/android/allshare/bg;->n:F

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/sec/android/allshare/bg;->B:F

    iget v1, p0, Lcom/sec/android/allshare/bg;->C:F

    iget v2, p0, Lcom/sec/android/allshare/bg;->n:F

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/sec/android/allshare/bg;->C:F

    .line 481
    const-string v1, "ViewControllerImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[zoom] mMobileWidth : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/sec/android/allshare/bg;->D:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mMobileHeight : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/allshare/bg;->E:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mMobilePhoneRatio : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/allshare/bg;->p:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mRelativeZoomRate : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/allshare/bg;->n:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    iget v1, p0, Lcom/sec/android/allshare/bg;->j:F

    iget v2, p0, Lcom/sec/android/allshare/bg;->l:F

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_e

    iget v1, p0, Lcom/sec/android/allshare/bg;->i:F

    iget v2, p0, Lcom/sec/android/allshare/bg;->k:F

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_e

    iget v1, p0, Lcom/sec/android/allshare/bg;->p:F

    iget v2, p0, Lcom/sec/android/allshare/bg;->o:F

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_d

    iget v1, p0, Lcom/sec/android/allshare/bg;->k:F

    iget v2, p0, Lcom/sec/android/allshare/bg;->E:F

    iget v3, p0, Lcom/sec/android/allshare/bg;->k:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/allshare/bg;->l:F

    div-float/2addr v2, v3

    div-float/2addr v1, v2

    :goto_3
    const-string v2, "ViewControllerImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[zoom] magicRate : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    invoke-direct {p0}, Lcom/sec/android/allshare/bg;->d()V

    .line 487
    iget v2, p0, Lcom/sec/android/allshare/bg;->u:I

    sub-int v2, p1, v2

    iput v2, p0, Lcom/sec/android/allshare/bg;->s:I

    .line 488
    iget v2, p0, Lcom/sec/android/allshare/bg;->v:I

    sub-int v2, p2, v2

    iput v2, p0, Lcom/sec/android/allshare/bg;->t:I

    .line 490
    iget v2, p0, Lcom/sec/android/allshare/bg;->s:I

    if-gez v2, :cond_7

    .line 491
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/android/allshare/bg;->s:I

    .line 493
    :cond_7
    iget v2, p0, Lcom/sec/android/allshare/bg;->t:I

    if-gez v2, :cond_8

    .line 494
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/android/allshare/bg;->t:I

    .line 496
    :cond_8
    const-string v2, "ViewControllerImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[zoom] mMarginX : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/sec/android/allshare/bg;->q:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mMarginY : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/allshare/bg;->r:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mOrgX : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/allshare/bg;->u:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mOrgY : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/allshare/bg;->v:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mOrgCenterX : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/allshare/bg;->s:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mOrgCenterY : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/allshare/bg;->t:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 498
    if-eqz v0, :cond_12

    .line 503
    iget v0, p0, Lcom/sec/android/allshare/bg;->i:F

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    float-to-int v1, v0

    .line 504
    iget v0, p0, Lcom/sec/android/allshare/bg;->j:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v0, v2

    float-to-int v0, v0

    .line 512
    :goto_4
    iget v2, p0, Lcom/sec/android/allshare/bg;->B:F

    iget v3, p0, Lcom/sec/android/allshare/bg;->D:F

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_14

    .line 514
    iget v1, p0, Lcom/sec/android/allshare/bg;->i:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    float-to-int v5, v1

    .line 517
    :goto_5
    iget v1, p0, Lcom/sec/android/allshare/bg;->C:F

    iget v2, p0, Lcom/sec/android/allshare/bg;->E:F

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_13

    .line 518
    iget v0, p0, Lcom/sec/android/allshare/bg;->j:F

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    float-to-int v6, v0

    .line 520
    :goto_6
    const-string v0, "ViewControllerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[zoom] tvCenterX : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " tvCenterY : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mTvOrgX : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/allshare/bg;->w:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mTvOrgY : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/allshare/bg;->x:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mOrgCenterX : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/allshare/bg;->s:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mOrgCenterY : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/allshare/bg;->t:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 522
    iget v0, p0, Lcom/sec/android/allshare/bg;->u:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/android/allshare/bg;->s:I

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/allshare/bg;->n:F

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/allshare/bg;->u:I

    .line 523
    iget v0, p0, Lcom/sec/android/allshare/bg;->v:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/android/allshare/bg;->t:I

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/allshare/bg;->n:F

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/allshare/bg;->v:I

    .line 525
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->a:Lcom/sec/android/allshare/IAppControlAPI;

    const/16 v1, 0x25

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/allshare/IAppControlAPI;->a(IDIII)V

    .line 526
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->a:Lcom/sec/android/allshare/IAppControlAPI;

    const/16 v1, 0x27

    int-to-double v2, p3

    move v4, p4

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/allshare/IAppControlAPI;->a(IDIII)V

    .line 527
    iget-object v0, p0, Lcom/sec/android/allshare/bg;->a:Lcom/sec/android/allshare/IAppControlAPI;

    const/16 v1, 0x26

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/allshare/IAppControlAPI;->a(IDIII)V

    .line 529
    iget v0, p0, Lcom/sec/android/allshare/bg;->G:F

    iget v1, p0, Lcom/sec/android/allshare/bg;->n:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/allshare/bg;->G:F

    .line 531
    const-string v0, "ViewControllerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[zoom] mOrgX : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/allshare/bg;->u:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mOrgY : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/allshare/bg;->v:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mTvOrgX : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/allshare/bg;->w:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mTvOrgY : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/allshare/bg;->x:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 532
    const-string v0, "ViewControllerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[zoom] mPrevAbsZoomRate : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/allshare/bg;->G:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 414
    :cond_9
    iget v0, p0, Lcom/sec/android/allshare/bg;->A:F

    iget v1, p0, Lcom/sec/android/allshare/bg;->o:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_a

    .line 416
    iget v0, p0, Lcom/sec/android/allshare/bg;->i:F

    iget v1, p0, Lcom/sec/android/allshare/bg;->k:F

    iget v2, p0, Lcom/sec/android/allshare/bg;->l:F

    div-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/allshare/bg;->j:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    float-to-int v0, v0

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/android/allshare/bg;->y:F

    .line 417
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/allshare/bg;->z:F

    goto/16 :goto_1

    .line 421
    :cond_a
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/allshare/bg;->y:F

    .line 422
    iget v0, p0, Lcom/sec/android/allshare/bg;->j:F

    iget v1, p0, Lcom/sec/android/allshare/bg;->l:F

    iget v2, p0, Lcom/sec/android/allshare/bg;->k:F

    div-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/allshare/bg;->i:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    float-to-int v0, v0

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/android/allshare/bg;->z:F

    goto/16 :goto_1

    .line 459
    :cond_b
    int-to-float v1, p3

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v1, v2

    iput v1, p0, Lcom/sec/android/allshare/bg;->n:F

    .line 460
    iget v1, p0, Lcom/sec/android/allshare/bg;->F:F

    iget v2, p0, Lcom/sec/android/allshare/bg;->n:F

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/sec/android/allshare/bg;->F:F

    .line 462
    iget v1, p0, Lcom/sec/android/allshare/bg;->F:F

    const/high16 v2, 0x3f800000    # 1.0f

    cmpg-float v1, v1, v2

    if-gez v1, :cond_6

    .line 464
    const/16 v0, 0x64

    if-ge p3, v0, :cond_c

    .line 465
    const/4 p3, 0x0

    .line 467
    :cond_c
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/allshare/bg;->n:F

    .line 468
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/allshare/bg;->G:F

    .line 469
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/allshare/bg;->F:F

    .line 470
    const/4 v0, 0x1

    .line 471
    iget v1, p0, Lcom/sec/android/allshare/bg;->D:F

    iget v2, p0, Lcom/sec/android/allshare/bg;->E:F

    invoke-direct {p0, v1, v2}, Lcom/sec/android/allshare/bg;->a(FF)V

    .line 473
    invoke-direct {p0}, Lcom/sec/android/allshare/bg;->d()V

    .line 474
    iget v1, p0, Lcom/sec/android/allshare/bg;->q:I

    iput v1, p0, Lcom/sec/android/allshare/bg;->u:I

    .line 475
    iget v1, p0, Lcom/sec/android/allshare/bg;->r:I

    iput v1, p0, Lcom/sec/android/allshare/bg;->v:I

    goto/16 :goto_2

    .line 483
    :cond_d
    iget v1, p0, Lcom/sec/android/allshare/bg;->k:F

    iget v2, p0, Lcom/sec/android/allshare/bg;->D:F

    div-float/2addr v1, v2

    goto/16 :goto_3

    :cond_e
    iget v1, p0, Lcom/sec/android/allshare/bg;->A:F

    iget v2, p0, Lcom/sec/android/allshare/bg;->o:F

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_10

    iget v1, p0, Lcom/sec/android/allshare/bg;->p:F

    iget v2, p0, Lcom/sec/android/allshare/bg;->o:F

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_f

    iget v1, p0, Lcom/sec/android/allshare/bg;->j:F

    iget v2, p0, Lcom/sec/android/allshare/bg;->E:F

    div-float/2addr v1, v2

    goto/16 :goto_3

    :cond_f
    iget v1, p0, Lcom/sec/android/allshare/bg;->j:F

    iget v2, p0, Lcom/sec/android/allshare/bg;->k:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/allshare/bg;->l:F

    div-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/allshare/bg;->D:F

    div-float/2addr v1, v2

    goto/16 :goto_3

    :cond_10
    iget v1, p0, Lcom/sec/android/allshare/bg;->p:F

    iget v2, p0, Lcom/sec/android/allshare/bg;->o:F

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_11

    iget v1, p0, Lcom/sec/android/allshare/bg;->i:F

    iget v2, p0, Lcom/sec/android/allshare/bg;->D:F

    div-float/2addr v1, v2

    goto/16 :goto_3

    :cond_11
    iget v1, p0, Lcom/sec/android/allshare/bg;->i:F

    iget v2, p0, Lcom/sec/android/allshare/bg;->l:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/allshare/bg;->k:F

    div-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/allshare/bg;->E:F

    div-float/2addr v1, v2

    goto/16 :goto_3

    .line 508
    :cond_12
    iget v0, p0, Lcom/sec/android/allshare/bg;->s:I

    int-to-float v0, v0

    iget v2, p0, Lcom/sec/android/allshare/bg;->G:F

    div-float/2addr v0, v2

    mul-float/2addr v0, v1

    iget v2, p0, Lcom/sec/android/allshare/bg;->w:F

    add-float/2addr v0, v2

    float-to-int v2, v0

    .line 509
    iget v0, p0, Lcom/sec/android/allshare/bg;->t:I

    int-to-float v0, v0

    iget v3, p0, Lcom/sec/android/allshare/bg;->G:F

    div-float/2addr v0, v3

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/allshare/bg;->x:F

    add-float/2addr v0, v1

    float-to-int v0, v0

    move v1, v2

    goto/16 :goto_4

    :cond_13
    move v6, v0

    goto/16 :goto_6

    :cond_14
    move v5, v1

    goto/16 :goto_5
.end method
