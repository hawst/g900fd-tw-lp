.class final Lcom/sec/android/allshare/t;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/allshare/media/FileReceiver$IFileReceiverProgressUpdateEventListener;


# instance fields
.field final synthetic a:Lcom/sec/android/allshare/n;


# direct methods
.method constructor <init>(Lcom/sec/android/allshare/n;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/allshare/t;->a:Lcom/sec/android/allshare/n;

    .line 597
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCompleted(Lcom/sec/android/allshare/media/FileReceiver;Ljava/lang/String;Ljava/io/File;Lcom/sec/android/allshare/ERROR;)V
    .locals 4

    .prologue
    .line 671
    const-string v0, "FileReceiverImpl"

    const-string v1, "onCompleted()"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 675
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/allshare/t;->a:Lcom/sec/android/allshare/n;

    invoke-static {v0, p2}, Lcom/sec/android/allshare/n;->c(Lcom/sec/android/allshare/n;Ljava/lang/String;)Lcom/sec/android/allshare/x;

    move-result-object v0

    .line 679
    invoke-virtual {v0}, Lcom/sec/android/allshare/x;->b()Lcom/sec/android/allshare/media/FileReceiver$IFileReceiverProgressUpdateEventListener;

    move-result-object v1

    .line 682
    if-eqz v1, :cond_1

    .line 683
    invoke-interface {v1, p1, p2, p3, p4}, Lcom/sec/android/allshare/media/FileReceiver$IFileReceiverProgressUpdateEventListener;->onCompleted(Lcom/sec/android/allshare/media/FileReceiver;Ljava/lang/String;Ljava/io/File;Lcom/sec/android/allshare/ERROR;)V

    .line 688
    :goto_0
    invoke-virtual {v0}, Lcom/sec/android/allshare/x;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 690
    const-string v1, "FileReceiverImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "All of FileTransfer was completed ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/allshare/x;->c()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 691
    iget-object v0, p0, Lcom/sec/android/allshare/t;->a:Lcom/sec/android/allshare/n;

    invoke-static {v0, p2}, Lcom/sec/android/allshare/n;->d(Lcom/sec/android/allshare/n;Ljava/lang/String;)V

    .line 700
    :cond_0
    :goto_1
    return-void

    .line 685
    :cond_1
    const-string v1, "FileReceiverImpl"

    const-string v2, "onCompleted listener is null!"

    invoke-static {v1, v2}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 697
    :catch_0
    move-exception v0

    const-string v0, "FileReceiverImpl"

    const-string v1, "FileReceiverImpl Fail to get SessionKeyInfoMap : NullPointerException"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final onFailed(Lcom/sec/android/allshare/media/FileReceiver;Ljava/lang/String;Ljava/io/File;Lcom/sec/android/allshare/ERROR;)V
    .locals 3

    .prologue
    .line 631
    const-string v0, "FileReceiverImpl"

    const-string v1, "onFailed()"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 636
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/allshare/t;->a:Lcom/sec/android/allshare/n;

    invoke-static {v0, p2}, Lcom/sec/android/allshare/n;->c(Lcom/sec/android/allshare/n;Ljava/lang/String;)Lcom/sec/android/allshare/x;

    move-result-object v0

    .line 640
    invoke-virtual {v0}, Lcom/sec/android/allshare/x;->b()Lcom/sec/android/allshare/media/FileReceiver$IFileReceiverProgressUpdateEventListener;

    move-result-object v1

    .line 643
    if-eqz v1, :cond_1

    .line 644
    invoke-interface {v1, p1, p2, p3, p4}, Lcom/sec/android/allshare/media/FileReceiver$IFileReceiverProgressUpdateEventListener;->onFailed(Lcom/sec/android/allshare/media/FileReceiver;Ljava/lang/String;Ljava/io/File;Lcom/sec/android/allshare/ERROR;)V

    .line 648
    :goto_0
    iget-object v1, p0, Lcom/sec/android/allshare/t;->a:Lcom/sec/android/allshare/n;

    invoke-static {v1}, Lcom/sec/android/allshare/n;->b(Lcom/sec/android/allshare/n;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/sec/android/allshare/x;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 651
    iget-object v0, p0, Lcom/sec/android/allshare/t;->a:Lcom/sec/android/allshare/n;

    invoke-static {v0, p2}, Lcom/sec/android/allshare/n;->d(Lcom/sec/android/allshare/n;Ljava/lang/String;)V

    .line 653
    iget-object v0, p0, Lcom/sec/android/allshare/t;->a:Lcom/sec/android/allshare/n;

    invoke-static {v0}, Lcom/sec/android/allshare/n;->c(Lcom/sec/android/allshare/n;)V

    .line 666
    :cond_0
    :goto_1
    return-void

    .line 646
    :cond_1
    const-string v1, "FileReceiverImpl"

    const-string v2, "onFailed listener is null!"

    invoke-static {v1, v2}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 664
    :catch_0
    move-exception v0

    const-string v0, "FileReceiverImpl"

    const-string v1, "FileReceiverImpl Fail to get SessionKeyInfoMap : NullPointerException"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 655
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/allshare/t;->a:Lcom/sec/android/allshare/n;

    invoke-static {v0}, Lcom/sec/android/allshare/n;->b(Lcom/sec/android/allshare/n;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 658
    iget-object v0, p0, Lcom/sec/android/allshare/t;->a:Lcom/sec/android/allshare/n;

    invoke-static {v0, p2}, Lcom/sec/android/allshare/n;->d(Lcom/sec/android/allshare/n;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public final onProgressUpdated(Lcom/sec/android/allshare/media/FileReceiver;Ljava/lang/String;JJLjava/io/File;Lcom/sec/android/allshare/ERROR;)V
    .locals 9

    .prologue
    .line 603
    const-string v0, "FileReceiverImpl"

    const-string v1, "onProgressUpdated()"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 609
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/allshare/t;->a:Lcom/sec/android/allshare/n;

    invoke-static {v0, p2}, Lcom/sec/android/allshare/n;->c(Lcom/sec/android/allshare/n;Ljava/lang/String;)Lcom/sec/android/allshare/x;

    move-result-object v0

    .line 613
    invoke-virtual {v0}, Lcom/sec/android/allshare/x;->b()Lcom/sec/android/allshare/media/FileReceiver$IFileReceiverProgressUpdateEventListener;

    move-result-object v0

    .line 616
    if-eqz v0, :cond_0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    move-wide v5, p5

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    .line 617
    invoke-interface/range {v0 .. v8}, Lcom/sec/android/allshare/media/FileReceiver$IFileReceiverProgressUpdateEventListener;->onProgressUpdated(Lcom/sec/android/allshare/media/FileReceiver;Ljava/lang/String;JJLjava/io/File;Lcom/sec/android/allshare/ERROR;)V

    .line 626
    :goto_0
    return-void

    .line 619
    :cond_0
    const-string v0, "FileReceiverImpl"

    const-string v1, "onProgressUpdated listener is null!"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 624
    :catch_0
    move-exception v0

    const-string v0, "FileReceiverImpl"

    const-string v1, "FileReceiverImpl Fail to get SessionKeyInfoMap : NullPointerException"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
