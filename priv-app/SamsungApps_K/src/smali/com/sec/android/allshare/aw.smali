.class final Lcom/sec/android/allshare/aw;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field a:Ljava/lang/String;

.field b:Landroid/os/Bundle;

.field c:Lcom/sec/android/allshare/d;

.field final synthetic d:Lcom/sec/android/allshare/at;


# direct methods
.method public constructor <init>(Lcom/sec/android/allshare/at;Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/allshare/d;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 943
    iput-object p1, p0, Lcom/sec/android/allshare/aw;->d:Lcom/sec/android/allshare/at;

    .line 942
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 938
    iput-object v0, p0, Lcom/sec/android/allshare/aw;->a:Ljava/lang/String;

    .line 939
    iput-object v0, p0, Lcom/sec/android/allshare/aw;->b:Landroid/os/Bundle;

    .line 940
    iput-object v0, p0, Lcom/sec/android/allshare/aw;->c:Lcom/sec/android/allshare/d;

    .line 944
    iput-object p2, p0, Lcom/sec/android/allshare/aw;->a:Ljava/lang/String;

    .line 945
    iput-object p3, p0, Lcom/sec/android/allshare/aw;->b:Landroid/os/Bundle;

    .line 946
    iput-object p4, p0, Lcom/sec/android/allshare/aw;->c:Lcom/sec/android/allshare/d;

    .line 947
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 952
    move-object v0, p1

    check-cast v0, Lcom/sec/android/allshare/aw;

    .line 953
    if-nez p1, :cond_0

    move v0, v1

    .line 961
    :goto_0
    return v0

    .line 955
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 956
    goto :goto_0

    .line 957
    :cond_1
    instance-of v3, p1, Lcom/sec/android/allshare/aw;

    if-nez v3, :cond_2

    move v0, v1

    .line 958
    goto :goto_0

    .line 959
    :cond_2
    iget-object v3, v0, Lcom/sec/android/allshare/aw;->a:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/allshare/aw;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, v0, Lcom/sec/android/allshare/aw;->b:Landroid/os/Bundle;

    iget-object v4, p0, Lcom/sec/android/allshare/aw;->b:Landroid/os/Bundle;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v0, v0, Lcom/sec/android/allshare/aw;->c:Lcom/sec/android/allshare/d;

    iget-object v3, p0, Lcom/sec/android/allshare/aw;->c:Lcom/sec/android/allshare/d;

    invoke-virtual {v0, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v2

    .line 960
    goto :goto_0

    :cond_3
    move v0, v1

    .line 961
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 967
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method
