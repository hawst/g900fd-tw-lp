.class public Lcom/sec/android/allshare/media/SearchCriteria$Builder;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iget-object v0, p0, Lcom/sec/android/allshare/media/SearchCriteria$Builder;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 85
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/allshare/media/SearchCriteria$Builder;->a:Ljava/lang/String;

    .line 86
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/allshare/media/SearchCriteria$Builder;->b:Ljava/util/ArrayList;

    .line 87
    return-void
.end method

.method static synthetic a(Lcom/sec/android/allshare/media/SearchCriteria$Builder;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/allshare/media/SearchCriteria$Builder;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/android/allshare/media/SearchCriteria$Builder;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/allshare/media/SearchCriteria$Builder;->b:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public addItemType(Lcom/sec/android/allshare/Item$MediaType;)Lcom/sec/android/allshare/media/SearchCriteria$Builder;
    .locals 1

    .prologue
    .line 116
    if-eqz p1, :cond_0

    .line 117
    iget-object v0, p0, Lcom/sec/android/allshare/media/SearchCriteria$Builder;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 118
    :cond_0
    return-object p0
.end method

.method public build()Lcom/sec/android/allshare/media/SearchCriteria;
    .locals 2

    .prologue
    .line 131
    new-instance v0, Lcom/sec/android/allshare/media/SearchCriteria;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/allshare/media/SearchCriteria;-><init>(Lcom/sec/android/allshare/media/SearchCriteria$Builder;B)V

    return-object v0
.end method

.method public setKeyword(Ljava/lang/String;)Lcom/sec/android/allshare/media/SearchCriteria$Builder;
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/sec/android/allshare/media/SearchCriteria$Builder;->a:Ljava/lang/String;

    .line 101
    return-object p0
.end method
