.class final Lcom/sec/android/allshare/ay;
.super Landroid/os/Handler;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/allshare/TVControllerImpl;


# direct methods
.method constructor <init>(Lcom/sec/android/allshare/TVControllerImpl;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/allshare/ay;->a:Lcom/sec/android/allshare/TVControllerImpl;

    .line 110
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 116
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 118
    const-string v0, "TVControllerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[TVControl] Event : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 225
    :cond_0
    :goto_0
    return-void

    .line 122
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/allshare/ay;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0, v3}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;Z)V

    .line 124
    iget-object v0, p0, Lcom/sec/android/allshare/ay;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/sec/android/allshare/ay;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/allshare/ay;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v2, Lcom/sec/android/allshare/ERROR;->INVALID_DEVICE:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onConnectResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 132
    :sswitch_1
    const-string v0, "TVControllerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[TVControl] Event : IAPP_AUTHENTICATION arg : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-nez v0, :cond_1

    .line 135
    iget-object v0, p0, Lcom/sec/android/allshare/ay;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0, v3}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;Z)V

    .line 137
    iget-object v0, p0, Lcom/sec/android/allshare/ay;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->b(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/sec/android/allshare/ay;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->b(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IEventListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/allshare/ay;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v2, Lcom/sec/android/allshare/ERROR;->FAIL:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$IEventListener;->onAuthenticationFailed(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 142
    :cond_1
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-ne v0, v4, :cond_2

    .line 144
    iget-object v0, p0, Lcom/sec/android/allshare/ay;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->c(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/IAppControlAPI;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/allshare/IAppControlAPI;->g()V

    .line 145
    iget-object v0, p0, Lcom/sec/android/allshare/ay;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->c(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/IAppControlAPI;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/allshare/IAppControlAPI;->f()V

    .line 147
    iget-object v0, p0, Lcom/sec/android/allshare/ay;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->d(Lcom/sec/android/allshare/TVControllerImpl;)V

    goto :goto_0

    .line 152
    :cond_2
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-ne v0, v5, :cond_0

    .line 154
    iget-object v0, p0, Lcom/sec/android/allshare/ay;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->c(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/IAppControlAPI;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/allshare/IAppControlAPI;->g()V

    .line 155
    iget-object v0, p0, Lcom/sec/android/allshare/ay;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->c(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/IAppControlAPI;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/allshare/IAppControlAPI;->f()V

    .line 157
    iget-object v0, p0, Lcom/sec/android/allshare/ay;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->d(Lcom/sec/android/allshare/TVControllerImpl;)V

    goto/16 :goto_0

    .line 165
    :sswitch_2
    const-string v0, "TVControllerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[TVControl] Event : IAPP_AUTHENTICATION_TIMEOUT arg : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    iget-object v0, p0, Lcom/sec/android/allshare/ay;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0, v3}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;Z)V

    .line 169
    iget-object v0, p0, Lcom/sec/android/allshare/ay;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->b(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/sec/android/allshare/ay;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->b(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IEventListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/allshare/ay;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v2, Lcom/sec/android/allshare/ERROR;->TIME_OUT:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$IEventListener;->onAuthenticationFailed(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 176
    :sswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 177
    const-string v1, "TVControllerImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[TVControl] Event : IAPP_KEYBOARD_SYNC arg : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    iget-object v1, p0, Lcom/sec/android/allshare/ay;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v1}, Lcom/sec/android/allshare/TVControllerImpl;->b(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IEventListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 181
    iget-object v1, p0, Lcom/sec/android/allshare/ay;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v1}, Lcom/sec/android/allshare/TVControllerImpl;->b(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IEventListener;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/allshare/ay;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v3, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v1, v2, v0, v3}, Lcom/sec/android/allshare/control/TVController$IEventListener;->onStringChanged(Lcom/sec/android/allshare/control/TVController;Ljava/lang/String;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 186
    :sswitch_4
    const-string v0, "TVControllerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[TVControl] Event : IAPP_EXIT arg : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    iget-object v0, p0, Lcom/sec/android/allshare/ay;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0, v3}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;Z)V

    .line 190
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-nez v0, :cond_3

    .line 192
    iget-object v0, p0, Lcom/sec/android/allshare/ay;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->b(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/sec/android/allshare/ay;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->b(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IEventListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/allshare/ay;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v2, Lcom/sec/android/allshare/ERROR;->DELETED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$IEventListener;->onAuthenticationFailed(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 197
    :cond_3
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-ne v0, v4, :cond_4

    .line 199
    iget-object v0, p0, Lcom/sec/android/allshare/ay;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->b(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/sec/android/allshare/ay;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->b(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IEventListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/allshare/ay;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v2, Lcom/sec/android/allshare/ERROR;->PERMISSION_NOT_ALLOWED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$IEventListener;->onAuthenticationFailed(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 204
    :cond_4
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-ne v0, v5, :cond_0

    goto/16 :goto_0

    .line 214
    :sswitch_5
    const-string v0, "TVControllerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[TVControl] Event : IAPP_REMOCON arg : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 218
    :sswitch_6
    const-string v0, "TVControllerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[TVControl] Event : IAPP_STATUS arg : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 222
    :sswitch_7
    const-string v0, "TVControllerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[TVControl] Event : IAPP_REMOTE_INPUT_TYPE arg : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 119
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_5
        0x3 -> :sswitch_3
        0xa -> :sswitch_7
        0x64 -> :sswitch_1
        0x65 -> :sswitch_2
        0xc8 -> :sswitch_6
        0x12c -> :sswitch_4
        0x270f -> :sswitch_0
    .end sparse-switch
.end method
