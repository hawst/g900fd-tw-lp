.class final Lcom/sec/android/allshare/ao;
.super Lcom/sec/android/allshare/d;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/allshare/an;


# direct methods
.method constructor <init>(Lcom/sec/android/allshare/an;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/allshare/ao;->a:Lcom/sec/android/allshare/an;

    .line 58
    invoke-direct {p0, p2}, Lcom/sec/android/allshare/d;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 7

    .prologue
    .line 64
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v0

    .line 65
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v5

    .line 67
    iget-object v1, p0, Lcom/sec/android/allshare/ao;->a:Lcom/sec/android/allshare/an;

    invoke-static {v1}, Lcom/sec/android/allshare/an;->a(Lcom/sec/android/allshare/an;)Lcom/sec/android/allshare/media/Receiver$IProgressUpdateEventListener;

    move-result-object v1

    if-nez v1, :cond_1

    .line 117
    :cond_0
    :goto_0
    return-void

    .line 69
    :cond_1
    const-string v1, "BUNDLE_ENUM_ERROR"

    invoke-virtual {v5, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 70
    sget-object v2, Lcom/sec/android/allshare/ERROR;->FAIL:Lcom/sec/android/allshare/ERROR;

    .line 71
    if-nez v1, :cond_2

    .line 72
    sget-object v6, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    .line 76
    :goto_1
    const-string v1, "com.sec.android.allshare.event.EVENT_RECEIVER_PROGRESS_UPDATE_BY_ITEM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 78
    const-string v0, "BUNDLE_LONG_PROGRESS"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    .line 79
    const-string v0, "BUNDLE_LONG_TOTAL_SIZE"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    .line 80
    const-string v0, "BUNDLE_PARCELABLE_ITEM"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 82
    iget-object v5, p0, Lcom/sec/android/allshare/ao;->a:Lcom/sec/android/allshare/an;

    invoke-static {v0}, Lcom/sec/android/allshare/an;->a(Landroid/os/Bundle;)Lcom/sec/android/allshare/Item;

    move-result-object v5

    .line 85
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/allshare/ao;->a:Lcom/sec/android/allshare/an;

    invoke-static {v0}, Lcom/sec/android/allshare/an;->a(Lcom/sec/android/allshare/an;)Lcom/sec/android/allshare/media/Receiver$IProgressUpdateEventListener;

    move-result-object v0

    invoke-interface/range {v0 .. v6}, Lcom/sec/android/allshare/media/Receiver$IProgressUpdateEventListener;->onProgressUpdated(JJLcom/sec/android/allshare/Item;Lcom/sec/android/allshare/ERROR;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 87
    :catch_0
    move-exception v0

    .line 89
    const-string v1, "ReceiverImpl"

    const-string v2, "mEventHandler(EVENT_RECEIVER_PROGRESS_UPDATE_BY_ITEM) Exception "

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 74
    :cond_2
    invoke-static {v1}, Lcom/sec/android/allshare/ERROR;->valueOf(Ljava/lang/String;)Lcom/sec/android/allshare/ERROR;

    move-result-object v6

    goto :goto_1

    .line 91
    :catch_1
    move-exception v0

    .line 93
    const-string v1, "ReceiverImpl"

    const-string v2, "mEventHandler(EVENT_RECEIVER_PROGRESS_UPDATE_BY_ITEM) Error "

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Error;)V

    goto :goto_0

    .line 97
    :cond_3
    const-string v1, "com.sec.android.allshare.event.EVENT_RECEIVER_COMPLETED_BY_ITEM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    const-string v0, "BUNDLE_PARCELABLE_ITEM"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 100
    iget-object v1, p0, Lcom/sec/android/allshare/ao;->a:Lcom/sec/android/allshare/an;

    invoke-static {v0}, Lcom/sec/android/allshare/an;->a(Landroid/os/Bundle;)Lcom/sec/android/allshare/Item;

    move-result-object v0

    .line 103
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/allshare/ao;->a:Lcom/sec/android/allshare/an;

    invoke-static {v1}, Lcom/sec/android/allshare/an;->a(Lcom/sec/android/allshare/an;)Lcom/sec/android/allshare/media/Receiver$IProgressUpdateEventListener;

    move-result-object v1

    invoke-interface {v1, v0, v6}, Lcom/sec/android/allshare/media/Receiver$IProgressUpdateEventListener;->onCompleted(Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/ERROR;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Error; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_0

    .line 105
    :catch_2
    move-exception v0

    .line 107
    const-string v1, "ReceiverImpl"

    const-string v2, "mEventHandler(EVENT_RECEIVER_COMPLETED_BY_ITEM) Exception "

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 109
    :catch_3
    move-exception v0

    .line 111
    const-string v1, "ReceiverImpl"

    const-string v2, "mEventHandler(EVENT_RECEIVER_COMPLETED_BY_ITEM) Error "

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Error;)V

    goto :goto_0
.end method
