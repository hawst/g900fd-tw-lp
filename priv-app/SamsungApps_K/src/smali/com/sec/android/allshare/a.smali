.class final Lcom/sec/android/allshare/a;
.super Lcom/sec/android/allshare/media/AVPlayer;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/allshare/iface/IBundleHolder;
.implements Lcom/sec/android/allshare/iface/IHandlerHolder;


# static fields
.field private static synthetic i:[I


# instance fields
.field a:Ljava/util/ArrayList;

.field private b:Lcom/sec/android/allshare/j;

.field private c:Lcom/sec/android/allshare/IAllShareConnector;

.field private d:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerEventListener;

.field private e:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

.field private f:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;

.field private g:Lcom/sec/android/allshare/d;

.field private h:Lcom/sec/android/allshare/e;


# direct methods
.method constructor <init>(Lcom/sec/android/allshare/IAllShareConnector;Lcom/sec/android/allshare/j;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 54
    invoke-direct {p0}, Lcom/sec/android/allshare/media/AVPlayer;-><init>()V

    .line 46
    iput-object v0, p0, Lcom/sec/android/allshare/a;->b:Lcom/sec/android/allshare/j;

    .line 48
    iput-object v0, p0, Lcom/sec/android/allshare/a;->d:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerEventListener;

    .line 49
    iput-object v0, p0, Lcom/sec/android/allshare/a;->e:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    .line 50
    iput-object v0, p0, Lcom/sec/android/allshare/a;->f:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;

    .line 52
    iput-object v0, p0, Lcom/sec/android/allshare/a;->a:Ljava/util/ArrayList;

    .line 631
    new-instance v0, Lcom/sec/android/allshare/b;

    invoke-static {}, Lcom/sec/android/allshare/ServiceConnector;->b()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/allshare/b;-><init>(Lcom/sec/android/allshare/a;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/allshare/a;->g:Lcom/sec/android/allshare/d;

    .line 716
    new-instance v0, Lcom/sec/android/allshare/c;

    invoke-static {}, Lcom/sec/android/allshare/ServiceConnector;->b()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/allshare/c;-><init>(Lcom/sec/android/allshare/a;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/allshare/a;->h:Lcom/sec/android/allshare/e;

    .line 56
    if-nez p1, :cond_0

    .line 58
    const-string v0, "AVPlayerImpl"

    const-string v1, "Connection FAIL: AllShare Service Connector does not exist"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    :goto_0
    return-void

    .line 62
    :cond_0
    iput-object p1, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    .line 63
    iput-object p2, p0, Lcom/sec/android/allshare/a;->b:Lcom/sec/android/allshare/j;

    .line 65
    const-string v0, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    invoke-virtual {p2}, Lcom/sec/android/allshare/j;->getBundle()Landroid/os/Bundle;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/allshare/a;->g:Lcom/sec/android/allshare/d;

    invoke-interface {p1, v0, v1, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/allshare/d;)Z

    goto :goto_0
.end method

.method private static a(Lcom/sec/android/allshare/Item;)Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 531
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 532
    instance-of v1, p0, Lcom/sec/android/allshare/iface/IBundleHolder;

    if-eqz v1, :cond_0

    .line 533
    const-string v1, "BUNDLE_PARCELABLE_ITEM"

    check-cast p0, Lcom/sec/android/allshare/iface/IBundleHolder;

    invoke-interface {p0}, Lcom/sec/android/allshare/iface/IBundleHolder;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 535
    :cond_0
    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/allshare/a;)Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerEventListener;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/allshare/a;->d:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerEventListener;

    return-object v0
.end method

.method private a(Landroid/net/Uri;Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/media/ContentInfo;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 588
    iget-object v0, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 590
    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/a;->e:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p2, p3, v1}, Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;->onPlayResponseReceived(Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/media/ContentInfo;Lcom/sec/android/allshare/ERROR;)V

    .line 607
    :goto_0
    return-void

    .line 594
    :cond_1
    invoke-virtual {p2}, Lcom/sec/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 596
    new-instance v1, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v1}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 597
    invoke-virtual {v1, p4}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 599
    invoke-static {p2}, Lcom/sec/android/allshare/a;->a(Lcom/sec/android/allshare/Item;)Landroid/os/Bundle;

    move-result-object v2

    .line 600
    const-string v3, "BUNDLE_STRING_TITLE"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 601
    const-string v0, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/a;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 602
    const-string v0, "BUNDLE_PARCELABLE_URI"

    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 603
    const-string v0, "BUNDLE_PARCELABLE_CONTENT_INFO"

    invoke-virtual {v2, v0, p3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 605
    invoke-virtual {v1, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 606
    iget-object v0, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v2, p0, Lcom/sec/android/allshare/a;->h:Lcom/sec/android/allshare/e;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    goto :goto_0
.end method

.method private static synthetic a()[I
    .locals 3

    .prologue
    .line 42
    sget-object v0, Lcom/sec/android/allshare/a;->i:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/allshare/Item$MediaType;->values()[Lcom/sec/android/allshare/Item$MediaType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/allshare/Item$MediaType;->ITEM_AUDIO:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/allshare/Item$MediaType;->ITEM_FOLDER:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/android/allshare/Item$MediaType;->ITEM_IMAGE:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    :try_start_3
    sget-object v1, Lcom/sec/android/allshare/Item$MediaType;->ITEM_UNKNOWN:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_4
    :try_start_4
    sget-object v1, Lcom/sec/android/allshare/Item$MediaType;->ITEM_VIDEO:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_5
    sput-object v0, Lcom/sec/android/allshare/a;->i:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_5

    :catch_1
    move-exception v1

    goto :goto_4

    :catch_2
    move-exception v1

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_1
.end method

.method static synthetic b(Lcom/sec/android/allshare/a;)Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/allshare/a;->e:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/android/allshare/a;)Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/allshare/a;->f:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;

    return-object v0
.end method


# virtual methods
.method public final getBundle()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 522
    iget-object v0, p0, Lcom/sec/android/allshare/a;->b:Lcom/sec/android/allshare/j;

    if-nez v0, :cond_0

    .line 524
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 526
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/a;->b:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method public final getDeviceDomain()Lcom/sec/android/allshare/Device$DeviceDomain;
    .locals 1

    .prologue
    .line 512
    iget-object v0, p0, Lcom/sec/android/allshare/a;->b:Lcom/sec/android/allshare/j;

    if-nez v0, :cond_0

    .line 514
    sget-object v0, Lcom/sec/android/allshare/Device$DeviceDomain;->UNKNOWN:Lcom/sec/android/allshare/Device$DeviceDomain;

    .line 516
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/a;->b:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getDeviceDomain()Lcom/sec/android/allshare/Device$DeviceDomain;

    move-result-object v0

    goto :goto_0
.end method

.method public final getDeviceType()Lcom/sec/android/allshare/Device$DeviceType;
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Lcom/sec/android/allshare/a;->b:Lcom/sec/android/allshare/j;

    if-nez v0, :cond_0

    .line 454
    sget-object v0, Lcom/sec/android/allshare/Device$DeviceType;->UNKNOWN:Lcom/sec/android/allshare/Device$DeviceType;

    .line 456
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/a;->b:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getDeviceType()Lcom/sec/android/allshare/Device$DeviceType;

    move-result-object v0

    goto :goto_0
.end method

.method public final getID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 502
    iget-object v0, p0, Lcom/sec/android/allshare/a;->b:Lcom/sec/android/allshare/j;

    if-nez v0, :cond_0

    .line 504
    const-string v0, ""

    .line 506
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/a;->b:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getID()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getIPAdress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 462
    iget-object v0, p0, Lcom/sec/android/allshare/a;->b:Lcom/sec/android/allshare/j;

    if-nez v0, :cond_0

    .line 464
    const-string v0, ""

    .line 466
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/a;->b:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getIPAdress()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getIcon()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 482
    iget-object v0, p0, Lcom/sec/android/allshare/a;->b:Lcom/sec/android/allshare/j;

    if-nez v0, :cond_0

    .line 484
    const-string v0, ""

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 486
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/a;->b:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getIcon()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public final getIconList()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 492
    iget-object v0, p0, Lcom/sec/android/allshare/a;->b:Lcom/sec/android/allshare/j;

    if-nez v0, :cond_0

    .line 494
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 496
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/a;->b:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getIconList()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public final getMediaInfo()V
    .locals 4

    .prologue
    .line 844
    iget-object v0, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 846
    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/a;->e:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    const/4 v1, 0x0

    sget-object v2, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;->onGetMediaInfoResponseReceived(Lcom/sec/android/allshare/media/MediaInfo;Lcom/sec/android/allshare/ERROR;)V

    .line 858
    :goto_0
    return-void

    .line 850
    :cond_1
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 851
    const-string v1, "com.sec.android.allshare.action.ACTION_AV_PLAYER_REQUEST_GET_MEDIA_INFO"

    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 853
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 854
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/a;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 855
    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 857
    iget-object v1, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v2, p0, Lcom/sec/android/allshare/a;->h:Lcom/sec/android/allshare/e;

    invoke-interface {v1, v0, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    goto :goto_0
.end method

.method public final getModelName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 442
    iget-object v0, p0, Lcom/sec/android/allshare/a;->b:Lcom/sec/android/allshare/j;

    if-nez v0, :cond_0

    .line 444
    const-string v0, ""

    .line 446
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/a;->b:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getModelName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getMute()V
    .locals 4

    .prologue
    .line 327
    iget-object v0, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 329
    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/a;->f:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;

    const/4 v1, 0x0

    sget-object v2, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;->onSetMuteResponseReceived(ZLcom/sec/android/allshare/ERROR;)V

    .line 340
    :goto_0
    return-void

    .line 333
    :cond_1
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 334
    const-string v1, "com.sec.android.allshare.action.ACTION_AV_PLAYER_REQUEST_GET_MUTE"

    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 335
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 336
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/a;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 339
    iget-object v1, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v2, p0, Lcom/sec/android/allshare/a;->h:Lcom/sec/android/allshare/e;

    invoke-interface {v1, v0, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    goto :goto_0
.end method

.method public final getNIC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 935
    iget-object v0, p0, Lcom/sec/android/allshare/a;->b:Lcom/sec/android/allshare/j;

    if-nez v0, :cond_0

    .line 936
    const-string v0, ""

    .line 938
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/a;->b:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getNIC()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 472
    iget-object v0, p0, Lcom/sec/android/allshare/a;->b:Lcom/sec/android/allshare/j;

    if-nez v0, :cond_0

    .line 474
    const-string v0, ""

    .line 476
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/a;->b:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getPlayPosition()V
    .locals 4

    .prologue
    .line 367
    iget-object v0, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 369
    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/a;->e:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    const-wide/16 v1, -0x1

    sget-object v3, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;->onGetPlayPositionResponseReceived(JLcom/sec/android/allshare/ERROR;)V

    .line 381
    :goto_0
    return-void

    .line 373
    :cond_1
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 374
    const-string v1, "com.sec.android.allshare.action.ACTION_AV_PLAYER_REQUEST_PLAY_POSITION"

    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 375
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 377
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/a;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 380
    iget-object v1, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v2, p0, Lcom/sec/android/allshare/a;->h:Lcom/sec/android/allshare/e;

    invoke-interface {v1, v0, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    goto :goto_0
.end method

.method public final getPlayerState()Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;
    .locals 5

    .prologue
    .line 386
    sget-object v0, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;->UNKNOWN:Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

    .line 388
    iget-object v1, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v1}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 416
    :cond_0
    :goto_0
    return-object v0

    .line 391
    :cond_1
    new-instance v1, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v1}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 392
    const-string v2, "com.sec.android.allshare.action.ACTION_AV_PLAYER_GET_PLAYER_STATE_SYNC"

    invoke-virtual {v1, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 394
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 395
    const-string v3, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/a;->getID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    invoke-virtual {v1, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 398
    iget-object v2, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v2, v1}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v1

    .line 399
    if-eqz v1, :cond_0

    .line 402
    invoke-virtual {v1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v1

    .line 404
    if-eqz v1, :cond_0

    .line 410
    :try_start_0
    const-string v2, "BUNDLE_STRING_AV_PLAER_STATE"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;->valueOf(Ljava/lang/String;)Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 412
    :catch_0
    move-exception v1

    .line 414
    const-string v2, "AVPlayerImpl"

    const-string v3, "getPlayerState Exception"

    invoke-static {v2, v3, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final getState()V
    .locals 4

    .prologue
    .line 944
    iget-object v0, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 946
    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/a;->e:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    sget-object v1, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;->UNKNOWN:Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

    sget-object v2, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;->onGetStateResponseReceived(Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;Lcom/sec/android/allshare/ERROR;)V

    .line 958
    :goto_0
    return-void

    .line 950
    :cond_1
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 951
    const-string v1, "com.sec.android.allshare.action.ACTION_AV_PLAYER_REQUEST_GET_PLAYER_STATE"

    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 953
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 954
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/a;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 955
    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 957
    iget-object v1, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v2, p0, Lcom/sec/android/allshare/a;->h:Lcom/sec/android/allshare/e;

    invoke-interface {v1, v0, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    goto :goto_0
.end method

.method public final getVolume()V
    .locals 4

    .prologue
    .line 280
    iget-object v0, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 282
    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/a;->f:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;

    const/4 v1, -0x1

    sget-object v2, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;->onGetVolumeResponseReceived(ILcom/sec/android/allshare/ERROR;)V

    .line 294
    :goto_0
    return-void

    .line 286
    :cond_1
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 287
    const-string v1, "com.sec.android.allshare.action.ACTION_AV_PLAYER_REQUEST_GET_VOLUME"

    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 288
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 290
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/a;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 293
    iget-object v1, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v2, p0, Lcom/sec/android/allshare/a;->h:Lcom/sec/android/allshare/e;

    invoke-interface {v1, v0, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    goto :goto_0
.end method

.method public final isSupportAudio()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 899
    .line 900
    iget-object v1, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v1}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 928
    :cond_0
    :goto_0
    return v0

    .line 903
    :cond_1
    new-instance v1, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v1}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 904
    const-string v2, "com.sec.android.allshare.action.ACTION_AV_PLAYER_IS_SUPPORT_AUDIO_SYNC"

    invoke-virtual {v1, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 906
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 907
    const-string v3, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/a;->getID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 908
    invoke-virtual {v1, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 910
    iget-object v2, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v2, v1}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v1

    .line 911
    if-eqz v1, :cond_0

    .line 914
    invoke-virtual {v1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v1

    .line 916
    if-eqz v1, :cond_0

    .line 922
    :try_start_0
    const-string v2, "BUNDLE_BOOLEAN_SUPPORT_AUDIO"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 924
    :catch_0
    move-exception v1

    .line 926
    const-string v2, "AVPlayerImpl"

    const-string v3, "isSupportAudio Exception"

    invoke-static {v2, v3, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final isSupportVideo()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 863
    .line 864
    iget-object v1, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v1}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 892
    :cond_0
    :goto_0
    return v0

    .line 867
    :cond_1
    new-instance v1, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v1}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 868
    const-string v2, "com.sec.android.allshare.action.ACTION_AV_PLAYER_IS_SUPPORT_VIDEO_SYNC"

    invoke-virtual {v1, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 870
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 871
    const-string v3, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/a;->getID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 872
    invoke-virtual {v1, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 874
    iget-object v2, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v2, v1}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v1

    .line 875
    if-eqz v1, :cond_0

    .line 878
    invoke-virtual {v1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v1

    .line 880
    if-eqz v1, :cond_0

    .line 886
    :try_start_0
    const-string v2, "BUNDLE_BOOLEAN_SUPPORT_VIDEO"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 888
    :catch_0
    move-exception v1

    .line 890
    const-string v2, "AVPlayerImpl"

    const-string v3, "isSupportVideo Exception"

    invoke-static {v2, v3, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final pause()V
    .locals 4

    .prologue
    .line 241
    iget-object v0, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 243
    :cond_0
    const-string v0, "AVPlayerImpl"

    const-string v1, "pause fail : SERVICE_NOT_CONNECTED"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    iget-object v0, p0, Lcom/sec/android/allshare/a;->e:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1}, Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;->onPauseResponseReceived(Lcom/sec/android/allshare/ERROR;)V

    .line 255
    :goto_0
    return-void

    .line 247
    :cond_1
    const-string v0, "AVPlayerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "pause "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/allshare/a;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 249
    const-string v1, "com.sec.android.allshare.action.ACTION_AV_PLAYER_PAUSE"

    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 250
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 251
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/a;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 254
    iget-object v1, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v2, p0, Lcom/sec/android/allshare/a;->h:Lcom/sec/android/allshare/e;

    invoke-interface {v1, v0, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    goto :goto_0
.end method

.method public final play(Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/media/ContentInfo;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 71
    iget-object v0, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 73
    :cond_0
    const-string v0, "AVPlayerImpl"

    const-string v1, "play fail : SERVICE_NOT_CONNECTED"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    iget-object v0, p0, Lcom/sec/android/allshare/a;->e:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p1, p2, v1}, Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;->onPlayResponseReceived(Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/media/ContentInfo;Lcom/sec/android/allshare/ERROR;)V

    .line 194
    :cond_1
    :goto_0
    return-void

    .line 78
    :cond_2
    if-nez p1, :cond_3

    .line 80
    const-string v0, "AVPlayerImpl"

    const-string v1, "play item == null"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lcom/sec/android/allshare/a;->e:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    if-eqz v0, :cond_1

    .line 82
    iget-object v0, p0, Lcom/sec/android/allshare/a;->e:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p1, p2, v1}, Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;->onPlayResponseReceived(Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/media/ContentInfo;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 86
    :cond_3
    invoke-virtual {p1}, Lcom/sec/android/allshare/Item;->getType()Lcom/sec/android/allshare/Item$MediaType;

    move-result-object v0

    .line 88
    if-nez v0, :cond_4

    .line 90
    const-string v0, "AVPlayerImpl"

    const-string v1, "Invalid media type"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lcom/sec/android/allshare/a;->e:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    if-eqz v0, :cond_1

    .line 92
    iget-object v0, p0, Lcom/sec/android/allshare/a;->e:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p1, p2, v1}, Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;->onPlayResponseReceived(Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/media/ContentInfo;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 97
    :cond_4
    iput-object v1, p0, Lcom/sec/android/allshare/a;->a:Ljava/util/ArrayList;

    .line 99
    invoke-static {}, Lcom/sec/android/allshare/a;->a()[I

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    .line 105
    :pswitch_0
    const-string v0, "AVPlayerImpl"

    const-string v1, "Invalid media type"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lcom/sec/android/allshare/a;->e:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    if-eqz v0, :cond_1

    .line 107
    iget-object v0, p0, Lcom/sec/android/allshare/a;->e:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p1, p2, v1}, Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;->onPlayResponseReceived(Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/media/ContentInfo;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 110
    :pswitch_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 111
    instance-of v2, p1, Lcom/sec/android/allshare/iface/IBundleHolder;

    if-eqz v2, :cond_5

    move-object v0, p1

    .line 113
    check-cast v0, Lcom/sec/android/allshare/iface/IBundleHolder;

    invoke-interface {v0}, Lcom/sec/android/allshare/iface/IBundleHolder;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 116
    :cond_5
    const-string v2, "BUNDLE_STRING_ITEM_MIMETYPE"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 117
    const-string v3, "BUNDLE_STRING_ITEM_CONSTRUCTOR_KEY"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 120
    const-string v4, "WEB_CONTENT"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 122
    const-string v0, "AVPlayerImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "play WEB_CONTENT - "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/allshare/a;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    const-string v0, "AVPlayerImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "play URI "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    const-string v0, "image"

    invoke-virtual {v2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {p1}, Lcom/sec/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_7

    .line 127
    :cond_6
    const-string v0, "AVPlayerImpl"

    const-string v1, "try to play invalid content"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    iget-object v0, p0, Lcom/sec/android/allshare/a;->e:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    if-eqz v0, :cond_1

    .line 129
    iget-object v0, p0, Lcom/sec/android/allshare/a;->e:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p1, p2, v1}, Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;->onPlayResponseReceived(Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/media/ContentInfo;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 133
    :cond_7
    if-eqz p2, :cond_8

    .line 134
    const-string v0, "AVPlayerImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "play position - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/sec/android/allshare/media/ContentInfo;->getStartingPosition()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    :cond_8
    invoke-virtual {p1}, Lcom/sec/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v2

    if-eqz p2, :cond_17

    new-instance v0, Lcom/sec/android/allshare/media/ContentInfo$Builder;

    invoke-direct {v0}, Lcom/sec/android/allshare/media/ContentInfo$Builder;-><init>()V

    invoke-virtual {p2}, Lcom/sec/android/allshare/media/ContentInfo;->getStartingPosition()J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    mul-long/2addr v3, v5

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/allshare/media/ContentInfo$Builder;->setStartingPosition(J)Lcom/sec/android/allshare/media/ContentInfo$Builder;

    invoke-virtual {v0}, Lcom/sec/android/allshare/media/ContentInfo$Builder;->build()Lcom/sec/android/allshare/media/ContentInfo;

    move-result-object v0

    :goto_1
    const-string v1, "com.sec.android.allshare.action.ACTION_AV_PLAYER_PLAY_URI"

    invoke-direct {p0, v2, p1, v0, v1}, Lcom/sec/android/allshare/a;->a(Landroid/net/Uri;Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/media/ContentInfo;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 138
    :cond_9
    const-string v1, "MEDIA_SERVER"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 140
    const-string v0, "AVPlayerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "play MEDIA_SERVER - "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/allshare/a;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    invoke-virtual {p1}, Lcom/sec/android/allshare/Item;->getType()Lcom/sec/android/allshare/Item$MediaType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/allshare/Item$MediaType;->ITEM_IMAGE:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/Item$MediaType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 144
    const-string v0, "AVPlayerImpl"

    const-string v1, "try to play image using avplayer"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    iget-object v0, p0, Lcom/sec/android/allshare/a;->e:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    if-eqz v0, :cond_1

    .line 146
    iget-object v0, p0, Lcom/sec/android/allshare/a;->e:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p1, p2, v1}, Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;->onPlayResponseReceived(Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/media/ContentInfo;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 149
    :cond_a
    if-eqz p2, :cond_b

    .line 150
    const-string v0, "AVPlayerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "play position - "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/sec/android/allshare/media/ContentInfo;->getStartingPosition()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    :cond_b
    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_PLAY"

    iget-object v1, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v1}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v1

    if-nez v1, :cond_d

    :cond_c
    iget-object v0, p0, Lcom/sec/android/allshare/a;->e:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p1, p2, v1}, Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;->onPlayResponseReceived(Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/media/ContentInfo;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    :cond_d
    new-instance v1, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v1}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    invoke-virtual {v1, v0}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/sec/android/allshare/a;->a(Lcom/sec/android/allshare/Item;)Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/a;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "BUNDLE_PARCELABLE_CONTENT_INFO"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v1, v0}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v2, p0, Lcom/sec/android/allshare/a;->h:Lcom/sec/android/allshare/e;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    goto/16 :goto_0

    .line 154
    :cond_e
    const-string v1, "LOCAL_CONTENT"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 156
    invoke-virtual {p1}, Lcom/sec/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v1

    .line 157
    const-string v3, "image"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_f

    if-nez v1, :cond_10

    .line 159
    :cond_f
    const-string v0, "AVPlayerImpl"

    const-string v1, "try to play invalid content"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    iget-object v0, p0, Lcom/sec/android/allshare/a;->e:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    if-eqz v0, :cond_1

    .line 161
    iget-object v0, p0, Lcom/sec/android/allshare/a;->e:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p1, p2, v1}, Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;->onPlayResponseReceived(Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/media/ContentInfo;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 165
    :cond_10
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    .line 167
    const-string v3, "file"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_14

    .line 169
    const-string v3, "BUNDLE_STRING_FILEPATH"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 170
    const-string v3, "AVPlayerImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "play LOCAL_CONTENT file - "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/allshare/a;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    const-string v3, "AVPlayerImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "play filePath- "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    if-nez v0, :cond_11

    .line 175
    const-string v0, "AVPlayerImpl"

    const-string v1, "play LOCAL_CONTENT : uri == null"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    iget-object v0, p0, Lcom/sec/android/allshare/a;->e:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p1, p2, v1}, Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;->onPlayResponseReceived(Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/media/ContentInfo;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 180
    :cond_11
    if-eqz p2, :cond_12

    .line 181
    const-string v0, "AVPlayerImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "play position - "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/sec/android/allshare/media/ContentInfo;->getStartingPosition()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    :cond_12
    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_PLAY_LOCAL_CONTENS_FILEPATH_WITH_METADATA"

    iget-object v3, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v3, :cond_13

    iget-object v3, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v3}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v3

    if-nez v3, :cond_16

    :cond_13
    iget-object v0, p0, Lcom/sec/android/allshare/a;->e:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    sget-object v3, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p1, p2, v3}, Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;->onPlayResponseReceived(Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/media/ContentInfo;Lcom/sec/android/allshare/ERROR;)V

    .line 184
    :cond_14
    :goto_2
    const-string v0, "content"

    invoke-virtual {v2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 186
    const-string v0, "AVPlayerImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "play LOCAL_CONTENT content - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/allshare/a;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    const-string v0, "AVPlayerImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "play uri- "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    if-eqz p2, :cond_15

    .line 189
    const-string v0, "AVPlayerImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "play position - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/sec/android/allshare/media/ContentInfo;->getStartingPosition()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    :cond_15
    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_PLAY_LOCAL_CONTENS_URI"

    invoke-direct {p0, v1, p1, p2, v0}, Lcom/sec/android/allshare/a;->a(Landroid/net/Uri;Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/media/ContentInfo;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 182
    :cond_16
    invoke-virtual {p1}, Lcom/sec/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v4}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    invoke-virtual {v4, v0}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/sec/android/allshare/a;->a(Lcom/sec/android/allshare/Item;)Landroid/os/Bundle;

    move-result-object v0

    const-string v5, "BUNDLE_STRING_TITLE"

    invoke-virtual {v0, v5, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/a;->getID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "BUNDLE_PARCELABLE_CONTENT_INFO"

    invoke-virtual {v0, v3, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v4, v0}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v3, p0, Lcom/sec/android/allshare/a;->h:Lcom/sec/android/allshare/e;

    invoke-interface {v0, v4, v3}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    goto/16 :goto_2

    :cond_17
    move-object v0, v1

    goto/16 :goto_1

    .line 99
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final removeEventHandler()V
    .locals 4

    .prologue
    .line 963
    iget-object v0, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_UNSUBSCRIBE"

    invoke-virtual {p0}, Lcom/sec/android/allshare/a;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/allshare/a;->g:Lcom/sec/android/allshare/d;

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/android/allshare/IAllShareConnector;->b(Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/allshare/d;)V

    .line 964
    return-void
.end method

.method public final resume()V
    .locals 4

    .prologue
    .line 260
    iget-object v0, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 262
    :cond_0
    const-string v0, "AVPlayerImpl"

    const-string v1, "resume fail : SERVICE_NOT_CONNECTED"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    iget-object v0, p0, Lcom/sec/android/allshare/a;->e:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1}, Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;->onResumeResponseReceived(Lcom/sec/android/allshare/ERROR;)V

    .line 275
    :goto_0
    return-void

    .line 266
    :cond_1
    const-string v0, "AVPlayerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "resume "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/allshare/a;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 268
    const-string v1, "com.sec.android.allshare.action.ACTION_AV_PLAYER_RESUME"

    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 269
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 271
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/a;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 274
    iget-object v1, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v2, p0, Lcom/sec/android/allshare/a;->h:Lcom/sec/android/allshare/e;

    invoke-interface {v1, v0, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    goto :goto_0
.end method

.method public final seek(J)V
    .locals 5

    .prologue
    .line 219
    iget-object v0, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 221
    :cond_0
    const-string v0, "AVPlayerImpl"

    const-string v1, "seek fail : SERVICE_NOT_CONNECTED"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    iget-object v0, p0, Lcom/sec/android/allshare/a;->e:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p1, p2, v1}, Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;->onSeekResponseReceived(JLcom/sec/android/allshare/ERROR;)V

    .line 236
    :goto_0
    return-void

    .line 225
    :cond_1
    const-string v0, "AVPlayerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "seek pos :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/allshare/a;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 227
    const-string v1, "com.sec.android.allshare.action.ACTION_AV_PLAYER_SEEK"

    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 228
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 230
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/a;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p1

    .line 232
    const-string v4, "BUNDLE_LONG_POSITION"

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 234
    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 235
    iget-object v1, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v2, p0, Lcom/sec/android/allshare/a;->h:Lcom/sec/android/allshare/e;

    invoke-interface {v1, v0, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    goto :goto_0
.end method

.method public final setEventListener(Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerEventListener;)V
    .locals 0

    .prologue
    .line 436
    iput-object p1, p0, Lcom/sec/android/allshare/a;->d:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerEventListener;

    .line 437
    return-void
.end method

.method public final setMute(Z)V
    .locals 4

    .prologue
    .line 345
    iget-object v0, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 347
    :cond_0
    const-string v0, "AVPlayerImpl"

    const-string v1, "setMute fail : SERVICE_NOT_CONNECTED"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    iget-object v0, p0, Lcom/sec/android/allshare/a;->f:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p1, v1}, Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;->onSetMuteResponseReceived(ZLcom/sec/android/allshare/ERROR;)V

    .line 362
    :goto_0
    return-void

    .line 352
    :cond_1
    const-string v0, "AVPlayerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setMute - "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/allshare/a;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 354
    const-string v1, "com.sec.android.allshare.action.ACTION_AV_PLAYER_REQUEST_SET_MUTE"

    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 355
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 357
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/a;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    const-string v2, "BUNDLE_BOOLEAN_MUTE"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 360
    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 361
    iget-object v1, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v2, p0, Lcom/sec/android/allshare/a;->h:Lcom/sec/android/allshare/e;

    invoke-interface {v1, v0, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    goto :goto_0
.end method

.method public final setResponseListener(Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;)V
    .locals 0

    .prologue
    .line 423
    iput-object p1, p0, Lcom/sec/android/allshare/a;->e:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    .line 424
    return-void
.end method

.method public final setResponseListener(Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;)V
    .locals 0

    .prologue
    .line 429
    iput-object p1, p0, Lcom/sec/android/allshare/a;->f:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;

    .line 431
    return-void
.end method

.method public final setVolume(I)V
    .locals 4

    .prologue
    .line 299
    iget-object v0, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 301
    :cond_0
    const-string v0, "AVPlayerImpl"

    const-string v1, "setVolume fail : SERVICE_NOT_CONNECTED"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    iget-object v0, p0, Lcom/sec/android/allshare/a;->f:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p1, v1}, Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;->onSetVolumeResponseReceived(ILcom/sec/android/allshare/ERROR;)V

    .line 322
    :goto_0
    return-void

    .line 305
    :cond_1
    if-ltz p1, :cond_2

    const/16 v0, 0x64

    if-le p1, v0, :cond_3

    .line 307
    :cond_2
    const-string v0, "AVPlayerImpl"

    const-string v1, "setVolume fail : level (INVALID_ARGUMENT)"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    iget-object v0, p0, Lcom/sec/android/allshare/a;->f:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p1, v1}, Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;->onSetVolumeResponseReceived(ILcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 312
    :cond_3
    const-string v0, "AVPlayerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setVolume -level : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/allshare/a;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 314
    const-string v1, "com.sec.android.allshare.action.ACTION_AV_PLAYER_REQUEST_SET_VOLUME"

    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 315
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 317
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/a;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    const-string v2, "BUNDLE_INT_VOLUME"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 320
    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 321
    iget-object v1, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v2, p0, Lcom/sec/android/allshare/a;->h:Lcom/sec/android/allshare/e;

    invoke-interface {v1, v0, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    goto :goto_0
.end method

.method public final stop()V
    .locals 4

    .prologue
    .line 199
    iget-object v0, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 201
    :cond_0
    const-string v0, "AVPlayerImpl"

    const-string v1, "stop fail : SERVICE_NOT_CONNECTED"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    iget-object v0, p0, Lcom/sec/android/allshare/a;->e:Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1}, Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;->onStopResponseReceived(Lcom/sec/android/allshare/ERROR;)V

    .line 214
    :goto_0
    return-void

    .line 205
    :cond_1
    const-string v0, "AVPlayerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "stop : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/allshare/a;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 207
    const-string v1, "com.sec.android.allshare.action.ACTION_AV_PLAYER_STOP"

    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 208
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 210
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/a;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 213
    iget-object v1, p0, Lcom/sec/android/allshare/a;->c:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v2, p0, Lcom/sec/android/allshare/a;->h:Lcom/sec/android/allshare/e;

    invoke-interface {v1, v0, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    goto :goto_0
.end method
