.class final Lcom/sec/android/allshare/ad;
.super Lcom/sec/android/allshare/d;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/allshare/ac;

.field private b:Ljava/util/HashMap;


# direct methods
.method constructor <init>(Lcom/sec/android/allshare/ac;Landroid/os/Looper;)V
    .locals 3

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/allshare/ad;->a:Lcom/sec/android/allshare/ac;

    .line 72
    invoke-direct {p0, p2}, Lcom/sec/android/allshare/d;-><init>(Landroid/os/Looper;)V

    .line 75
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/allshare/ad;->b:Ljava/util/HashMap;

    .line 77
    iget-object v0, p0, Lcom/sec/android/allshare/ad;->b:Ljava/util/HashMap;

    const-string v1, "com.sec.android.allshare.event.EVENT_RENDERER_STATE_BUFFERING"

    sget-object v2, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;->BUFFERING:Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    iget-object v0, p0, Lcom/sec/android/allshare/ad;->b:Ljava/util/HashMap;

    const-string v1, "com.sec.android.allshare.event.EVENT_RENDERER_STATE_NOMEDIA"

    sget-object v2, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;->STOPPED:Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    iget-object v0, p0, Lcom/sec/android/allshare/ad;->b:Ljava/util/HashMap;

    const-string v1, "com.sec.android.allshare.event.EVENT_RENDERER_STATE_PAUSED"

    sget-object v2, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;->SHOWING:Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    iget-object v0, p0, Lcom/sec/android/allshare/ad;->b:Ljava/util/HashMap;

    const-string v1, "com.sec.android.allshare.event.EVENT_RENDERER_STATE_STOPPED"

    sget-object v2, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;->STOPPED:Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    iget-object v0, p0, Lcom/sec/android/allshare/ad;->b:Ljava/util/HashMap;

    const-string v1, "com.sec.android.allshare.event.EVENT_RENDERER_STATE_PLAYING"

    sget-object v2, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;->SHOWING:Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    iget-object v0, p0, Lcom/sec/android/allshare/ad;->b:Ljava/util/HashMap;

    const-string v1, "com.sec.android.allshare.event.EVENT_RENDERER_STATE_CONTENT_CHANGED"

    sget-object v2, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;->CONTENT_CHANGED:Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private a(Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;Lcom/sec/android/allshare/ERROR;)V
    .locals 3

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/allshare/ad;->a:Lcom/sec/android/allshare/ac;

    invoke-static {v0}, Lcom/sec/android/allshare/ac;->a(Lcom/sec/android/allshare/ac;)Lcom/sec/android/allshare/media/ImageViewer$IImageViewerEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 138
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/allshare/ad;->a:Lcom/sec/android/allshare/ac;

    invoke-static {v0}, Lcom/sec/android/allshare/ac;->a(Lcom/sec/android/allshare/ac;)Lcom/sec/android/allshare/media/ImageViewer$IImageViewerEventListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/sec/android/allshare/media/ImageViewer$IImageViewerEventListener;->onDeviceChanged(Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;Lcom/sec/android/allshare/ERROR;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    .line 150
    :cond_0
    :goto_0
    return-void

    .line 140
    :catch_0
    move-exception v0

    .line 142
    const-string v1, "ImageViewerImpl"

    const-string v2, "mEventHandler.notifyEvent Exception"

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 144
    :catch_1
    move-exception v0

    .line 146
    const-string v1, "ImageViewerImpl"

    const-string v2, "mEventHandler.notifyEvent Error"

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Error;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 88
    sget-object v0, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;->UNKNOWN:Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    .line 89
    sget-object v0, Lcom/sec/android/allshare/ERROR;->FAIL:Lcom/sec/android/allshare/ERROR;

    .line 92
    :try_start_0
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v4

    .line 93
    iget-object v0, p0, Lcom/sec/android/allshare/ad;->b:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    .line 94
    const-string v2, "BUNDLE_ENUM_ERROR"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 95
    if-nez v2, :cond_1

    .line 96
    sget-object v2, Lcom/sec/android/allshare/ERROR;->FAIL:Lcom/sec/android/allshare/ERROR;

    move-object v3, v2

    .line 100
    :goto_0
    if-nez v0, :cond_6

    .line 102
    sget-object v0, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;->UNKNOWN:Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    move-object v2, v0

    .line 105
    :goto_1
    sget-object v0, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;->CONTENT_CHANGED:Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    invoke-virtual {v2, v0}, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 107
    iget-object v0, p0, Lcom/sec/android/allshare/ad;->a:Lcom/sec/android/allshare/ac;

    iget-object v0, v0, Lcom/sec/android/allshare/ac;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    const-string v0, "BUNDLE_STRING_APP_ITEM_ID"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_2

    move v0, v1

    :goto_2
    if-nez v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/sec/android/allshare/ad;->a:Lcom/sec/android/allshare/ac;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/sec/android/allshare/ac;->a:Ljava/util/ArrayList;

    .line 110
    invoke-direct {p0, v2, v3}, Lcom/sec/android/allshare/ad;->a(Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;Lcom/sec/android/allshare/ERROR;)V

    .line 130
    :cond_0
    :goto_3
    return-void

    .line 98
    :cond_1
    invoke-static {v2}, Lcom/sec/android/allshare/ERROR;->valueOf(Ljava/lang/String;)Lcom/sec/android/allshare/ERROR;

    move-result-object v2

    move-object v3, v2

    goto :goto_0

    .line 107
    :cond_2
    iget-object v0, p0, Lcom/sec/android/allshare/ad;->a:Lcom/sec/android/allshare/ac;

    iget-object v0, v0, Lcom/sec/android/allshare/ac;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    goto :goto_2

    :cond_4
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    goto :goto_2

    .line 115
    :cond_5
    invoke-direct {p0, v2, v3}, Lcom/sec/android/allshare/ad;->a(Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;Lcom/sec/android/allshare/ERROR;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_3

    .line 120
    :catch_0
    move-exception v0

    const-string v0, "ImageViewerImpl"

    const-string v1, "mEventHandler.handleEventMessage Fail to notify event : NullPointerException"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 124
    :catch_1
    move-exception v0

    const-string v0, "ImageViewerImpl"

    const-string v1, "mEventHandler.handleEventMessage Fail to notify event"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 126
    :catch_2
    move-exception v0

    .line 128
    const-string v1, "ImageViewerImpl"

    const-string v2, "mEventHandler.handleEventMessage Error"

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Error;)V

    goto :goto_3

    :cond_6
    move-object v2, v0

    goto :goto_1
.end method
