.class final Lcom/sec/android/allshare/au;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Lcom/sec/android/allshare/at;


# direct methods
.method constructor <init>(Lcom/sec/android/allshare/at;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/allshare/au;->a:Lcom/sec/android/allshare/at;

    .line 606
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3

    .prologue
    .line 612
    const-string v0, "AllShareConnector"

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/sec/android/allshare/au;->a:Lcom/sec/android/allshare/at;

    invoke-static {v2}, Lcom/sec/android/allshare/at;->a(Lcom/sec/android/allshare/at;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "Subscriber Connected"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    iget-object v0, p0, Lcom/sec/android/allshare/au;->a:Lcom/sec/android/allshare/at;

    invoke-static {p2}, Lcom/sec/android/allshare/iface/ISubscriber$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/android/allshare/iface/ISubscriber;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/at;->a(Lcom/sec/android/allshare/at;Lcom/sec/android/allshare/iface/ISubscriber;)V

    .line 614
    iget-object v0, p0, Lcom/sec/android/allshare/au;->a:Lcom/sec/android/allshare/at;

    invoke-static {v0, p1}, Lcom/sec/android/allshare/at;->a(Lcom/sec/android/allshare/at;Landroid/content/ComponentName;)V

    .line 615
    iget-object v0, p0, Lcom/sec/android/allshare/au;->a:Lcom/sec/android/allshare/at;

    invoke-static {v0}, Lcom/sec/android/allshare/at;->b(Lcom/sec/android/allshare/at;)V

    .line 617
    return-void
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 622
    const-string v0, "AllShareConnector"

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/sec/android/allshare/au;->a:Lcom/sec/android/allshare/at;

    invoke-static {v2}, Lcom/sec/android/allshare/at;->a(Lcom/sec/android/allshare/at;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "Subscriber Disconnedted"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 623
    iget-object v0, p0, Lcom/sec/android/allshare/au;->a:Lcom/sec/android/allshare/at;

    invoke-static {v0, v3}, Lcom/sec/android/allshare/at;->a(Lcom/sec/android/allshare/at;Lcom/sec/android/allshare/iface/ISubscriber;)V

    .line 624
    iget-object v0, p0, Lcom/sec/android/allshare/au;->a:Lcom/sec/android/allshare/at;

    invoke-static {v0, v3}, Lcom/sec/android/allshare/at;->a(Lcom/sec/android/allshare/at;Landroid/content/ComponentName;)V

    .line 625
    iget-object v0, p0, Lcom/sec/android/allshare/au;->a:Lcom/sec/android/allshare/at;

    invoke-static {v0}, Lcom/sec/android/allshare/at;->c(Lcom/sec/android/allshare/at;)V

    .line 627
    return-void
.end method
