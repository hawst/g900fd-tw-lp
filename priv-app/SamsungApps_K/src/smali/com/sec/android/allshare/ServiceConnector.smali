.class public Lcom/sec/android/allshare/ServiceConnector;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static a:Ljava/lang/ref/WeakReference;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/allshare/ServiceConnector;->a:Ljava/lang/ref/WeakReference;

    .line 50
    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    return-void
.end method

.method static a()Landroid/content/Context;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 122
    sget-object v0, Lcom/sec/android/allshare/ServiceConnector;->a:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_1

    move-object v0, v1

    .line 129
    :cond_0
    :goto_0
    return-object v0

    .line 125
    :cond_1
    sget-object v0, Lcom/sec/android/allshare/ServiceConnector;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 126
    if-nez v0, :cond_0

    move-object v0, v1

    .line 127
    goto :goto_0
.end method

.method static b()Landroid/os/Looper;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 280
    sget-object v0, Lcom/sec/android/allshare/ServiceConnector;->a:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    move-object v0, v1

    .line 286
    :goto_0
    return-object v0

    .line 282
    :cond_0
    sget-object v0, Lcom/sec/android/allshare/ServiceConnector;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 283
    if-nez v0, :cond_1

    move-object v0, v1

    .line 284
    goto :goto_0

    .line 286
    :cond_1
    invoke-virtual {v0}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    goto :goto_0
.end method

.method public static createServiceProvider(Landroid/content/Context;Lcom/sec/android/allshare/ServiceConnector$IServiceConnectEventListener;)Lcom/sec/android/allshare/ERROR;
    .locals 4

    .prologue
    .line 156
    invoke-static {}, Lcom/sec/android/allshare/DLog;->a()V

    .line 158
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 160
    :cond_0
    const-string v0, "ServiceConnector"

    const-string v1, "Context or Event listener is null"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    sget-object v0, Lcom/sec/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/sec/android/allshare/ERROR;

    .line 238
    :goto_0
    return-object v0

    .line 165
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 168
    :try_start_0
    const-string v1, "com.sec.android.allshare.framework"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 169
    const-string v1, "ServiceConnector"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Framework Version Name : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 177
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/sec/android/allshare/ServiceConnector;->a:Ljava/lang/ref/WeakReference;

    .line 179
    new-instance v0, Lcom/sec/android/allshare/at;

    invoke-direct {v0, p0}, Lcom/sec/android/allshare/at;-><init>(Landroid/content/Context;)V

    .line 181
    new-instance v1, Lcom/sec/android/allshare/as;

    invoke-direct {v1, p1, p0, v0}, Lcom/sec/android/allshare/as;-><init>(Lcom/sec/android/allshare/ServiceConnector$IServiceConnectEventListener;Landroid/content/Context;Lcom/sec/android/allshare/at;)V

    .line 235
    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/at;->a(Landroid/os/Handler$Callback;)V

    .line 236
    invoke-virtual {v0}, Lcom/sec/android/allshare/at;->c()V

    .line 238
    sget-object v0, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    goto :goto_0

    .line 173
    :catch_0
    move-exception v0

    const-string v0, "ServiceConnector"

    const-string v1, "Couldn\'t find AllShare Framework "

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    sget-object v0, Lcom/sec/android/allshare/ERROR;->FRAMEWORK_NOT_INSTALLED:Lcom/sec/android/allshare/ERROR;

    goto :goto_0
.end method

.method public static deleteServiceProvider(Lcom/sec/android/allshare/ServiceProvider;)V
    .locals 6

    .prologue
    .line 253
    if-nez p0, :cond_0

    .line 276
    :goto_0
    return-void

    :cond_0
    move-object v0, p0

    .line 256
    check-cast v0, Lcom/sec/android/allshare/ax;

    iget-object v1, v0, Lcom/sec/android/allshare/ax;->d:Lcom/sec/android/allshare/at;

    .line 259
    invoke-static {v1}, Lcom/sec/android/allshare/at;->d(Lcom/sec/android/allshare/at;)Ljava/util/HashSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashSet;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    .line 260
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 261
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 269
    invoke-virtual {p0}, Lcom/sec/android/allshare/ServiceProvider;->getScreenCastManager()Lcom/sec/android/allshare/screen/ScreenCastManager;

    move-result-object v0

    check-cast v0, Lcom/sec/android/allshare/aq;

    .line 270
    if-eqz v0, :cond_1

    .line 271
    invoke-virtual {v0}, Lcom/sec/android/allshare/aq;->a()V

    .line 273
    :cond_1
    invoke-virtual {v1}, Lcom/sec/android/allshare/at;->d()V

    .line 274
    invoke-virtual {v1}, Lcom/sec/android/allshare/at;->e()V

    goto :goto_0

    .line 263
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/allshare/aw;

    .line 264
    iget-object v3, v0, Lcom/sec/android/allshare/aw;->a:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/android/allshare/aw;->b:Landroid/os/Bundle;

    iget-object v5, v0, Lcom/sec/android/allshare/aw;->c:Lcom/sec/android/allshare/d;

    invoke-virtual {v1, v3, v4, v5}, Lcom/sec/android/allshare/at;->b(Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/allshare/d;)V

    .line 265
    invoke-static {v1}, Lcom/sec/android/allshare/at;->d(Lcom/sec/android/allshare/at;)Ljava/util/HashSet;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_1
.end method
