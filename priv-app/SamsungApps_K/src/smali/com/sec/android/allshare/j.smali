.class final Lcom/sec/android/allshare/j;
.super Lcom/sec/android/allshare/Device;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/allshare/iface/IBundleHolder;


# instance fields
.field private a:Landroid/os/Bundle;


# direct methods
.method constructor <init>(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/android/allshare/Device;-><init>()V

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/allshare/j;->a:Landroid/os/Bundle;

    .line 42
    iput-object p1, p0, Lcom/sec/android/allshare/j;->a:Landroid/os/Bundle;

    .line 43
    return-void
.end method


# virtual methods
.method public final getBundle()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/android/allshare/j;->a:Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 140
    const/4 v0, 0x0

    .line 142
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/j;->a:Landroid/os/Bundle;

    goto :goto_0
.end method

.method public final getDeviceDomain()Lcom/sec/android/allshare/Device$DeviceDomain;
    .locals 4

    .prologue
    .line 117
    sget-object v1, Lcom/sec/android/allshare/Device$DeviceDomain;->UNKNOWN:Lcom/sec/android/allshare/Device$DeviceDomain;

    .line 120
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/allshare/j;->a:Landroid/os/Bundle;

    const-string v2, "BUNDLE_ENUM_DEVICE_DOMAIN"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/allshare/Device$DeviceDomain;->valueOf(Ljava/lang/String;)Lcom/sec/android/allshare/Device$DeviceDomain;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 127
    :goto_0
    if-nez v0, :cond_0

    .line 129
    sget-object v0, Lcom/sec/android/allshare/Device$DeviceDomain;->UNKNOWN:Lcom/sec/android/allshare/Device$DeviceDomain;

    .line 132
    :cond_0
    return-object v0

    .line 122
    :catch_0
    move-exception v0

    .line 124
    const-string v2, "DeviceImpl"

    const-string v3, "getDeviceDomain Exception"

    invoke-static {v2, v3, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public final getDeviceType()Lcom/sec/android/allshare/Device$DeviceType;
    .locals 4

    .prologue
    .line 91
    sget-object v1, Lcom/sec/android/allshare/Device$DeviceType;->UNKNOWN:Lcom/sec/android/allshare/Device$DeviceType;

    .line 94
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/allshare/j;->a:Landroid/os/Bundle;

    const-string v2, "BUNDLE_ENUM_DEVICE_TYPE"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/allshare/Device$DeviceType;->valueOf(Ljava/lang/String;)Lcom/sec/android/allshare/Device$DeviceType;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 101
    :goto_0
    if-nez v0, :cond_0

    .line 103
    sget-object v0, Lcom/sec/android/allshare/Device$DeviceType;->UNKNOWN:Lcom/sec/android/allshare/Device$DeviceType;

    .line 105
    :cond_0
    return-object v0

    .line 96
    :catch_0
    move-exception v0

    .line 98
    const-string v2, "DeviceImpl"

    const-string v3, "getDeviceType  Exception"

    invoke-static {v2, v3, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public final getID()Ljava/lang/String;
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/allshare/j;->a:Landroid/os/Bundle;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/j;->a:Landroid/os/Bundle;

    const-string v1, "BUNDLE_STRING_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getIPAdress()Ljava/lang/String;
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/allshare/j;->a:Landroid/os/Bundle;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/j;->a:Landroid/os/Bundle;

    const-string v1, "BUNDLE_STRING_DEVICE_IP_ADDRESS"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getIcon()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/allshare/j;->a:Landroid/os/Bundle;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    check-cast v0, Landroid/net/Uri;

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/j;->a:Landroid/os/Bundle;

    const-string v1, "BUNDLE_PARCELABLE_DEVICE_DEFAULT_ICON"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    goto :goto_0
.end method

.method public final getIconList()Ljava/util/ArrayList;
    .locals 4

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/allshare/j;->a:Landroid/os/Bundle;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 61
    :goto_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 63
    if-nez v0, :cond_1

    move-object v0, v1

    .line 73
    :goto_1
    return-object v0

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/j;->a:Landroid/os/Bundle;

    const-string v1, "BUNDLE_PARCELABLE_DEVICE_DEFAULT_ICONLIST"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0

    .line 68
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    move-object v0, v1

    .line 73
    goto :goto_1

    .line 68
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    .line 70
    new-instance v3, Lcom/sec/android/allshare/aa;

    check-cast v0, Landroid/os/Bundle;

    invoke-direct {v3, v0}, Lcom/sec/android/allshare/aa;-><init>(Landroid/os/Bundle;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method public final getModelName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/allshare/j;->a:Landroid/os/Bundle;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/j;->a:Landroid/os/Bundle;

    const-string v1, "BUNDLE_STRING_DEVICE_MODELNAME"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getNIC()Ljava/lang/String;
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/allshare/j;->a:Landroid/os/Bundle;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/j;->a:Landroid/os/Bundle;

    const-string v1, "BUNDLE_STRING_BOUND_INTERFACE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/allshare/j;->a:Landroid/os/Bundle;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/j;->a:Landroid/os/Bundle;

    const-string v1, "BUNDLE_STRING_DEVICE_NAME"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
