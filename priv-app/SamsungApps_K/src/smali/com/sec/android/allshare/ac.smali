.class final Lcom/sec/android/allshare/ac;
.super Lcom/sec/android/allshare/media/ImageViewer;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/allshare/iface/IBundleHolder;
.implements Lcom/sec/android/allshare/iface/IHandlerHolder;


# static fields
.field private static synthetic i:[I


# instance fields
.field a:Ljava/util/ArrayList;

.field b:Lcom/sec/android/allshare/d;

.field c:Lcom/sec/android/allshare/e;

.field private d:Lcom/sec/android/allshare/IAllShareConnector;

.field private e:Lcom/sec/android/allshare/j;

.field private f:Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;

.field private g:Lcom/sec/android/allshare/media/ImageViewer$IImageViewerEventListener;

.field private h:Lcom/sec/android/allshare/media/ViewController;


# direct methods
.method constructor <init>(Lcom/sec/android/allshare/IAllShareConnector;Lcom/sec/android/allshare/j;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 57
    invoke-direct {p0}, Lcom/sec/android/allshare/media/ImageViewer;-><init>()V

    .line 48
    iput-object v0, p0, Lcom/sec/android/allshare/ac;->d:Lcom/sec/android/allshare/IAllShareConnector;

    .line 49
    iput-object v0, p0, Lcom/sec/android/allshare/ac;->e:Lcom/sec/android/allshare/j;

    .line 50
    iput-object v0, p0, Lcom/sec/android/allshare/ac;->f:Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    .line 51
    iput-object v0, p0, Lcom/sec/android/allshare/ac;->g:Lcom/sec/android/allshare/media/ImageViewer$IImageViewerEventListener;

    .line 53
    iput-object v0, p0, Lcom/sec/android/allshare/ac;->h:Lcom/sec/android/allshare/media/ViewController;

    .line 55
    iput-object v0, p0, Lcom/sec/android/allshare/ac;->a:Ljava/util/ArrayList;

    .line 72
    new-instance v0, Lcom/sec/android/allshare/ad;

    invoke-static {}, Lcom/sec/android/allshare/ServiceConnector;->b()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/allshare/ad;-><init>(Lcom/sec/android/allshare/ac;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/allshare/ac;->b:Lcom/sec/android/allshare/d;

    .line 167
    new-instance v0, Lcom/sec/android/allshare/ae;

    invoke-static {}, Lcom/sec/android/allshare/ServiceConnector;->b()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/allshare/ae;-><init>(Lcom/sec/android/allshare/ac;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/allshare/ac;->c:Lcom/sec/android/allshare/e;

    .line 60
    if-nez p1, :cond_0

    .line 62
    const-string v0, "ImageViewerImpl"

    const-string v1, "Connection FAIL: AllShare Service Connector does not exist"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    :goto_0
    return-void

    .line 66
    :cond_0
    iput-object p2, p0, Lcom/sec/android/allshare/ac;->e:Lcom/sec/android/allshare/j;

    .line 67
    iput-object p1, p0, Lcom/sec/android/allshare/ac;->d:Lcom/sec/android/allshare/IAllShareConnector;

    .line 69
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->d:Lcom/sec/android/allshare/IAllShareConnector;

    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    invoke-virtual {p2}, Lcom/sec/android/allshare/j;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/allshare/ac;->b:Lcom/sec/android/allshare/d;

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/android/allshare/IAllShareConnector;->a(Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/allshare/d;)Z

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/allshare/ac;)Lcom/sec/android/allshare/media/ImageViewer$IImageViewerEventListener;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->g:Lcom/sec/android/allshare/media/ImageViewer$IImageViewerEventListener;

    return-object v0
.end method

.method private static synthetic a()[I
    .locals 3

    .prologue
    .line 44
    sget-object v0, Lcom/sec/android/allshare/ac;->i:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/allshare/Item$MediaType;->values()[Lcom/sec/android/allshare/Item$MediaType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/allshare/Item$MediaType;->ITEM_AUDIO:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/allshare/Item$MediaType;->ITEM_FOLDER:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/android/allshare/Item$MediaType;->ITEM_IMAGE:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    :try_start_3
    sget-object v1, Lcom/sec/android/allshare/Item$MediaType;->ITEM_UNKNOWN:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_4
    :try_start_4
    sget-object v1, Lcom/sec/android/allshare/Item$MediaType;->ITEM_VIDEO:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_5
    sput-object v0, Lcom/sec/android/allshare/ac;->i:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_5

    :catch_1
    move-exception v1

    goto :goto_4

    :catch_2
    move-exception v1

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_1
.end method

.method static synthetic b(Lcom/sec/android/allshare/ac;)Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->f:Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    return-object v0
.end method


# virtual methods
.method public final getBundle()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 655
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->e:Lcom/sec/android/allshare/j;

    if-nez v0, :cond_0

    .line 657
    const/4 v0, 0x0

    .line 659
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->e:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method public final getDeviceDomain()Lcom/sec/android/allshare/Device$DeviceDomain;
    .locals 1

    .prologue
    .line 574
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->e:Lcom/sec/android/allshare/j;

    if-nez v0, :cond_0

    .line 576
    sget-object v0, Lcom/sec/android/allshare/Device$DeviceDomain;->UNKNOWN:Lcom/sec/android/allshare/Device$DeviceDomain;

    .line 578
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->e:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getDeviceDomain()Lcom/sec/android/allshare/Device$DeviceDomain;

    move-result-object v0

    goto :goto_0
.end method

.method public final getDeviceType()Lcom/sec/android/allshare/Device$DeviceType;
    .locals 1

    .prologue
    .line 624
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->e:Lcom/sec/android/allshare/j;

    if-nez v0, :cond_0

    .line 626
    sget-object v0, Lcom/sec/android/allshare/Device$DeviceType;->UNKNOWN:Lcom/sec/android/allshare/Device$DeviceType;

    .line 629
    :goto_0
    return-object v0

    .line 628
    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->e:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getDeviceType()Lcom/sec/android/allshare/Device$DeviceType;

    move-result-object v0

    goto :goto_0
.end method

.method public final getID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 584
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->e:Lcom/sec/android/allshare/j;

    if-nez v0, :cond_0

    .line 586
    const-string v0, ""

    .line 588
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->e:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getID()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getIPAdress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 645
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->e:Lcom/sec/android/allshare/j;

    if-nez v0, :cond_0

    .line 647
    const-string v0, ""

    .line 649
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->e:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getIPAdress()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getIcon()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 594
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->e:Lcom/sec/android/allshare/j;

    if-nez v0, :cond_0

    .line 596
    const/4 v0, 0x0

    .line 598
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->e:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getIcon()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public final getIconList()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 604
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->e:Lcom/sec/android/allshare/j;

    if-nez v0, :cond_0

    .line 606
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 608
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->e:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getIconList()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public final getModelName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 635
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->e:Lcom/sec/android/allshare/j;

    if-nez v0, :cond_0

    .line 637
    const-string v0, ""

    .line 639
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->e:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getModelName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getNIC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->e:Lcom/sec/android/allshare/j;

    if-nez v0, :cond_0

    .line 237
    const-string v0, ""

    .line 239
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->e:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getNIC()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 614
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->e:Lcom/sec/android/allshare/j;

    if-nez v0, :cond_0

    .line 616
    const-string v0, ""

    .line 618
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->e:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getState()V
    .locals 4

    .prologue
    .line 729
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->d:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/ac;->d:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 731
    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->f:Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    sget-object v1, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;->UNKNOWN:Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    sget-object v2, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onGetStateResponseReceived(Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;Lcom/sec/android/allshare/ERROR;)V

    .line 743
    :goto_0
    return-void

    .line 735
    :cond_1
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 736
    const-string v1, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_REQUEST_GET_VIEWER_STATE"

    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 738
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 739
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/ac;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 740
    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 742
    iget-object v1, p0, Lcom/sec/android/allshare/ac;->d:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v2, p0, Lcom/sec/android/allshare/ac;->c:Lcom/sec/android/allshare/e;

    invoke-interface {v1, v0, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    goto :goto_0
.end method

.method public final getViewController()Lcom/sec/android/allshare/media/ViewController;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 665
    iget-object v1, p0, Lcom/sec/android/allshare/ac;->d:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/allshare/ac;->d:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v1}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 723
    :cond_0
    :goto_0
    return-object v0

    .line 668
    :cond_1
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 669
    invoke-virtual {p0}, Lcom/sec/android/allshare/ac;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    .line 670
    if-nez v2, :cond_2

    .line 672
    const-string v1, "ImageViewerImpl"

    const-string v2, "getViewController : bundle is Null"

    invoke-static {v1, v2}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 676
    :cond_2
    const-string v3, "BUNDLE_STRING_DEVICE_ID"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 678
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_4

    .line 680
    :cond_3
    const-string v1, "ImageViewerImpl"

    const-string v2, "getViewController : deviceId is Null"

    invoke-static {v1, v2}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 684
    :cond_4
    const-string v3, "BUNDLE_STRING_DEVICE_ID"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 686
    new-instance v2, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v2}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 687
    const-string v3, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SET_VIEW_CONTROLLER_SYNC"

    invoke-virtual {v2, v3}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 688
    invoke-virtual {v2, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 690
    iget-object v1, p0, Lcom/sec/android/allshare/ac;->d:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v1, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v1

    .line 692
    if-nez v1, :cond_5

    .line 694
    const-string v1, "ImageViewerImpl"

    const-string v2, "res_message is Null"

    invoke-static {v1, v2}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 698
    :cond_5
    invoke-virtual {v1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v1

    .line 700
    if-nez v1, :cond_6

    .line 702
    const-string v1, "ImageViewerImpl"

    const-string v2, "res_bundle is Null"

    invoke-static {v1, v2}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 706
    :cond_6
    const-string v2, "BUNDLE_INT_TV_WIDTH_RESOLUTION"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 707
    const-string v3, "BUNDLE_INT_TV_HEIGHT_RESOLUTION"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 709
    const-string v4, "BUNDLE_BOOLEAN_ZOOMABLE"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    .line 710
    const-string v5, "BUNDLE_BOOLEAN_ROTATABLE"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 712
    if-eqz v4, :cond_0

    if-eqz v1, :cond_0

    .line 720
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->h:Lcom/sec/android/allshare/media/ViewController;

    if-nez v0, :cond_7

    .line 721
    new-instance v0, Lcom/sec/android/allshare/bg;

    iget-object v1, p0, Lcom/sec/android/allshare/ac;->d:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v4, p0, Lcom/sec/android/allshare/ac;->e:Lcom/sec/android/allshare/j;

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/sec/android/allshare/bg;-><init>(Lcom/sec/android/allshare/IAllShareConnector;Lcom/sec/android/allshare/j;II)V

    iput-object v0, p0, Lcom/sec/android/allshare/ac;->h:Lcom/sec/android/allshare/media/ViewController;

    .line 723
    :cond_7
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->h:Lcom/sec/android/allshare/media/ViewController;

    goto/16 :goto_0
.end method

.method public final getViewerState()Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;
    .locals 4

    .prologue
    .line 520
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->d:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/ac;->d:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 521
    :cond_0
    sget-object v0, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;->UNKNOWN:Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    .line 555
    :goto_0
    return-object v0

    .line 523
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 524
    invoke-virtual {p0}, Lcom/sec/android/allshare/ac;->getBundle()Landroid/os/Bundle;

    move-result-object v1

    .line 525
    if-nez v1, :cond_2

    .line 526
    sget-object v0, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;->UNKNOWN:Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    goto :goto_0

    .line 527
    :cond_2
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 528
    if-nez v1, :cond_3

    .line 529
    sget-object v0, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;->UNKNOWN:Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    goto :goto_0

    .line 531
    :cond_3
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 533
    new-instance v1, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v1}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 534
    const-string v2, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_GET_VIEWER_STATE_SYNC"

    invoke-virtual {v1, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 535
    invoke-virtual {v1, v0}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 537
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->d:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0, v1}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;)Lcom/sec/android/allshare/iface/CVMessage;

    move-result-object v0

    .line 539
    if-nez v0, :cond_4

    .line 540
    sget-object v0, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;->UNKNOWN:Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    goto :goto_0

    .line 542
    :cond_4
    invoke-virtual {v0}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 544
    if-nez v0, :cond_5

    .line 545
    sget-object v0, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;->UNKNOWN:Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    goto :goto_0

    .line 549
    :cond_5
    :try_start_0
    const-string v1, "BUNDLE_STRING_IMAGE_VIEWEW_STATE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;->valueOf(Ljava/lang/String;)Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 551
    :catch_0
    move-exception v0

    .line 553
    const-string v1, "ImageViewerImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getViewerState Exception : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 555
    sget-object v0, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;->UNKNOWN:Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    goto :goto_0
.end method

.method public final removeEventHandler()V
    .locals 4

    .prologue
    .line 748
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->d:Lcom/sec/android/allshare/IAllShareConnector;

    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_UNSUBSCRIBE"

    invoke-virtual {p0}, Lcom/sec/android/allshare/ac;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/allshare/ac;->b:Lcom/sec/android/allshare/d;

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/android/allshare/IAllShareConnector;->b(Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/allshare/d;)V

    .line 749
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->h:Lcom/sec/android/allshare/media/ViewController;

    if-eqz v0, :cond_0

    .line 751
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->h:Lcom/sec/android/allshare/media/ViewController;

    check-cast v0, Lcom/sec/android/allshare/bg;

    invoke-virtual {v0}, Lcom/sec/android/allshare/bg;->a()V

    .line 753
    :cond_0
    return-void
.end method

.method public final setEventListener(Lcom/sec/android/allshare/media/ImageViewer$IImageViewerEventListener;)V
    .locals 0

    .prologue
    .line 567
    iput-object p1, p0, Lcom/sec/android/allshare/ac;->g:Lcom/sec/android/allshare/media/ImageViewer$IImageViewerEventListener;

    .line 569
    return-void
.end method

.method public final setResponseListener(Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;)V
    .locals 0

    .prologue
    .line 561
    iput-object p1, p0, Lcom/sec/android/allshare/ac;->f:Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    .line 562
    return-void
.end method

.method public final show(Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/media/ContentInfo;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 245
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->d:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/ac;->d:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 247
    :cond_0
    const-string v0, "ImageViewerImpl"

    const-string v1, "show : SERVICE_NOT_CONNECTED"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->f:Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p1, p2, v1}, Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onShowResponseReceived(Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/media/ContentInfo;Lcom/sec/android/allshare/ERROR;)V

    .line 326
    :cond_1
    :goto_0
    return-void

    .line 252
    :cond_2
    if-nez p1, :cond_3

    .line 254
    const-string v0, "ImageViewerImpl"

    const-string v1, "show item == null"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->f:Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    if-eqz v0, :cond_1

    .line 256
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->f:Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p1, p2, v1}, Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onShowResponseReceived(Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/media/ContentInfo;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 260
    :cond_3
    invoke-virtual {p1}, Lcom/sec/android/allshare/Item;->getType()Lcom/sec/android/allshare/Item$MediaType;

    move-result-object v0

    .line 262
    if-nez v0, :cond_4

    .line 264
    const-string v0, "ImageViewerImpl"

    const-string v1, "Invalid media type"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->f:Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    if-eqz v0, :cond_1

    .line 266
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->f:Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p1, p2, v1}, Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onShowResponseReceived(Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/media/ContentInfo;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 271
    :cond_4
    iput-object v2, p0, Lcom/sec/android/allshare/ac;->a:Ljava/util/ArrayList;

    .line 273
    invoke-static {}, Lcom/sec/android/allshare/ac;->a()[I

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/android/allshare/Item$MediaType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 278
    const-string v0, "ImageViewerImpl"

    const-string v1, "Invalid media type"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->f:Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    if-eqz v0, :cond_1

    .line 280
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->f:Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p1, p2, v1}, Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onShowResponseReceived(Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/media/ContentInfo;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 283
    :pswitch_0
    const-string v0, "MEDIA_SERVER"

    .line 285
    instance-of v1, p1, Lcom/sec/android/allshare/iface/IBundleHolder;

    if-eqz v1, :cond_5

    move-object v0, p1

    .line 287
    check-cast v0, Lcom/sec/android/allshare/iface/IBundleHolder;

    invoke-interface {v0}, Lcom/sec/android/allshare/iface/IBundleHolder;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 288
    const-string v1, "BUNDLE_STRING_ITEM_CONSTRUCTOR_KEY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 291
    :cond_5
    const-string v1, "MEDIA_SERVER"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 293
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->d:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/allshare/ac;->d:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_7

    :cond_6
    const-string v0, "ImageViewerImpl"

    const-string v1, "showMediaContent : SERVICE_NOT_CONNECTED"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/allshare/ac;->f:Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p1, p2, v1}, Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onShowResponseReceived(Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/media/ContentInfo;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    :cond_7
    new-instance v1, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v1}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    const-string v0, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SHOW"

    invoke-virtual {v1, v0}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v0, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/ac;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    instance-of v0, p1, Lcom/sec/android/allshare/iface/IBundleHolder;

    if-eqz v0, :cond_8

    const-string v3, "BUNDLE_PARCELABLE_ITEM"

    move-object v0, p1

    check-cast v0, Lcom/sec/android/allshare/iface/IBundleHolder;

    invoke-interface {v0}, Lcom/sec/android/allshare/iface/IBundleHolder;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_8
    const-string v0, "BUNDLE_PARCELABLE_CONTENT_INFO"

    invoke-virtual {v2, v0, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/sec/android/allshare/ac;->d:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v2, p0, Lcom/sec/android/allshare/ac;->c:Lcom/sec/android/allshare/e;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    const-string v0, "ImageViewerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "showMediaContent : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/allshare/ac;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 296
    :cond_9
    const-string v1, "WEB_CONTENT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 298
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->d:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/sec/android/allshare/ac;->d:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_b

    :cond_a
    const-string v0, "ImageViewerImpl"

    const-string v1, "showWebContent : SERVICE_NOT_CONNECTED"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/allshare/ac;->f:Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p1, p2, v1}, Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onShowResponseReceived(Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/media/ContentInfo;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    :cond_b
    if-nez p1, :cond_c

    const-string v0, "ImageViewerImpl"

    const-string v1, "showLocalContentContentScheme Fail :  Item does not exist "

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/allshare/ac;->f:Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->ITEM_NOT_EXIST:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p1, p2, v1}, Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onShowResponseReceived(Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/media/ContentInfo;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    :cond_c
    new-instance v1, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v1}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    const-string v0, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SHOW_URI"

    invoke-virtual {v1, v0}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v0, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/ac;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    instance-of v0, p1, Lcom/sec/android/allshare/iface/IBundleHolder;

    if-eqz v0, :cond_d

    const-string v3, "BUNDLE_PARCELABLE_ITEM"

    move-object v0, p1

    check-cast v0, Lcom/sec/android/allshare/iface/IBundleHolder;

    invoke-interface {v0}, Lcom/sec/android/allshare/iface/IBundleHolder;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_d
    const-string v0, "BUNDLE_PARCELABLE_CONTENT_INFO"

    invoke-virtual {v2, v0, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/sec/android/allshare/ac;->d:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v2, p0, Lcom/sec/android/allshare/ac;->c:Lcom/sec/android/allshare/e;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    const-string v0, "ImageViewerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "showWebContent : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/allshare/ac;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "ImageViewerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "showWebContent - uri : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 301
    :cond_e
    const-string v1, "LOCAL_CONTENT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 303
    invoke-virtual {p1}, Lcom/sec/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v0

    .line 305
    if-nez v0, :cond_f

    .line 307
    const-string v0, "ImageViewerImpl"

    const-string v1, "uri == null"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->f:Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    if-eqz v0, :cond_1

    .line 309
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->f:Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p1, p2, v1}, Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onShowResponseReceived(Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/media/ContentInfo;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 313
    :cond_f
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 314
    const-string v1, "content"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_18

    .line 316
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->d:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/sec/android/allshare/ac;->d:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_11

    :cond_10
    const-string v0, "ImageViewerImpl"

    const-string v1, "showLocalContentContentScheme Fail :  SERVICE_NOT_CONNECTED "

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/allshare/ac;->f:Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p1, p2, v1}, Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onShowResponseReceived(Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/media/ContentInfo;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    :cond_11
    if-nez p1, :cond_12

    const-string v0, "ImageViewerImpl"

    const-string v1, "showLocalContentContentScheme Fail :  Item does not exist "

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/allshare/ac;->f:Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->ITEM_NOT_EXIST:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p1, p2, v1}, Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onShowResponseReceived(Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/media/ContentInfo;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    :cond_12
    invoke-virtual {p1}, Lcom/sec/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v1

    if-nez v1, :cond_13

    const-string v0, "ImageViewerImpl"

    const-string v1, "showLocalContentContentScheme Fail :  uri == null "

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/allshare/ac;->f:Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p1, p2, v1}, Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onShowResponseReceived(Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/media/ContentInfo;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    :cond_13
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->d:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->a()Landroid/content/ContentResolver;

    move-result-object v0

    if-nez v0, :cond_14

    const-string v0, "ImageViewerImpl"

    const-string v1, "showLocalContentContentScheme Fail :  resolver == null "

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/allshare/ac;->f:Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p1, p2, v1}, Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onShowResponseReceived(Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/media/ContentInfo;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    :cond_14
    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_15

    const-string v0, "ImageViewerImpl"

    const-string v1, "showLocalContentContentScheme Fail :  INVALID_ARGUMENT (cur == null) "

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/allshare/ac;->f:Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p1, p2, v1}, Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onShowResponseReceived(Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/media/ContentInfo;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    :cond_15
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    const-string v2, "_data"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    if-gez v2, :cond_16

    const-string v0, "ImageViewerImpl"

    const-string v1, "showLocalContentContentScheme Fail :  INVALID_ARGUMENT(idx < 0)"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/allshare/ac;->f:Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p1, p2, v1}, Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onShowResponseReceived(Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/media/ContentInfo;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    :cond_16
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    new-instance v2, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v2}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    const-string v0, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SHOW_LOCAL_CONTENT_URI"

    invoke-virtual {v2, v0}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string v0, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/ac;->getID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    instance-of v0, p1, Lcom/sec/android/allshare/iface/IBundleHolder;

    if-eqz v0, :cond_17

    const-string v4, "BUNDLE_PARCELABLE_ITEM"

    move-object v0, p1

    check-cast v0, Lcom/sec/android/allshare/iface/IBundleHolder;

    invoke-interface {v0}, Lcom/sec/android/allshare/iface/IBundleHolder;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_17
    const-string v0, "BUNDLE_PARCELABLE_CONTENT_INFO"

    invoke-virtual {v3, v0, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/sec/android/allshare/ac;->d:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v3, p0, Lcom/sec/android/allshare/ac;->c:Lcom/sec/android/allshare/e;

    invoke-interface {v0, v2, v3}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    const-string v0, "ImageViewerImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "showLocalContentContentScheme : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/allshare/ac;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "ImageViewerImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "showLocalContentContentScheme - uri :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 318
    :cond_18
    const-string v1, "file"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 320
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->d:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/sec/android/allshare/ac;->d:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_1a

    :cond_19
    const-string v0, "ImageViewerImpl"

    const-string v1, "showLocalContentFileScheme Fail - LOCAL_CONTENT : SERVICE_NOT_CONNECTED"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/allshare/ac;->f:Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, p1, p2, v1}, Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onShowResponseReceived(Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/media/ContentInfo;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    :cond_1a
    const-string v1, ""

    const-string v0, ""

    instance-of v2, p1, Lcom/sec/android/allshare/iface/IBundleHolder;

    if-eqz v2, :cond_1b

    move-object v0, p1

    check-cast v0, Lcom/sec/android/allshare/iface/IBundleHolder;

    invoke-interface {v0}, Lcom/sec/android/allshare/iface/IBundleHolder;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "BUNDLE_STRING_FILEPATH"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "BUNDLE_STRING_ITEM_MIMETYPE"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_1b
    new-instance v2, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v2}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    const-string v3, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SHOW_LOCAL_CONTENT"

    invoke-virtual {v2, v3}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string v4, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/ac;->getID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "BUNDLE_STRING_FILEPATH"

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "BUNDLE_STRING_ITEM_MIMETYPE"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    instance-of v0, p1, Lcom/sec/android/allshare/iface/IBundleHolder;

    if-eqz v0, :cond_1c

    const-string v4, "BUNDLE_PARCELABLE_ITEM"

    move-object v0, p1

    check-cast v0, Lcom/sec/android/allshare/iface/IBundleHolder;

    invoke-interface {v0}, Lcom/sec/android/allshare/iface/IBundleHolder;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_1c
    const-string v0, "BUNDLE_PARCELABLE_CONTENT_INFO"

    invoke-virtual {v3, v0, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/sec/android/allshare/ac;->d:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v3, p0, Lcom/sec/android/allshare/ac;->c:Lcom/sec/android/allshare/e;

    invoke-interface {v0, v2, v3}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    const-string v0, "ImageViewerImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "showLocalContentFileScheme : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/allshare/ac;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "ImageViewerImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "showLocalContentFileScheme - file : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 325
    :cond_1d
    const-string v0, "ImageViewerImpl"

    const-string v1, "show fail - INVALID ARG "

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 273
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public final stop()V
    .locals 4

    .prologue
    .line 499
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->d:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/ac;->d:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 501
    :cond_0
    const-string v0, "ImageViewerImpl"

    const-string v1, "stop : SERVICE_NOT_CONNECTED"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 502
    iget-object v0, p0, Lcom/sec/android/allshare/ac;->f:Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    sget-object v1, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1}, Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onStopResponseReceived(Lcom/sec/android/allshare/ERROR;)V

    .line 515
    :goto_0
    return-void

    .line 506
    :cond_1
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 507
    const-string v1, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_STOP"

    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 508
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 510
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/ac;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 513
    iget-object v1, p0, Lcom/sec/android/allshare/ac;->d:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v2, p0, Lcom/sec/android/allshare/ac;->c:Lcom/sec/android/allshare/e;

    invoke-interface {v1, v0, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    .line 514
    const-string v0, "ImageViewerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "stop : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/allshare/ac;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
