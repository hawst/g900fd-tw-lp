.class final Lcom/sec/android/allshare/al;
.super Lcom/sec/android/allshare/d;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/allshare/ProviderImpl;


# direct methods
.method constructor <init>(Lcom/sec/android/allshare/ProviderImpl;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/allshare/al;->a:Lcom/sec/android/allshare/ProviderImpl;

    .line 130
    invoke-direct {p0, p2}, Lcom/sec/android/allshare/d;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method final a(Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 3

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/allshare/al;->a:Lcom/sec/android/allshare/ProviderImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/ProviderImpl;->a(Lcom/sec/android/allshare/ProviderImpl;)Lcom/sec/android/allshare/media/Provider$IProviderEventListener;

    move-result-object v0

    if-nez v0, :cond_1

    .line 155
    :cond_0
    :goto_0
    return-void

    .line 139
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v0

    .line 140
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v1

    .line 141
    if-eqz v1, :cond_0

    .line 143
    const-string v2, "BUNDLE_ENUM_ERROR"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 145
    const-string v2, "com.sec.android.allshare.event.EVENT_PROVIDER_CONTENTS_UPDATED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    if-nez v1, :cond_2

    .line 148
    iget-object v0, p0, Lcom/sec/android/allshare/al;->a:Lcom/sec/android/allshare/ProviderImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/ProviderImpl;->a(Lcom/sec/android/allshare/ProviderImpl;)Lcom/sec/android/allshare/media/Provider$IProviderEventListener;

    move-result-object v0

    sget-object v1, Lcom/sec/android/allshare/ERROR;->FAIL:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1}, Lcom/sec/android/allshare/media/Provider$IProviderEventListener;->onContentUpdated(Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 151
    :cond_2
    invoke-static {v1}, Lcom/sec/android/allshare/ERROR;->valueOf(Ljava/lang/String;)Lcom/sec/android/allshare/ERROR;

    move-result-object v0

    .line 152
    iget-object v1, p0, Lcom/sec/android/allshare/al;->a:Lcom/sec/android/allshare/ProviderImpl;

    invoke-static {v1}, Lcom/sec/android/allshare/ProviderImpl;->a(Lcom/sec/android/allshare/ProviderImpl;)Lcom/sec/android/allshare/media/Provider$IProviderEventListener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/sec/android/allshare/media/Provider$IProviderEventListener;->onContentUpdated(Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0
.end method
