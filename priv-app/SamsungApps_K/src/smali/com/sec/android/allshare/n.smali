.class final Lcom/sec/android/allshare/n;
.super Lcom/sec/android/allshare/media/FileReceiver;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/allshare/iface/IBundleHolder;
.implements Lcom/sec/android/allshare/iface/IHandlerHolder;


# instance fields
.field private a:Lcom/sec/android/allshare/j;

.field private b:Lcom/sec/android/allshare/IAllShareConnector;

.field private c:Ljava/util/HashMap;

.field private d:Ljava/util/HashMap;

.field private e:Lcom/sec/android/allshare/media/FileReceiver;

.field private f:Z

.field private g:Lcom/sec/android/allshare/e;

.field private h:Lcom/sec/android/allshare/d;

.field private i:Lcom/sec/android/allshare/media/FileReceiver$IFileReceiverProgressUpdateEventListener;


# direct methods
.method constructor <init>(Lcom/sec/android/allshare/IAllShareConnector;Lcom/sec/android/allshare/j;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 103
    invoke-direct {p0}, Lcom/sec/android/allshare/media/FileReceiver;-><init>()V

    .line 36
    iput-object v1, p0, Lcom/sec/android/allshare/n;->a:Lcom/sec/android/allshare/j;

    .line 37
    iput-object v1, p0, Lcom/sec/android/allshare/n;->b:Lcom/sec/android/allshare/IAllShareConnector;

    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/allshare/n;->c:Ljava/util/HashMap;

    .line 42
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/allshare/n;->d:Ljava/util/HashMap;

    .line 44
    iput-object v1, p0, Lcom/sec/android/allshare/n;->e:Lcom/sec/android/allshare/media/FileReceiver;

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/allshare/n;->f:Z

    .line 309
    new-instance v0, Lcom/sec/android/allshare/o;

    invoke-static {}, Lcom/sec/android/allshare/ServiceConnector;->b()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/allshare/o;-><init>(Lcom/sec/android/allshare/n;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/allshare/n;->g:Lcom/sec/android/allshare/e;

    .line 504
    new-instance v0, Lcom/sec/android/allshare/p;

    invoke-static {}, Lcom/sec/android/allshare/ServiceConnector;->b()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/allshare/p;-><init>(Lcom/sec/android/allshare/n;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/allshare/n;->h:Lcom/sec/android/allshare/d;

    .line 597
    new-instance v0, Lcom/sec/android/allshare/t;

    invoke-direct {v0, p0}, Lcom/sec/android/allshare/t;-><init>(Lcom/sec/android/allshare/n;)V

    iput-object v0, p0, Lcom/sec/android/allshare/n;->i:Lcom/sec/android/allshare/media/FileReceiver$IFileReceiverProgressUpdateEventListener;

    .line 105
    if-nez p1, :cond_0

    .line 107
    const-string v0, "FileReceiverImpl"

    const-string v1, "Connection FAIL: AllShare Service Connector does not exist"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    :goto_0
    return-void

    .line 111
    :cond_0
    invoke-interface {p1}, Lcom/sec/android/allshare/IAllShareConnector;->a()Landroid/content/ContentResolver;

    move-result-object v0

    .line 112
    if-eqz v0, :cond_1

    .line 113
    const-string v1, "FileReceiverImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ContentResolver : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    :cond_1
    iput-object p2, p0, Lcom/sec/android/allshare/n;->a:Lcom/sec/android/allshare/j;

    .line 115
    iput-object p1, p0, Lcom/sec/android/allshare/n;->b:Lcom/sec/android/allshare/IAllShareConnector;

    .line 118
    iget-object v0, p0, Lcom/sec/android/allshare/n;->i:Lcom/sec/android/allshare/media/FileReceiver$IFileReceiverProgressUpdateEventListener;

    iget-object v1, p0, Lcom/sec/android/allshare/n;->b:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/allshare/n;->b:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v1}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v1

    if-nez v1, :cond_3

    .line 119
    :cond_2
    :goto_1
    iput-object p0, p0, Lcom/sec/android/allshare/n;->e:Lcom/sec/android/allshare/media/FileReceiver;

    goto :goto_0

    .line 118
    :cond_3
    if-eqz v0, :cond_4

    iget-object v1, p0, Lcom/sec/android/allshare/n;->b:Lcom/sec/android/allshare/IAllShareConnector;

    const-string v2, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    iget-object v3, p0, Lcom/sec/android/allshare/n;->a:Lcom/sec/android/allshare/j;

    invoke-virtual {v3}, Lcom/sec/android/allshare/j;->getBundle()Landroid/os/Bundle;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/allshare/n;->h:Lcom/sec/android/allshare/d;

    invoke-interface {v1, v2, v3, v4}, Lcom/sec/android/allshare/IAllShareConnector;->a(Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/allshare/d;)Z

    :goto_2
    iput-object v0, p0, Lcom/sec/android/allshare/n;->i:Lcom/sec/android/allshare/media/FileReceiver$IFileReceiverProgressUpdateEventListener;

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lcom/sec/android/allshare/n;->b:Lcom/sec/android/allshare/IAllShareConnector;

    const-string v2, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    iget-object v3, p0, Lcom/sec/android/allshare/n;->a:Lcom/sec/android/allshare/j;

    invoke-virtual {v3}, Lcom/sec/android/allshare/j;->getBundle()Landroid/os/Bundle;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/allshare/n;->h:Lcom/sec/android/allshare/d;

    invoke-interface {v1, v2, v3, v4}, Lcom/sec/android/allshare/IAllShareConnector;->b(Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/allshare/d;)V

    goto :goto_2
.end method

.method static synthetic a(Lcom/sec/android/allshare/n;)Lcom/sec/android/allshare/media/FileReceiver;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/allshare/n;->e:Lcom/sec/android/allshare/media/FileReceiver;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/allshare/n;Ljava/lang/String;)Lcom/sec/android/allshare/x;
    .locals 3

    .prologue
    .line 232
    const-string v0, "FileReceiverImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getTimeKeyInfoMap() called. key : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/allshare/n;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/allshare/x;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/allshare/n;Ljava/lang/String;Lcom/sec/android/allshare/x;)V
    .locals 4

    .prologue
    .line 205
    const-string v0, "FileReceiverImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "putSessionKeyInfoMap() called. key : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", count : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/sec/android/allshare/x;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/sec/android/allshare/x;

    invoke-virtual {p2}, Lcom/sec/android/allshare/x;->c()I

    move-result v1

    invoke-virtual {p2}, Lcom/sec/android/allshare/x;->a()Lcom/sec/android/allshare/media/FileReceiver$IFileReceiverReceiveResponseListener;

    move-result-object v2

    invoke-virtual {p2}, Lcom/sec/android/allshare/x;->b()Lcom/sec/android/allshare/media/FileReceiver$IFileReceiverProgressUpdateEventListener;

    move-result-object v3

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/sec/android/allshare/x;-><init>(Lcom/sec/android/allshare/n;ILcom/sec/android/allshare/media/FileReceiver$IFileReceiverReceiveResponseListener;Lcom/sec/android/allshare/media/FileReceiver$IFileReceiverProgressUpdateEventListener;)V

    iget-object v1, p0, Lcom/sec/android/allshare/n;->d:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method static synthetic b(Lcom/sec/android/allshare/n;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 238
    iget-object v0, p0, Lcom/sec/android/allshare/n;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "FileReceiverImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "removeTimeKeyInfoMap() called. key : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "size : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/allshare/n;->c:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lcom/sec/android/allshare/n;)Z
    .locals 1

    .prologue
    .line 199
    iget-boolean v0, p0, Lcom/sec/android/allshare/n;->f:Z

    return v0
.end method

.method static synthetic c(Lcom/sec/android/allshare/n;Ljava/lang/String;)Lcom/sec/android/allshare/x;
    .locals 3

    .prologue
    .line 212
    const-string v0, "FileReceiverImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getSessionKeyInfoMap() called. key : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "size : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/allshare/n;->d:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/sec/android/allshare/n;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/allshare/x;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/android/allshare/n;)V
    .locals 1

    .prologue
    .line 194
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/allshare/n;->f:Z

    return-void
.end method

.method static synthetic d(Lcom/sec/android/allshare/n;)Lcom/sec/android/allshare/media/FileReceiver$IFileReceiverProgressUpdateEventListener;
    .locals 1

    .prologue
    .line 597
    iget-object v0, p0, Lcom/sec/android/allshare/n;->i:Lcom/sec/android/allshare/media/FileReceiver$IFileReceiverProgressUpdateEventListener;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/android/allshare/n;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 218
    iget-object v0, p0, Lcom/sec/android/allshare/n;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "FileReceiverImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "removeSessionKeyInfoMap() called. key : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "size : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/allshare/n;->c:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final cancel(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/android/allshare/n;->b:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/n;->b:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 146
    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/n;->g:Lcom/sec/android/allshare/e;

    new-instance v1, Lcom/sec/android/allshare/u;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/allshare/u;-><init>(Lcom/sec/android/allshare/n;Ljava/lang/String;)V

    .line 166
    const-wide/16 v2, 0x1

    .line 146
    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/allshare/e;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 186
    :cond_1
    :goto_0
    return-void

    .line 171
    :cond_2
    if-eqz p1, :cond_1

    .line 173
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 174
    const-string v1, "com.sec.android.allshare.action.ACTION_FILE_ARRAYLIST_RECEIVER_CANCEL"

    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 176
    invoke-virtual {v0}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v1

    .line 177
    const-string v2, "BUNDLE_STRING_ID"

    iget-object v3, p0, Lcom/sec/android/allshare/n;->a:Lcom/sec/android/allshare/j;

    invoke-virtual {v3}, Lcom/sec/android/allshare/j;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    const-string v2, "BUNDLE_STRING_SESSIONID"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    const-string v1, "FileReceiverImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sessionID : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    iget-object v1, p0, Lcom/sec/android/allshare/n;->b:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v2, p0, Lcom/sec/android/allshare/n;->g:Lcom/sec/android/allshare/e;

    invoke-interface {v1, v0, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    .line 184
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/allshare/n;->f:Z

    goto :goto_0
.end method

.method public final getBundle()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 755
    iget-object v0, p0, Lcom/sec/android/allshare/n;->a:Lcom/sec/android/allshare/j;

    if-nez v0, :cond_0

    .line 757
    const/4 v0, 0x0

    .line 759
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/n;->a:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method public final getDeviceDomain()Lcom/sec/android/allshare/Device$DeviceDomain;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/sec/android/allshare/n;->a:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getDeviceDomain()Lcom/sec/android/allshare/Device$DeviceDomain;

    move-result-object v0

    return-object v0
.end method

.method public final getDeviceType()Lcom/sec/android/allshare/Device$DeviceType;
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lcom/sec/android/allshare/n;->a:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getDeviceType()Lcom/sec/android/allshare/Device$DeviceType;

    move-result-object v0

    return-object v0
.end method

.method public final getID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/sec/android/allshare/n;->a:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getIPAdress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/sec/android/allshare/n;->a:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getIPAdress()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getIcon()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lcom/sec/android/allshare/n;->a:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getIcon()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final getIconList()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 290
    iget-object v0, p0, Lcom/sec/android/allshare/n;->a:Lcom/sec/android/allshare/j;

    if-nez v0, :cond_0

    .line 292
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 294
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/n;->a:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getIconList()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public final getModelName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lcom/sec/android/allshare/n;->a:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getModelName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getNIC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/allshare/n;->a:Lcom/sec/android/allshare/j;

    if-nez v0, :cond_0

    .line 127
    const-string v0, ""

    .line 129
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/n;->a:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getNIC()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lcom/sec/android/allshare/n;->a:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final receive(Ljava/util/ArrayList;Ljava/lang/String;Lcom/sec/android/allshare/media/FileReceiver$IFileReceiverReceiveResponseListener;Lcom/sec/android/allshare/media/FileReceiver$IFileReceiverProgressUpdateEventListener;)V
    .locals 6

    .prologue
    .line 706
    iget-object v0, p0, Lcom/sec/android/allshare/n;->b:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/n;->b:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 708
    :cond_0
    iget-object v1, p0, Lcom/sec/android/allshare/n;->e:Lcom/sec/android/allshare/media/FileReceiver;

    const-string v2, ""

    sget-object v5, Lcom/sec/android/allshare/ERROR;->FAIL:Lcom/sec/android/allshare/ERROR;

    move-object v0, p3

    move-object v3, p1

    move-object v4, p2

    invoke-interface/range {v0 .. v5}, Lcom/sec/android/allshare/media/FileReceiver$IFileReceiverReceiveResponseListener;->onReceiveResponseReceived(Lcom/sec/android/allshare/media/FileReceiver;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Lcom/sec/android/allshare/ERROR;)V

    .line 750
    :goto_0
    return-void

    .line 712
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 714
    :cond_2
    iget-object v0, p0, Lcom/sec/android/allshare/n;->g:Lcom/sec/android/allshare/e;

    new-instance v1, Lcom/sec/android/allshare/v;

    invoke-direct {v1, p0, p3, p1, p2}, Lcom/sec/android/allshare/v;-><init>(Lcom/sec/android/allshare/n;Lcom/sec/android/allshare/media/FileReceiver$IFileReceiverReceiveResponseListener;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 722
    const-wide/16 v2, 0x1

    .line 714
    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/allshare/e;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 726
    :cond_3
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 728
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_4

    .line 733
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 734
    const-string v2, "com.sec.android.allshare.action.ACTION_FILE_ARRAYLIST_RECEIVER_RECEIVE"

    invoke-virtual {v0, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 736
    invoke-virtual {v0}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    .line 737
    const-string v3, "BUNDLE_STRING_ID"

    iget-object v4, p0, Lcom/sec/android/allshare/n;->a:Lcom/sec/android/allshare/j;

    invoke-virtual {v4}, Lcom/sec/android/allshare/j;->getID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 738
    const-string v3, "BUNDLE_STRING_NAME"

    invoke-virtual {v2, v3, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 739
    const-string v3, "BUNDLE_STRING_ARRAYLIST_FILE_PATH"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 741
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    .line 742
    const-string v4, "BUNDLE_STRING_UNIQUEKEY"

    invoke-virtual {v2, v4, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 745
    new-instance v2, Lcom/sec/android/allshare/x;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {v2, p0, v1, p3, p4}, Lcom/sec/android/allshare/x;-><init>(Lcom/sec/android/allshare/n;ILcom/sec/android/allshare/media/FileReceiver$IFileReceiverReceiveResponseListener;Lcom/sec/android/allshare/media/FileReceiver$IFileReceiverProgressUpdateEventListener;)V

    .line 746
    const-string v1, "FileReceiverImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "putTimeKeyInfoMap() called. key : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/sec/android/allshare/x;->c()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/sec/android/allshare/x;

    invoke-virtual {v2}, Lcom/sec/android/allshare/x;->c()I

    move-result v4

    invoke-virtual {v2}, Lcom/sec/android/allshare/x;->a()Lcom/sec/android/allshare/media/FileReceiver$IFileReceiverReceiveResponseListener;

    move-result-object v5

    invoke-virtual {v2}, Lcom/sec/android/allshare/x;->b()Lcom/sec/android/allshare/media/FileReceiver$IFileReceiverProgressUpdateEventListener;

    move-result-object v2

    invoke-direct {v1, p0, v4, v5, v2}, Lcom/sec/android/allshare/x;-><init>(Lcom/sec/android/allshare/n;ILcom/sec/android/allshare/media/FileReceiver$IFileReceiverReceiveResponseListener;Lcom/sec/android/allshare/media/FileReceiver$IFileReceiverProgressUpdateEventListener;)V

    iget-object v2, p0, Lcom/sec/android/allshare/n;->c:Ljava/util/HashMap;

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 748
    iget-object v1, p0, Lcom/sec/android/allshare/n;->b:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v2, p0, Lcom/sec/android/allshare/n;->g:Lcom/sec/android/allshare/e;

    invoke-interface {v1, v0, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    goto/16 :goto_0

    .line 728
    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 730
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1
.end method

.method public final removeEventHandler()V
    .locals 4

    .prologue
    .line 764
    iget-object v0, p0, Lcom/sec/android/allshare/n;->b:Lcom/sec/android/allshare/IAllShareConnector;

    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_UNSUBSCRIBE"

    invoke-virtual {p0}, Lcom/sec/android/allshare/n;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/allshare/n;->h:Lcom/sec/android/allshare/d;

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/android/allshare/IAllShareConnector;->b(Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/allshare/d;)V

    .line 765
    return-void
.end method
