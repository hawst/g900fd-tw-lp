.class final Lcom/sec/android/allshare/bj;
.super Landroid/os/Handler;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/allshare/bg;


# direct methods
.method constructor <init>(Lcom/sec/android/allshare/bg;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/allshare/bj;->a:Lcom/sec/android/allshare/bg;

    .line 627
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 632
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 634
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 721
    :cond_0
    :goto_0
    return-void

    .line 637
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/allshare/bj;->a:Lcom/sec/android/allshare/bg;

    invoke-static {v0}, Lcom/sec/android/allshare/bg;->c(Lcom/sec/android/allshare/bg;)V

    .line 639
    iget-object v0, p0, Lcom/sec/android/allshare/bj;->a:Lcom/sec/android/allshare/bg;

    invoke-static {v0}, Lcom/sec/android/allshare/bg;->d(Lcom/sec/android/allshare/bg;)Lcom/sec/android/allshare/media/ViewController$IResponseListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 641
    iget-object v0, p0, Lcom/sec/android/allshare/bj;->a:Lcom/sec/android/allshare/bg;

    invoke-static {v0}, Lcom/sec/android/allshare/bg;->d(Lcom/sec/android/allshare/bg;)Lcom/sec/android/allshare/media/ViewController$IResponseListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/allshare/bj;->a:Lcom/sec/android/allshare/bg;

    sget-object v2, Lcom/sec/android/allshare/ERROR;->INVALID_DEVICE:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/media/ViewController$IResponseListener;->onConnectResponseReceived(Lcom/sec/android/allshare/media/ViewController;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 646
    :sswitch_1
    const-string v0, "ViewControllerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[ViewControl] Event : IAPP_AUTHENTICATION arg : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 647
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-nez v0, :cond_1

    .line 649
    iget-object v0, p0, Lcom/sec/android/allshare/bj;->a:Lcom/sec/android/allshare/bg;

    invoke-static {v0}, Lcom/sec/android/allshare/bg;->c(Lcom/sec/android/allshare/bg;)V

    .line 651
    iget-object v0, p0, Lcom/sec/android/allshare/bj;->a:Lcom/sec/android/allshare/bg;

    invoke-static {v0}, Lcom/sec/android/allshare/bg;->a(Lcom/sec/android/allshare/bg;)Lcom/sec/android/allshare/media/ViewController$IEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 653
    iget-object v0, p0, Lcom/sec/android/allshare/bj;->a:Lcom/sec/android/allshare/bg;

    invoke-static {v0}, Lcom/sec/android/allshare/bg;->a(Lcom/sec/android/allshare/bg;)Lcom/sec/android/allshare/media/ViewController$IEventListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/allshare/bj;->a:Lcom/sec/android/allshare/bg;

    sget-object v2, Lcom/sec/android/allshare/ERROR;->FAIL:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/media/ViewController$IEventListener;->onAuthenticationFailed(Lcom/sec/android/allshare/media/ViewController;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 656
    :cond_1
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-ne v0, v3, :cond_2

    .line 658
    iget-object v0, p0, Lcom/sec/android/allshare/bj;->a:Lcom/sec/android/allshare/bg;

    invoke-static {v0}, Lcom/sec/android/allshare/bg;->e(Lcom/sec/android/allshare/bg;)V

    goto :goto_0

    .line 660
    :cond_2
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-ne v0, v4, :cond_0

    .line 662
    iget-object v0, p0, Lcom/sec/android/allshare/bj;->a:Lcom/sec/android/allshare/bg;

    invoke-static {v0}, Lcom/sec/android/allshare/bg;->e(Lcom/sec/android/allshare/bg;)V

    goto :goto_0

    .line 667
    :sswitch_2
    const-string v0, "ViewControllerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[ViewControl] Event : IAPP_AUTHENTICATION_TIMEOUT arg : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 669
    iget-object v0, p0, Lcom/sec/android/allshare/bj;->a:Lcom/sec/android/allshare/bg;

    invoke-static {v0}, Lcom/sec/android/allshare/bg;->c(Lcom/sec/android/allshare/bg;)V

    .line 671
    iget-object v0, p0, Lcom/sec/android/allshare/bj;->a:Lcom/sec/android/allshare/bg;

    invoke-static {v0}, Lcom/sec/android/allshare/bg;->a(Lcom/sec/android/allshare/bg;)Lcom/sec/android/allshare/media/ViewController$IEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 673
    iget-object v0, p0, Lcom/sec/android/allshare/bj;->a:Lcom/sec/android/allshare/bg;

    invoke-static {v0}, Lcom/sec/android/allshare/bg;->a(Lcom/sec/android/allshare/bg;)Lcom/sec/android/allshare/media/ViewController$IEventListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/allshare/bj;->a:Lcom/sec/android/allshare/bg;

    sget-object v2, Lcom/sec/android/allshare/ERROR;->TIME_OUT:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/media/ViewController$IEventListener;->onAuthenticationFailed(Lcom/sec/android/allshare/media/ViewController;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 678
    :sswitch_3
    const-string v0, "ViewControllerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[ViewControl] Event : IAPP_EXIT arg : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 680
    iget-object v0, p0, Lcom/sec/android/allshare/bj;->a:Lcom/sec/android/allshare/bg;

    invoke-static {v0}, Lcom/sec/android/allshare/bg;->c(Lcom/sec/android/allshare/bg;)V

    .line 682
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-nez v0, :cond_3

    .line 684
    iget-object v0, p0, Lcom/sec/android/allshare/bj;->a:Lcom/sec/android/allshare/bg;

    invoke-static {v0}, Lcom/sec/android/allshare/bg;->a(Lcom/sec/android/allshare/bg;)Lcom/sec/android/allshare/media/ViewController$IEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 686
    iget-object v0, p0, Lcom/sec/android/allshare/bj;->a:Lcom/sec/android/allshare/bg;

    invoke-static {v0}, Lcom/sec/android/allshare/bg;->a(Lcom/sec/android/allshare/bg;)Lcom/sec/android/allshare/media/ViewController$IEventListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/allshare/bj;->a:Lcom/sec/android/allshare/bg;

    sget-object v2, Lcom/sec/android/allshare/ERROR;->DELETED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/media/ViewController$IEventListener;->onAuthenticationFailed(Lcom/sec/android/allshare/media/ViewController;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 689
    :cond_3
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-ne v0, v3, :cond_4

    .line 691
    iget-object v0, p0, Lcom/sec/android/allshare/bj;->a:Lcom/sec/android/allshare/bg;

    invoke-static {v0}, Lcom/sec/android/allshare/bg;->a(Lcom/sec/android/allshare/bg;)Lcom/sec/android/allshare/media/ViewController$IEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 693
    iget-object v0, p0, Lcom/sec/android/allshare/bj;->a:Lcom/sec/android/allshare/bg;

    invoke-static {v0}, Lcom/sec/android/allshare/bg;->a(Lcom/sec/android/allshare/bg;)Lcom/sec/android/allshare/media/ViewController$IEventListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/allshare/bj;->a:Lcom/sec/android/allshare/bg;

    sget-object v2, Lcom/sec/android/allshare/ERROR;->PERMISSION_NOT_ALLOWED:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/media/ViewController$IEventListener;->onAuthenticationFailed(Lcom/sec/android/allshare/media/ViewController;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 696
    :cond_4
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-ne v0, v4, :cond_0

    goto/16 :goto_0

    .line 706
    :sswitch_4
    const-string v0, "ViewControllerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[ViewControl] Event : IAPP_KEYBOARD_SYNC arg : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 710
    :sswitch_5
    const-string v0, "ViewControllerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[ViewControl] Event : IAPP_REMOCON arg : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 714
    :sswitch_6
    const-string v0, "ViewControllerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[ViewControl] Event : IAPP_STATUS arg : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 718
    :sswitch_7
    const-string v0, "ViewControllerImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[ViewControl] Event : IAPP_REMOTE_INPUT_TYPE arg : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 634
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_5
        0x3 -> :sswitch_4
        0xa -> :sswitch_7
        0x64 -> :sswitch_1
        0x65 -> :sswitch_2
        0xc8 -> :sswitch_6
        0x12c -> :sswitch_3
        0x270f -> :sswitch_0
    .end sparse-switch
.end method
