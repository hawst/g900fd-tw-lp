.class final Lcom/sec/android/allshare/ProviderImpl;
.super Lcom/sec/android/allshare/media/Provider;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/allshare/iface/IBundleHolder;
.implements Lcom/sec/android/allshare/iface/IHandlerHolder;


# static fields
.field private static f:Lcom/sec/android/allshare/Item;


# instance fields
.field private a:Lcom/sec/android/allshare/IAllShareConnector;

.field private b:Lcom/sec/android/allshare/j;

.field private c:Lcom/sec/android/allshare/media/Provider$IProviderBrowseResponseListener;

.field private d:Lcom/sec/android/allshare/media/Provider$IProviderSearchResponseListener;

.field private e:Lcom/sec/android/allshare/media/Provider$IProviderEventListener;

.field private g:Lcom/sec/android/allshare/media/Receiver;

.field private h:Lcom/sec/android/allshare/d;

.field private i:Lcom/sec/android/allshare/e;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v3, 0x0

    .line 63
    sput-object v3, Lcom/sec/android/allshare/ProviderImpl;->f:Lcom/sec/android/allshare/Item;

    .line 69
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 70
    const-string v1, "BUNDLE_STRING_ITEM_TITLE"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    const-string v1, "BUNDLE_PARCELABLE_ITEM_URI"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 72
    const-string v1, "BUNDLE_LONG_ITEM_DATE"

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 73
    const-string v1, "BUNDLE_LONG_ITEM_FILE_SIZE"

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 74
    const-string v1, "BUNDLE_STRING_OBJECT_ID"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    new-instance v1, Lcom/sec/android/allshare/ProviderImpl$RootFolderItem;

    invoke-direct {v1, v0}, Lcom/sec/android/allshare/ProviderImpl$RootFolderItem;-><init>(Landroid/os/Bundle;)V

    sput-object v1, Lcom/sec/android/allshare/ProviderImpl;->f:Lcom/sec/android/allshare/Item;

    .line 52
    return-void
.end method

.method constructor <init>(Lcom/sec/android/allshare/IAllShareConnector;Lcom/sec/android/allshare/j;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 109
    invoke-direct {p0}, Lcom/sec/android/allshare/media/Provider;-><init>()V

    .line 56
    iput-object v2, p0, Lcom/sec/android/allshare/ProviderImpl;->a:Lcom/sec/android/allshare/IAllShareConnector;

    .line 57
    iput-object v2, p0, Lcom/sec/android/allshare/ProviderImpl;->b:Lcom/sec/android/allshare/j;

    .line 59
    iput-object v2, p0, Lcom/sec/android/allshare/ProviderImpl;->c:Lcom/sec/android/allshare/media/Provider$IProviderBrowseResponseListener;

    .line 60
    iput-object v2, p0, Lcom/sec/android/allshare/ProviderImpl;->d:Lcom/sec/android/allshare/media/Provider$IProviderSearchResponseListener;

    .line 61
    iput-object v2, p0, Lcom/sec/android/allshare/ProviderImpl;->e:Lcom/sec/android/allshare/media/Provider$IProviderEventListener;

    .line 130
    new-instance v0, Lcom/sec/android/allshare/al;

    invoke-static {}, Lcom/sec/android/allshare/ServiceConnector;->b()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/allshare/al;-><init>(Lcom/sec/android/allshare/ProviderImpl;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->h:Lcom/sec/android/allshare/d;

    .line 162
    new-instance v0, Lcom/sec/android/allshare/am;

    invoke-static {}, Lcom/sec/android/allshare/ServiceConnector;->b()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/allshare/am;-><init>(Lcom/sec/android/allshare/ProviderImpl;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->i:Lcom/sec/android/allshare/e;

    .line 111
    if-nez p1, :cond_0

    .line 113
    const-string v0, "ProviderImpl"

    const-string v1, "Connection FAIL: AllShare Service Connector does not exist"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    :goto_0
    return-void

    .line 117
    :cond_0
    iput-object p2, p0, Lcom/sec/android/allshare/ProviderImpl;->b:Lcom/sec/android/allshare/j;

    .line 118
    iput-object p1, p0, Lcom/sec/android/allshare/ProviderImpl;->a:Lcom/sec/android/allshare/IAllShareConnector;

    .line 121
    invoke-virtual {p2}, Lcom/sec/android/allshare/j;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 122
    const-string v1, "BUNDLE_BOOLEAN_RECEIVERABLE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 123
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 124
    new-instance v0, Lcom/sec/android/allshare/an;

    invoke-direct {v0, p1, p2}, Lcom/sec/android/allshare/an;-><init>(Lcom/sec/android/allshare/IAllShareConnector;Lcom/sec/android/allshare/j;)V

    iput-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->g:Lcom/sec/android/allshare/media/Receiver;

    .line 127
    :goto_1
    const-string v0, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    invoke-virtual {p2}, Lcom/sec/android/allshare/j;->getBundle()Landroid/os/Bundle;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/allshare/ProviderImpl;->h:Lcom/sec/android/allshare/d;

    invoke-interface {p1, v0, v1, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/allshare/d;)Z

    goto :goto_0

    .line 126
    :cond_1
    iput-object v2, p0, Lcom/sec/android/allshare/ProviderImpl;->g:Lcom/sec/android/allshare/media/Receiver;

    goto :goto_1
.end method

.method static synthetic a(Lcom/sec/android/allshare/ProviderImpl;)Lcom/sec/android/allshare/media/Provider$IProviderEventListener;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->e:Lcom/sec/android/allshare/media/Provider$IProviderEventListener;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/android/allshare/ProviderImpl;)Lcom/sec/android/allshare/media/Provider$IProviderSearchResponseListener;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->d:Lcom/sec/android/allshare/media/Provider$IProviderSearchResponseListener;

    return-object v0
.end method

.method static synthetic c(Lcom/sec/android/allshare/ProviderImpl;)Lcom/sec/android/allshare/media/Provider$IProviderBrowseResponseListener;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->c:Lcom/sec/android/allshare/media/Provider$IProviderBrowseResponseListener;

    return-object v0
.end method


# virtual methods
.method public final browse(Lcom/sec/android/allshare/Item;II)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v2, -0x1

    .line 324
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->a:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->a:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 326
    :cond_0
    const-string v0, "ProviderImpl"

    const-string v1, "browse fail : SERVICE_NOT_CONNECTED"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->c:Lcom/sec/android/allshare/media/Provider$IProviderBrowseResponseListener;

    if-eqz v0, :cond_1

    .line 328
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->c:Lcom/sec/android/allshare/media/Provider$IProviderBrowseResponseListener;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sget-object v6, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    move v3, v2

    invoke-interface/range {v0 .. v6}, Lcom/sec/android/allshare/media/Provider$IProviderBrowseResponseListener;->onBrowseResponseReceived(Ljava/util/ArrayList;IILcom/sec/android/allshare/Item;ZLcom/sec/android/allshare/ERROR;)V

    .line 364
    :cond_1
    :goto_0
    return-void

    .line 332
    :cond_2
    if-eqz p1, :cond_3

    if-eq p2, v2, :cond_3

    if-gtz p3, :cond_4

    .line 334
    :cond_3
    const-string v0, "ProviderImpl"

    const-string v1, "browse fail : INVALID_ARGUMENT"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->c:Lcom/sec/android/allshare/media/Provider$IProviderBrowseResponseListener;

    if-eqz v0, :cond_1

    .line 336
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->c:Lcom/sec/android/allshare/media/Provider$IProviderBrowseResponseListener;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sget-object v6, Lcom/sec/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/sec/android/allshare/ERROR;

    move v2, p2

    move v3, p3

    move-object v4, p1

    invoke-interface/range {v0 .. v6}, Lcom/sec/android/allshare/media/Provider$IProviderBrowseResponseListener;->onBrowseResponseReceived(Ljava/util/ArrayList;IILcom/sec/android/allshare/Item;ZLcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 340
    :cond_4
    invoke-virtual {p1}, Lcom/sec/android/allshare/Item;->getType()Lcom/sec/android/allshare/Item$MediaType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/allshare/Item$MediaType;->ITEM_FOLDER:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/Item$MediaType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 342
    const-string v0, "ProviderImpl"

    const-string v1, "browse fail : INVALID_ARGUMENT"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->c:Lcom/sec/android/allshare/media/Provider$IProviderBrowseResponseListener;

    if-eqz v0, :cond_1

    .line 344
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->c:Lcom/sec/android/allshare/media/Provider$IProviderBrowseResponseListener;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sget-object v6, Lcom/sec/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/sec/android/allshare/ERROR;

    move v2, p2

    move v3, p3

    move-object v4, p1

    invoke-interface/range {v0 .. v6}, Lcom/sec/android/allshare/media/Provider$IProviderBrowseResponseListener;->onBrowseResponseReceived(Ljava/util/ArrayList;IILcom/sec/android/allshare/Item;ZLcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 348
    :cond_5
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 349
    const-string v1, "com.sec.android.allshare.action.ACTION_PROVIDER_REQUEST_ITEMS"

    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 350
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 352
    instance-of v2, p1, Lcom/sec/android/allshare/iface/IBundleHolder;

    if-eqz v2, :cond_6

    .line 353
    check-cast p1, Lcom/sec/android/allshare/iface/IBundleHolder;

    invoke-interface {p1}, Lcom/sec/android/allshare/iface/IBundleHolder;->getBundle()Landroid/os/Bundle;

    move-result-object v4

    .line 355
    :cond_6
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/ProviderImpl;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    const-string v2, "BUNDLE_PARCELABLE_FOLDERITEM"

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 357
    const-string v2, "BUNDLE_INT_STARTINDEX"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 358
    const-string v2, "BUNDLE_INT_REQUESTCOUNT"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 360
    const-string v2, "ProviderImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "browse "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/allshare/ProviderImpl;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", index: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", count: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 363
    iget-object v1, p0, Lcom/sec/android/allshare/ProviderImpl;->a:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v2, p0, Lcom/sec/android/allshare/ProviderImpl;->i:Lcom/sec/android/allshare/e;

    invoke-interface {v1, v0, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    goto/16 :goto_0
.end method

.method public final getBundle()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 455
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->b:Lcom/sec/android/allshare/j;

    if-nez v0, :cond_0

    .line 457
    const/4 v0, 0x0

    .line 459
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->b:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method public final getDeviceDomain()Lcom/sec/android/allshare/Device$DeviceDomain;
    .locals 1

    .prologue
    .line 375
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->b:Lcom/sec/android/allshare/j;

    if-nez v0, :cond_0

    .line 377
    sget-object v0, Lcom/sec/android/allshare/Device$DeviceDomain;->UNKNOWN:Lcom/sec/android/allshare/Device$DeviceDomain;

    .line 379
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->b:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getDeviceDomain()Lcom/sec/android/allshare/Device$DeviceDomain;

    move-result-object v0

    goto :goto_0
.end method

.method public final getDeviceType()Lcom/sec/android/allshare/Device$DeviceType;
    .locals 1

    .prologue
    .line 425
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->b:Lcom/sec/android/allshare/j;

    if-nez v0, :cond_0

    .line 427
    sget-object v0, Lcom/sec/android/allshare/Device$DeviceType;->UNKNOWN:Lcom/sec/android/allshare/Device$DeviceType;

    .line 429
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->b:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getDeviceType()Lcom/sec/android/allshare/Device$DeviceType;

    move-result-object v0

    goto :goto_0
.end method

.method public final getID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 385
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->b:Lcom/sec/android/allshare/j;

    if-nez v0, :cond_0

    .line 387
    const-string v0, ""

    .line 389
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->b:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getID()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getIPAdress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 445
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->b:Lcom/sec/android/allshare/j;

    if-nez v0, :cond_0

    .line 447
    const-string v0, ""

    .line 449
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->b:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getIPAdress()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getIcon()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 395
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->b:Lcom/sec/android/allshare/j;

    if-nez v0, :cond_0

    .line 397
    const/4 v0, 0x0

    .line 399
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->b:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getIcon()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public final getIconList()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 405
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->b:Lcom/sec/android/allshare/j;

    if-nez v0, :cond_0

    .line 407
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 409
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->b:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getIconList()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public final getModelName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 435
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->b:Lcom/sec/android/allshare/j;

    if-nez v0, :cond_0

    .line 437
    const-string v0, ""

    .line 439
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->b:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getModelName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getNIC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 556
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->b:Lcom/sec/android/allshare/j;

    if-nez v0, :cond_0

    .line 558
    const-string v0, ""

    .line 560
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->b:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getNIC()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 415
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->b:Lcom/sec/android/allshare/j;

    if-nez v0, :cond_0

    .line 417
    const-string v0, ""

    .line 419
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->b:Lcom/sec/android/allshare/j;

    invoke-virtual {v0}, Lcom/sec/android/allshare/j;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getReceiver()Lcom/sec/android/allshare/media/Receiver;
    .locals 1

    .prologue
    .line 465
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->g:Lcom/sec/android/allshare/media/Receiver;

    return-object v0
.end method

.method public final getRootFolder()Lcom/sec/android/allshare/Item;
    .locals 1

    .prologue
    .line 307
    sget-object v0, Lcom/sec/android/allshare/ProviderImpl;->f:Lcom/sec/android/allshare/Item;

    return-object v0
.end method

.method public final isSearchable()Z
    .locals 2

    .prologue
    .line 313
    invoke-virtual {p0}, Lcom/sec/android/allshare/ProviderImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 314
    if-nez v0, :cond_0

    .line 316
    const/4 v0, 0x0

    .line 318
    :goto_0
    return v0

    :cond_0
    const-string v1, "BUNDLE_BOOLEAN_SEARCHABLE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public final removeEventHandler()V
    .locals 4

    .prologue
    .line 566
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->a:Lcom/sec/android/allshare/IAllShareConnector;

    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_UNSUBSCRIBE"

    invoke-virtual {p0}, Lcom/sec/android/allshare/ProviderImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/allshare/ProviderImpl;->h:Lcom/sec/android/allshare/d;

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/android/allshare/IAllShareConnector;->b(Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/allshare/d;)V

    .line 567
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->g:Lcom/sec/android/allshare/media/Receiver;

    if-eqz v0, :cond_0

    .line 569
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->g:Lcom/sec/android/allshare/media/Receiver;

    check-cast v0, Lcom/sec/android/allshare/an;

    invoke-virtual {v0}, Lcom/sec/android/allshare/an;->a()V

    .line 571
    :cond_0
    return-void
.end method

.method public final search(Lcom/sec/android/allshare/media/SearchCriteria;II)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v2, -0x1

    const/4 v5, 0x0

    .line 479
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->a:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->a:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 481
    :cond_0
    const-string v0, "ProviderImpl"

    const-string v1, "search fail : SERVICE_NOT_CONNECTED"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->d:Lcom/sec/android/allshare/media/Provider$IProviderSearchResponseListener;

    if-eqz v0, :cond_1

    .line 483
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->d:Lcom/sec/android/allshare/media/Provider$IProviderSearchResponseListener;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sget-object v6, Lcom/sec/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/sec/android/allshare/ERROR;

    move v3, v2

    invoke-interface/range {v0 .. v6}, Lcom/sec/android/allshare/media/Provider$IProviderSearchResponseListener;->onSearchResponseReceived(Ljava/util/ArrayList;IILcom/sec/android/allshare/media/SearchCriteria;ZLcom/sec/android/allshare/ERROR;)V

    .line 544
    :cond_1
    :goto_0
    return-void

    .line 487
    :cond_2
    if-nez p1, :cond_3

    .line 489
    const-string v0, "ProviderImpl"

    const-string v1, "search fail : INVALID_ARGUMENT (searchCriteria)"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 490
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->d:Lcom/sec/android/allshare/media/Provider$IProviderSearchResponseListener;

    if-eqz v0, :cond_1

    .line 491
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->d:Lcom/sec/android/allshare/media/Provider$IProviderSearchResponseListener;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sget-object v6, Lcom/sec/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/sec/android/allshare/ERROR;

    move v2, p2

    move v3, p3

    invoke-interface/range {v0 .. v6}, Lcom/sec/android/allshare/media/Provider$IProviderSearchResponseListener;->onSearchResponseReceived(Ljava/util/ArrayList;IILcom/sec/android/allshare/media/SearchCriteria;ZLcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 495
    :cond_3
    if-eq p2, v2, :cond_4

    if-gtz p3, :cond_5

    .line 497
    :cond_4
    const-string v0, "ProviderImpl"

    const-string v1, "search fail : INVALID_ARGUMENT (index)"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 498
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->d:Lcom/sec/android/allshare/media/Provider$IProviderSearchResponseListener;

    if-eqz v0, :cond_1

    .line 499
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->d:Lcom/sec/android/allshare/media/Provider$IProviderSearchResponseListener;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sget-object v6, Lcom/sec/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/sec/android/allshare/ERROR;

    move v2, p2

    move v3, p3

    move-object v4, p1

    invoke-interface/range {v0 .. v6}, Lcom/sec/android/allshare/media/Provider$IProviderSearchResponseListener;->onSearchResponseReceived(Ljava/util/ArrayList;IILcom/sec/android/allshare/media/SearchCriteria;ZLcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 503
    :cond_5
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 504
    const-string v1, "com.sec.android.allshare.action.ACTION_PROVIDER_REQUEST_SEARCHCRITERIA_ITEMS"

    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 505
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 506
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 509
    sget-object v3, Lcom/sec/android/allshare/Item$MediaType;->ITEM_UNKNOWN:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {p1, v3}, Lcom/sec/android/allshare/media/SearchCriteria;->isMatchedItemType(Lcom/sec/android/allshare/Item$MediaType;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 511
    const-string v0, "ProviderImpl"

    const-string v1, "search fail : INVALID_ARGUMENT (MediaType)"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->d:Lcom/sec/android/allshare/media/Provider$IProviderSearchResponseListener;

    if-eqz v0, :cond_1

    .line 513
    iget-object v0, p0, Lcom/sec/android/allshare/ProviderImpl;->d:Lcom/sec/android/allshare/media/Provider$IProviderSearchResponseListener;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sget-object v6, Lcom/sec/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/sec/android/allshare/ERROR;

    move v2, p2

    move v3, p3

    move-object v4, p1

    invoke-interface/range {v0 .. v6}, Lcom/sec/android/allshare/media/Provider$IProviderSearchResponseListener;->onSearchResponseReceived(Ljava/util/ArrayList;IILcom/sec/android/allshare/media/SearchCriteria;ZLcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 517
    :cond_6
    sget-object v3, Lcom/sec/android/allshare/Item$MediaType;->ITEM_AUDIO:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {p1, v3}, Lcom/sec/android/allshare/media/SearchCriteria;->isMatchedItemType(Lcom/sec/android/allshare/Item$MediaType;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 519
    sget-object v3, Lcom/sec/android/allshare/Item$MediaType;->ITEM_AUDIO:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v3}, Lcom/sec/android/allshare/Item$MediaType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 521
    :cond_7
    sget-object v3, Lcom/sec/android/allshare/Item$MediaType;->ITEM_VIDEO:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {p1, v3}, Lcom/sec/android/allshare/media/SearchCriteria;->isMatchedItemType(Lcom/sec/android/allshare/Item$MediaType;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 523
    sget-object v3, Lcom/sec/android/allshare/Item$MediaType;->ITEM_VIDEO:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v3}, Lcom/sec/android/allshare/Item$MediaType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 525
    :cond_8
    sget-object v3, Lcom/sec/android/allshare/Item$MediaType;->ITEM_IMAGE:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {p1, v3}, Lcom/sec/android/allshare/media/SearchCriteria;->isMatchedItemType(Lcom/sec/android/allshare/Item$MediaType;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 527
    sget-object v3, Lcom/sec/android/allshare/Item$MediaType;->ITEM_IMAGE:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v3}, Lcom/sec/android/allshare/Item$MediaType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 529
    :cond_9
    sget-object v3, Lcom/sec/android/allshare/Item$MediaType;->ITEM_FOLDER:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {p1, v3}, Lcom/sec/android/allshare/media/SearchCriteria;->isMatchedItemType(Lcom/sec/android/allshare/Item$MediaType;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 531
    sget-object v3, Lcom/sec/android/allshare/Item$MediaType;->ITEM_FOLDER:Lcom/sec/android/allshare/Item$MediaType;

    invoke-virtual {v3}, Lcom/sec/android/allshare/Item$MediaType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 534
    :cond_a
    const-string v3, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/sec/android/allshare/ProviderImpl;->getID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 535
    const-string v3, "BUNDLE_STRING_SEARCHSTRING"

    invoke-virtual {p1}, Lcom/sec/android/allshare/media/SearchCriteria;->getKeyword()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 536
    const-string v3, "BUNDLE_INT_STARTINDEX"

    invoke-virtual {v1, v3, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 537
    const-string v3, "BUNDLE_INT_REQUESTCOUNT"

    invoke-virtual {v1, v3, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 538
    const-string v3, "BUNDLE_STRING_ITEM_TYPE_ARRAYLIST"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 540
    const-string v2, "ProviderImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "search "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/allshare/ProviderImpl;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", index: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", count: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 541
    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 542
    iget-object v1, p0, Lcom/sec/android/allshare/ProviderImpl;->a:Lcom/sec/android/allshare/IAllShareConnector;

    iget-object v2, p0, Lcom/sec/android/allshare/ProviderImpl;->i:Lcom/sec/android/allshare/e;

    invoke-interface {v1, v0, v2}, Lcom/sec/android/allshare/IAllShareConnector;->a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J

    goto/16 :goto_0
.end method

.method public final setBrowseItemsResponseListener(Lcom/sec/android/allshare/media/Provider$IProviderBrowseResponseListener;)V
    .locals 0

    .prologue
    .line 369
    iput-object p1, p0, Lcom/sec/android/allshare/ProviderImpl;->c:Lcom/sec/android/allshare/media/Provider$IProviderBrowseResponseListener;

    .line 370
    return-void
.end method

.method public final setEventListener(Lcom/sec/android/allshare/media/Provider$IProviderEventListener;)V
    .locals 0

    .prologue
    .line 471
    iput-object p1, p0, Lcom/sec/android/allshare/ProviderImpl;->e:Lcom/sec/android/allshare/media/Provider$IProviderEventListener;

    .line 473
    return-void
.end method

.method public final setSearchResponseListener(Lcom/sec/android/allshare/media/Provider$IProviderSearchResponseListener;)V
    .locals 0

    .prologue
    .line 549
    iput-object p1, p0, Lcom/sec/android/allshare/ProviderImpl;->d:Lcom/sec/android/allshare/media/Provider$IProviderSearchResponseListener;

    .line 551
    return-void
.end method
