.class final Lcom/sec/android/allshare/g;
.super Lcom/sec/android/allshare/DeviceFinder;
.source "ProGuard"


# static fields
.field private static a:Ljava/util/HashMap;

.field private static b:Ljava/util/HashMap;

.field private static synthetic k:[I


# instance fields
.field private c:Ljava/util/HashMap;

.field private d:Ljava/util/HashMap;

.field private e:Ljava/util/HashMap;

.field private f:Ljava/util/HashMap;

.field private g:Ljava/util/HashMap;

.field private h:Ljava/util/HashMap;

.field private i:Lcom/sec/android/allshare/IAllShareConnector;

.field private j:Lcom/sec/android/allshare/d;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 58
    sput-object v3, Lcom/sec/android/allshare/g;->a:Ljava/util/HashMap;

    .line 61
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 62
    sput-object v0, Lcom/sec/android/allshare/g;->a:Ljava/util/HashMap;

    sget-object v1, Lcom/sec/android/allshare/Device$DeviceType;->DEVICE_PROVIDER:Lcom/sec/android/allshare/Device$DeviceType;

    const-string v2, "com.sec.android.allshare.event.EVENT_PROVIDER_DISCOVERY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    sget-object v0, Lcom/sec/android/allshare/g;->a:Ljava/util/HashMap;

    sget-object v1, Lcom/sec/android/allshare/Device$DeviceType;->DEVICE_AVPLAYER:Lcom/sec/android/allshare/Device$DeviceType;

    const-string v2, "com.sec.android.allshare.event.EVENT_AV_PLAYER_DISCOVERY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    sget-object v0, Lcom/sec/android/allshare/g;->a:Ljava/util/HashMap;

    sget-object v1, Lcom/sec/android/allshare/Device$DeviceType;->DEVICE_IMAGEVIEWER:Lcom/sec/android/allshare/Device$DeviceType;

    const-string v2, "com.sec.android.allshare.event.EVENT_IMAGE_VIEWER_DISCOVERY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    sget-object v0, Lcom/sec/android/allshare/g;->a:Ljava/util/HashMap;

    sget-object v1, Lcom/sec/android/allshare/Device$DeviceType;->DEVICE_TV_CONTROLLER:Lcom/sec/android/allshare/Device$DeviceType;

    const-string v2, "com.sec.android.allshare.event.EVENT_SMARTCONTROL_DISCOVERY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    sget-object v0, Lcom/sec/android/allshare/g;->a:Ljava/util/HashMap;

    sget-object v1, Lcom/sec/android/allshare/Device$DeviceType;->DEVICE_FILERECEIVER:Lcom/sec/android/allshare/Device$DeviceType;

    const-string v2, "com.sec.android.allshare.event.EVENT_FILERECEIVER_DISCOVERY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    sput-object v3, Lcom/sec/android/allshare/g;->b:Ljava/util/HashMap;

    .line 73
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 74
    sput-object v0, Lcom/sec/android/allshare/g;->b:Ljava/util/HashMap;

    const-string v1, "com.sec.android.allshare.event.EVENT_PROVIDER_DISCOVERY"

    sget-object v2, Lcom/sec/android/allshare/Device$DeviceType;->DEVICE_PROVIDER:Lcom/sec/android/allshare/Device$DeviceType;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    sget-object v0, Lcom/sec/android/allshare/g;->b:Ljava/util/HashMap;

    const-string v1, "com.sec.android.allshare.event.EVENT_AV_PLAYER_DISCOVERY"

    sget-object v2, Lcom/sec/android/allshare/Device$DeviceType;->DEVICE_AVPLAYER:Lcom/sec/android/allshare/Device$DeviceType;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    sget-object v0, Lcom/sec/android/allshare/g;->b:Ljava/util/HashMap;

    const-string v1, "com.sec.android.allshare.event.EVENT_IMAGE_VIEWER_DISCOVERY"

    sget-object v2, Lcom/sec/android/allshare/Device$DeviceType;->DEVICE_IMAGEVIEWER:Lcom/sec/android/allshare/Device$DeviceType;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    sget-object v0, Lcom/sec/android/allshare/g;->b:Ljava/util/HashMap;

    const-string v1, "com.sec.android.allshare.event.EVENT_SMARTCONTROL_DISCOVERY"

    sget-object v2, Lcom/sec/android/allshare/Device$DeviceType;->DEVICE_TV_CONTROLLER:Lcom/sec/android/allshare/Device$DeviceType;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    sget-object v0, Lcom/sec/android/allshare/g;->b:Ljava/util/HashMap;

    const-string v1, "com.sec.android.allshare.event.EVENT_FILERECEIVER_DISCOVERY"

    sget-object v2, Lcom/sec/android/allshare/Device$DeviceType;->DEVICE_FILERECEIVER:Lcom/sec/android/allshare/Device$DeviceType;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    return-void
.end method

.method constructor <init>(Lcom/sec/android/allshare/IAllShareConnector;)V
    .locals 2

    .prologue
    .line 102
    invoke-direct {p0}, Lcom/sec/android/allshare/DeviceFinder;-><init>()V

    .line 82
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/allshare/g;->c:Ljava/util/HashMap;

    .line 89
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/allshare/g;->d:Ljava/util/HashMap;

    .line 90
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/allshare/g;->e:Ljava/util/HashMap;

    .line 91
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/allshare/g;->f:Ljava/util/HashMap;

    .line 92
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/allshare/g;->g:Ljava/util/HashMap;

    .line 93
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/allshare/g;->h:Ljava/util/HashMap;

    .line 95
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/allshare/g;->i:Lcom/sec/android/allshare/IAllShareConnector;

    .line 113
    new-instance v0, Lcom/sec/android/allshare/h;

    invoke-static {}, Lcom/sec/android/allshare/ServiceConnector;->b()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/allshare/h;-><init>(Lcom/sec/android/allshare/g;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/allshare/g;->j:Lcom/sec/android/allshare/d;

    .line 104
    if-nez p1, :cond_0

    .line 106
    const-string v0, "DeviceFinderImpl"

    const-string v1, "Connection FAIL: AllShare Service Connector does not exist"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    :goto_0
    return-void

    .line 110
    :cond_0
    iput-object p1, p0, Lcom/sec/android/allshare/g;->i:Lcom/sec/android/allshare/IAllShareConnector;

    goto :goto_0
.end method

.method private a(Landroid/os/Bundle;Lcom/sec/android/allshare/Device$DeviceType;)Lcom/sec/android/allshare/Device;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 323
    if-nez p1, :cond_0

    move-object v0, v1

    .line 392
    :goto_0
    return-object v0

    .line 326
    :cond_0
    const-string v0, "BUNDLE_STRING_ID"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 328
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 331
    :cond_1
    const-string v0, "TC"

    const-string v2, "id is null!!"

    invoke-static {v0, v2}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 332
    goto :goto_0

    .line 338
    :cond_2
    :try_start_0
    new-instance v2, Lcom/sec/android/allshare/j;

    invoke-direct {v2, p1}, Lcom/sec/android/allshare/j;-><init>(Landroid/os/Bundle;)V

    .line 340
    invoke-static {}, Lcom/sec/android/allshare/g;->b()[I

    move-result-object v3

    invoke-virtual {p2}, Lcom/sec/android/allshare/Device$DeviceType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    :goto_1
    move-object v0, v1

    .line 392
    goto :goto_0

    .line 343
    :pswitch_0
    iget-object v3, p0, Lcom/sec/android/allshare/g;->e:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 345
    new-instance v3, Lcom/sec/android/allshare/a;

    iget-object v4, p0, Lcom/sec/android/allshare/g;->i:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-direct {v3, v4, v2}, Lcom/sec/android/allshare/a;-><init>(Lcom/sec/android/allshare/IAllShareConnector;Lcom/sec/android/allshare/j;)V

    .line 346
    iget-object v2, p0, Lcom/sec/android/allshare/g;->e:Ljava/util/HashMap;

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 348
    :cond_3
    iget-object v2, p0, Lcom/sec/android/allshare/g;->e:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/allshare/Device;

    goto :goto_0

    .line 350
    :pswitch_1
    iget-object v3, p0, Lcom/sec/android/allshare/g;->f:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 352
    new-instance v3, Lcom/sec/android/allshare/ac;

    iget-object v4, p0, Lcom/sec/android/allshare/g;->i:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-direct {v3, v4, v2}, Lcom/sec/android/allshare/ac;-><init>(Lcom/sec/android/allshare/IAllShareConnector;Lcom/sec/android/allshare/j;)V

    .line 353
    iget-object v2, p0, Lcom/sec/android/allshare/g;->f:Ljava/util/HashMap;

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 355
    :cond_4
    iget-object v2, p0, Lcom/sec/android/allshare/g;->f:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/allshare/Device;

    goto :goto_0

    .line 358
    :pswitch_2
    iget-object v3, p0, Lcom/sec/android/allshare/g;->d:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 360
    new-instance v3, Lcom/sec/android/allshare/ProviderImpl;

    iget-object v4, p0, Lcom/sec/android/allshare/g;->i:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-direct {v3, v4, v2}, Lcom/sec/android/allshare/ProviderImpl;-><init>(Lcom/sec/android/allshare/IAllShareConnector;Lcom/sec/android/allshare/j;)V

    .line 361
    iget-object v2, p0, Lcom/sec/android/allshare/g;->d:Ljava/util/HashMap;

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 363
    :cond_5
    iget-object v2, p0, Lcom/sec/android/allshare/g;->d:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/allshare/Device;

    goto/16 :goto_0

    .line 366
    :pswitch_3
    iget-object v3, p0, Lcom/sec/android/allshare/g;->g:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 368
    new-instance v3, Lcom/sec/android/allshare/TVControllerImpl;

    iget-object v4, p0, Lcom/sec/android/allshare/g;->i:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-direct {v3, v4, v2}, Lcom/sec/android/allshare/TVControllerImpl;-><init>(Lcom/sec/android/allshare/IAllShareConnector;Lcom/sec/android/allshare/j;)V

    .line 370
    iget-object v2, p0, Lcom/sec/android/allshare/g;->g:Ljava/util/HashMap;

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 373
    :cond_6
    iget-object v2, p0, Lcom/sec/android/allshare/g;->g:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/allshare/Device;

    goto/16 :goto_0

    .line 376
    :pswitch_4
    iget-object v3, p0, Lcom/sec/android/allshare/g;->h:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 378
    new-instance v3, Lcom/sec/android/allshare/n;

    iget-object v4, p0, Lcom/sec/android/allshare/g;->i:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-direct {v3, v4, v2}, Lcom/sec/android/allshare/n;-><init>(Lcom/sec/android/allshare/IAllShareConnector;Lcom/sec/android/allshare/j;)V

    .line 380
    iget-object v2, p0, Lcom/sec/android/allshare/g;->h:Ljava/util/HashMap;

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 383
    :cond_7
    iget-object v2, p0, Lcom/sec/android/allshare/g;->h:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/allshare/Device;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 387
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    .line 340
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic a(Lcom/sec/android/allshare/g;Landroid/os/Bundle;Lcom/sec/android/allshare/Device$DeviceType;)Lcom/sec/android/allshare/Device;
    .locals 1

    .prologue
    .line 321
    invoke-direct {p0, p1, p2}, Lcom/sec/android/allshare/g;->a(Landroid/os/Bundle;Lcom/sec/android/allshare/Device$DeviceType;)Lcom/sec/android/allshare/Device;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Lcom/sec/android/allshare/Device$DeviceDomain;Lcom/sec/android/allshare/Device$DeviceType;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 6

    .prologue
    .line 397
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 399
    if-nez p3, :cond_0

    move-object v0, v1

    .line 444
    :goto_0
    return-object v0

    .line 402
    :cond_0
    new-instance v0, Lcom/sec/android/allshare/i;

    const/4 v2, 0x0

    invoke-direct {v0, p0, p1, v2}, Lcom/sec/android/allshare/i;-><init>(Lcom/sec/android/allshare/g;Ljava/lang/String;B)V

    .line 405
    const-string v2, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_GET_DEVICES_BY_DOMAIN_SYNC"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz p2, :cond_1

    .line 407
    const-string v2, "BUNDLE_ENUM_DEVICE_DOMAIN"

    invoke-virtual {p2}, Lcom/sec/android/allshare/Device$DeviceDomain;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/allshare/i;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    const-string v2, "BUNDLE_ENUM_DEVICE_TYPE"

    invoke-virtual {p3}, Lcom/sec/android/allshare/Device$DeviceType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/allshare/i;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    :goto_1
    invoke-virtual {v0}, Lcom/sec/android/allshare/i;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 426
    if-nez v0, :cond_4

    move-object v0, v1

    .line 427
    goto :goto_0

    .line 410
    :cond_1
    const-string v2, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_GET_DEVICES_BY_TYPE_IFACE_SYNC"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz p4, :cond_2

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_2

    .line 412
    const-string v2, "BUNDLE_STRING_BOUND_INTERFACE"

    invoke-virtual {v0, v2, p4}, Lcom/sec/android/allshare/i;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    const-string v2, "BUNDLE_ENUM_DEVICE_TYPE"

    invoke-virtual {p3}, Lcom/sec/android/allshare/Device$DeviceType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/allshare/i;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 415
    :cond_2
    const-string v2, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_GET_DEVICES_SYNC"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 417
    const-string v2, "BUNDLE_ENUM_DEVICE_TYPE"

    invoke-virtual {p3}, Lcom/sec/android/allshare/Device$DeviceType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/allshare/i;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 421
    goto :goto_0

    .line 429
    :cond_4
    const-string v2, "BUNDLE_PARCELABLE_ARRAYLIST_DEVICE"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 431
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_6

    :cond_5
    move-object v0, v1

    .line 432
    goto :goto_0

    .line 434
    :cond_6
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_7
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_8

    move-object v0, v1

    .line 444
    goto :goto_0

    .line 434
    :cond_8
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 436
    invoke-direct {p0, v0, p3}, Lcom/sec/android/allshare/g;->a(Landroid/os/Bundle;Lcom/sec/android/allshare/Device$DeviceType;)Lcom/sec/android/allshare/Device;

    move-result-object v0

    .line 437
    if-eqz v0, :cond_7

    .line 439
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 440
    const-string v3, "DeviceFinderImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "devices : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sec/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method static synthetic a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/sec/android/allshare/g;->b:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/allshare/g;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/android/allshare/g;->c:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/android/allshare/g;)Lcom/sec/android/allshare/IAllShareConnector;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/allshare/g;->i:Lcom/sec/android/allshare/IAllShareConnector;

    return-object v0
.end method

.method static synthetic b(Lcom/sec/android/allshare/g;Landroid/os/Bundle;Lcom/sec/android/allshare/Device$DeviceType;)V
    .locals 3

    .prologue
    .line 273
    if-eqz p1, :cond_0

    const-string v0, "BUNDLE_STRING_ID"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    :try_start_0
    invoke-static {}, Lcom/sec/android/allshare/g;->b()[I

    move-result-object v0

    invoke-virtual {p2}, Lcom/sec/android/allshare/Device$DeviceType;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/allshare/g;->e:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/allshare/a;

    invoke-virtual {v0}, Lcom/sec/android/allshare/a;->removeEventHandler()V

    iget-object v0, p0, Lcom/sec/android/allshare/g;->e:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :pswitch_1
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/allshare/g;->f:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/allshare/ac;

    invoke-virtual {v0}, Lcom/sec/android/allshare/ac;->removeEventHandler()V

    iget-object v0, p0, Lcom/sec/android/allshare/g;->f:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/allshare/g;->d:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/allshare/ProviderImpl;

    invoke-virtual {v0}, Lcom/sec/android/allshare/ProviderImpl;->removeEventHandler()V

    iget-object v0, p0, Lcom/sec/android/allshare/g;->d:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/allshare/g;->g:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/allshare/TVControllerImpl;

    invoke-virtual {v0}, Lcom/sec/android/allshare/TVControllerImpl;->removeEventHandler()V

    iget-object v0, p0, Lcom/sec/android/allshare/g;->g:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/allshare/g;->h:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/allshare/n;

    invoke-virtual {v0}, Lcom/sec/android/allshare/n;->removeEventHandler()V

    iget-object v0, p0, Lcom/sec/android/allshare/g;->h:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method private static synthetic b()[I
    .locals 3

    .prologue
    .line 50
    sget-object v0, Lcom/sec/android/allshare/g;->k:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/sec/android/allshare/Device$DeviceType;->values()[Lcom/sec/android/allshare/Device$DeviceType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/sec/android/allshare/Device$DeviceType;->DEVICE_AVPLAYER:Lcom/sec/android/allshare/Device$DeviceType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Device$DeviceType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_5

    :goto_1
    :try_start_1
    sget-object v1, Lcom/sec/android/allshare/Device$DeviceType;->DEVICE_FILERECEIVER:Lcom/sec/android/allshare/Device$DeviceType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Device$DeviceType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_4

    :goto_2
    :try_start_2
    sget-object v1, Lcom/sec/android/allshare/Device$DeviceType;->DEVICE_IMAGEVIEWER:Lcom/sec/android/allshare/Device$DeviceType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Device$DeviceType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_3

    :goto_3
    :try_start_3
    sget-object v1, Lcom/sec/android/allshare/Device$DeviceType;->DEVICE_PROVIDER:Lcom/sec/android/allshare/Device$DeviceType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Device$DeviceType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_2

    :goto_4
    :try_start_4
    sget-object v1, Lcom/sec/android/allshare/Device$DeviceType;->DEVICE_TV_CONTROLLER:Lcom/sec/android/allshare/Device$DeviceType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Device$DeviceType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_1

    :goto_5
    :try_start_5
    sget-object v1, Lcom/sec/android/allshare/Device$DeviceType;->UNKNOWN:Lcom/sec/android/allshare/Device$DeviceType;

    invoke-virtual {v1}, Lcom/sec/android/allshare/Device$DeviceType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_0

    :goto_6
    sput-object v0, Lcom/sec/android/allshare/g;->k:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_6

    :catch_1
    move-exception v1

    goto :goto_5

    :catch_2
    move-exception v1

    goto :goto_4

    :catch_3
    move-exception v1

    goto :goto_3

    :catch_4
    move-exception v1

    goto :goto_2

    :catch_5
    move-exception v1

    goto :goto_1
.end method


# virtual methods
.method public final getDevice(Ljava/lang/String;Lcom/sec/android/allshare/Device$DeviceType;)Lcom/sec/android/allshare/Device;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 255
    iget-object v1, p0, Lcom/sec/android/allshare/g;->i:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/allshare/g;->i:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v1}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 270
    :cond_0
    :goto_0
    return-object v0

    .line 258
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz p2, :cond_0

    .line 261
    new-instance v1, Lcom/sec/android/allshare/i;

    const-string v2, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_GET_DEVICE_BY_ID_SYNC"

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, v3}, Lcom/sec/android/allshare/i;-><init>(Lcom/sec/android/allshare/g;Ljava/lang/String;B)V

    .line 262
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {v1, v2, p1}, Lcom/sec/android/allshare/i;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    const-string v2, "BUNDLE_ENUM_DEVICE_TYPE"

    invoke-virtual {p2}, Lcom/sec/android/allshare/Device$DeviceType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/allshare/i;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    invoke-virtual {v1}, Lcom/sec/android/allshare/i;->a()Landroid/os/Bundle;

    move-result-object v1

    .line 267
    if-eqz v1, :cond_0

    .line 269
    const-string v0, "BUNDLE_PARCELABLE_DEVICE"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 270
    invoke-direct {p0, v0, p2}, Lcom/sec/android/allshare/g;->a(Landroid/os/Bundle;Lcom/sec/android/allshare/Device$DeviceType;)Lcom/sec/android/allshare/Device;

    move-result-object v0

    goto :goto_0
.end method

.method public final getDevices(Lcom/sec/android/allshare/Device$DeviceDomain;Lcom/sec/android/allshare/Device$DeviceType;)Ljava/util/ArrayList;
    .locals 3

    .prologue
    .line 233
    iget-object v0, p0, Lcom/sec/android/allshare/g;->i:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/g;->i:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 234
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 238
    :goto_0
    return-object v0

    .line 236
    :cond_1
    const-string v0, "DeviceFinderImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getDevices - type : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", domain : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    const-string v0, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_GET_DEVICES_BY_DOMAIN_SYNC"

    .line 238
    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, p2, v1}, Lcom/sec/android/allshare/g;->a(Ljava/lang/String;Lcom/sec/android/allshare/Device$DeviceDomain;Lcom/sec/android/allshare/Device$DeviceType;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public final getDevices(Lcom/sec/android/allshare/Device$DeviceType;)Ljava/util/ArrayList;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 244
    iget-object v0, p0, Lcom/sec/android/allshare/g;->i:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/g;->i:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 245
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 249
    :goto_0
    return-object v0

    .line 247
    :cond_1
    const-string v0, "DeviceFinderImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getDevices - type : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    const-string v0, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_GET_DEVICES_SYNC"

    .line 249
    invoke-direct {p0, v0, v3, p1, v3}, Lcom/sec/android/allshare/g;->a(Ljava/lang/String;Lcom/sec/android/allshare/Device$DeviceDomain;Lcom/sec/android/allshare/Device$DeviceType;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public final getDevices(Lcom/sec/android/allshare/Device$DeviceType;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 3

    .prologue
    .line 222
    iget-object v0, p0, Lcom/sec/android/allshare/g;->i:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/g;->i:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 223
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 227
    :goto_0
    return-object v0

    .line 225
    :cond_1
    const-string v0, "DeviceFinderImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getDevices - type : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", NIC : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    const-string v0, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_GET_DEVICES_BY_TYPE_IFACE_SYNC"

    .line 227
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/sec/android/allshare/g;->a(Ljava/lang/String;Lcom/sec/android/allshare/Device$DeviceDomain;Lcom/sec/android/allshare/Device$DeviceType;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public final refresh()V
    .locals 3

    .prologue
    .line 197
    iget-object v0, p0, Lcom/sec/android/allshare/g;->i:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/g;->i:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 203
    :cond_0
    :goto_0
    return-void

    .line 200
    :cond_1
    const-string v0, "DeviceFinderImpl"

    const-string v1, "refresh"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    new-instance v0, Lcom/sec/android/allshare/i;

    const-string v1, "com.sec.android.allshare.action.ACTION_DEVICE_FINDER_REFRESH"

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/allshare/i;-><init>(Lcom/sec/android/allshare/g;Ljava/lang/String;B)V

    .line 202
    invoke-virtual {v0}, Lcom/sec/android/allshare/i;->a()Landroid/os/Bundle;

    goto :goto_0
.end method

.method public final setDeviceFinderEventListener(Lcom/sec/android/allshare/Device$DeviceType;Lcom/sec/android/allshare/DeviceFinder$IDeviceFinderEventListener;)V
    .locals 4

    .prologue
    .line 208
    iget-object v0, p0, Lcom/sec/android/allshare/g;->i:Lcom/sec/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/g;->i:Lcom/sec/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/sec/android/allshare/IAllShareConnector;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 217
    :cond_0
    :goto_0
    return-void

    .line 211
    :cond_1
    if-eqz p1, :cond_0

    .line 214
    iget-object v1, p0, Lcom/sec/android/allshare/g;->c:Ljava/util/HashMap;

    sget-object v0, Lcom/sec/android/allshare/g;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 216
    iget-object v1, p0, Lcom/sec/android/allshare/g;->i:Lcom/sec/android/allshare/IAllShareConnector;

    sget-object v0, Lcom/sec/android/allshare/g;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/allshare/g;->j:Lcom/sec/android/allshare/d;

    invoke-interface {v1, v0, v2, v3}, Lcom/sec/android/allshare/IAllShareConnector;->a(Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/allshare/d;)Z

    goto :goto_0
.end method
