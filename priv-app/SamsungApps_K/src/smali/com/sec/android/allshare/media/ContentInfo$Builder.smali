.class public Lcom/sec/android/allshare/media/ContentInfo$Builder;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private a:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/allshare/media/ContentInfo$Builder;->a:J

    .line 65
    return-void
.end method

.method static synthetic a(Lcom/sec/android/allshare/media/ContentInfo$Builder;)J
    .locals 2

    .prologue
    .line 57
    iget-wide v0, p0, Lcom/sec/android/allshare/media/ContentInfo$Builder;->a:J

    return-wide v0
.end method


# virtual methods
.method public build()Lcom/sec/android/allshare/media/ContentInfo;
    .locals 4

    .prologue
    .line 91
    iget-wide v0, p0, Lcom/sec/android/allshare/media/ContentInfo$Builder;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 92
    const/4 v0, 0x0

    .line 94
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/sec/android/allshare/media/ContentInfo;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/allshare/media/ContentInfo;-><init>(Lcom/sec/android/allshare/media/ContentInfo$Builder;B)V

    goto :goto_0
.end method

.method public setStartingPosition(J)Lcom/sec/android/allshare/media/ContentInfo$Builder;
    .locals 0

    .prologue
    .line 78
    iput-wide p1, p0, Lcom/sec/android/allshare/media/ContentInfo$Builder;->a:J

    .line 79
    return-object p0
.end method
