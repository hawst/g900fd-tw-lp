.class final Lcom/sec/android/allshare/c;
.super Lcom/sec/android/allshare/e;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/allshare/a;


# direct methods
.method constructor <init>(Lcom/sec/android/allshare/a;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/allshare/c;->a:Lcom/sec/android/allshare/a;

    .line 716
    invoke-direct {p0, p2}, Lcom/sec/android/allshare/e;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 10

    .prologue
    .line 722
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v2

    .line 723
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v3

    .line 724
    if-eqz v2, :cond_0

    if-nez v3, :cond_1

    .line 815
    :cond_0
    :goto_0
    return-void

    .line 728
    :cond_1
    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_PLAY"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_PLAY_LOCAL_CONTENS_URI"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_PLAY_LOCAL_CONTENS_FILEPATH_WITH_METADATA"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_PLAY_LOCAL_CONTENS_FILEPATH"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_PLAY_WEB_CONTENTS"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_PLAY_URI"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 730
    :cond_2
    iget-object v0, p0, Lcom/sec/android/allshare/c;->a:Lcom/sec/android/allshare/a;

    const-string v1, "BUNDLE_STRING_SERVER_URI_LIST"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/allshare/a;->a:Ljava/util/ArrayList;

    .line 733
    :cond_3
    const-string v0, "BUNDLE_ENUM_ERROR"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/allshare/ERROR;->valueOf(Ljava/lang/String;)Lcom/sec/android/allshare/ERROR;

    move-result-object v4

    .line 735
    iget-object v0, p0, Lcom/sec/android/allshare/c;->a:Lcom/sec/android/allshare/a;

    invoke-static {v0}, Lcom/sec/android/allshare/a;->b(Lcom/sec/android/allshare/a;)Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 737
    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_PLAY"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_PLAY_LOCAL_CONTENS_URI"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_PLAY_LOCAL_CONTENS_FILEPATH_WITH_METADATA"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_PLAY_LOCAL_CONTENS_FILEPATH"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_PLAY_WEB_CONTENTS"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_PLAY_URI"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 739
    :cond_4
    const-string v0, "BUNDLE_PARCELABLE_ITEM"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    const-string v1, "BUNDLE_PARCELABLE_CONTENT_INFO"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/sec/android/allshare/media/ContentInfo;

    invoke-static {v0}, Lcom/sec/android/allshare/ag;->a(Landroid/os/Bundle;)Lcom/sec/android/allshare/Item;

    move-result-object v5

    const-string v6, "BUNDLE_STRING_ITEM_CONSTRUCTOR_KEY"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    const-string v6, "WEB_CONTENT"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    if-eqz v1, :cond_5

    new-instance v0, Lcom/sec/android/allshare/media/ContentInfo$Builder;

    invoke-direct {v0}, Lcom/sec/android/allshare/media/ContentInfo$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/sec/android/allshare/media/ContentInfo;->getStartingPosition()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    long-to-int v1, v6

    int-to-long v6, v1

    invoke-virtual {v0, v6, v7}, Lcom/sec/android/allshare/media/ContentInfo$Builder;->setStartingPosition(J)Lcom/sec/android/allshare/media/ContentInfo$Builder;

    invoke-virtual {v0}, Lcom/sec/android/allshare/media/ContentInfo$Builder;->build()Lcom/sec/android/allshare/media/ContentInfo;

    move-result-object v1

    :cond_5
    if-nez v5, :cond_7

    iget-object v0, p0, Lcom/sec/android/allshare/c;->a:Lcom/sec/android/allshare/a;

    invoke-static {v0}, Lcom/sec/android/allshare/a;->b(Lcom/sec/android/allshare/a;)Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    move-result-object v0

    sget-object v6, Lcom/sec/android/allshare/ERROR;->ITEM_NOT_EXIST:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v5, v1, v6}, Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;->onPlayResponseReceived(Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/media/ContentInfo;Lcom/sec/android/allshare/ERROR;)V

    .line 791
    :cond_6
    :goto_1
    iget-object v0, p0, Lcom/sec/android/allshare/c;->a:Lcom/sec/android/allshare/a;

    invoke-static {v0}, Lcom/sec/android/allshare/a;->c(Lcom/sec/android/allshare/a;)Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 793
    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_REQUEST_GET_VOLUME"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 795
    const-string v0, "BUNDLE_INT_VOLUME"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 796
    iget-object v1, p0, Lcom/sec/android/allshare/c;->a:Lcom/sec/android/allshare/a;

    invoke-static {v1}, Lcom/sec/android/allshare/a;->c(Lcom/sec/android/allshare/a;)Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;

    move-result-object v1

    invoke-interface {v1, v0, v4}, Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;->onGetVolumeResponseReceived(ILcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 739
    :cond_7
    iget-object v0, p0, Lcom/sec/android/allshare/c;->a:Lcom/sec/android/allshare/a;

    invoke-static {v0}, Lcom/sec/android/allshare/a;->b(Lcom/sec/android/allshare/a;)Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    move-result-object v0

    invoke-interface {v0, v5, v1, v4}, Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;->onPlayResponseReceived(Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/media/ContentInfo;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_1

    .line 741
    :cond_8
    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_STOP"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 743
    iget-object v0, p0, Lcom/sec/android/allshare/c;->a:Lcom/sec/android/allshare/a;

    invoke-static {v0}, Lcom/sec/android/allshare/a;->b(Lcom/sec/android/allshare/a;)Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    move-result-object v0

    invoke-interface {v0, v4}, Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;->onStopResponseReceived(Lcom/sec/android/allshare/ERROR;)V

    goto :goto_1

    .line 745
    :cond_9
    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_SEEK"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 747
    const-string v0, "BUNDLE_LONG_POSITION"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 748
    iget-object v5, p0, Lcom/sec/android/allshare/c;->a:Lcom/sec/android/allshare/a;

    invoke-static {v5}, Lcom/sec/android/allshare/a;->b(Lcom/sec/android/allshare/a;)Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    move-result-object v5

    invoke-interface {v5, v0, v1, v4}, Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;->onSeekResponseReceived(JLcom/sec/android/allshare/ERROR;)V

    goto :goto_1

    .line 750
    :cond_a
    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_PAUSE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 752
    iget-object v0, p0, Lcom/sec/android/allshare/c;->a:Lcom/sec/android/allshare/a;

    invoke-static {v0}, Lcom/sec/android/allshare/a;->b(Lcom/sec/android/allshare/a;)Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    move-result-object v0

    invoke-interface {v0, v4}, Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;->onPauseResponseReceived(Lcom/sec/android/allshare/ERROR;)V

    goto :goto_1

    .line 754
    :cond_b
    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_RESUME"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 756
    iget-object v0, p0, Lcom/sec/android/allshare/c;->a:Lcom/sec/android/allshare/a;

    invoke-static {v0}, Lcom/sec/android/allshare/a;->b(Lcom/sec/android/allshare/a;)Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    move-result-object v0

    invoke-interface {v0, v4}, Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;->onResumeResponseReceived(Lcom/sec/android/allshare/ERROR;)V

    goto :goto_1

    .line 758
    :cond_c
    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_REQUEST_PLAY_POSITION"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 760
    const-string v0, "BUNDLE_LONG_POSITION"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 761
    iget-object v5, p0, Lcom/sec/android/allshare/c;->a:Lcom/sec/android/allshare/a;

    invoke-static {v5}, Lcom/sec/android/allshare/a;->b(Lcom/sec/android/allshare/a;)Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    move-result-object v5

    invoke-interface {v5, v0, v1, v4}, Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;->onGetPlayPositionResponseReceived(JLcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_1

    .line 763
    :cond_d
    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_REQUEST_GET_MEDIA_INFO"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 765
    const/4 v0, 0x0

    .line 767
    sget-object v1, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    invoke-virtual {v1, v4}, Lcom/sec/android/allshare/ERROR;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 768
    new-instance v0, Lcom/sec/android/allshare/aj;

    invoke-direct {v0, v3}, Lcom/sec/android/allshare/aj;-><init>(Landroid/os/Bundle;)V

    .line 770
    :cond_e
    iget-object v1, p0, Lcom/sec/android/allshare/c;->a:Lcom/sec/android/allshare/a;

    invoke-static {v1}, Lcom/sec/android/allshare/a;->b(Lcom/sec/android/allshare/a;)Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    move-result-object v1

    invoke-interface {v1, v0, v4}, Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;->onGetMediaInfoResponseReceived(Lcom/sec/android/allshare/media/MediaInfo;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_1

    .line 772
    :cond_f
    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_REQUEST_GET_PLAYER_STATE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 774
    const-string v0, "BUNDLE_STRING_AV_PLAER_STATE"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 776
    sget-object v1, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;->UNKNOWN:Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

    .line 780
    :try_start_0
    invoke-static {v0}, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;->valueOf(Ljava/lang/String;)Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 787
    :goto_2
    iget-object v1, p0, Lcom/sec/android/allshare/c;->a:Lcom/sec/android/allshare/a;

    invoke-static {v1}, Lcom/sec/android/allshare/a;->b(Lcom/sec/android/allshare/a;)Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    move-result-object v1

    invoke-interface {v1, v0, v4}, Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;->onGetStateResponseReceived(Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_1

    .line 784
    :catch_0
    move-exception v0

    sget-object v0, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;->UNKNOWN:Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

    goto :goto_2

    .line 798
    :cond_10
    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_REQUEST_SET_VOLUME"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 800
    const-string v0, "BUNDLE_INT_VOLUME"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 801
    iget-object v1, p0, Lcom/sec/android/allshare/c;->a:Lcom/sec/android/allshare/a;

    invoke-static {v1}, Lcom/sec/android/allshare/a;->c(Lcom/sec/android/allshare/a;)Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;

    move-result-object v1

    invoke-interface {v1, v0, v4}, Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;->onSetVolumeResponseReceived(ILcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 803
    :cond_11
    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_REQUEST_GET_MUTE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 805
    const-string v0, "BUNDLE_BOOLEAN_MUTE"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 806
    iget-object v1, p0, Lcom/sec/android/allshare/c;->a:Lcom/sec/android/allshare/a;

    invoke-static {v1}, Lcom/sec/android/allshare/a;->c(Lcom/sec/android/allshare/a;)Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;

    move-result-object v1

    invoke-interface {v1, v0, v4}, Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;->onGetMuteResponseReceived(ZLcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 808
    :cond_12
    const-string v0, "com.sec.android.allshare.action.ACTION_AV_PLAYER_REQUEST_SET_MUTE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 810
    const-string v0, "BUNDLE_BOOLEAN_MUTE"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 811
    iget-object v1, p0, Lcom/sec/android/allshare/c;->a:Lcom/sec/android/allshare/a;

    invoke-static {v1}, Lcom/sec/android/allshare/a;->c(Lcom/sec/android/allshare/a;)Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;

    move-result-object v1

    invoke-interface {v1, v0, v4}, Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerVolumeResponseListener;->onSetMuteResponseReceived(ZLcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0
.end method
