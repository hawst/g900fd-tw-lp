.class public abstract Lcom/sec/android/allshare/Item;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/os/Parcelable;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    return-void
.end method

.method static synthetic a(Ljava/lang/String;)Lcom/sec/android/allshare/Item$MediaType;
    .locals 2

    .prologue
    .line 678
    if-eqz p0, :cond_3

    new-instance v0, Ljava/util/StringTokenizer;

    const-string v1, "/"

    invoke-direct {v0, p0, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v1, "video"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v0, Lcom/sec/android/allshare/Item$MediaType;->ITEM_VIDEO:Lcom/sec/android/allshare/Item$MediaType;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/allshare/Item$MediaType;->ITEM_UNKNOWN:Lcom/sec/android/allshare/Item$MediaType;

    goto :goto_0

    :cond_1
    const-string v1, "audio"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v0, Lcom/sec/android/allshare/Item$MediaType;->ITEM_AUDIO:Lcom/sec/android/allshare/Item$MediaType;

    goto :goto_0

    :cond_2
    const-string v1, "image"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/sec/android/allshare/Item$MediaType;->ITEM_IMAGE:Lcom/sec/android/allshare/Item$MediaType;

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/sec/android/allshare/Item$MediaType;->ITEM_UNKNOWN:Lcom/sec/android/allshare/Item$MediaType;

    goto :goto_0
.end method


# virtual methods
.method public abstract getAlbumTitle()Ljava/lang/String;
.end method

.method public abstract getArtist()Ljava/lang/String;
.end method

.method public abstract getContentBuildType()Lcom/sec/android/allshare/Item$ContentBuildType;
.end method

.method public abstract getDate()Ljava/util/Date;
.end method

.method public abstract getDuration()J
.end method

.method public abstract getExtension()Ljava/lang/String;
.end method

.method public abstract getFileSize()J
.end method

.method public abstract getGenre()Ljava/lang/String;
.end method

.method public abstract getLocation()Landroid/location/Location;
.end method

.method public abstract getMimetype()Ljava/lang/String;
.end method

.method public abstract getResolution()Ljava/lang/String;
.end method

.method public abstract getSubtitle()Landroid/net/Uri;
.end method

.method public abstract getThumbnail()Landroid/net/Uri;
.end method

.method public abstract getTitle()Ljava/lang/String;
.end method

.method public abstract getType()Lcom/sec/android/allshare/Item$MediaType;
.end method

.method public abstract getURI()Landroid/net/Uri;
.end method

.method public abstract isRootFolder()Z
.end method
