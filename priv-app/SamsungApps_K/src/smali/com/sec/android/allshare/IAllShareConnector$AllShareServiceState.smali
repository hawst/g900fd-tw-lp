.class public final enum Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum ALLSHARE_SERVICE_CONNECTED:Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;

.field public static final enum ALLSHARE_SERVICE_DISCONNECTED:Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;

.field private static final synthetic a:[Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 43
    new-instance v0, Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;

    const-string v1, "ALLSHARE_SERVICE_CONNECTED"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;->ALLSHARE_SERVICE_CONNECTED:Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;

    new-instance v0, Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;

    const-string v1, "ALLSHARE_SERVICE_DISCONNECTED"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;->ALLSHARE_SERVICE_DISCONNECTED:Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;

    .line 41
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;

    sget-object v1, Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;->ALLSHARE_SERVICE_CONNECTED:Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;->ALLSHARE_SERVICE_DISCONNECTED:Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;->a:[Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;->a:[Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
