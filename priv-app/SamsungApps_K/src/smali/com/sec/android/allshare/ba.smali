.class final Lcom/sec/android/allshare/ba;
.super Lcom/sec/android/allshare/e;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/allshare/TVControllerImpl;


# direct methods
.method constructor <init>(Lcom/sec/android/allshare/TVControllerImpl;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    .line 257
    invoke-direct {p0, p2}, Lcom/sec/android/allshare/e;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 5

    .prologue
    .line 263
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v0

    .line 264
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v1

    .line 266
    const-string v2, "AllShareResponseHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[TVControl] Response : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    const-string v2, "com.sec.android.allshare.action.ACTION_CONTROL_TV_RUN_BROWSER"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 270
    const-string v0, "BUNDLE_STRING_BROWSER_OPEN"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 271
    const-string v2, "BUNDLE_STRING_BROWSER_URL"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 272
    if-eqz v0, :cond_1

    const-string v2, "OK"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 274
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 276
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v3, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v2, v1, v3}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onOpenWebPageResponseReceived(Lcom/sec/android/allshare/control/TVController;Ljava/lang/String;Lcom/sec/android/allshare/ERROR;)V

    .line 617
    :cond_0
    :goto_0
    return-void

    .line 281
    :cond_1
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 283
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v3, Lcom/sec/android/allshare/ERROR;->FAIL:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v2, v1, v3}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onOpenWebPageResponseReceived(Lcom/sec/android/allshare/control/TVController;Ljava/lang/String;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 287
    :cond_2
    const-string v2, "com.sec.android.allshare.action.ACTION_CONTROL_TV_STOP_BROWSER"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 289
    const-string v0, "BUNDLE_STRING_BROWSER_STOP"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 290
    if-eqz v0, :cond_3

    const-string v1, "OK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 292
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v2, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onCloseWebPageResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 299
    :cond_3
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 301
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v2, Lcom/sec/android/allshare/ERROR;->FAIL:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onCloseWebPageResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto :goto_0

    .line 305
    :cond_4
    const-string v2, "com.sec.android.allshare.action.ACTION_CONTROL_TV_REQUESTCURRENTURL"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 307
    const-string v0, "BUNDLE_STRING_BROWSER_URL"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 309
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_5

    .line 311
    iget-object v1, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v1}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 313
    iget-object v1, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v1}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v3, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v1, v2, v0, v3}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onGetBrowserURLResponseReceived(Lcom/sec/android/allshare/control/TVController;Ljava/lang/String;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 318
    :cond_5
    iget-object v1, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v1}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 320
    iget-object v1, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v1}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v3, Lcom/sec/android/allshare/ERROR;->FAIL:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v1, v2, v0, v3}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onGetBrowserURLResponseReceived(Lcom/sec/android/allshare/control/TVController;Ljava/lang/String;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 324
    :cond_6
    const-string v2, "com.sec.android.allshare.action.ACTION_CONTROL_TV_GETBROWSERMODE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 326
    const-string v0, "BUNDLE_STRING_BROWSER_MODE"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 327
    const-string v1, "AllShareResponseHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[TVControl] BrowserMode : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    iget-object v1, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v1}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 330
    const-string v1, "Tab"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 332
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 334
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v2, Lcom/sec/android/allshare/control/TVController$BrowserMode;->LINK_BROWSING:Lcom/sec/android/allshare/control/TVController$BrowserMode;

    sget-object v3, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onGetBrowserModeResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/control/TVController$BrowserMode;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 337
    :cond_7
    const-string v1, "Pointer"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 339
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v2, Lcom/sec/android/allshare/control/TVController$BrowserMode;->POINT_BROWSING:Lcom/sec/android/allshare/control/TVController$BrowserMode;

    sget-object v3, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onGetBrowserModeResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/control/TVController$BrowserMode;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 346
    :cond_8
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 348
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v2, Lcom/sec/android/allshare/control/TVController$BrowserMode;->UNKNOWN:Lcom/sec/android/allshare/control/TVController$BrowserMode;

    sget-object v3, Lcom/sec/android/allshare/ERROR;->FAIL:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onGetBrowserModeResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/control/TVController$BrowserMode;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 353
    :cond_9
    const-string v2, "com.sec.android.allshare.action.ACTION_CONTROL_TV_GET_DTV_INFORMATION"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 355
    const-string v0, "AllShareResponseHandler"

    const-string v2, "[TVControl] GetDTVInformation response"

    invoke-static {v0, v2}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    const-string v0, "BUNDLE_STRING_DTV_INFORMATION"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 358
    iget-object v1, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v1}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 360
    new-instance v1, Lcom/sec/android/allshare/TVControllerImpl$TVInformationImpl;

    iget-object v2, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-direct {v1, v2, v0}, Lcom/sec/android/allshare/TVControllerImpl$TVInformationImpl;-><init>(Lcom/sec/android/allshare/TVControllerImpl;Ljava/lang/String;)V

    .line 361
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v3, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v2, v1, v3}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onGetTVInformationResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/control/TVController$TVInformation;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 364
    :cond_a
    const-string v2, "com.sec.android.allshare.action.ACTION_CONTROL_TV_GENERAL_UPNP_ACTION"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 372
    const-string v2, "com.sec.android.allshare.action.ACTION_CONTROL_TV_GET_DTV_INIT_INFO"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 374
    const-string v0, "AllShareResponseHandler"

    const-string v2, "[TVControl] GetDTVInitInformation response"

    invoke-static {v0, v2}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    const-string v0, "BUNDLE_STRING_DTV_INFORMATION"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 377
    new-instance v1, Lcom/sec/android/allshare/TVControllerImpl$TVInformationImpl;

    iget-object v2, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-direct {v1, v2, v0}, Lcom/sec/android/allshare/TVControllerImpl$TVInformationImpl;-><init>(Lcom/sec/android/allshare/TVControllerImpl;Ljava/lang/String;)V

    .line 379
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0, v1}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;Lcom/sec/android/allshare/control/TVController$TVInformation;)V

    .line 381
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->b(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IEventListener;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 383
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->f(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$TVInformation;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 385
    const-string v0, "AllShareResponseHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[TVControl] Version : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v2}, Lcom/sec/android/allshare/TVControllerImpl;->f(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$TVInformation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/allshare/control/TVController$TVInformation;->getVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    :cond_b
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->b(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IEventListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v2, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$IEventListener;->onConnected(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    .line 391
    :cond_c
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 393
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v2, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onConnectResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    .line 396
    :cond_d
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;Z)V

    goto/16 :goto_0

    .line 398
    :cond_e
    const-string v2, "com.sec.android.allshare.action.ACTION_CONTROL_TV_GOHOME_BROWSER"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 400
    const-string v0, "BUNDLE_STRING_TV_IVY_RESULT"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 401
    if-eqz v0, :cond_f

    const-string v1, "OK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 403
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 405
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v2, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onGoHomePageResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 410
    :cond_f
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 412
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v2, Lcom/sec/android/allshare/ERROR;->FAIL:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onGoHomePageResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 416
    :cond_10
    const-string v2, "com.sec.android.allshare.action.ACTION_CONTROL_TV_REQUESTBROWSER_REFRESH"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 418
    const-string v0, "BUNDLE_STRING_TV_IVY_RESULT"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 419
    if-eqz v0, :cond_11

    const-string v1, "OK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 421
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 423
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v2, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onRefreshWebPageResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 428
    :cond_11
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 430
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v2, Lcom/sec/android/allshare/ERROR;->FAIL:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onRefreshWebPageResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 434
    :cond_12
    const-string v2, "com.sec.android.allshare.action.ACTION_CONTROL_TV_REQUESTBROWSER_STOPPAGE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 436
    const-string v0, "BUNDLE_STRING_TV_IVY_RESULT"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 437
    if-eqz v0, :cond_13

    const-string v1, "OK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 439
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 441
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v2, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onStopWebPageResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 446
    :cond_13
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 448
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v2, Lcom/sec/android/allshare/ERROR;->FAIL:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onStopWebPageResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 452
    :cond_14
    const-string v2, "com.sec.android.allshare.action.ACTION_CONTROL_TV_REQUESTBROWSER_NEXTPAGE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 454
    const-string v0, "BUNDLE_STRING_TV_IVY_RESULT"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 455
    if-eqz v0, :cond_15

    const-string v1, "OK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 457
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 459
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v2, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onGoNextPageResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 464
    :cond_15
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 466
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v2, Lcom/sec/android/allshare/ERROR;->FAIL:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onGoNextPageResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 470
    :cond_16
    const-string v2, "com.sec.android.allshare.action.ACTION_CONTROL_TV_REQUESTBROWSER_PREVIOUSPAGE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_18

    .line 472
    const-string v0, "BUNDLE_STRING_TV_IVY_RESULT"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 473
    if-eqz v0, :cond_17

    const-string v1, "OK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 475
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 477
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v2, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onGoPreviousPageResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 482
    :cond_17
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 484
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v2, Lcom/sec/android/allshare/ERROR;->FAIL:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onGoPreviousPageResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 488
    :cond_18
    const-string v2, "com.sec.android.allshare.action.ACTION_CONTROL_TV_REQUESTBROWSER_ZOOMIN"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 490
    const-string v0, "BUNDLE_STRING_TV_IVY_RESULT"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 491
    if-eqz v0, :cond_19

    const-string v1, "OK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 493
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 495
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v2, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onBrowserZoomInResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 500
    :cond_19
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 502
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v2, Lcom/sec/android/allshare/ERROR;->FAIL:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onBrowserZoomInResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 506
    :cond_1a
    const-string v2, "com.sec.android.allshare.action.ACTION_CONTROL_TV_REQUESTBROWSER_ZOOMOUT"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 508
    const-string v0, "BUNDLE_STRING_TV_IVY_RESULT"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 509
    if-eqz v0, :cond_1b

    const-string v1, "OK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 511
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 513
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v2, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onBrowserZoomOutResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 518
    :cond_1b
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 520
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v2, Lcom/sec/android/allshare/ERROR;->FAIL:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onBrowserZoomOutResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 524
    :cond_1c
    const-string v2, "com.sec.android.allshare.action.ACTION_CONTROL_TV_REQUESTBROWSER_ZOOMDEFAULT"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 526
    const-string v0, "BUNDLE_STRING_TV_IVY_RESULT"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 527
    if-eqz v0, :cond_1d

    const-string v1, "OK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 529
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 531
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v2, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onBrowserZoomDefaultResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 536
    :cond_1d
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 538
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v2, Lcom/sec/android/allshare/ERROR;->FAIL:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onBrowserZoomDefaultResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 542
    :cond_1e
    const-string v2, "com.sec.android.allshare.action.ACTION_CONTROL_TV_REQUESTBROWSER_MODE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_22

    .line 544
    const-string v0, "BUNDLE_STRING_TV_IVY_RESULT"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 545
    const-string v0, "BUNDLE_STRING_BROWSER_INPUT_MODE"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 546
    sget-object v0, Lcom/sec/android/allshare/control/TVController$BrowserMode;->UNKNOWN:Lcom/sec/android/allshare/control/TVController$BrowserMode;

    .line 548
    if-eqz v1, :cond_1f

    .line 550
    const-string v3, "SetTabMode"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_20

    .line 552
    sget-object v0, Lcom/sec/android/allshare/control/TVController$BrowserMode;->LINK_BROWSING:Lcom/sec/android/allshare/control/TVController$BrowserMode;

    .line 560
    :cond_1f
    :goto_1
    if-eqz v2, :cond_21

    const-string v1, "OK"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_21

    .line 562
    iget-object v1, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v1}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 564
    iget-object v1, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v1}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v3, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v1, v2, v0, v3}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onSetBrowserModeResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/control/TVController$BrowserMode;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 554
    :cond_20
    const-string v3, "SetPointerMode"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 556
    sget-object v0, Lcom/sec/android/allshare/control/TVController$BrowserMode;->POINT_BROWSING:Lcom/sec/android/allshare/control/TVController$BrowserMode;

    goto :goto_1

    .line 569
    :cond_21
    iget-object v1, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v1}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 571
    iget-object v1, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v1}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v3, Lcom/sec/android/allshare/ERROR;->FAIL:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v1, v2, v0, v3}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onSetBrowserModeResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/control/TVController$BrowserMode;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 575
    :cond_22
    const-string v2, "com.sec.android.allshare.action.ACTION_CONTROL_TV_BROWSER_SCROLLUP"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_24

    .line 577
    const-string v0, "BUNDLE_STRING_BROWSER_URL"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 579
    if-eqz v0, :cond_23

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_23

    .line 581
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->c(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/IAppControlAPI;

    move-result-object v0

    const-string v1, "KEY_REWIND"

    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/IAppControlAPI;->b(Ljava/lang/String;)V

    .line 583
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 585
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v2, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onBrowserScrollUpResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 590
    :cond_23
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 592
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v2, Lcom/sec/android/allshare/ERROR;->FAIL:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onBrowserScrollUpResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 596
    :cond_24
    const-string v2, "com.sec.android.allshare.action.ACTION_CONTROL_TV_BROWSER_SCROLLDOWN"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 598
    const-string v0, "BUNDLE_STRING_BROWSER_URL"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 600
    if-eqz v0, :cond_25

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_25

    .line 602
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->c(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/IAppControlAPI;

    move-result-object v0

    const-string v1, "KEY_FF"

    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/IAppControlAPI;->b(Ljava/lang/String;)V

    .line 604
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 606
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v2, Lcom/sec/android/allshare/ERROR;->SUCCESS:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onBrowserScrollDownResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 611
    :cond_25
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 613
    iget-object v0, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->a(Lcom/sec/android/allshare/TVControllerImpl;)Lcom/sec/android/allshare/control/TVController$IResponseListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/allshare/ba;->a:Lcom/sec/android/allshare/TVControllerImpl;

    sget-object v2, Lcom/sec/android/allshare/ERROR;->FAIL:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/allshare/control/TVController$IResponseListener;->onBrowserScrollDownResponseReceived(Lcom/sec/android/allshare/control/TVController;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0
.end method
