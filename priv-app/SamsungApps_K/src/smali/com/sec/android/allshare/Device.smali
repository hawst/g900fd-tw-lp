.class public abstract Lcom/sec/android/allshare/Device;
.super Ljava/lang/Object;
.source "ProGuard"


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    return-void
.end method


# virtual methods
.method public abstract getDeviceDomain()Lcom/sec/android/allshare/Device$DeviceDomain;
.end method

.method public abstract getDeviceType()Lcom/sec/android/allshare/Device$DeviceType;
.end method

.method public abstract getID()Ljava/lang/String;
.end method

.method public abstract getIPAdress()Ljava/lang/String;
.end method

.method public abstract getIcon()Landroid/net/Uri;
.end method

.method public abstract getIconList()Ljava/util/ArrayList;
.end method

.method public abstract getModelName()Ljava/lang/String;
.end method

.method public abstract getNIC()Ljava/lang/String;
.end method

.method public abstract getName()Ljava/lang/String;
.end method
