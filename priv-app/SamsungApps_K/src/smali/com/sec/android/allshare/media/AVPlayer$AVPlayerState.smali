.class public final enum Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum BUFFERING:Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

.field public static final enum CONTENT_CHANGED:Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

.field public static final enum PAUSED:Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

.field public static final enum PLAYING:Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

.field public static final enum STOPPED:Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

.field public static final enum UNKNOWN:Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

.field private static final synthetic a:[Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 37
    new-instance v0, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

    const-string v1, "STOPPED"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;-><init>(Ljava/lang/String;I)V

    .line 44
    sput-object v0, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;->STOPPED:Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

    .line 45
    new-instance v0, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

    const-string v1, "BUFFERING"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;-><init>(Ljava/lang/String;I)V

    .line 52
    sput-object v0, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;->BUFFERING:Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

    .line 53
    new-instance v0, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

    const-string v1, "PLAYING"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;-><init>(Ljava/lang/String;I)V

    .line 59
    sput-object v0, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;->PLAYING:Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

    .line 60
    new-instance v0, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;-><init>(Ljava/lang/String;I)V

    .line 67
    sput-object v0, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;->PAUSED:Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

    .line 68
    new-instance v0, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

    const-string v1, "CONTENT_CHANGED"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;-><init>(Ljava/lang/String;I)V

    .line 78
    sput-object v0, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;->CONTENT_CHANGED:Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

    .line 79
    new-instance v0, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;-><init>(Ljava/lang/String;I)V

    .line 85
    sput-object v0, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;->UNKNOWN:Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

    .line 35
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

    sget-object v1, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;->STOPPED:Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;->BUFFERING:Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;->PLAYING:Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;->PAUSED:Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;->CONTENT_CHANGED:Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;->UNKNOWN:Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;->a:[Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;->a:[Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
