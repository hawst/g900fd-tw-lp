.class final Lcom/sec/android/allshare/o;
.super Lcom/sec/android/allshare/e;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/allshare/n;


# direct methods
.method constructor <init>(Lcom/sec/android/allshare/n;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/allshare/o;->a:Lcom/sec/android/allshare/n;

    .line 309
    invoke-direct {p0, p2}, Lcom/sec/android/allshare/e;-><init>(Landroid/os/Looper;)V

    return-void
.end method

.method private a(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 336
    const-string v0, "BUNDLE_ENUM_ERROR"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 337
    const-string v1, "BUNDLE_STRING_NAME"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 339
    const-string v1, "BUNDLE_STRING_SESSIONID"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 340
    const-string v1, "BUNDLE_STRING_UNIQUEKEY"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 342
    const-string v3, "FileReceiverImpl"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "sessionID : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    const-string v3, "FileReceiverImpl"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "action : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    invoke-static {v0}, Lcom/sec/android/allshare/ERROR;->valueOf(Ljava/lang/String;)Lcom/sec/android/allshare/ERROR;

    move-result-object v5

    .line 346
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 348
    const-string v0, "com.sec.android.allshare.action.ACTION_FILE_ARRAYLIST_RECEIVER_RECEIVE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 350
    const-string v0, "FileReceiverImpl"

    const-string v6, "notifyListResponse() ACTION_FILE_ARRAYLIST_RECEIVER_RECEIVE"

    invoke-static {v0, v6}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    iget-object v0, p0, Lcom/sec/android/allshare/o;->a:Lcom/sec/android/allshare/n;

    invoke-static {v0, v1}, Lcom/sec/android/allshare/n;->a(Lcom/sec/android/allshare/n;Ljava/lang/String;)Lcom/sec/android/allshare/x;

    move-result-object v0

    .line 354
    iget-object v6, p0, Lcom/sec/android/allshare/o;->a:Lcom/sec/android/allshare/n;

    invoke-static {v6, v2, v0}, Lcom/sec/android/allshare/n;->a(Lcom/sec/android/allshare/n;Ljava/lang/String;Lcom/sec/android/allshare/x;)V

    .line 357
    iget-object v0, p0, Lcom/sec/android/allshare/o;->a:Lcom/sec/android/allshare/n;

    invoke-static {v0, v1}, Lcom/sec/android/allshare/n;->b(Lcom/sec/android/allshare/n;Ljava/lang/String;)V

    .line 359
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/allshare/o;->a:Lcom/sec/android/allshare/n;

    invoke-static {v0, v2}, Lcom/sec/android/allshare/n;->c(Lcom/sec/android/allshare/n;Ljava/lang/String;)Lcom/sec/android/allshare/x;

    move-result-object v0

    .line 364
    invoke-virtual {v0}, Lcom/sec/android/allshare/x;->a()Lcom/sec/android/allshare/media/FileReceiver$IFileReceiverReceiveResponseListener;

    move-result-object v0

    .line 366
    const-string v1, "BUNDLE_STRING_ARRAYLIST_FILE_PATH"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 367
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_1

    .line 370
    if-eqz v0, :cond_2

    .line 371
    iget-object v1, p0, Lcom/sec/android/allshare/o;->a:Lcom/sec/android/allshare/n;

    invoke-static {v1}, Lcom/sec/android/allshare/n;->a(Lcom/sec/android/allshare/n;)Lcom/sec/android/allshare/media/FileReceiver;

    move-result-object v1

    invoke-interface/range {v0 .. v5}, Lcom/sec/android/allshare/media/FileReceiver$IFileReceiverReceiveResponseListener;->onReceiveResponseReceived(Lcom/sec/android/allshare/media/FileReceiver;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Lcom/sec/android/allshare/ERROR;)V

    .line 417
    :cond_0
    :goto_1
    return-void

    .line 367
    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 368
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 378
    :catch_0
    move-exception v0

    const-string v0, "FileReceiverImpl"

    const-string v1, "FileReceiverImpl Fail to get SessionKeyInfoMap : NullPointerException"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 373
    :cond_2
    :try_start_1
    const-string v0, "FileReceiverImpl"

    const-string v1, "onReceiveResponseReceived listener is null!"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 382
    :cond_3
    const-string v0, "com.sec.android.allshare.action.ACTION_FILE_ARRAYLIST_RECEIVER_CANCEL"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 384
    const-string v0, "FileReceiverImpl"

    const-string v1, "notifyListResponse() ACTION_FILE_ARRAYLIST_RECEIVER_CANCEL"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    :try_start_2
    const-string v0, "FileReceiverImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "sessionID : "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    iget-object v0, p0, Lcom/sec/android/allshare/o;->a:Lcom/sec/android/allshare/n;

    invoke-static {v0, v2}, Lcom/sec/android/allshare/n;->c(Lcom/sec/android/allshare/n;Ljava/lang/String;)Lcom/sec/android/allshare/x;

    move-result-object v0

    .line 393
    invoke-virtual {v0}, Lcom/sec/android/allshare/x;->a()Lcom/sec/android/allshare/media/FileReceiver$IFileReceiverReceiveResponseListener;

    move-result-object v1

    .line 395
    if-eqz v1, :cond_4

    .line 397
    const-string v3, "AllShareResponseHandler"

    const-string v4, "listener.onCancelResponseReceived( mReceiver, sessionId, err )"

    invoke-static {v3, v4}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    iget-object v3, p0, Lcom/sec/android/allshare/o;->a:Lcom/sec/android/allshare/n;

    invoke-static {v3}, Lcom/sec/android/allshare/n;->a(Lcom/sec/android/allshare/n;)Lcom/sec/android/allshare/media/FileReceiver;

    move-result-object v3

    invoke-interface {v1, v3, v2, v5}, Lcom/sec/android/allshare/media/FileReceiver$IFileReceiverReceiveResponseListener;->onCancelResponseReceived(Lcom/sec/android/allshare/media/FileReceiver;Ljava/lang/String;Lcom/sec/android/allshare/ERROR;)V

    .line 400
    iget-object v1, p0, Lcom/sec/android/allshare/o;->a:Lcom/sec/android/allshare/n;

    invoke-static {v1}, Lcom/sec/android/allshare/n;->b(Lcom/sec/android/allshare/n;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/allshare/x;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 403
    iget-object v0, p0, Lcom/sec/android/allshare/o;->a:Lcom/sec/android/allshare/n;

    invoke-static {v0, v2}, Lcom/sec/android/allshare/n;->d(Lcom/sec/android/allshare/n;Ljava/lang/String;)V

    .line 405
    iget-object v0, p0, Lcom/sec/android/allshare/o;->a:Lcom/sec/android/allshare/n;

    invoke-static {v0}, Lcom/sec/android/allshare/n;->c(Lcom/sec/android/allshare/n;)V
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 414
    :catch_1
    move-exception v0

    const-string v0, "FileReceiverImpl"

    const-string v1, "FileReceiverImpl Fail to get SessionKeyInfoMap : NullPointerException"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 409
    :cond_4
    :try_start_3
    const-string v0, "FileReceiverImpl"

    const-string v1, "onCancelResponseReceived listener is null!"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_1
.end method

.method private b(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 421
    const-string v0, "BUNDLE_ENUM_ERROR"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 422
    const-string v1, "BUNDLE_STRING_NAME"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 423
    const-string v1, "BUNDLE_STRING_SESSIONID"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 424
    const-string v1, "BUNDLE_STRING_UNIQUEKEY"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 426
    invoke-static {v0}, Lcom/sec/android/allshare/ERROR;->valueOf(Ljava/lang/String;)Lcom/sec/android/allshare/ERROR;

    move-result-object v5

    .line 428
    const-string v0, "com.sec.android.allshare.action.ACTION_FILE_RECEIVER_RECEIVE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 430
    const-string v0, "FileReceiverImpl"

    const-string v3, "notifyListResponse()- ACTION_FILE_RECEIVER_RECEIVE"

    invoke-static {v0, v3}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/allshare/o;->a:Lcom/sec/android/allshare/n;

    invoke-static {v0, v1}, Lcom/sec/android/allshare/n;->a(Lcom/sec/android/allshare/n;Ljava/lang/String;)Lcom/sec/android/allshare/x;

    move-result-object v0

    .line 439
    iget-object v3, p0, Lcom/sec/android/allshare/o;->a:Lcom/sec/android/allshare/n;

    invoke-static {v3, v2, v0}, Lcom/sec/android/allshare/n;->a(Lcom/sec/android/allshare/n;Ljava/lang/String;Lcom/sec/android/allshare/x;)V

    .line 441
    iget-object v0, p0, Lcom/sec/android/allshare/o;->a:Lcom/sec/android/allshare/n;

    invoke-static {v0, v1}, Lcom/sec/android/allshare/n;->b(Lcom/sec/android/allshare/n;Ljava/lang/String;)V

    .line 443
    iget-object v0, p0, Lcom/sec/android/allshare/o;->a:Lcom/sec/android/allshare/n;

    invoke-static {v0, v2}, Lcom/sec/android/allshare/n;->c(Lcom/sec/android/allshare/n;Ljava/lang/String;)Lcom/sec/android/allshare/x;

    move-result-object v0

    .line 445
    invoke-virtual {v0}, Lcom/sec/android/allshare/x;->a()Lcom/sec/android/allshare/media/FileReceiver$IFileReceiverReceiveResponseListener;

    move-result-object v0

    .line 447
    const-string v1, "BUNDLE_STRING_FILE_PATH"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 448
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 449
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 451
    if-eqz v0, :cond_1

    .line 452
    iget-object v1, p0, Lcom/sec/android/allshare/o;->a:Lcom/sec/android/allshare/n;

    invoke-static {v1}, Lcom/sec/android/allshare/n;->a(Lcom/sec/android/allshare/n;)Lcom/sec/android/allshare/media/FileReceiver;

    move-result-object v1

    invoke-interface/range {v0 .. v5}, Lcom/sec/android/allshare/media/FileReceiver$IFileReceiverReceiveResponseListener;->onReceiveResponseReceived(Lcom/sec/android/allshare/media/FileReceiver;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Lcom/sec/android/allshare/ERROR;)V

    .line 496
    :cond_0
    :goto_0
    return-void

    .line 454
    :cond_1
    const-string v0, "FileReceiverImpl"

    const-string v1, "onReceiveResponseReceived listener is null!"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 459
    :catch_0
    move-exception v0

    const-string v0, "FileReceiverImpl"

    const-string v1, "FileReceiverImpl Fail to get Session or TimeKeyInfoMap : NullPointerException"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 463
    :cond_2
    const-string v0, "com.sec.android.allshare.action.ACTION_FILE_RECEIVER_CANCEL"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 465
    const-string v0, "FileReceiverImpl"

    const-string v1, "notifyListResponse()- ACTION_FILE_RECEIVER_CANCEL"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/allshare/o;->a:Lcom/sec/android/allshare/n;

    invoke-static {v0, v2}, Lcom/sec/android/allshare/n;->c(Lcom/sec/android/allshare/n;Ljava/lang/String;)Lcom/sec/android/allshare/x;

    move-result-object v0

    .line 474
    invoke-virtual {v0}, Lcom/sec/android/allshare/x;->a()Lcom/sec/android/allshare/media/FileReceiver$IFileReceiverReceiveResponseListener;

    move-result-object v1

    .line 475
    if-eqz v1, :cond_3

    .line 477
    iget-object v3, p0, Lcom/sec/android/allshare/o;->a:Lcom/sec/android/allshare/n;

    invoke-static {v3}, Lcom/sec/android/allshare/n;->a(Lcom/sec/android/allshare/n;)Lcom/sec/android/allshare/media/FileReceiver;

    move-result-object v3

    invoke-interface {v1, v3, v2, v5}, Lcom/sec/android/allshare/media/FileReceiver$IFileReceiverReceiveResponseListener;->onCancelResponseReceived(Lcom/sec/android/allshare/media/FileReceiver;Ljava/lang/String;Lcom/sec/android/allshare/ERROR;)V

    .line 479
    iget-object v1, p0, Lcom/sec/android/allshare/o;->a:Lcom/sec/android/allshare/n;

    invoke-static {v1}, Lcom/sec/android/allshare/n;->b(Lcom/sec/android/allshare/n;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/allshare/x;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 482
    iget-object v0, p0, Lcom/sec/android/allshare/o;->a:Lcom/sec/android/allshare/n;

    invoke-static {v0, v2}, Lcom/sec/android/allshare/n;->d(Lcom/sec/android/allshare/n;Ljava/lang/String;)V

    .line 484
    iget-object v0, p0, Lcom/sec/android/allshare/o;->a:Lcom/sec/android/allshare/n;

    invoke-static {v0}, Lcom/sec/android/allshare/n;->c(Lcom/sec/android/allshare/n;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 493
    :catch_1
    move-exception v0

    const-string v0, "FileReceiverImpl"

    const-string v1, "FileReceiverImpl Fail to get SessionKeyInfoMap : NullPointerException"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 488
    :cond_3
    :try_start_2
    const-string v0, "FileReceiverImpl"

    const-string v1, "onCancelResponseReceived listener is null!"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 3

    .prologue
    .line 314
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v0

    .line 315
    const-string v1, "com.sec.android.allshare.action.ACTION_FILE_RECEIVER_RECEIVE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 317
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/allshare/o;->b(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 323
    :cond_0
    :goto_0
    const-string v1, "com.sec.android.allshare.action.ACTION_FILE_ARRAYLIST_RECEIVER_RECEIVE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 325
    const-string v1, "FileReceiverImpl"

    const-string v2, "mRespHandler.handleResponseMessage() called.."

    invoke-static {v1, v2}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/allshare/o;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 332
    :cond_1
    :goto_1
    return-void

    .line 319
    :cond_2
    const-string v1, "com.sec.android.allshare.action.ACTION_FILE_RECEIVER_CANCEL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 321
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/allshare/o;->b(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 328
    :cond_3
    const-string v1, "com.sec.android.allshare.action.ACTION_FILE_ARRAYLIST_RECEIVER_CANCEL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 330
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/allshare/o;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_1
.end method
