.class final Lcom/sec/android/allshare/aa;
.super Lcom/sec/android/allshare/Icon;
.source "ProGuard"


# instance fields
.field private a:Landroid/os/Bundle;


# direct methods
.method protected constructor <init>(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/android/allshare/Icon;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/allshare/aa;->a:Landroid/os/Bundle;

    .line 24
    iput-object p1, p0, Lcom/sec/android/allshare/aa;->a:Landroid/os/Bundle;

    .line 25
    return-void
.end method


# virtual methods
.method public final getDepth()I
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/allshare/aa;->a:Landroid/os/Bundle;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/aa;->a:Landroid/os/Bundle;

    const-string v1, "ICON_DEPTH"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public final getHeight()I
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/allshare/aa;->a:Landroid/os/Bundle;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/aa;->a:Landroid/os/Bundle;

    const-string v1, "ICON_HEIGHT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public final getMimetype()Ljava/lang/String;
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/allshare/aa;->a:Landroid/os/Bundle;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/aa;->a:Landroid/os/Bundle;

    const-string v1, "ICON_MIMETYPE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getUri()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/allshare/aa;->a:Landroid/os/Bundle;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    check-cast v0, Landroid/net/Uri;

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/aa;->a:Landroid/os/Bundle;

    const-string v1, "ICON_URI"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    goto :goto_0
.end method

.method public final getWidth()I
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/allshare/aa;->a:Landroid/os/Bundle;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/aa;->a:Landroid/os/Bundle;

    const-string v1, "ICON_WIDTH"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method
