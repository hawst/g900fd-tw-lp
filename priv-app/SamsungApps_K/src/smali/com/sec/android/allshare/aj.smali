.class final Lcom/sec/android/allshare/aj;
.super Lcom/sec/android/allshare/media/MediaInfo;
.source "ProGuard"


# instance fields
.field private a:Landroid/os/Bundle;

.field private b:J


# direct methods
.method constructor <init>(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/allshare/media/MediaInfo;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/allshare/aj;->a:Landroid/os/Bundle;

    .line 24
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/allshare/aj;->b:J

    .line 28
    iput-object p1, p0, Lcom/sec/android/allshare/aj;->a:Landroid/os/Bundle;

    .line 29
    return-void
.end method


# virtual methods
.method public final getDuration()J
    .locals 4

    .prologue
    const-wide/16 v0, -0x1

    .line 34
    iget-wide v2, p0, Lcom/sec/android/allshare/aj;->b:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_0

    .line 36
    iget-object v2, p0, Lcom/sec/android/allshare/aj;->a:Landroid/os/Bundle;

    if-nez v2, :cond_1

    :goto_0
    iput-wide v0, p0, Lcom/sec/android/allshare/aj;->b:J

    .line 39
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/allshare/aj;->b:J

    return-wide v0

    .line 36
    :cond_1
    iget-object v0, p0, Lcom/sec/android/allshare/aj;->a:Landroid/os/Bundle;

    const-string v1, "BUNDLE_LONG_DURATION"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0
.end method
