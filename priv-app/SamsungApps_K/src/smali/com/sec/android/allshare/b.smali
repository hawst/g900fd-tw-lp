.class final Lcom/sec/android/allshare/b;
.super Lcom/sec/android/allshare/d;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/allshare/a;

.field private b:Ljava/util/HashMap;


# direct methods
.method constructor <init>(Lcom/sec/android/allshare/a;Landroid/os/Looper;)V
    .locals 3

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/allshare/b;->a:Lcom/sec/android/allshare/a;

    .line 631
    invoke-direct {p0, p2}, Lcom/sec/android/allshare/d;-><init>(Landroid/os/Looper;)V

    .line 633
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/allshare/b;->b:Ljava/util/HashMap;

    .line 635
    iget-object v0, p0, Lcom/sec/android/allshare/b;->b:Ljava/util/HashMap;

    const-string v1, "com.sec.android.allshare.event.EVENT_RENDERER_STATE_BUFFERING"

    sget-object v2, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;->BUFFERING:Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 636
    iget-object v0, p0, Lcom/sec/android/allshare/b;->b:Ljava/util/HashMap;

    const-string v1, "com.sec.android.allshare.event.EVENT_RENDERER_STATE_PAUSED"

    sget-object v2, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;->PAUSED:Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 637
    iget-object v0, p0, Lcom/sec/android/allshare/b;->b:Ljava/util/HashMap;

    const-string v1, "com.sec.android.allshare.event.EVENT_RENDERER_STATE_STOPPED"

    sget-object v2, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;->STOPPED:Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 638
    iget-object v0, p0, Lcom/sec/android/allshare/b;->b:Ljava/util/HashMap;

    const-string v1, "com.sec.android.allshare.event.EVENT_RENDERER_STATE_PLAYING"

    sget-object v2, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;->PLAYING:Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 639
    iget-object v0, p0, Lcom/sec/android/allshare/b;->b:Ljava/util/HashMap;

    const-string v1, "com.sec.android.allshare.event.EVENT_RENDERER_STATE_NOMEDIA"

    sget-object v2, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;->STOPPED:Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 640
    iget-object v0, p0, Lcom/sec/android/allshare/b;->b:Ljava/util/HashMap;

    const-string v1, "com.sec.android.allshare.event.EVENT_RENDERER_STATE_CONTENT_CHANGED"

    sget-object v2, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;->CONTENT_CHANGED:Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private a(Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;Lcom/sec/android/allshare/ERROR;)V
    .locals 3

    .prologue
    .line 688
    iget-object v0, p0, Lcom/sec/android/allshare/b;->a:Lcom/sec/android/allshare/a;

    invoke-static {v0}, Lcom/sec/android/allshare/a;->a(Lcom/sec/android/allshare/a;)Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 692
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/allshare/b;->a:Lcom/sec/android/allshare/a;

    invoke-static {v0}, Lcom/sec/android/allshare/a;->a(Lcom/sec/android/allshare/a;)Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerEventListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/sec/android/allshare/media/AVPlayer$IAVPlayerEventListener;->onDeviceChanged(Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;Lcom/sec/android/allshare/ERROR;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 699
    :cond_0
    :goto_0
    return-void

    .line 694
    :catch_0
    move-exception v0

    .line 696
    const-string v1, "AVPlayerImpl"

    const-string v2, "mEventHandler.notifyEvent Error"

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 646
    sget-object v0, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;->UNKNOWN:Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

    .line 647
    sget-object v0, Lcom/sec/android/allshare/ERROR;->FAIL:Lcom/sec/android/allshare/ERROR;

    .line 650
    :try_start_0
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v4

    .line 651
    iget-object v0, p0, Lcom/sec/android/allshare/b;->b:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

    .line 652
    const-string v1, "BUNDLE_ENUM_ERROR"

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 653
    if-nez v1, :cond_1

    .line 654
    sget-object v1, Lcom/sec/android/allshare/ERROR;->FAIL:Lcom/sec/android/allshare/ERROR;

    move-object v3, v1

    .line 658
    :goto_0
    sget-object v1, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;->CONTENT_CHANGED:Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;

    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 660
    iget-object v1, p0, Lcom/sec/android/allshare/b;->a:Lcom/sec/android/allshare/a;

    iget-object v1, v1, Lcom/sec/android/allshare/a;->a:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    const-string v1, "BUNDLE_STRING_APP_ITEM_ID"

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_2

    move v1, v2

    :goto_1
    if-nez v1, :cond_0

    .line 662
    iget-object v1, p0, Lcom/sec/android/allshare/b;->a:Lcom/sec/android/allshare/a;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/sec/android/allshare/a;->a:Ljava/util/ArrayList;

    .line 663
    invoke-direct {p0, v0, v3}, Lcom/sec/android/allshare/b;->a(Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;Lcom/sec/android/allshare/ERROR;)V

    .line 684
    :cond_0
    :goto_2
    return-void

    .line 656
    :cond_1
    invoke-static {v1}, Lcom/sec/android/allshare/ERROR;->valueOf(Ljava/lang/String;)Lcom/sec/android/allshare/ERROR;

    move-result-object v1

    move-object v3, v1

    goto :goto_0

    .line 660
    :cond_2
    iget-object v1, p0, Lcom/sec/android/allshare/b;->a:Lcom/sec/android/allshare/a;

    iget-object v1, v1, Lcom/sec/android/allshare/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_4

    move v1, v2

    goto :goto_1

    :cond_4
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    goto :goto_1

    .line 668
    :cond_5
    invoke-direct {p0, v0, v3}, Lcom/sec/android/allshare/b;->a(Lcom/sec/android/allshare/media/AVPlayer$AVPlayerState;Lcom/sec/android/allshare/ERROR;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_2

    .line 673
    :catch_0
    move-exception v0

    const-string v0, "AVPlayerImpl"

    const-string v1, "handleEventMessage Fail to notify event : NullPointerException"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 677
    :catch_1
    move-exception v0

    const-string v0, "AVPlayerImpl"

    const-string v1, "handleEventMessage Fail to notify event : Exception"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 679
    :catch_2
    move-exception v0

    .line 681
    const-string v1, "AVPlayerImpl"

    const-string v2, "handleEventMessage Error"

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Error;)V

    goto :goto_2
.end method
