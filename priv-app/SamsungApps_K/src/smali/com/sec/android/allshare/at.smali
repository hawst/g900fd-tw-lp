.class final Lcom/sec/android/allshare/at;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/android/allshare/IAllShareConnector;


# instance fields
.field private a:Ljava/lang/ref/WeakReference;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Lcom/sec/android/allshare/iface/ISubscriber;

.field private e:Z

.field private f:Landroid/os/Handler$Callback;

.field private g:Ljava/util/HashSet;

.field private h:Landroid/content/ComponentName;

.field private i:Landroid/content/ServiceConnection;

.field private j:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 371
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 347
    iput-object v1, p0, Lcom/sec/android/allshare/at;->a:Ljava/lang/ref/WeakReference;

    .line 349
    iput-object v1, p0, Lcom/sec/android/allshare/at;->b:Ljava/lang/String;

    .line 353
    iput-object v1, p0, Lcom/sec/android/allshare/at;->c:Ljava/lang/String;

    .line 356
    iput-object v1, p0, Lcom/sec/android/allshare/at;->d:Lcom/sec/android/allshare/iface/ISubscriber;

    .line 358
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/allshare/at;->e:Z

    .line 361
    iput-object v1, p0, Lcom/sec/android/allshare/at;->f:Landroid/os/Handler$Callback;

    .line 362
    iput-object v1, p0, Lcom/sec/android/allshare/at;->g:Ljava/util/HashSet;

    .line 364
    iput-object v1, p0, Lcom/sec/android/allshare/at;->h:Landroid/content/ComponentName;

    .line 606
    new-instance v0, Lcom/sec/android/allshare/au;

    invoke-direct {v0, p0}, Lcom/sec/android/allshare/au;-><init>(Lcom/sec/android/allshare/at;)V

    iput-object v0, p0, Lcom/sec/android/allshare/at;->i:Landroid/content/ServiceConnection;

    .line 634
    new-instance v0, Lcom/sec/android/allshare/av;

    invoke-direct {v0, p0}, Lcom/sec/android/allshare/av;-><init>(Lcom/sec/android/allshare/at;)V

    iput-object v0, p0, Lcom/sec/android/allshare/at;->j:Landroid/content/BroadcastReceiver;

    .line 373
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/allshare/at;->a:Ljava/lang/ref/WeakReference;

    .line 376
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/allshare/at;->b:Ljava/lang/String;

    .line 382
    iget-object v0, p0, Lcom/sec/android/allshare/at;->b:Ljava/lang/String;

    const-string v1, "\\."

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 388
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/sec/android/allshare/at;->c:Ljava/lang/String;

    .line 390
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/allshare/at;->g:Ljava/util/HashSet;

    .line 392
    return-void
.end method

.method static synthetic a(Lcom/sec/android/allshare/at;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 353
    iget-object v0, p0, Lcom/sec/android/allshare/at;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/sec/android/allshare/at;Landroid/content/ComponentName;)V
    .locals 0

    .prologue
    .line 364
    iput-object p1, p0, Lcom/sec/android/allshare/at;->h:Landroid/content/ComponentName;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/allshare/at;Lcom/sec/android/allshare/iface/ISubscriber;)V
    .locals 0

    .prologue
    .line 356
    iput-object p1, p0, Lcom/sec/android/allshare/at;->d:Lcom/sec/android/allshare/iface/ISubscriber;

    return-void
.end method

.method static synthetic b(Lcom/sec/android/allshare/at;)V
    .locals 3

    .prologue
    .line 549
    const-string v0, "AllShareConnector"

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/sec/android/allshare/at;->c:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "onConnected"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/allshare/at;->e:Z

    iget-object v0, p0, Lcom/sec/android/allshare/at;->f:Landroid/os/Handler$Callback;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    sget-object v1, Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;->ALLSHARE_SERVICE_CONNECTED:Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v1, p0, Lcom/sec/android/allshare/at;->f:Landroid/os/Handler$Callback;

    invoke-interface {v1, v0}, Landroid/os/Handler$Callback;->handleMessage(Landroid/os/Message;)Z

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/sec/android/allshare/at;)V
    .locals 3

    .prologue
    .line 561
    const-string v0, "AllShareConnector"

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/sec/android/allshare/at;->c:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "onDisconnected"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/allshare/at;->e:Z

    iget-object v0, p0, Lcom/sec/android/allshare/at;->f:Landroid/os/Handler$Callback;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    sget-object v1, Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;->ALLSHARE_SERVICE_DISCONNECTED:Lcom/sec/android/allshare/IAllShareConnector$AllShareServiceState;

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v1, p0, Lcom/sec/android/allshare/at;->f:Landroid/os/Handler$Callback;

    invoke-interface {v1, v0}, Landroid/os/Handler$Callback;->handleMessage(Landroid/os/Message;)Z

    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/sec/android/allshare/at;)Ljava/util/HashSet;
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lcom/sec/android/allshare/at;->g:Ljava/util/HashSet;

    return-object v0
.end method

.method private g()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 689
    iget-object v0, p0, Lcom/sec/android/allshare/at;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 691
    if-nez v0, :cond_0

    .line 693
    const-string v0, "AllShareConnector"

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/sec/android/allshare/at;->c:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "oops~~..context is not vaild.."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 704
    :goto_0
    return v0

    .line 697
    :cond_0
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.android.allshare.framework.ServiceManager.START_SERVICE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 698
    invoke-virtual {v0, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v0

    if-nez v0, :cond_1

    .line 700
    const-string v0, "AllShareConnector"

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/sec/android/allshare/at;->c:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "AllShare Service is not installed yet..."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 701
    goto :goto_0

    .line 704
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/sec/android/allshare/iface/CVMessage;Lcom/sec/android/allshare/e;)J
    .locals 7

    .prologue
    const-wide/16 v0, -0x1

    .line 743
    iget-object v2, p0, Lcom/sec/android/allshare/at;->d:Lcom/sec/android/allshare/iface/ISubscriber;

    if-eqz v2, :cond_0

    if-nez p2, :cond_1

    .line 780
    :cond_0
    :goto_0
    return-wide v0

    .line 751
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    if-nez v2, :cond_2

    .line 753
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p1, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 757
    :cond_2
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    .line 758
    invoke-virtual {p1, v2, v3}, Lcom/sec/android/allshare/iface/CVMessage;->setMsgID(J)V

    .line 761
    const/4 v4, 0x2

    invoke-virtual {p1, v4}, Lcom/sec/android/allshare/iface/CVMessage;->setMsgType(I)V

    .line 762
    new-instance v4, Landroid/os/Messenger;

    invoke-direct {v4, p2}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    invoke-virtual {p1, v4}, Lcom/sec/android/allshare/iface/CVMessage;->setMessenger(Landroid/os/Messenger;)V

    .line 763
    const-string v4, "ServiceConnector"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "**Messenger : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getMessenger()Landroid/os/Messenger;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 768
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/allshare/at;->d:Lcom/sec/android/allshare/iface/ISubscriber;

    iget-object v5, p0, Lcom/sec/android/allshare/at;->b:Ljava/lang/String;

    invoke-interface {v4, v5, p1}, Lcom/sec/android/allshare/iface/ISubscriber;->requestCVAsync(Ljava/lang/String;Lcom/sec/android/allshare/iface/CVMessage;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 770
    const-string v2, "AllShareConnector"

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/sec/android/allshare/at;->c:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "oops~~. Request failure...Maybe Invalid Action Request."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 776
    :catch_0
    move-exception v2

    const-string v2, "AllShareConnector"

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/sec/android/allshare/at;->c:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "oops~~. Request error..."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move-wide v0, v2

    .line 780
    goto :goto_0
.end method

.method public final a()Landroid/content/ContentResolver;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 986
    iget-object v1, p0, Lcom/sec/android/allshare/at;->a:Ljava/lang/ref/WeakReference;

    if-nez v1, :cond_1

    .line 990
    :cond_0
    :goto_0
    return-object v0

    .line 988
    :cond_1
    iget-object v1, p0, Lcom/sec/android/allshare/at;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 990
    iget-object v0, p0, Lcom/sec/android/allshare/at;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/sec/android/allshare/iface/CVMessage;)Lcom/sec/android/allshare/iface/CVMessage;
    .locals 4

    .prologue
    .line 793
    iget-object v0, p0, Lcom/sec/android/allshare/at;->d:Lcom/sec/android/allshare/iface/ISubscriber;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 795
    :cond_0
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 824
    :goto_0
    return-object v0

    .line 799
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_2

    .line 801
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p1, v0}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 805
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/sec/android/allshare/iface/CVMessage;->setMsgType(I)V

    .line 808
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/allshare/at;->d:Lcom/sec/android/allshare/iface/ISubscriber;

    iget-object v1, p0, Lcom/sec/android/allshare/at;->b:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Lcom/sec/android/allshare/iface/ISubscriber;->requestCVSync(Ljava/lang/String;Lcom/sec/android/allshare/iface/CVMessage;)Lcom/sec/android/allshare/iface/CVMessage;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    goto :goto_0

    .line 813
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 815
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 816
    const-string v2, "AllShareConnector"

    const-string v3, "requestCVMSync RemoteException"

    invoke-static {v2, v3, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 818
    :catch_1
    move-exception v0

    move-object v1, v0

    .line 820
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v0}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 821
    const-string v2, "AllShareConnector"

    const-string v3, "requestCVMSync NullPointerException"

    invoke-static {v2, v3, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final a(Landroid/os/Handler$Callback;)V
    .locals 0

    .prologue
    .line 400
    iput-object p1, p0, Lcom/sec/android/allshare/at;->f:Landroid/os/Handler$Callback;

    .line 401
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/allshare/d;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 838
    .line 840
    iget-object v1, p0, Lcom/sec/android/allshare/at;->d:Lcom/sec/android/allshare/iface/ISubscriber;

    if-nez v1, :cond_0

    .line 867
    :goto_0
    return v0

    .line 846
    :cond_0
    if-nez p2, :cond_1

    .line 848
    new-instance p2, Landroid/os/Bundle;

    invoke-direct {p2}, Landroid/os/Bundle;-><init>()V

    .line 851
    :cond_1
    new-instance v1, Lcom/sec/android/allshare/iface/CVMessage;

    const/4 v2, 0x4

    invoke-direct {v1, v2, p1, p2}, Lcom/sec/android/allshare/iface/CVMessage;-><init>(ILjava/lang/String;Landroid/os/Bundle;)V

    .line 852
    new-instance v2, Landroid/os/Messenger;

    invoke-direct {v2, p3}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    .line 853
    invoke-virtual {v1, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setMessenger(Landroid/os/Messenger;)V

    .line 856
    new-instance v2, Lcom/sec/android/allshare/aw;

    invoke-direct {v2, p0, p1, p2, p3}, Lcom/sec/android/allshare/aw;-><init>(Lcom/sec/android/allshare/at;Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/allshare/d;)V

    iget-object v3, p0, Lcom/sec/android/allshare/at;->g:Ljava/util/HashSet;

    invoke-virtual {v3, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 860
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/allshare/at;->d:Lcom/sec/android/allshare/iface/ISubscriber;

    iget-object v3, p0, Lcom/sec/android/allshare/at;->b:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Lcom/sec/android/allshare/iface/ISubscriber;->subscribeEvent(Ljava/lang/String;Lcom/sec/android/allshare/iface/CVMessage;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 862
    :catch_0
    move-exception v1

    .line 864
    const-string v2, "AllShareConnector"

    const-string v3, "subscribeAllShareEvent RemoteException"

    invoke-static {v2, v3, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/allshare/d;)V
    .locals 3

    .prologue
    .line 878
    iget-object v0, p0, Lcom/sec/android/allshare/at;->d:Lcom/sec/android/allshare/iface/ISubscriber;

    if-nez v0, :cond_0

    .line 904
    :goto_0
    return-void

    .line 884
    :cond_0
    if-nez p2, :cond_1

    .line 886
    new-instance p2, Landroid/os/Bundle;

    invoke-direct {p2}, Landroid/os/Bundle;-><init>()V

    .line 889
    :cond_1
    new-instance v0, Lcom/sec/android/allshare/iface/CVMessage;

    const/4 v1, 0x4

    invoke-direct {v0, v1, p1, p2}, Lcom/sec/android/allshare/iface/CVMessage;-><init>(ILjava/lang/String;Landroid/os/Bundle;)V

    .line 890
    new-instance v1, Landroid/os/Messenger;

    invoke-direct {v1, p3}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    .line 891
    invoke-virtual {v0, v1}, Lcom/sec/android/allshare/iface/CVMessage;->setMessenger(Landroid/os/Messenger;)V

    .line 893
    new-instance v1, Lcom/sec/android/allshare/aw;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/sec/android/allshare/aw;-><init>(Lcom/sec/android/allshare/at;Ljava/lang/String;Landroid/os/Bundle;Lcom/sec/android/allshare/d;)V

    iget-object v2, p0, Lcom/sec/android/allshare/at;->g:Ljava/util/HashSet;

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 897
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/allshare/at;->d:Lcom/sec/android/allshare/iface/ISubscriber;

    iget-object v2, p0, Lcom/sec/android/allshare/at;->b:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lcom/sec/android/allshare/iface/ISubscriber;->unsubscribeEvent(Ljava/lang/String;Lcom/sec/android/allshare/iface/CVMessage;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 899
    :catch_0
    move-exception v0

    .line 901
    const-string v1, "AllShareConnector"

    const-string v2, "unsubscribeAllShareEvent RemoteException"

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 676
    iget-object v0, p0, Lcom/sec/android/allshare/at;->d:Lcom/sec/android/allshare/iface/ISubscriber;

    if-nez v0, :cond_0

    .line 677
    const/4 v0, 0x0

    .line 679
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final declared-synchronized c()V
    .locals 4

    .prologue
    .line 408
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/sec/android/allshare/at;->e:Z

    if-eqz v0, :cond_0

    .line 410
    const-string v0, "AllShareConnector"

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/sec/android/allshare/at;->c:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "Already trying to connecting...wait.."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 429
    :goto_0
    monitor-exit p0

    return-void

    .line 413
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/sec/android/allshare/at;->e:Z

    .line 415
    invoke-virtual {p0}, Lcom/sec/android/allshare/at;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 417
    const-string v0, "AllShareConnector"

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/sec/android/allshare/at;->c:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "Already connected to AllShare service framework"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 408
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 422
    :cond_1
    :try_start_2
    invoke-direct {p0}, Lcom/sec/android/allshare/at;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 424
    iget-object v0, p0, Lcom/sec/android/allshare/at;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    if-nez v0, :cond_3

    const-string v0, "AllShareConnector"

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/sec/android/allshare/at;->c:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "oops~~..context is not vaild.."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 428
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sec/android/allshare/at;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    if-nez v0, :cond_5

    const-string v0, "AllShareConnector"

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/sec/android/allshare/at;->c:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "oops~~..context is not vaild.."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 424
    :cond_3
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.allshare.framework.SUBSCRIBE_SERVICE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "com.sec.android.allshare.iface.subscriber"

    iget-object v3, p0, Lcom/sec/android/allshare/at;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/allshare/at;->i:Landroid/content/ServiceConnection;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v1, "AllShareConnector"

    const-string v2, "BindService FAIL: the connection is not made"

    invoke-static {v1, v2}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/android/allshare/at;->g()Z

    :cond_4
    if-nez v0, :cond_2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/allshare/at;->e:Z

    const-string v0, "AllShareConnector"

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/sec/android/allshare/at;->c:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "oops~~..bindService error.."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "AllShareConnector"

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/sec/android/allshare/at;->c:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "check a context is child activities\' context"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "AllShareConnector"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unbind Service : mConnecting "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/sec/android/allshare/at;->e:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 428
    :cond_5
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "com.sec.android.allshare.framework.ServiceManager.START_COMPLETED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "com.sec.android.allshare.framework.ServiceManager.STOP_COMPLETED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/allshare/at;->j:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 517
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/allshare/at;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    if-nez v0, :cond_1

    const-string v0, "AllShareConnector"

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/sec/android/allshare/at;->c:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "oops~~..context is not vaild.."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 519
    :goto_0
    iget-object v0, p0, Lcom/sec/android/allshare/at;->i:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    .line 523
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/allshare/at;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 525
    if-nez v0, :cond_2

    .line 527
    const-string v0, "AllShareConnector"

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/sec/android/allshare/at;->c:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "oops~~..context is not vaild.."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 544
    :cond_0
    :goto_1
    return-void

    .line 517
    :cond_1
    :try_start_2
    iget-object v1, p0, Lcom/sec/android/allshare/at;->j:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "AllShareConnector"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unregisterSvcCastReceiver exception "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 531
    :cond_2
    :try_start_3
    iget-object v1, p0, Lcom/sec/android/allshare/at;->i:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 533
    invoke-virtual {p0}, Lcom/sec/android/allshare/at;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 534
    iget-object v0, p0, Lcom/sec/android/allshare/at;->i:Landroid/content/ServiceConnection;

    iget-object v1, p0, Lcom/sec/android/allshare/at;->h:Landroid/content/ComponentName;

    invoke-interface {v0, v1}, Landroid/content/ServiceConnection;->onServiceDisconnected(Landroid/content/ComponentName;)V

    .line 536
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/allshare/at;->i:Landroid/content/ServiceConnection;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 539
    :catch_1
    move-exception v0

    .line 541
    const-string v1, "AllShareConnector"

    const-string v2, "disconnect Exception"

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public final e()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 722
    invoke-virtual {p0}, Lcom/sec/android/allshare/at;->d()V

    .line 723
    const-string v0, "AllShareConnector"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "before"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/sec/android/allshare/at;->e:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 725
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/allshare/at;->e:Z

    .line 727
    iput-object v3, p0, Lcom/sec/android/allshare/at;->f:Landroid/os/Handler$Callback;

    .line 728
    iput-object v3, p0, Lcom/sec/android/allshare/at;->d:Lcom/sec/android/allshare/iface/ISubscriber;

    .line 730
    const-string v0, "AllShareConnector"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "after"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/sec/android/allshare/at;->e:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 731
    return-void
.end method

.method public final f()Ljava/lang/String;
    .locals 3

    .prologue
    .line 913
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/allshare/at;->d:Lcom/sec/android/allshare/iface/ISubscriber;

    invoke-interface {v0}, Lcom/sec/android/allshare/iface/ISubscriber;->getServiceVersion()Ljava/lang/String;

    move-result-object v0

    .line 919
    if-nez v0, :cond_0

    .line 920
    const-string v0, "1.0.0"
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 933
    :cond_0
    :goto_0
    return-object v0

    .line 922
    :catch_0
    move-exception v0

    .line 924
    const-string v1, "AllShareConnector"

    const-string v2, "getServiceVersion RemoteException"

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 925
    const-string v0, ""

    goto :goto_0

    .line 927
    :catch_1
    move-exception v0

    .line 929
    const-string v1, "AllShareConnector"

    const-string v2, "getServiceVersion Exception"

    invoke-static {v1, v2, v0}, Lcom/sec/android/allshare/DLog;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 930
    const-string v0, ""

    goto :goto_0
.end method
