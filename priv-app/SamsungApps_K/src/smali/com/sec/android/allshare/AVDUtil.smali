.class public Lcom/sec/android/allshare/AVDUtil;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field public static final AVD_MAC_ADDRESS:Ljava/lang/String; = "00-24-54-92-0A-44"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isAVD()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 19
    sget-object v1, Landroid/os/Build;->BRAND:Ljava/lang/String;

    .line 20
    if-nez v1, :cond_1

    .line 27
    :cond_0
    :goto_0
    return v0

    .line 23
    :cond_1
    const-string v2, "generic"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 27
    const/4 v0, 0x0

    goto :goto_0
.end method
