.class public Lcom/sec/android/allshare/media/SearchCriteria;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/util/ArrayList;


# direct methods
.method private constructor <init>(Lcom/sec/android/allshare/media/SearchCriteria$Builder;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-static {p1}, Lcom/sec/android/allshare/media/SearchCriteria$Builder;->a(Lcom/sec/android/allshare/media/SearchCriteria$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/allshare/media/SearchCriteria;->a:Ljava/lang/String;

    .line 37
    invoke-static {p1}, Lcom/sec/android/allshare/media/SearchCriteria$Builder;->b(Lcom/sec/android/allshare/media/SearchCriteria$Builder;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/allshare/media/SearchCriteria;->b:Ljava/util/ArrayList;

    .line 38
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/allshare/media/SearchCriteria$Builder;B)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/sec/android/allshare/media/SearchCriteria;-><init>(Lcom/sec/android/allshare/media/SearchCriteria$Builder;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 138
    instance-of v0, p1, Lcom/sec/android/allshare/media/SearchCriteria;

    if-eqz v0, :cond_1

    .line 140
    check-cast p1, Lcom/sec/android/allshare/media/SearchCriteria;

    .line 144
    iget-object v0, p1, Lcom/sec/android/allshare/media/SearchCriteria;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/allshare/media/SearchCriteria;->a:Ljava/lang/String;

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p1, Lcom/sec/android/allshare/media/SearchCriteria;->a:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/allshare/media/SearchCriteria;->a:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 202
    :cond_1
    :goto_0
    return v3

    .line 148
    :cond_2
    iget-object v0, p1, Lcom/sec/android/allshare/media/SearchCriteria;->a:Ljava/lang/String;

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/allshare/media/SearchCriteria;->a:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v4

    .line 158
    :goto_1
    if-eqz v0, :cond_1

    .line 162
    iget-object v0, p1, Lcom/sec/android/allshare/media/SearchCriteria;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/allshare/media/SearchCriteria;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_8

    .line 164
    iget-object v0, p0, Lcom/sec/android/allshare/media/SearchCriteria;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 165
    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_5

    move v3, v4

    .line 187
    goto :goto_0

    .line 152
    :cond_4
    iget-object v0, p1, Lcom/sec/android/allshare/media/SearchCriteria;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/allshare/media/SearchCriteria;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_9

    move v0, v4

    .line 154
    goto :goto_1

    .line 167
    :cond_5
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/allshare/Item$MediaType;

    .line 170
    iget-object v1, p1, Lcom/sec/android/allshare/media/SearchCriteria;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v2, v3

    .line 171
    :cond_6
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_7

    .line 181
    if-nez v2, :cond_3

    goto :goto_0

    .line 173
    :cond_7
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/allshare/Item$MediaType;

    .line 175
    if-ne v0, v1, :cond_6

    move v2, v4

    .line 177
    goto :goto_2

    .line 189
    :cond_8
    iget-object v0, p1, Lcom/sec/android/allshare/media/SearchCriteria;->b:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/allshare/media/SearchCriteria;->b:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    move v3, v4

    .line 191
    goto :goto_0

    :cond_9
    move v0, v3

    goto :goto_1
.end method

.method public getKeyword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/allshare/media/SearchCriteria;->a:Ljava/lang/String;

    return-object v0
.end method

.method public isMatchedItemType(Lcom/sec/android/allshare/Item$MediaType;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 58
    iget-object v0, p0, Lcom/sec/android/allshare/media/SearchCriteria;->b:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    move v0, v1

    .line 65
    :goto_0
    return v0

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/media/SearchCriteria;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 65
    goto :goto_0

    .line 62
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/allshare/Item$MediaType;

    .line 63
    invoke-virtual {v0, p1}, Lcom/sec/android/allshare/Item$MediaType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 64
    const/4 v0, 0x1

    goto :goto_0
.end method
