.class public final enum Lcom/sec/android/allshare/ServiceConnector$ServiceState;
.super Ljava/lang/Enum;
.source "ProGuard"


# static fields
.field public static final enum DISABLED:Lcom/sec/android/allshare/ServiceConnector$ServiceState;

.field public static final enum ENABLED:Lcom/sec/android/allshare/ServiceConnector$ServiceState;

.field public static final enum UNKNOWN:Lcom/sec/android/allshare/ServiceConnector$ServiceState;

.field private static final synthetic a:[Lcom/sec/android/allshare/ServiceConnector$ServiceState;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 73
    new-instance v0, Lcom/sec/android/allshare/ServiceConnector$ServiceState;

    const-string v1, "ENABLED"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/allshare/ServiceConnector$ServiceState;-><init>(Ljava/lang/String;I)V

    .line 77
    sput-object v0, Lcom/sec/android/allshare/ServiceConnector$ServiceState;->ENABLED:Lcom/sec/android/allshare/ServiceConnector$ServiceState;

    .line 78
    new-instance v0, Lcom/sec/android/allshare/ServiceConnector$ServiceState;

    const-string v1, "DISABLED"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/allshare/ServiceConnector$ServiceState;-><init>(Ljava/lang/String;I)V

    .line 82
    sput-object v0, Lcom/sec/android/allshare/ServiceConnector$ServiceState;->DISABLED:Lcom/sec/android/allshare/ServiceConnector$ServiceState;

    .line 83
    new-instance v0, Lcom/sec/android/allshare/ServiceConnector$ServiceState;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/allshare/ServiceConnector$ServiceState;-><init>(Ljava/lang/String;I)V

    .line 87
    sput-object v0, Lcom/sec/android/allshare/ServiceConnector$ServiceState;->UNKNOWN:Lcom/sec/android/allshare/ServiceConnector$ServiceState;

    .line 71
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/allshare/ServiceConnector$ServiceState;

    sget-object v1, Lcom/sec/android/allshare/ServiceConnector$ServiceState;->ENABLED:Lcom/sec/android/allshare/ServiceConnector$ServiceState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/allshare/ServiceConnector$ServiceState;->DISABLED:Lcom/sec/android/allshare/ServiceConnector$ServiceState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/allshare/ServiceConnector$ServiceState;->UNKNOWN:Lcom/sec/android/allshare/ServiceConnector$ServiceState;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/allshare/ServiceConnector$ServiceState;->a:[Lcom/sec/android/allshare/ServiceConnector$ServiceState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/allshare/ServiceConnector$ServiceState;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/sec/android/allshare/ServiceConnector$ServiceState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/allshare/ServiceConnector$ServiceState;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/allshare/ServiceConnector$ServiceState;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/sec/android/allshare/ServiceConnector$ServiceState;->a:[Lcom/sec/android/allshare/ServiceConnector$ServiceState;

    array-length v1, v0

    new-array v2, v1, [Lcom/sec/android/allshare/ServiceConnector$ServiceState;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
