.class final Lcom/sec/android/allshare/az;
.super Lcom/sec/android/allshare/d;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/allshare/TVControllerImpl;


# direct methods
.method constructor <init>(Lcom/sec/android/allshare/TVControllerImpl;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/allshare/az;->a:Lcom/sec/android/allshare/TVControllerImpl;

    .line 229
    invoke-direct {p0, p2}, Lcom/sec/android/allshare/d;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 5

    .prologue
    .line 238
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 239
    if-nez v0, :cond_1

    .line 254
    :cond_0
    :goto_0
    return-void

    .line 242
    :cond_1
    const-string v1, "BUNDLE_STRING_MAIN_TV_EVENT_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 243
    const-string v2, "BUNDLE_STRING_MAIN_TV_EVENT_XML"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 244
    const-string v2, "AllShareEventHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[TVControl] mAllShareEvent : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/android/allshare/DLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    new-instance v1, Lcom/sec/android/allshare/bb;

    iget-object v2, p0, Lcom/sec/android/allshare/az;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-direct {v1, v2}, Lcom/sec/android/allshare/bb;-><init>(Lcom/sec/android/allshare/TVControllerImpl;)V

    .line 247
    invoke-virtual {v1, v0}, Lcom/sec/android/allshare/bb;->a(Ljava/lang/String;)V

    .line 249
    invoke-virtual {v1}, Lcom/sec/android/allshare/bb;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PowerOFF"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252
    iget-object v0, p0, Lcom/sec/android/allshare/az;->a:Lcom/sec/android/allshare/TVControllerImpl;

    invoke-static {v0}, Lcom/sec/android/allshare/TVControllerImpl;->e(Lcom/sec/android/allshare/TVControllerImpl;)V

    goto :goto_0
.end method
