.class final Lcom/sec/android/allshare/ax;
.super Lcom/sec/android/allshare/ServiceProvider;
.source "ProGuard"


# instance fields
.field a:Lcom/sec/android/allshare/ServiceConnector$ServiceState;

.field b:Lcom/sec/android/allshare/g;

.field c:Lcom/sec/android/allshare/aq;

.field d:Lcom/sec/android/allshare/at;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/allshare/at;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 298
    invoke-direct {p0}, Lcom/sec/android/allshare/ServiceProvider;-><init>()V

    .line 293
    sget-object v0, Lcom/sec/android/allshare/ServiceConnector$ServiceState;->DISABLED:Lcom/sec/android/allshare/ServiceConnector$ServiceState;

    iput-object v0, p0, Lcom/sec/android/allshare/ax;->a:Lcom/sec/android/allshare/ServiceConnector$ServiceState;

    .line 294
    iput-object v1, p0, Lcom/sec/android/allshare/ax;->b:Lcom/sec/android/allshare/g;

    .line 295
    iput-object v1, p0, Lcom/sec/android/allshare/ax;->c:Lcom/sec/android/allshare/aq;

    .line 296
    iput-object v1, p0, Lcom/sec/android/allshare/ax;->d:Lcom/sec/android/allshare/at;

    .line 300
    iput-object p2, p0, Lcom/sec/android/allshare/ax;->d:Lcom/sec/android/allshare/at;

    .line 301
    new-instance v0, Lcom/sec/android/allshare/g;

    invoke-direct {v0, p2}, Lcom/sec/android/allshare/g;-><init>(Lcom/sec/android/allshare/IAllShareConnector;)V

    iput-object v0, p0, Lcom/sec/android/allshare/ax;->b:Lcom/sec/android/allshare/g;

    .line 302
    invoke-static {p1, p2}, Lcom/sec/android/allshare/aq;->a(Landroid/content/Context;Lcom/sec/android/allshare/IAllShareConnector;)Lcom/sec/android/allshare/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/allshare/ax;->c:Lcom/sec/android/allshare/aq;

    .line 303
    return-void
.end method


# virtual methods
.method public final bridge synthetic getDeviceFinder()Lcom/sec/android/allshare/DeviceFinder;
    .locals 1

    .prologue
    .line 1
    iget-object v0, p0, Lcom/sec/android/allshare/ax;->b:Lcom/sec/android/allshare/g;

    return-object v0
.end method

.method public final bridge synthetic getScreenCastManager()Lcom/sec/android/allshare/screen/ScreenCastManager;
    .locals 1

    .prologue
    .line 1
    iget-object v0, p0, Lcom/sec/android/allshare/ax;->c:Lcom/sec/android/allshare/aq;

    return-object v0
.end method

.method public final getServiceState()Lcom/sec/android/allshare/ServiceConnector$ServiceState;
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lcom/sec/android/allshare/ax;->a:Lcom/sec/android/allshare/ServiceConnector$ServiceState;

    return-object v0
.end method

.method public final getServiceVersion()Ljava/lang/String;
    .locals 2

    .prologue
    .line 320
    iget-object v0, p0, Lcom/sec/android/allshare/ax;->d:Lcom/sec/android/allshare/at;

    if-nez v0, :cond_0

    .line 322
    const-string v0, "ServiceProviderImpl"

    const-string v1, "Connection FAIL: AllShare Service Connector does not exist"

    invoke-static {v0, v1}, Lcom/sec/android/allshare/DLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    const-string v0, ""

    .line 326
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/ax;->d:Lcom/sec/android/allshare/at;

    invoke-virtual {v0}, Lcom/sec/android/allshare/at;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
