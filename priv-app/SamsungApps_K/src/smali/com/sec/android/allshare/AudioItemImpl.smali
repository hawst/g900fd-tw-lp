.class final Lcom/sec/android/allshare/AudioItemImpl;
.super Lcom/sec/android/allshare/Item;
.source "ProGuard"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/sec/android/allshare/iface/IBundleHolder;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:Lcom/sec/android/allshare/ItemImpl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 211
    new-instance v0, Lcom/sec/android/allshare/f;

    invoke-direct {v0}, Lcom/sec/android/allshare/f;-><init>()V

    sput-object v0, Lcom/sec/android/allshare/AudioItemImpl;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 37
    return-void
.end method

.method constructor <init>(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/android/allshare/Item;-><init>()V

    .line 43
    new-instance v0, Lcom/sec/android/allshare/ItemImpl;

    invoke-direct {v0, p1}, Lcom/sec/android/allshare/ItemImpl;-><init>(Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/sec/android/allshare/AudioItemImpl;->a:Lcom/sec/android/allshare/ItemImpl;

    .line 44
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 202
    invoke-direct {p0}, Lcom/sec/android/allshare/Item;-><init>()V

    .line 204
    const-class v0, Landroid/os/Bundle;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readBundle(Ljava/lang/ClassLoader;)Landroid/os/Bundle;

    move-result-object v0

    .line 205
    new-instance v1, Lcom/sec/android/allshare/ItemImpl;

    invoke-direct {v1, v0}, Lcom/sec/android/allshare/ItemImpl;-><init>(Landroid/os/Bundle;)V

    iput-object v1, p0, Lcom/sec/android/allshare/AudioItemImpl;->a:Lcom/sec/android/allshare/ItemImpl;

    .line 206
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 202
    invoke-direct {p0, p1}, Lcom/sec/android/allshare/AudioItemImpl;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 189
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 157
    if-ne p0, p1, :cond_1

    .line 163
    :cond_0
    :goto_0
    return v0

    .line 159
    :cond_1
    if-eqz p1, :cond_2

    instance-of v2, p1, Lcom/sec/android/allshare/AudioItemImpl;

    if-nez v2, :cond_3

    :cond_2
    move v0, v1

    .line 160
    goto :goto_0

    .line 163
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/allshare/AudioItemImpl;->hashCode()I

    move-result v2

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v3

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final getAlbumTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/allshare/AudioItemImpl;->a:Lcom/sec/android/allshare/ItemImpl;

    invoke-virtual {v0}, Lcom/sec/android/allshare/ItemImpl;->a()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/AudioItemImpl;->a:Lcom/sec/android/allshare/ItemImpl;

    invoke-virtual {v0}, Lcom/sec/android/allshare/ItemImpl;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "BUNDLE_STRING_AUDIO_ITEM_ALBUM_TITLE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getArtist()Ljava/lang/String;
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/allshare/AudioItemImpl;->a:Lcom/sec/android/allshare/ItemImpl;

    invoke-virtual {v0}, Lcom/sec/android/allshare/ItemImpl;->a()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/AudioItemImpl;->a:Lcom/sec/android/allshare/ItemImpl;

    invoke-virtual {v0}, Lcom/sec/android/allshare/ItemImpl;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "BUNDLE_STRING_AUDIO_ITEM_ARTIST"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getBundle()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/sec/android/allshare/AudioItemImpl;->a:Lcom/sec/android/allshare/ItemImpl;

    invoke-virtual {v0}, Lcom/sec/android/allshare/ItemImpl;->a()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public final getContentBuildType()Lcom/sec/android/allshare/Item$ContentBuildType;
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/sec/android/allshare/AudioItemImpl;->a:Lcom/sec/android/allshare/ItemImpl;

    if-nez v0, :cond_0

    .line 260
    sget-object v0, Lcom/sec/android/allshare/Item$ContentBuildType;->UNKNOWN:Lcom/sec/android/allshare/Item$ContentBuildType;

    .line 262
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/AudioItemImpl;->a:Lcom/sec/android/allshare/ItemImpl;

    invoke-virtual {v0}, Lcom/sec/android/allshare/ItemImpl;->getContentBuildType()Lcom/sec/android/allshare/Item$ContentBuildType;

    move-result-object v0

    goto :goto_0
.end method

.method public final getDate()Ljava/util/Date;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/allshare/AudioItemImpl;->a:Lcom/sec/android/allshare/ItemImpl;

    invoke-virtual {v0}, Lcom/sec/android/allshare/ItemImpl;->getDate()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public final getDuration()J
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/android/allshare/AudioItemImpl;->a:Lcom/sec/android/allshare/ItemImpl;

    invoke-virtual {v0}, Lcom/sec/android/allshare/ItemImpl;->a()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/AudioItemImpl;->a:Lcom/sec/android/allshare/ItemImpl;

    invoke-virtual {v0}, Lcom/sec/android/allshare/ItemImpl;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "BUNDLE_LONG_AUDIO_ITEM_DURATION"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public final getExtension()Ljava/lang/String;
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/android/allshare/AudioItemImpl;->a:Lcom/sec/android/allshare/ItemImpl;

    invoke-virtual {v0}, Lcom/sec/android/allshare/ItemImpl;->a()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/AudioItemImpl;->a:Lcom/sec/android/allshare/ItemImpl;

    invoke-virtual {v0}, Lcom/sec/android/allshare/ItemImpl;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "BUNDLE_STRING_ITEM_EXTENSION"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getFileSize()J
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/allshare/AudioItemImpl;->a:Lcom/sec/android/allshare/ItemImpl;

    invoke-virtual {v0}, Lcom/sec/android/allshare/ItemImpl;->getFileSize()J

    move-result-wide v0

    return-wide v0
.end method

.method public final getGenre()Ljava/lang/String;
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, Lcom/sec/android/allshare/AudioItemImpl;->a:Lcom/sec/android/allshare/ItemImpl;

    invoke-virtual {v0}, Lcom/sec/android/allshare/ItemImpl;->a()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/AudioItemImpl;->a:Lcom/sec/android/allshare/ItemImpl;

    invoke-virtual {v0}, Lcom/sec/android/allshare/ItemImpl;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "BUNDLE_STRING_AUDIO_ITEM_GENRE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getLocation()Landroid/location/Location;
    .locals 1

    .prologue
    .line 227
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getMimetype()Ljava/lang/String;
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/android/allshare/AudioItemImpl;->a:Lcom/sec/android/allshare/ItemImpl;

    invoke-virtual {v0}, Lcom/sec/android/allshare/ItemImpl;->a()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/AudioItemImpl;->a:Lcom/sec/android/allshare/ItemImpl;

    invoke-virtual {v0}, Lcom/sec/android/allshare/ItemImpl;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "BUNDLE_STRING_ITEM_MIMETYPE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getResolution()Ljava/lang/String;
    .locals 1

    .prologue
    .line 233
    const-string v0, ""

    return-object v0
.end method

.method public final getSubtitle()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 239
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getThumbnail()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 245
    iget-object v0, p0, Lcom/sec/android/allshare/AudioItemImpl;->a:Lcom/sec/android/allshare/ItemImpl;

    invoke-virtual {v0}, Lcom/sec/android/allshare/ItemImpl;->a()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    check-cast v0, Landroid/net/Uri;

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/allshare/AudioItemImpl;->a:Lcom/sec/android/allshare/ItemImpl;

    invoke-virtual {v0}, Lcom/sec/android/allshare/ItemImpl;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "BUNDLE_PARCELABLE_AUDIO_ITEM_ALBUMART"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    goto :goto_0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/android/allshare/AudioItemImpl;->a:Lcom/sec/android/allshare/ItemImpl;

    invoke-virtual {v0}, Lcom/sec/android/allshare/ItemImpl;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getType()Lcom/sec/android/allshare/Item$MediaType;
    .locals 1

    .prologue
    .line 121
    sget-object v0, Lcom/sec/android/allshare/Item$MediaType;->ITEM_AUDIO:Lcom/sec/android/allshare/Item$MediaType;

    return-object v0
.end method

.method public final getURI()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/sec/android/allshare/AudioItemImpl;->a:Lcom/sec/android/allshare/ItemImpl;

    invoke-virtual {v0}, Lcom/sec/android/allshare/ItemImpl;->getURI()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 169
    iget-object v1, p0, Lcom/sec/android/allshare/AudioItemImpl;->a:Lcom/sec/android/allshare/ItemImpl;

    if-nez v1, :cond_1

    .line 177
    :cond_0
    :goto_0
    return v0

    .line 172
    :cond_1
    iget-object v1, p0, Lcom/sec/android/allshare/AudioItemImpl;->a:Lcom/sec/android/allshare/ItemImpl;

    invoke-virtual {v1}, Lcom/sec/android/allshare/ItemImpl;->b()Ljava/lang/String;

    move-result-object v1

    .line 174
    if-eqz v1, :cond_0

    .line 177
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final isRootFolder()Z
    .locals 1

    .prologue
    .line 252
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 195
    invoke-virtual {p0}, Lcom/sec/android/allshare/AudioItemImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 196
    return-void
.end method
