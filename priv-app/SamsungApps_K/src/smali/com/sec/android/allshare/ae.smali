.class final Lcom/sec/android/allshare/ae;
.super Lcom/sec/android/allshare/e;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/sec/android/allshare/ac;


# direct methods
.method constructor <init>(Lcom/sec/android/allshare/ac;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/allshare/ae;->a:Lcom/sec/android/allshare/ac;

    .line 167
    invoke-direct {p0, p2}, Lcom/sec/android/allshare/e;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 4

    .prologue
    .line 173
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v1

    .line 174
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v3

    .line 176
    if-eqz v1, :cond_0

    if-nez v3, :cond_1

    .line 228
    :cond_0
    :goto_0
    return-void

    .line 180
    :cond_1
    const-string v0, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SHOW"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SHOW_LOCAL_CONTENT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SHOW_LOCAL_CONTENT_URI"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SHOW_URI"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 182
    :cond_2
    iget-object v0, p0, Lcom/sec/android/allshare/ae;->a:Lcom/sec/android/allshare/ac;

    const-string v2, "BUNDLE_STRING_SERVER_URI_LIST"

    invoke-virtual {v3, v2}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, v0, Lcom/sec/android/allshare/ac;->a:Ljava/util/ArrayList;

    .line 185
    :cond_3
    iget-object v0, p0, Lcom/sec/android/allshare/ae;->a:Lcom/sec/android/allshare/ac;

    invoke-static {v0}, Lcom/sec/android/allshare/ac;->b(Lcom/sec/android/allshare/ac;)Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 188
    sget-object v0, Lcom/sec/android/allshare/ERROR;->FAIL:Lcom/sec/android/allshare/ERROR;

    .line 189
    const-string v2, "BUNDLE_ENUM_ERROR"

    invoke-virtual {v3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 190
    if-eqz v2, :cond_8

    .line 191
    invoke-static {v2}, Lcom/sec/android/allshare/ERROR;->valueOf(Ljava/lang/String;)Lcom/sec/android/allshare/ERROR;

    move-result-object v0

    move-object v2, v0

    .line 193
    :goto_1
    const-string v0, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SHOW"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SHOW_LOCAL_CONTENT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SHOW_LOCAL_CONTENT_URI"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_SHOW_URI"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 195
    :cond_4
    const-string v0, "BUNDLE_PARCELABLE_CONTENT_INFO"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/allshare/media/ContentInfo;

    .line 197
    const-string v1, "BUNDLE_PARCELABLE_ITEM"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    .line 199
    invoke-static {v1}, Lcom/sec/android/allshare/ag;->a(Landroid/os/Bundle;)Lcom/sec/android/allshare/Item;

    move-result-object v1

    .line 201
    if-nez v1, :cond_5

    .line 202
    iget-object v2, p0, Lcom/sec/android/allshare/ae;->a:Lcom/sec/android/allshare/ac;

    invoke-static {v2}, Lcom/sec/android/allshare/ac;->b(Lcom/sec/android/allshare/ac;)Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    move-result-object v2

    sget-object v3, Lcom/sec/android/allshare/ERROR;->ITEM_NOT_EXIST:Lcom/sec/android/allshare/ERROR;

    invoke-interface {v2, v1, v0, v3}, Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onShowResponseReceived(Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/media/ContentInfo;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 204
    :cond_5
    iget-object v3, p0, Lcom/sec/android/allshare/ae;->a:Lcom/sec/android/allshare/ac;

    invoke-static {v3}, Lcom/sec/android/allshare/ac;->b(Lcom/sec/android/allshare/ac;)Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    move-result-object v3

    invoke-interface {v3, v1, v0, v2}, Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onShowResponseReceived(Lcom/sec/android/allshare/Item;Lcom/sec/android/allshare/media/ContentInfo;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 206
    :cond_6
    const-string v0, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_STOP"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 208
    iget-object v0, p0, Lcom/sec/android/allshare/ae;->a:Lcom/sec/android/allshare/ac;

    invoke-static {v0}, Lcom/sec/android/allshare/ac;->b(Lcom/sec/android/allshare/ac;)Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onStopResponseReceived(Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 210
    :cond_7
    const-string v0, "com.sec.android.allshare.action.ACTION_IMAGE_VIEWER_REQUEST_GET_VIEWER_STATE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 212
    const-string v0, "BUNDLE_STRING_IMAGE_VIEWEW_STATE"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 214
    sget-object v1, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;->UNKNOWN:Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    .line 218
    :try_start_0
    invoke-static {v0}, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;->valueOf(Ljava/lang/String;)Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 225
    :goto_2
    iget-object v1, p0, Lcom/sec/android/allshare/ae;->a:Lcom/sec/android/allshare/ac;

    invoke-static {v1}, Lcom/sec/android/allshare/ac;->b(Lcom/sec/android/allshare/ac;)Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;

    move-result-object v1

    invoke-interface {v1, v0, v2}, Lcom/sec/android/allshare/media/ImageViewer$IImageViewerResponseListener;->onGetStateResponseReceived(Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;Lcom/sec/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 222
    :catch_0
    move-exception v0

    sget-object v0, Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;->UNKNOWN:Lcom/sec/android/allshare/media/ImageViewer$ImageViewerState;

    goto :goto_2

    :cond_8
    move-object v2, v0

    goto/16 :goto_1
.end method
