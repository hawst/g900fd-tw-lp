.class public Lcom/sec/spp/push/dlc/api/DlcApi;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Lcom/sec/spp/push/dlc/api/IDlcApi;


# static fields
.field public static final RC_SVC_NOT_CONNECTED:I = -0x3e8


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getResultStr(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 7
    const-string v0, ""

    .line 8
    sparse-switch p0, :sswitch_data_0

    .line 34
    :goto_0
    return-object v0

    .line 10
    :sswitch_0
    const-string v0, "Success"

    goto :goto_0

    .line 13
    :sswitch_1
    const-string v0, "Invalid parameter"

    goto :goto_0

    .line 16
    :sswitch_2
    const-string v0, "Permission error"

    goto :goto_0

    .line 19
    :sswitch_3
    const-string v0, "Application is blocked"

    goto :goto_0

    .line 22
    :sswitch_4
    const-string v0, "Service is unavailable"

    goto :goto_0

    .line 25
    :sswitch_5
    const-string v0, "Service is not connected"

    goto :goto_0

    .line 28
    :sswitch_6
    const-string v0, "Urgent logging is not allowed"

    goto :goto_0

    .line 31
    :sswitch_7
    const-string v0, "This app is not registered yet"

    goto :goto_0

    .line 8
    :sswitch_data_0
    .sparse-switch
        -0x3e8 -> :sswitch_5
        -0x6 -> :sswitch_7
        -0x5 -> :sswitch_6
        -0x4 -> :sswitch_2
        -0x3 -> :sswitch_3
        -0x2 -> :sswitch_4
        -0x1 -> :sswitch_1
        0x0 -> :sswitch_0
    .end sparse-switch
.end method
