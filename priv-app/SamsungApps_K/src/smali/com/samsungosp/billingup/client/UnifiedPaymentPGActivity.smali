.class public Lcom/samsungosp/billingup/client/UnifiedPaymentPGActivity;
.super Landroid/app/Activity;
.source "ProGuard"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 11
    const-string v0, "UnifiedPaymentPGResultActivity"

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentPGActivity;->a:Ljava/lang/String;

    .line 12
    const-string v0, "ISP_OK"

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentPGActivity;->b:Ljava/lang/String;

    .line 9
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 17
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 18
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UnifiedPaymentPGResultActivity onCreate"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 20
    invoke-virtual {p0}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 22
    invoke-virtual {v0}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v0

    .line 23
    const-string v1, "UnifiedPaymentPGResultActivity, params is null"

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 25
    if-nez v0, :cond_0

    .line 26
    const-string v1, "UnifiedPaymentPGResultActivity, data is null"

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 27
    invoke-virtual {p0}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGActivity;->finish()V

    .line 30
    :cond_0
    const-string v1, "UPReadersHub://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 31
    const-string v1, "UPLearningHub://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 32
    const-string v1, "UPMediaHub://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 33
    const-string v1, "UPVideoHub://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 34
    const-string v1, "UPMusicHub://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 35
    const-string v1, "UPSamsungCloud://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 36
    const-string v1, "UPSamsungApps://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 38
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 39
    const/high16 v2, 0x24000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 42
    const-string v2, "ISP_SUCCESS"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 43
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "UnifiedPaymentPGResultActivity, ISP_SUCCESS : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 46
    const-string v0, "ISP_OK"

    sput-object v0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->mISPResult:Ljava/lang/String;

    .line 47
    invoke-virtual {p0, v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGActivity;->startActivity(Landroid/content/Intent;)V

    .line 51
    :goto_0
    invoke-virtual {p0}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGActivity;->finish()V

    .line 52
    return-void

    .line 49
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UnifiedPaymentPGResultActivity, no data"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 63
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 64
    const-string v0, "UnifiedPaymentPGResultActivity onDestroy"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 65
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 57
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 58
    const-string v0, "UnifiedPaymentPGResultActivity onResume"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 59
    return-void
.end method
