.class public Lcom/samsungosp/billingup/client/UnifiedPayment;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field public static final TAG:Ljava/lang/String; = "UnifiedPayment"

.field public static VERSION_LOG:Ljava/lang/String;

.field private static a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 14
    const-string v0, "1.01.18"

    sput-object v0, Lcom/samsungosp/billingup/client/UnifiedPayment;->a:Ljava/lang/String;

    .line 16
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[UP_VERSION : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/samsungosp/billingup/client/UnifiedPayment;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsungosp/billingup/client/UnifiedPayment;->VERSION_LOG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkout(Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0, p1}, Lcom/samsungosp/billingup/client/requestparam/RequestParamValidator;->check(Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;Landroid/content/Context;)V

    .line 31
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 32
    const-string v1, "UNIFIED_PAYMENT_REQUEST"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 33
    const-string v1, "PAYMENT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 35
    return-object v0
.end method

.method public static getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/samsungosp/billingup/client/UnifiedPayment;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static makeCreditCardIntent(Lcom/samsungosp/billingup/client/requestparam/CreditCardData;Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 41
    invoke-static {p0, p1}, Lcom/samsungosp/billingup/client/requestparam/RequestParamValidator;->check(Lcom/samsungosp/billingup/client/requestparam/CreditCardData;Landroid/content/Context;)V

    .line 43
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 44
    const-string v1, "CREDIT_CARD_REQUEST"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 45
    const-string v1, "CREDIT_CARD"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 47
    return-object v0
.end method

.method public static makeGiftCardIntent(Lcom/samsungosp/billingup/client/requestparam/GiftCardData;Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 53
    invoke-static {p0, p1}, Lcom/samsungosp/billingup/client/requestparam/RequestParamValidator;->check(Lcom/samsungosp/billingup/client/requestparam/GiftCardData;Landroid/content/Context;)V

    .line 55
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 56
    const-string v1, "GIFT_CARD_REQUEST"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 57
    const-string v1, "GIFT_CARD"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 59
    return-object v0
.end method

.method public static makeRegisterCardIntent(Lcom/samsungosp/billingup/client/requestparam/CreditCardData;Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 64
    invoke-static {p0, p1}, Lcom/samsungosp/billingup/client/requestparam/RequestParamValidator;->check(Lcom/samsungosp/billingup/client/requestparam/CreditCardData;Landroid/content/Context;)V

    .line 66
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsungosp/billingup/client/CreditCardActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 67
    const-string v1, "CREDIT_CARD_REQUEST"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 68
    const-string v1, "REGISTER_CREDIT_CARD"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 70
    return-object v0
.end method

.method public static setVersion(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 23
    sput-object p0, Lcom/samsungosp/billingup/client/UnifiedPayment;->a:Ljava/lang/String;

    .line 24
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[UP_VERSION : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/samsungosp/billingup/client/UnifiedPayment;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsungosp/billingup/client/UnifiedPayment;->VERSION_LOG:Ljava/lang/String;

    .line 25
    return-void
.end method
