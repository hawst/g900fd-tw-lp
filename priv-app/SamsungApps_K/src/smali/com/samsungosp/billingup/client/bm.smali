.class final Lcom/samsungosp/billingup/client/bm;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# instance fields
.field final synthetic a:Lcom/samsungosp/billingup/client/bi;

.field private final synthetic b:Landroid/webkit/WebView;

.field private final synthetic c:Landroid/os/Handler;


# direct methods
.method constructor <init>(Lcom/samsungosp/billingup/client/bi;Landroid/webkit/WebView;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsungosp/billingup/client/bm;->a:Lcom/samsungosp/billingup/client/bi;

    iput-object p2, p0, Lcom/samsungosp/billingup/client/bm;->b:Landroid/webkit/WebView;

    iput-object p3, p0, Lcom/samsungosp/billingup/client/bm;->c:Landroid/os/Handler;

    .line 359
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 364
    const-string v0, "SSLError AlertDialog HW back key press"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 365
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bm;->b:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->stopLoading()V

    .line 366
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bm;->b:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V

    .line 367
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bm;->a:Lcom/samsungosp/billingup/client/bi;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/bi;->b(Lcom/samsungosp/billingup/client/bi;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_0

    .line 370
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bm;->c:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 371
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bm;->a:Lcom/samsungosp/billingup/client/bi;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/bi;->b(Lcom/samsungosp/billingup/client/bi;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 372
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bm;->a:Lcom/samsungosp/billingup/client/bi;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/bi;->c(Lcom/samsungosp/billingup/client/bi;)V

    .line 373
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bm;->c:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 374
    return-void

    .line 368
    :cond_0
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bm;->a:Lcom/samsungosp/billingup/client/bi;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/bi;->b(Lcom/samsungosp/billingup/client/bi;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/webkit/SslErrorHandler;

    invoke-virtual {v0}, Landroid/webkit/SslErrorHandler;->cancel()V

    .line 367
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method
