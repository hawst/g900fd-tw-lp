.class final Lcom/samsungosp/billingup/client/an;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

.field private final synthetic b:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsungosp/billingup/client/an;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    iput-object p2, p0, Lcom/samsungosp/billingup/client/an;->b:Ljava/lang/String;

    .line 2222
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 2226
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/samsungosp/billingup/client/an;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/samsungosp/billingup/client/util/CommonUtil;->checkPGURL(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2227
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[jwmPay] checkPGURL is fasle : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsungosp/billingup/client/an;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 2228
    iget-object v0, p0, Lcom/samsungosp/billingup/client/an;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->setResult(I)V

    .line 2229
    iget-object v0, p0, Lcom/samsungosp/billingup/client/an;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->finish()V

    .line 2261
    :goto_0
    return-void

    .line 2232
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[jwmPay] webMoneyUrl : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsungosp/billingup/client/an;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 2233
    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/samsungosp/billingup/client/an;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v0

    .line 2235
    const-class v2, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;

    .line 2234
    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2236
    const-string v0, "UNIFIED_PAYMENT_PG_TYPE"

    const-string v2, "WEBMONEY"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2245
    :try_start_0
    iget-object v0, p0, Lcom/samsungosp/billingup/client/an;->b:Ljava/lang/String;

    const-string v2, "UTF-8"

    invoke-static {v0, v2}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2246
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2247
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[jwmPay] webMoneyUrl decoded : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 2248
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "[jwmPay] webMoneyUrl real Open : "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "://"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "?"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 2249
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "[jwmPay] webMoneyUrl query : "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 2251
    const-string v0, "UNIFIED_PAYMENT_PG_URL"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2252
    const-string v0, "UNIFIED_PAYMENT_PG_REQUEST"

    invoke-virtual {v2}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2253
    const-string v0, "IS_UX_V2"

    iget-object v2, p0, Lcom/samsungosp/billingup/client/an;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->k(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2260
    :goto_1
    iget-object v0, p0, Lcom/samsungosp/billingup/client/an;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v0

    const/16 v2, 0x66

    invoke-virtual {v0, v1, v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 2255
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_1
.end method
