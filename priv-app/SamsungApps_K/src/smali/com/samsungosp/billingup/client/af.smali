.class final Lcom/samsungosp/billingup/client/af;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

.field private final synthetic b:Ljava/lang/String;

.field private final synthetic c:Ljava/lang/String;

.field private final synthetic d:Ljava/lang/String;

.field private final synthetic e:Ljava/lang/String;

.field private final synthetic f:Ljava/lang/String;

.field private final synthetic g:Ljava/lang/String;

.field private final synthetic h:Ljava/lang/String;

.field private final synthetic i:Ljava/lang/String;

.field private final synthetic j:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsungosp/billingup/client/af;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    iput-object p2, p0, Lcom/samsungosp/billingup/client/af;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/samsungosp/billingup/client/af;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/samsungosp/billingup/client/af;->d:Ljava/lang/String;

    iput-object p5, p0, Lcom/samsungosp/billingup/client/af;->e:Ljava/lang/String;

    iput-object p6, p0, Lcom/samsungosp/billingup/client/af;->f:Ljava/lang/String;

    iput-object p7, p0, Lcom/samsungosp/billingup/client/af;->g:Ljava/lang/String;

    iput-object p8, p0, Lcom/samsungosp/billingup/client/af;->h:Ljava/lang/String;

    iput-object p9, p0, Lcom/samsungosp/billingup/client/af;->i:Ljava/lang/String;

    iput-object p10, p0, Lcom/samsungosp/billingup/client/af;->j:Ljava/lang/String;

    .line 1888
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1891
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[kpiMRCpay] mId : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsungosp/billingup/client/af;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", merchantkey : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1892
    iget-object v1, p0, Lcom/samsungosp/billingup/client/af;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", orderid : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsungosp/billingup/client/af;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1893
    const-string v1, ", returnurl : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsungosp/billingup/client/af;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1894
    const-string v1, ", merchantreserved : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsungosp/billingup/client/af;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1895
    const-string v1, ", paymentAmount : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsungosp/billingup/client/af;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1896
    const-string v1, ", buyername : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsungosp/billingup/client/af;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1897
    const-string v1, ", type : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsungosp/billingup/client/af;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1898
    const-string v1, ", p_noti : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsungosp/billingup/client/af;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1891
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 1900
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/samsungosp/billingup/client/af;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v1

    .line 1901
    const-class v2, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;

    .line 1900
    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1903
    new-instance v1, Lcom/samsungosp/billingup/client/requestparam/InicisMRCRequestData;

    invoke-direct {v1}, Lcom/samsungosp/billingup/client/requestparam/InicisMRCRequestData;-><init>()V

    .line 1905
    iget-object v2, p0, Lcom/samsungosp/billingup/client/af;->b:Ljava/lang/String;

    iput-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/InicisMRCRequestData;->mid:Ljava/lang/String;

    .line 1906
    iget-object v2, p0, Lcom/samsungosp/billingup/client/af;->c:Ljava/lang/String;

    iput-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/InicisMRCRequestData;->merchantkey:Ljava/lang/String;

    .line 1907
    const-string v2, "Samsung Billing"

    iput-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/InicisMRCRequestData;->buyername:Ljava/lang/String;

    .line 1909
    iget-object v2, p0, Lcom/samsungosp/billingup/client/af;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->h(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    move-result-object v2

    iget-object v2, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->productInfo:Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

    iget-object v2, v2, Lcom/samsungosp/billingup/client/requestparam/ProductInfo;->detailProductInfos:[Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;

    array-length v2, v2

    .line 1910
    iget-object v3, p0, Lcom/samsungosp/billingup/client/af;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v3}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->h(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    move-result-object v3

    iget-object v3, v3, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->productInfo:Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

    iget-object v3, v3, Lcom/samsungosp/billingup/client/requestparam/ProductInfo;->detailProductInfos:[Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;->productName:Ljava/lang/String;

    .line 1911
    invoke-static {v3, v2}, Lcom/samsungosp/billingup/client/util/CommonUtil;->multipleProductNameInicis(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/InicisMRCRequestData;->goodname:Ljava/lang/String;

    .line 1912
    const-string v2, "1"

    iput-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/InicisMRCRequestData;->type:Ljava/lang/String;

    .line 1913
    iget-object v2, p0, Lcom/samsungosp/billingup/client/af;->d:Ljava/lang/String;

    iput-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/InicisMRCRequestData;->orderid:Ljava/lang/String;

    .line 1916
    iget-object v2, p0, Lcom/samsungosp/billingup/client/af;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->j(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1917
    iget-object v2, p0, Lcom/samsungosp/billingup/client/af;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->j(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;Ljava/lang/String;)V

    .line 1919
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/samsungosp/billingup/client/af;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v3}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->j(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "up-web/mrc_cb.do"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/InicisMRCRequestData;->returnurl:Ljava/lang/String;

    .line 1920
    iget-object v2, p0, Lcom/samsungosp/billingup/client/af;->f:Ljava/lang/String;

    iput-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/InicisMRCRequestData;->merchantreserved:Ljava/lang/String;

    .line 1921
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyyMMddHHmmss"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 1922
    const-string v3, "UTC"

    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1923
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 1924
    iput-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/InicisMRCRequestData;->timestamp:Ljava/lang/String;

    .line 1925
    iget-object v3, p0, Lcom/samsungosp/billingup/client/af;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v3}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->h(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    move-result-object v3

    iget-object v3, v3, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->productInfo:Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

    iget-object v3, v3, Lcom/samsungosp/billingup/client/requestparam/ProductInfo;->detailProductInfos:[Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;->validityPeriod:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 1926
    iget-object v3, p0, Lcom/samsungosp/billingup/client/af;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v3}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->h(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    move-result-object v3

    iget-object v3, v3, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->productInfo:Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

    iget-object v3, v3, Lcom/samsungosp/billingup/client/requestparam/ProductInfo;->detailProductInfos:[Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;->validityPeriod:Ljava/lang/String;

    iput-object v3, v1, Lcom/samsungosp/billingup/client/requestparam/InicisMRCRequestData;->period:Ljava/lang/String;

    .line 1928
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/samsungosp/billingup/client/af;->b:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/samsungosp/billingup/client/af;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1929
    iget-object v3, p0, Lcom/samsungosp/billingup/client/af;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1928
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsungosp/billingup/client/util/CommonUtil;->changeSHA256(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1930
    iput-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/InicisMRCRequestData;->hashdata:Ljava/lang/String;

    .line 1931
    iget-object v2, p0, Lcom/samsungosp/billingup/client/af;->g:Ljava/lang/String;

    iput-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/InicisMRCRequestData;->price:Ljava/lang/String;

    .line 1933
    const-string v2, "UNIFIED_PAYMENT_PG_TYPE"

    const-string v3, "KPIMRC"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1934
    const-string v2, "UNIFIED_PAYMENT_PG_REQUEST"

    .line 1935
    invoke-virtual {v1}, Lcom/samsungosp/billingup/client/requestparam/InicisMRCRequestData;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1934
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1936
    const-string v2, "IS_UX_V2"

    iget-object v3, p0, Lcom/samsungosp/billingup/client/af;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v3}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->k(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1939
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "after post : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/samsungosp/billingup/client/requestparam/InicisMRCRequestData;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 1940
    iget-object v1, p0, Lcom/samsungosp/billingup/client/af;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v1

    const/16 v2, 0x66

    invoke-virtual {v1, v0, v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1941
    return-void
.end method
