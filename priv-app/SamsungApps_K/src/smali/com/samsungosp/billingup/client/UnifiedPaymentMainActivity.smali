.class public Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;
.super Landroid/app/Activity;
.source "ProGuard"


# static fields
.field public static final ERROR_ID:Ljava/lang/String; = "ERROR_ID"

.field public static final ERROR_MESSAGE:Ljava/lang/String; = "ERROR_MESSAGE"

.field public static final PAYMENT_RECEITE:Ljava/lang/String; = "PAYMENT_RECEITE"

.field public static final RESULT_CANCELD:I = 0x2

.field public static final RESULT_FAILURE:I = 0x3

.field public static final RESULT_OK:I = 0x1

.field public static final RESULT_PENDING:I = 0x4

.field public static final SIGNATURE:Ljava/lang/String; = "SIGNATURE"

.field public static mISPResult:Ljava/lang/String;


# instance fields
.field a:Landroid/os/Handler;

.field b:Ljava/lang/Runnable;

.field c:Ljava/lang/Runnable;

.field d:Ljava/lang/Runnable;

.field e:Ljava/lang/Runnable;

.field private f:Landroid/webkit/WebView;

.field private g:Landroid/app/ProgressDialog;

.field private h:Landroid/app/AlertDialog;

.field private i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

.field private j:Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

.field private k:Lcom/samsungosp/billingup/client/requestparam/GiftCardData;

.field private l:Landroid/content/BroadcastReceiver;

.field private m:Lcom/samsungosp/billingup/client/bi;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Z

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 88
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 113
    new-instance v0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$SamsungAccountBroadcastReceiver;

    invoke-direct {v0, p0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$SamsungAccountBroadcastReceiver;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)V

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->l:Landroid/content/BroadcastReceiver;

    .line 117
    const-string v0, ""

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    .line 118
    const-string v0, ""

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->o:Ljava/lang/String;

    .line 121
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->r:Ljava/lang/String;

    .line 125
    iput-boolean v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->u:Z

    .line 126
    iput-boolean v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->v:Z

    .line 127
    iput-boolean v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->w:Z

    .line 128
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->x:Z

    .line 129
    iput-boolean v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->y:Z

    .line 978
    new-instance v0, Lcom/samsungosp/billingup/client/i;

    invoke-direct {v0, p0}, Lcom/samsungosp/billingup/client/i;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)V

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    .line 2736
    new-instance v0, Lcom/samsungosp/billingup/client/k;

    invoke-direct {v0, p0}, Lcom/samsungosp/billingup/client/k;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)V

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->b:Ljava/lang/Runnable;

    .line 2756
    new-instance v0, Lcom/samsungosp/billingup/client/l;

    invoke-direct {v0, p0}, Lcom/samsungosp/billingup/client/l;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)V

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->c:Ljava/lang/Runnable;

    .line 2778
    new-instance v0, Lcom/samsungosp/billingup/client/m;

    invoke-direct {v0, p0}, Lcom/samsungosp/billingup/client/m;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)V

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->d:Ljava/lang/Runnable;

    .line 2793
    new-instance v0, Lcom/samsungosp/billingup/client/n;

    invoke-direct {v0, p0}, Lcom/samsungosp/billingup/client/n;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)V

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->e:Ljava/lang/Runnable;

    .line 88
    return-void
.end method

.method static synthetic a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->g:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/samsungosp/billingup/client/requestparam/InicisRequestData;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2629
    new-instance v0, Lcom/samsungosp/billingup/client/requestparam/InicisRequestData;

    invoke-direct {v0}, Lcom/samsungosp/billingup/client/requestparam/InicisRequestData;-><init>()V

    iput-object p1, v0, Lcom/samsungosp/billingup/client/requestparam/InicisRequestData;->P_MID:Ljava/lang/String;

    iput-object p2, v0, Lcom/samsungosp/billingup/client/requestparam/InicisRequestData;->P_OID:Ljava/lang/String;

    iput-object p4, v0, Lcom/samsungosp/billingup/client/requestparam/InicisRequestData;->P_AMT:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->userEmail:Ljava/lang/String;

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/CommonUtil;->encryptEmailAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/InicisRequestData;->P_UNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "paymentID="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",requestID="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",paymentMethod=kpi"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/InicisRequestData;->P_NOTI:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "up-web/kcc_cb.do"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/InicisRequestData;->P_NEXT_URL:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->userEmail:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/InicisRequestData;->P_EMAIL:Ljava/lang/String;

    invoke-static {p6}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/InicisRequestData;->P_RETURN_URL:Ljava/lang/String;

    invoke-static {p5}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/InicisRequestData;->P_NOTI_URL:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->productInfo:Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/ProductInfo;->detailProductInfos:[Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;

    array-length v1, v1

    iget-object v2, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v2, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->productInfo:Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

    iget-object v2, v2, Lcom/samsungosp/billingup/client/requestparam/ProductInfo;->detailProductInfos:[Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;->productName:Ljava/lang/String;

    invoke-static {v2, v1}, Lcom/samsungosp/billingup/client/util/CommonUtil;->multipleProductNameInicis(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/InicisRequestData;->P_GOODS:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->productInfo:Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/ProductInfo;->detailProductInfos:[Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;

    aget-object v1, v1, v3

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;->validityPeriod:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->productInfo:Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/ProductInfo;->detailProductInfos:[Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;

    aget-object v1, v1, v3

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;->validityPeriod:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/InicisRequestData;->P_OFFER_PERIOD:Ljava/lang/String;

    :cond_1
    const-string v1, "1"

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/InicisRequestData;->P_HPP_METHOD:Ljava/lang/String;

    invoke-static {p0}, Lcom/samsungosp/billingup/client/util/CommonUtil;->getPhoneNumber(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_8

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/InicisRequestData;->P_MOBILE:Ljava/lang/String;

    const-string v1, ""

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/InicisRequestData;->P_RESERVED:Ljava/lang/String;

    invoke-static {p0}, Lcom/samsungosp/billingup/client/util/CommonUtil;->getSimOperator(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Lcom/samsungosp/billingup/client/util/CommonUtil;->getSimOperatorName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "KTF"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "45002"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "45004"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "45008"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    const-string v1, "KTF"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "nexturl=get&hpp_corp="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&hpp_noauth=Y"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/InicisRequestData;->P_RESERVED:Ljava/lang/String;

    const-string v1, "4"

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/InicisRequestData;->P_HPP_METHOD:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_3
    const-string v3, "SKTelecom"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    const-string v3, "45003"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    const-string v3, "45005"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    const-string v3, "45011"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    :cond_4
    const-string v1, "SKT"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "nexturl=get&hpp_corp="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&hpp_noauth=Y"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/InicisRequestData;->P_RESERVED:Ljava/lang/String;

    const-string v1, "4"

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/InicisRequestData;->P_HPP_METHOD:Ljava/lang/String;

    goto :goto_0

    :cond_5
    const-string v3, "LGT"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    const-string v2, "45006"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    :cond_6
    const-string v1, "LGT"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "nexturl=get&hpp_corp="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/InicisRequestData;->P_RESERVED:Ljava/lang/String;

    const-string v1, "1"

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/InicisRequestData;->P_HPP_METHOD:Ljava/lang/String;

    goto :goto_0

    :cond_7
    const-string v1, "nexturl=get"

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/InicisRequestData;->P_RESERVED:Ljava/lang/String;

    const-string v1, "1"

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/InicisRequestData;->P_HPP_METHOD:Ljava/lang/String;

    goto :goto_0

    :cond_8
    const-string v1, "nexturl=get"

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/InicisRequestData;->P_RESERVED:Ljava/lang/String;

    goto :goto_0
.end method

.method private a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2704
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->userID:Ljava/lang/String;

    .line 2705
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->j:Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->j:Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->userID:Ljava/lang/String;

    goto :goto_0

    .line 2706
    :cond_1
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->k:Lcom/samsungosp/billingup/client/requestparam/GiftCardData;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->userID:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2872
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "showSslAlertDialog baseUrl : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", url : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->h:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->h:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1080027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    sget v1, Lcom/samsungosp/billingup/client/R$string;->ids_ph_header_attention:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    sget v1, Lcom/samsungosp/billingup/client/R$string;->ids_ph_pop_unauthorised_access_to_your_account_has_been_detected_this_payment_will_be_stopped_to_protect_your_information:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    sget v1, Lcom/samsungosp/billingup/client/R$string;->ids_ph_button_stop:I

    new-instance v2, Lcom/samsungosp/billingup/client/s;

    invoke-direct {v2, p0}, Lcom/samsungosp/billingup/client/s;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->h:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->h:Landroid/app/AlertDialog;

    new-instance v1, Lcom/samsungosp/billingup/client/j;

    invoke-direct {v1, p0}, Lcom/samsungosp/billingup/client/j;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->h:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/16 v2, 0x40

    .line 504
    :try_start_0
    invoke-virtual {p0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;->isAlipayInstalled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "install ok"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->productInfo:Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/ProductInfo;->detailProductInfos:[Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;->productName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v2, :cond_0

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->productInfo:Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/ProductInfo;->detailProductInfos:[Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;->productName:Ljava/lang/String;

    const/4 v1, 0x0

    const/16 v2, 0x40

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-static {p2, v0, p1, p3}, Lcom/samsungosp/billingup/client/util/CommonUtil;->getOrderInfoAlipay(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "paymentId : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", productName : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->e(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "orderInfo : "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->e(Ljava/lang/String;)V

    invoke-static {}, Lcom/samsungosp/billingup/client/util/CommonUtil;->getSignTypeAlipay()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/samsungosp/billingup/client/util/CommonUtil;->signAlipay(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "&sign=\""

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\"&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/samsungosp/billingup/client/util/CommonUtil;->getSignTypeAlipay()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;

    invoke-direct {v1}, Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;-><init>()V

    iget-object v2, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    const/16 v3, 0x7a

    invoke-virtual {v1, v0, v2, v3, p0}, Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;->pay(Ljava/lang/String;Landroid/os/Handler;ILandroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v0, "not installed"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    new-instance v0, Lcom/samsungosp/billingup/client/o;

    invoke-direct {v0, p0}, Lcom/samsungosp/billingup/client/o;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)V

    invoke-virtual {p0, v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    const-string v0, "[Alipay] reInit()"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;Z)V
    .locals 0

    .prologue
    .line 125
    iput-boolean p1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->u:Z

    return-void
.end method

.method private b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2713
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->userEmail:Ljava/lang/String;

    .line 2714
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->j:Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->j:Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->userEmail:Ljava/lang/String;

    goto :goto_0

    .line 2715
    :cond_1
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->k:Lcom/samsungosp/billingup/client/requestparam/GiftCardData;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->userEmail:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic b(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->s:Ljava/lang/String;

    return-void
.end method

.method static synthetic b(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;Z)V
    .locals 0

    .prologue
    .line 127
    iput-boolean p1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->w:Z

    return-void
.end method

.method static synthetic b(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Z
    .locals 1

    .prologue
    .line 126
    iget-boolean v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->v:Z

    return v0
.end method

.method static synthetic c(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->f:Landroid/webkit/WebView;

    return-object v0
.end method

.method private c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2721
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->accessToken:Ljava/lang/String;

    .line 2722
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->j:Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->j:Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->accessToken:Ljava/lang/String;

    goto :goto_0

    .line 2723
    :cond_1
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->k:Lcom/samsungosp/billingup/client/requestparam/GiftCardData;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->accessToken:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic c(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->t:Ljava/lang/String;

    return-void
.end method

.method static synthetic c(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;Z)V
    .locals 0

    .prologue
    .line 128
    iput-boolean p1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->x:Z

    return-void
.end method

.method static synthetic d(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->o:Ljava/lang/String;

    return-void
.end method

.method static synthetic d(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Z
    .locals 1

    .prologue
    .line 125
    iget-boolean v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->u:Z

    return v0
.end method

.method static synthetic e(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->r:Ljava/lang/String;

    return-void
.end method

.method static synthetic e(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Z
    .locals 1

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->w:Z

    return v0
.end method

.method static synthetic f(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->p:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->z:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    return-object v0
.end method

.method static synthetic h(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    return-object v0
.end method

.method static synthetic i(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Lcom/samsungosp/billingup/client/requestparam/CreditCardData;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->j:Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    return-object v0
.end method

.method static synthetic j(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic k(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Z
    .locals 1

    .prologue
    .line 129
    iget-boolean v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->y:Z

    return v0
.end method

.method static synthetic l(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2702
    invoke-direct {p0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic m(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2711
    invoke-direct {p0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic n(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2720
    invoke-direct {p0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic o(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->o:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic p(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Landroid/content/BroadcastReceiver;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->l:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic q(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->r:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic r(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->s:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic s(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->t:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public finish()V
    .locals 1

    .prologue
    .line 950
    invoke-super {p0}, Landroid/app/Activity;->finish()V

    .line 951
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->g:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 952
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->g:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 955
    :cond_0
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->h:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->h:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 956
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->h:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 959
    :cond_1
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 836
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 837
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UnifiedPaymentMainActivity onActivityResult : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 839
    const/16 v0, 0x66

    if-ne p1, v0, :cond_10

    .line 840
    if-ne p2, v2, :cond_1

    .line 841
    invoke-virtual {p0, p2, p3}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->setResult(ILandroid/content/Intent;)V

    .line 842
    invoke-virtual {p0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->finish()V

    .line 945
    :cond_0
    :goto_0
    return-void

    .line 843
    :cond_1
    const/4 v0, 0x3

    if-ne p2, v0, :cond_2

    .line 844
    invoke-virtual {p0, p2, p3}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->setResult(ILandroid/content/Intent;)V

    .line 845
    invoke-virtual {p0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->finish()V

    goto :goto_0

    .line 846
    :cond_2
    const/4 v0, 0x4

    if-ne p2, v0, :cond_3

    .line 847
    const-string v0, "ResultCode is ResultPending"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 848
    :cond_3
    const/4 v0, 0x2

    if-ne p2, v0, :cond_4

    .line 849
    const-string v0, "payment result canceled"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 850
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->f:Landroid/webkit/WebView;

    const-string v1, "javascript:reInit()"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0

    .line 853
    :cond_4
    if-nez p2, :cond_6

    .line 855
    sget-object v0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->mISPResult:Ljava/lang/String;

    if-eqz v0, :cond_5

    const-string v0, ""

    sget-object v1, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->mISPResult:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 856
    const-string v0, "ISP payment not reInit"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 857
    const/4 v0, 0x0

    sput-object v0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->mISPResult:Ljava/lang/String;

    goto :goto_0

    .line 859
    :cond_5
    const-string v0, "ISP payment ResultCode is ResultCanceled"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 860
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->f:Landroid/webkit/WebView;

    const-string v1, "javascript:reInit()"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0

    .line 863
    :cond_6
    const/4 v0, 0x5

    if-ne p2, v0, :cond_7

    .line 864
    const-string v0, "Japan Webmoney Status Checking....."

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 865
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->f:Landroid/webkit/WebView;

    const-string v1, "javascript:jwmComplete()"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0

    .line 867
    :cond_7
    const/4 v0, 0x6

    if-ne p2, v0, :cond_8

    .line 868
    const-string v0, "WebMoney or NEW SINA payment is fail"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 869
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->f:Landroid/webkit/WebView;

    const-string v1, "javascript:reInitOnFailPayment()"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0

    .line 870
    :cond_8
    const/4 v0, 0x7

    if-ne p2, v0, :cond_9

    .line 871
    const-string v0, "China NEW SINA Status Checking....."

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 872
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->f:Landroid/webkit/WebView;

    const-string v1, "javascript:csmComplete()"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0

    .line 873
    :cond_9
    const/16 v0, 0x8

    if-ne p2, v0, :cond_a

    .line 874
    const-string v0, "Russia Qiwi Status Checking....."

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 875
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->f:Landroid/webkit/WebView;

    const-string v1, "javascript:rqwComplete()"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 876
    :cond_a
    const/16 v0, 0x9

    if-ne p2, v0, :cond_b

    .line 877
    const-string v0, "LiveGamer Complete call....."

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 878
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->f:Landroid/webkit/WebView;

    const-string v1, "javascript:slgComplete()"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 879
    :cond_b
    const/16 v0, 0xa

    if-ne p2, v0, :cond_c

    .line 880
    const-string v0, "UPoint retrieve call..."

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 881
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->f:Landroid/webkit/WebView;

    const-string v1, "javascript:checkUpointBalance()"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 882
    :cond_c
    const/16 v0, 0xb

    if-ne p2, v0, :cond_d

    .line 883
    const-string v0, "GPP Complete call..."

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 884
    const-string v0, "PAYER_ID"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 885
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GPP payerID : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 886
    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->f:Landroid/webkit/WebView;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "javascript:callGppComplete(\'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\')"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 887
    :cond_d
    const/16 v0, 0xc

    if-ne p2, v0, :cond_e

    .line 888
    const-string v0, "KCC Complete call..."

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 889
    const-string v0, "KCC_QUERY_STRING"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 890
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "KCC QueryString : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 891
    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->f:Landroid/webkit/WebView;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "javascript:callKccComplete(\'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\')"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 892
    :cond_e
    const/16 v0, 0xd

    if-ne p2, v0, :cond_f

    .line 893
    const-string v0, "MRC Complete call..."

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 894
    const-string v0, "MRC_QUERY_STRING"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 895
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MRC queryString : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 896
    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->f:Landroid/webkit/WebView;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "javascript:callKccMrcComplete(\'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\')"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 897
    :cond_f
    const/16 v0, 0xe

    if-ne p2, v0, :cond_0

    .line 898
    const-string v0, "ICC Register complete..."

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 899
    const-string v0, "ICC_QUERY_STRING"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 900
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MRC queryString : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 901
    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->f:Landroid/webkit/WebView;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "javascript:callIccRegister(\'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\')"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 904
    :cond_10
    const/16 v0, 0x64

    if-ne p1, v0, :cond_17

    .line 905
    const/4 v0, -0x1

    if-ne p2, v0, :cond_14

    .line 906
    if-eqz p3, :cond_13

    .line 907
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "parameter userId : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 908
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "received userId : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "userid"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 909
    invoke-virtual {p0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/AccountManager;->getInstance(Landroid/content/Context;)Lcom/samsungosp/billingup/client/AccountManager;

    move-result-object v0

    .line 910
    const-string v1, "userid"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsungosp/billingup/client/AccountManager;->setConfirmed(Ljava/lang/String;Ljava/lang/String;)V

    .line 913
    const-string v0, "userid"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 914
    invoke-direct {p0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a()Ljava/lang/String;

    move-result-object v1

    .line 913
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 914
    if-eqz v0, :cond_11

    .line 916
    invoke-virtual {p0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/AccountManager;->getInstance(Landroid/content/Context;)Lcom/samsungosp/billingup/client/AccountManager;

    move-result-object v0

    .line 917
    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/AccountManager;->getAccountEmail()Ljava/lang/String;

    move-result-object v0

    .line 918
    invoke-direct {p0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_12

    .line 919
    :cond_11
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->z:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    const-string v1, ""

    .line 920
    const-string v2, "Does not match Samsung Account information...."

    .line 919
    invoke-virtual {v0, v1, v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->receivePaymentErrorResult(Ljava/lang/String;Ljava/lang/String;)V

    .line 939
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "resultCode : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 922
    :cond_12
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->f:Landroid/webkit/WebView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setVisibility(I)V

    goto :goto_1

    .line 925
    :cond_13
    const-string v0, "data is null..."

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    goto :goto_1

    .line 928
    :cond_14
    if-nez p2, :cond_15

    .line 929
    invoke-virtual {p0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->finish()V

    goto :goto_1

    .line 931
    :cond_15
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[SamsungAccount] error_code : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "error_code"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 932
    const-string v0, "SAC_0105"

    const-string v1, "error_code"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 933
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->appServiceID:Ljava/lang/String;

    new-instance v1, Lcom/samsungosp/billingup/client/q;

    invoke-direct {v1, p0, v0}, Lcom/samsungosp/billingup/client/q;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 935
    :cond_16
    const-string v0, "[SamsungAccount] confirmPassword error"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 936
    invoke-virtual {p0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->finish()V

    goto :goto_1

    .line 940
    :cond_17
    const/16 v0, 0x65

    if-ne p1, v0, :cond_0

    .line 941
    if-ne p2, v2, :cond_0

    .line 942
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->f:Landroid/webkit/WebView;

    const-string v1, "javascript:getCreditCard()"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 2732
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2734
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const/4 v4, 0x0

    const-wide/32 v9, 0xea60

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 136
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 137
    invoke-virtual {p0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x100

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 139
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UPLibrary Version : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/samsungosp/billingup/client/UnifiedPayment;->getVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->i(Ljava/lang/String;)V

    .line 140
    const-string v0, "UnifiedPaymentMainActivity - onCreate"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 142
    invoke-virtual {p0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 145
    const-string v0, "PAYMENT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 146
    invoke-virtual {p0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "UNIFIED_PAYMENT_REQUEST"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    .line 147
    const-string v0, "v2"

    iget-object v2, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v2, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->uxVersion:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 148
    iput-boolean v8, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->y:Z

    .line 149
    sget v0, Lcom/samsungosp/billingup/client/R$layout;->up_billing_layout_v2:I

    invoke-virtual {p0, v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->setContentView(I)V

    .line 154
    :goto_0
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->libraryVersion:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, ""

    iget-object v2, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v2, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->libraryVersion:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 155
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "[LIB ver]"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v2, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->libraryVersion:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v3, v3, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->appServiceID:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/samsungosp/billingup/client/util/Blowfish;->decrypt(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 156
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->libraryVersion:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v2, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->appServiceID:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/samsungosp/billingup/client/util/Blowfish;->decrypt(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPayment;->setVersion(Ljava/lang/String;)V

    .line 163
    :cond_0
    :goto_1
    sput-object v4, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->mISPResult:Ljava/lang/String;

    .line 165
    new-instance v0, Landroid/app/ProgressDialog;

    sget v2, Lcom/samsungosp/billingup/client/R$style;->TransparentDialog:I

    invoke-direct {v0, p0, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->g:Landroid/app/ProgressDialog;

    .line 166
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->g:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v7}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 167
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->g:Landroid/app/ProgressDialog;

    sget v2, Lcom/samsungosp/billingup/client/R$string;->ids_rh_body_loading_ing:I

    invoke-virtual {p0, v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 169
    sget v0, Lcom/samsungosp/billingup/client/R$id;->webview:I

    invoke-virtual {p0, v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->f:Landroid/webkit/WebView;

    .line 170
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->f:Landroid/webkit/WebView;

    invoke-virtual {v0, v7}, Landroid/webkit/WebView;->setBackgroundColor(I)V

    .line 173
    invoke-static {p0}, Lcom/samsungosp/billingup/client/util/CommonUtil;->isProxyDirectConnectMode(Landroid/content/Context;)Z

    move-result v0

    .line 178
    new-instance v2, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-direct {v2, p0, v7}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;B)V

    iput-object v2, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->z:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    .line 179
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "UPLibrary Android Version : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->z:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-virtual {v3}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->getAndroidVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsungosp/billingup/client/util/UPLog;->i(Ljava/lang/String;)V

    .line 180
    iget-object v2, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->f:Landroid/webkit/WebView;

    iget-object v3, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->z:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    const-string v4, "UnifiedPayment"

    invoke-virtual {v2, v3, v4}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 181
    new-instance v2, Lcom/samsungosp/billingup/client/bi;

    iget-object v3, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    invoke-direct {v2, v3, v0}, Lcom/samsungosp/billingup/client/bi;-><init>(Landroid/os/Handler;Z)V

    iput-object v2, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->m:Lcom/samsungosp/billingup/client/bi;

    .line 182
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->f:Landroid/webkit/WebView;

    iget-object v2, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->m:Lcom/samsungosp/billingup/client/bi;

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 183
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->f:Landroid/webkit/WebView;

    new-instance v2, Lcom/samsungosp/billingup/client/UnifiedPaymentWebChromeClient;

    invoke-direct {v2, p0}, Lcom/samsungosp/billingup/client/UnifiedPaymentWebChromeClient;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 185
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->f:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 186
    invoke-virtual {v0, v8}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 187
    sget-object v2, Landroid/webkit/WebSettings$RenderPriority;->HIGH:Landroid/webkit/WebSettings$RenderPriority;

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setRenderPriority(Landroid/webkit/WebSettings$RenderPriority;)V

    .line 189
    const-wide/32 v2, 0x800000

    invoke-virtual {v0, v2, v3}, Landroid/webkit/WebSettings;->setAppCacheMaxSize(J)V

    .line 190
    const-string v2, "/data/data/com.samsungosp.billingup.client/cache"

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setAppCachePath(Ljava/lang/String;)V

    .line 191
    invoke-virtual {v0, v8}, Landroid/webkit/WebSettings;->setAppCacheEnabled(Z)V

    .line 192
    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setCacheMode(I)V

    .line 195
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->getCacheDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "http"

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 198
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Local Cache DIR : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->getCacheDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    .line 201
    :try_start_0
    const-string v2, "android.net.http.HttpResponseCache"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 202
    const-string v3, "install"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Ljava/io/File;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    sget-object v6, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 203
    const/4 v3, 0x0

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    const-wide/32 v5, 0x800000

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 208
    :goto_2
    const-string v0, "https://mop.samsungosp.com"

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    .line 211
    const-string v0, "PAYMENT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 212
    invoke-virtual {p0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "UNIFIED_PAYMENT_REQUEST"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;

    if-eqz v0, :cond_9

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PAYMENT UnifiedPaymentData.AppServiceID : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->appServiceID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", UnifiedPaymentData.ServiceStoreInfo.Country : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;->country:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    :goto_3
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    invoke-static {}, Lcom/samsungosp/billingup/client/util/CommonUtil;->getTimeOffset()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->timeOffset:Ljava/lang/String;

    :goto_4
    const-string v0, "N"

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->paymentInfo:Lcom/samsungosp/billingup/client/requestparam/PaymentInfo;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/PaymentInfo;->confirmPasswordYN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    const-string v0, "KOR"

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;->country:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    const-string v0, "confirmPassword start"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->appServiceID:Ljava/lang/String;

    new-instance v1, Lcom/samsungosp/billingup/client/p;

    invoke-direct {v1, p0, v0}, Lcom/samsungosp/billingup/client/p;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_5
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->billingServerInfo:Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    new-instance v1, Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;

    invoke-direct {v1}, Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;-><init>()V

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->billingServerInfo:Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;

    :cond_1
    invoke-static {}, Lcom/samsungosp/billingup/client/util/CommonUtil;->getUpjson()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_c

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    :cond_2
    :goto_6
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    const-string v1, "http://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "upServerUrl startWith \'http://\'"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    const-string v1, "http://"

    const-string v2, "https://"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    :cond_3
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->billingServerInfo:Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;

    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;->upServerURL:Ljava/lang/String;

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "/"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    :cond_4
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/JSONUtil;->toJSONString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->p:Ljava/lang/String;

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->p:Ljava/lang/String;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/CommonUtil;->makeJsonDataToPrint(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->q:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "modify upServerUrl : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v9, v10}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    const-string v0, "payment.do"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unifiedPaymentRequest.deviceInfo.displayType : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v2, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    iget-object v2, v2, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->uxVersion:Ljava/lang/String;

    if-eqz v1, :cond_f

    const-string v1, "v2"

    iget-object v2, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v2, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->uxVersion:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    if-eqz v1, :cond_e

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    invoke-static {v1}, Lcom/samsungosp/billingup/client/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_e

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    const-string v2, "T"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "v2/tablet/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_7
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[New UX] V2 url : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    :cond_5
    :goto_8
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/samsungosp/billingup/client/be;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "up-web/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "up-web/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, p0, v3, v0}, Lcom/samsungosp/billingup/client/be;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 222
    :cond_6
    :goto_9
    return-void

    .line 151
    :cond_7
    sget v0, Lcom/samsungosp/billingup/client/R$layout;->up_billing_layout:I

    invoke-virtual {p0, v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->setContentView(I)V

    goto/16 :goto_0

    .line 159
    :cond_8
    sget v0, Lcom/samsungosp/billingup/client/R$layout;->up_billing_layout:I

    invoke-virtual {p0, v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->setContentView(I)V

    goto/16 :goto_1

    .line 205
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    .line 212
    :cond_9
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PAYMENT UnifiedPaymentData.AppServiceID : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->appServiceID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", UnifiedPaymentData.ServiceStoreInfo : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_a
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PAYMENT UnifiedPaymentData : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    goto/16 :goto_4

    :cond_b
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->f:Landroid/webkit/WebView;

    invoke-virtual {v0, v7}, Landroid/webkit/WebView;->setVisibility(I)V

    goto/16 :goto_5

    :cond_c
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->billingServerInfo:Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;->upServerURL:Ljava/lang/String;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->billingServerInfo:Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;->upServerURL:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    goto/16 :goto_6

    :cond_d
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;->country:Ljava/lang/String;

    const-string v1, "CHN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "https://cn-mop.samsungosp.com"

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    goto/16 :goto_6

    :cond_e
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "v2/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v8}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->setRequestedOrientation(I)V

    goto/16 :goto_7

    :cond_f
    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    invoke-static {v1}, Lcom/samsungosp/billingup/client/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    const-string v2, "T"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v0, "tablet_payment.do"

    goto/16 :goto_8

    .line 215
    :cond_10
    const-string v0, "CREDIT_CARD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 216
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->f:Landroid/webkit/WebView;

    invoke-virtual {v0, v7}, Landroid/webkit/WebView;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "CREDIT_CARD_REQUEST"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->j:Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->j:Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->j:Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    invoke-static {}, Lcom/samsungosp/billingup/client/util/CommonUtil;->getTimeOffset()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->timeOffset:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CREDIT_CARD CreditCardData.AppServiceID : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->j:Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->appServiceID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", CreditCardData.Country : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->j:Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->country:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    :goto_a
    invoke-static {}, Lcom/samsungosp/billingup/client/util/CommonUtil;->makeStoreRequestID()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CERDIT CARD storeRequestID : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->j:Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    iput-object v0, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->storeRequestID:Ljava/lang/String;

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->j:Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    if-eqz v0, :cond_11

    invoke-static {}, Lcom/samsungosp/billingup/client/util/CommonUtil;->getUpjson()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_16

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_16

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    :cond_11
    :goto_b
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    const-string v1, "http://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    const-string v0, "upServerUrl startWith \'http://\'"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    const-string v1, "http://"

    const-string v2, "https://"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    :cond_12
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->j:Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->upServerURL:Ljava/lang/String;

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_13

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "/"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    :cond_13
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->j:Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/JSONUtil;->toJSONString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->p:Ljava/lang/String;

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->p:Ljava/lang/String;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/CommonUtil;->makeJsonDataToPrint(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->q:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "modify upServerUrl : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v9, v10}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    const-string v0, "creditcard.do"

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->j:Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->uxVersion:Ljava/lang/String;

    if-eqz v1, :cond_19

    const-string v1, "v2"

    iget-object v2, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->j:Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    iget-object v2, v2, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->uxVersion:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_19

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->j:Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    if-eqz v1, :cond_18

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->j:Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    invoke-static {v1}, Lcom/samsungosp/billingup/client/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_18

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->j:Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    const-string v2, "T"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_18

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "v2/tablet/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_c
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[New UX] V2 url : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    :cond_14
    :goto_d
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/samsungosp/billingup/client/be;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "up-web/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "up-web/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, p0, v3, v0}, Lcom/samsungosp/billingup/client/be;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    goto/16 :goto_9

    :cond_15
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CREDIT_CARD CreditCardData : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i:Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    goto/16 :goto_a

    :cond_16
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->j:Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->upServerURL:Ljava/lang/String;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_17

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->j:Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->upServerURL:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    goto/16 :goto_b

    :cond_17
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->j:Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->country:Ljava/lang/String;

    const-string v1, "CHN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    const-string v0, "https://cn-mop.samsungosp.com"

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    goto/16 :goto_b

    :cond_18
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "v2/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v8}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->setRequestedOrientation(I)V

    goto/16 :goto_c

    :cond_19
    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->j:Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    if-eqz v1, :cond_14

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->j:Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    invoke-static {v1}, Lcom/samsungosp/billingup/client/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_14

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->j:Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    const-string v2, "T"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_14

    const-string v0, "tablet_creditcard.do"

    goto/16 :goto_d

    .line 219
    :cond_1a
    const-string v0, "GIFT_CARD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 220
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->f:Landroid/webkit/WebView;

    invoke-virtual {v0, v7}, Landroid/webkit/WebView;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "GIFT_CARD_REQUEST"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->k:Lcom/samsungosp/billingup/client/requestparam/GiftCardData;

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->k:Lcom/samsungosp/billingup/client/requestparam/GiftCardData;

    if-eqz v0, :cond_1f

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->k:Lcom/samsungosp/billingup/client/requestparam/GiftCardData;

    invoke-static {}, Lcom/samsungosp/billingup/client/util/CommonUtil;->getTimeOffset()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->timeOffset:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "GIFT_CARD GiftCardData.AppServiceID : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->k:Lcom/samsungosp/billingup/client/requestparam/GiftCardData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->appServiceID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", GiftCardData.Country : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->k:Lcom/samsungosp/billingup/client/requestparam/GiftCardData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->country:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    :goto_e
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->k:Lcom/samsungosp/billingup/client/requestparam/GiftCardData;

    if-eqz v0, :cond_1b

    invoke-static {}, Lcom/samsungosp/billingup/client/util/CommonUtil;->getUpjson()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_20

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_20

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    :cond_1b
    :goto_f
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    const-string v1, "http://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    const-string v0, "upServerUrl startWith \'http://\'"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    const-string v1, "http://"

    const-string v2, "https://"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    :cond_1c
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->k:Lcom/samsungosp/billingup/client/requestparam/GiftCardData;

    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->upServerURL:Ljava/lang/String;

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1d

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "/"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    :cond_1d
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->k:Lcom/samsungosp/billingup/client/requestparam/GiftCardData;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/JSONUtil;->toJSONString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->p:Ljava/lang/String;

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->p:Ljava/lang/String;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/CommonUtil;->makeJsonDataToPrint(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->q:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "modify upServerUrl : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v9, v10}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    const-string v0, "giftcard.do"

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->k:Lcom/samsungosp/billingup/client/requestparam/GiftCardData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->uxVersion:Ljava/lang/String;

    if-eqz v1, :cond_23

    const-string v1, "v2"

    iget-object v2, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->k:Lcom/samsungosp/billingup/client/requestparam/GiftCardData;

    iget-object v2, v2, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->uxVersion:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_23

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->k:Lcom/samsungosp/billingup/client/requestparam/GiftCardData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    if-eqz v1, :cond_22

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->k:Lcom/samsungosp/billingup/client/requestparam/GiftCardData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    invoke-static {v1}, Lcom/samsungosp/billingup/client/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_22

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->k:Lcom/samsungosp/billingup/client/requestparam/GiftCardData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    const-string v2, "T"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_22

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "v2/tablet/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_10
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[New UX] V2 url : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    :cond_1e
    :goto_11
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/samsungosp/billingup/client/be;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "up-web/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "up-web/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, p0, v3, v0}, Lcom/samsungosp/billingup/client/be;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    goto/16 :goto_9

    :cond_1f
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "GIFT_CARD GiftCardData : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->k:Lcom/samsungosp/billingup/client/requestparam/GiftCardData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    goto/16 :goto_e

    :cond_20
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->k:Lcom/samsungosp/billingup/client/requestparam/GiftCardData;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->upServerURL:Ljava/lang/String;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_21

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->k:Lcom/samsungosp/billingup/client/requestparam/GiftCardData;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->upServerURL:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    goto/16 :goto_f

    :cond_21
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->k:Lcom/samsungosp/billingup/client/requestparam/GiftCardData;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->country:Ljava/lang/String;

    const-string v1, "CHN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    const-string v0, "https://cn-mop.samsungosp.com"

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n:Ljava/lang/String;

    goto/16 :goto_f

    :cond_22
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "v2/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v8}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->setRequestedOrientation(I)V

    goto/16 :goto_10

    :cond_23
    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->k:Lcom/samsungosp/billingup/client/requestparam/GiftCardData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    if-eqz v1, :cond_1e

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->k:Lcom/samsungosp/billingup/client/requestparam/GiftCardData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    invoke-static {v1}, Lcom/samsungosp/billingup/client/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1e

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->k:Lcom/samsungosp/billingup/client/requestparam/GiftCardData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    const-string v2, "T"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1e

    const-string v0, "tablet_giftcard.do"

    goto/16 :goto_11
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 253
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 254
    const-string v0, "UnifiedPaymentMainActivity - onDestroy"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 255
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->v:Z

    .line 256
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->l:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 257
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 963
    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    .line 964
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UnifiedPaymentMainActivity onKeyDown KEYCODE_BACK mBackKeyFlag : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->x:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    .line 965
    iget-boolean v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->x:Z

    if-eqz v0, :cond_0

    .line 966
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->f:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 967
    const-string v0, "UnifiedPaymentMainActivity WebView goBack()"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    .line 968
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->f:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    .line 969
    const/4 v0, 0x1

    .line 975
    :goto_0
    return v0

    .line 972
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 975
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 826
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 827
    const-string v0, "ISP_SUCCESS"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 828
    const-string v0, "ISP SUCCESS"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 829
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->f:Landroid/webkit/WebView;

    const-string v1, "javascript:ispComplete()"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 831
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 238
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 239
    const-string v0, "UnifiedPaymentMainActivity - onPause"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 240
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 226
    const-string v0, "UnifiedPaymentMainActivity - onResume"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 227
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 228
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 229
    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->l:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 231
    invoke-virtual {p0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/AccountManager;->getInstance(Landroid/content/Context;)Lcom/samsungosp/billingup/client/AccountManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/AccountManager;->isAccountSignedIn()Z

    move-result v0

    if-nez v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->f:Landroid/webkit/WebView;

    const-string v1, "javascript:showExpiredAccountPopup()"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 234
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 235
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 246
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 247
    const-string v0, "UnifiedPaymentMainActivity - onStop"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 248
    return-void
.end method
