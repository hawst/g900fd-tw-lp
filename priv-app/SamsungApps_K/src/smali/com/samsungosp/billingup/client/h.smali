.class final Lcom/samsungosp/billingup/client/h;
.super Landroid/content/BroadcastReceiver;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/samsungosp/billingup/client/SMSManager;


# direct methods
.method constructor <init>(Lcom/samsungosp/billingup/client/SMSManager;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/samsungosp/billingup/client/h;->a:Lcom/samsungosp/billingup/client/SMSManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 65
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 66
    if-nez v0, :cond_1

    .line 82
    :cond_0
    :goto_0
    return-void

    .line 69
    :cond_1
    const-string v1, "UNIFIEDPAYMENT_ACTION_MSG_SENT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PSMS : UNIFIEDPAYMENT_ACTION_MSG_SENT onReceive intent : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", resultCode"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsungosp/billingup/client/h;->getResultCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcom/samsungosp/billingup/client/h;->a:Lcom/samsungosp/billingup/client/SMSManager;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/SMSManager;->a(Lcom/samsungosp/billingup/client/SMSManager;)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/PendingIntent;->cancel()V

    .line 72
    iget-object v0, p0, Lcom/samsungosp/billingup/client/h;->a:Lcom/samsungosp/billingup/client/SMSManager;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/SMSManager;->b(Lcom/samsungosp/billingup/client/SMSManager;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/samsungosp/billingup/client/h;->a:Lcom/samsungosp/billingup/client/SMSManager;

    invoke-static {v1}, Lcom/samsungosp/billingup/client/SMSManager;->c(Lcom/samsungosp/billingup/client/SMSManager;)Lcom/samsungosp/billingup/client/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 73
    iget-object v0, p0, Lcom/samsungosp/billingup/client/h;->a:Lcom/samsungosp/billingup/client/SMSManager;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/SMSManager;->d(Lcom/samsungosp/billingup/client/SMSManager;)V

    .line 74
    iget-object v0, p0, Lcom/samsungosp/billingup/client/h;->a:Lcom/samsungosp/billingup/client/SMSManager;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/SMSManager;->e(Lcom/samsungosp/billingup/client/SMSManager;)Lcom/samsungosp/billingup/client/SMSManager$SMSSendListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 75
    invoke-virtual {p0}, Lcom/samsungosp/billingup/client/h;->getResultCode()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 76
    iget-object v0, p0, Lcom/samsungosp/billingup/client/h;->a:Lcom/samsungosp/billingup/client/SMSManager;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/SMSManager;->e(Lcom/samsungosp/billingup/client/SMSManager;)Lcom/samsungosp/billingup/client/SMSManager$SMSSendListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsungosp/billingup/client/SMSManager$SMSSendListener;->sendSMSSuccess()V

    goto :goto_0

    .line 78
    :cond_2
    iget-object v0, p0, Lcom/samsungosp/billingup/client/h;->a:Lcom/samsungosp/billingup/client/SMSManager;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/SMSManager;->e(Lcom/samsungosp/billingup/client/SMSManager;)Lcom/samsungosp/billingup/client/SMSManager$SMSSendListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsungosp/billingup/client/SMSManager$SMSSendListener;->sendSMSFail()V

    goto :goto_0
.end method
