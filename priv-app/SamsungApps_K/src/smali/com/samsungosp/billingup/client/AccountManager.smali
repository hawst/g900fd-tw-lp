.class public Lcom/samsungosp/billingup/client/AccountManager;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Lcom/samsungosp/billingup/client/AccountManager;


# instance fields
.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsungosp/billingup/client/AccountManager;->a:Ljava/lang/String;

    .line 35
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/samsungosp/billingup/client/AccountManager;->e:Landroid/content/Context;

    .line 51
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 255
    iget-object v0, p0, Lcom/samsungosp/billingup/client/AccountManager;->e:Landroid/content/Context;

    const-string v1, "UPACCOUNT"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 256
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 257
    const-string v1, "E-MAIL"

    invoke-virtual {p0}, Lcom/samsungosp/billingup/client/AccountManager;->getAccountEmail()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsungosp/billingup/client/util/Blowfish;->encrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 258
    const-string v1, "USER-ID"

    invoke-static {p1}, Lcom/samsungosp/billingup/client/util/Blowfish;->encrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 259
    const-string v1, "TIME"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsungosp/billingup/client/util/Blowfish;->encrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 260
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 261
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 264
    new-instance v0, Lorg/json/simple/JSONObject;

    invoke-direct {v0}, Lorg/json/simple/JSONObject;-><init>()V

    .line 265
    const-string v1, "aa1"

    invoke-static {p1}, Lcom/samsungosp/billingup/client/util/SHA512Encrypt;->encrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/simple/JSONObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 266
    const-string v1, "aa2"

    invoke-virtual {p0}, Lcom/samsungosp/billingup/client/AccountManager;->getAccountEmail()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsungosp/billingup/client/util/SHA512Encrypt;->encrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/simple/JSONObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 267
    const-string v1, "aa3"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p2}, Lcom/samsungosp/billingup/client/util/Blowfish;->encrypt(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/simple/JSONObject;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269
    invoke-virtual {v0}, Lorg/json/simple/JSONObject;->toJSONString()Ljava/lang/String;

    move-result-object v0

    .line 270
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MD5 jsonString : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    .line 271
    const/4 v2, 0x0

    .line 273
    :try_start_0
    new-instance v1, Ljava/io/FileWriter;

    new-instance v3, Ljava/io/File;

    sget-object v4, Lcom/samsungosp/billingup/client/AccountManager;->a:Ljava/lang/String;

    const-string v5, "0418hwir93.json"

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v3}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 274
    :try_start_1
    invoke-virtual {v1, v0}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 280
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 290
    :cond_0
    :goto_0
    return-void

    .line 277
    :catch_0
    move-exception v0

    move-object v1, v2

    :goto_1
    :try_start_3
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 280
    if-eqz v1, :cond_0

    .line 281
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 285
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 278
    :catchall_0
    move-exception v0

    .line 280
    :goto_2
    if-eqz v2, :cond_1

    .line 281
    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 287
    :cond_1
    :goto_3
    throw v0

    .line 285
    :catch_2
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 278
    :catchall_1
    move-exception v0

    move-object v2, v1

    goto :goto_2

    .line 277
    :catch_4
    move-exception v0

    goto :goto_1
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/samsungosp/billingup/client/AccountManager;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/samsungosp/billingup/client/AccountManager;->b:Lcom/samsungosp/billingup/client/AccountManager;

    if-nez v0, :cond_0

    .line 44
    new-instance v0, Lcom/samsungosp/billingup/client/AccountManager;

    invoke-direct {v0, p0}, Lcom/samsungosp/billingup/client/AccountManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsungosp/billingup/client/AccountManager;->b:Lcom/samsungosp/billingup/client/AccountManager;

    .line 46
    :cond_0
    sget-object v0, Lcom/samsungosp/billingup/client/AccountManager;->b:Lcom/samsungosp/billingup/client/AccountManager;

    return-object v0
.end method


# virtual methods
.method public getAccountEmail()Ljava/lang/String;
    .locals 2

    .prologue
    .line 311
    iget-object v0, p0, Lcom/samsungosp/billingup/client/AccountManager;->e:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 310
    const-string v1, "com.osp.app.signin"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 313
    array-length v1, v0

    if-lez v1, :cond_0

    .line 314
    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 316
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSavedTimeFromFile(Ljava/lang/String;)J
    .locals 7

    .prologue
    const-wide/16 v3, 0x0

    .line 134
    new-instance v0, Lorg/json/simple/parser/JSONParser;

    invoke-direct {v0}, Lorg/json/simple/parser/JSONParser;-><init>()V

    .line 135
    :try_start_0
    new-instance v1, Ljava/io/FileReader;

    new-instance v2, Ljava/io/File;

    sget-object v5, Lcom/samsungosp/billingup/client/AccountManager;->a:Ljava/lang/String;

    const-string v6, "0418hwir93.json"

    invoke-direct {v2, v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-virtual {v0, v1}, Lorg/json/simple/parser/JSONParser;->parse(Ljava/io/Reader;)Ljava/lang/Object;

    move-result-object v0

    .line 141
    check-cast v0, Lorg/json/simple/JSONObject;

    .line 142
    const-string v1, "aa1"

    invoke-virtual {v0, v1}, Lorg/json/simple/JSONObject;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 143
    const-string v2, "aa2"

    invoke-virtual {v0, v2}, Lorg/json/simple/JSONObject;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 144
    const-string v5, "aa3"

    invoke-virtual {v0, v5}, Lorg/json/simple/JSONObject;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 148
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "TIME : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 150
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    if-nez v0, :cond_1

    :cond_0
    move-wide v0, v3

    .line 170
    :goto_0
    return-wide v0

    .line 154
    :cond_1
    iput-object v1, p0, Lcom/samsungosp/billingup/client/AccountManager;->c:Ljava/lang/String;

    .line 155
    iput-object v2, p0, Lcom/samsungosp/billingup/client/AccountManager;->d:Ljava/lang/String;

    .line 156
    invoke-static {v0, p1}, Lcom/samsungosp/billingup/client/util/Blowfish;->decrypt(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/json/simple/parser/ParseException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 157
    if-nez v0, :cond_2

    move-wide v0, v3

    .line 158
    goto :goto_0

    .line 161
    :catch_0
    move-exception v0

    const-string v0, ""

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->e(Ljava/lang/String;)V

    move-wide v0, v3

    .line 162
    goto :goto_0

    .line 164
    :catch_1
    move-exception v0

    const-string v0, ""

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->e(Ljava/lang/String;)V

    move-wide v0, v3

    .line 165
    goto :goto_0

    .line 167
    :catch_2
    move-exception v0

    const-string v0, ""

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->e(Ljava/lang/String;)V

    move-wide v0, v3

    .line 168
    goto :goto_0

    .line 170
    :cond_2
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public getSavedTimeFromSharedPreferences(Ljava/lang/String;)J
    .locals 7

    .prologue
    const-wide/16 v3, 0x0

    .line 174
    const-string v0, "[getSavedTimeFromSharedPreferences] in"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    .line 179
    :try_start_0
    iget-object v0, p0, Lcom/samsungosp/billingup/client/AccountManager;->e:Landroid/content/Context;

    const-string v1, "account_write_name"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 180
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[getSavedTimeFromSharedPreferences] data : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "account_write_key"

    const-string v5, ""

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    .line 182
    const-string v1, "account_write_key"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/json/simple/JSONValue;->parse(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 183
    check-cast v0, Lorg/json/simple/JSONObject;

    .line 185
    const-string v1, "aa1"

    invoke-virtual {v0, v1}, Lorg/json/simple/JSONObject;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 186
    const-string v2, "aa2"

    invoke-virtual {v0, v2}, Lorg/json/simple/JSONObject;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 187
    const-string v5, "aa3"

    invoke-virtual {v0, v5}, Lorg/json/simple/JSONObject;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 191
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "TIME : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 193
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    if-nez v0, :cond_1

    :cond_0
    move-wide v0, v3

    .line 208
    :goto_0
    return-wide v0

    .line 197
    :cond_1
    iput-object v1, p0, Lcom/samsungosp/billingup/client/AccountManager;->c:Ljava/lang/String;

    .line 198
    iput-object v2, p0, Lcom/samsungosp/billingup/client/AccountManager;->d:Ljava/lang/String;

    .line 199
    invoke-static {v0, p1}, Lcom/samsungosp/billingup/client/util/Blowfish;->decrypt(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 200
    if-nez v0, :cond_2

    move-wide v0, v3

    .line 201
    goto :goto_0

    .line 205
    :catch_0
    move-exception v0

    const-string v0, "[getSavedTimeFromSharedPreferences] exception"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    move-wide v0, v3

    .line 206
    goto :goto_0

    .line 208
    :cond_2
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public isAccountSignedIn()Z
    .locals 2

    .prologue
    .line 320
    iget-object v0, p0, Lcom/samsungosp/billingup/client/AccountManager;->e:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 321
    const-string v1, "com.osp.app.signin"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 322
    array-length v0, v0

    if-lez v0, :cond_0

    .line 324
    const/4 v0, 0x1

    .line 328
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isConfirmed(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    .line 77
    .line 79
    :try_start_0
    iget-object v0, p0, Lcom/samsungosp/billingup/client/AccountManager;->e:Landroid/content/Context;

    const-string v1, "UPACCOUNT"

    const/4 v5, 0x0

    invoke-virtual {v0, v1, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "USER-ID"

    const-string v5, ""

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v5, "E-MAIL"

    const-string v6, ""

    invoke-interface {v0, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "TIME"

    const-string v7, ""

    invoke-interface {v0, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v6, ""

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, ""

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, ""

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/accounts/AccountsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    if-eqz v6, :cond_2

    :cond_0
    move-wide v0, v2

    .line 84
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    .line 86
    invoke-virtual {p0}, Lcom/samsungosp/billingup/client/AccountManager;->getAccountEmail()Ljava/lang/String;

    move-result-object v7

    .line 88
    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    if-nez v7, :cond_3

    :cond_1
    move v0, v4

    .line 99
    :goto_1
    return v0

    .line 79
    :cond_2
    :try_start_1
    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/Blowfish;->decrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v5}, Lcom/samsungosp/billingup/client/util/Blowfish;->decrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/Blowfish;->decrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v1, p0, Lcom/samsungosp/billingup/client/AccountManager;->c:Ljava/lang/String;

    iput-object v5, p0, Lcom/samsungosp/billingup/client/AccountManager;->d:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_1
    .catch Landroid/accounts/AccountsException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-wide v0

    goto :goto_0

    .line 82
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/accounts/AccountsException;->printStackTrace()V

    move-wide v0, v2

    goto :goto_0

    .line 90
    :cond_3
    iget-object v2, p0, Lcom/samsungosp/billingup/client/AccountManager;->d:Ljava/lang/String;

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/samsungosp/billingup/client/AccountManager;->c:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 91
    sub-long v0, v5, v0

    const-wide/32 v2, 0x124f80

    cmp-long v0, v0, v2

    if-gez v0, :cond_4

    .line 92
    const-string v0, "equal email and userID emailId"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 93
    invoke-direct {p0, p1}, Lcom/samsungosp/billingup/client/AccountManager;->a(Ljava/lang/String;)V

    .line 94
    const/4 v0, 0x1

    goto :goto_1

    .line 97
    :cond_4
    const-string v0, "not equal email and userID emailId"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    move v0, v4

    .line 99
    goto :goto_1
.end method

.method public isConfirmed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 10

    .prologue
    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    .line 104
    .line 106
    :try_start_0
    invoke-virtual {p0, p3}, Lcom/samsungosp/billingup/client/AccountManager;->getSavedTimeFromFile(Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/accounts/AccountsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 111
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    .line 113
    invoke-virtual {p0}, Lcom/samsungosp/billingup/client/AccountManager;->getAccountEmail()Ljava/lang/String;

    move-result-object v7

    .line 115
    invoke-static {p1}, Lcom/samsungosp/billingup/client/util/SHA512Encrypt;->encrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 116
    invoke-static {p2}, Lcom/samsungosp/billingup/client/util/SHA512Encrypt;->encrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 119
    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    if-nez v7, :cond_1

    :cond_0
    move v0, v4

    .line 130
    :goto_1
    return v0

    .line 109
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/accounts/AccountsException;->printStackTrace()V

    move-wide v0, v2

    goto :goto_0

    .line 121
    :cond_1
    iget-object v2, p0, Lcom/samsungosp/billingup/client/AccountManager;->d:Ljava/lang/String;

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsungosp/billingup/client/AccountManager;->c:Ljava/lang/String;

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 122
    sub-long v0, v5, v0

    const-wide/32 v2, 0x124f80

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    .line 123
    const-string v0, "equal email and userID emailId"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 124
    invoke-direct {p0, p1, p3}, Lcom/samsungosp/billingup/client/AccountManager;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    const/4 v0, 0x1

    goto :goto_1

    .line 128
    :cond_2
    const-string v0, "not equal email and userID emailId"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    move v0, v4

    .line 130
    goto :goto_1
.end method

.method public setConfirmed(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 216
    invoke-direct {p0, p1}, Lcom/samsungosp/billingup/client/AccountManager;->a(Ljava/lang/String;)V

    .line 217
    return-void
.end method

.method public setConfirmed(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 220
    invoke-direct {p0, p1, p2}, Lcom/samsungosp/billingup/client/AccountManager;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    return-void
.end method
