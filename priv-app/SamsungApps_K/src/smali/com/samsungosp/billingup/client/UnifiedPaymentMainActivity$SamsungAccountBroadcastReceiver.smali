.class public Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$SamsungAccountBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;


# direct methods
.method public constructor <init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)V
    .locals 0

    .prologue
    .line 2809
    iput-object p1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$SamsungAccountBroadcastReceiver;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const-wide/16 v3, 0x12c

    .line 2814
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SamsungAccountBroadcastReceiver : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 2815
    const-string v0, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2817
    const-string v0, "CONNECTIVITY_ACTION "

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 2818
    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 2819
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 2821
    if-eqz v0, :cond_0

    .line 2822
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "activeNetInfo.getType() == "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 2823
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "activeNetInfo.isConnected() == "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 2824
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "activeNetInfo.getDetailedState() == "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 2826
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2827
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$SamsungAccountBroadcastReceiver;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->r(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$SamsungAccountBroadcastReceiver;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->r(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2828
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$SamsungAccountBroadcastReceiver;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->g(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    move-result-object v0

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$SamsungAccountBroadcastReceiver;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->r(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->gcbPayStart(Ljava/lang/String;)V

    .line 2829
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$SamsungAccountBroadcastReceiver;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->b(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;Ljava/lang/String;)V

    .line 2830
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$SamsungAccountBroadcastReceiver;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$SamsungAccountBroadcastReceiver;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->c:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2867
    :cond_0
    :goto_0
    return-void

    .line 2831
    :cond_1
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$SamsungAccountBroadcastReceiver;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->s(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$SamsungAccountBroadcastReceiver;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->s(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2832
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$SamsungAccountBroadcastReceiver;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->g(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    move-result-object v0

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$SamsungAccountBroadcastReceiver;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->s(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->telefonicaPayStart(Ljava/lang/String;)V

    .line 2833
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$SamsungAccountBroadcastReceiver;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->c(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;Ljava/lang/String;)V

    .line 2834
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$SamsungAccountBroadcastReceiver;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$SamsungAccountBroadcastReceiver;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->c:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 2837
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2838
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$SamsungAccountBroadcastReceiver;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->d(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2839
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$SamsungAccountBroadcastReceiver;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$SamsungAccountBroadcastReceiver;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->d:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 2840
    :cond_3
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$SamsungAccountBroadcastReceiver;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->e(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2841
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$SamsungAccountBroadcastReceiver;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$SamsungAccountBroadcastReceiver;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->e:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 2847
    :cond_4
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$SamsungAccountBroadcastReceiver;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->o(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2848
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2850
    const-string v1, "isSuccess"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 2851
    if-eqz v1, :cond_5

    .line 2852
    const-string v1, "trid"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2853
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SamsungAccountBroadcastReceiver s wallet trid : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 2854
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$SamsungAccountBroadcastReceiver;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->c(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Landroid/webkit/WebView;

    move-result-object v0

    const-string v1, "javascript:kwaComplete()"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2856
    :cond_5
    const-string v1, "errorCodeDesc"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2857
    const-string v2, "errorMsg"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2858
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SamsungAccountBroadcastReceiver s wallet errorCodeDesc : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 2859
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SamsungAccountBroadcastReceiver s wallet errMsg : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 2860
    const-string v0, "A000"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2861
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$SamsungAccountBroadcastReceiver;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->c(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Landroid/webkit/WebView;

    move-result-object v0

    const-string v1, "javascript:reInit()"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2863
    :cond_6
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$SamsungAccountBroadcastReceiver;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->c(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Landroid/webkit/WebView;

    move-result-object v0

    const-string v1, "javascript:kwaFail()"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
