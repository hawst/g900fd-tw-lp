.class final Lcom/samsungosp/billingup/client/ad;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

.field private final synthetic b:Ljava/lang/String;

.field private final synthetic c:Ljava/lang/String;

.field private final synthetic d:Ljava/lang/String;

.field private final synthetic e:Ljava/lang/String;

.field private final synthetic f:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsungosp/billingup/client/ad;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    iput-object p2, p0, Lcom/samsungosp/billingup/client/ad;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/samsungosp/billingup/client/ad;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/samsungosp/billingup/client/ad;->d:Ljava/lang/String;

    iput-object p5, p0, Lcom/samsungosp/billingup/client/ad;->e:Ljava/lang/String;

    iput-object p6, p0, Lcom/samsungosp/billingup/client/ad;->f:Ljava/lang/String;

    .line 1764
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1767
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[inicisMRCpay1] mId : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsungosp/billingup/client/ad;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", merchantkey : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1768
    iget-object v1, p0, Lcom/samsungosp/billingup/client/ad;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", orderid : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsungosp/billingup/client/ad;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1769
    const-string v1, ", returnurl : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsungosp/billingup/client/ad;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1770
    const-string v1, ", merchantreserved : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsungosp/billingup/client/ad;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1767
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 1772
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/samsungosp/billingup/client/ad;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v1

    .line 1773
    const-class v2, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;

    .line 1772
    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1775
    new-instance v1, Lcom/samsungosp/billingup/client/requestparam/InicisMRCRequestData;

    invoke-direct {v1}, Lcom/samsungosp/billingup/client/requestparam/InicisMRCRequestData;-><init>()V

    .line 1777
    iget-object v2, p0, Lcom/samsungosp/billingup/client/ad;->b:Ljava/lang/String;

    iput-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/InicisMRCRequestData;->mid:Ljava/lang/String;

    .line 1778
    iget-object v2, p0, Lcom/samsungosp/billingup/client/ad;->c:Ljava/lang/String;

    iput-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/InicisMRCRequestData;->merchantkey:Ljava/lang/String;

    .line 1779
    iget-object v2, p0, Lcom/samsungosp/billingup/client/ad;->f:Ljava/lang/String;

    const-string v3, ".*buyername=.*"

    invoke-virtual {v2, v3}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1780
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "buyername : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsungosp/billingup/client/ad;->f:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsungosp/billingup/client/ad;->f:Ljava/lang/String;

    const-string v5, "buyername="

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v4, v4, 0xa

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 1781
    iget-object v2, p0, Lcom/samsungosp/billingup/client/ad;->f:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsungosp/billingup/client/ad;->f:Ljava/lang/String;

    const-string v4, "buyername="

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0xa

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/InicisMRCRequestData;->buyername:Ljava/lang/String;

    .line 1784
    :cond_0
    iget-object v2, p0, Lcom/samsungosp/billingup/client/ad;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->h(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    move-result-object v2

    iget-object v2, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->productInfo:Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

    iget-object v2, v2, Lcom/samsungosp/billingup/client/requestparam/ProductInfo;->detailProductInfos:[Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;

    array-length v2, v2

    .line 1785
    iget-object v3, p0, Lcom/samsungosp/billingup/client/ad;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v3}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->h(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    move-result-object v3

    iget-object v3, v3, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->productInfo:Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

    iget-object v3, v3, Lcom/samsungosp/billingup/client/requestparam/ProductInfo;->detailProductInfos:[Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;

    aget-object v3, v3, v6

    iget-object v3, v3, Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;->productName:Ljava/lang/String;

    .line 1786
    invoke-static {v3, v2}, Lcom/samsungosp/billingup/client/util/CommonUtil;->multipleProductNameInicis(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/InicisMRCRequestData;->goodname:Ljava/lang/String;

    .line 1788
    iget-object v2, p0, Lcom/samsungosp/billingup/client/ad;->d:Ljava/lang/String;

    iput-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/InicisMRCRequestData;->orderid:Ljava/lang/String;

    .line 1791
    iget-object v2, p0, Lcom/samsungosp/billingup/client/ad;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->j(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1792
    iget-object v2, p0, Lcom/samsungosp/billingup/client/ad;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->j(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;Ljava/lang/String;)V

    .line 1794
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/samsungosp/billingup/client/ad;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v3}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->j(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "up-web/mrc_cb.do"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/InicisMRCRequestData;->returnurl:Ljava/lang/String;

    .line 1795
    iget-object v2, p0, Lcom/samsungosp/billingup/client/ad;->f:Ljava/lang/String;

    iput-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/InicisMRCRequestData;->merchantreserved:Ljava/lang/String;

    .line 1796
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyyMMddHHmmss"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 1797
    const-string v3, "UTC"

    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1798
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 1799
    iput-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/InicisMRCRequestData;->timestamp:Ljava/lang/String;

    .line 1800
    iget-object v3, p0, Lcom/samsungosp/billingup/client/ad;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v3}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->h(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    move-result-object v3

    iget-object v3, v3, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->productInfo:Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

    iget-object v3, v3, Lcom/samsungosp/billingup/client/requestparam/ProductInfo;->detailProductInfos:[Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;

    aget-object v3, v3, v6

    iget-object v3, v3, Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;->validityPeriod:Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 1801
    iget-object v3, p0, Lcom/samsungosp/billingup/client/ad;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v3}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->h(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    move-result-object v3

    iget-object v3, v3, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->productInfo:Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

    iget-object v3, v3, Lcom/samsungosp/billingup/client/requestparam/ProductInfo;->detailProductInfos:[Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;

    aget-object v3, v3, v6

    iget-object v3, v3, Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;->validityPeriod:Ljava/lang/String;

    iput-object v3, v1, Lcom/samsungosp/billingup/client/requestparam/InicisMRCRequestData;->period:Ljava/lang/String;

    .line 1803
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/samsungosp/billingup/client/ad;->b:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/samsungosp/billingup/client/ad;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1804
    iget-object v3, p0, Lcom/samsungosp/billingup/client/ad;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1803
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsungosp/billingup/client/util/CommonUtil;->changeSHA256(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1805
    iput-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/InicisMRCRequestData;->hashdata:Ljava/lang/String;

    .line 1807
    const-string v2, "UNIFIED_PAYMENT_PG_TYPE"

    const-string v3, "INICISMRC"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1808
    const-string v2, "UNIFIED_PAYMENT_PG_REQUEST"

    .line 1809
    invoke-virtual {v1}, Lcom/samsungosp/billingup/client/requestparam/InicisMRCRequestData;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1808
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1810
    const-string v2, "IS_UX_V2"

    iget-object v3, p0, Lcom/samsungosp/billingup/client/ad;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v3}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->k(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1813
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "after post : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/samsungosp/billingup/client/requestparam/InicisMRCRequestData;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 1814
    iget-object v1, p0, Lcom/samsungosp/billingup/client/ad;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v1

    const/16 v2, 0x66

    invoke-virtual {v1, v0, v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1815
    return-void
.end method
