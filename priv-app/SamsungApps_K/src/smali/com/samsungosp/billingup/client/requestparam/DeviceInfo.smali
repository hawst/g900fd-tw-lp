.class public Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public appVersion:Ljava/lang/String;

.field public carrierYN:Ljava/lang/String;

.field public channelID:Ljava/lang/String;

.field public csc:Ljava/lang/String;

.field public deviceID:Ljava/lang/String;

.field public deviceUID:Ljava/lang/String;

.field public displayType:Ljava/lang/String;

.field public language:Ljava/lang/String;

.field public mcc:Ljava/lang/String;

.field public mnc:Ljava/lang/String;

.field public msisdn:Ljava/lang/String;

.field public osVersion:Ljava/lang/String;

.field public shopID:Ljava/lang/String;

.field public userAgent:Ljava/lang/String;

.field public usimType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    new-instance v0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo$1;

    invoke-direct {v0}, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo$1;-><init>()V

    sput-object v0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 80
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static readFromParcel(Landroid/os/Parcel;)Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;
    .locals 2

    .prologue
    .line 51
    new-instance v0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    invoke-direct {v0}, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;-><init>()V

    .line 53
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->shopID:Ljava/lang/String;

    .line 54
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->channelID:Ljava/lang/String;

    .line 55
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->deviceID:Ljava/lang/String;

    .line 56
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->deviceUID:Ljava/lang/String;

    .line 57
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    .line 58
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->language:Ljava/lang/String;

    .line 59
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->mnc:Ljava/lang/String;

    .line 60
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->mcc:Ljava/lang/String;

    .line 61
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->csc:Ljava/lang/String;

    .line 62
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->usimType:Ljava/lang/String;

    .line 63
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->msisdn:Ljava/lang/String;

    .line 64
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->osVersion:Ljava/lang/String;

    .line 65
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->appVersion:Ljava/lang/String;

    .line 66
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->userAgent:Ljava/lang/String;

    .line 67
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->carrierYN:Ljava/lang/String;

    .line 69
    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->shopID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 34
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->channelID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 35
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->deviceID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->deviceUID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 38
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->language:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->mnc:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->mcc:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->csc:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 42
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->usimType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 43
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->msisdn:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 44
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->osVersion:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->appVersion:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 46
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->userAgent:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 47
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->carrierYN:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 48
    return-void
.end method
