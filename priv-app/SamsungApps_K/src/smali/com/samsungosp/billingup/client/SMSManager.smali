.class public Lcom/samsungosp/billingup/client/SMSManager;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Landroid/telephony/SmsManager;

.field private e:Landroid/app/PendingIntent;

.field private final f:Ljava/lang/String;

.field private g:Lcom/samsungosp/billingup/client/h;

.field private h:Lcom/samsungosp/billingup/client/SMSManager$SMSSendListener;

.field private i:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsungosp/billingup/client/SMSManager;->d:Landroid/telephony/SmsManager;

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsungosp/billingup/client/SMSManager;->e:Landroid/app/PendingIntent;

    .line 20
    const-string v0, "UNIFIEDPAYMENT_ACTION_MSG_SENT"

    iput-object v0, p0, Lcom/samsungosp/billingup/client/SMSManager;->f:Ljava/lang/String;

    .line 26
    iput-object p1, p0, Lcom/samsungosp/billingup/client/SMSManager;->i:Landroid/content/Context;

    .line 27
    iput-object p2, p0, Lcom/samsungosp/billingup/client/SMSManager;->a:Ljava/lang/String;

    .line 28
    iput-object p3, p0, Lcom/samsungosp/billingup/client/SMSManager;->b:Ljava/lang/String;

    .line 29
    iput-object p4, p0, Lcom/samsungosp/billingup/client/SMSManager;->c:Ljava/lang/String;

    .line 30
    return-void
.end method

.method static synthetic a(Lcom/samsungosp/billingup/client/SMSManager;)Landroid/app/PendingIntent;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/samsungosp/billingup/client/SMSManager;->e:Landroid/app/PendingIntent;

    return-object v0
.end method

.method static synthetic b(Lcom/samsungosp/billingup/client/SMSManager;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsungosp/billingup/client/SMSManager;->i:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic c(Lcom/samsungosp/billingup/client/SMSManager;)Lcom/samsungosp/billingup/client/h;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/samsungosp/billingup/client/SMSManager;->g:Lcom/samsungosp/billingup/client/h;

    return-object v0
.end method

.method static synthetic d(Lcom/samsungosp/billingup/client/SMSManager;)V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsungosp/billingup/client/SMSManager;->g:Lcom/samsungosp/billingup/client/h;

    return-void
.end method

.method static synthetic e(Lcom/samsungosp/billingup/client/SMSManager;)Lcom/samsungosp/billingup/client/SMSManager$SMSSendListener;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsungosp/billingup/client/SMSManager;->h:Lcom/samsungosp/billingup/client/SMSManager$SMSSendListener;

    return-object v0
.end method


# virtual methods
.method public addSMSSendSuccessListener(Lcom/samsungosp/billingup/client/SMSManager$SMSSendListener;)V
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lcom/samsungosp/billingup/client/SMSManager;->h:Lcom/samsungosp/billingup/client/SMSManager$SMSSendListener;

    .line 34
    return-void
.end method

.method public send()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 37
    iget-object v0, p0, Lcom/samsungosp/billingup/client/SMSManager;->i:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "UNIFIEDPAYMENT_ACTION_MSG_SENT"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v3, v1, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lcom/samsungosp/billingup/client/SMSManager;->e:Landroid/app/PendingIntent;

    .line 39
    iget-object v0, p0, Lcom/samsungosp/billingup/client/SMSManager;->e:Landroid/app/PendingIntent;

    if-eqz v0, :cond_0

    .line 40
    iget-object v0, p0, Lcom/samsungosp/billingup/client/SMSManager;->g:Lcom/samsungosp/billingup/client/h;

    if-nez v0, :cond_0

    .line 41
    new-instance v0, Lcom/samsungosp/billingup/client/h;

    invoke-direct {v0, p0}, Lcom/samsungosp/billingup/client/h;-><init>(Lcom/samsungosp/billingup/client/SMSManager;)V

    iput-object v0, p0, Lcom/samsungosp/billingup/client/SMSManager;->g:Lcom/samsungosp/billingup/client/h;

    .line 42
    iget-object v0, p0, Lcom/samsungosp/billingup/client/SMSManager;->i:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsungosp/billingup/client/SMSManager;->g:Lcom/samsungosp/billingup/client/h;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "UNIFIEDPAYMENT_ACTION_MSG_SENT"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 47
    :cond_0
    :try_start_0
    const-string v0, "SMSManager send : start smsManager.sendTextMessage"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    .line 48
    iget-object v0, p0, Lcom/samsungosp/billingup/client/SMSManager;->d:Landroid/telephony/SmsManager;

    iget-object v1, p0, Lcom/samsungosp/billingup/client/SMSManager;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsungosp/billingup/client/SMSManager;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsungosp/billingup/client/SMSManager;->c:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsungosp/billingup/client/SMSManager;->e:Landroid/app/PendingIntent;

    .line 49
    const/4 v5, 0x0

    .line 48
    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    :cond_1
    :goto_0
    return-void

    .line 51
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 53
    const-string v0, "SMSManager send : fail smsManager.sendTextMessage"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    .line 54
    iget-object v0, p0, Lcom/samsungosp/billingup/client/SMSManager;->h:Lcom/samsungosp/billingup/client/SMSManager$SMSSendListener;

    if-eqz v0, :cond_1

    .line 55
    iget-object v0, p0, Lcom/samsungosp/billingup/client/SMSManager;->h:Lcom/samsungosp/billingup/client/SMSManager$SMSSendListener;

    invoke-interface {v0}, Lcom/samsungosp/billingup/client/SMSManager$SMSSendListener;->sendSMSFail()V

    goto :goto_0
.end method
