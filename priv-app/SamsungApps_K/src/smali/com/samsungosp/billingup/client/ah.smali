.class final Lcom/samsungosp/billingup/client/ah;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

.field private final synthetic b:Ljava/lang/String;

.field private final synthetic c:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsungosp/billingup/client/ah;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    iput-object p2, p0, Lcom/samsungosp/billingup/client/ah;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/samsungosp/billingup/client/ah;->c:Ljava/lang/String;

    .line 1948
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 1952
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsungosp/billingup/client/ah;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/samsungosp/billingup/client/util/CommonUtil;->checkPGURL(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1953
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[gppPay] checkPGURL is fasle : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsungosp/billingup/client/ah;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 1954
    iget-object v0, p0, Lcom/samsungosp/billingup/client/ah;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->setResult(I)V

    .line 1955
    iget-object v0, p0, Lcom/samsungosp/billingup/client/ah;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->finish()V

    .line 1988
    :goto_0
    return-void

    .line 1958
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[gppPay] paypalUrl : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsungosp/billingup/client/ah;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", token : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsungosp/billingup/client/ah;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 1959
    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/samsungosp/billingup/client/ah;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v0

    .line 1961
    const-class v2, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;

    .line 1960
    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1962
    const-string v0, "UNIFIED_PAYMENT_PG_TYPE"

    const-string v2, "PAYPAL"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1969
    :try_start_0
    iget-object v0, p0, Lcom/samsungosp/billingup/client/ah;->b:Ljava/lang/String;

    const-string v2, "UTF-8"

    invoke-static {v0, v2}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1970
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsungosp/billingup/client/ah;->c:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1971
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1972
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[gppPay] tokenUrl : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 1974
    const-string v0, "UNIFIED_PAYMENT_PG_URL"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1975
    const-string v0, "UNIFIED_PAYMENT_PG_REQUEST"

    invoke-virtual {v2}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1976
    const-string v0, "IS_UX_V2"

    iget-object v2, p0, Lcom/samsungosp/billingup/client/ah;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->k(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1987
    :goto_1
    iget-object v0, p0, Lcom/samsungosp/billingup/client/ah;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v0

    const/16 v2, 0x66

    invoke-virtual {v0, v1, v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 1979
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 1982
    const-string v0, "UNIFIED_PAYMENT_PG_URL"

    const-string v2, "https://www.paypal.com/webscr"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1983
    const-string v0, "UNIFIED_PAYMENT_PG_REQUEST"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "cmd=_express-checkout-mobile&token="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsungosp/billingup/client/ah;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1984
    const-string v0, "IS_UX_V2"

    iget-object v2, p0, Lcom/samsungosp/billingup/client/ah;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->k(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_1
.end method
