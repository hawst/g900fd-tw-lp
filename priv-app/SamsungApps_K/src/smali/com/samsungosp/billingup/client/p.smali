.class final Lcom/samsungosp/billingup/client/p;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

.field private final synthetic b:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsungosp/billingup/client/p;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iput-object p2, p0, Lcom/samsungosp/billingup/client/p;->b:Ljava/lang/String;

    .line 568
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 573
    :try_start_0
    iget-object v0, p0, Lcom/samsungosp/billingup/client/p;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 574
    const-string v1, "com.osp.app.signin"

    const/16 v2, 0x80

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 575
    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    .line 576
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[SamsungAccount] version : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 578
    const v1, 0x2244e

    if-lt v0, v1, :cond_2

    iget-object v0, p0, Lcom/samsungosp/billingup/client/p;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->g(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->getAndroidVersion()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    const/16 v1, 0xe

    if-lt v0, v1, :cond_2

    iget-object v0, p0, Lcom/samsungosp/billingup/client/p;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/CommonUtil;->checkResolution(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 579
    iget-object v0, p0, Lcom/samsungosp/billingup/client/p;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 580
    const-string v1, "com.osp.app.signin"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 581
    array-length v0, v0

    if-lez v0, :cond_0

    .line 583
    iget-object v0, p0, Lcom/samsungosp/billingup/client/p;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/AccountManager;->getInstance(Landroid/content/Context;)Lcom/samsungosp/billingup/client/AccountManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsungosp/billingup/client/p;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->l(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/samsungosp/billingup/client/p;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->m(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsungosp/billingup/client/p;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v3}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsungosp/billingup/client/AccountManager;->isConfirmed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 585
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.CONFIRM_PASSWORD_POPUP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 586
    const-string v1, "client_id"

    iget-object v2, p0, Lcom/samsungosp/billingup/client/p;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 587
    const-string v1, "client_secret"

    const-string v2, "asdf"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 588
    const-string v1, "more_info"

    const-string v2, "netflix"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 589
    const-string v1, "theme"

    const-string v2, "light"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 591
    iget-object v1, p0, Lcom/samsungosp/billingup/client/p;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    const/16 v2, 0x64

    invoke-virtual {v1, v0, v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 616
    :cond_0
    :goto_0
    return-void

    .line 594
    :cond_1
    iget-object v0, p0, Lcom/samsungosp/billingup/client/p;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->c(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Landroid/webkit/WebView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setVisibility(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 613
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 614
    iget-object v0, p0, Lcom/samsungosp/billingup/client/p;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->finish()V

    goto :goto_0

    .line 598
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/samsungosp/billingup/client/p;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/AccountManager;->getInstance(Landroid/content/Context;)Lcom/samsungosp/billingup/client/AccountManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsungosp/billingup/client/p;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->l(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/samsungosp/billingup/client/p;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->m(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsungosp/billingup/client/p;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v3}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsungosp/billingup/client/AccountManager;->isConfirmed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 599
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 600
    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.AccountView"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 601
    const-string v1, "client_id"

    iget-object v2, p0, Lcom/samsungosp/billingup/client/p;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 602
    const-string v1, "client_ secret"

    const-string v2, "asdf"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 603
    const-string v1, "account_mode"

    const-string v2, "ACCOUNT_VERIFY"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 604
    const-string v1, "OSP_VER"

    const-string v2, "OSP_02"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 605
    const-string v1, "more_info"

    const-string v2, "netflix"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 606
    iget-object v1, p0, Lcom/samsungosp/billingup/client/p;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    const/16 v2, 0x64

    invoke-virtual {v1, v0, v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 608
    :cond_3
    iget-object v0, p0, Lcom/samsungosp/billingup/client/p;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->c(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Landroid/webkit/WebView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setVisibility(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
