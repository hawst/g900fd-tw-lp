.class public Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public serviceType:Ljava/lang/String;

.field public upServerURL:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    new-instance v0, Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo$1;

    invoke-direct {v0}, Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo$1;-><init>()V

    sput-object v0, Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 42
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static readFromParcel(Landroid/os/Parcel;)Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;
    .locals 2

    .prologue
    .line 26
    new-instance v0, Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;

    invoke-direct {v0}, Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;-><init>()V

    .line 28
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;->serviceType:Ljava/lang/String;

    .line 29
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;->upServerURL:Ljava/lang/String;

    .line 31
    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;->serviceType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 22
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;->upServerURL:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 23
    return-void
.end method
