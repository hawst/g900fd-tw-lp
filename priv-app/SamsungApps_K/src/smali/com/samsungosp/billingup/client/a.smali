.class final Lcom/samsungosp/billingup/client/a;
.super Landroid/os/Handler;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/samsungosp/billingup/client/CreditCardActivity;


# direct methods
.method constructor <init>(Lcom/samsungosp/billingup/client/CreditCardActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsungosp/billingup/client/a;->a:Lcom/samsungosp/billingup/client/CreditCardActivity;

    .line 248
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 250
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CreditCardActivity Handler what : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    .line 251
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 305
    :cond_0
    :goto_0
    return-void

    .line 254
    :sswitch_0
    iget-object v0, p0, Lcom/samsungosp/billingup/client/a;->a:Lcom/samsungosp/billingup/client/CreditCardActivity;

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/CreditCardActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 255
    iget-object v0, p0, Lcom/samsungosp/billingup/client/a;->a:Lcom/samsungosp/billingup/client/CreditCardActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/CreditCardActivity;->a(Lcom/samsungosp/billingup/client/CreditCardActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 256
    iget-object v0, p0, Lcom/samsungosp/billingup/client/a;->a:Lcom/samsungosp/billingup/client/CreditCardActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/CreditCardActivity;->a(Lcom/samsungosp/billingup/client/CreditCardActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 257
    iget-object v0, p0, Lcom/samsungosp/billingup/client/a;->a:Lcom/samsungosp/billingup/client/CreditCardActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/CreditCardActivity;->a(Lcom/samsungosp/billingup/client/CreditCardActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    sget v1, Lcom/samsungosp/billingup/client/R$layout;->common_layout_progressbar_popup:I

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setContentView(I)V

    goto :goto_0

    .line 262
    :sswitch_1
    iget-object v0, p0, Lcom/samsungosp/billingup/client/a;->a:Lcom/samsungosp/billingup/client/CreditCardActivity;

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/CreditCardActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 263
    iget-object v0, p0, Lcom/samsungosp/billingup/client/a;->a:Lcom/samsungosp/billingup/client/CreditCardActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/CreditCardActivity;->b(Lcom/samsungosp/billingup/client/CreditCardActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 264
    iget-object v0, p0, Lcom/samsungosp/billingup/client/a;->a:Lcom/samsungosp/billingup/client/CreditCardActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/CreditCardActivity;->a(Lcom/samsungosp/billingup/client/CreditCardActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    goto :goto_0

    .line 270
    :sswitch_2
    const-string v0, "handler received PAGE_ERROR"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 271
    iget-object v0, p0, Lcom/samsungosp/billingup/client/a;->a:Lcom/samsungosp/billingup/client/CreditCardActivity;

    invoke-virtual {v0, v5}, Lcom/samsungosp/billingup/client/CreditCardActivity;->setResult(I)V

    .line 272
    iget-object v0, p0, Lcom/samsungosp/billingup/client/a;->a:Lcom/samsungosp/billingup/client/CreditCardActivity;

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/CreditCardActivity;->finish()V

    goto :goto_0

    .line 275
    :sswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/String;

    .line 276
    if-eqz v0, :cond_0

    array-length v1, v0

    if-ne v1, v2, :cond_0

    .line 277
    iget-object v1, p0, Lcom/samsungosp/billingup/client/a;->a:Lcom/samsungosp/billingup/client/CreditCardActivity;

    aget-object v2, v0, v3

    aget-object v0, v0, v4

    invoke-static {v1, v2, v0}, Lcom/samsungosp/billingup/client/CreditCardActivity;->a(Lcom/samsungosp/billingup/client/CreditCardActivity;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 281
    :sswitch_4
    const-string v0, "register card success"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 282
    iget-object v0, p0, Lcom/samsungosp/billingup/client/a;->a:Lcom/samsungosp/billingup/client/CreditCardActivity;

    invoke-virtual {v0, v4}, Lcom/samsungosp/billingup/client/CreditCardActivity;->setResult(I)V

    .line 283
    iget-object v0, p0, Lcom/samsungosp/billingup/client/a;->a:Lcom/samsungosp/billingup/client/CreditCardActivity;

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/CreditCardActivity;->finish()V

    goto :goto_0

    .line 286
    :sswitch_5
    const-string v0, "register card cancel"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 287
    iget-object v0, p0, Lcom/samsungosp/billingup/client/a;->a:Lcom/samsungosp/billingup/client/CreditCardActivity;

    invoke-virtual {v0, v2}, Lcom/samsungosp/billingup/client/CreditCardActivity;->setResult(I)V

    .line 288
    iget-object v0, p0, Lcom/samsungosp/billingup/client/a;->a:Lcom/samsungosp/billingup/client/CreditCardActivity;

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/CreditCardActivity;->finish()V

    goto/16 :goto_0

    .line 291
    :sswitch_6
    const-string v0, "register card fail"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 292
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 293
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CreditCardActivity queryString : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 294
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "@@"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v3

    .line 295
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "@@"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    aget-object v2, v2, v4

    .line 296
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "errorId : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", errorMessage : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 298
    const-string v3, "ERROR_ID"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 299
    const-string v1, "ERROR_MESSAGE"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 300
    iget-object v1, p0, Lcom/samsungosp/billingup/client/a;->a:Lcom/samsungosp/billingup/client/CreditCardActivity;

    invoke-virtual {v1, v5, v0}, Lcom/samsungosp/billingup/client/CreditCardActivity;->setResult(ILandroid/content/Intent;)V

    .line 301
    iget-object v0, p0, Lcom/samsungosp/billingup/client/a;->a:Lcom/samsungosp/billingup/client/CreditCardActivity;

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/CreditCardActivity;->finish()V

    goto/16 :goto_0

    .line 251
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x10 -> :sswitch_4
        0x11 -> :sswitch_5
        0x12 -> :sswitch_6
        0x25 -> :sswitch_3
    .end sparse-switch
.end method
