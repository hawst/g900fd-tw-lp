.class final Lcom/samsungosp/billingup/client/b;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/samsungosp/billingup/client/CreditCardActivity;


# direct methods
.method constructor <init>(Lcom/samsungosp/billingup/client/CreditCardActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsungosp/billingup/client/b;->a:Lcom/samsungosp/billingup/client/CreditCardActivity;

    .line 309
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 313
    const-string v0, "over 1 minute oneMinuteCheckRunnable"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->i(Ljava/lang/String;)V

    .line 314
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 315
    const-string v1, "ERROR_MESSAGE"

    const-string v2, "Waiting time has been exceeded."

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 317
    iget-object v1, p0, Lcom/samsungosp/billingup/client/b;->a:Lcom/samsungosp/billingup/client/CreditCardActivity;

    .line 318
    iget-object v2, p0, Lcom/samsungosp/billingup/client/b;->a:Lcom/samsungosp/billingup/client/CreditCardActivity;

    sget v3, Lcom/samsungosp/billingup/client/R$string;->ids_ph_pop_unable_to_connect_to_network_try_later:I

    invoke-virtual {v2, v3}, Lcom/samsungosp/billingup/client/CreditCardActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 319
    const/4 v3, 0x0

    .line 316
    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 319
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 320
    iget-object v1, p0, Lcom/samsungosp/billingup/client/b;->a:Lcom/samsungosp/billingup/client/CreditCardActivity;

    const/4 v2, 0x3

    invoke-virtual {v1, v2, v0}, Lcom/samsungosp/billingup/client/CreditCardActivity;->setResult(ILandroid/content/Intent;)V

    .line 321
    iget-object v0, p0, Lcom/samsungosp/billingup/client/b;->a:Lcom/samsungosp/billingup/client/CreditCardActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/CreditCardActivity;->c(Lcom/samsungosp/billingup/client/CreditCardActivity;)Landroid/webkit/WebView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->clearCache(Z)V

    .line 322
    iget-object v0, p0, Lcom/samsungosp/billingup/client/b;->a:Lcom/samsungosp/billingup/client/CreditCardActivity;

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/CreditCardActivity;->finish()V

    .line 323
    return-void
.end method
