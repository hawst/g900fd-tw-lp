.class final Lcom/samsungosp/billingup/client/alipay/c;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;

.field private final synthetic b:Ljava/lang/String;

.field private final synthetic c:I

.field private final synthetic d:Landroid/os/Handler;


# direct methods
.method constructor <init>(Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;Ljava/lang/String;ILandroid/os/Handler;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsungosp/billingup/client/alipay/c;->a:Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;

    iput-object p2, p0, Lcom/samsungosp/billingup/client/alipay/c;->b:Ljava/lang/String;

    iput p3, p0, Lcom/samsungosp/billingup/client/alipay/c;->c:I

    iput-object p4, p0, Lcom/samsungosp/billingup/client/alipay/c;->d:Landroid/os/Handler;

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 81
    :try_start_0
    iget-object v0, p0, Lcom/samsungosp/billingup/client/alipay/c;->a:Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;

    iget-object v1, v0, Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;->b:Ljava/lang/Integer;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    :try_start_1
    iget-object v0, p0, Lcom/samsungosp/billingup/client/alipay/c;->a:Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;->c:Lcom/alipay/android/app/IAlixPay;

    if-nez v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/samsungosp/billingup/client/alipay/c;->a:Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V

    .line 81
    :cond_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 88
    :try_start_2
    iget-object v0, p0, Lcom/samsungosp/billingup/client/alipay/c;->a:Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;->c:Lcom/alipay/android/app/IAlixPay;

    iget-object v1, p0, Lcom/samsungosp/billingup/client/alipay/c;->a:Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;

    invoke-static {v1}, Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;->a(Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;)Lcom/alipay/android/app/IRemoteServiceCallback;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/alipay/android/app/IAlixPay;->registerCallback(Lcom/alipay/android/app/IRemoteServiceCallback;)V

    .line 91
    iget-object v0, p0, Lcom/samsungosp/billingup/client/alipay/c;->a:Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;->c:Lcom/alipay/android/app/IAlixPay;

    iget-object v1, p0, Lcom/samsungosp/billingup/client/alipay/c;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/alipay/android/app/IAlixPay;->Pay(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 96
    iget-object v1, p0, Lcom/samsungosp/billingup/client/alipay/c;->a:Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;->d:Z

    .line 97
    iget-object v1, p0, Lcom/samsungosp/billingup/client/alipay/c;->a:Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;->c:Lcom/alipay/android/app/IAlixPay;

    iget-object v2, p0, Lcom/samsungosp/billingup/client/alipay/c;->a:Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;

    invoke-static {v2}, Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;->a(Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;)Lcom/alipay/android/app/IRemoteServiceCallback;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/alipay/android/app/IAlixPay;->unregisterCallback(Lcom/alipay/android/app/IRemoteServiceCallback;)V

    .line 98
    iget-object v1, p0, Lcom/samsungosp/billingup/client/alipay/c;->a:Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;->e:Landroid/app/Activity;

    iget-object v2, p0, Lcom/samsungosp/billingup/client/alipay/c;->a:Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;

    invoke-static {v2}, Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;->b(Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;)Landroid/content/ServiceConnection;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Activity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 101
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 102
    iget v2, p0, Lcom/samsungosp/billingup/client/alipay/c;->c:I

    iput v2, v1, Landroid/os/Message;->what:I

    .line 103
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 104
    iget-object v0, p0, Lcom/samsungosp/billingup/client/alipay/c;->d:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 116
    :goto_0
    return-void

    .line 81
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 106
    :catch_0
    move-exception v0

    .line 108
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 111
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 112
    iget v2, p0, Lcom/samsungosp/billingup/client/alipay/c;->c:I

    iput v2, v1, Landroid/os/Message;->what:I

    .line 113
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 114
    iget-object v0, p0, Lcom/samsungosp/billingup/client/alipay/c;->d:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
