.class public Lcom/samsungosp/billingup/client/CreditCardActivity;
.super Landroid/app/Activity;
.source "ProGuard"


# static fields
.field public static final ERROR_ID:Ljava/lang/String; = "ERROR_ID"

.field public static final ERROR_MESSAGE:Ljava/lang/String; = "ERROR_MESSAGE"

.field public static final RESULT_CANCELD:I = 0x2

.field public static final RESULT_FAILURE:I = 0x3

.field public static final RESULT_OK:I = 0x1

.field public static final RESULT_PENDING:I = 0x4


# instance fields
.field a:Ljava/lang/Runnable;

.field private b:Lcom/samsungosp/billingup/client/bi;

.field private c:Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

.field private d:Landroid/webkit/WebView;

.field private e:Landroid/app/ProgressDialog;

.field private f:Landroid/app/AlertDialog;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Z

.field private j:Z

.field private k:Ljava/lang/String;

.field private l:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->i:Z

    .line 65
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->j:Z

    .line 67
    const-string v0, ""

    iput-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->k:Ljava/lang/String;

    .line 248
    new-instance v0, Lcom/samsungosp/billingup/client/a;

    invoke-direct {v0, p0}, Lcom/samsungosp/billingup/client/a;-><init>(Lcom/samsungosp/billingup/client/CreditCardActivity;)V

    iput-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->l:Landroid/os/Handler;

    .line 309
    new-instance v0, Lcom/samsungosp/billingup/client/b;

    invoke-direct {v0, p0}, Lcom/samsungosp/billingup/client/b;-><init>(Lcom/samsungosp/billingup/client/CreditCardActivity;)V

    iput-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->a:Ljava/lang/Runnable;

    .line 43
    return-void
.end method

.method static synthetic a(Lcom/samsungosp/billingup/client/CreditCardActivity;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->e:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic a(Lcom/samsungosp/billingup/client/CreditCardActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 515
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "showSslAlertDialog baseUrl : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", url : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->f:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->f:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1080027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    sget v1, Lcom/samsungosp/billingup/client/R$string;->ids_ph_header_attention:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    sget v1, Lcom/samsungosp/billingup/client/R$string;->ids_ph_pop_unauthorised_access_to_your_account_has_been_detected_this_payment_will_be_stopped_to_protect_your_information:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    sget v1, Lcom/samsungosp/billingup/client/R$string;->ids_ph_button_stop:I

    new-instance v2, Lcom/samsungosp/billingup/client/c;

    invoke-direct {v2, p0}, Lcom/samsungosp/billingup/client/c;-><init>(Lcom/samsungosp/billingup/client/CreditCardActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->f:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->f:Landroid/app/AlertDialog;

    new-instance v1, Lcom/samsungosp/billingup/client/d;

    invoke-direct {v1, p0}, Lcom/samsungosp/billingup/client/d;-><init>(Lcom/samsungosp/billingup/client/CreditCardActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    iget-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->f:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/samsungosp/billingup/client/CreditCardActivity;)Z
    .locals 1

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->i:Z

    return v0
.end method

.method static synthetic c(Lcom/samsungosp/billingup/client/CreditCardActivity;)Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->d:Landroid/webkit/WebView;

    return-object v0
.end method

.method static synthetic d(Lcom/samsungosp/billingup/client/CreditCardActivity;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->l:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic e(Lcom/samsungosp/billingup/client/CreditCardActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public finish()V
    .locals 1

    .prologue
    .line 503
    invoke-super {p0}, Landroid/app/Activity;->finish()V

    .line 504
    const-string v0, "CreditCardActivity - finish"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 505
    iget-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 506
    iget-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 509
    :cond_0
    iget-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->f:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->f:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 510
    iget-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->f:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 513
    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const-wide/32 v3, 0x800000

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 72
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 73
    invoke-virtual {p0}, Lcom/samsungosp/billingup/client/CreditCardActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x100

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UPLibrary Version : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/samsungosp/billingup/client/UnifiedPayment;->getVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->i(Ljava/lang/String;)V

    .line 76
    const-string v0, "CreditCardActivity - onCreate"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 77
    sget v0, Lcom/samsungosp/billingup/client/R$layout;->up_billing_layout_v2:I

    invoke-virtual {p0, v0}, Lcom/samsungosp/billingup/client/CreditCardActivity;->setContentView(I)V

    .line 80
    new-instance v0, Landroid/app/ProgressDialog;

    sget v1, Lcom/samsungosp/billingup/client/R$style;->TransparentDialog:I

    invoke-direct {v0, p0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->e:Landroid/app/ProgressDialog;

    .line 81
    iget-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v6}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 82
    iget-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->e:Landroid/app/ProgressDialog;

    sget v1, Lcom/samsungosp/billingup/client/R$string;->ids_rh_body_loading_ing:I

    invoke-virtual {p0, v1}, Lcom/samsungosp/billingup/client/CreditCardActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 84
    sget v0, Lcom/samsungosp/billingup/client/R$id;->webview:I

    invoke-virtual {p0, v0}, Lcom/samsungosp/billingup/client/CreditCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->d:Landroid/webkit/WebView;

    .line 85
    iget-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->d:Landroid/webkit/WebView;

    invoke-virtual {v0, v6}, Landroid/webkit/WebView;->setBackgroundColor(I)V

    .line 88
    invoke-static {p0}, Lcom/samsungosp/billingup/client/util/CommonUtil;->isProxyDirectConnectMode(Landroid/content/Context;)Z

    move-result v0

    .line 96
    new-instance v1, Lcom/samsungosp/billingup/client/bi;

    iget-object v2, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->l:Landroid/os/Handler;

    invoke-direct {v1, v2, v0}, Lcom/samsungosp/billingup/client/bi;-><init>(Landroid/os/Handler;Z)V

    iput-object v1, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->b:Lcom/samsungosp/billingup/client/bi;

    .line 97
    iget-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->d:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->b:Lcom/samsungosp/billingup/client/bi;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 98
    iget-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->d:Landroid/webkit/WebView;

    new-instance v1, Lcom/samsungosp/billingup/client/UnifiedPaymentWebChromeClient;

    invoke-direct {v1, p0}, Lcom/samsungosp/billingup/client/UnifiedPaymentWebChromeClient;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 100
    iget-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->d:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 101
    invoke-virtual {v0, v7}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 102
    sget-object v1, Landroid/webkit/WebSettings$RenderPriority;->HIGH:Landroid/webkit/WebSettings$RenderPriority;

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setRenderPriority(Landroid/webkit/WebSettings$RenderPriority;)V

    .line 104
    invoke-virtual {v0, v3, v4}, Landroid/webkit/WebSettings;->setAppCacheMaxSize(J)V

    .line 105
    const-string v1, "/data/data/com.samsungosp.billingup.client/cache"

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setAppCachePath(Ljava/lang/String;)V

    .line 106
    invoke-virtual {v0, v7}, Landroid/webkit/WebSettings;->setAppCacheEnabled(Z)V

    .line 107
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setCacheMode(I)V

    .line 110
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/samsungosp/billingup/client/CreditCardActivity;->getCacheDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "http"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 113
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Local Cache DIR : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/samsungosp/billingup/client/CreditCardActivity;->getCacheDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    .line 116
    :try_start_0
    const-string v1, "android.net.http.HttpResponseCache"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 117
    const-string v2, "install"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Ljava/io/File;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    sget-object v5, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 118
    const/4 v2, 0x0

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    const-wide/32 v4, 0x800000

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 123
    :goto_0
    iget-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->d:Landroid/webkit/WebView;

    invoke-virtual {v0, v6}, Landroid/webkit/WebView;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/samsungosp/billingup/client/CreditCardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "CREDIT_CARD_REQUEST"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    iput-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->c:Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    invoke-static {}, Lcom/samsungosp/billingup/client/util/CommonUtil;->getUpjson()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iput-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->k:Ljava/lang/String;

    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->c:Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->k:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->upServerURL:Ljava/lang/String;

    iget-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->k:Ljava/lang/String;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->k:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "/"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->k:Ljava/lang/String;

    :cond_1
    iget-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->c:Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/JSONUtil;->toJSONString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->g:Ljava/lang/String;

    iget-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->g:Ljava/lang/String;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/CommonUtil;->makeJsonDataToPrint(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->h:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "modify jsonDataToPrint : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "modify upServerUrl : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->c:Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->upServerURL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    const-string v0, "v2/register_creditcard.do"

    iget-object v1, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->c:Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->c:Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    invoke-static {v1}, Lcom/samsungosp/billingup/client/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->c:Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->displayType:Ljava/lang/String;

    const-string v2, "T"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v0, "v2/tablet/register_creditcard.do"

    :goto_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[New UX] V2 url : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/samsungosp/billingup/client/e;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->k:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "up-web/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->k:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "up-web/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, p0, v3, v0}, Lcom/samsungosp/billingup/client/e;-><init>(Lcom/samsungosp/billingup/client/CreditCardActivity;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 124
    return-void

    .line 120
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 123
    :cond_2
    iget-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->c:Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->upServerURL:Ljava/lang/String;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/requestparam/RequestParamValidator;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->c:Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->upServerURL:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->k:Ljava/lang/String;

    goto/16 :goto_1

    :cond_3
    iget-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->c:Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->country:Ljava/lang/String;

    const-string v1, "CHN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "https://cn-mop.samsungosp.com"

    iput-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->k:Ljava/lang/String;

    goto/16 :goto_1

    :cond_4
    invoke-virtual {p0, v7}, Lcom/samsungosp/billingup/client/CreditCardActivity;->setRequestedOrientation(I)V

    goto :goto_2
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 495
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 496
    const-string v0, "CreditCardActivity - onDestroy"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 497
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->i:Z

    .line 498
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 234
    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    .line 235
    iget-boolean v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->j:Z

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->d:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 237
    iget-object v0, p0, Lcom/samsungosp/billingup/client/CreditCardActivity;->d:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    .line 238
    const/4 v0, 0x1

    .line 244
    :goto_0
    return v0

    .line 241
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 244
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 488
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 489
    const-string v0, "CreditCardActivity - onPause"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 490
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 481
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 482
    const-string v0, "CreditCardActivity - onResume"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 483
    return-void
.end method
