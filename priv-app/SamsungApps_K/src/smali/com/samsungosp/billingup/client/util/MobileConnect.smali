.class public Lcom/samsungosp/billingup/client/util/MobileConnect;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field public static mNetWorkState:Lcom/samsungosp/billingup/client/util/MobileConnect$NetWorkState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/samsungosp/billingup/client/util/MobileConnect$NetWorkState;->Not_Change_NetWork:Lcom/samsungosp/billingup/client/util/MobileConnect$NetWorkState;

    sput-object v0, Lcom/samsungosp/billingup/client/util/MobileConnect;->mNetWorkState:Lcom/samsungosp/billingup/client/util/MobileConnect$NetWorkState;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 159
    :try_start_0
    invoke-static {p0}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 166
    invoke-virtual {v0}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v0

    .line 167
    const/4 v1, 0x3

    aget-byte v1, v0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x18

    const/4 v2, 0x2

    aget-byte v2, v0, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x10

    or-int/2addr v1, v2

    .line 168
    const/4 v2, 0x1

    aget-byte v2, v0, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    .line 167
    or-int/2addr v1, v2

    .line 168
    const/4 v2, 0x0

    aget-byte v0, v0, v2

    and-int/lit16 v0, v0, 0xff

    .line 167
    or-int/2addr v0, v1

    .line 169
    :goto_0
    return v0

    .line 162
    :catch_0
    move-exception v0

    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static connectMobileNetWork(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 175
    .line 176
    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 175
    check-cast v0, Landroid/net/ConnectivityManager;

    .line 177
    sget-object v3, Lcom/samsungosp/billingup/client/util/MobileConnect$NetWorkState;->Not_Change_NetWork:Lcom/samsungosp/billingup/client/util/MobileConnect$NetWorkState;

    sput-object v3, Lcom/samsungosp/billingup/client/util/MobileConnect;->mNetWorkState:Lcom/samsungosp/billingup/client/util/MobileConnect$NetWorkState;

    .line 179
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 180
    sget-object v0, Lcom/samsungosp/billingup/client/util/MobileConnect$NetWorkState;->DirectBillingAuthUrlCheck:Lcom/samsungosp/billingup/client/util/MobileConnect$NetWorkState;

    sput-object v0, Lcom/samsungosp/billingup/client/util/MobileConnect;->mNetWorkState:Lcom/samsungosp/billingup/client/util/MobileConnect$NetWorkState;

    .line 181
    const-string v0, "wifi"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 182
    invoke-virtual {v0, v2}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    move v0, v1

    .line 185
    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method public static disconnectMobileNetWork(Landroid/content/Context;Lcom/samsungosp/billingup/client/util/MobileConnect$NetWorkState;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 191
    sget-object v0, Lcom/samsungosp/billingup/client/util/MobileConnect;->mNetWorkState:Lcom/samsungosp/billingup/client/util/MobileConnect$NetWorkState;

    sget-object v2, Lcom/samsungosp/billingup/client/util/MobileConnect$NetWorkState;->DirectBillingAuthUrlCheck:Lcom/samsungosp/billingup/client/util/MobileConnect$NetWorkState;

    if-ne v0, v2, :cond_0

    .line 193
    const-string v0, "wifi"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 194
    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 195
    sput-object p1, Lcom/samsungosp/billingup/client/util/MobileConnect;->mNetWorkState:Lcom/samsungosp/billingup/client/util/MobileConnect$NetWorkState;

    move v0, v1

    .line 198
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static extractAddressFromUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 122
    const-string v0, "://"

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 126
    if-lez v0, :cond_0

    .line 127
    add-int/lit8 v0, v0, 0x3

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 128
    :cond_0
    const/16 v0, 0x3a

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 136
    if-ltz v0, :cond_1

    .line 137
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 142
    :cond_1
    const/16 v0, 0x2f

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 143
    if-ltz v0, :cond_2

    .line 144
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 149
    :cond_2
    const/16 v0, 0x3f

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 150
    if-ltz v0, :cond_3

    .line 151
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 153
    :cond_3
    return-object p0
.end method

.method public static startForceMobileConnection(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 9

    .prologue
    const/4 v8, -0x1

    const/4 v7, 0x5

    const/4 v2, 0x0

    .line 23
    .line 24
    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 23
    check-cast v0, Landroid/net/ConnectivityManager;

    .line 25
    if-nez v0, :cond_1

    .line 26
    const-string v0, "ConnectivityManager is null, cannot try to force a mobile connection"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    move v0, v2

    .line 94
    :cond_0
    :goto_0
    return v0

    .line 31
    :cond_1
    invoke-virtual {v0, v7}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 32
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v1

    .line 33
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "TYPE_MOBILE_HIPRI network state: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    .line 34
    sget-object v3, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    invoke-virtual {v1, v3}, Landroid/net/NetworkInfo$State;->compareTo(Ljava/lang/Enum;)I

    move-result v3

    if-eqz v3, :cond_2

    sget-object v3, Landroid/net/NetworkInfo$State;->CONNECTING:Landroid/net/NetworkInfo$State;

    invoke-virtual {v1, v3}, Landroid/net/NetworkInfo$State;->compareTo(Ljava/lang/Enum;)I

    move-result v1

    if-nez v1, :cond_3

    .line 35
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 41
    :cond_3
    const-string v1, "enableHIPRI"

    .line 40
    invoke-virtual {v0, v2, v1}, Landroid/net/ConnectivityManager;->startUsingNetworkFeature(ILjava/lang/String;)I

    move-result v1

    .line 42
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "startUsingNetworkFeature for enableHIPRI result: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    .line 48
    if-ne v8, v1, :cond_4

    .line 49
    const-string v0, "Wrong result of startUsingNetworkFeature, maybe problems"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->e(Ljava/lang/String;)V

    move v0, v2

    .line 50
    goto :goto_0

    .line 52
    :cond_4
    if-nez v1, :cond_5

    .line 53
    const-string v1, "No need to perform additional network settings"

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    :cond_5
    move v1, v2

    .line 58
    :goto_1
    const/16 v3, 0x1e

    if-lt v1, v3, :cond_7

    .line 75
    :cond_6
    :goto_2
    invoke-static {p1}, Lcom/samsungosp/billingup/client/util/MobileConnect;->extractAddressFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 76
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Source address: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    .line 77
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Destination host address to route: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    .line 78
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 82
    :goto_3
    invoke-static {p1}, Lcom/samsungosp/billingup/client/util/MobileConnect;->a(Ljava/lang/String;)I

    move-result v1

    .line 83
    if-ne v8, v1, :cond_8

    .line 84
    const-string v0, "Wrong host address transformation, result was -1"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->e(Ljava/lang/String;)V

    move v0, v2

    .line 85
    goto/16 :goto_0

    .line 60
    :cond_7
    const/4 v3, 0x5

    .line 59
    :try_start_0
    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v3

    .line 60
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v3

    .line 61
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "startForceMobileConnection counter : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", checkState : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 62
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 61
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 62
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "State.CONNECTED : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v6, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    invoke-virtual {v6}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 61
    invoke-static {v4, v5}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    sget-object v4, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    invoke-virtual {v3, v4}, Landroid/net/NetworkInfo$State;->compareTo(Ljava/lang/Enum;)I

    move-result v3

    if-eqz v3, :cond_6

    .line 65
    const-wide/16 v3, 0x3e8

    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    .line 88
    :cond_8
    invoke-virtual {v0, v7, v1}, Landroid/net/ConnectivityManager;->requestRouteToHost(II)Z

    move-result v0

    .line 90
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "requestRouteToHost result: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    .line 91
    if-nez v0, :cond_0

    .line 92
    const-string v1, "Wrong requestRouteToHost result: expected true, but was false"

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto/16 :goto_2

    :cond_9
    move-object p1, v1

    goto :goto_3
.end method

.method public static stopForceMobileConnection(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 98
    .line 99
    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 98
    check-cast v0, Landroid/net/ConnectivityManager;

    .line 100
    if-nez v0, :cond_0

    .line 101
    const-string v0, "ConnectivityManager is null, cannot try to force a mobile connection"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    move v0, v1

    .line 118
    :goto_0
    return v0

    .line 106
    :cond_0
    const-string v2, "enableHIPRI"

    .line 105
    invoke-virtual {v0, v1, v2}, Landroid/net/ConnectivityManager;->stopUsingNetworkFeature(ILjava/lang/String;)I

    move-result v0

    .line 107
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "stopUsingNetworkFeature for enableHIPRI result: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    .line 109
    const/4 v2, -0x1

    if-ne v2, v0, :cond_1

    .line 110
    const-string v0, "Wrong result of startUsingNetworkFeature, maybe problems"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->e(Ljava/lang/String;)V

    move v0, v1

    .line 111
    goto :goto_0

    .line 113
    :cond_1
    if-nez v0, :cond_2

    .line 114
    const-string v0, "No need to perform additional network settings"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    .line 115
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 118
    goto :goto_0
.end method
