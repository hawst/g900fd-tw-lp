.class public Lcom/samsungosp/billingup/client/util/JSONUtil;
.super Ljava/lang/Object;
.source "ProGuard"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static toJSONString(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    :try_start_0
    invoke-static {p0}, Lcom/samsungosp/billingup/client/util/JSONUtil;->valueOf(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    .line 17
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 21
    :goto_0
    return-object v0

    .line 18
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 21
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/Object;)Lorg/json/JSONObject;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 28
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 30
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 32
    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v5

    array-length v6, v5

    move v3, v2

    .line 34
    :goto_0
    if-lt v3, v6, :cond_0

    .line 73
    return-object v4

    .line 32
    :cond_0
    aget-object v0, v5, v3

    .line 35
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 37
    invoke-virtual {v0}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v7

    .line 38
    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 40
    const-string v8, "CREATOR"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 41
    if-eqz v1, :cond_1

    .line 46
    instance-of v8, v1, Ljava/lang/String;

    if-eqz v8, :cond_2

    .line 47
    invoke-virtual {v4, v7, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 34
    :cond_1
    :goto_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 48
    :cond_2
    instance-of v8, v1, [Ljava/lang/Object;

    if-eqz v8, :cond_6

    .line 49
    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 51
    if-eqz v0, :cond_1

    .line 52
    new-instance v8, Lorg/json/JSONArray;

    invoke-direct {v8}, Lorg/json/JSONArray;-><init>()V

    move v1, v2

    .line 54
    :goto_2
    array-length v9, v0

    if-lt v1, v9, :cond_3

    .line 64
    invoke-virtual {v4, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_1

    .line 55
    :cond_3
    aget-object v9, v0, v1

    instance-of v9, v9, Ljava/lang/String;

    if-eqz v9, :cond_5

    .line 56
    aget-object v9, v0, v1

    invoke-virtual {v8, v9}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 54
    :cond_4
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 58
    :cond_5
    aget-object v9, v0, v1

    if-eqz v9, :cond_4

    .line 59
    aget-object v9, v0, v1

    invoke-static {v9}, Lcom/samsungosp/billingup/client/util/JSONUtil;->valueOf(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v9

    invoke-virtual {v8, v9}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_3

    .line 68
    :cond_6
    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/JSONUtil;->valueOf(Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v4, v7, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_1
.end method
