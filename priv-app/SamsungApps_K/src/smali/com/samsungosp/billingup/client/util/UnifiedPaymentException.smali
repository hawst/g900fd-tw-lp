.class public Lcom/samsungosp/billingup/client/util/UnifiedPaymentException;
.super Ljava/lang/Exception;
.source "ProGuard"


# static fields
.field public static final CHECKOUT_EXCEPTION:Ljava/lang/String; = "UnifiedPayment checkout parameter exception"

.field public static final CREDITCARD_EXCEPTION:Ljava/lang/String; = "UnifiedPayment creditcard parameter exception"

.field public static final GIFTCARD_EXCEPTION:Ljava/lang/String; = "UnifiedPayment giftcard parameter exception"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    .line 16
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 20
    return-void
.end method
