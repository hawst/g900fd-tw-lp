.class public Lcom/samsungosp/billingup/client/requestparam/SignatureInfo;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public baseString:Ljava/lang/String;

.field public signature:Ljava/lang/String;

.field public timeStamp:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    new-instance v0, Lcom/samsungosp/billingup/client/requestparam/SignatureInfo$1;

    invoke-direct {v0}, Lcom/samsungosp/billingup/client/requestparam/SignatureInfo$1;-><init>()V

    sput-object v0, Lcom/samsungosp/billingup/client/requestparam/SignatureInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 45
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static readFromParcel(Landroid/os/Parcel;)Lcom/samsungosp/billingup/client/requestparam/SignatureInfo;
    .locals 2

    .prologue
    .line 28
    new-instance v0, Lcom/samsungosp/billingup/client/requestparam/SignatureInfo;

    invoke-direct {v0}, Lcom/samsungosp/billingup/client/requestparam/SignatureInfo;-><init>()V

    .line 30
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/SignatureInfo;->timeStamp:Ljava/lang/String;

    .line 31
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/SignatureInfo;->baseString:Ljava/lang/String;

    .line 32
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/SignatureInfo;->signature:Ljava/lang/String;

    .line 34
    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/SignatureInfo;->timeStamp:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 23
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/SignatureInfo;->baseString:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 24
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/SignatureInfo;->signature:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 25
    return-void
.end method
