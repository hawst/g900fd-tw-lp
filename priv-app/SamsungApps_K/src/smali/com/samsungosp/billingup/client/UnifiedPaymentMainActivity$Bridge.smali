.class Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;


# direct methods
.method private constructor <init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)V
    .locals 0

    .prologue
    .line 1055
    iput-object p1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;B)V
    .locals 0

    .prologue
    .line 1055
    invoke-direct {p0, p1}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)V

    return-void
.end method

.method static synthetic a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;
    .locals 1

    .prologue
    .line 1055
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    return-object v0
.end method


# virtual methods
.method public alert(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1134
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    new-instance v1, Lcom/samsungosp/billingup/client/ay;

    invoke-direct {v1, p0, p1, p2}, Lcom/samsungosp/billingup/client/ay;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1143
    return-void
.end method

.method public blockBackKey()V
    .locals 2

    .prologue
    .line 2119
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->c(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;Z)V

    .line 2120
    return-void
.end method

.method public cacPay(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1410
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    new-instance v1, Lcom/samsungosp/billingup/client/w;

    invoke-direct {v1, p0, p4, p2, p5}, Lcom/samsungosp/billingup/client/w;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1417
    return-void
.end method

.method public cancelCreditCard()V
    .locals 2

    .prologue
    .line 1230
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    new-instance v1, Lcom/samsungosp/billingup/client/bb;

    invoke-direct {v1, p0}, Lcom/samsungosp/billingup/client/bb;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1238
    return-void
.end method

.method public cancelPayment()V
    .locals 2

    .prologue
    .line 2071
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    new-instance v1, Lcom/samsungosp/billingup/client/al;

    invoke-direct {v1, p0}, Lcom/samsungosp/billingup/client/al;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2079
    return-void
.end method

.method public checkSession()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2090
    const-string v0, "N"

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->h(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    move-result-object v1

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->paymentInfo:Lcom/samsungosp/billingup/client/requestparam/PaymentInfo;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/PaymentInfo;->confirmPasswordYN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "KOR"

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->h(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    move-result-object v1

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;->country:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2092
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 2091
    invoke-static {v0}, Lcom/samsungosp/billingup/client/AccountManager;->getInstance(Landroid/content/Context;)Lcom/samsungosp/billingup/client/AccountManager;

    move-result-object v0

    .line 2093
    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->l(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->m(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v3}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->n(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsungosp/billingup/client/AccountManager;->isConfirmed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 2094
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "checkSession(), isValid ? : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 2095
    if-eqz v0, :cond_0

    const-string v0, "true"

    .line 2097
    :goto_0
    return-object v0

    .line 2095
    :cond_0
    const-string v0, "false"

    goto :goto_0

    .line 2097
    :cond_1
    const-string v0, "true"

    goto :goto_0
.end method

.method public gcbPay(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1554
    new-instance v0, Lcom/samsungosp/billingup/client/z;

    invoke-direct {v0, p0, p1}, Lcom/samsungosp/billingup/client/z;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;)V

    .line 1575
    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 1576
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 1578
    return-void
.end method

.method public gcbPayStart(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1425
    new-instance v0, Lcom/samsungosp/billingup/client/x;

    invoke-direct {v0, p0, p1}, Lcom/samsungosp/billingup/client/x;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;)V

    .line 1541
    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 1544
    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1549
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public getAndroidVersion()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2112
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 2113
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[Platform Version]"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 2114
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDateFormat()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2137
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "date_format"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2138
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2139
    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-virtual {v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/text/format/DateFormat;->getMediumDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    .line 2143
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Date format : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 2145
    return-object v0

    .line 2141
    :cond_0
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-direct {v1, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getRecentPayment(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2582
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2583
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[RecentPayment] data : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "recent_payment_key"

    const-string v3, ""

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 2584
    const-string v1, "recent_payment_key"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUnifiedPaymentData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2084
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->f(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2104
    invoke-static {}, Lcom/samsungosp/billingup/client/UnifiedPayment;->getVersion()Ljava/lang/String;

    move-result-object v0

    .line 2106
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getVersion() : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 2107
    return-object v0
.end method

.method public gppPay(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1948
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    new-instance v1, Lcom/samsungosp/billingup/client/ah;

    invoke-direct {v1, p0, p1, p2}, Lcom/samsungosp/billingup/client/ah;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1990
    return-void
.end method

.method public hideLoadingAnimation()V
    .locals 2

    .prologue
    .line 2066
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2067
    return-void
.end method

.method public iccRegisterCard(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2451
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    new-instance v1, Lcom/samsungosp/billingup/client/at;

    invoke-direct {v1, p0, p1}, Lcom/samsungosp/billingup/client/at;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2483
    return-void
.end method

.method public inicisMRCpay(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 1764
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v7, v0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    new-instance v0, Lcom/samsungosp/billingup/client/ad;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/samsungosp/billingup/client/ad;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1817
    return-void
.end method

.method public inicisMRCpay(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 1825
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v8, v0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    new-instance v0, Lcom/samsungosp/billingup/client/ae;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/samsungosp/billingup/client/ae;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1880
    return-void
.end method

.method public installAlipay()V
    .locals 2

    .prologue
    .line 2419
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    new-instance v1, Lcom/samsungosp/billingup/client/aq;

    invoke-direct {v1, p0}, Lcom/samsungosp/billingup/client/aq;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2427
    return-void
.end method

.method public installSwallet()V
    .locals 2

    .prologue
    .line 2433
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    new-instance v1, Lcom/samsungosp/billingup/client/ar;

    invoke-direct {v1, p0}, Lcom/samsungosp/billingup/client/ar;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2445
    return-void
.end method

.method public isInstalledApplication()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 2268
    const-string v1, "in isInstalledApplication()"

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 2269
    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-virtual {v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 2270
    :try_start_0
    const-string v2, "com.sec.android.wallet"

    const/16 v3, 0x80

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2288
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isInstalledApplication() return availableWallet : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 2289
    return v0

    .line 2276
    :catch_0
    move-exception v1

    const-string v1, "isInstalledApplication() exception"

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 2277
    new-instance v1, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;

    iget-object v2, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-direct {v1, v2}, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;-><init>(Landroid/content/Context;)V

    .line 2278
    const-string v2, "isInstalledApplication() new CheckWalletAvaliable"

    invoke-static {v2}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 2279
    invoke-virtual {v1}, Lcom/sec/android/wallet/confirm/http/CheckWalletAvaliable;->canLinkWallet()I

    move-result v1

    .line 2280
    if-ne v1, v0, :cond_0

    .line 2281
    const-string v1, "isInstalledApplication() wallet enable"

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 2284
    :cond_0
    const-string v0, "isInstalledApplication() wallet disable"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 2285
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMobileDataEnabled()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2295
    const-string v0, "isMobileDataEnabled()"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 2296
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "connectivity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 2299
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 2300
    const-string v3, "getMobileDataEnabled"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 2301
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 2302
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2305
    :goto_0
    return v0

    .line 2303
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move v0, v1

    .line 2305
    goto :goto_0
.end method

.method public kcbSendSMS(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2534
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "kcbSendSMS smsMO : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", smsMessage : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", requestID : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mPrevRequestID : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->q(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    .line 2535
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->q(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->q(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2536
    const-string v0, "kcbSendSMS duplicate sms sending"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    .line 2537
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    new-instance v1, Lcom/samsungosp/billingup/client/av;

    invoke-direct {v1, p0}, Lcom/samsungosp/billingup/client/av;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)V

    invoke-virtual {v0, v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2572
    :goto_0
    return-void

    .line 2545
    :cond_0
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v0, p3}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->e(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;Ljava/lang/String;)V

    .line 2548
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    new-instance v1, Lcom/samsungosp/billingup/client/aw;

    invoke-direct {v1, p0, p1, p2}, Lcom/samsungosp/billingup/client/aw;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public kccPay(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 1257
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v8, v0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    new-instance v0, Lcom/samsungosp/billingup/client/bd;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p6

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/samsungosp/billingup/client/bd;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1315
    return-void
.end method

.method public kpiMRCpay(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 12

    .prologue
    .line 1888
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v11, v0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    new-instance v0, Lcom/samsungosp/billingup/client/af;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v10}, Lcom/samsungosp/billingup/client/af;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v11, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1943
    return-void
.end method

.method public kpiPay(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 1377
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v8, v0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    new-instance v0, Lcom/samsungosp/billingup/client/v;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/samsungosp/billingup/client/v;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1403
    return-void
.end method

.method public kupPay(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 1322
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v8, v0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    new-instance v0, Lcom/samsungosp/billingup/client/u;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p6

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/samsungosp/billingup/client/u;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1371
    return-void
.end method

.method public loadComplete()V
    .locals 2

    .prologue
    .line 2129
    const-string v0, "call loadComplete()"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 2130
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2131
    return-void
.end method

.method public loadPage(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2489
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    new-instance v1, Lcom/samsungosp/billingup/client/au;

    invoke-direct {v1, p0, p1}, Lcom/samsungosp/billingup/client/au;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2529
    return-void
.end method

.method public openOnNewActivity(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1147
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    new-instance v1, Lcom/samsungosp/billingup/client/az;

    invoke-direct {v1, p0, p1}, Lcom/samsungosp/billingup/client/az;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1185
    return-void
.end method

.method public openOnNewActivity(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1189
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    new-instance v1, Lcom/samsungosp/billingup/client/ba;

    invoke-direct {v1, p0, p1, p2}, Lcom/samsungosp/billingup/client/ba;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1203
    return-void
.end method

.method public psmsPay(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1995
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    new-instance v1, Lcom/samsungosp/billingup/client/ai;

    invoke-direct {v1, p0, p1, p2}, Lcom/samsungosp/billingup/client/ai;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2017
    return-void
.end method

.method public receiveCreditCardResult(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1242
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    new-instance v1, Lcom/samsungosp/billingup/client/bc;

    invoke-direct {v1, p0, p1}, Lcom/samsungosp/billingup/client/bc;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1250
    return-void
.end method

.method public receivePaymentErrorResult(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1114
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    new-instance v1, Lcom/samsungosp/billingup/client/as;

    invoke-direct {v1, p0, p1, p2}, Lcom/samsungosp/billingup/client/as;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1130
    return-void
.end method

.method public receivePaymentPendingResult()V
    .locals 2

    .prologue
    .line 1098
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    new-instance v1, Lcom/samsungosp/billingup/client/ag;

    invoke-direct {v1, p0}, Lcom/samsungosp/billingup/client/ag;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1109
    return-void
.end method

.method public receivePaymentResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1061
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    new-instance v1, Lcom/samsungosp/billingup/client/t;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/samsungosp/billingup/client/t;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1093
    return-void
.end method

.method public registerCreditCard()V
    .locals 3

    .prologue
    .line 1207
    const-string v0, "UnifiedPaymentMainActivity registerCreditCard()"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    .line 1208
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    const-class v2, Lcom/samsungosp/billingup/client/CreditCardActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1209
    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1210
    new-instance v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    invoke-direct {v1}, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;-><init>()V

    .line 1211
    iget-object v2, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->h(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    move-result-object v2

    iget-object v2, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->appServiceID:Ljava/lang/String;

    iput-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->appServiceID:Ljava/lang/String;

    .line 1212
    iget-object v2, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->h(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    move-result-object v2

    iget-object v2, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;

    iget-object v2, v2, Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;->country:Ljava/lang/String;

    iput-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->country:Ljava/lang/String;

    .line 1213
    iget-object v2, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->h(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    move-result-object v2

    iget-object v2, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    iput-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    .line 1214
    iget-object v2, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->h(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    move-result-object v2

    iget-object v2, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    iget-object v2, v2, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->language:Ljava/lang/String;

    iput-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->language:Ljava/lang/String;

    .line 1215
    iget-object v2, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->h(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    move-result-object v2

    iget-object v2, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->storeRequestID:Ljava/lang/String;

    iput-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->storeRequestID:Ljava/lang/String;

    .line 1216
    iget-object v2, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->h(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    move-result-object v2

    iget-object v2, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->timeOffset:Ljava/lang/String;

    iput-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->timeOffset:Ljava/lang/String;

    .line 1217
    iget-object v2, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->h(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    move-result-object v2

    iget-object v2, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->billingServerInfo:Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;

    iget-object v2, v2, Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;->upServerURL:Ljava/lang/String;

    iput-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->upServerURL:Ljava/lang/String;

    .line 1218
    iget-object v2, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->h(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    move-result-object v2

    iget-object v2, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    iput-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    .line 1219
    iget-object v2, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->h(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    move-result-object v2

    iget-object v2, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->uxVersion:Ljava/lang/String;

    iput-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->uxVersion:Ljava/lang/String;

    .line 1220
    const-string v2, "CREDIT_CARD_REQUEST"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1225
    :goto_0
    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    const/16 v2, 0x65

    invoke-virtual {v1, v0, v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1226
    return-void

    .line 1222
    :cond_0
    const-string v1, "CREDIT_CARD_REQUEST"

    iget-object v2, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->i(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public releaseBackKey()V
    .locals 2

    .prologue
    .line 2124
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->c(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;Z)V

    .line 2125
    return-void
.end method

.method public setMobileDataEnabled()V
    .locals 6

    .prologue
    .line 2312
    const-string v0, "setMobileDataEnabled()"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 2313
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 2315
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 2316
    const-string v2, "mService"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 2317
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 2318
    invoke-virtual {v1, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 2319
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 2320
    const-string v2, "setMobileDataEnabled"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 2321
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 2323
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2328
    :goto_0
    return-void

    .line 2326
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public setRecentPayment(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2589
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[RecentPayment] userId : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " payment : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 2590
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2591
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 2593
    const-string v1, "recent_payment_key"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2594
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2595
    return-void
.end method

.method public showToast(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2576
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2577
    return-void
.end method

.method public slgPay(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2022
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    new-instance v1, Lcom/samsungosp/billingup/client/ak;

    invoke-direct {v1, p0, p1, p2}, Lcom/samsungosp/billingup/client/ak;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2060
    return-void
.end method

.method public startQiwi(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2376
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    new-instance v1, Lcom/samsungosp/billingup/client/ap;

    invoke-direct {v1, p0, p1}, Lcom/samsungosp/billingup/client/ap;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2413
    return-void
.end method

.method public startSINA(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2334
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    new-instance v1, Lcom/samsungosp/billingup/client/ao;

    invoke-direct {v1, p0, p1}, Lcom/samsungosp/billingup/client/ao;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2371
    return-void
.end method

.method public startWallet(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 15

    .prologue
    .line 2153
    iget-object v2, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-virtual {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 2155
    :try_start_0
    const-string v3, "com.sec.android.wallet"

    const/16 v4, 0x80

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    .line 2156
    const-string v2, "[S Wallet] Wallet was installed"

    invoke-static {v2}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 2159
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 2160
    iget-object v3, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "com.sec.android.wallet.PAYMENT_COMPLETED_"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->d(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;Ljava/lang/String;)V

    .line 2161
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SWALLET_BROADCAST_CODE_APPSERVICEID : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v4}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->o(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 2162
    iget-object v3, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v3}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->o(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2163
    iget-object v3, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v4, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v4}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->p(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Landroid/content/BroadcastReceiver;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2165
    const-string v6, "wallet://com.sec.android.wallet?"

    .line 2167
    const-string v2, "mobile.instore"

    .line 2168
    const-string v3, "payment"

    .line 2169
    iget-object v4, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v4}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->h(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    move-result-object v4

    iget-object v4, v4, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->productInfo:Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

    iget-object v4, v4, Lcom/samsungosp/billingup/client/requestparam/ProductInfo;->detailProductInfos:[Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;

    array-length v4, v4

    .line 2178
    iget-object v5, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v5}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->h(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    move-result-object v5

    iget-object v5, v5, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->productInfo:Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

    iget-object v5, v5, Lcom/samsungosp/billingup/client/requestparam/ProductInfo;->detailProductInfos:[Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;

    const/4 v7, 0x0

    aget-object v5, v5, v7

    iget-object v5, v5, Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;->productName:Ljava/lang/String;

    .line 2179
    invoke-static {v5, v4}, Lcom/samsungosp/billingup/client/util/CommonUtil;->cutInStringByBytesSWallet(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    .line 2180
    const-string v13, ""

    .line 2181
    iget-object v4, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v4}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->h(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    move-result-object v4

    iget-object v4, v4, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->productInfo:Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

    iget-object v4, v4, Lcom/samsungosp/billingup/client/requestparam/ProductInfo;->detailProductInfos:[Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;

    const/4 v7, 0x0

    aget-object v4, v4, v7

    iget-object v4, v4, Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;->validityPeriod:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 2182
    iget-object v4, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v4}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->h(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    move-result-object v4

    iget-object v4, v4, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->productInfo:Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

    iget-object v4, v4, Lcom/samsungosp/billingup/client/requestparam/ProductInfo;->detailProductInfos:[Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;

    const/4 v7, 0x0

    aget-object v4, v4, v7

    iget-object v13, v4, Lcom/samsungosp/billingup/client/requestparam/DetailProductInfos;->validityPeriod:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2186
    :cond_0
    :try_start_1
    move-object/from16 v0, p2

    move-object/from16 v1, p10

    invoke-static {v0, v1}, Lcom/samsungosp/billingup/client/wallet/SWGAESEncryption;->encryptionAES(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 2187
    move-object/from16 v0, p6

    move-object/from16 v1, p10

    invoke-static {v0, v1}, Lcom/samsungosp/billingup/client/wallet/SWGAESEncryption;->encryptionAES(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p6

    .line 2188
    move-object/from16 v0, p10

    invoke-static {v5, v0}, Lcom/samsungosp/billingup/client/wallet/SWGAESEncryption;->encryptionAES(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2189
    move-object/from16 v0, p10

    invoke-static {v13, v0}, Lcom/samsungosp/billingup/client/wallet/SWGAESEncryption;->encryptionAES(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v13

    move-object v12, v5

    move-object/from16 v9, p6

    move-object/from16 v5, p2

    .line 2195
    :goto_0
    :try_start_2
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v14, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v4, p1

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    invoke-static/range {v2 .. v13}, Lcom/samsungosp/billingup/client/util/CommonUtil;->getRawDataSWallet(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2196
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[S Wallet] Wallet mPostUrl : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 2198
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2199
    const-string v4, "android.intent.category.DEFAULT"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 2200
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2201
    iget-object v2, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-virtual {v2, v3}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->startActivity(Landroid/content/Intent;)V

    .line 2217
    :goto_1
    return-void

    .line 2192
    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    move-object v12, v5

    move-object/from16 v9, p6

    move-object/from16 v5, p2

    goto :goto_0

    .line 2207
    :catch_1
    move-exception v2

    const-string v2, "[S Wallet] Wallet was not installed"

    invoke-static {v2}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 2208
    iget-object v2, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    new-instance v3, Lcom/samsungosp/billingup/client/am;

    invoke-direct {v3, p0}, Lcom/samsungosp/billingup/client/am;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)V

    invoke-virtual {v2, v3}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2214
    const-string v2, "[S Wallet] reInit()"

    invoke-static {v2}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public telefonicaPay(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1732
    new-instance v0, Lcom/samsungosp/billingup/client/ac;

    invoke-direct {v0, p0, p1}, Lcom/samsungosp/billingup/client/ac;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;)V

    .line 1753
    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 1754
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 1756
    return-void
.end method

.method public telefonicaPayStart(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1584
    new-instance v0, Lcom/samsungosp/billingup/client/aa;

    invoke-direct {v0, p0, p1}, Lcom/samsungosp/billingup/client/aa;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;)V

    .line 1719
    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 1722
    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1727
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public webMoneyPay(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2222
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v0, v0, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    new-instance v1, Lcom/samsungosp/billingup/client/an;

    invoke-direct {v1, p0, p1}, Lcom/samsungosp/billingup/client/an;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2263
    return-void
.end method
