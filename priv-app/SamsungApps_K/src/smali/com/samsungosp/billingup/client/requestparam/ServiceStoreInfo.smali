.class public Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public billingInterfaceURL:Lcom/samsungosp/billingup/client/requestparam/BillingInterfaceURL;

.field public country:Ljava/lang/String;

.field public serviceURL:Ljava/lang/String;

.field public telNoForCS:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    new-instance v0, Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo$1;

    invoke-direct {v0}, Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo$1;-><init>()V

    sput-object v0, Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 49
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static readFromParcel(Landroid/os/Parcel;)Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;
    .locals 2

    .prologue
    .line 31
    new-instance v1, Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;

    invoke-direct {v1}, Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;-><init>()V

    .line 33
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;->country:Ljava/lang/String;

    .line 34
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;->telNoForCS:Ljava/lang/String;

    .line 35
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;->serviceURL:Ljava/lang/String;

    .line 36
    const-class v0, Lcom/samsungosp/billingup/client/requestparam/BillingInterfaceURL;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/samsungosp/billingup/client/requestparam/BillingInterfaceURL;

    iput-object v0, v1, Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/samsungosp/billingup/client/requestparam/BillingInterfaceURL;

    .line 38
    return-object v1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;->country:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 25
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;->telNoForCS:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 26
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;->serviceURL:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 27
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;->billingInterfaceURL:Lcom/samsungosp/billingup/client/requestparam/BillingInterfaceURL;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 28
    return-void
.end method
