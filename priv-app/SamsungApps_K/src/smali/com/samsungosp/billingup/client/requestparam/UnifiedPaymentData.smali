.class public Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public appServiceID:Ljava/lang/String;

.field public billingServerInfo:Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;

.field public deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

.field public libraryVersion:Ljava/lang/String;

.field public paymentInfo:Lcom/samsungosp/billingup/client/requestparam/PaymentInfo;

.field private platformCode:Ljava/lang/String;

.field public productInfo:Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

.field public serviceStoreInfo:Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;

.field public signatureInfo:Lcom/samsungosp/billingup/client/requestparam/SignatureInfo;

.field public storeRequestID:Ljava/lang/String;

.field public subscriptionInfo:Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo;

.field public timeOffset:Ljava/lang/String;

.field public userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

.field public uxVersion:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 82
    new-instance v0, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData$1;

    invoke-direct {v0}, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData$1;-><init>()V

    sput-object v0, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 90
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const-string v0, "A"

    iput-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->platformCode:Ljava/lang/String;

    .line 26
    return-void
.end method

.method public static readFromParcel(Landroid/os/Parcel;)Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;
    .locals 2

    .prologue
    .line 54
    new-instance v1, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    invoke-direct {v1}, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;-><init>()V

    .line 56
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->appServiceID:Ljava/lang/String;

    .line 57
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->storeRequestID:Ljava/lang/String;

    .line 58
    const-class v0, Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

    .line 59
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 58
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

    iput-object v0, v1, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->productInfo:Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

    .line 60
    const-class v0, Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    .line 61
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 60
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    iput-object v0, v1, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    .line 63
    const-class v0, Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;

    .line 62
    iput-object v0, v1, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->billingServerInfo:Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;

    .line 64
    const-class v0, Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;

    .line 65
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 64
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;

    iput-object v0, v1, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;

    .line 66
    const-class v0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    .line 67
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 66
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    iput-object v0, v1, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    .line 68
    const-class v0, Lcom/samsungosp/billingup/client/requestparam/PaymentInfo;

    .line 69
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 68
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/samsungosp/billingup/client/requestparam/PaymentInfo;

    iput-object v0, v1, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->paymentInfo:Lcom/samsungosp/billingup/client/requestparam/PaymentInfo;

    .line 70
    const-class v0, Lcom/samsungosp/billingup/client/requestparam/SignatureInfo;

    .line 71
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 70
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/samsungosp/billingup/client/requestparam/SignatureInfo;

    iput-object v0, v1, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->signatureInfo:Lcom/samsungosp/billingup/client/requestparam/SignatureInfo;

    .line 72
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->platformCode:Ljava/lang/String;

    .line 73
    const-class v0, Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo;

    .line 74
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 73
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo;

    iput-object v0, v1, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->subscriptionInfo:Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo;

    .line 75
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->timeOffset:Ljava/lang/String;

    .line 76
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->uxVersion:Ljava/lang/String;

    .line 77
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->libraryVersion:Ljava/lang/String;

    .line 79
    return-object v1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 37
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->appServiceID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 38
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->storeRequestID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->productInfo:Lcom/samsungosp/billingup/client/requestparam/ProductInfo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 40
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 41
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->billingServerInfo:Lcom/samsungosp/billingup/client/requestparam/BillingServerInfo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 42
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 43
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 44
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->paymentInfo:Lcom/samsungosp/billingup/client/requestparam/PaymentInfo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 45
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->signatureInfo:Lcom/samsungosp/billingup/client/requestparam/SignatureInfo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 46
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->platformCode:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 47
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->subscriptionInfo:Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 48
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->timeOffset:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 49
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->uxVersion:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->libraryVersion:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 51
    return-void
.end method
