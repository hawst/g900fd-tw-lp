.class final Lcom/samsungosp/billingup/client/ba;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

.field private final synthetic b:Ljava/lang/String;

.field private final synthetic c:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsungosp/billingup/client/ba;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    iput-object p2, p0, Lcom/samsungosp/billingup/client/ba;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/samsungosp/billingup/client/ba;->c:Ljava/lang/String;

    .line 1189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 1192
    iget-object v0, p0, Lcom/samsungosp/billingup/client/ba;->b:Ljava/lang/String;

    const-string v1, "CREDIT_CARD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1193
    new-instance v0, Landroid/content/Intent;

    .line 1194
    iget-object v1, p0, Lcom/samsungosp/billingup/client/ba;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v1

    .line 1195
    const-class v2, Lcom/samsungosp/billingup/client/CreditCardActivity;

    .line 1193
    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1196
    const-string v1, "DISCLAIMER_BODY"

    iget-object v2, p0, Lcom/samsungosp/billingup/client/ba;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1197
    iget-object v1, p0, Lcom/samsungosp/billingup/client/ba;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->startActivity(Landroid/content/Intent;)V

    .line 1201
    :goto_0
    return-void

    .line 1198
    :cond_0
    iget-object v0, p0, Lcom/samsungosp/billingup/client/ba;->b:Ljava/lang/String;

    const-string v1, "GIFT_CARD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    goto :goto_0
.end method
