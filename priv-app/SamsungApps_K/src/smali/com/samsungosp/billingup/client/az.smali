.class final Lcom/samsungosp/billingup/client/az;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

.field private final synthetic b:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsungosp/billingup/client/az;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    iput-object p2, p0, Lcom/samsungosp/billingup/client/az;->b:Ljava/lang/String;

    .line 1147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 1151
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "type : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsungosp/billingup/client/az;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 1153
    iget-object v0, p0, Lcom/samsungosp/billingup/client/az;->b:Ljava/lang/String;

    const-string v1, "CREDIT_CARD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1154
    new-instance v0, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    invoke-direct {v0}, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;-><init>()V

    .line 1156
    iget-object v1, p0, Lcom/samsungosp/billingup/client/az;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->h(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    move-result-object v1

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->appServiceID:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->appServiceID:Ljava/lang/String;

    .line 1157
    iget-object v1, p0, Lcom/samsungosp/billingup/client/az;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->h(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    move-result-object v1

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->serviceStoreInfo:Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/ServiceStoreInfo;->country:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->country:Ljava/lang/String;

    .line 1158
    iget-object v1, p0, Lcom/samsungosp/billingup/client/az;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->h(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    move-result-object v1

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    iget-object v1, v1, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;->language:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->language:Ljava/lang/String;

    .line 1159
    new-instance v1, Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    invoke-direct {v1}, Lcom/samsungosp/billingup/client/requestparam/UserInfo;-><init>()V

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    .line 1160
    iget-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    iget-object v2, p0, Lcom/samsungosp/billingup/client/az;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->h(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    move-result-object v2

    iget-object v2, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    iget-object v2, v2, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->accessToken:Ljava/lang/String;

    iput-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->accessToken:Ljava/lang/String;

    .line 1161
    iget-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    iget-object v2, p0, Lcom/samsungosp/billingup/client/az;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->h(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    move-result-object v2

    iget-object v2, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    iget-object v2, v2, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->userEmail:Ljava/lang/String;

    iput-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->userEmail:Ljava/lang/String;

    .line 1162
    iget-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    iget-object v2, p0, Lcom/samsungosp/billingup/client/az;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->h(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    move-result-object v2

    iget-object v2, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    iget-object v2, v2, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->userID:Ljava/lang/String;

    iput-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->userID:Ljava/lang/String;

    .line 1163
    iget-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    iget-object v2, p0, Lcom/samsungosp/billingup/client/az;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->h(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;

    move-result-object v2

    iget-object v2, v2, Lcom/samsungosp/billingup/client/requestparam/UnifiedPaymentData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    iget-object v2, v2, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->userName:Ljava/lang/String;

    iput-object v2, v1, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->userName:Ljava/lang/String;

    .line 1168
    :try_start_0
    iget-object v1, p0, Lcom/samsungosp/billingup/client/az;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v1

    .line 1167
    invoke-static {v0, v1}, Lcom/samsungosp/billingup/client/UnifiedPayment;->makeCreditCardIntent(Lcom/samsungosp/billingup/client/requestparam/CreditCardData;Landroid/content/Context;)Landroid/content/Intent;
    :try_end_0
    .catch Lcom/samsungosp/billingup/client/util/UnifiedPaymentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1175
    if-eqz v0, :cond_0

    .line 1176
    iget-object v1, p0, Lcom/samsungosp/billingup/client/az;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v1

    .line 1177
    const/16 v2, 0x65

    .line 1176
    invoke-virtual {v1, v0, v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1183
    :cond_0
    :goto_0
    return-void

    .line 1171
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/util/UnifiedPaymentException;->printStackTrace()V

    goto :goto_0

    .line 1180
    :cond_1
    iget-object v0, p0, Lcom/samsungosp/billingup/client/az;->b:Ljava/lang/String;

    const-string v1, "GIFT_CARD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    goto :goto_0
.end method
