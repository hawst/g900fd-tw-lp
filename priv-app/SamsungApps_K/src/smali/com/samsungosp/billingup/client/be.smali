.class final Lcom/samsungosp/billingup/client/be;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field final synthetic c:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;


# direct methods
.method public constructor <init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 655
    iput-object p1, p0, Lcom/samsungosp/billingup/client/be;->c:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 656
    iput-object p2, p0, Lcom/samsungosp/billingup/client/be;->a:Ljava/lang/String;

    .line 657
    iput-object p3, p0, Lcom/samsungosp/billingup/client/be;->b:Ljava/lang/String;

    .line 658
    return-void
.end method

.method private a(Ljava/net/HttpURLConnection;)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v9, 0x1

    .line 748
    const-string v0, "HttpURLConnection setRequestMethod"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    .line 753
    const-string v0, "POST"

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 755
    const-string v0, "HttpURLConnection setRequestProperty"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    .line 758
    const-string v0, "Content-Type"

    const-string v1, "application/json"

    invoke-virtual {p1, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 759
    const-string v0, "Accept"

    const-string v1, "application/json"

    invoke-virtual {p1, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 760
    const-string v0, "ClientVersion"

    invoke-static {}, Lcom/samsungosp/billingup/client/UnifiedPayment;->getVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 764
    const-string v0, "HttpURLConnection setUseCaches"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    .line 765
    invoke-virtual {p1, v9}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    .line 766
    invoke-virtual {p1, v9}, Ljava/net/HttpURLConnection;->setDefaultUseCaches(Z)V

    .line 768
    const-string v0, "HttpURLConnection setDoInput"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    .line 769
    invoke-virtual {p1, v9}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 770
    invoke-virtual {p1, v9}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 771
    iget-object v0, p0, Lcom/samsungosp/billingup/client/be;->c:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->f(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "UTF-8"

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    array-length v0, v0

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V

    .line 773
    const-string v0, "HttpURLConnection write"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    .line 774
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    .line 775
    iget-object v1, p0, Lcom/samsungosp/billingup/client/be;->c:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->f(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    .line 776
    const-string v1, "HttpURLConnection OutputStream write jsonData"

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    .line 777
    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 778
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 780
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    .line 781
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Load ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsungosp/billingup/client/be;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 783
    const/16 v1, 0xc8

    if-eq v0, v1, :cond_0

    .line 784
    const/16 v1, 0x130

    if-ne v0, v1, :cond_4

    .line 786
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Load Success : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsungosp/billingup/client/be;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 789
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v5

    .line 790
    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 792
    const-string v0, ">>> START RESPONSE HEADER "

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 794
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 804
    const-string v0, "<<< END RESPONSE HEADER "

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 806
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    .line 807
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 808
    const/16 v2, 0x400

    new-array v2, v2, [B

    .line 811
    :goto_1
    array-length v3, v2

    invoke-virtual {v0, v2, v4, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v3

    const/4 v5, -0x1

    if-ne v3, v5, :cond_3

    .line 814
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    .line 816
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    .line 819
    :goto_2
    return-object v0

    .line 795
    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 796
    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 797
    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    move v3, v4

    .line 798
    :goto_3
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lt v3, v2, :cond_2

    .line 801
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, ":"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 799
    :cond_2
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v2, ";"

    invoke-direct {v8, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 798
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_3

    .line 812
    :cond_3
    invoke-virtual {v1, v2, v4, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_1

    .line 819
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 683
    move v4, v0

    move-object v3, v1

    .line 717
    :goto_0
    const/4 v0, 0x3

    if-lt v4, v0, :cond_3

    :cond_0
    move-object v0, v3

    .line 721
    :cond_1
    :goto_1
    if-eqz v0, :cond_2

    .line 724
    iget-object v1, p0, Lcom/samsungosp/billingup/client/be;->c:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    new-instance v2, Lcom/samsungosp/billingup/client/bf;

    invoke-direct {v2, p0, v0}, Lcom/samsungosp/billingup/client/bf;-><init>(Lcom/samsungosp/billingup/client/be;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 734
    :cond_2
    return-void

    .line 689
    :cond_3
    :try_start_0
    new-instance v2, Ljava/net/URL;

    iget-object v0, p0, Lcom/samsungosp/billingup/client/be;->b:Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 690
    sget-object v0, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    invoke-virtual {v2, v0}, Ljava/net/URL;->openConnection(Ljava/net/Proxy;)Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljavax/net/ssl/SSLHandshakeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 691
    :try_start_1
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "HttpURLConnection openConnection has called url : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    .line 695
    invoke-direct {p0, v0}, Lcom/samsungosp/billingup/client/be;->a(Ljava/net/HttpURLConnection;)Ljava/lang/String;
    :try_end_1
    .catch Ljavax/net/ssl/SSLHandshakeException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v3

    .line 711
    if-eqz v0, :cond_4

    .line 712
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_4
    move-object v0, v3

    .line 716
    :goto_2
    if-nez v0, :cond_1

    .line 717
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-object v3, v0

    goto :goto_0

    .line 700
    :catch_0
    move-exception v0

    :goto_3
    :try_start_2
    invoke-virtual {v0}, Ljavax/net/ssl/SSLHandshakeException;->printStackTrace()V

    .line 701
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 702
    const/16 v2, 0x25

    iput v2, v0, Landroid/os/Message;->what:I

    .line 703
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsungosp/billingup/client/be;->a:Ljava/lang/String;

    aput-object v5, v2, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/samsungosp/billingup/client/be;->b:Ljava/lang/String;

    aput-object v5, v2, v4

    .line 704
    iput-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 705
    iget-object v2, p0, Lcom/samsungosp/billingup/client/be;->c:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    iget-object v2, v2, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 711
    if-eqz v1, :cond_0

    .line 712
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    move-object v0, v3

    .line 706
    goto :goto_1

    .line 707
    :catch_1
    move-exception v0

    move-object v2, v1

    :goto_4
    :try_start_3
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 711
    if-eqz v2, :cond_4

    .line 712
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    move-object v0, v3

    goto :goto_2

    .line 710
    :catchall_0
    move-exception v0

    .line 711
    :goto_5
    if-eqz v1, :cond_5

    .line 712
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 714
    :cond_5
    throw v0

    .line 710
    :catchall_1
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_5

    :catchall_2
    move-exception v0

    move-object v1, v2

    goto :goto_5

    .line 707
    :catch_2
    move-exception v2

    move-object v7, v2

    move-object v2, v0

    move-object v0, v7

    goto :goto_4

    .line 700
    :catch_3
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_3
.end method
