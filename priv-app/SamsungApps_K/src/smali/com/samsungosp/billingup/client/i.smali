.class final Lcom/samsungosp/billingup/client/i;
.super Landroid/os/Handler;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;


# direct methods
.method constructor <init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsungosp/billingup/client/i;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    .line 978
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 982
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UnifiedPaymentMainActivity Handler what : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    .line 983
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 1048
    :cond_0
    :goto_0
    return-void

    .line 985
    :sswitch_0
    iget-object v0, p0, Lcom/samsungosp/billingup/client/i;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 986
    iget-object v0, p0, Lcom/samsungosp/billingup/client/i;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 987
    iget-object v0, p0, Lcom/samsungosp/billingup/client/i;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 988
    iget-object v0, p0, Lcom/samsungosp/billingup/client/i;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    sget v1, Lcom/samsungosp/billingup/client/R$layout;->common_layout_progressbar_popup:I

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setContentView(I)V

    goto :goto_0

    .line 993
    :sswitch_1
    iget-object v0, p0, Lcom/samsungosp/billingup/client/i;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 994
    iget-object v0, p0, Lcom/samsungosp/billingup/client/i;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->b(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 995
    iget-object v0, p0, Lcom/samsungosp/billingup/client/i;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    goto :goto_0

    .line 1001
    :sswitch_2
    const-string v0, "handler received PAGE_ERROR"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 1002
    iget-object v0, p0, Lcom/samsungosp/billingup/client/i;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->setResult(I)V

    .line 1003
    iget-object v0, p0, Lcom/samsungosp/billingup/client/i;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->finish()V

    goto :goto_0

    .line 1008
    :sswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 1009
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "strRet : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    .line 1011
    const-string v1, "memo="

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, ";result="

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1012
    :cond_1
    const-string v0, "[Alipay] msg exception !!"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 1013
    iget-object v0, p0, Lcom/samsungosp/billingup/client/i;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->c(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Landroid/webkit/WebView;

    move-result-object v0

    const-string v1, "javascript:reInit()"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1016
    :cond_2
    const-string v1, "memo="

    .line 1017
    const-string v2, "memo="

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 1018
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v1, v2

    .line 1019
    const-string v2, ";result="

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 1020
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 1022
    new-instance v1, Lcom/samsungosp/billingup/client/alipay/ResultChecker;

    invoke-direct {v1, v0}, Lcom/samsungosp/billingup/client/alipay/ResultChecker;-><init>(Ljava/lang/String;)V

    .line 1024
    invoke-virtual {v1}, Lcom/samsungosp/billingup/client/alipay/ResultChecker;->checkSign()I

    move-result v0

    .line 1025
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[Alipay] result code : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 1026
    if-eq v0, v3, :cond_0

    .line 1028
    if-ne v0, v4, :cond_3

    .line 1029
    iget-object v0, p0, Lcom/samsungosp/billingup/client/i;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->c(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Landroid/webkit/WebView;

    move-result-object v0

    const-string v1, "javascript:cacComplete()"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1030
    :cond_3
    if-nez v0, :cond_0

    .line 1031
    iget-object v0, p0, Lcom/samsungosp/billingup/client/i;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->c(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Landroid/webkit/WebView;

    move-result-object v0

    const-string v1, "javascript:reInit()"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1038
    :sswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/String;

    .line 1039
    if-eqz v0, :cond_0

    array-length v1, v0

    if-ne v1, v4, :cond_0

    .line 1040
    iget-object v1, p0, Lcom/samsungosp/billingup/client/i;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    aget-object v2, v0, v2

    aget-object v0, v0, v3

    invoke-static {v1, v2, v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1044
    :sswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/String;

    .line 1045
    if-eqz v0, :cond_0

    array-length v1, v0

    if-ne v1, v4, :cond_0

    .line 1046
    iget-object v1, p0, Lcom/samsungosp/billingup/client/i;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    aget-object v2, v0, v2

    aget-object v0, v0, v3

    invoke-static {v1, v2, v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 983
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x25 -> :sswitch_4
        0x26 -> :sswitch_5
        0x7a -> :sswitch_3
    .end sparse-switch
.end method
