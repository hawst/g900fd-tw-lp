.class final Lcom/samsungosp/billingup/client/bg;
.super Landroid/os/Handler;
.source "ProGuard"


# instance fields
.field final synthetic a:Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;


# direct methods
.method constructor <init>(Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsungosp/billingup/client/bg;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;

    .line 181
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 184
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UnifiedPaymentPGActivity Handler what : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    .line 185
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 266
    :cond_0
    :goto_0
    return-void

    .line 188
    :pswitch_0
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bg;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bg;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 190
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bg;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 192
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bg;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    sget v1, Lcom/samsungosp/billingup/client/R$layout;->common_layout_progressbar_popup:I

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setContentView(I)V

    goto :goto_0

    .line 197
    :pswitch_1
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bg;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bg;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->b(Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bg;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    goto :goto_0

    .line 205
    :pswitch_2
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bg;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->finish()V

    goto :goto_0

    .line 208
    :pswitch_3
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bg;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->mIsISPPage:Z

    goto :goto_0

    .line 211
    :pswitch_4
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bg;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;

    const/16 v1, 0xa

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->setResult(ILandroid/content/Intent;)V

    .line 212
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bg;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->finish()V

    goto :goto_0

    .line 216
    :pswitch_5
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bg;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->setResult(I)V

    .line 217
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bg;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->finish()V

    goto :goto_0

    .line 221
    :pswitch_6
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bg;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->setResult(I)V

    .line 222
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bg;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->finish()V

    goto :goto_0

    .line 226
    :pswitch_7
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bg;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->setResult(I)V

    .line 227
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bg;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->finish()V

    goto/16 :goto_0

    .line 231
    :pswitch_8
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bg;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->setResult(I)V

    .line 232
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bg;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->finish()V

    goto/16 :goto_0

    .line 236
    :pswitch_9
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 237
    const-string v1, "PAYER_ID"

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 238
    iget-object v1, p0, Lcom/samsungosp/billingup/client/bg;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;

    const/16 v2, 0xb

    invoke-virtual {v1, v2, v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->setResult(ILandroid/content/Intent;)V

    .line 239
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bg;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->finish()V

    goto/16 :goto_0

    .line 242
    :pswitch_a
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 243
    const-string v1, "KCC_QUERY_STRING"

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 244
    iget-object v1, p0, Lcom/samsungosp/billingup/client/bg;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;

    const/16 v2, 0xc

    invoke-virtual {v1, v2, v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->setResult(ILandroid/content/Intent;)V

    .line 245
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bg;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->finish()V

    goto/16 :goto_0

    .line 248
    :pswitch_b
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 249
    const-string v1, "MRC_QUERY_STRING"

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 250
    iget-object v1, p0, Lcom/samsungosp/billingup/client/bg;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;

    const/16 v2, 0xd

    invoke-virtual {v1, v2, v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->setResult(ILandroid/content/Intent;)V

    .line 251
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bg;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->finish()V

    goto/16 :goto_0

    .line 254
    :pswitch_c
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bg;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->setResult(I)V

    .line 255
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bg;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->finish()V

    goto/16 :goto_0

    .line 258
    :pswitch_d
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bg;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->setResult(I)V

    .line 259
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bg;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->finish()V

    goto/16 :goto_0

    .line 262
    :pswitch_e
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 263
    const-string v1, "ICC_QUERY_STRING"

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 264
    iget-object v1, p0, Lcom/samsungosp/billingup/client/bg;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;

    const/16 v2, 0xe

    invoke-virtual {v1, v2, v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->setResult(ILandroid/content/Intent;)V

    .line 265
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bg;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->finish()V

    goto/16 :goto_0

    .line 185
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method
