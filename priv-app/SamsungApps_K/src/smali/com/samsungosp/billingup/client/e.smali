.class final Lcom/samsungosp/billingup/client/e;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field final synthetic c:Lcom/samsungosp/billingup/client/CreditCardActivity;


# direct methods
.method public constructor <init>(Lcom/samsungosp/billingup/client/CreditCardActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 332
    iput-object p1, p0, Lcom/samsungosp/billingup/client/e;->c:Lcom/samsungosp/billingup/client/CreditCardActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 333
    iput-object p2, p0, Lcom/samsungosp/billingup/client/e;->a:Ljava/lang/String;

    .line 334
    iput-object p3, p0, Lcom/samsungosp/billingup/client/e;->b:Ljava/lang/String;

    .line 335
    return-void
.end method

.method private a(Ljava/net/HttpURLConnection;)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v9, 0x1

    .line 407
    const-string v0, "POST"

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 415
    const-string v0, "Content-Type"

    const-string v1, "application/json"

    invoke-virtual {p1, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    const-string v0, "Accept"

    const-string v1, "application/json"

    invoke-virtual {p1, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    const-string v0, "ClientVersion"

    invoke-static {}, Lcom/samsungosp/billingup/client/UnifiedPayment;->getVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    invoke-virtual {p1, v9}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    .line 422
    invoke-virtual {p1, v9}, Ljava/net/HttpURLConnection;->setDefaultUseCaches(Z)V

    .line 424
    invoke-virtual {p1, v9}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 425
    invoke-virtual {p1, v9}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 426
    iget-object v0, p0, Lcom/samsungosp/billingup/client/e;->c:Lcom/samsungosp/billingup/client/CreditCardActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/CreditCardActivity;->e(Lcom/samsungosp/billingup/client/CreditCardActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "UTF-8"

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    array-length v0, v0

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V

    .line 428
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    .line 429
    iget-object v1, p0, Lcom/samsungosp/billingup/client/e;->c:Lcom/samsungosp/billingup/client/CreditCardActivity;

    invoke-static {v1}, Lcom/samsungosp/billingup/client/CreditCardActivity;->e(Lcom/samsungosp/billingup/client/CreditCardActivity;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    .line 430
    const-string v1, "HttpURLConnection OutputStream write jsonData"

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    .line 431
    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 432
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 434
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    .line 435
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Load ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsungosp/billingup/client/e;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 437
    const/16 v1, 0xc8

    if-eq v0, v1, :cond_0

    .line 438
    const/16 v1, 0x130

    if-ne v0, v1, :cond_4

    .line 440
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Load Success : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsungosp/billingup/client/e;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 443
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v5

    .line 444
    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 446
    const-string v0, ">>> START RESPONSE HEADER "

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 448
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 458
    const-string v0, "<<< END RESPONSE HEADER "

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 460
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    .line 461
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 462
    const/16 v2, 0x400

    new-array v2, v2, [B

    .line 465
    :goto_1
    array-length v3, v2

    invoke-virtual {v0, v2, v4, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v3

    const/4 v5, -0x1

    if-ne v3, v5, :cond_3

    .line 468
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    .line 470
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    .line 474
    :goto_2
    return-object v0

    .line 449
    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 450
    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 451
    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    move v3, v4

    .line 452
    :goto_3
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lt v3, v2, :cond_2

    .line 455
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, ":"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 453
    :cond_2
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v2, ";"

    invoke-direct {v8, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 452
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_3

    .line 466
    :cond_3
    invoke-virtual {v1, v2, v4, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_1

    .line 474
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 342
    move v4, v0

    move-object v3, v1

    .line 376
    :goto_0
    const/4 v0, 0x3

    if-lt v4, v0, :cond_3

    :cond_0
    move-object v0, v3

    .line 380
    :cond_1
    :goto_1
    if-eqz v0, :cond_2

    .line 383
    iget-object v1, p0, Lcom/samsungosp/billingup/client/e;->c:Lcom/samsungosp/billingup/client/CreditCardActivity;

    new-instance v2, Lcom/samsungosp/billingup/client/f;

    invoke-direct {v2, p0, v0}, Lcom/samsungosp/billingup/client/f;-><init>(Lcom/samsungosp/billingup/client/e;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/samsungosp/billingup/client/CreditCardActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 393
    :cond_2
    return-void

    .line 348
    :cond_3
    :try_start_0
    new-instance v2, Ljava/net/URL;

    iget-object v0, p0, Lcom/samsungosp/billingup/client/e;->b:Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 349
    sget-object v0, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    invoke-virtual {v2, v0}, Ljava/net/URL;->openConnection(Ljava/net/Proxy;)Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljavax/net/ssl/SSLHandshakeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 350
    :try_start_1
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "HttpURLConnection openConnection has called url : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    .line 354
    invoke-direct {p0, v0}, Lcom/samsungosp/billingup/client/e;->a(Ljava/net/HttpURLConnection;)Ljava/lang/String;
    :try_end_1
    .catch Ljavax/net/ssl/SSLHandshakeException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v3

    .line 370
    if-eqz v0, :cond_4

    .line 371
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_4
    move-object v0, v3

    .line 375
    :goto_2
    if-nez v0, :cond_1

    .line 376
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-object v3, v0

    goto :goto_0

    .line 359
    :catch_0
    move-exception v0

    :goto_3
    :try_start_2
    invoke-virtual {v0}, Ljavax/net/ssl/SSLHandshakeException;->printStackTrace()V

    .line 360
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 361
    const/16 v2, 0x25

    iput v2, v0, Landroid/os/Message;->what:I

    .line 362
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsungosp/billingup/client/e;->a:Ljava/lang/String;

    aput-object v5, v2, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/samsungosp/billingup/client/e;->b:Ljava/lang/String;

    aput-object v5, v2, v4

    .line 363
    iput-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 364
    iget-object v2, p0, Lcom/samsungosp/billingup/client/e;->c:Lcom/samsungosp/billingup/client/CreditCardActivity;

    invoke-static {v2}, Lcom/samsungosp/billingup/client/CreditCardActivity;->d(Lcom/samsungosp/billingup/client/CreditCardActivity;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 370
    if-eqz v1, :cond_0

    .line 371
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    move-object v0, v3

    .line 365
    goto :goto_1

    .line 366
    :catch_1
    move-exception v0

    move-object v2, v1

    :goto_4
    :try_start_3
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 370
    if-eqz v2, :cond_4

    .line 371
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    move-object v0, v3

    goto :goto_2

    .line 369
    :catchall_0
    move-exception v0

    .line 370
    :goto_5
    if-eqz v1, :cond_5

    .line 371
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 373
    :cond_5
    throw v0

    .line 369
    :catchall_1
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_5

    :catchall_2
    move-exception v0

    move-object v1, v2

    goto :goto_5

    .line 366
    :catch_2
    move-exception v2

    move-object v7, v2

    move-object v2, v0

    move-object v0, v7

    goto :goto_4

    .line 359
    :catch_3
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_3
.end method
