.class final Lcom/samsungosp/billingup/client/as;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

.field private final synthetic b:Ljava/lang/String;

.field private final synthetic c:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsungosp/billingup/client/as;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    iput-object p2, p0, Lcom/samsungosp/billingup/client/as;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/samsungosp/billingup/client/as;->c:Ljava/lang/String;

    .line 1114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 1117
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1118
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "errorId : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsungosp/billingup/client/as;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", errorMessage : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1119
    iget-object v2, p0, Lcom/samsungosp/billingup/client/as;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1118
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 1121
    const-string v1, "ERROR_ID"

    iget-object v2, p0, Lcom/samsungosp/billingup/client/as;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1122
    const-string v1, "ERROR_MESSAGE"

    iget-object v2, p0, Lcom/samsungosp/billingup/client/as;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1126
    iget-object v1, p0, Lcom/samsungosp/billingup/client/as;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v1}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2, v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->setResult(ILandroid/content/Intent;)V

    .line 1127
    iget-object v0, p0, Lcom/samsungosp/billingup/client/as;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->finish()V

    .line 1128
    return-void
.end method
