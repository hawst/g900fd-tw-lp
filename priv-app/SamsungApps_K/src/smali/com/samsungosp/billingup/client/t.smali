.class final Lcom/samsungosp/billingup/client/t;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

.field private final synthetic b:Ljava/lang/String;

.field private final synthetic c:Ljava/lang/String;

.field private final synthetic d:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsungosp/billingup/client/t;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    iput-object p2, p0, Lcom/samsungosp/billingup/client/t;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/samsungosp/billingup/client/t;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/samsungosp/billingup/client/t;->d:Ljava/lang/String;

    .line 1061
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 1064
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 1065
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "resultCode : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsungosp/billingup/client/t;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", resStr : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1066
    iget-object v1, p0, Lcom/samsungosp/billingup/client/t;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", signature : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsungosp/billingup/client/t;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1065
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 1069
    iget-object v0, p0, Lcom/samsungosp/billingup/client/t;->c:Ljava/lang/String;

    invoke-static {v0}, Lorg/json/simple/JSONValue;->parse(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 1070
    check-cast v0, Lorg/json/simple/JSONObject;

    .line 1071
    const-string v1, "userID"

    invoke-virtual {v0, v1}, Lorg/json/simple/JSONObject;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1072
    const-string v3, "paymentMethod"

    invoke-virtual {v0, v3}, Lorg/json/simple/JSONObject;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1073
    const-string v3, "FCD"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1074
    const-string v0, "GCC"

    .line 1077
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[receivePaymentResult] userID : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 1078
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[receivePaymentResult] paymentMethod : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 1080
    if-eqz v0, :cond_1

    const-string v3, ""

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "CPN"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1081
    iget-object v3, p0, Lcom/samsungosp/billingup/client/t;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v3}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->g(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->setRecentPayment(Ljava/lang/String;Ljava/lang/String;)V

    .line 1084
    :cond_1
    const-string v0, "PAYMENT_RECEITE"

    iget-object v1, p0, Lcom/samsungosp/billingup/client/t;->c:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1085
    const-string v0, "SIGNATURE"

    iget-object v1, p0, Lcom/samsungosp/billingup/client/t;->d:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1089
    iget-object v0, p0, Lcom/samsungosp/billingup/client/t;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->setResult(ILandroid/content/Intent;)V

    .line 1090
    iget-object v0, p0, Lcom/samsungosp/billingup/client/t;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;->a(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity$Bridge;)Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->finish()V

    .line 1091
    return-void
.end method
