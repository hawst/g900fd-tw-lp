.class public Lcom/samsungosp/billingup/client/requestparam/UserInfo;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public accessToken:Ljava/lang/String;

.field public authAppID:Ljava/lang/String;

.field public storeUserID:Ljava/lang/String;

.field public userEmail:Ljava/lang/String;

.field public userID:Ljava/lang/String;

.field public userName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lcom/samsungosp/billingup/client/requestparam/UserInfo$1;

    invoke-direct {v0}, Lcom/samsungosp/billingup/client/requestparam/UserInfo$1;-><init>()V

    sput-object v0, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 53
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static readFromParcel(Landroid/os/Parcel;)Lcom/samsungosp/billingup/client/requestparam/UserInfo;
    .locals 2

    .prologue
    .line 33
    new-instance v0, Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    invoke-direct {v0}, Lcom/samsungosp/billingup/client/requestparam/UserInfo;-><init>()V

    .line 35
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->userID:Ljava/lang/String;

    .line 36
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->userName:Ljava/lang/String;

    .line 37
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->userEmail:Ljava/lang/String;

    .line 38
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->accessToken:Ljava/lang/String;

    .line 39
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->authAppID:Ljava/lang/String;

    .line 40
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->storeUserID:Ljava/lang/String;

    .line 42
    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->userID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 25
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->userName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 26
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->userEmail:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 27
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->accessToken:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 28
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->authAppID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 29
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/UserInfo;->storeUserID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 30
    return-void
.end method
