.class public Lcom/samsungosp/billingup/client/requestparam/CreditCardData;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public appServiceID:Ljava/lang/String;

.field public country:Ljava/lang/String;

.field public deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

.field public isOnlyRegister:Ljava/lang/String;

.field public language:Ljava/lang/String;

.field private platformCode:Ljava/lang/String;

.field public storeRequestID:Ljava/lang/String;

.field public timeOffset:Ljava/lang/String;

.field public upServerURL:Ljava/lang/String;

.field public userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

.field public uxVersion:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    new-instance v0, Lcom/samsungosp/billingup/client/requestparam/CreditCardData$1;

    invoke-direct {v0}, Lcom/samsungosp/billingup/client/requestparam/CreditCardData$1;-><init>()V

    sput-object v0, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 75
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const-string v0, "A"

    iput-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->platformCode:Ljava/lang/String;

    .line 24
    return-void
.end method

.method public static readFromParcel(Landroid/os/Parcel;)Lcom/samsungosp/billingup/client/requestparam/CreditCardData;
    .locals 2

    .prologue
    .line 49
    new-instance v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;

    invoke-direct {v1}, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;-><init>()V

    .line 51
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->appServiceID:Ljava/lang/String;

    .line 52
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->country:Ljava/lang/String;

    .line 53
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->language:Ljava/lang/String;

    .line 54
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->upServerURL:Ljava/lang/String;

    .line 55
    const-class v0, Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    .line 56
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 55
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    iput-object v0, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    .line 57
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->platformCode:Ljava/lang/String;

    .line 58
    const-class v0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    .line 59
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 58
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    iput-object v0, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    .line 60
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->timeOffset:Ljava/lang/String;

    .line 61
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->storeRequestID:Ljava/lang/String;

    .line 62
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->isOnlyRegister:Ljava/lang/String;

    .line 63
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->uxVersion:Ljava/lang/String;

    .line 64
    return-object v1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 35
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->appServiceID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->country:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->language:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 38
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->upServerURL:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 40
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->platformCode:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 42
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->timeOffset:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 43
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->storeRequestID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 44
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->isOnlyRegister:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/CreditCardData;->uxVersion:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 46
    return-void
.end method
