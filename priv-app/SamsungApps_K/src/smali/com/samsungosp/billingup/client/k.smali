.class final Lcom/samsungosp/billingup/client/k;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;


# direct methods
.method constructor <init>(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsungosp/billingup/client/k;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    .line 2736
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 2740
    const-string v0, "over 1 minute oneMinuteCheckRunnable"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->i(Ljava/lang/String;)V

    .line 2741
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2742
    const-string v1, "ERROR_MESSAGE"

    const-string v2, "Waiting time has been exceeded."

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2744
    iget-object v1, p0, Lcom/samsungosp/billingup/client/k;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    .line 2745
    iget-object v2, p0, Lcom/samsungosp/billingup/client/k;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    sget v3, Lcom/samsungosp/billingup/client/R$string;->ids_ph_pop_unable_to_connect_to_network_try_later:I

    invoke-virtual {v2, v3}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2746
    const/4 v3, 0x0

    .line 2743
    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 2746
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 2747
    iget-object v1, p0, Lcom/samsungosp/billingup/client/k;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    const/4 v2, 0x3

    invoke-virtual {v1, v2, v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->setResult(ILandroid/content/Intent;)V

    .line 2748
    iget-object v0, p0, Lcom/samsungosp/billingup/client/k;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->c(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Landroid/webkit/WebView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2749
    iget-object v0, p0, Lcom/samsungosp/billingup/client/k;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->c(Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;)Landroid/webkit/WebView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->clearCache(Z)V

    .line 2751
    :cond_0
    iget-object v0, p0, Lcom/samsungosp/billingup/client/k;->a:Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;

    invoke-virtual {v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentMainActivity;->finish()V

    .line 2752
    return-void
.end method
