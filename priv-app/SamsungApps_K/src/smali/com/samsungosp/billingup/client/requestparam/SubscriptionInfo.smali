.class public Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public dayPeriodPayment:Ljava/lang/String;

.field public freeTrialPeriod:Ljava/lang/String;

.field public notificationURL:Ljava/lang/String;

.field public periodStartPaymentInfoList:[Ljava/lang/String;

.field public repeatCount:Ljava/lang/String;

.field public repeatNumber:Ljava/lang/String;

.field public retryCount:Ljava/lang/String;

.field public retryPeriod:Ljava/lang/String;

.field public statusNotificationURL:Ljava/lang/String;

.field public subscriptionID:Ljava/lang/String;

.field public subscriptionOrderURL:Ljava/lang/String;

.field public subscriptionTypeCode:Ljava/lang/String;

.field public timeZone:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 76
    new-instance v0, Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo$1;

    invoke-direct {v0}, Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo$1;-><init>()V

    sput-object v0, Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 84
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static readFromParcel(Landroid/os/Parcel;)Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo;
    .locals 2

    .prologue
    .line 53
    new-instance v0, Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo;

    invoke-direct {v0}, Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo;-><init>()V

    .line 55
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo;->subscriptionTypeCode:Ljava/lang/String;

    .line 56
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo;->freeTrialPeriod:Ljava/lang/String;

    .line 57
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo;->subscriptionOrderURL:Ljava/lang/String;

    .line 58
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo;->timeZone:Ljava/lang/String;

    .line 59
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo;->retryCount:Ljava/lang/String;

    .line 60
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo;->retryPeriod:Ljava/lang/String;

    .line 61
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo;->repeatCount:Ljava/lang/String;

    .line 62
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo;->dayPeriodPayment:Ljava/lang/String;

    .line 63
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 64
    if-eqz v1, :cond_0

    .line 65
    new-array v1, v1, [Ljava/lang/String;

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo;->periodStartPaymentInfoList:[Ljava/lang/String;

    .line 66
    iget-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo;->periodStartPaymentInfoList:[Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/os/Parcel;->readStringArray([Ljava/lang/String;)V

    .line 68
    :cond_0
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo;->notificationURL:Ljava/lang/String;

    .line 69
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo;->subscriptionID:Ljava/lang/String;

    .line 70
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo;->repeatNumber:Ljava/lang/String;

    .line 71
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo;->statusNotificationURL:Ljava/lang/String;

    .line 73
    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo;->subscriptionTypeCode:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 33
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo;->freeTrialPeriod:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 34
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo;->subscriptionOrderURL:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 35
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo;->timeZone:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo;->retryCount:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo;->retryPeriod:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 38
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo;->repeatCount:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo;->dayPeriodPayment:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo;->periodStartPaymentInfoList:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 41
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 46
    :goto_0
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo;->notificationURL:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 47
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo;->subscriptionID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 48
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo;->repeatNumber:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 49
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo;->statusNotificationURL:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 50
    return-void

    .line 43
    :cond_0
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo;->periodStartPaymentInfoList:[Ljava/lang/String;

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 44
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/SubscriptionInfo;->periodStartPaymentInfoList:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    goto :goto_0
.end method
