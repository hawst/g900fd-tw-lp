.class public Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field static a:Ljava/lang/String;


# instance fields
.field b:Ljava/lang/Integer;

.field c:Lcom/alipay/android/app/IAlixPay;

.field d:Z

.field e:Landroid/app/Activity;

.field private f:Landroid/content/ServiceConnection;

.field private g:Lcom/alipay/android/app/IRemoteServiceCallback;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-string v0, "MobileSecurePayer"

    sput-object v0, Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;->b:Ljava/lang/Integer;

    .line 32
    iput-object v2, p0, Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;->c:Lcom/alipay/android/app/IAlixPay;

    .line 33
    iput-boolean v1, p0, Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;->d:Z

    .line 35
    iput-object v2, p0, Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;->e:Landroid/app/Activity;

    .line 37
    new-instance v0, Lcom/samsungosp/billingup/client/alipay/a;

    invoke-direct {v0, p0}, Lcom/samsungosp/billingup/client/alipay/a;-><init>(Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;)V

    iput-object v0, p0, Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;->f:Landroid/content/ServiceConnection;

    .line 126
    new-instance v0, Lcom/samsungosp/billingup/client/alipay/b;

    invoke-direct {v0, p0}, Lcom/samsungosp/billingup/client/alipay/b;-><init>(Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;)V

    iput-object v0, p0, Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;->g:Lcom/alipay/android/app/IRemoteServiceCallback;

    .line 27
    return-void
.end method

.method static synthetic a(Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;)Lcom/alipay/android/app/IRemoteServiceCallback;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;->g:Lcom/alipay/android/app/IRemoteServiceCallback;

    return-object v0
.end method

.method static synthetic b(Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;)Landroid/content/ServiceConnection;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;->f:Landroid/content/ServiceConnection;

    return-object v0
.end method

.method public static isAlipayInstalled(Landroid/content/Context;)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 161
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 162
    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v3

    move v1, v2

    .line 163
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-lt v1, v0, :cond_0

    .line 169
    :goto_1
    return v2

    .line 164
    :cond_0
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageInfo;

    .line 165
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const-string v4, "com.alipay.android.app"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 166
    const/4 v2, 0x1

    goto :goto_1

    .line 163
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method


# virtual methods
.method public pay(Ljava/lang/String;Landroid/os/Handler;ILandroid/app/Activity;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 59
    iget-boolean v1, p0, Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;->d:Z

    if-eqz v1, :cond_0

    .line 60
    const/4 v0, 0x0

    .line 119
    :goto_0
    return v0

    .line 61
    :cond_0
    iput-boolean v0, p0, Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;->d:Z

    .line 64
    iput-object p4, p0, Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;->e:Landroid/app/Activity;

    .line 67
    iget-object v1, p0, Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;->c:Lcom/alipay/android/app/IAlixPay;

    if-nez v1, :cond_1

    .line 69
    iget-object v1, p0, Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;->e:Landroid/app/Activity;

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/alipay/android/app/IAlixPay;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;->f:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2, v3, v0}, Landroid/app/Activity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 74
    :cond_1
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/samsungosp/billingup/client/alipay/c;

    invoke-direct {v2, p0, p1, p3, p2}, Lcom/samsungosp/billingup/client/alipay/c;-><init>(Lcom/samsungosp/billingup/client/alipay/MobileSecurePayer;Ljava/lang/String;ILandroid/os/Handler;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 117
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method
