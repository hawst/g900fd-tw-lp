.class public Lcom/samsungosp/billingup/client/Constants;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field public static final ACTION_SAMSUNGACCOUNT_AIDL_SERVICE:Ljava/lang/String; = "com.msc.action.samsungaccount.REQUEST_SERVICE"

.field public static final CARRIERBILLING_TIMEOUT:I = 0xea60

.field public static final CHECK_ICC_URL:I = 0x5

.field public static final CHECK_KUP_RETRIEVE_URL:I = 0x6

.field public static final CHECK_LIVE_GAMER_URL:I = 0x4

.field public static final CHECK_PAYPAL_URL:I = 0x0

.field public static final CHECK_QIWI_URL:I = 0x3

.field public static final CHECK_SINA_URL:I = 0x2

.field public static final CHECK_WEBMONEY_URL:I = 0x1

.field public static final COUNTRY_CODE_CHINA:Ljava/lang/String; = "CHN"

.field public static final COUNTRY_CODE_KOREA:Ljava/lang/String; = "KOR"

.field public static final DIRECT_MODE_MNC_MCC:[Ljava/lang/String;

.field public static final DISPLAY_TYPE_MOBILE:Ljava/lang/String; = "M"

.field public static final DISPLAY_TYPE_TABLET:Ljava/lang/String; = "T"

.field public static final HOST_ICC_URL:Ljava/lang/String; = "samsung.samanepay.com"

.field public static final HOST_KUP_RETRIEVE_URL:Ljava/lang/String; = "account.samsung.com"

.field public static final HOST_LIVE_GAMER_STG_URL:Ljava/lang/String; = "demo.paysbuy.com"

.field public static final HOST_LIVE_GAMER_URL:Ljava/lang/String; = "www.paysbuy.com"

.field public static final HOST_PAYPAL_URL:Ljava/lang/String; = "www.paypal.com"

.field public static final HOST_QIWI_URL:Ljava/lang/String; = "w.qiwi.com"

.field public static final HOST_SINA_URL:Ljava/lang/String; = "ota.pay.mobile.sina.cn"

.field public static final HOST_WEBMONEY_URL:Ljava/lang/String; = "ipn.teledit.com"

.field public static final HPP_CORP_KTF:Ljava/lang/String; = "KTF"

.field public static final HPP_CORP_LGT:Ljava/lang/String; = "LGT"

.field public static final HPP_CORP_SKT:Ljava/lang/String; = "SKT"

.field public static final HTTPS_PROTOCOL:Ljava/lang/String; = "https://"

.field public static final HTTP_PROTOCOL:Ljava/lang/String; = "http://"

.field public static final ICC_QUERY_STRING:Ljava/lang/String; = "ICC_QUERY_STRING"

.field public static final INTENT_ACTION_CREDITCARD:Ljava/lang/String; = "CREDIT_CARD"

.field public static final INTENT_ACTION_GIFTCARD:Ljava/lang/String; = "GIFT_CARD"

.field public static final INTENT_ACTION_ISP_SUCCESS:Ljava/lang/String; = "ISP_SUCCESS"

.field public static final INTENT_ACTION_PAYMENT:Ljava/lang/String; = "PAYMENT"

.field public static final INTENT_ACTION_REGISTER_CREDITCARD:Ljava/lang/String; = "REGISTER_CREDIT_CARD"

.field public static final INTENT_EXTRA_CREDIT_CARD_REQUEST:Ljava/lang/String; = "CREDIT_CARD_REQUEST"

.field public static final INTENT_EXTRA_GIFT_CARD_REQUEST:Ljava/lang/String; = "GIFT_CARD_REQUEST"

.field public static final INTENT_EXTRA_IS_UX_V2:Ljava/lang/String; = "IS_UX_V2"

.field public static final INTENT_EXTRA_UNIFIED_PAYMENT_REQUEST:Ljava/lang/String; = "UNIFIED_PAYMENT_REQUEST"

.field public static final INTENT_SCHEMA_LERANINGHUB:Ljava/lang/String; = "UPLearningHub://"

.field public static final INTENT_SCHEMA_MAILTO:Ljava/lang/String; = "mailto"

.field public static final INTENT_SCHEMA_MEDIAHUB:Ljava/lang/String; = "UPMediaHub://"

.field public static final INTENT_SCHEMA_MUSICSHUB:Ljava/lang/String; = "UPMusicHub://"

.field public static final INTENT_SCHEMA_READERSHUB:Ljava/lang/String; = "UPReadersHub://"

.field public static final INTENT_SCHEMA_SAMSUNGAPPS:Ljava/lang/String; = "UPSamsungApps://"

.field public static final INTENT_SCHEMA_SCLOUDHUB:Ljava/lang/String; = "UPSamsungCloud://"

.field public static final INTENT_SCHEMA_VIDEOHUB:Ljava/lang/String; = "UPVideoHub://"

.field public static final KCC_QUERY_STRING:Ljava/lang/String; = "KCC_QUERY_STRING"

.field public static final KEY_UP_JSON_URL:Ljava/lang/String; = "UPServerURL"

.field public static final MRC_QUERY_STRING:Ljava/lang/String; = "MRC_QUERY_STRING"

.field public static final N_LOWER_CASE:Ljava/lang/String; = "n"

.field public static final N_UPPER_CASE:Ljava/lang/String; = "N"

.field public static final PATH_CREDITCARD:Ljava/lang/String; = "creditcard.do"

.field public static final PATH_CREDITCARD_REGISTER:Ljava/lang/String; = "register_creditcard.do"

.field public static final PATH_GIFTCARD:Ljava/lang/String; = "giftcard.do"

.field public static final PATH_PAYMENT:Ljava/lang/String; = "payment.do"

.field public static final PATH_TABLET:Ljava/lang/String; = "tablet"

.field public static final PATH_TABLET_CREDITCARD:Ljava/lang/String; = "tablet_creditcard.do"

.field public static final PATH_TABLET_GIFTCARD:Ljava/lang/String; = "tablet_giftcard.do"

.field public static final PATH_TABLET_PAYMENT:Ljava/lang/String; = "tablet_payment.do"

.field public static final PATH_UP_JSON:Ljava/lang/String; = "/mnt/sdcard/up.json"

.field public static final PAYER_ID:Ljava/lang/String; = "PAYER_ID"

.field public static final POSTFIX_URL_UPOINT_CI_PAGE_CLOSE:Ljava/lang/String; = "close=true"

.field public static final PREFIX_URL_JAVASCRIPT_FUNCTION_CALL:Ljava/lang/String; = "javascript:"

.field public static final RECENT_PAYMENT_KEY:Ljava/lang/String; = "recent_payment_key"

.field public static final REGISTER_CARD_FAIL_STRING:Ljava/lang/String; = "REGISTER_CARD_FAIL_STRING"

.field public static final REQUEST_ID_ACCOUNT_VERIFY:I = 0x64

.field public static final REQUEST_ID_CREDIT_CARD:I = 0x65

.field public static final REQUEST_ID_PG:I = 0x66

.field public static final RESULT_CARD_REGISTER:I = 0xf

.field public static final RESULT_CSM_STATUS_CHECK:I = 0x7

.field public static final RESULT_GPP_COMPLETE:I = 0xb

.field public static final RESULT_ICC_REGISTER:I = 0xe

.field public static final RESULT_JWM_STATUS_CHECK:I = 0x5

.field public static final RESULT_KCC_COMPLETE:I = 0xc

.field public static final RESULT_LIVE_GAMER_COMPLETE:I = 0x9

.field public static final RESULT_MRC_COMPLETE:I = 0xd

.field public static final RESULT_PG_FAIL:I = 0x6

.field public static final RESULT_QIWI_STATUS_CHECK:I = 0x8

.field public static final RESULT_UPOINT_RETRIEVE:I = 0xa

.field public static final RETURN_NETWORK_DELAY:I = 0x12c

.field public static final SAMSUNG_ACCOUNT_AIDL_VERSION:I = 0x24ab8

.field public static final SAMSUNG_ACCOUNT_NEW_INTERFACE_VERSION:I = 0x2244e

.field public static final SAMSUNG_ACCOUNT_RETURN_CODE:Ljava/lang/String; = "SAC_0105"

.field public static final SIMOPERATORNAME_KTF:Ljava/lang/String; = "KTF"

.field public static final SIMOPERATORNAME_LGT:Ljava/lang/String; = "LGT"

.field public static final SIMOPERATORNAME_SKT:Ljava/lang/String; = "SKTelecom"

.field public static final SIMOPERATOR_KTF_1:Ljava/lang/String; = "45002"

.field public static final SIMOPERATOR_KTF_2:Ljava/lang/String; = "45004"

.field public static final SIMOPERATOR_KTF_3:Ljava/lang/String; = "45008"

.field public static final SIMOPERATOR_LGT_1:Ljava/lang/String; = "45006"

.field public static final SIMOPERATOR_SKT_1:Ljava/lang/String; = "45003"

.field public static final SIMOPERATOR_SKT_2:Ljava/lang/String; = "45005"

.field public static final SIMOPERATOR_SKT_3:Ljava/lang/String; = "45011"

.field public static final SLASH:Ljava/lang/String; = "/"

.field public static final SSLHANDSHAKE_ERROR:I = 0x25

.field public static final SSL_VERIFICATION_ERROR:I = 0x26

.field public static final STRING_NULL:Ljava/lang/String; = "NULL"

.field public static final SWALLET_BROADCAST_CODE:Ljava/lang/String; = "com.sec.android.wallet.PAYMENT_COMPLETED_"

.field public static final SWALLET_CANCEL_CODE:Ljava/lang/String; = "A000"

.field public static final SWALLET_PACKAGENAME:Ljava/lang/String; = "com.sec.android.wallet"

.field public static final UP_SERVER_URL:Ljava/lang/String; = "https://mop.samsungosp.com"

.field public static final UP_SERVER_URL_CHINA:Ljava/lang/String; = "https://cn-mop.samsungosp.com"

.field public static final UX_V2:Ljava/lang/String; = "v2"

.field public static final Y_LOWER_CASE:Ljava/lang/String; = "y"

.field public static final Y_UPPER_CASE:Ljava/lang/String; = "Y"


# instance fields
.field public final HOSTNAME_CN_PRD:Ljava/lang/String;

.field public final HOSTNAME_PRD:Ljava/lang/String;

.field public final HOSTNAME_PRT:Ljava/lang/String;

.field public final HOSTNAME_STG1:Ljava/lang/String;

.field public final HOSTNAME_STG2:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 89
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 90
    const-string v2, "20809"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 91
    const-string v2, "20810"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 92
    const-string v2, "20811"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 93
    const-string v2, "20813"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 94
    const-string v2, "20814"

    aput-object v2, v0, v1

    .line 89
    sput-object v0, Lcom/samsungosp/billingup/client/Constants;->DIRECT_MODE_MNC_MCC:[Ljava/lang/String;

    .line 177
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    const-string v0, "mop.samsungosp.com"

    iput-object v0, p0, Lcom/samsungosp/billingup/client/Constants;->HOSTNAME_PRD:Ljava/lang/String;

    .line 73
    const-string v0, "stg-api.samsungosp.com"

    iput-object v0, p0, Lcom/samsungosp/billingup/client/Constants;->HOSTNAME_STG2:Ljava/lang/String;

    .line 74
    const-string v0, "mop.samsungosp.com"

    iput-object v0, p0, Lcom/samsungosp/billingup/client/Constants;->HOSTNAME_STG1:Ljava/lang/String;

    .line 75
    const-string v0, "cn-mop.samsungosp.com"

    iput-object v0, p0, Lcom/samsungosp/billingup/client/Constants;->HOSTNAME_CN_PRD:Ljava/lang/String;

    .line 76
    const-string v0, "stg-mop.samsungosp.com"

    iput-object v0, p0, Lcom/samsungosp/billingup/client/Constants;->HOSTNAME_PRT:Ljava/lang/String;

    .line 4
    return-void
.end method
