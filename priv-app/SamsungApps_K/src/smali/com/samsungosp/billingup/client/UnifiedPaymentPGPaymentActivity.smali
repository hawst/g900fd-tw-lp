.class public Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;
.super Landroid/app/Activity;
.source "ProGuard"


# static fields
.field public static final UNIFIED_PAYMENT_PG_REQUEST:Ljava/lang/String; = "UNIFIED_PAYMENT_PG_REQUEST"

.field public static final UNIFIED_PAYMENT_PG_TYPE:Ljava/lang/String; = "UNIFIED_PAYMENT_PG_TYPE"

.field public static final UNIFIED_PAYMENT_PG_TYPE_ICC:Ljava/lang/String; = "ICC"

.field public static final UNIFIED_PAYMENT_PG_TYPE_INICIS_MRC:Ljava/lang/String; = "INICISMRC"

.field public static final UNIFIED_PAYMENT_PG_TYPE_KCC:Ljava/lang/String; = "KCC"

.field public static final UNIFIED_PAYMENT_PG_TYPE_KPI:Ljava/lang/String; = "KPI"

.field public static final UNIFIED_PAYMENT_PG_TYPE_KPI_MRC:Ljava/lang/String; = "KPIMRC"

.field public static final UNIFIED_PAYMENT_PG_TYPE_KUP:Ljava/lang/String; = "KUP"

.field public static final UNIFIED_PAYMENT_PG_TYPE_KUP_RETRIEVE:Ljava/lang/String; = "KUP_RETRIEVE"

.field public static final UNIFIED_PAYMENT_PG_TYPE_LIVE_GAMER:Ljava/lang/String; = "LIVE_GAMER"

.field public static final UNIFIED_PAYMENT_PG_TYPE_PAYPAL:Ljava/lang/String; = "PAYPAL"

.field public static final UNIFIED_PAYMENT_PG_TYPE_QIWI:Ljava/lang/String; = "QIWI"

.field public static final UNIFIED_PAYMENT_PG_TYPE_SINA:Ljava/lang/String; = "SINA"

.field public static final UNIFIED_PAYMENT_PG_TYPE_WEBMONEY:Ljava/lang/String; = "WEBMONEY"

.field public static final UNIFIED_PAYMENT_PG_URL:Ljava/lang/String; = "UNIFIED_PAYMENT_PG_URL"


# instance fields
.field public final TAG:Ljava/lang/String;

.field private a:Landroid/webkit/WebView;

.field private b:Landroid/app/ProgressDialog;

.field private c:Landroid/app/AlertDialog;

.field private d:Z

.field private e:Landroid/os/Handler;

.field protected mIsISPPage:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 27
    const-string v0, "UnifiedPaymentPGActivity"

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->TAG:Ljava/lang/String;

    .line 63
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->d:Z

    .line 181
    new-instance v0, Lcom/samsungosp/billingup/client/bg;

    invoke-direct {v0, p0}, Lcom/samsungosp/billingup/client/bg;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;)V

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->e:Landroid/os/Handler;

    .line 25
    return-void
.end method

.method static synthetic a(Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->b:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private a()Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 496
    .line 498
    invoke-static {}, Landroid/net/Proxy;->getDefaultHost()Ljava/lang/String;

    move-result-object v3

    .line 499
    invoke-static {}, Landroid/net/Proxy;->getDefaultPort()I

    move-result v4

    .line 501
    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 504
    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 507
    if-eqz v0, :cond_0

    .line 508
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    move v1, v0

    .line 513
    :goto_0
    const-string v0, "phone"

    invoke-virtual {p0, v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 515
    if-eqz v1, :cond_3

    .line 517
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v5

    .line 518
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimOperatorName()Ljava/lang/String;

    .line 520
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, "MobileNetwork : "

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 521
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "proxyAddress : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", proxyPort : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 522
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 521
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 526
    sget-object v1, Lcom/samsungosp/billingup/client/Constants;->DIRECT_MODE_MNC_MCC:[Ljava/lang/String;

    array-length v3, v1

    move v0, v2

    :goto_1
    if-lt v2, v3, :cond_1

    .line 532
    :goto_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isProxyDirectConnectMode : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 534
    return v0

    :cond_0
    move v1, v2

    .line 510
    goto :goto_0

    .line 526
    :cond_1
    aget-object v4, v1, v2

    .line 527
    if-eqz v4, :cond_2

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 528
    const/4 v0, 0x1

    .line 526
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2
.end method

.method static synthetic b(Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;)Z
    .locals 1

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->d:Z

    return v0
.end method


# virtual methods
.method public finish()V
    .locals 1

    .prologue
    .line 540
    invoke-super {p0}, Landroid/app/Activity;->finish()V

    .line 541
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 542
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 544
    :cond_0
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->c:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->c:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 545
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->c:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 547
    :cond_1
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 552
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 554
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 68
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 69
    invoke-virtual {p0}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 70
    const-string v0, "UNIFIED_PAYMENT_PG_TYPE"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 71
    const-string v0, "UNIFIED_PAYMENT_PG_URL"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 72
    const-string v0, "UNIFIED_PAYMENT_PG_REQUEST"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 73
    const-string v0, "IS_UX_V2"

    invoke-virtual {v2, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 75
    if-eqz v0, :cond_2

    .line 76
    sget v0, Lcom/samsungosp/billingup/client/R$layout;->up_pg_activity_v2:I

    invoke-virtual {p0, v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->setContentView(I)V

    .line 81
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, "UnifiedPaymentPGActivity onCreate"

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 84
    new-instance v0, Landroid/app/ProgressDialog;

    sget v5, Lcom/samsungosp/billingup/client/R$style;->TransparentDialog:I

    invoke-direct {v0, p0, v5}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->b:Landroid/app/ProgressDialog;

    .line 85
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->b:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v6}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 86
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->b:Landroid/app/ProgressDialog;

    sget v5, Lcom/samsungosp/billingup/client/R$string;->ids_rh_body_loading_ing:I

    invoke-virtual {p0, v5}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 88
    sget v0, Lcom/samsungosp/billingup/client/R$id;->pg_webview:I

    invoke-virtual {p0, v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->a:Landroid/webkit/WebView;

    .line 89
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->a:Landroid/webkit/WebView;

    const-string v5, "#e6e6e6"

    invoke-static {v5}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v0, v5}, Landroid/webkit/WebView;->setBackgroundColor(I)V

    .line 90
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, v6}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 93
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 95
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/webkit/WebSettings;->setJavaScriptCanOpenWindowsAutomatically(Z)V

    .line 98
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/webkit/WebSettings;->setSavePassword(Z)V

    .line 99
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    .line 103
    if-nez v2, :cond_0

    .line 104
    invoke-virtual {p0}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->finish()V

    .line 109
    :cond_0
    if-eqz v4, :cond_11

    .line 112
    const-string v0, "KCC"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 114
    const-string v0, "https://mobile.inicis.com/smart/wcard/"

    .line 144
    :goto_1
    const-string v1, "INICISMRC"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "KPIMRC"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 145
    :cond_1
    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->a:Landroid/webkit/WebView;

    const-string v2, "UTF-8"

    invoke-static {v4, v2}, Lorg/apache/http/util/EncodingUtils;->getBytes(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/webkit/WebView;->postUrl(Ljava/lang/String;[B)V

    .line 154
    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UnifiedPaymentPGActivity, params : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 167
    :goto_3
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->a:Landroid/webkit/WebView;

    new-instance v1, Lcom/samsungosp/billingup/client/bi;

    iget-object v2, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->e:Landroid/os/Handler;

    .line 168
    invoke-direct {p0}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->a()Z

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/samsungosp/billingup/client/bi;-><init>(Landroid/os/Handler;Z)V

    .line 167
    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 169
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->a:Landroid/webkit/WebView;

    new-instance v1, Lcom/samsungosp/billingup/client/UnifiedPaymentWebChromeClient;

    invoke-direct {v1, p0}, Lcom/samsungosp/billingup/client/UnifiedPaymentWebChromeClient;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 170
    return-void

    .line 78
    :cond_2
    sget v0, Lcom/samsungosp/billingup/client/R$layout;->up_pg_activity:I

    invoke-virtual {p0, v0}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->setContentView(I)V

    goto/16 :goto_0

    .line 115
    :cond_3
    const-string v0, "INICISMRC"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 116
    const-string v0, "https://inilite.inicis.com/inibill/inibill_card.jsp"

    goto :goto_1

    .line 119
    :cond_4
    const-string v0, "KPIMRC"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 120
    const-string v0, "https://inilite.inicis.com/inibill/inibill_hpp.jsp"

    goto :goto_1

    .line 123
    :cond_5
    const-string v0, "PAYPAL"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    move-object v0, v1

    .line 126
    goto :goto_1

    :cond_6
    const-string v0, "WEBMONEY"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    move-object v0, v1

    .line 128
    goto :goto_1

    :cond_7
    const-string v0, "SINA"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    move-object v0, v1

    .line 130
    goto/16 :goto_1

    :cond_8
    const-string v0, "KUP"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 131
    const-string v0, "https://mobile.inicis.com/smart/upoint/"

    goto/16 :goto_1

    .line 132
    :cond_9
    const-string v0, "QIWI"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    move-object v0, v1

    .line 134
    goto/16 :goto_1

    :cond_a
    const-string v0, "LIVE_GAMER"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    move-object v0, v1

    .line 136
    goto/16 :goto_1

    :cond_b
    const-string v0, "ICC"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    move-object v0, v1

    .line 138
    goto/16 :goto_1

    :cond_c
    const-string v0, "KUP_RETRIEVE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    move-object v0, v1

    .line 140
    goto/16 :goto_1

    .line 141
    :cond_d
    const-string v0, "https://mobile.inicis.com/smart/mobile/"

    goto/16 :goto_1

    .line 146
    :cond_e
    const-string v1, "QIWI"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    const-string v1, "LIVE_GAMER"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    const-string v1, "ICC"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    .line 147
    const-string v1, "KUP_RETRIEVE"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 148
    :cond_f
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "params url : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 149
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "params params : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 150
    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 152
    :cond_10
    iget-object v1, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->a:Landroid/webkit/WebView;

    const-string v2, "EUC-KR"

    invoke-static {v4, v2}, Lorg/apache/http/util/EncodingUtils;->getBytes(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/webkit/WebView;->postUrl(Ljava/lang/String;[B)V

    goto/16 :goto_2

    .line 156
    :cond_11
    invoke-virtual {v2}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v0

    .line 157
    const-string v1, "UnifiedPaymentPGActivity, params is null"

    invoke-static {v1}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 159
    if-nez v0, :cond_12

    .line 160
    const-string v0, "UnifiedPaymentPGActivity, data is null"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 161
    invoke-virtual {p0}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->finish()V

    .line 165
    :goto_4
    invoke-virtual {p0}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->finish()V

    goto/16 :goto_3

    .line 163
    :cond_12
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UnifiedPaymentPGActivity, no data"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    goto :goto_4
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 487
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 488
    const-string v0, "UnifiedPaymentPGActivity onDestroy"

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 489
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->d:Z

    .line 493
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    .line 558
    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    .line 559
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UnifiedPaymentPGPaymentActivity onKeyDown : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->d(Ljava/lang/String;)V

    .line 560
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->c:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->c:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 561
    const/4 v0, 0x0

    .line 583
    :goto_0
    return v0

    .line 563
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 564
    sget v1, Lcom/samsungosp/billingup/client/R$string;->ids_ph_header_attention:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 565
    sget v1, Lcom/samsungosp/billingup/client/R$string;->ids_ph_pop_the_current_purchase_will_be_cancelled:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 566
    sget v1, Lcom/samsungosp/billingup/client/R$string;->ids_ph_button_cancel:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 567
    sget v1, Lcom/samsungosp/billingup/client/R$string;->ids_ph_button_ok:I

    new-instance v2, Lcom/samsungosp/billingup/client/bh;

    invoke-direct {v2, p0}, Lcom/samsungosp/billingup/client/bh;-><init>(Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 578
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->c:Landroid/app/AlertDialog;

    .line 579
    iget-object v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->c:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 580
    const/4 v0, 0x1

    goto :goto_0

    .line 583
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 175
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 176
    iget-boolean v0, p0, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->mIsISPPage:Z

    if-eqz v0, :cond_0

    .line 177
    invoke-virtual {p0}, Lcom/samsungosp/billingup/client/UnifiedPaymentPGPaymentActivity;->finish()V

    .line 179
    :cond_0
    return-void
.end method
