.class public Lcom/samsungosp/billingup/client/requestparam/GiftCardData;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public appServiceID:Ljava/lang/String;

.field public country:Ljava/lang/String;

.field public deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

.field public language:Ljava/lang/String;

.field private platformCode:Ljava/lang/String;

.field public timeOffset:Ljava/lang/String;

.field public upServerURL:Ljava/lang/String;

.field public userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

.field public uxVersion:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    new-instance v0, Lcom/samsungosp/billingup/client/requestparam/GiftCardData$1;

    invoke-direct {v0}, Lcom/samsungosp/billingup/client/requestparam/GiftCardData$1;-><init>()V

    sput-object v0, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 68
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const-string v0, "A"

    iput-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->platformCode:Ljava/lang/String;

    .line 20
    return-void
.end method

.method public static readFromParcel(Landroid/os/Parcel;)Lcom/samsungosp/billingup/client/requestparam/GiftCardData;
    .locals 2

    .prologue
    .line 43
    new-instance v1, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;

    invoke-direct {v1}, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;-><init>()V

    .line 45
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->appServiceID:Ljava/lang/String;

    .line 46
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->country:Ljava/lang/String;

    .line 47
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->language:Ljava/lang/String;

    .line 48
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->upServerURL:Ljava/lang/String;

    .line 49
    const-class v0, Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    .line 50
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 49
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    iput-object v0, v1, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    .line 51
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->platformCode:Ljava/lang/String;

    .line 52
    const-class v0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    .line 53
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 52
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    iput-object v0, v1, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    .line 54
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->timeOffset:Ljava/lang/String;

    .line 55
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->uxVersion:Ljava/lang/String;

    .line 57
    return-object v1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 31
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->appServiceID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 32
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->country:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 33
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->language:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 34
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->upServerURL:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 35
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->userInfo:Lcom/samsungosp/billingup/client/requestparam/UserInfo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 36
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->platformCode:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->deviceInfo:Lcom/samsungosp/billingup/client/requestparam/DeviceInfo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 38
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->timeOffset:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lcom/samsungosp/billingup/client/requestparam/GiftCardData;->uxVersion:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 40
    return-void
.end method
