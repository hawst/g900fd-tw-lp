.class final Lcom/samsungosp/billingup/client/bl;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/samsungosp/billingup/client/bi;

.field private final synthetic b:Landroid/webkit/WebView;

.field private final synthetic c:Landroid/os/Handler;


# direct methods
.method constructor <init>(Lcom/samsungosp/billingup/client/bi;Landroid/webkit/WebView;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsungosp/billingup/client/bl;->a:Lcom/samsungosp/billingup/client/bi;

    iput-object p2, p0, Lcom/samsungosp/billingup/client/bl;->b:Landroid/webkit/WebView;

    iput-object p3, p0, Lcom/samsungosp/billingup/client/bl;->c:Landroid/os/Handler;

    .line 340
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .prologue
    .line 345
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SSLError AlertDialog cancel click which : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsungosp/billingup/client/util/UPLog;->v(Ljava/lang/String;)V

    .line 346
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bl;->b:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->stopLoading()V

    .line 347
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bl;->b:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V

    .line 348
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bl;->a:Lcom/samsungosp/billingup/client/bi;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/bi;->b(Lcom/samsungosp/billingup/client/bi;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_0

    .line 351
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bl;->a:Lcom/samsungosp/billingup/client/bi;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/bi;->b(Lcom/samsungosp/billingup/client/bi;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 352
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bl;->a:Lcom/samsungosp/billingup/client/bi;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/bi;->c(Lcom/samsungosp/billingup/client/bi;)V

    .line 353
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bl;->c:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 355
    return-void

    .line 349
    :cond_0
    iget-object v0, p0, Lcom/samsungosp/billingup/client/bl;->a:Lcom/samsungosp/billingup/client/bi;

    invoke-static {v0}, Lcom/samsungosp/billingup/client/bi;->b(Lcom/samsungosp/billingup/client/bi;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/webkit/SslErrorHandler;

    invoke-virtual {v0}, Landroid/webkit/SslErrorHandler;->cancel()V

    .line 348
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method
