.class final Lorg/apache/commons/codec/language/b;
.super Lorg/apache/commons/codec/language/a;
.source "ProGuard"


# instance fields
.field final synthetic d:Lorg/apache/commons/codec/language/ColognePhonetic;


# direct methods
.method public constructor <init>(Lorg/apache/commons/codec/language/ColognePhonetic;[C)V
    .locals 0

    .prologue
    .line 234
    iput-object p1, p0, Lorg/apache/commons/codec/language/b;->d:Lorg/apache/commons/codec/language/ColognePhonetic;

    .line 235
    invoke-direct {p0, p1, p2}, Lorg/apache/commons/codec/language/a;-><init>(Lorg/apache/commons/codec/language/ColognePhonetic;[C)V

    .line 236
    return-void
.end method

.method private e()I
    .locals 2

    .prologue
    .line 255
    iget-object v0, p0, Lorg/apache/commons/codec/language/b;->a:[C

    array-length v0, v0

    iget v1, p0, Lorg/apache/commons/codec/language/b;->b:I

    sub-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method protected final a(I)[C
    .locals 4

    .prologue
    .line 245
    new-array v0, p1, [C

    .line 246
    iget-object v1, p0, Lorg/apache/commons/codec/language/b;->a:[C

    iget-object v2, p0, Lorg/apache/commons/codec/language/b;->a:[C

    array-length v2, v2

    iget v3, p0, Lorg/apache/commons/codec/language/b;->b:I

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x0

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 247
    return-object v0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 239
    iget v0, p0, Lorg/apache/commons/codec/language/b;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/commons/codec/language/b;->b:I

    .line 240
    iget-object v0, p0, Lorg/apache/commons/codec/language/b;->a:[C

    invoke-direct {p0}, Lorg/apache/commons/codec/language/b;->e()I

    move-result v1

    const/16 v2, 0x53

    aput-char v2, v0, v1

    .line 241
    return-void
.end method

.method public final c()C
    .locals 2

    .prologue
    .line 251
    iget-object v0, p0, Lorg/apache/commons/codec/language/b;->a:[C

    invoke-direct {p0}, Lorg/apache/commons/codec/language/b;->e()I

    move-result v1

    aget-char v0, v0, v1

    return v0
.end method

.method public final d()C
    .locals 2

    .prologue
    .line 259
    invoke-virtual {p0}, Lorg/apache/commons/codec/language/b;->c()C

    move-result v0

    .line 260
    iget v1, p0, Lorg/apache/commons/codec/language/b;->b:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/commons/codec/language/b;->b:I

    .line 261
    return v0
.end method
