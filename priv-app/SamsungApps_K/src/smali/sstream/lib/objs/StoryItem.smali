.class public Lsstream/lib/objs/StoryItem;
.super Ljava/lang/Object;
.source "ProGuard"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Lsstream/lib/objs/StoryItem$StoryType;

.field private e:Lsstream/lib/objs/Author;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:J

.field private i:Lsstream/lib/objs/Image;

.field private j:I

.field private k:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lsstream/lib/objs/StoryItem$StoryType;Lsstream/lib/objs/Author;Lsstream/lib/objs/Image;JILjava/lang/String;)V
    .locals 3

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 63
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 65
    :cond_1
    const-string v0, "samsung.media"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "samsung.personal"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "samsung.here.and.now"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "flipboard.temporary.will.change.later.news"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "flipboard.temporary.will.change.later.social"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 66
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Not a valid streamId: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 68
    :cond_2
    iput-object p1, p0, Lsstream/lib/objs/StoryItem;->a:Ljava/lang/String;

    .line 69
    iput-object p2, p0, Lsstream/lib/objs/StoryItem;->b:Ljava/lang/String;

    .line 70
    iput-object p3, p0, Lsstream/lib/objs/StoryItem;->c:Ljava/lang/String;

    .line 71
    iput-object p7, p0, Lsstream/lib/objs/StoryItem;->e:Lsstream/lib/objs/Author;

    .line 72
    iput-object p4, p0, Lsstream/lib/objs/StoryItem;->f:Ljava/lang/String;

    .line 73
    iput-object p5, p0, Lsstream/lib/objs/StoryItem;->g:Ljava/lang/String;

    .line 74
    if-nez p6, :cond_3

    sget-object p6, Lsstream/lib/objs/StoryItem$StoryType;->ARTICLE:Lsstream/lib/objs/StoryItem$StoryType;

    :cond_3
    iput-object p6, p0, Lsstream/lib/objs/StoryItem;->d:Lsstream/lib/objs/StoryItem$StoryType;

    .line 75
    iput-wide p9, p0, Lsstream/lib/objs/StoryItem;->h:J

    .line 76
    iput-object p8, p0, Lsstream/lib/objs/StoryItem;->i:Lsstream/lib/objs/Image;

    .line 77
    iput p11, p0, Lsstream/lib/objs/StoryItem;->j:I

    .line 78
    iput-object p12, p0, Lsstream/lib/objs/StoryItem;->k:Ljava/lang/String;

    .line 79
    return-void
.end method

.method public static getTypeFromString(Ljava/lang/String;)Lsstream/lib/objs/StoryItem$StoryType;
    .locals 1

    .prologue
    .line 158
    sget-object v0, Lsstream/lib/objs/StoryItem$StoryType;->CALENDAR:Lsstream/lib/objs/StoryItem$StoryType;

    invoke-virtual {v0}, Lsstream/lib/objs/StoryItem$StoryType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    sget-object v0, Lsstream/lib/objs/StoryItem$StoryType;->CALENDAR:Lsstream/lib/objs/StoryItem$StoryType;

    .line 165
    :goto_0
    return-object v0

    .line 160
    :cond_0
    sget-object v0, Lsstream/lib/objs/StoryItem$StoryType;->CALL:Lsstream/lib/objs/StoryItem$StoryType;

    invoke-virtual {v0}, Lsstream/lib/objs/StoryItem$StoryType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 161
    sget-object v0, Lsstream/lib/objs/StoryItem$StoryType;->CALL:Lsstream/lib/objs/StoryItem$StoryType;

    goto :goto_0

    .line 162
    :cond_1
    sget-object v0, Lsstream/lib/objs/StoryItem$StoryType;->MESSAGE:Lsstream/lib/objs/StoryItem$StoryType;

    invoke-virtual {v0}, Lsstream/lib/objs/StoryItem$StoryType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 163
    sget-object v0, Lsstream/lib/objs/StoryItem$StoryType;->MESSAGE:Lsstream/lib/objs/StoryItem$StoryType;

    goto :goto_0

    .line 165
    :cond_2
    sget-object v0, Lsstream/lib/objs/StoryItem$StoryType;->MESSAGE:Lsstream/lib/objs/StoryItem$StoryType;

    goto :goto_0
.end method


# virtual methods
.method public getAppPackage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lsstream/lib/objs/StoryItem;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getAuthor()Lsstream/lib/objs/Author;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lsstream/lib/objs/StoryItem;->e:Lsstream/lib/objs/Author;

    return-object v0
.end method

.method public getBody()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lsstream/lib/objs/StoryItem;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lsstream/lib/objs/StoryItem;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lsstream/lib/objs/StoryItem;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getImage()Lsstream/lib/objs/Image;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lsstream/lib/objs/StoryItem;->i:Lsstream/lib/objs/Image;

    return-object v0
.end method

.method public getMore()I
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Lsstream/lib/objs/StoryItem;->j:I

    return v0
.end method

.method public getSource()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lsstream/lib/objs/StoryItem;->k:Ljava/lang/String;

    return-object v0
.end method

.method public getStreamId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lsstream/lib/objs/StoryItem;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getTimestamp()J
    .locals 2

    .prologue
    .line 103
    iget-wide v0, p0, Lsstream/lib/objs/StoryItem;->h:J

    return-wide v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lsstream/lib/objs/StoryItem;->f:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lsstream/lib/objs/StoryItem$StoryType;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lsstream/lib/objs/StoryItem;->d:Lsstream/lib/objs/StoryItem$StoryType;

    return-object v0
.end method

.method public isValid()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 138
    iget-object v1, p0, Lsstream/lib/objs/StoryItem;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lsstream/lib/objs/StoryItem;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lsstream/lib/objs/StoryItem;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lsstream/lib/objs/StoryItem;->e:Lsstream/lib/objs/Author;

    if-nez v1, :cond_1

    .line 144
    :cond_0
    :goto_0
    return v0

    .line 141
    :cond_1
    iget-object v1, p0, Lsstream/lib/objs/StoryItem;->b:Ljava/lang/String;

    const-string v2, "samsung.media"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lsstream/lib/objs/StoryItem;->b:Ljava/lang/String;

    const-string v2, "samsung.personal"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lsstream/lib/objs/StoryItem;->b:Ljava/lang/String;

    const-string v2, "samsung.here.and.now"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lsstream/lib/objs/StoryItem;->b:Ljava/lang/String;

    const-string v2, "flipboard.temporary.will.change.later.news"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lsstream/lib/objs/StoryItem;->b:Ljava/lang/String;

    const-string v2, "flipboard.temporary.will.change.later.social"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 144
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 149
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 150
    iget-object v0, p0, Lsstream/lib/objs/StoryItem;->f:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    const-string v0, " - "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    iget-object v0, p0, Lsstream/lib/objs/StoryItem;->i:Lsstream/lib/objs/Image;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lsstream/lib/objs/StoryItem;->i:Lsstream/lib/objs/Image;

    invoke-virtual {v0}, Lsstream/lib/objs/Image;->getLocation()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 152
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
