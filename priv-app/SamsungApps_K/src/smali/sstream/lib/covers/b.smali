.class final Lsstream/lib/covers/b;
.super Ljava/lang/Object;
.source "ProGuard"

# interfaces
.implements Landroid/content/ServiceConnection;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 5

    .prologue
    .line 143
    sget-object v1, Lsstream/lib/covers/a;->c:Ljava/util/Map;

    monitor-enter v1

    .line 147
    :try_start_0
    new-instance v0, Landroid/os/Messenger;

    invoke-direct {v0, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    sput-object v0, Lsstream/lib/covers/a;->a:Landroid/os/Messenger;

    .line 151
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 152
    const-string v2, "library_version"

    const-string v3, "1.2"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    const-string v2, "app_package_name"

    sget-object v3, Lsstream/lib/covers/a;->d:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    const/4 v2, 0x1

    invoke-static {v2, v0}, Lsstream/lib/covers/a;->a(ILandroid/os/Bundle;)V

    .line 155
    sget-object v0, Lsstream/lib/covers/a;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsstream/lib/covers/d;

    .line 156
    iget-boolean v3, v0, Lsstream/lib/covers/d;->c:Z

    if-nez v3, :cond_0

    .line 157
    const/4 v3, 0x3

    iget-object v4, v0, Lsstream/lib/covers/d;->a:Landroid/os/Bundle;

    invoke-static {v3, v4}, Lsstream/lib/covers/a;->a(ILandroid/os/Bundle;)V

    .line 158
    const/4 v3, 0x1

    iput-boolean v3, v0, Lsstream/lib/covers/d;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 161
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1

    .prologue
    .line 168
    const/4 v0, 0x0

    sput-object v0, Lsstream/lib/covers/a;->a:Landroid/os/Messenger;

    .line 170
    invoke-static {}, Lsstream/lib/covers/a;->b()Z

    .line 171
    return-void
.end method
