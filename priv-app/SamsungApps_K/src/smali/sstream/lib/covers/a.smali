.class final Lsstream/lib/covers/a;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field static a:Landroid/os/Messenger;

.field static b:Z

.field static c:Ljava/util/Map;

.field static d:Landroid/content/Context;

.field static e:Lsstream/lib/covers/StreamUpdateListener;

.field static f:Landroid/os/Messenger;

.field private static g:Landroid/content/ServiceConnection;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 47
    sput-object v0, Lsstream/lib/covers/a;->a:Landroid/os/Messenger;

    .line 54
    sput-object v0, Lsstream/lib/covers/a;->e:Lsstream/lib/covers/StreamUpdateListener;

    .line 134
    new-instance v0, Landroid/os/Messenger;

    new-instance v1, Lsstream/lib/covers/c;

    invoke-direct {v1}, Lsstream/lib/covers/c;-><init>()V

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    sput-object v0, Lsstream/lib/covers/a;->f:Landroid/os/Messenger;

    .line 139
    new-instance v0, Lsstream/lib/covers/b;

    invoke-direct {v0}, Lsstream/lib/covers/b;-><init>()V

    sput-object v0, Lsstream/lib/covers/a;->g:Landroid/content/ServiceConnection;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    sput-object p1, Lsstream/lib/covers/a;->d:Landroid/content/Context;

    .line 74
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    sput-object v0, Lsstream/lib/covers/a;->c:Ljava/util/Map;

    .line 75
    return-void
.end method

.method static synthetic a(ILandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 27
    invoke-static {p0, p1}, Lsstream/lib/covers/a;->b(ILandroid/os/Bundle;)V

    return-void
.end method

.method static a()Z
    .locals 4

    .prologue
    .line 180
    new-instance v0, Landroid/content/Intent;

    const-string v1, "sstream.flipboard.service.MessengerService"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 181
    sget-object v1, Lsstream/lib/covers/a;->d:Landroid/content/Context;

    sget-object v2, Lsstream/lib/covers/a;->g:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    .line 182
    sput-boolean v0, Lsstream/lib/covers/a;->b:Z

    return v0
.end method

.method private static b(ILandroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 216
    sget-boolean v0, Lsstream/lib/covers/a;->b:Z

    if-nez v0, :cond_1

    .line 235
    :cond_0
    :goto_0
    return-void

    .line 220
    :cond_1
    sget-object v0, Lsstream/lib/covers/a;->a:Landroid/os/Messenger;

    if-eqz v0, :cond_0

    .line 222
    const/4 v0, 0x0

    :try_start_0
    invoke-static {v0, p0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 223
    sget-object v1, Lsstream/lib/covers/a;->f:Landroid/os/Messenger;

    iput-object v1, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 225
    if-eqz p1, :cond_2

    .line 226
    invoke-virtual {v0, p1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 229
    :cond_2
    sget-object v1, Lsstream/lib/covers/a;->a:Landroid/os/Messenger;

    invoke-virtual {v1, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static b()Z
    .locals 4

    .prologue
    .line 191
    sget-object v1, Lsstream/lib/covers/a;->c:Ljava/util/Map;

    monitor-enter v1

    .line 192
    :try_start_0
    sget-boolean v0, Lsstream/lib/covers/a;->b:Z

    if-eqz v0, :cond_0

    .line 195
    const/4 v0, 0x2

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lsstream/lib/covers/a;->b(ILandroid/os/Bundle;)V

    .line 197
    sget-object v0, Lsstream/lib/covers/a;->d:Landroid/content/Context;

    sget-object v2, Lsstream/lib/covers/a;->g:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 198
    const/4 v0, 0x0

    sput-boolean v0, Lsstream/lib/covers/a;->b:Z

    .line 201
    :cond_0
    sget-object v0, Lsstream/lib/covers/a;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsstream/lib/covers/d;

    .line 202
    iget-object v0, v0, Lsstream/lib/covers/d;->b:Lsstream/lib/covers/StreamUpdateListener;

    sget-object v3, Lsstream/lib/covers/StreamUpdateListener$ErrorMessage;->DISCONNECTED_FROM_SSTREAM:Lsstream/lib/covers/StreamUpdateListener$ErrorMessage;

    invoke-interface {v0, v3}, Lsstream/lib/covers/StreamUpdateListener;->onFailure(Lsstream/lib/covers/StreamUpdateListener$ErrorMessage;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 207
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 204
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    sput-object v0, Lsstream/lib/covers/a;->a:Landroid/os/Messenger;

    .line 205
    sget-object v0, Lsstream/lib/covers/a;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 206
    sget-boolean v0, Lsstream/lib/covers/a;->b:Z

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return v0
.end method


# virtual methods
.method final a(Ljava/lang/String;Lsstream/lib/covers/StreamUpdateListener;)V
    .locals 5

    .prologue
    .line 246
    sget-object v1, Lsstream/lib/covers/a;->c:Ljava/util/Map;

    monitor-enter v1

    .line 247
    :try_start_0
    sget-object v0, Lsstream/lib/covers/a;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 248
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 250
    const-string v2, "stream_id"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    new-instance v2, Lsstream/lib/covers/d;

    invoke-direct {v2, p0, v0, p2}, Lsstream/lib/covers/d;-><init>(Lsstream/lib/covers/a;Landroid/os/Bundle;Lsstream/lib/covers/StreamUpdateListener;)V

    .line 254
    sget-object v0, Lsstream/lib/covers/a;->c:Ljava/util/Map;

    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 256
    sget-object v0, Lsstream/lib/covers/a;->a:Landroid/os/Messenger;

    if-nez v0, :cond_1

    .line 257
    invoke-static {}, Lsstream/lib/covers/a;->a()Z

    .line 258
    sget-boolean v0, Lsstream/lib/covers/a;->b:Z

    if-eqz v0, :cond_0

    .line 259
    const-string v0, "CoverLockLibrary"

    const-string v2, "Binding"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    .line 263
    :cond_1
    sget-object v0, Lsstream/lib/covers/a;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsstream/lib/covers/d;

    .line 264
    iget-boolean v3, v0, Lsstream/lib/covers/d;->c:Z

    if-nez v3, :cond_2

    .line 265
    const/4 v3, 0x3

    iget-object v4, v0, Lsstream/lib/covers/d;->a:Landroid/os/Bundle;

    invoke-static {v3, v4}, Lsstream/lib/covers/a;->b(ILandroid/os/Bundle;)V

    .line 266
    const/4 v3, 0x1

    iput-boolean v3, v0, Lsstream/lib/covers/d;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 274
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 272
    :cond_3
    :try_start_1
    sget-object v0, Lsstream/lib/covers/StreamUpdateListener$ErrorMessage;->DUPLICATE_REQUEST:Lsstream/lib/covers/StreamUpdateListener$ErrorMessage;

    invoke-interface {p2, v0}, Lsstream/lib/covers/StreamUpdateListener;->onFailure(Lsstream/lib/covers/StreamUpdateListener$ErrorMessage;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
