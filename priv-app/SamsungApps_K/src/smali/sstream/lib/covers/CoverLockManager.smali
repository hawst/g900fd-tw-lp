.class public Lsstream/lib/covers/CoverLockManager;
.super Ljava/lang/Object;
.source "ProGuard"


# static fields
.field private static final a:Ljava/util/List;

.field private static final b:Lsstream/lib/covers/CoverLockManager;

.field private static c:Lsstream/lib/covers/a;

.field private static d:Landroid/content/Context;

.field private static e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lsstream/lib/covers/CoverLockManager;->a:Ljava/util/List;

    .line 26
    new-instance v0, Lsstream/lib/covers/CoverLockManager;

    invoke-direct {v0}, Lsstream/lib/covers/CoverLockManager;-><init>()V

    sput-object v0, Lsstream/lib/covers/CoverLockManager;->b:Lsstream/lib/covers/CoverLockManager;

    .line 32
    const/4 v0, 0x0

    sput-boolean v0, Lsstream/lib/covers/CoverLockManager;->e:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    return-void
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 150
    const/4 v0, 0x1

    .line 151
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 152
    const/4 v0, 0x0

    .line 154
    :cond_0
    return v0
.end method

.method public static getInstance(Landroid/content/Context;)Lsstream/lib/covers/CoverLockManager;
    .locals 2

    .prologue
    .line 46
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lsstream/lib/covers/CoverLockManager;->d:Landroid/content/Context;

    .line 47
    sget-object v0, Lsstream/lib/covers/CoverLockManager;->c:Lsstream/lib/covers/a;

    if-eqz v0, :cond_0

    .line 48
    sget-object v0, Lsstream/lib/covers/CoverLockManager;->b:Lsstream/lib/covers/CoverLockManager;

    .line 52
    :goto_0
    return-object v0

    .line 50
    :cond_0
    new-instance v0, Lsstream/lib/covers/a;

    sget-object v1, Lsstream/lib/covers/CoverLockManager;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Lsstream/lib/covers/a;-><init>(Landroid/content/Context;)V

    sput-object v0, Lsstream/lib/covers/CoverLockManager;->c:Lsstream/lib/covers/a;

    .line 52
    sget-object v0, Lsstream/lib/covers/CoverLockManager;->b:Lsstream/lib/covers/CoverLockManager;

    goto :goto_0
.end method


# virtual methods
.method public getCoverItems(Landroid/content/Context;Ljava/lang/String;I)Ljava/util/List;
    .locals 23

    .prologue
    .line 69
    if-eqz p1, :cond_8

    if-eqz p2, :cond_8

    const-string v3, "flipboard.temporary.will.change.later.news"

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "flipboard.temporary.will.change.later.social"

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 70
    :cond_0
    if-nez p3, :cond_1

    const/16 p3, 0x3

    .line 71
    :cond_1
    if-eqz p2, :cond_7

    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lsstream/lib/constants/StreamProviderConstants;->STORY_URI:Landroid/net/Uri;

    sget-object v5, Lsstream/lib/constants/StreamProviderConstants;->STORY_PROJECTION:[Ljava/lang/String;

    const-string v6, "stream_id IN(?)"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object p2, v7, v8

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "time_stamp DESC LIMIT "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p3

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_6

    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToFirst()Z

    :cond_2
    const/4 v3, 0x0

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v3, 0x1

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v3, 0x2

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v3, 0x3

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v3, 0x4

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v3, 0x5

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v3, 0x6

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    const/4 v3, 0x7

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    const/16 v3, 0x8

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    const/16 v3, 0x9

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    const/16 v3, 0xa

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    const/16 v3, 0xb

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v21

    const/16 v3, 0xc

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    const/16 v3, 0xe

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    const/16 v3, 0xf

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static/range {v19 .. v19}, Lsstream/lib/covers/CoverLockManager;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    const/4 v3, 0x0

    invoke-static/range {v20 .. v20}, Lsstream/lib/covers/CoverLockManager;->a(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_3

    new-instance v3, Lsstream/lib/objs/Image;

    const/4 v10, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v22

    move/from16 v2, v21

    invoke-direct {v3, v0, v1, v2, v10}, Lsstream/lib/objs/Image;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    :cond_3
    new-instance v10, Lsstream/lib/objs/Author;

    move-object/from16 v0, v19

    invoke-direct {v10, v0, v3}, Lsstream/lib/objs/Author;-><init>(Ljava/lang/String;Lsstream/lib/objs/Image;)V

    :cond_4
    invoke-static {v12}, Lsstream/lib/covers/CoverLockManager;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_5

    new-instance v11, Lsstream/lib/objs/Image;

    const/4 v3, 0x0

    move/from16 v0, v18

    invoke-direct {v11, v12, v0, v13, v3}, Lsstream/lib/objs/Image;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    :cond_5
    new-instance v3, Ljava/sql/Timestamp;

    const/16 v12, 0xd

    move-object/from16 v0, v17

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    invoke-direct {v3, v12, v13}, Ljava/sql/Timestamp;-><init>(J)V

    invoke-virtual {v3}, Ljava/sql/Timestamp;->getTime()J

    move-result-wide v12

    new-instance v3, Lsstream/lib/objs/StoryItem;

    invoke-static {v9}, Lsstream/lib/objs/StoryItem;->getTypeFromString(Ljava/lang/String;)Lsstream/lib/objs/StoryItem$StoryType;

    move-result-object v9

    invoke-direct/range {v3 .. v15}, Lsstream/lib/objs/StoryItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lsstream/lib/objs/StoryItem$StoryType;Lsstream/lib/objs/Author;Lsstream/lib/objs/Image;JILjava/lang/String;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_2

    :cond_6
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    move-object/from16 v3, v16

    :goto_0
    return-object v3

    :cond_7
    sget-object v3, Lsstream/lib/covers/CoverLockManager;->a:Ljava/util/List;

    goto :goto_0

    .line 73
    :cond_8
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Invalid Stream Id: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public isSstreamAvailable()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 179
    sget-boolean v1, Lsstream/lib/covers/CoverLockManager;->e:Z

    if-nez v1, :cond_0

    .line 180
    sget-object v1, Lsstream/lib/covers/CoverLockManager;->c:Lsstream/lib/covers/a;

    invoke-static {}, Lsstream/lib/covers/a;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 181
    invoke-static {}, Lsstream/lib/covers/a;->b()Z

    .line 183
    sput-boolean v0, Lsstream/lib/covers/CoverLockManager;->e:Z

    .line 187
    :cond_0
    :goto_0
    return v0

    .line 185
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updateItemsFromSever(Landroid/content/Context;Ljava/lang/String;Lsstream/lib/covers/StreamUpdateListener;)V
    .locals 3

    .prologue
    .line 166
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    const-string v0, "flipboard.temporary.will.change.later.news"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "flipboard.temporary.will.change.later.social"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 167
    :cond_0
    sget-object v0, Lsstream/lib/covers/CoverLockManager;->c:Lsstream/lib/covers/a;

    invoke-virtual {v0, p2, p3}, Lsstream/lib/covers/a;->a(Ljava/lang/String;Lsstream/lib/covers/StreamUpdateListener;)V

    return-void

    .line 169
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid Stream Id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
