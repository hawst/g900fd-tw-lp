.class public Lcom/google/android/partnersetup/LauncherConfigService;
.super Landroid/app/IntentService;
.source "LauncherConfigService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/partnersetup/LauncherConfigService$MyBinder;
    }
.end annotation


# instance fields
.field private final mBinder:Landroid/os/IBinder;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    const-string v0, "GooglePartnerSetup"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 18
    new-instance v0, Lcom/google/android/partnersetup/LauncherConfigService$MyBinder;

    invoke-direct {v0, p0}, Lcom/google/android/partnersetup/LauncherConfigService$MyBinder;-><init>(Lcom/google/android/partnersetup/LauncherConfigService;)V

    iput-object v0, p0, Lcom/google/android/partnersetup/LauncherConfigService;->mBinder:Landroid/os/IBinder;

    .line 28
    return-void
.end method

.method public static startLauncherConfigService(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/partnersetup/LauncherConfigService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 32
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 33
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/partnersetup/LauncherConfigService;->mBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 42
    const-string v1, "GooglePartnerSetup"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 43
    const-string v1, "GooglePartnerSetup"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "LauncherConfigService: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    :cond_0
    new-instance v0, Lcom/google/android/partnersetup/LauncherConfig;

    invoke-direct {v0, p0}, Lcom/google/android/partnersetup/LauncherConfig;-><init>(Landroid/content/Context;)V

    .line 47
    .local v0, "cfg":Lcom/google/android/partnersetup/LauncherConfig;
    invoke-virtual {v0}, Lcom/google/android/partnersetup/LauncherConfig;->sendPreloadWorkspaceBroadcast()V

    .line 48
    return-void
.end method
