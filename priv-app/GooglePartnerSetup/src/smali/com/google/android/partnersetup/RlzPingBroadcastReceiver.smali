.class public Lcom/google/android/partnersetup/RlzPingBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "RlzPingBroadcastReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 29
    const-string v0, "GooglePartnerSetup"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 30
    const-string v0, "GooglePartnerSetup"

    const-string v1, "RlzPingBroadcastReceiver.onReceive"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 32
    :cond_0
    invoke-static {p1, p2}, Lcom/google/android/partnersetup/RlzPingIntentService;->startRlzPingService(Landroid/content/Context;Landroid/content/Intent;)V

    .line 33
    return-void
.end method
