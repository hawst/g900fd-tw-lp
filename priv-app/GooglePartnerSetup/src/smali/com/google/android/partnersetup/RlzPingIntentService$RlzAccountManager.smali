.class Lcom/google/android/partnersetup/RlzPingIntentService$RlzAccountManager;
.super Ljava/lang/Object;
.source "RlzPingIntentService.java"

# interfaces
.implements Lcom/google/android/partnersetup/RlzPingIntentService$RlzAccountManagerInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/partnersetup/RlzPingIntentService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RlzAccountManager"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "c"    # Landroid/content/Context;

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p1, p0, Lcom/google/android/partnersetup/RlzPingIntentService$RlzAccountManager;->mContext:Landroid/content/Context;

    .line 66
    return-void
.end method


# virtual methods
.method public getAccounts()[Landroid/accounts/Account;
    .locals 2

    .prologue
    .line 69
    iget-object v1, p0, Lcom/google/android/partnersetup/RlzPingIntentService$RlzAccountManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 70
    .local v0, "am":Landroid/accounts/AccountManager;
    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    return-object v1
.end method
