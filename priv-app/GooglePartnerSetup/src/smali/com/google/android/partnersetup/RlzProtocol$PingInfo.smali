.class public final Lcom/google/android/partnersetup/RlzProtocol$PingInfo;
.super Ljava/lang/Object;
.source "RlzProtocol.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/partnersetup/RlzProtocol;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PingInfo"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/partnersetup/RlzProtocol$PingInfo$RLZ;,
        Lcom/google/android/partnersetup/RlzProtocol$PingInfo$Event;
    }
.end annotation


# direct methods
.method public static final getResourceIdForEvent(Ljava/lang/String;)I
    .locals 1
    .param p0, "eventType"    # Ljava/lang/String;

    .prologue
    .line 358
    const-string v0, "I"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 359
    const v0, 0x7f03002d

    .line 363
    :goto_0
    return v0

    .line 360
    :cond_0
    const-string v0, "F"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 361
    const v0, 0x7f03002e

    goto :goto_0

    .line 363
    :cond_1
    const v0, 0x7f030003

    goto :goto_0
.end method

.method public static final rlzStatusOfChar(Ljava/lang/String;)I
    .locals 2
    .param p0, "eventType"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 371
    const-string v1, "I"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 377
    :cond_0
    :goto_0
    return v0

    .line 373
    :cond_1
    const-string v1, "F"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 374
    const/4 v0, 0x1

    goto :goto_0
.end method
