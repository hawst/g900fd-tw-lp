.class public Lcom/google/android/partnersetup/AppHider;
.super Ljava/lang/Object;
.source "AppHider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/partnersetup/AppHider$1;,
        Lcom/google/android/partnersetup/AppHider$BadGservicesValue;
    }
.end annotation


# static fields
.field static final DEFAULT_HIDDEN_APPS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final context:Landroid/content/Context;

.field private final prefs:Lcom/google/android/partnersetup/ApplicationHidingPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 76
    const-string v0, "com.google.earth"

    invoke-static {v0}, Lcom/google/common/collect/ImmutableSet;->of(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    sput-object v0, Lcom/google/android/partnersetup/AppHider;->DEFAULT_HIDDEN_APPS:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 86
    new-instance v0, Lcom/google/android/partnersetup/ApplicationHidingPreferences;

    invoke-direct {v0, p1}, Lcom/google/android/partnersetup/ApplicationHidingPreferences;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1, v0}, Lcom/google/android/partnersetup/AppHider;-><init>(Landroid/content/Context;Lcom/google/android/partnersetup/ApplicationHidingPreferences;)V

    .line 87
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/google/android/partnersetup/ApplicationHidingPreferences;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "prefs"    # Lcom/google/android/partnersetup/ApplicationHidingPreferences;

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    iput-object p1, p0, Lcom/google/android/partnersetup/AppHider;->context:Landroid/content/Context;

    .line 92
    iput-object p2, p0, Lcom/google/android/partnersetup/AppHider;->prefs:Lcom/google/android/partnersetup/ApplicationHidingPreferences;

    .line 93
    return-void
.end method

.method private extractEnabled(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "enabledString"    # Ljava/lang/String;
    .param p2, "clause"    # Ljava/lang/String;
    .param p3, "gserviceKey"    # Ljava/lang/String;
    .param p4, "gserviceValue"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/partnersetup/AppHider$BadGservicesValue;
        }
    .end annotation

    .prologue
    .line 290
    const-string v0, "enabled"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 291
    const/4 v0, 0x1

    .line 293
    :goto_0
    return v0

    .line 292
    :cond_0
    const-string v0, "disabled"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 293
    const/4 v0, 0x0

    goto :goto_0

    .line 295
    :cond_1
    new-instance v0, Lcom/google/android/partnersetup/AppHider$BadGservicesValue;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad enabled/disabled in clause \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, p3, p4}, Lcom/google/android/partnersetup/AppHider;->gserviceToString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/partnersetup/AppHider$BadGservicesValue;-><init>(Ljava/lang/String;Lcom/google/android/partnersetup/AppHider$1;)V

    throw v0
.end method

.method private extractMinVersion(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 4
    .param p1, "minVersionString"    # Ljava/lang/String;
    .param p2, "clause"    # Ljava/lang/String;
    .param p3, "gserviceKey"    # Ljava/lang/String;
    .param p4, "gserviceValue"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/partnersetup/AppHider$BadGservicesValue;
        }
    .end annotation

    .prologue
    .line 281
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    return v1

    .line 282
    :catch_0
    move-exception v0

    .line 283
    .local v0, "e":Ljava/lang/NumberFormatException;
    new-instance v1, Lcom/google/android/partnersetup/AppHider$BadGservicesValue;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Non-numeric minVersion in clause \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0, p3, p4}, Lcom/google/android/partnersetup/AppHider;->gserviceToString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/android/partnersetup/AppHider$BadGservicesValue;-><init>(Ljava/lang/String;Lcom/google/android/partnersetup/AppHider$1;)V

    throw v1
.end method

.method private gserviceToString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "gserviceKey"    # Ljava/lang/String;
    .param p2, "gserviceValue"    # Ljava/lang/String;

    .prologue
    .line 304
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Gservice ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\')"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method getVisibilitiesAndOverrides(IILjava/lang/String;Ljava/lang/String;)Landroid/util/Pair;
    .locals 6
    .param p1, "runCount"    # I
    .param p2, "lockCount"    # I
    .param p3, "homeCountry"    # Ljava/lang/String;
    .param p4, "gservicesCountry"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x3

    .line 98
    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v3

    .line 102
    .local v3, "visibilities":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Boolean;>;"
    if-lt p1, p2, :cond_1

    invoke-static {p3, p4}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 103
    const-string v4, "GooglePartnerSetup"

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 104
    const-string v4, "GooglePartnerSetup"

    const-string v5, "Locked into different country, only updating visibilities for Market."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    :cond_0
    const-string v4, "com.android.vending"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    iget-object v4, p0, Lcom/google/android/partnersetup/AppHider;->prefs:Lcom/google/android/partnersetup/ApplicationHidingPreferences;

    const-string v5, "gms_disable:com.android.vending"

    invoke-virtual {v4, v5}, Lcom/google/android/partnersetup/ApplicationHidingPreferences;->getGservicesStringsByPrefix(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v2

    .line 121
    .local v2, "overrides":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :goto_0
    invoke-static {v3, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    return-object v4

    .line 111
    .end local v2    # "overrides":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_1
    const-string v4, "GooglePartnerSetup"

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 112
    const-string v4, "GooglePartnerSetup"

    const-string v5, "Either we have not reached lock count or we are in home country, updating all visibilities."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    :cond_2
    sget-object v4, Lcom/google/android/partnersetup/AppHider;->DEFAULT_HIDDEN_APPS:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 116
    .local v0, "disabledPackage":Ljava/lang/String;
    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 118
    .end local v0    # "disabledPackage":Ljava/lang/String;
    :cond_3
    iget-object v4, p0, Lcom/google/android/partnersetup/AppHider;->prefs:Lcom/google/android/partnersetup/ApplicationHidingPreferences;

    const-string v5, "gms_disable:com."

    invoke-virtual {v4, v5}, Lcom/google/android/partnersetup/ApplicationHidingPreferences;->getGservicesStringsByPrefix(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v2

    .restart local v2    # "overrides":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    goto :goto_0
.end method

.method public setAppVisibilities()V
    .locals 24

    .prologue
    .line 128
    const-string v21, "GooglePartnerSetup"

    const/16 v22, 0x3

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v21

    if-eqz v21, :cond_0

    .line 129
    const-string v21, "GooglePartnerSetup"

    const-string v22, "Running setAppVisibilities"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/partnersetup/AppHider;->prefs:Lcom/google/android/partnersetup/ApplicationHidingPreferences;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/partnersetup/ApplicationHidingPreferences;->getHidingRunCount()I

    move-result v17

    .line 133
    .local v17, "runCount":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/partnersetup/AppHider;->prefs:Lcom/google/android/partnersetup/ApplicationHidingPreferences;

    move-object/from16 v21, v0

    const-string v22, "gms_disable:lock_count"

    const/16 v23, 0x4

    invoke-virtual/range {v21 .. v23}, Lcom/google/android/partnersetup/ApplicationHidingPreferences;->getGservicesInt(Ljava/lang/String;I)I

    move-result v12

    .line 136
    .local v12, "lockCount":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/partnersetup/AppHider;->prefs:Lcom/google/android/partnersetup/ApplicationHidingPreferences;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/partnersetup/ApplicationHidingPreferences;->getHomeCountry()Ljava/lang/String;

    move-result-object v7

    .line 137
    .local v7, "homeCountry":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/partnersetup/AppHider;->prefs:Lcom/google/android/partnersetup/ApplicationHidingPreferences;

    move-object/from16 v21, v0

    const-string v22, "device_country"

    const/16 v23, 0x0

    invoke-virtual/range {v21 .. v23}, Lcom/google/android/partnersetup/ApplicationHidingPreferences;->getGservicesString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 140
    .local v6, "gservicesCountry":Ljava/lang/String;
    if-nez v7, :cond_1

    .line 141
    const/16 v17, 0x0

    .line 144
    :cond_1
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1, v12, v7, v6}, Lcom/google/android/partnersetup/AppHider;->getVisibilitiesAndOverrides(IILjava/lang/String;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v19

    .line 146
    .local v19, "visibilitiesAndOverrides":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Boolean;>;Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    move-object/from16 v0, v19

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v18, v0

    check-cast v18, Ljava/util/Map;

    .line 147
    .local v18, "visibilities":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Boolean;>;"
    move-object/from16 v0, v19

    iget-object v14, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v14, Ljava/util/Map;

    .line 149
    .local v14, "overrides":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    move/from16 v0, v17

    if-ge v0, v12, :cond_2

    .line 151
    if-eqz v6, :cond_5

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_5

    .line 152
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/partnersetup/AppHider;->prefs:Lcom/google/android/partnersetup/ApplicationHidingPreferences;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/partnersetup/ApplicationHidingPreferences;->incrementHidingRunCount()V

    .line 156
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/partnersetup/AppHider;->prefs:Lcom/google/android/partnersetup/ApplicationHidingPreferences;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Lcom/google/android/partnersetup/ApplicationHidingPreferences;->setHomeCountry(Ljava/lang/String;)Z

    .line 159
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/partnersetup/AppHider;->context:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v15

    .line 162
    .local v15, "packageManager":Landroid/content/pm/PackageManager;
    invoke-interface/range {v18 .. v18}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/google/common/collect/Sets;->newHashSet(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_6

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    .line 163
    .local v16, "packageName":Ljava/lang/String;
    invoke-static/range {v15 .. v16}, Lcom/google/android/partnersetup/GooglePartnerSetup;->getSystemPackageInfo(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v9

    .line 164
    .local v9, "info":Landroid/content/pm/PackageInfo;
    if-nez v9, :cond_3

    .line 165
    const-string v21, "GooglePartnerSetup"

    const/16 v22, 0x3

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v21

    if-eqz v21, :cond_4

    .line 166
    const-string v21, "GooglePartnerSetup"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Package "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " not installed, can\'t change its visibility"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    :cond_4
    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 154
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v9    # "info":Landroid/content/pm/PackageInfo;
    .end local v15    # "packageManager":Landroid/content/pm/PackageManager;
    .end local v16    # "packageName":Ljava/lang/String;
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/partnersetup/AppHider;->prefs:Lcom/google/android/partnersetup/ApplicationHidingPreferences;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    invoke-virtual/range {v21 .. v22}, Lcom/google/android/partnersetup/ApplicationHidingPreferences;->setHidingRunCount(I)Z

    goto :goto_0

    .line 175
    .restart local v8    # "i$":Ljava/util/Iterator;
    .restart local v15    # "packageManager":Landroid/content/pm/PackageManager;
    :cond_6
    invoke-interface {v14}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_7
    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_9

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/util/Map$Entry;

    .line 176
    .local v13, "override":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v13}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/String;

    const-string v22, "gms_disable:"

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->length()I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v16

    .line 177
    .restart local v16    # "packageName":Ljava/lang/String;
    invoke-interface {v13}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 178
    .local v5, "gserviceValue":Ljava/lang/String;
    invoke-static/range {v15 .. v16}, Lcom/google/android/partnersetup/GooglePartnerSetup;->getSystemPackageInfo(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v9

    .line 179
    .restart local v9    # "info":Landroid/content/pm/PackageInfo;
    if-eqz v9, :cond_8

    .line 181
    if-eqz v5, :cond_7

    .line 182
    :try_start_0
    invoke-interface {v13}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/String;

    invoke-interface {v13}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/String;

    iget v0, v9, Landroid/content/pm/PackageInfo;->versionCode:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    move/from16 v3, v23

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/partnersetup/AppHider;->shouldBeEnabled(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v21

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    move-object/from16 v2, v21

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/android/partnersetup/AppHider$BadGservicesValue; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 185
    :catch_0
    move-exception v4

    .line 187
    .local v4, "e":Lcom/google/android/partnersetup/AppHider$BadGservicesValue;
    const-string v21, "GooglePartnerSetup"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Malformed Gservices value for package "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-static {v0, v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 191
    .end local v4    # "e":Lcom/google/android/partnersetup/AppHider$BadGservicesValue;
    :cond_8
    const-string v21, "GooglePartnerSetup"

    const/16 v22, 0x3

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v21

    if-eqz v21, :cond_7

    .line 192
    const-string v21, "GooglePartnerSetup"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Package "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " not installed, can\'t change its visibility"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 200
    .end local v5    # "gserviceValue":Ljava/lang/String;
    .end local v9    # "info":Landroid/content/pm/PackageInfo;
    .end local v13    # "override":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v16    # "packageName":Ljava/lang/String;
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/partnersetup/AppHider;->prefs:Lcom/google/android/partnersetup/ApplicationHidingPreferences;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/partnersetup/ApplicationHidingPreferences;->getInstalledApps()Ljava/util/Set;

    move-result-object v11

    .line 201
    .local v11, "installedApps":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_b

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 202
    .local v10, "installedApp":Ljava/lang/String;
    const-string v21, "GooglePartnerSetup"

    const/16 v22, 0x3

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v21

    if-eqz v21, :cond_a

    .line 203
    const-string v21, "GooglePartnerSetup"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Skipping Gservices override for package "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    :cond_a
    move-object/from16 v0, v18

    invoke-interface {v0, v10}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 210
    .end local v10    # "installedApp":Ljava/lang/String;
    :cond_b
    invoke-interface/range {v18 .. v18}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_c

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/util/Map$Entry;

    .line 212
    .local v20, "visibility":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Boolean;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/partnersetup/AppHider;->prefs:Lcom/google/android/partnersetup/ApplicationHidingPreferences;

    move-object/from16 v22, v0

    invoke-interface/range {v20 .. v20}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/String;

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/google/android/partnersetup/ApplicationHidingPreferences;->addManagedApp(Ljava/lang/String;)Z

    .line 213
    invoke-interface/range {v20 .. v20}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/String;

    invoke-interface/range {v20 .. v20}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/Boolean;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v22

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v15, v1, v2}, Lcom/google/android/partnersetup/AppHider;->setEnabled(Landroid/content/pm/PackageManager;Ljava/lang/String;Z)V

    goto :goto_4

    .line 215
    .end local v20    # "visibility":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Boolean;>;"
    :cond_c
    return-void
.end method

.method setEnabled(Landroid/content/pm/PackageManager;Ljava/lang/String;Z)V
    .locals 4
    .param p1, "packageManager"    # Landroid/content/pm/PackageManager;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "enabled"    # Z

    .prologue
    const/4 v2, 0x3

    .line 312
    if-eqz p3, :cond_3

    const/4 v0, 0x1

    .line 316
    .local v0, "newState":I
    :goto_0
    if-eqz p3, :cond_0

    invoke-virtual {p1, p2}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I

    move-result v1

    if-eq v1, v2, :cond_4

    .line 318
    :cond_0
    const-string v1, "GooglePartnerSetup"

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 319
    const-string v1, "GooglePartnerSetup"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Setting visibility of package "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, Landroid/content/pm/PackageManager;->setApplicationEnabledSetting(Ljava/lang/String;II)V

    .line 328
    :cond_2
    :goto_1
    return-void

    .line 312
    .end local v0    # "newState":I
    :cond_3
    const/4 v0, 0x2

    goto :goto_0

    .line 324
    .restart local v0    # "newState":I
    :cond_4
    const-string v1, "GooglePartnerSetup"

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 325
    const-string v1, "GooglePartnerSetup"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " has been disabled by the user"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method shouldBeEnabled(Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 12
    .param p1, "gserviceKey"    # Ljava/lang/String;
    .param p2, "gserviceValue"    # Ljava/lang/String;
    .param p3, "installedVersion"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/partnersetup/AppHider$BadGservicesValue;
        }
    .end annotation

    .prologue
    .line 228
    const/high16 v5, -0x80000000

    .line 229
    .local v5, "maxVersion":I
    const/4 v6, 0x0

    .line 231
    .local v6, "maxVersionEnabled":Ljava/lang/Boolean;
    if-nez p2, :cond_0

    .line 232
    new-instance v9, Lcom/google/android/partnersetup/AppHider$BadGservicesValue;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "No rule for version "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " in "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-direct {p0, p1, p2}, Lcom/google/android/partnersetup/AppHider;->gserviceToString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    invoke-direct {v9, v10, v11}, Lcom/google/android/partnersetup/AppHider$BadGservicesValue;-><init>(Ljava/lang/String;Lcom/google/android/partnersetup/AppHider$1;)V

    throw v9

    .line 236
    :cond_0
    new-instance v1, Ljava/util/StringTokenizer;

    const-string v9, ","

    invoke-direct {v1, p2, v9}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    .local v1, "clauseTokenizer":Ljava/util/StringTokenizer;
    :cond_1
    :goto_0
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 240
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    .line 241
    .local v0, "clause":Ljava/lang/String;
    new-instance v4, Ljava/util/StringTokenizer;

    const-string v9, ":"

    invoke-direct {v4, v0, v9}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    .local v4, "innerTokenizer":Ljava/util/StringTokenizer;
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v9

    const/4 v10, 0x2

    if-eq v9, v10, :cond_2

    .line 243
    new-instance v9, Lcom/google/android/partnersetup/AppHider$BadGservicesValue;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Bad token count in clause \'"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\' in "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-direct {p0, p1, p2}, Lcom/google/android/partnersetup/AppHider;->gserviceToString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    invoke-direct {v9, v10, v11}, Lcom/google/android/partnersetup/AppHider$BadGservicesValue;-><init>(Ljava/lang/String;Lcom/google/android/partnersetup/AppHider$1;)V

    throw v9

    .line 246
    :cond_2
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v8

    .line 247
    .local v8, "minVersionString":Ljava/lang/String;
    invoke-direct {p0, v8, v0, p1, p2}, Lcom/google/android/partnersetup/AppHider;->extractMinVersion(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v7

    .line 250
    .local v7, "minVersion":I
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    .line 251
    .local v2, "enabledString":Ljava/lang/String;
    invoke-direct {p0, v2, v0, p1, p2}, Lcom/google/android/partnersetup/AppHider;->extractEnabled(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    .line 255
    .local v3, "explicitEnabled":Z
    if-lt v7, v5, :cond_1

    if-gt v7, p3, :cond_1

    .line 256
    if-ne v7, v5, :cond_3

    .line 258
    const-string v9, "GooglePartnerSetup"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Multiple entries for minVersion "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " in "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-direct {p0, p1, p2}, Lcom/google/android/partnersetup/AppHider;->gserviceToString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    :cond_3
    move v5, v7

    .line 262
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    goto/16 :goto_0

    .line 266
    .end local v0    # "clause":Ljava/lang/String;
    .end local v2    # "enabledString":Ljava/lang/String;
    .end local v3    # "explicitEnabled":Z
    .end local v4    # "innerTokenizer":Ljava/util/StringTokenizer;
    .end local v7    # "minVersion":I
    .end local v8    # "minVersionString":Ljava/lang/String;
    :cond_4
    if-nez v6, :cond_5

    .line 267
    new-instance v9, Lcom/google/android/partnersetup/AppHider$BadGservicesValue;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "No rule for version "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " in "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-direct {p0, p1, p2}, Lcom/google/android/partnersetup/AppHider;->gserviceToString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    invoke-direct {v9, v10, v11}, Lcom/google/android/partnersetup/AppHider$BadGservicesValue;-><init>(Ljava/lang/String;Lcom/google/android/partnersetup/AppHider$1;)V

    throw v9

    .line 270
    :cond_5
    const-string v9, "GooglePartnerSetup"

    const/4 v10, 0x3

    invoke-static {v9, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 271
    const-string v9, "GooglePartnerSetup"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Found visibility "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " for minVersion "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " from "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-direct {p0, p1, p2}, Lcom/google/android/partnersetup/AppHider;->gserviceToString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    :cond_6
    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    return v9
.end method
