.class public final Lcom/google/android/partnersetup/ClientId;
.super Ljava/lang/Object;
.source "ClientId.java"


# direct methods
.method public static formatClientId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p0, "base"    # Ljava/lang/String;
    .param p1, "country"    # Ljava/lang/String;

    .prologue
    .line 179
    const-string v4, "{country}"

    invoke-virtual {p0, v4, p1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 182
    const-string v4, "{device}"

    sget-object v5, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 185
    const-string v4, "{manufacturer}"

    sget-object v5, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 188
    const-string v4, "{model}"

    sget-object v5, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 191
    const-string v0, "-"

    .line 194
    .local v0, "delimeter":Ljava/lang/String;
    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 195
    .local v3, "tmp":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, v3

    if-ge v1, v4, :cond_1

    .line 196
    aget-object v4, v3, v1

    const-string v5, "{"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    aget-object v4, v3, v1

    const-string v5, "}"

    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 197
    aget-object v4, v3, v1

    const-string v5, "{"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 198
    .local v2, "sysvar":Ljava/lang/String;
    const-string v4, "}"

    const-string v5, ""

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 199
    aget-object v4, v3, v1

    const-string v5, "unknown"

    invoke-static {v2, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 195
    .end local v2    # "sysvar":Ljava/lang/String;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 203
    :cond_1
    return-object p0
.end method

.method public static getClientIdData(Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 150
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 151
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v1, "client_id set to: %s\nsearch client id: %s\nmaps client id: %s\nyoutube client id: %s\nmarket client id: %s\nvoicesearch client id: %s\nshopper client id: %s\nwallet client id: %s\nchrome client id: %s\n"

    const/16 v2, 0x9

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "client_id"

    invoke-static {v0, v4}, Lcom/google/android/gsf/GoogleSettingsContract$Partner;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "search_client_id"

    invoke-static {v0, v4}, Lcom/google/android/gsf/GoogleSettingsContract$Partner;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "maps_client_id"

    invoke-static {v0, v4}, Lcom/google/android/gsf/GoogleSettingsContract$Partner;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "youtube_client_id"

    invoke-static {v0, v4}, Lcom/google/android/gsf/GoogleSettingsContract$Partner;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "market_client_id"

    invoke-static {v0, v4}, Lcom/google/android/gsf/GoogleSettingsContract$Partner;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "voicesearch_client_id"

    invoke-static {v0, v4}, Lcom/google/android/gsf/GoogleSettingsContract$Partner;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "shopper_client_id"

    invoke-static {v0, v4}, Lcom/google/android/gsf/GoogleSettingsContract$Partner;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "wallet_client_id"

    invoke-static {v0, v4}, Lcom/google/android/gsf/GoogleSettingsContract$Partner;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "chrome_client_id"

    invoke-static {v0, v4}, Lcom/google/android/gsf/GoogleSettingsContract$Partner;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static getCountryCode(Landroid/content/Context;)Ljava/lang/String;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 214
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "device_country"

    const-string v5, "unknown"

    invoke-static {v3, v4, v5}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 216
    .local v0, "countryCode":Ljava/lang/String;
    const-string v3, "phone"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 218
    .local v2, "tm":Landroid/telephony/TelephonyManager;
    if-eqz v2, :cond_1

    .line 219
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v1

    .line 220
    .local v1, "simCountryCode":Ljava/lang/String;
    invoke-static {v1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 221
    const-string v3, "GooglePartnerSetup"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 222
    const-string v3, "GooglePartnerSetup"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Replacing client id {country} with SIM country code: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    :cond_0
    move-object v0, v1

    .line 229
    .end local v1    # "simCountryCode":Ljava/lang/String;
    :cond_1
    return-object v0
.end method

.method public static printClientIdData(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 172
    const-string v0, "GooglePartnerSetup"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    const-string v0, "GooglePartnerSetup"

    invoke-static {p0}, Lcom/google/android/partnersetup/ClientId;->getClientIdData(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    :cond_0
    return-void
.end method

.method private static setClientId(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "prefix"    # Ljava/lang/String;
    .param p3, "roValue"    # Ljava/lang/String;
    .param p5, "fallbackId"    # Ljava/lang/String;
    .param p6, "country"    # Ljava/lang/String;
    .param p7, "requiredPackage"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 132
    .local p4, "clientIdsGservices":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 134
    .local v3, "pm":Landroid/content/pm/PackageManager;
    if-eqz p7, :cond_0

    invoke-static {v3, p7}, Lcom/google/android/partnersetup/GooglePartnerSetup;->isPackageInstalled(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 136
    const/4 v1, 0x0

    .line 146
    :goto_0
    return-object v1

    .line 138
    :cond_0
    invoke-static {p3, p5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 139
    .local v0, "clientId":Ljava/lang/String;
    sget-object v5, Lcom/google/android/partnersetup/ClientIdPreferences;->GSERVICES_CLIENTID_MAP:Ljava/util/Map;

    invoke-interface {v5, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 140
    .local v2, "gservicesKey":Ljava/lang/String;
    invoke-interface {p4, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 141
    invoke-interface {p4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "clientId":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 143
    .restart local v0    # "clientId":Ljava/lang/String;
    :cond_1
    invoke-static {v0, p6}, Lcom/google/android/partnersetup/ClientId;->formatClientId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 144
    .local v1, "formattedId":Ljava/lang/String;
    if-nez p2, :cond_2

    move-object v4, v1

    .line 145
    .local v4, "value":Ljava/lang/String;
    :goto_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-static {v5, p1, v4}, Lcom/google/android/gsf/GoogleSettingsContract$Partner;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0

    .line 144
    .end local v4    # "value":Ljava/lang/String;
    :cond_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_1
.end method

.method public static setClientId(Landroid/content/Context;)V
    .locals 19
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    const-string v0, "GooglePartnerSetup"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    const-string v0, "GooglePartnerSetup"

    const-string v1, "Running client id setup"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 38
    :cond_0
    invoke-static/range {p0 .. p0}, Lcom/google/android/partnersetup/ClientId;->getCountryCode(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    .line 40
    .local v6, "countryCode":Ljava/lang/String;
    const-string v5, "unknown"

    .line 43
    .local v5, "baseClientId":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v7, "clientid_"

    aput-object v7, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/gsf/Gservices;->getStringsByPrefix(Landroid/content/ContentResolver;[Ljava/lang/String;)Ljava/util/Map;

    move-result-object v4

    .line 45
    .local v4, "clientIdsGservices":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v0, "clientid_base"

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "baseClientId":Ljava/lang/String;
    check-cast v5, Ljava/lang/String;

    .line 49
    .restart local v5    # "baseClientId":Ljava/lang/String;
    if-eqz v5, :cond_2

    .line 50
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "data_store_version"

    const-string v3, "GSERVICES"

    invoke-static {v0, v1, v3}, Lcom/google/android/gsf/GoogleSettingsContract$Partner;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 64
    :goto_0
    invoke-static {v5, v6}, Lcom/google/android/partnersetup/ClientId;->formatClientId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 69
    const-string v1, "client_id"

    const/4 v2, 0x0

    const-string v3, "ro.com.google.clientidbase.ms"

    const/4 v7, 0x0

    move-object/from16 v0, p0

    invoke-static/range {v0 .. v7}, Lcom/google/android/partnersetup/ClientId;->setClientId(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 73
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v15

    .line 74
    .local v15, "config":Landroid/content/res/Configuration;
    iget v0, v15, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v1, 0x258

    if-lt v0, v1, :cond_4

    const/16 v16, 0x1

    .line 75
    .local v16, "hasLargeScreen":Z
    :goto_1
    iget v0, v15, Landroid/content/res/Configuration;->touchscreen:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    iget v0, v15, Landroid/content/res/Configuration;->touchscreen:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_5

    :cond_1
    const/16 v17, 0x1

    .line 78
    .local v17, "hasTouchscreen":Z
    :goto_2
    if-eqz v16, :cond_6

    if-eqz v17, :cond_6

    const-string v2, "tablet-"

    .line 80
    .local v2, "searchPrefix":Ljava/lang/String;
    :goto_3
    const-string v1, "search_client_id"

    const-string v3, "ro.com.google.clientidbase.ms"

    const/4 v7, 0x0

    move-object/from16 v0, p0

    invoke-static/range {v0 .. v7}, Lcom/google/android/partnersetup/ClientId;->setClientId(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 85
    .local v18, "msClientId":Ljava/lang/String;
    const-string v1, "chrome_client_id"

    const-string v3, "ro.com.google.clientidbase.ms"

    const-string v7, "com.android.chrome"

    move-object/from16 v0, p0

    invoke-static/range {v0 .. v7}, Lcom/google/android/partnersetup/ClientId;->setClientId(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 90
    const-string v8, "maps_client_id"

    const-string v9, "gmm-"

    const-string v10, "ro.com.google.clientidbase.gmm"

    const-string v14, "com.google.android.apps.maps"

    move-object/from16 v7, p0

    move-object v11, v4

    move-object v12, v5

    move-object v13, v6

    invoke-static/range {v7 .. v14}, Lcom/google/android/partnersetup/ClientId;->setClientId(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 94
    const-string v8, "youtube_client_id"

    const-string v9, "mvapp-"

    const-string v10, "ro.com.google.clientidbase.yt"

    const-string v14, "com.google.android.youtube"

    move-object/from16 v7, p0

    move-object v11, v4

    move-object v12, v5

    move-object v13, v6

    invoke-static/range {v7 .. v14}, Lcom/google/android/partnersetup/ClientId;->setClientId(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 98
    const-string v8, "market_client_id"

    const-string v9, "am-"

    const-string v10, "ro.com.google.clientidbase.am"

    const-string v14, "com.android.vending"

    move-object/from16 v7, p0

    move-object v11, v4

    move-object v12, v5

    move-object v13, v6

    invoke-static/range {v7 .. v14}, Lcom/google/android/partnersetup/ClientId;->setClientId(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 103
    const-string v8, "voicesearch_client_id"

    const-string v10, "ro.com.google.clientidbase.vs"

    const-string v14, "com.google.android.voicesearch"

    move-object/from16 v7, p0

    move-object v9, v2

    move-object v11, v4

    move-object/from16 v12, v18

    move-object v13, v6

    invoke-static/range {v7 .. v14}, Lcom/google/android/partnersetup/ClientId;->setClientId(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 108
    const-string v8, "shopper_client_id"

    const-string v9, "shopper-"

    const-string v10, "ro.com.google.clientidbase.sh"

    const-string v14, "com.google.android.apps.shopper"

    move-object/from16 v7, p0

    move-object v11, v4

    move-object v12, v5

    move-object v13, v6

    invoke-static/range {v7 .. v14}, Lcom/google/android/partnersetup/ClientId;->setClientId(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 112
    const-string v8, "wallet_client_id"

    const-string v9, "wallet-"

    const-string v10, "ro.com.google.clientidbase.wal"

    const-string v14, "com.google.android.apps.walletnfcrel"

    move-object/from16 v7, p0

    move-object v11, v4

    move-object v12, v5

    move-object v13, v6

    invoke-static/range {v7 .. v14}, Lcom/google/android/partnersetup/ClientId;->setClientId(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 114
    return-void

    .line 53
    .end local v2    # "searchPrefix":Ljava/lang/String;
    .end local v15    # "config":Landroid/content/res/Configuration;
    .end local v16    # "hasLargeScreen":Z
    .end local v17    # "hasTouchscreen":Z
    .end local v18    # "msClientId":Ljava/lang/String;
    :cond_2
    const-string v0, "GooglePartnerSetup"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 54
    const-string v0, "GooglePartnerSetup"

    const-string v1, "Setting client id base from system property ro.com.google.clientidbase"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    :cond_3
    const-string v0, "ro.com.google.clientidbase"

    const-string v1, "unknown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 59
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "data_store_version"

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v3}, Lcom/google/android/gsf/GoogleSettingsContract$Partner;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 74
    .restart local v15    # "config":Landroid/content/res/Configuration;
    :cond_4
    const/16 v16, 0x0

    goto/16 :goto_1

    .line 75
    .restart local v16    # "hasLargeScreen":Z
    :cond_5
    const/16 v17, 0x0

    goto/16 :goto_2

    .line 78
    .restart local v17    # "hasTouchscreen":Z
    :cond_6
    const-string v2, "ms-"

    goto/16 :goto_3
.end method
