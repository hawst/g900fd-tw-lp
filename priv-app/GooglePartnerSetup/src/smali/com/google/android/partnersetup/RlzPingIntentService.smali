.class public Lcom/google/android/partnersetup/RlzPingIntentService;
.super Landroid/app/IntentService;
.source "RlzPingIntentService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/partnersetup/RlzPingIntentService$RlzAccountManager;,
        Lcom/google/android/partnersetup/RlzPingIntentService$RlzAccountManagerInterface;,
        Lcom/google/android/partnersetup/RlzPingIntentService$MyBinder;
    }
.end annotation


# instance fields
.field private findAppsInNewThread:Z

.field private mAccountManager:Lcom/google/android/partnersetup/RlzPingIntentService$RlzAccountManagerInterface;

.field private final mBinder:Landroid/os/IBinder;

.field private mPreferences:Lcom/google/android/partnersetup/RlzPreferencesInterface;

.field private mScheduler:Lcom/google/android/partnersetup/RlzPingService$RlzPingSchedulerInterface;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 75
    const-string v0, "GooglePartnerSetup"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/partnersetup/RlzPingIntentService;->findAppsInNewThread:Z

    .line 55
    new-instance v0, Lcom/google/android/partnersetup/RlzPingIntentService$MyBinder;

    invoke-direct {v0, p0}, Lcom/google/android/partnersetup/RlzPingIntentService$MyBinder;-><init>(Lcom/google/android/partnersetup/RlzPingIntentService;)V

    iput-object v0, p0, Lcom/google/android/partnersetup/RlzPingIntentService;->mBinder:Landroid/os/IBinder;

    .line 76
    return-void
.end method

.method public static startRlzPingService(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "rlzIntent"    # Landroid/content/Intent;

    .prologue
    .line 99
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 100
    .local v0, "intent":Landroid/content/Intent;
    const-class v1, Lcom/google/android/partnersetup/RlzPingIntentService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 101
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 102
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/partnersetup/RlzPingIntentService;->mBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 111
    invoke-virtual {p0, p1, p0}, Lcom/google/android/partnersetup/RlzPingIntentService;->onHandleIntentWithContext(Landroid/content/Intent;Landroid/content/Context;)V

    .line 112
    return-void
.end method

.method onHandleIntentWithContext(Landroid/content/Intent;Landroid/content/Context;)V
    .locals 19
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 116
    const-string v4, "GooglePartnerSetup"

    const/4 v6, 0x3

    invoke-static {v4, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 117
    const-string v4, "GooglePartnerSetup"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Handling intent "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/partnersetup/RlzPingIntentService;->mPreferences:Lcom/google/android/partnersetup/RlzPreferencesInterface;

    if-nez v4, :cond_1

    .line 121
    new-instance v4, Lcom/google/android/partnersetup/RlzPreferences;

    move-object/from16 v0, p2

    invoke-direct {v4, v0}, Lcom/google/android/partnersetup/RlzPreferences;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/partnersetup/RlzPingIntentService;->mPreferences:Lcom/google/android/partnersetup/RlzPreferencesInterface;

    .line 123
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/partnersetup/RlzPingIntentService;->mScheduler:Lcom/google/android/partnersetup/RlzPingService$RlzPingSchedulerInterface;

    if-nez v4, :cond_2

    .line 124
    new-instance v4, Lcom/google/android/partnersetup/RlzPingService$PingScheduler;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/partnersetup/RlzPingIntentService;->mPreferences:Lcom/google/android/partnersetup/RlzPreferencesInterface;

    move-object/from16 v0, p2

    invoke-direct {v4, v0, v6}, Lcom/google/android/partnersetup/RlzPingService$PingScheduler;-><init>(Landroid/content/Context;Lcom/google/android/partnersetup/RlzPreferencesInterface;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/partnersetup/RlzPingIntentService;->mScheduler:Lcom/google/android/partnersetup/RlzPingService$RlzPingSchedulerInterface;

    .line 126
    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v10

    .line 127
    .local v10, "action":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 128
    .local v3, "cr":Landroid/content/ContentResolver;
    const-string v4, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 135
    new-instance v18, Landroid/content/ContentValues;

    invoke-direct/range {v18 .. v18}, Landroid/content/ContentValues;-><init>()V

    .line 136
    .local v18, "values":Landroid/content/ContentValues;
    const-string v4, "status"

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 137
    sget-object v4, Lcom/google/android/partnersetup/RlzProtocol$Events;->CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v3, v4, v0, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 140
    const/4 v4, 0x1

    new-array v5, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v6, "_id"

    aput-object v6, v5, v4

    .line 141
    .local v5, "projection":[Ljava/lang/String;
    sget-object v4, Lcom/google/android/partnersetup/RlzProtocol$Events;->CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 142
    .local v14, "cur":Landroid/database/Cursor;
    invoke-interface {v14}, Landroid/database/Cursor;->getCount()I

    move-result v13

    .line 143
    .local v13, "count":I
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 145
    if-lez v13, :cond_4

    .line 147
    new-instance v4, Landroid/content/Intent;

    const-class v6, Lcom/google/android/partnersetup/RlzPingService;

    move-object/from16 v0, p2

    invoke-direct {v4, v0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 213
    .end local v5    # "projection":[Ljava/lang/String;
    .end local v13    # "count":I
    .end local v14    # "cur":Landroid/database/Cursor;
    .end local v18    # "values":Landroid/content/ContentValues;
    :cond_3
    :goto_0
    return-void

    .line 153
    .restart local v5    # "projection":[Ljava/lang/String;
    .restart local v13    # "count":I
    .restart local v14    # "cur":Landroid/database/Cursor;
    .restart local v18    # "values":Landroid/content/ContentValues;
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/partnersetup/RlzPingIntentService;->mScheduler:Lcom/google/android/partnersetup/RlzPingService$RlzPingSchedulerInterface;

    invoke-interface {v4}, Lcom/google/android/partnersetup/RlzPingService$RlzPingSchedulerInterface;->reschedulePing()V

    goto :goto_0

    .line 155
    .end local v5    # "projection":[Ljava/lang/String;
    .end local v13    # "count":I
    .end local v14    # "cur":Landroid/database/Cursor;
    .end local v18    # "values":Landroid/content/ContentValues;
    :cond_5
    const-string v4, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 161
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v16

    .line 164
    .local v16, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v4

    const/16 v6, 0x80

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v6}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v12

    .line 169
    .local v12, "app":Landroid/content/pm/ApplicationInfo;
    iget-object v4, v12, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v4, :cond_3

    iget-object v4, v12, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v6, "com.google.android.partnersetup.RLZ_ACCESS_POINT"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 171
    iget-object v4, v12, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v6, "com.google.android.partnersetup.RLZ_ACCESS_POINT"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 172
    .local v11, "ap":Ljava/lang/String;
    const/4 v4, 0x1

    new-array v5, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v6, "_id"

    aput-object v6, v5, v4

    .line 173
    .restart local v5    # "projection":[Ljava/lang/String;
    sget-object v4, Lcom/google/android/partnersetup/RlzProtocol$Apps;->CONTENT_URI:Landroid/net/Uri;

    const-string v6, "app_name=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v11, v7, v8

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 175
    .restart local v14    # "cur":Landroid/database/Cursor;
    invoke-interface {v14}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-nez v4, :cond_6

    .line 176
    invoke-static {v3, v11}, Lcom/google/android/partnersetup/RlzPingService;->addApplicationInstallEvent(Landroid/content/ContentResolver;Ljava/lang/String;)V

    .line 177
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/partnersetup/RlzPingIntentService;->mScheduler:Lcom/google/android/partnersetup/RlzPingService$RlzPingSchedulerInterface;

    invoke-interface {v4}, Lcom/google/android/partnersetup/RlzPingService$RlzPingSchedulerInterface;->scheduleEventPing()V

    .line 179
    :cond_6
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 166
    .end local v5    # "projection":[Ljava/lang/String;
    .end local v11    # "ap":Ljava/lang/String;
    .end local v12    # "app":Landroid/content/pm/ApplicationInfo;
    .end local v14    # "cur":Landroid/database/Cursor;
    :catch_0
    move-exception v15

    .line 167
    .local v15, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_0

    .line 181
    .end local v15    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v16    # "pm":Landroid/content/pm/PackageManager;
    :cond_7
    const-string v4, "android.accounts.LOGIN_ACCOUNTS_CHANGED"

    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 187
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/partnersetup/RlzPingIntentService;->mAccountManager:Lcom/google/android/partnersetup/RlzPingIntentService$RlzAccountManagerInterface;

    if-nez v4, :cond_8

    .line 188
    new-instance v4, Lcom/google/android/partnersetup/RlzPingIntentService$RlzAccountManager;

    move-object/from16 v0, p2

    invoke-direct {v4, v0}, Lcom/google/android/partnersetup/RlzPingIntentService$RlzAccountManager;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/partnersetup/RlzPingIntentService;->mAccountManager:Lcom/google/android/partnersetup/RlzPingIntentService$RlzAccountManagerInterface;

    .line 190
    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/partnersetup/RlzPingIntentService;->mAccountManager:Lcom/google/android/partnersetup/RlzPingIntentService$RlzAccountManagerInterface;

    invoke-interface {v4}, Lcom/google/android/partnersetup/RlzPingIntentService$RlzAccountManagerInterface;->getAccounts()[Landroid/accounts/Account;

    move-result-object v9

    .line 191
    .local v9, "accounts":[Landroid/accounts/Account;
    if-eqz v9, :cond_3

    array-length v4, v9

    if-lez v4, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/partnersetup/RlzPingIntentService;->mPreferences:Lcom/google/android/partnersetup/RlzPreferencesInterface;

    invoke-interface {v4}, Lcom/google/android/partnersetup/RlzPreferencesInterface;->isActivationPingPrepared()Z

    move-result v4

    if-nez v4, :cond_3

    .line 193
    new-instance v17, Lcom/google/android/partnersetup/RlzPingIntentService$1;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2}, Lcom/google/android/partnersetup/RlzPingIntentService$1;-><init>(Lcom/google/android/partnersetup/RlzPingIntentService;Landroid/content/Context;)V

    .line 204
    .local v17, "t":Ljava/lang/Thread;
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/partnersetup/RlzPingIntentService;->findAppsInNewThread:Z

    if-eqz v4, :cond_9

    .line 205
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Thread;->start()V

    .line 209
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/partnersetup/RlzPingIntentService;->mScheduler:Lcom/google/android/partnersetup/RlzPingService$RlzPingSchedulerInterface;

    invoke-interface {v4}, Lcom/google/android/partnersetup/RlzPingService$RlzPingSchedulerInterface;->scheduleEventPing()V

    .line 210
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/partnersetup/RlzPingIntentService;->mPreferences:Lcom/google/android/partnersetup/RlzPreferencesInterface;

    const/4 v6, 0x1

    invoke-interface {v4, v6}, Lcom/google/android/partnersetup/RlzPreferencesInterface;->setActivationPingPrepared(Z)V

    goto/16 :goto_0

    .line 207
    :cond_9
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Thread;->run()V

    goto :goto_1
.end method

.method setAccountManager(Lcom/google/android/partnersetup/RlzPingIntentService$RlzAccountManagerInterface;)V
    .locals 0
    .param p1, "am"    # Lcom/google/android/partnersetup/RlzPingIntentService$RlzAccountManagerInterface;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/google/android/partnersetup/RlzPingIntentService;->mAccountManager:Lcom/google/android/partnersetup/RlzPingIntentService$RlzAccountManagerInterface;

    .line 91
    return-void
.end method

.method setFindAppsInNewThreadFlag(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 95
    iput-boolean p1, p0, Lcom/google/android/partnersetup/RlzPingIntentService;->findAppsInNewThread:Z

    .line 96
    return-void
.end method

.method setPingScheduler(Lcom/google/android/partnersetup/RlzPingService$RlzPingSchedulerInterface;)V
    .locals 0
    .param p1, "sched"    # Lcom/google/android/partnersetup/RlzPingService$RlzPingSchedulerInterface;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/google/android/partnersetup/RlzPingIntentService;->mScheduler:Lcom/google/android/partnersetup/RlzPingService$RlzPingSchedulerInterface;

    .line 86
    return-void
.end method

.method setPreferences(Lcom/google/android/partnersetup/RlzPreferencesInterface;)V
    .locals 0
    .param p1, "prefs"    # Lcom/google/android/partnersetup/RlzPreferencesInterface;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/google/android/partnersetup/RlzPingIntentService;->mPreferences:Lcom/google/android/partnersetup/RlzPreferencesInterface;

    .line 81
    return-void
.end method
