.class Lcom/google/android/partnersetup/RlzPingService$1;
.super Ljava/lang/Thread;
.source "RlzPingService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/partnersetup/RlzPingService;->onStartCommand(Landroid/content/Intent;II)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/partnersetup/RlzPingService;

.field final synthetic val$intent:Landroid/content/Intent;

.field final synthetic val$startId:I


# direct methods
.method constructor <init>(Lcom/google/android/partnersetup/RlzPingService;Landroid/content/Intent;I)V
    .locals 0

    .prologue
    .line 331
    iput-object p1, p0, Lcom/google/android/partnersetup/RlzPingService$1;->this$0:Lcom/google/android/partnersetup/RlzPingService;

    iput-object p2, p0, Lcom/google/android/partnersetup/RlzPingService$1;->val$intent:Landroid/content/Intent;

    iput p3, p0, Lcom/google/android/partnersetup/RlzPingService$1;->val$startId:I

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 29

    .prologue
    .line 334
    const/4 v2, 0x4

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "events._id"

    aput-object v5, v4, v2

    const/4 v2, 0x1

    const-string v5, "ap"

    aput-object v5, v4, v2

    const/4 v2, 0x2

    const-string v5, "event_type"

    aput-object v5, v4, v2

    const/4 v2, 0x3

    const-string v5, "current_rlz"

    aput-object v5, v4, v2

    .line 340
    .local v4, "projection":[Ljava/lang/String;
    const/16 v24, -0x1

    .line 341
    .local v24, "numEventsProcessed":I
    sget-object v2, Lcom/google/android/partnersetup/RlzProtocol$Events;->CONTENT_URI:Landroid/net/Uri;

    const-string v5, "/pending"

    invoke-static {v2, v5}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 343
    .local v3, "pending":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/partnersetup/RlzPingService$1;->this$0:Lcom/google/android/partnersetup/RlzPingService;

    # getter for: Lcom/google/android/partnersetup/RlzPingService;->mResolver:Landroid/content/ContentResolver;
    invoke-static {v2}, Lcom/google/android/partnersetup/RlzPingService;->access$200(Lcom/google/android/partnersetup/RlzPingService;)Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    .line 344
    .local v17, "cur":Landroid/database/Cursor;
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-nez v2, :cond_4

    .line 346
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    .line 347
    new-instance v27, Ljava/util/ArrayList;

    invoke-direct/range {v27 .. v27}, Ljava/util/ArrayList;-><init>()V

    .line 348
    .local v27, "rlzs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/partnersetup/RlzProtocol$PingInfo$RLZ;>;"
    const/4 v2, 0x2

    new-array v7, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "app_name"

    aput-object v5, v7, v2

    const/4 v2, 0x1

    const-string v5, "current_rlz"

    aput-object v5, v7, v2

    .line 352
    .local v7, "rlzProj":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/partnersetup/RlzPingService$1;->this$0:Lcom/google/android/partnersetup/RlzPingService;

    # getter for: Lcom/google/android/partnersetup/RlzPingService;->mResolver:Landroid/content/ContentResolver;
    invoke-static {v2}, Lcom/google/android/partnersetup/RlzPingService;->access$200(Lcom/google/android/partnersetup/RlzPingService;)Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/google/android/partnersetup/RlzProtocol$Apps;->CONTENT_URI:Landroid/net/Uri;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    .line 353
    :goto_0
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 354
    new-instance v2, Lcom/google/android/partnersetup/RlzProtocol$PingInfo$RLZ;

    const/4 v5, 0x0

    move-object/from16 v0, v17

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    move-object/from16 v0, v17

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v5, v6}, Lcom/google/android/partnersetup/RlzProtocol$PingInfo$RLZ;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 356
    :cond_0
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    .line 357
    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/partnersetup/RlzPingService$1;->this$0:Lcom/google/android/partnersetup/RlzPingService;

    # getter for: Lcom/google/android/partnersetup/RlzPingService;->mMachineID:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/partnersetup/RlzPingService;->access$300(Lcom/google/android/partnersetup/RlzPingService;)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/partnersetup/RlzPingService$1;->this$0:Lcom/google/android/partnersetup/RlzPingService;

    # getter for: Lcom/google/android/partnersetup/RlzPingService;->mBrandCode:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/partnersetup/RlzPingService;->access$400(Lcom/google/android/partnersetup/RlzPingService;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/partnersetup/RlzPingService$1;->this$0:Lcom/google/android/partnersetup/RlzPingService;

    # getter for: Lcom/google/android/partnersetup/RlzPingService;->mPID:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/partnersetup/RlzPingService;->access$500(Lcom/google/android/partnersetup/RlzPingService;)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/partnersetup/RlzPingService$1;->this$0:Lcom/google/android/partnersetup/RlzPingService;

    invoke-virtual {v2}, Lcom/google/android/partnersetup/RlzPingService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/partnersetup/RlzAcap;->generateAcap(Landroid/content/res/Configuration;)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v9, v27

    invoke-static/range {v8 .. v13}, Lcom/google/android/partnersetup/RlzPingService;->buildPing(Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v25

    .line 360
    .local v25, "ping":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/partnersetup/RlzPingService$1;->this$0:Lcom/google/android/partnersetup/RlzPingService;

    move-object/from16 v0, v25

    # invokes: Lcom/google/android/partnersetup/RlzPingService;->tryPing(Landroid/net/Uri;)I
    invoke-static {v2, v0}, Lcom/google/android/partnersetup/RlzPingService;->access$600(Lcom/google/android/partnersetup/RlzPingService;Landroid/net/Uri;)I

    move-result v24

    .line 424
    .end local v7    # "rlzProj":[Ljava/lang/String;
    .end local v25    # "ping":Landroid/net/Uri;
    .end local v27    # "rlzs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/partnersetup/RlzProtocol$PingInfo$RLZ;>;"
    :goto_1
    if-ltz v24, :cond_1

    .line 426
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/partnersetup/RlzPingService$1;->this$0:Lcom/google/android/partnersetup/RlzPingService;

    # getter for: Lcom/google/android/partnersetup/RlzPingService;->mRlzPreferences:Lcom/google/android/partnersetup/RlzPreferencesInterface;
    invoke-static {v2}, Lcom/google/android/partnersetup/RlzPingService;->access$800(Lcom/google/android/partnersetup/RlzPingService;)Lcom/google/android/partnersetup/RlzPreferencesInterface;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/partnersetup/RlzPreferencesInterface;->resetBackoffTime()V

    .line 428
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/partnersetup/RlzPingService$1;->val$intent:Landroid/content/Intent;

    const-string v5, "com.google.android.partnersetup.intents.EXTRA_TESTING"

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 429
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/partnersetup/RlzPingService$1;->this$0:Lcom/google/android/partnersetup/RlzPingService;

    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.google.android.partnersetup.intents.ACTION_SERVICE_FINISHED"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Lcom/google/android/partnersetup/RlzPingService;->sendBroadcast(Landroid/content/Intent;)V

    .line 431
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/partnersetup/RlzPingService$1;->this$0:Lcom/google/android/partnersetup/RlzPingService;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/partnersetup/RlzPingService$1;->val$startId:I

    invoke-virtual {v2, v5}, Lcom/google/android/partnersetup/RlzPingService;->stopSelf(I)V

    .line 432
    return-void

    .line 419
    .local v8, "events":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/partnersetup/RlzProtocol$PingInfo$Event;>;"
    .local v9, "rlzs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/partnersetup/RlzProtocol$PingInfo$RLZ;>;"
    .local v19, "eventIDsToClear":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local v20, "eventsToClear":Ljava/lang/StringBuilder;
    .local v21, "isFirst":Z
    .restart local v25    # "ping":Landroid/net/Uri;
    .local v28, "values":Landroid/content/ContentValues;
    :cond_3
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    .line 420
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/partnersetup/RlzPingService$1;->this$0:Lcom/google/android/partnersetup/RlzPingService;

    # getter for: Lcom/google/android/partnersetup/RlzPingService;->mResolver:Landroid/content/ContentResolver;
    invoke-static {v2}, Lcom/google/android/partnersetup/RlzPingService;->access$200(Lcom/google/android/partnersetup/RlzPingService;)Landroid/content/ContentResolver;

    move-result-object v10

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object v11, v3

    move-object v12, v4

    invoke-virtual/range {v10 .. v15}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    .line 362
    .end local v8    # "events":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/partnersetup/RlzProtocol$PingInfo$Event;>;"
    .end local v9    # "rlzs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/partnersetup/RlzProtocol$PingInfo$RLZ;>;"
    .end local v19    # "eventIDsToClear":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v20    # "eventsToClear":Ljava/lang/StringBuilder;
    .end local v21    # "isFirst":Z
    .end local v25    # "ping":Landroid/net/Uri;
    .end local v28    # "values":Landroid/content/ContentValues;
    :cond_4
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_8

    .line 364
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 366
    .restart local v8    # "events":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/partnersetup/RlzProtocol$PingInfo$Event;>;"
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 368
    .restart local v9    # "rlzs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/partnersetup/RlzProtocol$PingInfo$RLZ;>;"
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    .line 369
    .restart local v20    # "eventsToClear":Ljava/lang/StringBuilder;
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 370
    .restart local v19    # "eventIDsToClear":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/16 v21, 0x1

    .line 371
    .restart local v21    # "isFirst":Z
    :goto_2
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 372
    const/4 v2, 0x0

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    .line 373
    .local v22, "id":J
    const/4 v2, 0x1

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 374
    .local v16, "ap":Ljava/lang/String;
    const/4 v2, 0x2

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 375
    .local v18, "et":Ljava/lang/String;
    const/4 v2, 0x3

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v26

    .line 376
    .local v26, "rlz":Ljava/lang/String;
    if-nez v26, :cond_5

    .line 377
    const-string v26, ""

    .line 379
    :cond_5
    new-instance v2, Lcom/google/android/partnersetup/RlzProtocol$PingInfo$Event;

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-direct {v2, v0, v1}, Lcom/google/android/partnersetup/RlzProtocol$PingInfo$Event;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v8, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 380
    new-instance v2, Lcom/google/android/partnersetup/RlzProtocol$PingInfo$RLZ;

    move-object/from16 v0, v16

    move-object/from16 v1, v26

    invoke-direct {v2, v0, v1}, Lcom/google/android/partnersetup/RlzProtocol$PingInfo$RLZ;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 381
    if-nez v21, :cond_6

    .line 382
    const-string v2, " OR "

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 384
    :cond_6
    const-string v2, "_id"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 385
    const-string v2, "=?"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 386
    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 387
    const/16 v21, 0x0

    .line 388
    goto :goto_2

    .line 389
    .end local v16    # "ap":Ljava/lang/String;
    .end local v18    # "et":Ljava/lang/String;
    .end local v22    # "id":J
    .end local v26    # "rlz":Ljava/lang/String;
    :cond_7
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->deactivate()V

    .line 390
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/partnersetup/RlzPingService$1;->this$0:Lcom/google/android/partnersetup/RlzPingService;

    # getter for: Lcom/google/android/partnersetup/RlzPingService;->mMachineID:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/partnersetup/RlzPingService;->access$300(Lcom/google/android/partnersetup/RlzPingService;)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/partnersetup/RlzPingService$1;->this$0:Lcom/google/android/partnersetup/RlzPingService;

    # getter for: Lcom/google/android/partnersetup/RlzPingService;->mBrandCode:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/partnersetup/RlzPingService;->access$400(Lcom/google/android/partnersetup/RlzPingService;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/partnersetup/RlzPingService$1;->this$0:Lcom/google/android/partnersetup/RlzPingService;

    # getter for: Lcom/google/android/partnersetup/RlzPingService;->mPID:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/partnersetup/RlzPingService;->access$500(Lcom/google/android/partnersetup/RlzPingService;)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/partnersetup/RlzPingService$1;->this$0:Lcom/google/android/partnersetup/RlzPingService;

    invoke-virtual {v2}, Lcom/google/android/partnersetup/RlzPingService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/partnersetup/RlzAcap;->generateAcap(Landroid/content/res/Configuration;)Ljava/lang/String;

    move-result-object v13

    invoke-static/range {v8 .. v13}, Lcom/google/android/partnersetup/RlzPingService;->buildPing(Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v25

    .line 393
    .restart local v25    # "ping":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/partnersetup/RlzPingService$1;->this$0:Lcom/google/android/partnersetup/RlzPingService;

    move-object/from16 v0, v25

    # invokes: Lcom/google/android/partnersetup/RlzPingService;->tryPing(Landroid/net/Uri;)I
    invoke-static {v2, v0}, Lcom/google/android/partnersetup/RlzPingService;->access$600(Lcom/google/android/partnersetup/RlzPingService;Landroid/net/Uri;)I

    move-result v24

    .line 400
    new-instance v28, Landroid/content/ContentValues;

    invoke-direct/range {v28 .. v28}, Landroid/content/ContentValues;-><init>()V

    .line 401
    .restart local v28    # "values":Landroid/content/ContentValues;
    const-string v2, "status"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v28

    invoke-virtual {v0, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 403
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/partnersetup/RlzPingService$1;->this$0:Lcom/google/android/partnersetup/RlzPingService;

    # getter for: Lcom/google/android/partnersetup/RlzPingService;->mResolver:Landroid/content/ContentResolver;
    invoke-static {v2}, Lcom/google/android/partnersetup/RlzPingService;->access$200(Lcom/google/android/partnersetup/RlzPingService;)Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/google/android/partnersetup/RlzProtocol$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    move-object/from16 v0, v28

    invoke-virtual {v5, v6, v0, v10, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 407
    if-gez v24, :cond_9

    .line 422
    .end local v8    # "events":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/partnersetup/RlzProtocol$PingInfo$Event;>;"
    .end local v9    # "rlzs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/partnersetup/RlzProtocol$PingInfo$RLZ;>;"
    .end local v19    # "eventIDsToClear":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v20    # "eventsToClear":Ljava/lang/StringBuilder;
    .end local v21    # "isFirst":Z
    .end local v25    # "ping":Landroid/net/Uri;
    .end local v28    # "values":Landroid/content/ContentValues;
    :cond_8
    :goto_3
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 415
    .restart local v8    # "events":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/partnersetup/RlzProtocol$PingInfo$Event;>;"
    .restart local v9    # "rlzs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/partnersetup/RlzProtocol$PingInfo$RLZ;>;"
    .restart local v19    # "eventIDsToClear":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v20    # "eventsToClear":Ljava/lang/StringBuilder;
    .restart local v21    # "isFirst":Z
    .restart local v25    # "ping":Landroid/net/Uri;
    .restart local v28    # "values":Landroid/content/ContentValues;
    :cond_9
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    move/from16 v0, v24

    if-ge v0, v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/partnersetup/RlzPingService$1;->this$0:Lcom/google/android/partnersetup/RlzPingService;

    # getter for: Lcom/google/android/partnersetup/RlzPingService;->mResolver:Landroid/content/ContentResolver;
    invoke-static {v2}, Lcom/google/android/partnersetup/RlzPingService;->access$200(Lcom/google/android/partnersetup/RlzPingService;)Landroid/content/ContentResolver;

    move-result-object v2

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v5

    move/from16 v0, v24

    # invokes: Lcom/google/android/partnersetup/RlzPingService;->eventsWereAdded(Landroid/content/ContentResolver;II)Z
    invoke-static {v2, v0, v5}, Lcom/google/android/partnersetup/RlzPingService;->access$700(Landroid/content/ContentResolver;II)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3
.end method
