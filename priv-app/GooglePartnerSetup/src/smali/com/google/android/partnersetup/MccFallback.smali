.class public Lcom/google/android/partnersetup/MccFallback;
.super Ljava/lang/Object;
.source "MccFallback.java"


# instance fields
.field private final context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/google/android/partnersetup/MccFallback;->context:Landroid/content/Context;

    .line 29
    return-void
.end method


# virtual methods
.method public setFallbackMcc()V
    .locals 11

    .prologue
    .line 51
    const/4 v5, 0x0

    .line 53
    .local v5, "mccNum":I
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v8

    invoke-interface {v8}, Landroid/app/IActivityManager;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 54
    .local v0, "config":Landroid/content/res/Configuration;
    iget-object v8, p0, Lcom/google/android/partnersetup/MccFallback;->context:Landroid/content/Context;

    const-string v9, "phone"

    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/telephony/TelephonyManager;

    .line 57
    .local v7, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v6

    .line 59
    .local v6, "simCountryCode":Ljava/lang/String;
    const-string v8, "ro.com.google.mcc_fallback"

    const-string v9, "0"

    invoke-static {v8, v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 60
    .local v1, "countrySysProp":Ljava/lang/String;
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 63
    .local v3, "fallbackMccNum":I
    iget-object v8, p0, Lcom/google/android/partnersetup/MccFallback;->context:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "device_country"

    const/4 v10, 0x0

    invoke-static {v8, v9, v10}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 70
    .local v4, "gservicesCountryCode":Ljava/lang/String;
    if-eqz v6, :cond_0

    const-string v8, ""

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    :cond_0
    if-eqz v3, :cond_4

    .line 71
    if-eqz v4, :cond_1

    const-string v8, "de"

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 73
    :cond_1
    const-string v8, "GooglePartnerSetup"

    const/4 v9, 0x3

    invoke-static {v8, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 74
    const-string v8, "GooglePartnerSetup"

    const-string v9, "Setting fallback MCC based on system property."

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    :cond_2
    move v5, v3

    .line 89
    :goto_0
    iput v5, v0, Landroid/content/res/Configuration;->mcc:I

    .line 90
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v8

    invoke-interface {v8, v0}, Landroid/app/IActivityManager;->updateConfiguration(Landroid/content/res/Configuration;)V

    .line 92
    const-string v8, "GooglePartnerSetup"

    const/4 v9, 0x3

    invoke-static {v8, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 93
    const-string v8, "GooglePartnerSetup"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Setting fallback mcc: mcc="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v0, Landroid/content/res/Configuration;->mcc:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    :cond_3
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v8

    invoke-interface {v8}, Landroid/app/IActivityManager;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 97
    const-string v8, "GooglePartnerSetup"

    const/4 v9, 0x3

    invoke-static {v8, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 98
    const-string v8, "GooglePartnerSetup"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "new config from system:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v0, Landroid/content/res/Configuration;->mcc:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    .end local v0    # "config":Landroid/content/res/Configuration;
    .end local v1    # "countrySysProp":Ljava/lang/String;
    .end local v3    # "fallbackMccNum":I
    .end local v4    # "gservicesCountryCode":Ljava/lang/String;
    .end local v6    # "simCountryCode":Ljava/lang/String;
    .end local v7    # "tm":Landroid/telephony/TelephonyManager;
    :cond_4
    :goto_1
    return-void

    .line 79
    .restart local v0    # "config":Landroid/content/res/Configuration;
    .restart local v1    # "countrySysProp":Ljava/lang/String;
    .restart local v3    # "fallbackMccNum":I
    .restart local v4    # "gservicesCountryCode":Ljava/lang/String;
    .restart local v6    # "simCountryCode":Ljava/lang/String;
    .restart local v7    # "tm":Landroid/telephony/TelephonyManager;
    :cond_5
    const-string v8, "GooglePartnerSetup"

    const/4 v9, 0x3

    invoke-static {v8, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 80
    const-string v8, "GooglePartnerSetup"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Clearing fallback MCC based on gservices country: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 86
    :cond_6
    const/16 v5, 0x3e7

    goto :goto_0

    .line 101
    .end local v0    # "config":Landroid/content/res/Configuration;
    .end local v1    # "countrySysProp":Ljava/lang/String;
    .end local v3    # "fallbackMccNum":I
    .end local v4    # "gservicesCountryCode":Ljava/lang/String;
    .end local v6    # "simCountryCode":Ljava/lang/String;
    .end local v7    # "tm":Landroid/telephony/TelephonyManager;
    :catch_0
    move-exception v2

    .line 102
    .local v2, "e":Ljava/lang/Exception;
    const-string v8, "GooglePartnerSetup"

    const-string v9, "Failed to set fallback mcc"

    invoke-static {v8, v9, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method
