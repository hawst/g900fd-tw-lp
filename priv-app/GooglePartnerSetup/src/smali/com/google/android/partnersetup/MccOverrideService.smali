.class public Lcom/google/android/partnersetup/MccOverrideService;
.super Landroid/app/IntentService;
.source "MccOverrideService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/partnersetup/MccOverrideService$MyBinder;
    }
.end annotation


# instance fields
.field private final mBinder:Landroid/os/IBinder;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    const-string v0, "GooglePartnerSetup"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 16
    new-instance v0, Lcom/google/android/partnersetup/MccOverrideService$MyBinder;

    invoke-direct {v0, p0}, Lcom/google/android/partnersetup/MccOverrideService$MyBinder;-><init>(Lcom/google/android/partnersetup/MccOverrideService;)V

    iput-object v0, p0, Lcom/google/android/partnersetup/MccOverrideService;->mBinder:Landroid/os/IBinder;

    .line 26
    return-void
.end method

.method public static startMccOverrideService(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/partnersetup/MccOverrideService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 30
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 31
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/partnersetup/MccOverrideService;->mBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 40
    const-string v1, "GooglePartnerSetup"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 41
    const-string v1, "GooglePartnerSetup"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handling intent "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    :cond_0
    new-instance v0, Lcom/google/android/partnersetup/MccOverride;

    invoke-direct {v0, p0}, Lcom/google/android/partnersetup/MccOverride;-><init>(Landroid/content/Context;)V

    .line 44
    .local v0, "mccOverride":Lcom/google/android/partnersetup/MccOverride;
    invoke-virtual {v0}, Lcom/google/android/partnersetup/MccOverride;->overrideMcc()V

    .line 45
    return-void
.end method
