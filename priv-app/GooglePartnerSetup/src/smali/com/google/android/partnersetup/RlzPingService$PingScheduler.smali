.class public Lcom/google/android/partnersetup/RlzPingService$PingScheduler;
.super Ljava/lang/Object;
.source "RlzPingService.java"

# interfaces
.implements Lcom/google/android/partnersetup/RlzPingService$RlzPingSchedulerInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/partnersetup/RlzPingService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PingScheduler"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mPreferences:Lcom/google/android/partnersetup/RlzPreferencesInterface;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/partnersetup/RlzPreferencesInterface;)V
    .locals 0
    .param p1, "c"    # Landroid/content/Context;
    .param p2, "prefs"    # Lcom/google/android/partnersetup/RlzPreferencesInterface;

    .prologue
    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    iput-object p1, p0, Lcom/google/android/partnersetup/RlzPingService$PingScheduler;->mContext:Landroid/content/Context;

    .line 155
    iput-object p2, p0, Lcom/google/android/partnersetup/RlzPingService$PingScheduler;->mPreferences:Lcom/google/android/partnersetup/RlzPreferencesInterface;

    .line 156
    return-void
.end method

.method private schedulePing(J)V
    .locals 9
    .param p1, "time"    # J

    .prologue
    const/4 v7, 0x0

    .line 163
    new-instance v1, Landroid/content/Intent;

    iget-object v3, p0, Lcom/google/android/partnersetup/RlzPingService$PingScheduler;->mContext:Landroid/content/Context;

    const-class v4, Lcom/google/android/partnersetup/RlzPingService;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 164
    .local v1, "launchPing":Landroid/content/Intent;
    iget-object v3, p0, Lcom/google/android/partnersetup/RlzPingService$PingScheduler;->mContext:Landroid/content/Context;

    invoke-static {v3, v7, v1, v7}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 167
    .local v2, "sender":Landroid/app/PendingIntent;
    iget-object v3, p0, Lcom/google/android/partnersetup/RlzPingService$PingScheduler;->mPreferences:Lcom/google/android/partnersetup/RlzPreferencesInterface;

    invoke-interface {v3, p1, p2}, Lcom/google/android/partnersetup/RlzPreferencesInterface;->setAlarmWakeTime(J)V

    .line 168
    # getter for: Lcom/google/android/partnersetup/RlzPingService;->LOG:Ljava/util/logging/Logger;
    invoke-static {}, Lcom/google/android/partnersetup/RlzPingService;->access$000()Ljava/util/logging/Logger;

    move-result-object v3

    sget-object v4, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Setting next ping for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 170
    iget-object v3, p0, Lcom/google/android/partnersetup/RlzPingService$PingScheduler;->mContext:Landroid/content/Context;

    const-string v4, "alarm"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 171
    .local v0, "am":Landroid/app/AlarmManager;
    invoke-virtual {v0, v7, p1, p2, v2}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 172
    return-void
.end method


# virtual methods
.method public reschedulePing()V
    .locals 2

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/partnersetup/RlzPingService$PingScheduler;->mPreferences:Lcom/google/android/partnersetup/RlzPreferencesInterface;

    invoke-interface {v0}, Lcom/google/android/partnersetup/RlzPreferencesInterface;->getAlarmWakeTime()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/partnersetup/RlzPingService$PingScheduler;->schedulePing(J)V

    .line 210
    return-void
.end method

.method public scheduleEventPing()V
    .locals 8

    .prologue
    .line 188
    iget-object v4, p0, Lcom/google/android/partnersetup/RlzPingService$PingScheduler;->mPreferences:Lcom/google/android/partnersetup/RlzPreferencesInterface;

    invoke-interface {v4}, Lcom/google/android/partnersetup/RlzPreferencesInterface;->getAlarmWakeTime()J

    move-result-wide v2

    .line 189
    .local v2, "scheduledTime":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v6, p0, Lcom/google/android/partnersetup/RlzPingService$PingScheduler;->mPreferences:Lcom/google/android/partnersetup/RlzPreferencesInterface;

    invoke-interface {v6}, Lcom/google/android/partnersetup/RlzPreferencesInterface;->getDelayAfterEvent()I

    move-result v6

    mul-int/lit16 v6, v6, 0x3e8

    int-to-long v6, v6

    add-long v0, v4, v6

    .line 191
    .local v0, "delayTime":J
    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Lcom/google/android/partnersetup/RlzPingService$PingScheduler;->schedulePing(J)V

    .line 192
    return-void
.end method

.method public schedulePeriodicPing()V
    .locals 6

    .prologue
    .line 178
    iget-object v1, p0, Lcom/google/android/partnersetup/RlzPingService$PingScheduler;->mPreferences:Lcom/google/android/partnersetup/RlzPreferencesInterface;

    invoke-interface {v1}, Lcom/google/android/partnersetup/RlzPreferencesInterface;->getPingInterval()I

    move-result v0

    .line 179
    .local v0, "secondsToAdd":I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    mul-int/lit16 v1, v0, 0x3e8

    int-to-long v4, v1

    add-long/2addr v2, v4

    invoke-direct {p0, v2, v3}, Lcom/google/android/partnersetup/RlzPingService$PingScheduler;->schedulePing(J)V

    .line 181
    return-void
.end method

.method public scheduleRetryPing()V
    .locals 6

    .prologue
    .line 198
    iget-object v2, p0, Lcom/google/android/partnersetup/RlzPingService$PingScheduler;->mPreferences:Lcom/google/android/partnersetup/RlzPreferencesInterface;

    invoke-interface {v2}, Lcom/google/android/partnersetup/RlzPreferencesInterface;->getBackoffTime()I

    move-result v0

    .line 199
    .local v0, "backoffTime":I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    mul-int/lit16 v4, v0, 0x3e8

    int-to-long v4, v4

    add-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 201
    .local v1, "retryTime":Ljava/lang/Long;
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/google/android/partnersetup/RlzPingService$PingScheduler;->schedulePing(J)V

    .line 202
    iget-object v2, p0, Lcom/google/android/partnersetup/RlzPingService$PingScheduler;->mPreferences:Lcom/google/android/partnersetup/RlzPreferencesInterface;

    invoke-interface {v2}, Lcom/google/android/partnersetup/RlzPreferencesInterface;->doubleBackoffTime()V

    .line 203
    return-void
.end method
