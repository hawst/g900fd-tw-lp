.class public Lcom/google/android/partnersetup/MccFallbackService;
.super Landroid/app/IntentService;
.source "MccFallbackService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/partnersetup/MccFallbackService$MyBinder;
    }
.end annotation


# instance fields
.field private final mBinder:Landroid/os/IBinder;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    const-string v0, "GooglePartnerSetup"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 18
    new-instance v0, Lcom/google/android/partnersetup/MccFallbackService$MyBinder;

    invoke-direct {v0, p0}, Lcom/google/android/partnersetup/MccFallbackService$MyBinder;-><init>(Lcom/google/android/partnersetup/MccFallbackService;)V

    iput-object v0, p0, Lcom/google/android/partnersetup/MccFallbackService;->mBinder:Landroid/os/IBinder;

    .line 28
    return-void
.end method

.method public static startSetFallbackMccService(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 34
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/partnersetup/MccFallbackService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 35
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 36
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/partnersetup/MccFallbackService;->mBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 45
    const-string v1, "GooglePartnerSetup"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 46
    const-string v1, "GooglePartnerSetup"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Handling intent "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    :cond_0
    new-instance v0, Lcom/google/android/partnersetup/MccFallback;

    invoke-direct {v0, p0}, Lcom/google/android/partnersetup/MccFallback;-><init>(Landroid/content/Context;)V

    .line 49
    .local v0, "mccFallback":Lcom/google/android/partnersetup/MccFallback;
    invoke-virtual {v0}, Lcom/google/android/partnersetup/MccFallback;->setFallbackMcc()V

    .line 50
    return-void
.end method
