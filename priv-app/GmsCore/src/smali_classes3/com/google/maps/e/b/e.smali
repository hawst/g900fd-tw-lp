.class public final Lcom/google/maps/e/b/e;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lcom/google/p/a/b/b/h;

.field private static b:Lcom/google/p/a/b/b/h;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x1

    const/16 v7, 0x21e

    const/4 v6, 0x0

    const/16 v5, 0x218

    const/16 v4, 0x21b

    new-instance v0, Lcom/google/p/a/b/b/h;

    invoke-direct {v0}, Lcom/google/p/a/b/b/h;-><init>()V

    sput-object v0, Lcom/google/maps/e/b/e;->a:Lcom/google/p/a/b/b/h;

    new-instance v0, Lcom/google/p/a/b/b/h;

    invoke-direct {v0}, Lcom/google/p/a/b/b/h;-><init>()V

    sput-object v0, Lcom/google/maps/e/b/e;->b:Lcom/google/p/a/b/b/h;

    sget-object v0, Lcom/google/maps/e/b/e;->a:Lcom/google/p/a/b/b/h;

    const/16 v1, 0x41b

    sget-object v2, Lcom/google/maps/e/a/a/a;->a:Lcom/google/p/a/b/b/h;

    invoke-virtual {v0, v1, v8, v2}, Lcom/google/p/a/b/b/h;->a(IILjava/lang/Object;)Lcom/google/p/a/b/b/h;

    move-result-object v0

    const/4 v1, 0x2

    const-wide/16 v2, 0x6

    invoke-static {v2, v3}, Lcom/google/p/a/e/q;->a(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/p/a/b/b/h;->a(IILjava/lang/Object;)Lcom/google/p/a/b/b/h;

    move-result-object v0

    const/4 v1, 0x3

    sget-object v2, Lcom/google/maps/a/a/a;->a:Lcom/google/p/a/b/b/h;

    invoke-virtual {v0, v4, v1, v2}, Lcom/google/p/a/b/b/h;->a(IILjava/lang/Object;)Lcom/google/p/a/b/b/h;

    move-result-object v0

    const/16 v1, 0x14

    sget-object v2, Lcom/google/maps/e/b/c;->a:Lcom/google/p/a/b/b/h;

    invoke-virtual {v0, v4, v1, v2}, Lcom/google/p/a/b/b/h;->a(IILjava/lang/Object;)Lcom/google/p/a/b/b/h;

    move-result-object v0

    const/16 v1, 0x215

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/p/a/b/b/h;->a(IILjava/lang/Object;)Lcom/google/p/a/b/b/h;

    move-result-object v0

    const/4 v1, 0x5

    const-wide/16 v2, 0x2

    invoke-static {v2, v3}, Lcom/google/p/a/e/q;->a(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/p/a/b/b/h;->a(IILjava/lang/Object;)Lcom/google/p/a/b/b/h;

    move-result-object v0

    const/4 v1, 0x6

    sget-object v2, Lcom/google/maps/e/a/a/a;->c:Lcom/google/p/a/b/b/h;

    invoke-virtual {v0, v4, v1, v2}, Lcom/google/p/a/b/b/h;->a(IILjava/lang/Object;)Lcom/google/p/a/b/b/h;

    move-result-object v0

    const/4 v1, 0x7

    const-wide/16 v2, 0x2

    invoke-static {v2, v3}, Lcom/google/p/a/e/q;->a(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/p/a/b/b/h;->a(IILjava/lang/Object;)Lcom/google/p/a/b/b/h;

    move-result-object v0

    const/16 v1, 0x8

    sget-object v2, Lcom/google/maps/e/a/a/a;->d:Lcom/google/p/a/b/b/h;

    invoke-virtual {v0, v4, v1, v2}, Lcom/google/p/a/b/b/h;->a(IILjava/lang/Object;)Lcom/google/p/a/b/b/h;

    move-result-object v0

    const/16 v1, 0x41b

    const/16 v2, 0x9

    sget-object v3, Lcom/google/maps/e/a/a/a;->b:Lcom/google/p/a/b/b/h;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/p/a/b/b/h;->a(IILjava/lang/Object;)Lcom/google/p/a/b/b/h;

    move-result-object v0

    const/16 v1, 0x224

    const/16 v2, 0x15

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/p/a/b/b/h;->a(IILjava/lang/Object;)Lcom/google/p/a/b/b/h;

    move-result-object v0

    const/16 v1, 0x224

    const/16 v2, 0xd

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/p/a/b/b/h;->a(IILjava/lang/Object;)Lcom/google/p/a/b/b/h;

    move-result-object v0

    const/16 v1, 0xc

    invoke-virtual {v0, v5, v1, v6}, Lcom/google/p/a/b/b/h;->a(IILjava/lang/Object;)Lcom/google/p/a/b/b/h;

    move-result-object v0

    const/16 v1, 0xe

    sget-object v2, Lcom/google/p/a/b/b/f;->a:Ljava/lang/Boolean;

    invoke-virtual {v0, v5, v1, v2}, Lcom/google/p/a/b/b/h;->a(IILjava/lang/Object;)Lcom/google/p/a/b/b/h;

    move-result-object v0

    const/16 v1, 0xf

    sget-object v2, Lcom/google/maps/e/b/i;->a:Lcom/google/p/a/b/b/h;

    invoke-virtual {v0, v4, v1, v2}, Lcom/google/p/a/b/b/h;->a(IILjava/lang/Object;)Lcom/google/p/a/b/b/h;

    move-result-object v0

    const/16 v1, 0x10

    sget-object v2, Lcom/google/p/a/b/b/f;->a:Ljava/lang/Boolean;

    invoke-virtual {v0, v5, v1, v2}, Lcom/google/p/a/b/b/h;->a(IILjava/lang/Object;)Lcom/google/p/a/b/b/h;

    move-result-object v0

    const/16 v1, 0x11

    sget-object v2, Lcom/google/p/a/b/b/f;->a:Ljava/lang/Boolean;

    invoke-virtual {v0, v5, v1, v2}, Lcom/google/p/a/b/b/h;->a(IILjava/lang/Object;)Lcom/google/p/a/b/b/h;

    move-result-object v0

    const/16 v1, 0x12

    sget-object v2, Lcom/google/p/a/b/b/f;->a:Ljava/lang/Boolean;

    invoke-virtual {v0, v5, v1, v2}, Lcom/google/p/a/b/b/h;->a(IILjava/lang/Object;)Lcom/google/p/a/b/b/h;

    move-result-object v0

    const/16 v1, 0x13

    sget-object v2, Lcom/google/maps/e/b/e;->b:Lcom/google/p/a/b/b/h;

    invoke-virtual {v0, v4, v1, v2}, Lcom/google/p/a/b/b/h;->a(IILjava/lang/Object;)Lcom/google/p/a/b/b/h;

    move-result-object v0

    const/16 v1, 0x16

    sget-object v2, Lcom/google/p/a/b/b/f;->a:Ljava/lang/Boolean;

    invoke-virtual {v0, v5, v1, v2}, Lcom/google/p/a/b/b/h;->a(IILjava/lang/Object;)Lcom/google/p/a/b/b/h;

    move-result-object v0

    const/16 v1, 0x215

    const/16 v2, 0x17

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/p/a/b/b/h;->a(IILjava/lang/Object;)Lcom/google/p/a/b/b/h;

    move-result-object v0

    const/16 v1, 0x18

    sget-object v2, Lcom/google/maps/e/b/a;->a:Lcom/google/p/a/b/b/h;

    invoke-virtual {v0, v4, v1, v2}, Lcom/google/p/a/b/b/h;->a(IILjava/lang/Object;)Lcom/google/p/a/b/b/h;

    sget-object v0, Lcom/google/maps/e/b/e;->b:Lcom/google/p/a/b/b/h;

    const-wide/16 v2, 0x1

    invoke-static {v2, v3}, Lcom/google/p/a/e/q;->a(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v7, v8, v1}, Lcom/google/p/a/b/b/h;->a(IILjava/lang/Object;)Lcom/google/p/a/b/b/h;

    return-void
.end method
