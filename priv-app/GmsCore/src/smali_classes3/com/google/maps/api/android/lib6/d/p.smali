.class public final Lcom/google/maps/api/android/lib6/d/p;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/maps/api/android/lib6/c/ba;


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:Lcom/google/android/gms/maps/model/CameraPosition;

.field public final f:I

.field public final g:I

.field public final h:D

.field private final i:D

.field private final j:Landroid/graphics/Point;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/maps/model/CameraPosition;IIDIIII)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iput p6, p0, Lcom/google/maps/api/android/lib6/d/p;->a:I

    iput p7, p0, Lcom/google/maps/api/android/lib6/d/p;->b:I

    iput p8, p0, Lcom/google/maps/api/android/lib6/d/p;->c:I

    iput p9, p0, Lcom/google/maps/api/android/lib6/d/p;->d:I

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/d/p;->e:Lcom/google/android/gms/maps/model/CameraPosition;

    iput p2, p0, Lcom/google/maps/api/android/lib6/d/p;->f:I

    iput p3, p0, Lcom/google/maps/api/android/lib6/d/p;->g:I

    iput-wide p4, p0, Lcom/google/maps/api/android/lib6/d/p;->h:D

    iget v0, p1, Lcom/google/android/gms/maps/model/CameraPosition;->b:F

    float-to-double v0, v0

    iput-wide v0, p0, Lcom/google/maps/api/android/lib6/d/p;->i:D

    new-instance v0, Landroid/graphics/Point;

    sub-int v1, p2, p6

    sub-int/2addr v1, p8

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, p6

    sub-int v2, p3, p7

    sub-int/2addr v2, p9

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, p7

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/d/p;->j:Landroid/graphics/Point;

    return-void
.end method

.method public static a(DD)D
    .locals 4

    const-wide/high16 v0, 0x4070000000000000L    # 256.0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3, p0, p1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    mul-double/2addr v0, v2

    mul-double/2addr v0, p2

    return-wide v0
.end method

.method public static a(Lcom/google/android/gms/maps/model/LatLng;DD)Lcom/google/maps/api/android/lib6/d/q;
    .locals 13

    const-wide/16 v0, 0x0

    cmpl-double v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/k/a/cj;->a(Z)V

    invoke-static {p0}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-wide v0, p0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-static/range {p1 .. p4}, Lcom/google/maps/api/android/lib6/d/p;->a(DD)D

    move-result-wide v2

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    div-double v4, v2, v4

    iget-wide v6, p0, Lcom/google/android/gms/maps/model/LatLng;->a:D

    invoke-static {v6, v7}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    const-wide v8, 0x4076800000000000L    # 360.0

    div-double/2addr v0, v8

    mul-double/2addr v0, v2

    add-double/2addr v0, v4

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    add-double/2addr v8, v6

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    sub-double v6, v10, v6

    div-double v6, v8, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->log(D)D

    move-result-wide v6

    const-wide/high16 v8, 0x4010000000000000L    # 4.0

    div-double/2addr v6, v8

    const-wide v8, 0x400921fb54442d18L    # Math.PI

    div-double/2addr v6, v8

    mul-double/2addr v2, v6

    sub-double v2, v4, v2

    new-instance v4, Lcom/google/maps/api/android/lib6/d/q;

    double-to-long v0, v0

    double-to-long v2, v2

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/google/maps/api/android/lib6/d/q;-><init>(JJ)V

    return-object v4

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/maps/model/LatLng;)Landroid/graphics/Point;
    .locals 10

    const-wide/16 v8, 0x2

    iget-wide v0, p0, Lcom/google/maps/api/android/lib6/d/p;->i:D

    iget-wide v2, p0, Lcom/google/maps/api/android/lib6/d/p;->h:D

    invoke-static {p1, v0, v1, v2, v3}, Lcom/google/maps/api/android/lib6/d/p;->a(Lcom/google/android/gms/maps/model/LatLng;DD)Lcom/google/maps/api/android/lib6/d/q;

    move-result-object v2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/p;->e:Lcom/google/android/gms/maps/model/CameraPosition;

    iget-object v0, v0, Lcom/google/android/gms/maps/model/CameraPosition;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v4, p0, Lcom/google/maps/api/android/lib6/d/p;->i:D

    iget-wide v6, p0, Lcom/google/maps/api/android/lib6/d/p;->h:D

    invoke-static {v0, v4, v5, v6, v7}, Lcom/google/maps/api/android/lib6/d/p;->a(Lcom/google/android/gms/maps/model/LatLng;DD)Lcom/google/maps/api/android/lib6/d/q;

    move-result-object v3

    iget-wide v0, v2, Lcom/google/maps/api/android/lib6/d/q;->a:J

    iget-wide v4, v3, Lcom/google/maps/api/android/lib6/d/q;->a:J

    sub-long/2addr v0, v4

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/d/p;->b()J

    move-result-wide v4

    div-long v6, v4, v8

    cmp-long v6, v0, v6

    if-lez v6, :cond_0

    sub-long/2addr v0, v4

    :cond_0
    neg-long v6, v4

    div-long/2addr v6, v8

    cmp-long v6, v0, v6

    if-gez v6, :cond_1

    add-long/2addr v0, v4

    :cond_1
    new-instance v4, Landroid/graphics/Point;

    iget-object v5, p0, Lcom/google/maps/api/android/lib6/d/p;->j:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    int-to-long v6, v5

    add-long/2addr v0, v6

    long-to-int v0, v0

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/d/p;->j:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-long v6, v1

    iget-wide v8, v2, Lcom/google/maps/api/android/lib6/d/q;->b:J

    iget-wide v2, v3, Lcom/google/maps/api/android/lib6/d/q;->b:J

    sub-long v2, v8, v2

    add-long/2addr v2, v6

    long-to-int v1, v2

    invoke-direct {v4, v0, v1}, Landroid/graphics/Point;-><init>(II)V

    return-object v4
.end method

.method public final a(Landroid/graphics/Point;)Lcom/google/android/gms/maps/model/LatLng;
    .locals 12

    const-wide v10, 0x401921fb54442d18L    # 6.283185307179586

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/p;->e:Lcom/google/android/gms/maps/model/CameraPosition;

    iget-object v0, v0, Lcom/google/android/gms/maps/model/CameraPosition;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, p0, Lcom/google/maps/api/android/lib6/d/p;->i:D

    iget-wide v4, p0, Lcom/google/maps/api/android/lib6/d/p;->h:D

    invoke-static {v0, v2, v3, v4, v5}, Lcom/google/maps/api/android/lib6/d/p;->a(Lcom/google/android/gms/maps/model/LatLng;DD)Lcom/google/maps/api/android/lib6/d/q;

    move-result-object v0

    new-instance v1, Lcom/google/maps/api/android/lib6/d/q;

    iget-wide v2, v0, Lcom/google/maps/api/android/lib6/d/q;->a:J

    iget-object v4, p0, Lcom/google/maps/api/android/lib6/d/p;->j:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->x:I

    int-to-long v4, v4

    sub-long/2addr v2, v4

    iget v4, p1, Landroid/graphics/Point;->x:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    iget-wide v4, v0, Lcom/google/maps/api/android/lib6/d/q;->b:J

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/p;->j:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-long v6, v0

    sub-long/2addr v4, v6

    iget v0, p1, Landroid/graphics/Point;->y:I

    int-to-long v6, v0

    add-long/2addr v4, v6

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/maps/api/android/lib6/d/q;-><init>(JJ)V

    iget-wide v2, p0, Lcom/google/maps/api/android/lib6/d/p;->i:D

    iget-wide v4, p0, Lcom/google/maps/api/android/lib6/d/p;->h:D

    invoke-static {v2, v3, v4, v5}, Lcom/google/maps/api/android/lib6/d/p;->a(DD)D

    move-result-wide v2

    iget-wide v4, v1, Lcom/google/maps/api/android/lib6/d/q;->a:J

    long-to-double v4, v4

    div-double/2addr v4, v2

    sub-double/2addr v4, v8

    mul-double/2addr v4, v10

    iget-wide v0, v1, Lcom/google/maps/api/android/lib6/d/q;->b:J

    neg-long v0, v0

    long-to-double v0, v0

    div-double/2addr v0, v2

    add-double/2addr v0, v8

    mul-double/2addr v0, v10

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->atan(D)D

    move-result-wide v0

    mul-double/2addr v0, v2

    const-wide v2, 0x3ff921fb54442d18L    # 1.5707963267948966

    sub-double/2addr v0, v2

    new-instance v2, Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v0, v1}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v0

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    invoke-direct {v2, v0, v1, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    return-object v2
.end method

.method public final a()Lcom/google/android/gms/maps/model/VisibleRegion;
    .locals 7

    new-instance v0, Landroid/graphics/Point;

    iget v1, p0, Lcom/google/maps/api/android/lib6/d/p;->a:I

    iget v2, p0, Lcom/google/maps/api/android/lib6/d/p;->b:I

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/d/p;->a(Landroid/graphics/Point;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v3

    new-instance v0, Landroid/graphics/Point;

    iget v1, p0, Lcom/google/maps/api/android/lib6/d/p;->f:I

    iget v2, p0, Lcom/google/maps/api/android/lib6/d/p;->c:I

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/google/maps/api/android/lib6/d/p;->b:I

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/d/p;->a(Landroid/graphics/Point;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v4

    new-instance v0, Landroid/graphics/Point;

    iget v1, p0, Lcom/google/maps/api/android/lib6/d/p;->a:I

    iget v2, p0, Lcom/google/maps/api/android/lib6/d/p;->g:I

    iget v5, p0, Lcom/google/maps/api/android/lib6/d/p;->d:I

    sub-int/2addr v2, v5

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/d/p;->a(Landroid/graphics/Point;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    new-instance v0, Landroid/graphics/Point;

    iget v2, p0, Lcom/google/maps/api/android/lib6/d/p;->f:I

    iget v5, p0, Lcom/google/maps/api/android/lib6/d/p;->c:I

    sub-int/2addr v2, v5

    iget v5, p0, Lcom/google/maps/api/android/lib6/d/p;->g:I

    iget v6, p0, Lcom/google/maps/api/android/lib6/d/p;->d:I

    sub-int/2addr v5, v6

    invoke-direct {v0, v2, v5}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/d/p;->a(Landroid/graphics/Point;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v2

    new-instance v5, Lcom/google/android/gms/maps/model/LatLngBounds;

    invoke-direct {v5, v1, v4}, Lcom/google/android/gms/maps/model/LatLngBounds;-><init>(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/maps/model/LatLng;)V

    new-instance v0, Lcom/google/android/gms/maps/model/VisibleRegion;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/maps/model/VisibleRegion;-><init>(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/maps/model/LatLngBounds;)V

    return-object v0
.end method

.method public final b()J
    .locals 4

    iget-wide v0, p0, Lcom/google/maps/api/android/lib6/d/p;->i:D

    iget-wide v2, p0, Lcom/google/maps/api/android/lib6/d/p;->h:D

    invoke-static {v0, v1, v2, v3}, Lcom/google/maps/api/android/lib6/d/p;->a(DD)D

    move-result-wide v0

    double-to-long v0, v0

    return-wide v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    instance-of v2, p1, Lcom/google/maps/api/android/lib6/d/p;

    if-nez v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/google/maps/api/android/lib6/d/p;

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/d/p;->e:Lcom/google/android/gms/maps/model/CameraPosition;

    iget-object v3, p1, Lcom/google/maps/api/android/lib6/d/p;->e:Lcom/google/android/gms/maps/model/CameraPosition;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget v2, p0, Lcom/google/maps/api/android/lib6/d/p;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p1, Lcom/google/maps/api/android/lib6/d/p;->f:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget v2, p0, Lcom/google/maps/api/android/lib6/d/p;->g:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p1, Lcom/google/maps/api/android/lib6/d/p;->g:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-wide v2, p0, Lcom/google/maps/api/android/lib6/d/p;->h:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    iget-wide v4, p1, Lcom/google/maps/api/android/lib6/d/p;->h:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget v2, p0, Lcom/google/maps/api/android/lib6/d/p;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p1, Lcom/google/maps/api/android/lib6/d/p;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget v2, p0, Lcom/google/maps/api/android/lib6/d/p;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p1, Lcom/google/maps/api/android/lib6/d/p;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget v2, p0, Lcom/google/maps/api/android/lib6/d/p;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p1, Lcom/google/maps/api/android/lib6/d/p;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget v2, p0, Lcom/google/maps/api/android/lib6/d/p;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget v3, p1, Lcom/google/maps/api/android/lib6/d/p;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 4

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/d/p;->e:Lcom/google/android/gms/maps/model/CameraPosition;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/maps/api/android/lib6/d/p;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/google/maps/api/android/lib6/d/p;->g:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/maps/api/android/lib6/d/p;->h:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lcom/google/maps/api/android/lib6/d/p;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, Lcom/google/maps/api/android/lib6/d/p;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget v2, p0, Lcom/google/maps/api/android/lib6/d/p;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget v2, p0, Lcom/google/maps/api/android/lib6/d/p;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bu;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
