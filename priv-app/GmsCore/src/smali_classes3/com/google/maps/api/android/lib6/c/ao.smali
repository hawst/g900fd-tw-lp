.class public final Lcom/google/maps/api/android/lib6/c/ao;
.super Lcom/google/android/gms/maps/internal/an;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Lcom/google/maps/api/android/lib6/c/dj;

.field private final b:Lcom/google/android/gms/maps/internal/t;

.field private final c:Lcom/google/maps/api/android/lib6/c/an;

.field private final d:Landroid/content/Context;

.field private final e:Landroid/content/res/Resources;

.field private final f:Lcom/google/maps/api/android/lib6/c/aq;

.field private g:Landroid/location/Location;

.field private h:Lcom/google/android/gms/maps/internal/t;

.field private final i:Lcom/google/maps/api/android/lib6/c/cd;

.field private j:Z

.field private k:Z

.field private l:Lcom/google/android/gms/maps/internal/bk;

.field private m:Lcom/google/android/gms/maps/internal/bh;

.field private n:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;Lcom/google/maps/api/android/lib6/c/dj;Lcom/google/maps/api/android/lib6/c/an;Lcom/google/maps/api/android/lib6/c/aq;Lcom/google/android/gms/maps/internal/t;Lcom/google/maps/api/android/lib6/c/cd;Z)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/maps/internal/an;-><init>()V

    invoke-static {p1}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->d:Landroid/content/Context;

    invoke-static {p3}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/api/android/lib6/c/dj;

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->a:Lcom/google/maps/api/android/lib6/c/dj;

    invoke-static {p4}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/api/android/lib6/c/an;

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->c:Lcom/google/maps/api/android/lib6/c/an;

    invoke-static {p5}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/api/android/lib6/c/aq;

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->f:Lcom/google/maps/api/android/lib6/c/aq;

    invoke-static {p6}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/internal/t;

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->b:Lcom/google/android/gms/maps/internal/t;

    invoke-static {p6}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/internal/t;

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->h:Lcom/google/android/gms/maps/internal/t;

    invoke-static {p7}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/api/android/lib6/c/cd;

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->i:Lcom/google/maps/api/android/lib6/c/cd;

    invoke-static {p2}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->e:Landroid/content/res/Resources;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->k:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->n:Z

    return-void
.end method

.method private a(Lcom/google/android/gms/maps/model/LatLng;F)F
    .locals 12

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->a:Lcom/google/maps/api/android/lib6/c/dj;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/dj;->c()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    iget v0, v0, Lcom/google/android/gms/maps/model/CameraPosition;->b:F

    const/high16 v1, 0x41200000    # 10.0f

    cmpg-float v1, v0, v1

    if-gtz v1, :cond_0

    const/high16 v0, 0x41700000    # 15.0f

    :cond_0
    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/ao;->a:Lcom/google/maps/api/android/lib6/c/dj;

    float-to-double v2, p2

    invoke-static {v2, v3}, Lcom/google/maps/api/android/lib6/c/ej;->a(D)D

    move-result-wide v4

    invoke-static {p1, v2, v3}, Lcom/google/maps/api/android/lib6/c/ej;->a(Lcom/google/android/gms/maps/model/LatLng;D)D

    move-result-wide v2

    iget-wide v6, p1, Lcom/google/android/gms/maps/model/LatLng;->a:D

    add-double/2addr v6, v4

    iget-wide v8, p1, Lcom/google/android/gms/maps/model/LatLng;->a:D

    sub-double v4, v8, v4

    iget-wide v8, p1, Lcom/google/android/gms/maps/model/LatLng;->b:D

    add-double/2addr v8, v2

    iget-wide v10, p1, Lcom/google/android/gms/maps/model/LatLng;->b:D

    sub-double v2, v10, v2

    new-instance v10, Lcom/google/android/gms/maps/model/LatLngBounds;

    new-instance v11, Lcom/google/android/gms/maps/model/LatLng;

    invoke-direct {v11, v4, v5, v2, v3}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    new-instance v2, Lcom/google/android/gms/maps/model/LatLng;

    invoke-direct {v2, v6, v7, v8, v9}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-direct {v10, v11, v2}, Lcom/google/android/gms/maps/model/LatLngBounds;-><init>(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/maps/model/LatLng;)V

    invoke-interface {v1, v10}, Lcom/google/maps/api/android/lib6/c/dj;->a(Lcom/google/android/gms/maps/model/LatLngBounds;)Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v1

    iget v1, v1, Lcom/google/android/gms/maps/model/CameraPosition;->b:F

    const/high16 v2, -0x40800000    # -1.0f

    cmpl-float v2, v1, v2

    if-nez v2, :cond_1

    :goto_0
    return v0

    :cond_1
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/maps/api/android/lib6/c/ao;)Landroid/content/res/Resources;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->e:Landroid/content/res/Resources;

    return-object v0
.end method

.method private f()V
    .locals 3

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->k:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->j:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v2, p0, Lcom/google/maps/api/android/lib6/c/ao;->c:Lcom/google/maps/api/android/lib6/c/an;

    iget-object v2, v2, Lcom/google/maps/api/android/lib6/c/an;->a:Landroid/view/View;

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/ao;->c:Lcom/google/maps/api/android/lib6/c/an;

    if-eqz v0, :cond_2

    :goto_2
    iget-object v0, v1, Lcom/google/maps/api/android/lib6/c/an;->a:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    const/16 v1, 0x8

    goto :goto_1

    :cond_2
    const/4 p0, 0x0

    goto :goto_2
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->j:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->j:Z

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->f:Lcom/google/maps/api/android/lib6/c/aq;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/aq;->a()V

    :try_start_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->h:Lcom/google/android/gms/maps/internal/t;

    invoke-interface {v0, p0}, Lcom/google/android/gms/maps/internal/t;->a(Lcom/google/android/gms/maps/internal/am;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-direct {p0}, Lcom/google/maps/api/android/lib6/c/ao;->f()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->g:Landroid/location/Location;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->g:Landroid/location/Location;

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/c/ao;->a(Landroid/location/Location;)V

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Landroid/location/Location;)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->f:Lcom/google/maps/api/android/lib6/c/aq;

    invoke-interface {v0, p1}, Lcom/google/maps/api/android/lib6/c/aq;->a(Landroid/location/Location;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->l:Lcom/google/android/gms/maps/internal/bk;

    if-eqz v0, :cond_0

    :try_start_0
    new-instance v0, Landroid/location/Location;

    invoke-direct {v0, p1}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    iget-boolean v1, p0, Lcom/google/maps/api/android/lib6/c/ao;->n:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/ao;->l:Lcom/google/android/gms/maps/internal/bk;

    invoke-interface {v1, v0}, Lcom/google/android/gms/maps/internal/bk;->a(Landroid/location/Location;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/google/maps/api/android/lib6/c/ao;->g:Landroid/location/Location;

    return-void

    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/ao;->l:Lcom/google/android/gms/maps/internal/bk;

    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/gms/maps/internal/bk;->a(Lcom/google/android/gms/b/l;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Lcom/google/android/gms/b/l;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/gms/b/p;->a(Lcom/google/android/gms/b/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/c/ao;->a(Landroid/location/Location;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/internal/bh;)V
    .locals 0

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/c/ao;->m:Lcom/google/android/gms/maps/internal/bh;

    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/internal/bk;)V
    .locals 0

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/c/ao;->l:Lcom/google/android/gms/maps/internal/bk;

    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/internal/t;)V
    .locals 2

    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->j:Z

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->h:Lcom/google/android/gms/maps/internal/t;

    invoke-interface {v0}, Lcom/google/android/gms/maps/internal/t;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    if-eqz p1, :cond_2

    :goto_0
    iput-object p1, p0, Lcom/google/maps/api/android/lib6/c/ao;->h:Lcom/google/android/gms/maps/internal/t;

    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->j:Z

    if-eqz v0, :cond_1

    :try_start_1
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->h:Lcom/google/android/gms/maps/internal/t;

    invoke-interface {v0, p0}, Lcom/google/android/gms/maps/internal/t;->a(Lcom/google/android/gms/maps/internal/am;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1

    :cond_2
    iget-object p1, p0, Lcom/google/maps/api/android/lib6/c/ao;->b:Lcom/google/android/gms/maps/internal/t;

    goto :goto_0

    :catch_1
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->k:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/google/maps/api/android/lib6/c/ao;->k:Z

    invoke-direct {p0}, Lcom/google/maps/api/android/lib6/c/ao;->f()V

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->j:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->j:Z

    invoke-direct {p0}, Lcom/google/maps/api/android/lib6/c/ao;->f()V

    :try_start_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->h:Lcom/google/android/gms/maps/internal/t;

    invoke-interface {v0}, Lcom/google/android/gms/maps/internal/t;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->f:Lcom/google/maps/api/android/lib6/c/aq;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/aq;->b()V

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final c()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->j:Z

    return v0
.end method

.method public final d()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->k:Z

    return v0
.end method

.method public final e()Landroid/location/Location;
    .locals 2

    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->j:Z

    const-string v1, "MyLocation layer not enabled"

    invoke-static {v0, v1}, Lcom/google/k/a/cj;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->g:Landroid/location/Location;

    return-object v0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 6

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->i:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->bd:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->a(Lcom/google/maps/api/android/lib6/c/cf;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->m:Lcom/google/android/gms/maps/internal/bh;

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->m:Lcom/google/android/gms/maps/internal/bh;

    invoke-interface {v0}, Lcom/google/android/gms/maps/internal/bh;->a()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1

    :cond_1
    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->j:Z

    const-string v1, "MyLocation layer not enabled"

    invoke-static {v0, v1}, Lcom/google/k/a/cj;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->g:Landroid/location/Location;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/ao;->g:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/ao;->g:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/ao;->g:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/maps/api/android/lib6/c/ao;->a(Lcom/google/android/gms/maps/model/LatLng;F)F

    move-result v1

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/c/ao;->a:Lcom/google/maps/api/android/lib6/c/dj;

    invoke-interface {v2}, Lcom/google/maps/api/android/lib6/c/dj;->c()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/maps/model/CameraPosition;->a(Lcom/google/android/gms/maps/model/CameraPosition;)Lcom/google/android/gms/maps/model/c;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/gms/maps/model/c;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/c;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/c;->a(F)Lcom/google/android/gms/maps/model/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/maps/model/c;->a()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/ao;->a:Lcom/google/maps/api/android/lib6/c/dj;

    const/4 v2, -0x1

    invoke-interface {v1, v0, v2}, Lcom/google/maps/api/android/lib6/c/dj;->a(Lcom/google/android/gms/maps/model/CameraPosition;I)V

    :cond_2
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/maps/api/android/lib6/c/ax;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ao;->g:Landroid/location/Location;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/ao;->g:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/ao;->g:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/ao;->g:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/maps/api/android/lib6/c/ao;->a(Lcom/google/android/gms/maps/model/LatLng;F)F

    move-result v1

    new-instance v2, Lcom/google/maps/api/android/lib6/c/bf;

    invoke-direct {v2, v0, v1}, Lcom/google/maps/api/android/lib6/c/bf;-><init>(Lcom/google/android/gms/maps/model/LatLng;F)V

    new-instance v0, Lcom/google/maps/api/android/lib6/c/ap;

    invoke-direct {v0, p0, p1}, Lcom/google/maps/api/android/lib6/c/ap;-><init>(Lcom/google/maps/api/android/lib6/c/ao;Landroid/view/View;)V

    invoke-virtual {v2, v0}, Lcom/google/maps/api/android/lib6/c/bf;->a(Lcom/google/maps/api/android/lib6/c/bg;)V

    invoke-static {}, Lcom/google/maps/api/android/lib6/b/h;->a()Lcom/google/maps/api/android/lib6/b/h;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/maps/api/android/lib6/b/h;->c(Lcom/google/maps/api/android/lib6/b/g;)V

    goto/16 :goto_0
.end method
