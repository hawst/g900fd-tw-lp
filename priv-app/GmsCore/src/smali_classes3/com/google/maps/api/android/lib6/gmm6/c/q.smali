.class final Lcom/google/maps/api/android/lib6/gmm6/c/q;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Lcom/google/maps/api/android/lib6/c/fa;

.field private synthetic b:Lcom/google/maps/api/android/lib6/gmm6/c/o;


# direct methods
.method constructor <init>(Lcom/google/maps/api/android/lib6/gmm6/c/o;Lcom/google/maps/api/android/lib6/c/fa;)V
    .locals 0

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/q;->b:Lcom/google/maps/api/android/lib6/gmm6/c/o;

    iput-object p2, p0, Lcom/google/maps/api/android/lib6/gmm6/c/q;->a:Lcom/google/maps/api/android/lib6/c/fa;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/q;->b:Lcom/google/maps/api/android/lib6/gmm6/c/o;

    iget-object v1, v0, Lcom/google/maps/api/android/lib6/gmm6/c/o;->a:Lcom/google/maps/api/android/lib6/gmm6/c/n;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/q;->b:Lcom/google/maps/api/android/lib6/gmm6/c/o;

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/gmm6/c/o;->a:Lcom/google/maps/api/android/lib6/gmm6/c/n;

    invoke-static {v0}, Lcom/google/maps/api/android/lib6/gmm6/c/n;->a(Lcom/google/maps/api/android/lib6/gmm6/c/n;)Lcom/google/android/gms/maps/internal/ag;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/q;->b:Lcom/google/maps/api/android/lib6/gmm6/c/o;

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/gmm6/c/o;->a:Lcom/google/maps/api/android/lib6/gmm6/c/n;

    invoke-static {v0}, Lcom/google/maps/api/android/lib6/gmm6/c/n;->a(Lcom/google/maps/api/android/lib6/gmm6/c/n;)Lcom/google/android/gms/maps/internal/ag;

    move-result-object v0

    new-instance v2, Lcom/google/maps/api/android/lib6/c/ez;

    iget-object v3, p0, Lcom/google/maps/api/android/lib6/gmm6/c/q;->b:Lcom/google/maps/api/android/lib6/gmm6/c/o;

    iget-object v3, v3, Lcom/google/maps/api/android/lib6/gmm6/c/o;->a:Lcom/google/maps/api/android/lib6/gmm6/c/n;

    iget-object v4, p0, Lcom/google/maps/api/android/lib6/gmm6/c/q;->a:Lcom/google/maps/api/android/lib6/c/fa;

    iget-object v5, p0, Lcom/google/maps/api/android/lib6/gmm6/c/q;->b:Lcom/google/maps/api/android/lib6/gmm6/c/o;

    iget-object v5, v5, Lcom/google/maps/api/android/lib6/gmm6/c/o;->a:Lcom/google/maps/api/android/lib6/gmm6/c/n;

    invoke-static {v5}, Lcom/google/maps/api/android/lib6/gmm6/c/n;->d(Lcom/google/maps/api/android/lib6/gmm6/c/n;)Lcom/google/maps/api/android/lib6/c/cd;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, Lcom/google/maps/api/android/lib6/c/ez;-><init>(Lcom/google/maps/api/android/lib6/c/e;Lcom/google/maps/api/android/lib6/c/fa;Lcom/google/maps/api/android/lib6/c/cd;)V

    invoke-interface {v0, v2}, Lcom/google/android/gms/maps/internal/ag;->a(Lcom/google/android/gms/maps/model/internal/m;)V

    :cond_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/q;->b:Lcom/google/maps/api/android/lib6/gmm6/c/o;

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/gmm6/c/o;->a:Lcom/google/maps/api/android/lib6/gmm6/c/n;

    invoke-static {v0}, Lcom/google/maps/api/android/lib6/gmm6/c/n;->b(Lcom/google/maps/api/android/lib6/gmm6/c/n;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/api/android/lib6/c/f;

    iget-object v3, p0, Lcom/google/maps/api/android/lib6/gmm6/c/q;->a:Lcom/google/maps/api/android/lib6/c/fa;

    invoke-interface {v0, v3}, Lcom/google/maps/api/android/lib6/c/f;->a(Lcom/google/maps/api/android/lib6/c/fa;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    new-instance v2, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v2, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-void
.end method
