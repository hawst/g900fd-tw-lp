.class public final Lcom/google/maps/api/android/lib6/d/m;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/maps/api/android/lib6/c/am;
.implements Lcom/google/maps/api/android/lib6/c/at;


# instance fields
.field final a:Ljava/util/List;

.field final b:Ljava/util/List;

.field final c:Ljava/util/List;

.field final d:Lcom/google/maps/api/android/lib6/d/i;

.field e:Lcom/google/maps/api/android/lib6/c/aj;

.field f:F

.field g:F

.field h:F

.field i:F

.field private j:Lcom/google/maps/api/android/lib6/d/n;


# direct methods
.method public constructor <init>(Lcom/google/maps/api/android/lib6/d/i;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/d/m;->a:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/d/m;->b:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/d/m;->c:Ljava/util/List;

    new-instance v0, Lcom/google/maps/api/android/lib6/d/n;

    invoke-direct {v0}, Lcom/google/maps/api/android/lib6/d/n;-><init>()V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/d/m;->j:Lcom/google/maps/api/android/lib6/d/n;

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/d/m;->d:Lcom/google/maps/api/android/lib6/d/i;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/maps/api/android/lib6/c/aj;
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/m;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/m;->c:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/api/android/lib6/c/aj;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/maps/api/android/lib6/c/aj;)Lcom/google/maps/api/android/lib6/c/ak;
    .locals 1

    new-instance v0, Lcom/google/maps/api/android/lib6/d/j;

    invoke-direct {v0, p1, p0}, Lcom/google/maps/api/android/lib6/d/j;-><init>(Lcom/google/maps/api/android/lib6/c/aj;Lcom/google/maps/api/android/lib6/d/m;)V

    return-object v0
.end method

.method public final a(Lcom/google/maps/api/android/lib6/c/au;Z)Lcom/google/maps/api/android/lib6/c/av;
    .locals 1

    new-instance v0, Lcom/google/maps/api/android/lib6/d/o;

    invoke-direct {v0, p1, p2, p0}, Lcom/google/maps/api/android/lib6/d/o;-><init>(Lcom/google/maps/api/android/lib6/c/au;ZLcom/google/maps/api/android/lib6/d/m;)V

    return-object v0
.end method

.method public final a(Lcom/google/maps/api/android/lib6/c/cb;)Lcom/google/maps/api/android/lib6/c/cc;
    .locals 2

    const/4 v0, 0x5

    const-string v1, "Tile Overlays are not supported in Lite Mode"

    invoke-static {v0, v1}, Lcom/google/maps/api/android/lib6/c/by;->a(ILjava/lang/String;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/google/maps/api/android/lib6/c/eu;)Lcom/google/maps/api/android/lib6/c/ev;
    .locals 2

    const/4 v0, 0x5

    const-string v1, "Ground Overlays are not supported in Lite Mode"

    invoke-static {v0, v1}, Lcom/google/maps/api/android/lib6/c/by;->a(ILjava/lang/String;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Landroid/graphics/Canvas;Lcom/google/maps/api/android/lib6/d/p;)V
    .locals 10

    const/4 v9, 0x0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/m;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/d/m;->j:Lcom/google/maps/api/android/lib6/d/n;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/m;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/api/android/lib6/d/r;

    invoke-interface {v0, p1, p2}, Lcom/google/maps/api/android/lib6/d/r;->a(Landroid/graphics/Canvas;Lcom/google/maps/api/android/lib6/d/p;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/m;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/api/android/lib6/d/j;

    iget-object v2, v0, Lcom/google/maps/api/android/lib6/d/j;->a:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v2}, Lcom/google/maps/api/android/lib6/c/aj;->q()Landroid/graphics/Bitmap;

    move-result-object v2

    iget-object v3, v0, Lcom/google/maps/api/android/lib6/d/j;->a:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v3}, Lcom/google/maps/api/android/lib6/c/aj;->s()F

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iget-object v4, v0, Lcom/google/maps/api/android/lib6/d/j;->a:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v4}, Lcom/google/maps/api/android/lib6/c/aj;->u()F

    move-result v4

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v4, v5

    float-to-int v4, v4

    iget-object v5, v0, Lcom/google/maps/api/android/lib6/d/j;->a:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v5}, Lcom/google/maps/api/android/lib6/c/aj;->p()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v5

    invoke-interface {p2, v5}, Lcom/google/maps/api/android/lib6/c/ba;->a(Lcom/google/android/gms/maps/model/LatLng;)Landroid/graphics/Point;

    move-result-object v5

    new-instance v6, Landroid/graphics/Point;

    iget v7, v5, Landroid/graphics/Point;->x:I

    sub-int/2addr v7, v3

    iget v8, v5, Landroid/graphics/Point;->y:I

    sub-int/2addr v8, v4

    invoke-direct {v6, v7, v8}, Landroid/graphics/Point;-><init>(II)V

    iput-object v6, v0, Lcom/google/maps/api/android/lib6/d/j;->c:Landroid/graphics/Point;

    new-instance v6, Landroid/graphics/Point;

    iget v7, v5, Landroid/graphics/Point;->x:I

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    sub-int v3, v8, v3

    add-int/2addr v3, v7

    iget v5, v5, Landroid/graphics/Point;->y:I

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    sub-int v4, v7, v4

    add-int/2addr v4, v5

    invoke-direct {v6, v3, v4}, Landroid/graphics/Point;-><init>(II)V

    iput-object v6, v0, Lcom/google/maps/api/android/lib6/d/j;->d:Landroid/graphics/Point;

    if-eqz v2, :cond_1

    iget-object v3, v0, Lcom/google/maps/api/android/lib6/d/j;->a:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v3}, Lcom/google/maps/api/android/lib6/c/aj;->j()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/google/maps/api/android/lib6/d/j;->b:Landroid/graphics/Paint;

    const/high16 v4, 0x437f0000    # 255.0f

    iget-object v5, v0, Lcom/google/maps/api/android/lib6/d/j;->a:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v5}, Lcom/google/maps/api/android/lib6/c/aj;->F()F

    move-result v5

    mul-float/2addr v4, v5

    float-to-int v4, v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v3, v0, Lcom/google/maps/api/android/lib6/d/j;->c:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    iget-object v4, v0, Lcom/google/maps/api/android/lib6/d/j;->c:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/d/j;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3, v4, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/m;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    if-eqz p2, :cond_4

    new-instance v1, Landroid/graphics/Rect;

    iget v0, p2, Lcom/google/maps/api/android/lib6/d/p;->f:I

    iget v2, p2, Lcom/google/maps/api/android/lib6/d/p;->g:I

    invoke-direct {v1, v9, v9, v0, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/m;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/api/android/lib6/d/j;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/d/j;->g()Landroid/graphics/Rect;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/maps/api/android/lib6/d/m;->c:Ljava/util/List;

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/d/j;->a:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/m;->e:Lcom/google/maps/api/android/lib6/c/aj;

    if-eqz v0, :cond_5

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/d/m;->e:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v2}, Lcom/google/maps/api/android/lib6/c/aj;->q()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/maps/api/android/lib6/c/aj;->H()Landroid/graphics/Rect;

    move-result-object v4

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {v2}, Lcom/google/maps/api/android/lib6/c/aj;->G()Lcom/google/maps/api/android/lib6/c/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/al;->b()Lcom/google/android/gms/maps/internal/q;

    move-result-object v0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v5

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v6

    invoke-interface {v0, v2, v5, v6}, Lcom/google/android/gms/maps/internal/q;->a(Lcom/google/android/gms/maps/model/internal/s;II)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_3
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/google/maps/api/android/lib6/d/m;->h:F

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/google/maps/api/android/lib6/d/m;->i:F

    invoke-virtual {v2}, Lcom/google/maps/api/android/lib6/c/aj;->v()F

    move-result v1

    invoke-virtual {v2}, Lcom/google/maps/api/android/lib6/c/aj;->x()F

    move-result v2

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    iget v6, v4, Landroid/graphics/Rect;->left:I

    int-to-float v6, v6

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v1, v7

    add-float/2addr v1, v6

    iget v6, p0, Lcom/google/maps/api/android/lib6/d/m;->h:F

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    sub-float/2addr v1, v6

    iput v1, p0, Lcom/google/maps/api/android/lib6/d/m;->f:F

    iget v1, v4, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p0, Lcom/google/maps/api/android/lib6/d/m;->i:F

    sub-float/2addr v1, v2

    iput v1, p0, Lcom/google/maps/api/android/lib6/d/m;->g:F

    iget v1, p0, Lcom/google/maps/api/android/lib6/d/m;->f:F

    iget v2, p0, Lcom/google/maps/api/android/lib6/d/m;->g:F

    invoke-virtual {p1, v0, v1, v2, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_5
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    move-object v0, v1

    goto :goto_3
.end method

.method final a(Lcom/google/maps/api/android/lib6/d/j;)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/m;->e:Lcom/google/maps/api/android/lib6/c/aj;

    iget-object v1, p1, Lcom/google/maps/api/android/lib6/d/j;->a:Lcom/google/maps/api/android/lib6/c/aj;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/d/m;->e:Lcom/google/maps/api/android/lib6/c/aj;

    :cond_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/m;->d:Lcom/google/maps/api/android/lib6/d/i;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/d/i;->invalidate()V

    return-void
.end method

.method public final b()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/d/m;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/m;->c:Ljava/util/List;

    return-object v0
.end method
