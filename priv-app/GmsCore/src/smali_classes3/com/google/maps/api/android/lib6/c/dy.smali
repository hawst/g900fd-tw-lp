.class public final Lcom/google/maps/api/android/lib6/c/dy;
.super Landroid/widget/ImageView;

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;
.implements Lcom/google/android/gms/maps/internal/ad;


# instance fields
.field private a:Landroid/graphics/Matrix;

.field private b:Landroid/graphics/Matrix;

.field private c:F

.field private d:F

.field private e:Landroid/view/animation/Animation;

.field private f:Landroid/view/animation/Animation;

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/c/dy;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    sget v0, Lcom/google/android/gms/maps/ab;->L:I

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/c/dy;->setImageResource(I)V

    sget v0, Lcom/google/android/gms/maps/ab;->h:I

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/c/dy;->setBackgroundResource(I)V

    sget v0, Lcom/google/android/gms/maps/w;->a:I

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/dy;->e:Landroid/view/animation/Animation;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dy;->e:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    sget v0, Lcom/google/android/gms/maps/w;->b:I

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/dy;->f:Landroid/view/animation/Animation;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dy;->f:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    return-void
.end method

.method private a()V
    .locals 9

    const/4 v1, 0x1

    const/4 v0, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    const/high16 v7, 0x3f000000    # 0.5f

    const/high16 v6, 0x40000000    # 2.0f

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/c/dy;->a:Landroid/graphics/Matrix;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/c/dy;->b:Landroid/graphics/Matrix;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/c/dy;->b:Landroid/graphics/Matrix;

    iget-object v3, p0, Lcom/google/maps/api/android/lib6/c/dy;->a:Landroid/graphics/Matrix;

    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/c/dy;->b:Landroid/graphics/Matrix;

    iget v3, p0, Lcom/google/maps/api/android/lib6/c/dy;->c:F

    neg-float v3, v3

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/dy;->getWidth()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v4, v6

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/dy;->getHeight()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v6

    invoke-virtual {v2, v3, v4, v5}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    iget v2, p0, Lcom/google/maps/api/android/lib6/c/dy;->d:F

    const/high16 v3, 0x42b40000    # 90.0f

    div-float/2addr v2, v3

    const v3, 0x3f333333    # 0.7f

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/google/maps/api/android/lib6/c/dy;->b:Landroid/graphics/Matrix;

    sub-float v4, v8, v2

    invoke-virtual {v3, v8, v4}, Landroid/graphics/Matrix;->postScale(FF)Z

    iget-object v3, p0, Lcom/google/maps/api/android/lib6/c/dy;->b:Landroid/graphics/Matrix;

    const/4 v4, 0x0

    div-float/2addr v2, v6

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/dy;->getHeight()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v2, v5

    invoke-virtual {v3, v4, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/c/dy;->b:Landroid/graphics/Matrix;

    invoke-virtual {p0, v2}, Lcom/google/maps/api/android/lib6/c/dy;->setImageMatrix(Landroid/graphics/Matrix;)V

    :cond_0
    iget v2, p0, Lcom/google/maps/api/android/lib6/c/dy;->d:F

    iget v3, p0, Lcom/google/maps/api/android/lib6/c/dy;->c:F

    cmpl-float v2, v2, v7

    if-gtz v2, :cond_2

    cmpg-float v2, v3, v7

    if-ltz v2, :cond_1

    const v2, 0x43b3c000    # 359.5f

    cmpl-float v2, v3, v2

    if-lez v2, :cond_5

    :cond_1
    move v2, v1

    :goto_0
    if-nez v2, :cond_3

    :cond_2
    move v0, v1

    :cond_3
    if-nez v0, :cond_6

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/dy;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/dy;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/dy;->f:Landroid/view/animation/Animation;

    if-eq v0, v1, :cond_4

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dy;->f:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/c/dy;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_4
    :goto_1
    return-void

    :cond_5
    move v2, v0

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/dy;->getVisibility()I

    move-result v0

    if-nez v0, :cond_7

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/dy;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/dy;->f:Landroid/view/animation/Animation;

    if-ne v0, v1, :cond_7

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/dy;->clearAnimation()V

    goto :goto_1

    :cond_7
    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/dy;->getVisibility()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/dy;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/dy;->e:Landroid/view/animation/Animation;

    if-eq v0, v1, :cond_4

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dy;->e:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/c/dy;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/maps/model/CameraPosition;)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/dy;->g:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p1, Lcom/google/android/gms/maps/model/CameraPosition;->d:F

    iput v0, p0, Lcom/google/maps/api/android/lib6/c/dy;->c:F

    iget v0, p1, Lcom/google/android/gms/maps/model/CameraPosition;->c:F

    iput v0, p0, Lcom/google/maps/api/android/lib6/c/dy;->d:F

    invoke-direct {p0}, Lcom/google/maps/api/android/lib6/c/dy;->a()V

    goto :goto_0
.end method

.method public final a(ZLcom/google/android/gms/maps/model/CameraPosition;)V
    .locals 1

    iput-boolean p1, p0, Lcom/google/maps/api/android/lib6/c/dy;->g:Z

    if-eqz p1, :cond_0

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/c/dy;->setVisibility(I)V

    invoke-virtual {p0, p2}, Lcom/google/maps/api/android/lib6/c/dy;->a(Lcom/google/android/gms/maps/model/CameraPosition;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/dy;->clearAnimation()V

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/c/dy;->setVisibility(I)V

    goto :goto_0
.end method

.method public final asBinder()Landroid/os/IBinder;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dy;->f:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/dy;->g:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/c/dy;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dy;->e:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/dy;->g:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/c/dy;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method protected final onSizeChanged(IIII)V
    .locals 5

    const/4 v4, 0x0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ImageView;->onSizeChanged(IIII)V

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/dy;->a:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/dy;->b:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/RectF;

    int-to-float v1, p1

    int-to-float v2, p2

    invoke-direct {v0, v4, v4, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    new-instance v1, Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/dy;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/dy;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-direct {v1, v4, v4, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/c/dy;->a:Landroid/graphics/Matrix;

    sget-object v3, Landroid/graphics/Matrix$ScaleToFit;->CENTER:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v2, v1, v0, v3}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    invoke-direct {p0}, Lcom/google/maps/api/android/lib6/c/dy;->a()V

    return-void
.end method
