.class public Lcom/google/maps/api/android/lib6/gmm6/c/as;
.super Lcom/google/maps/api/android/lib6/gmm6/o/bx;

# interfaces
.implements Lcom/google/maps/api/android/lib6/gmm6/c/aq;


# static fields
.field private static synthetic N:Z


# instance fields
.field private final A:Lcom/google/maps/api/android/lib6/gmm6/c/z;

.field private B:Lcom/google/android/gms/maps/internal/ap;

.field private C:Lcom/google/android/gms/maps/internal/av;

.field private D:Lcom/google/maps/api/android/lib6/gmm6/o/bo;

.field private E:Lcom/google/maps/api/android/lib6/gmm6/o/bo;

.field private F:Lcom/google/maps/api/android/lib6/gmm6/o/bo;

.field private G:Lcom/google/maps/api/android/lib6/gmm6/o/cz;

.field private H:I

.field private I:I

.field private J:I

.field private K:I

.field private L:Lcom/google/maps/api/android/lib6/c/bz;

.field private M:Lcom/google/maps/api/android/lib6/c/et;

.field private final n:Lcom/google/maps/api/android/lib6/gmm6/o/ae;

.field private final o:Lcom/google/maps/api/android/lib6/gmm6/c/ap;

.field private final p:Lcom/google/maps/api/android/lib6/gmm6/c/c;

.field private final q:Lcom/google/maps/api/android/lib6/gmm6/c/u;

.field private final r:Lcom/google/maps/api/android/lib6/gmm6/c/h;

.field private final s:Lcom/google/maps/api/android/lib6/gmm6/o/ao;

.field private final t:Lcom/google/maps/api/android/lib6/c/ew;

.field private final u:Lcom/google/maps/api/android/lib6/gmm6/o/bz;

.field private final v:Lcom/google/maps/api/android/lib6/gmm6/c/r;

.field private final w:Lcom/google/maps/api/android/lib6/gmm6/c/ba;

.field private final x:Lcom/google/maps/api/android/lib6/gmm6/c/ay;

.field private final y:Lcom/google/maps/api/android/lib6/gmm6/c/n;

.field private final z:Lcom/google/maps/api/android/lib6/gmm6/c/s;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/maps/api/android/lib6/gmm6/c/as;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->N:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;Lcom/google/maps/api/android/lib6/gmm6/o/ae;Lcom/google/maps/api/android/lib6/gmm6/o/ao;Lcom/google/maps/api/android/lib6/gmm6/c/ba;Lcom/google/maps/api/android/lib6/gmm6/c/n;Lcom/google/maps/api/android/lib6/gmm6/c/ay;Ljava/util/concurrent/ScheduledExecutorService;Landroid/view/View;Lcom/google/maps/api/android/lib6/c/dd;Ljava/lang/String;ZLandroid/widget/TextView;Lcom/google/maps/api/android/lib6/c/bz;)V
    .locals 7

    move-object/from16 v0, p10

    iget-object v1, v0, Lcom/google/maps/api/android/lib6/c/dd;->e:Lcom/google/maps/api/android/lib6/c/af;

    move-object/from16 v0, p13

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/maps/api/android/lib6/gmm6/o/bx;-><init>(Landroid/content/Context;Landroid/content/res/Resources;Landroid/widget/TextView;Lcom/google/maps/api/android/lib6/c/af;)V

    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->L:Lcom/google/maps/api/android/lib6/c/bz;

    iput-object p3, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->n:Lcom/google/maps/api/android/lib6/gmm6/o/ae;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->n:Lcom/google/maps/api/android/lib6/gmm6/o/ae;

    invoke-virtual {v1}, Lcom/google/maps/api/android/lib6/gmm6/o/ae;->b()V

    new-instance v1, Lcom/google/maps/api/android/lib6/gmm6/c/ap;

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->n:Lcom/google/maps/api/android/lib6/gmm6/o/ae;

    invoke-direct {v1, v2, p0}, Lcom/google/maps/api/android/lib6/gmm6/c/ap;-><init>(Lcom/google/maps/api/android/lib6/gmm6/o/ae;Lcom/google/maps/api/android/lib6/gmm6/c/aq;)V

    iput-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->o:Lcom/google/maps/api/android/lib6/gmm6/c/ap;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->n:Lcom/google/maps/api/android/lib6/gmm6/o/ae;

    iput-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/o/bx;->e:Lcom/google/maps/api/android/lib6/gmm6/o/ae;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/o/bx;->e:Lcom/google/maps/api/android/lib6/gmm6/o/ae;

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/gmm6/o/bx;->f:Lcom/google/maps/api/android/lib6/gmm6/o/bg;

    invoke-virtual {v1, v2}, Lcom/google/maps/api/android/lib6/gmm6/o/ae;->a(Lcom/google/maps/api/android/lib6/gmm6/o/t;)V

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/o/bx;->e:Lcom/google/maps/api/android/lib6/gmm6/o/ae;

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/gmm6/o/bx;->a:Lcom/google/maps/api/android/lib6/gmm6/o/l;

    invoke-virtual {v1, v2}, Lcom/google/maps/api/android/lib6/gmm6/o/ae;->a(Lcom/google/maps/api/android/lib6/gmm6/o/b/f;)V

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/o/bx;->a:Lcom/google/maps/api/android/lib6/gmm6/o/l;

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/gmm6/o/bx;->e:Lcom/google/maps/api/android/lib6/gmm6/o/ae;

    invoke-virtual {v1, v2}, Lcom/google/maps/api/android/lib6/gmm6/o/l;->a(Lcom/google/maps/api/android/lib6/gmm6/o/au;)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/maps/api/android/lib6/gmm6/o/cc;->m:Z

    if-nez p12, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {p0, v1}, Lcom/google/maps/api/android/lib6/gmm6/c/as;->h(Z)V

    iput-object p4, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->s:Lcom/google/maps/api/android/lib6/gmm6/o/ao;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->s:Lcom/google/maps/api/android/lib6/gmm6/o/ao;

    new-instance v2, Lcom/google/maps/api/android/lib6/gmm6/c/au;

    invoke-direct {v2}, Lcom/google/maps/api/android/lib6/gmm6/c/au;-><init>()V

    iput-object v2, v1, Lcom/google/maps/api/android/lib6/gmm6/o/ao;->e:Lcom/google/maps/api/android/lib6/gmm6/o/aq;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->n:Lcom/google/maps/api/android/lib6/gmm6/o/ae;

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->s:Lcom/google/maps/api/android/lib6/gmm6/o/ao;

    invoke-virtual {v1, v2}, Lcom/google/maps/api/android/lib6/gmm6/o/ae;->a(Lcom/google/maps/api/android/lib6/gmm6/o/ao;)V

    new-instance v1, Lcom/google/maps/api/android/lib6/gmm6/c/u;

    invoke-direct {v1, p0, p8}, Lcom/google/maps/api/android/lib6/gmm6/c/u;-><init>(Lcom/google/maps/api/android/lib6/gmm6/c/aq;Ljava/util/concurrent/ScheduledExecutorService;)V

    iput-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->q:Lcom/google/maps/api/android/lib6/gmm6/c/u;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->o:Lcom/google/maps/api/android/lib6/gmm6/c/ap;

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->L:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-static {p0, v1, v2}, Lcom/google/maps/api/android/lib6/gmm6/c/h;->a(Lcom/google/maps/api/android/lib6/gmm6/c/aq;Lcom/google/maps/api/android/lib6/gmm6/c/ap;Lcom/google/maps/api/android/lib6/c/bz;)Lcom/google/maps/api/android/lib6/gmm6/c/h;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->r:Lcom/google/maps/api/android/lib6/gmm6/c/h;

    new-instance v1, Lcom/google/maps/api/android/lib6/c/et;

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->r:Lcom/google/maps/api/android/lib6/gmm6/c/h;

    invoke-direct {v1, p0, v2}, Lcom/google/maps/api/android/lib6/c/et;-><init>(Landroid/view/View;Lcom/google/maps/api/android/lib6/c/am;)V

    iput-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->M:Lcom/google/maps/api/android/lib6/c/et;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->M:Lcom/google/maps/api/android/lib6/c/et;

    invoke-static {p0, v1}, Landroid/support/v4/view/ay;->a(Landroid/view/View;Landroid/support/v4/view/a;)V

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v2, Lcom/google/maps/api/android/lib6/gmm6/c/c;

    iget-object v3, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->o:Lcom/google/maps/api/android/lib6/gmm6/c/ap;

    invoke-direct {v2, p0, v3, v1}, Lcom/google/maps/api/android/lib6/gmm6/c/c;-><init>(Lcom/google/maps/api/android/lib6/gmm6/c/aq;Lcom/google/maps/api/android/lib6/gmm6/c/ap;Landroid/os/Handler;)V

    iput-object v2, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->p:Lcom/google/maps/api/android/lib6/gmm6/c/c;

    new-instance v2, Lcom/google/maps/api/android/lib6/gmm6/c/s;

    invoke-direct {v2, p0}, Lcom/google/maps/api/android/lib6/gmm6/c/s;-><init>(Lcom/google/maps/api/android/lib6/gmm6/c/aq;)V

    iput-object v2, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->z:Lcom/google/maps/api/android/lib6/gmm6/c/s;

    invoke-static {p0, v1}, Lcom/google/maps/api/android/lib6/gmm6/c/r;->a(Lcom/google/maps/api/android/lib6/gmm6/c/aq;Landroid/os/Handler;)Lcom/google/maps/api/android/lib6/gmm6/c/r;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->v:Lcom/google/maps/api/android/lib6/gmm6/c/r;

    iput-object p6, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->y:Lcom/google/maps/api/android/lib6/gmm6/c/n;

    iput-object p5, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->w:Lcom/google/maps/api/android/lib6/gmm6/c/ba;

    invoke-static/range {p11 .. p11}, Lcom/google/k/a/db;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->w:Lcom/google/maps/api/android/lib6/gmm6/c/ba;

    sget-object v2, Lcom/google/maps/api/android/lib6/gmm6/l/av;->a:Lcom/google/maps/api/android/lib6/gmm6/l/av;

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/gmm6/c/as;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/google/maps/api/android/lib6/gmm6/c/ba;->b(Lcom/google/maps/api/android/lib6/gmm6/l/av;Landroid/content/res/Resources;)Lcom/google/maps/api/android/lib6/gmm6/o/x;

    move-result-object v1

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/gmm6/o/bx;->a:Lcom/google/maps/api/android/lib6/gmm6/o/l;

    invoke-virtual {v2, v1}, Lcom/google/maps/api/android/lib6/gmm6/o/l;->a(Lcom/google/maps/api/android/lib6/gmm6/o/x;)V

    :cond_0
    iput-object p7, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->x:Lcom/google/maps/api/android/lib6/gmm6/c/ay;

    instance-of v1, p0, Landroid/view/SurfaceView;

    if-nez v1, :cond_2

    const/4 v5, 0x1

    :goto_1
    invoke-static {}, Lcom/google/maps/api/android/lib6/c/ah;->a()Ljava/util/concurrent/Executor;

    move-result-object v6

    new-instance v1, Lcom/google/maps/api/android/lib6/gmm6/c/z;

    move-object/from16 v0, p10

    iget-object v4, v0, Lcom/google/maps/api/android/lib6/c/dd;->a:Landroid/widget/RelativeLayout;

    move-object v2, p0

    move-object/from16 v3, p9

    invoke-direct/range {v1 .. v6}, Lcom/google/maps/api/android/lib6/gmm6/c/z;-><init>(Lcom/google/maps/api/android/lib6/gmm6/c/aq;Landroid/view/View;Landroid/view/View;ZLjava/util/concurrent/Executor;)V

    iput-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->A:Lcom/google/maps/api/android/lib6/gmm6/c/z;

    new-instance v1, Lcom/google/maps/api/android/lib6/gmm6/c/av;

    invoke-direct {v1, p0}, Lcom/google/maps/api/android/lib6/gmm6/c/av;-><init>(Lcom/google/maps/api/android/lib6/gmm6/c/as;)V

    iput-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->u:Lcom/google/maps/api/android/lib6/gmm6/o/bz;

    new-instance v1, Lcom/google/maps/api/android/lib6/c/ew;

    new-instance v2, Lcom/google/maps/api/android/lib6/gmm6/c/aw;

    invoke-direct {v2, p0}, Lcom/google/maps/api/android/lib6/gmm6/c/aw;-><init>(Lcom/google/maps/api/android/lib6/gmm6/c/as;)V

    invoke-direct {v1, v2}, Lcom/google/maps/api/android/lib6/c/ew;-><init>(Ljava/lang/Runnable;)V

    iput-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->t:Lcom/google/maps/api/android/lib6/c/ew;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->w:Lcom/google/maps/api/android/lib6/gmm6/c/ba;

    sget-object v2, Lcom/google/maps/api/android/lib6/gmm6/l/av;->o:Lcom/google/maps/api/android/lib6/gmm6/l/av;

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/gmm6/c/as;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/google/maps/api/android/lib6/gmm6/c/ba;->b(Lcom/google/maps/api/android/lib6/gmm6/l/av;Landroid/content/res/Resources;)Lcom/google/maps/api/android/lib6/gmm6/o/x;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/maps/api/android/lib6/gmm6/c/as;->a(Lcom/google/maps/api/android/lib6/gmm6/o/bo;)V

    invoke-static {}, Lcom/google/maps/api/android/lib6/gmm6/f;->a()Lcom/google/maps/api/android/lib6/gmm6/f;

    invoke-static {p2}, Lcom/google/maps/api/android/lib6/gmm6/f;->a(Landroid/content/res/Resources;)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, Lcom/google/maps/api/android/lib6/gmm6/o/e/c;->u:Lcom/google/maps/api/android/lib6/gmm6/o/e/c;

    :goto_2
    iget-object v2, p0, Lcom/google/maps/api/android/lib6/gmm6/o/bx;->a:Lcom/google/maps/api/android/lib6/gmm6/o/l;

    invoke-virtual {v2, v1}, Lcom/google/maps/api/android/lib6/gmm6/o/l;->a(Lcom/google/maps/api/android/lib6/gmm6/o/e/c;)V

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->r:Lcom/google/maps/api/android/lib6/gmm6/c/h;

    iget-object v1, v1, Lcom/google/maps/api/android/lib6/gmm6/c/h;->b:Lcom/google/maps/api/android/lib6/gmm6/o/bm;

    invoke-virtual {p0, v1}, Lcom/google/maps/api/android/lib6/gmm6/c/as;->a(Lcom/google/maps/api/android/lib6/gmm6/o/bo;)V

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->q:Lcom/google/maps/api/android/lib6/gmm6/c/u;

    invoke-virtual {v1}, Lcom/google/maps/api/android/lib6/gmm6/c/u;->d()Lcom/google/maps/api/android/lib6/gmm6/o/bo;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/maps/api/android/lib6/gmm6/c/as;->a(Lcom/google/maps/api/android/lib6/gmm6/o/bo;)V

    return-void

    :cond_1
    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_2
    const/4 v5, 0x0

    goto :goto_1

    :cond_3
    sget-object v1, Lcom/google/maps/api/android/lib6/gmm6/o/e/c;->t:Lcom/google/maps/api/android/lib6/gmm6/o/e/c;

    goto :goto_2
.end method

.method private J()V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->B:Lcom/google/android/gms/maps/internal/ap;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->C:Lcom/google/android/gms/maps/internal/av;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/o/bx;->b:Lcom/google/maps/api/android/lib6/gmm6/o/bz;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->u:Lcom/google/maps/api/android/lib6/gmm6/o/bz;

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/o/bx;->b:Lcom/google/maps/api/android/lib6/gmm6/o/bz;

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/maps/api/android/lib6/gmm6/c/as;)Lcom/google/android/gms/maps/internal/av;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->C:Lcom/google/android/gms/maps/internal/av;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/content/res/Resources;Lcom/google/maps/api/android/lib6/c/dz;Ljava/util/concurrent/ScheduledExecutorService;Landroid/view/View;Lcom/google/maps/api/android/lib6/c/dd;Lcom/google/maps/api/android/lib6/c/cd;Ljava/lang/String;ZLandroid/widget/TextView;Lcom/google/maps/api/android/lib6/c/bz;)Lcom/google/maps/api/android/lib6/gmm6/c/aq;
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Lcom/google/maps/api/android/lib6/gmm6/c/k;->a(Landroid/content/Context;Lcom/google/maps/api/android/lib6/c/dz;)V

    invoke-virtual/range {p2 .. p2}, Lcom/google/maps/api/android/lib6/c/dz;->a()Lcom/google/maps/api/android/lib6/b/h;

    move-result-object v2

    new-instance v5, Lcom/google/maps/api/android/lib6/gmm6/o/ae;

    move-object/from16 v0, p1

    invoke-direct {v5, v0}, Lcom/google/maps/api/android/lib6/gmm6/o/ae;-><init>(Landroid/content/res/Resources;)V

    new-instance v6, Lcom/google/maps/api/android/lib6/gmm6/o/ao;

    invoke-direct {v6, v2}, Lcom/google/maps/api/android/lib6/gmm6/o/ao;-><init>(Lcom/google/maps/api/android/lib6/b/o;)V

    new-instance v3, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-static {}, Lcom/google/maps/api/android/lib6/gmm6/j/q;->a()Lcom/google/maps/api/android/lib6/gmm6/j/q;

    move-result-object v4

    move-object/from16 v0, p6

    invoke-static {v4, v3, v0}, Lcom/google/maps/api/android/lib6/gmm6/c/n;->a(Lcom/google/maps/api/android/lib6/gmm6/j/q;Landroid/os/Handler;Lcom/google/maps/api/android/lib6/c/cd;)Lcom/google/maps/api/android/lib6/gmm6/c/n;

    move-result-object v8

    new-instance v7, Lcom/google/maps/api/android/lib6/gmm6/c/at;

    move-object/from16 v0, p7

    invoke-direct {v7, v0, v8}, Lcom/google/maps/api/android/lib6/gmm6/c/at;-><init>(Ljava/lang/String;Lcom/google/maps/api/android/lib6/gmm6/c/n;)V

    invoke-static/range {p7 .. p7}, Lcom/google/k/a/db;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/google/maps/api/android/lib6/gmm6/l/av;->j:Lcom/google/maps/api/android/lib6/gmm6/l/av;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v3, v0, v1, v2}, Lcom/google/maps/api/android/lib6/gmm6/o/ad;->a(Lcom/google/maps/api/android/lib6/gmm6/l/av;Landroid/content/Context;Landroid/content/res/Resources;Lcom/google/maps/api/android/lib6/b/h;)Lcom/google/maps/api/android/lib6/gmm6/m/y;

    sget-object v3, Lcom/google/maps/api/android/lib6/gmm6/l/av;->m:Lcom/google/maps/api/android/lib6/gmm6/l/av;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v3, v0, v1, v2}, Lcom/google/maps/api/android/lib6/gmm6/o/ad;->a(Lcom/google/maps/api/android/lib6/gmm6/l/av;Landroid/content/Context;Landroid/content/res/Resources;Lcom/google/maps/api/android/lib6/b/h;)Lcom/google/maps/api/android/lib6/gmm6/m/y;

    :cond_0
    new-instance v9, Lcom/google/maps/api/android/lib6/gmm6/c/ay;

    invoke-direct {v9}, Lcom/google/maps/api/android/lib6/gmm6/c/ay;-><init>()V

    new-instance v2, Lcom/google/maps/api/android/lib6/gmm6/c/as;

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v10, p3

    move-object/from16 v11, p4

    move-object/from16 v12, p5

    move-object/from16 v13, p7

    move/from16 v14, p8

    move-object/from16 v15, p9

    move-object/from16 v16, p10

    invoke-direct/range {v2 .. v16}, Lcom/google/maps/api/android/lib6/gmm6/c/as;-><init>(Landroid/content/Context;Landroid/content/res/Resources;Lcom/google/maps/api/android/lib6/gmm6/o/ae;Lcom/google/maps/api/android/lib6/gmm6/o/ao;Lcom/google/maps/api/android/lib6/gmm6/c/ba;Lcom/google/maps/api/android/lib6/gmm6/c/n;Lcom/google/maps/api/android/lib6/gmm6/c/ay;Ljava/util/concurrent/ScheduledExecutorService;Landroid/view/View;Lcom/google/maps/api/android/lib6/c/dd;Ljava/lang/String;ZLandroid/widget/TextView;Lcom/google/maps/api/android/lib6/c/bz;)V

    return-object v2
.end method

.method static synthetic a(Lcom/google/maps/api/android/lib6/gmm6/o/x;Ljava/lang/String;)V
    .locals 1

    invoke-static {p1}, Lcom/google/k/a/db;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/maps/api/android/lib6/gmm6/l/cg;

    invoke-direct {v0}, Lcom/google/maps/api/android/lib6/gmm6/l/cg;-><init>()V

    iput-object p1, v0, Lcom/google/maps/api/android/lib6/gmm6/l/cg;->a:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/l/cg;->a()Lcom/google/maps/api/android/lib6/gmm6/l/cf;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/gmm6/o/x;->a(Lcom/google/maps/api/android/lib6/gmm6/l/ak;)Z

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/google/maps/api/android/lib6/gmm6/c/as;)Lcom/google/android/gms/maps/internal/ap;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->B:Lcom/google/android/gms/maps/internal/ap;

    return-object v0
.end method

.method static synthetic c(Lcom/google/maps/api/android/lib6/gmm6/c/as;)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/google/maps/api/android/lib6/gmm6/o/bx;->a(ZZ)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->x:Lcom/google/maps/api/android/lib6/gmm6/c/ay;

    invoke-static {}, Lcom/google/maps/api/android/lib6/b/y;->a()Lcom/google/p/a/d/g;

    move-result-object v0

    invoke-static {}, Lcom/google/maps/api/android/lib6/gmm6/a;->a()V

    sget-object v1, Lcom/google/maps/api/android/lib6/gmm6/e;->a:Lcom/google/maps/api/android/lib6/gmm6/e;

    invoke-static {v1, v0}, Lcom/google/maps/api/android/lib6/gmm6/a;->a(Lcom/google/maps/api/android/lib6/gmm6/e;Lcom/google/p/a/d/g;)V

    invoke-super {p0}, Lcom/google/maps/api/android/lib6/gmm6/o/bx;->a()V

    return-void
.end method

.method public final a(FF)V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->M:Lcom/google/maps/api/android/lib6/c/et;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/et;->d()V

    invoke-super {p0, p1, p2}, Lcom/google/maps/api/android/lib6/gmm6/o/bx;->a(FF)V

    return-void
.end method

.method public final a(I)V
    .locals 4

    const/4 v2, 0x0

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    sget-object v1, Lcom/google/maps/api/android/lib6/gmm6/l/av;->a:Lcom/google/maps/api/android/lib6/gmm6/l/av;

    sget-object v0, Lcom/google/maps/api/android/lib6/gmm6/o/bc;->a:Lcom/google/maps/api/android/lib6/gmm6/o/bc;

    :goto_0
    iget-object v3, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->F:Lcom/google/maps/api/android/lib6/gmm6/o/bo;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->F:Lcom/google/maps/api/android/lib6/gmm6/o/bo;

    invoke-virtual {p0, v3}, Lcom/google/maps/api/android/lib6/gmm6/c/as;->b(Lcom/google/maps/api/android/lib6/gmm6/o/bo;)V

    iput-object v2, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->F:Lcom/google/maps/api/android/lib6/gmm6/o/bo;

    :cond_0
    if-eqz v1, :cond_1

    sget-object v2, Lcom/google/maps/api/android/lib6/gmm6/l/av;->a:Lcom/google/maps/api/android/lib6/gmm6/l/av;

    if-eq v1, v2, :cond_1

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->w:Lcom/google/maps/api/android/lib6/gmm6/c/ba;

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/gmm6/c/as;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lcom/google/maps/api/android/lib6/gmm6/c/ba;->a(Lcom/google/maps/api/android/lib6/gmm6/l/av;Landroid/content/res/Resources;)Lcom/google/maps/api/android/lib6/gmm6/o/x;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->F:Lcom/google/maps/api/android/lib6/gmm6/o/bo;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->F:Lcom/google/maps/api/android/lib6/gmm6/o/bo;

    invoke-virtual {p0, v1}, Lcom/google/maps/api/android/lib6/gmm6/c/as;->a(Lcom/google/maps/api/android/lib6/gmm6/o/bo;)V

    :cond_1
    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/o/bx;->a:Lcom/google/maps/api/android/lib6/gmm6/o/l;

    invoke-virtual {v1, v0}, Lcom/google/maps/api/android/lib6/gmm6/o/l;->b(Lcom/google/maps/api/android/lib6/gmm6/o/bc;)V

    return-void

    :pswitch_1
    sget-object v0, Lcom/google/maps/api/android/lib6/gmm6/o/bc;->e:Lcom/google/maps/api/android/lib6/gmm6/o/bc;

    move-object v1, v2

    goto :goto_0

    :pswitch_2
    sget-object v1, Lcom/google/maps/api/android/lib6/gmm6/l/av;->d:Lcom/google/maps/api/android/lib6/gmm6/l/av;

    sget-object v0, Lcom/google/maps/api/android/lib6/gmm6/o/bc;->e:Lcom/google/maps/api/android/lib6/gmm6/o/bc;

    goto :goto_0

    :pswitch_3
    sget-object v1, Lcom/google/maps/api/android/lib6/gmm6/l/av;->d:Lcom/google/maps/api/android/lib6/gmm6/l/av;

    sget-object v0, Lcom/google/maps/api/android/lib6/gmm6/o/bc;->b:Lcom/google/maps/api/android/lib6/gmm6/o/bc;

    goto :goto_0

    :pswitch_4
    sget-object v1, Lcom/google/maps/api/android/lib6/gmm6/l/av;->e:Lcom/google/maps/api/android/lib6/gmm6/l/av;

    sget-object v0, Lcom/google/maps/api/android/lib6/gmm6/o/bc;->d:Lcom/google/maps/api/android/lib6/gmm6/o/bc;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/maps/internal/ap;)V
    .locals 0

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->B:Lcom/google/android/gms/maps/internal/ap;

    invoke-direct {p0}, Lcom/google/maps/api/android/lib6/gmm6/c/as;->J()V

    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/internal/av;)V
    .locals 0

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->C:Lcom/google/android/gms/maps/internal/av;

    invoke-direct {p0}, Lcom/google/maps/api/android/lib6/gmm6/c/as;->J()V

    return-void
.end method

.method public final a(Lcom/google/maps/api/android/lib6/gmm6/c/ar;)V
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/o/bx;->c:Lcom/google/maps/api/android/lib6/gmm6/o/by;

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/maps/api/android/lib6/gmm6/c/ax;

    invoke-direct {v0, p1}, Lcom/google/maps/api/android/lib6/gmm6/c/ax;-><init>(Lcom/google/maps/api/android/lib6/gmm6/c/ar;)V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/o/bx;->c:Lcom/google/maps/api/android/lib6/gmm6/o/by;

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 2

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->D:Lcom/google/maps/api/android/lib6/gmm6/o/bo;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->w:Lcom/google/maps/api/android/lib6/gmm6/c/ba;

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/gmm6/c/as;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/gmm6/c/ba;->a(Landroid/content/res/Resources;)Lcom/google/maps/api/android/lib6/gmm6/o/x;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->D:Lcom/google/maps/api/android/lib6/gmm6/o/bo;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->D:Lcom/google/maps/api/android/lib6/gmm6/o/bo;

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/gmm6/c/as;->a(Lcom/google/maps/api/android/lib6/gmm6/o/bo;)V

    :cond_0
    :goto_0
    sget-boolean v0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->N:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->D:Lcom/google/maps/api/android/lib6/gmm6/o/bo;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-eq p1, v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->D:Lcom/google/maps/api/android/lib6/gmm6/o/bo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->D:Lcom/google/maps/api/android/lib6/gmm6/o/bo;

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/gmm6/c/as;->b(Lcom/google/maps/api/android/lib6/gmm6/o/bo;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->D:Lcom/google/maps/api/android/lib6/gmm6/o/bo;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    return-void
.end method

.method public final a(ZZ)V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->M:Lcom/google/maps/api/android/lib6/c/et;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/et;->d()V

    invoke-super {p0, p1, p2}, Lcom/google/maps/api/android/lib6/gmm6/o/bx;->a(ZZ)V

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->x:Lcom/google/maps/api/android/lib6/gmm6/c/ay;

    invoke-static {}, Lcom/google/maps/api/android/lib6/gmm6/a;->b()V

    invoke-super {p0}, Lcom/google/maps/api/android/lib6/gmm6/o/bx;->b()V

    return-void
.end method

.method public final b(Z)Z
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->y:Lcom/google/maps/api/android/lib6/gmm6/c/n;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/gmm6/c/as;->o()Z

    move-result v0

    if-eq v0, p1, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->y:Lcom/google/maps/api/android/lib6/gmm6/c/n;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/c/n;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->w:Lcom/google/maps/api/android/lib6/gmm6/c/ba;

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/gmm6/c/as;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/gmm6/c/as;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/maps/api/android/lib6/gmm6/c/ba;->a(Landroid/content/Context;Landroid/content/res/Resources;)Lcom/google/maps/api/android/lib6/gmm6/o/cz;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->G:Lcom/google/maps/api/android/lib6/gmm6/o/cz;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->G:Lcom/google/maps/api/android/lib6/gmm6/o/cz;

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/gmm6/c/as;->a(Lcom/google/maps/api/android/lib6/gmm6/o/bo;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->s:Lcom/google/maps/api/android/lib6/gmm6/o/ao;

    new-instance v1, Lcom/google/maps/api/android/lib6/gmm6/c/az;

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->y:Lcom/google/maps/api/android/lib6/gmm6/c/n;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/maps/api/android/lib6/gmm6/c/az;-><init>(Lcom/google/maps/api/android/lib6/gmm6/c/n;B)V

    iput-object v1, v0, Lcom/google/maps/api/android/lib6/gmm6/o/ao;->d:Lcom/google/maps/api/android/lib6/gmm6/o/ap;

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/gmm6/c/as;->o()Z

    move-result v0

    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->s:Lcom/google/maps/api/android/lib6/gmm6/o/ao;

    iput-object v1, v0, Lcom/google/maps/api/android/lib6/gmm6/o/ao;->d:Lcom/google/maps/api/android/lib6/gmm6/o/ap;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->G:Lcom/google/maps/api/android/lib6/gmm6/o/cz;

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/gmm6/c/as;->b(Lcom/google/maps/api/android/lib6/gmm6/o/bo;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->G:Lcom/google/maps/api/android/lib6/gmm6/o/cz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/o/cz;->m()V

    iput-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->G:Lcom/google/maps/api/android/lib6/gmm6/o/cz;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->y:Lcom/google/maps/api/android/lib6/gmm6/c/n;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/c/n;->b()V

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/gmm6/c/as;->b(Z)Z

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/gmm6/c/as;->h(Z)V

    invoke-super {p0}, Lcom/google/maps/api/android/lib6/gmm6/o/bx;->c()V

    return-void
.end method

.method public final c(Z)V
    .locals 3

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->E:Lcom/google/maps/api/android/lib6/gmm6/o/bo;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->w:Lcom/google/maps/api/android/lib6/gmm6/c/ba;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/o/bx;->a:Lcom/google/maps/api/android/lib6/gmm6/o/l;

    invoke-virtual {v1}, Lcom/google/maps/api/android/lib6/gmm6/o/l;->h()Lcom/google/maps/api/android/lib6/gmm6/o/x;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/gmm6/c/as;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/maps/api/android/lib6/gmm6/c/ba;->a(Lcom/google/maps/api/android/lib6/gmm6/o/x;Landroid/content/res/Resources;)Lcom/google/maps/api/android/lib6/gmm6/o/x;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->E:Lcom/google/maps/api/android/lib6/gmm6/o/bo;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->E:Lcom/google/maps/api/android/lib6/gmm6/o/bo;

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/gmm6/c/as;->a(Lcom/google/maps/api/android/lib6/gmm6/o/bo;)V

    :cond_0
    :goto_0
    sget-boolean v0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->N:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->E:Lcom/google/maps/api/android/lib6/gmm6/o/bo;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-eq p1, v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->E:Lcom/google/maps/api/android/lib6/gmm6/o/bo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->E:Lcom/google/maps/api/android/lib6/gmm6/o/bo;

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/gmm6/c/as;->b(Lcom/google/maps/api/android/lib6/gmm6/o/bo;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->E:Lcom/google/maps/api/android/lib6/gmm6/o/bo;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    return-void
.end method

.method public canScrollHorizontally(I)Z
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/o/bx;->d:Lcom/google/maps/api/android/lib6/gmm6/o/ak;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/o/ak;->a()Z

    move-result v0

    return v0
.end method

.method public canScrollVertically(I)Z
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/o/bx;->d:Lcom/google/maps/api/android/lib6/gmm6/o/ak;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/o/ak;->a()Z

    move-result v0

    return v0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->v:Lcom/google/maps/api/android/lib6/gmm6/c/r;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/c/r;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->t:Lcom/google/maps/api/android/lib6/c/ew;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/ew;->a()V

    return-void
.end method

.method public dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->M:Lcom/google/maps/api/android/lib6/c/et;

    invoke-virtual {v0, p1}, Lcom/google/maps/api/android/lib6/c/et;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/maps/api/android/lib6/gmm6/o/bx;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final e()Lcom/google/maps/api/android/lib6/gmm6/o/bj;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/o/bx;->a:Lcom/google/maps/api/android/lib6/gmm6/o/l;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/o/l;->f()Lcom/google/maps/api/android/lib6/gmm6/o/bj;

    move-result-object v0

    return-object v0
.end method

.method public final f()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public final g()Lcom/google/maps/api/android/lib6/c/dj;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->p:Lcom/google/maps/api/android/lib6/gmm6/c/c;

    return-object v0
.end method

.method public final h()Lcom/google/maps/api/android/lib6/c/am;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->r:Lcom/google/maps/api/android/lib6/gmm6/c/h;

    return-object v0
.end method

.method public final i()Lcom/google/maps/api/android/lib6/c/at;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->q:Lcom/google/maps/api/android/lib6/gmm6/c/u;

    return-object v0
.end method

.method public final j()Lcom/google/maps/api/android/lib6/c/aq;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->z:Lcom/google/maps/api/android/lib6/gmm6/c/s;

    return-object v0
.end method

.method public final k()Lcom/google/maps/api/android/lib6/c/e;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->y:Lcom/google/maps/api/android/lib6/gmm6/c/n;

    return-object v0
.end method

.method public final l()Lcom/google/maps/api/android/lib6/c/bi;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->A:Lcom/google/maps/api/android/lib6/gmm6/c/z;

    return-object v0
.end method

.method public final m()Lcom/google/maps/api/android/lib6/c/ad;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->v:Lcom/google/maps/api/android/lib6/gmm6/c/r;

    return-object v0
.end method

.method public final n()Z
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->D:Lcom/google/maps/api/android/lib6/gmm6/o/bo;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final o()Z
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->y:Lcom/google/maps/api/android/lib6/gmm6/c/n;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->y:Lcom/google/maps/api/android/lib6/gmm6/c/n;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/c/n;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/maps/api/android/lib6/gmm6/o/bx;->onSizeChanged(IIII)V

    return-void
.end method

.method public final p()Z
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->E:Lcom/google/maps/api/android/lib6/gmm6/o/bo;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final q()Ljava/lang/String;
    .locals 1

    const-string v0, "G"

    return-object v0
.end method

.method public final synthetic r()Lcom/google/maps/api/android/lib6/c/ba;
    .locals 6

    new-instance v0, Lcom/google/maps/api/android/lib6/gmm6/c/y;

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/gmm6/c/as;->u()Lcom/google/maps/api/android/lib6/gmm6/o/b/b;

    move-result-object v1

    iget v2, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->H:I

    iget v3, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->I:I

    iget v4, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->J:I

    iget v5, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->K:I

    invoke-direct/range {v0 .. v5}, Lcom/google/maps/api/android/lib6/gmm6/c/y;-><init>(Lcom/google/maps/api/android/lib6/gmm6/o/b/b;IIII)V

    return-object v0
.end method

.method public setPadding(IIII)V
    .locals 0

    iput p1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->H:I

    iput p2, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->I:I

    iput p3, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->J:I

    iput p4, p0, Lcom/google/maps/api/android/lib6/gmm6/c/as;->K:I

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/gmm6/c/as;->d()V

    return-void
.end method
