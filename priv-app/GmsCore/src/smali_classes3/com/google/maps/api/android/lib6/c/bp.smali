.class final Lcom/google/maps/api/android/lib6/c/bp;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/maps/api/android/lib6/c/br;


# instance fields
.field private synthetic a:Lcom/google/maps/api/android/lib6/c/bo;


# direct methods
.method constructor <init>(Lcom/google/maps/api/android/lib6/c/bo;)V
    .locals 0

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/c/bp;->a:Lcom/google/maps/api/android/lib6/c/bo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/google/android/gms/maps/model/StreetViewPanoramaLocation;)V
    .locals 2

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bp;->a:Lcom/google/maps/api/android/lib6/c/bo;

    invoke-static {v0}, Lcom/google/maps/api/android/lib6/c/bo;->a(Lcom/google/maps/api/android/lib6/c/bo;)Lcom/google/maps/api/android/lib6/c/bs;

    move-result-object v0

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/c/bs;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bp;->a:Lcom/google/maps/api/android/lib6/c/bo;

    invoke-static {v0}, Lcom/google/maps/api/android/lib6/c/bo;->a(Lcom/google/maps/api/android/lib6/c/bo;)Lcom/google/maps/api/android/lib6/c/bs;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/maps/api/android/lib6/c/bs;->a(Z)V

    :goto_0
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bp;->a:Lcom/google/maps/api/android/lib6/c/bo;

    invoke-static {v0}, Lcom/google/maps/api/android/lib6/c/bo;->b(Lcom/google/maps/api/android/lib6/c/bo;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/maps/api/android/lib6/c/ax;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bp;->a:Lcom/google/maps/api/android/lib6/c/bo;

    iget-object v1, p2, Lcom/google/android/gms/maps/model/StreetViewPanoramaLocation;->b:Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v0, v1}, Lcom/google/maps/api/android/lib6/c/bo;->b(Lcom/google/android/gms/maps/model/LatLng;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bp;->a:Lcom/google/maps/api/android/lib6/c/bo;

    invoke-static {v0}, Lcom/google/maps/api/android/lib6/c/bo;->a(Lcom/google/maps/api/android/lib6/c/bo;)Lcom/google/maps/api/android/lib6/c/bs;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/maps/api/android/lib6/c/bs;->a(Z)V

    goto :goto_0
.end method
