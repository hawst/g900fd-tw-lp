.class public Lcom/google/maps/api/android/lib6/c/h;
.super Lcom/google/android/gms/maps/internal/r;


# instance fields
.field private final a:Lcom/google/maps/api/android/lib6/c/bz;

.field private final b:Landroid/view/ViewGroup;

.field private final c:Lcom/google/maps/api/android/lib6/c/g;

.field private d:Lcom/google/android/gms/maps/internal/n;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/maps/api/android/lib6/c/h;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lcom/google/maps/api/android/lib6/c/bz;Landroid/view/ViewGroup;Lcom/google/maps/api/android/lib6/c/g;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/maps/internal/r;-><init>()V

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/c/h;->a:Lcom/google/maps/api/android/lib6/c/bz;

    iput-object p2, p0, Lcom/google/maps/api/android/lib6/c/h;->b:Landroid/view/ViewGroup;

    iput-object p3, p0, Lcom/google/maps/api/android/lib6/c/h;->c:Lcom/google/maps/api/android/lib6/c/g;

    return-void
.end method

.method private a(Lcom/google/android/gms/maps/model/internal/s;)Landroid/view/View;
    .locals 2

    const/4 v1, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/h;->d:Lcom/google/android/gms/maps/internal/n;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/h;->d:Lcom/google/android/gms/maps/internal/n;

    invoke-interface {v0, p1}, Lcom/google/android/gms/maps/internal/n;->a(Lcom/google/android/gms/maps/model/internal/s;)Lcom/google/android/gms/b/l;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Lcom/google/android/gms/b/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    if-nez v0, :cond_4

    :try_start_1
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/h;->d:Lcom/google/android/gms/maps/internal/n;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/h;->d:Lcom/google/android/gms/maps/internal/n;

    invoke-interface {v0, p1}, Lcom/google/android/gms/maps/internal/n;->b(Lcom/google/android/gms/maps/model/internal/s;)Lcom/google/android/gms/b/l;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Lcom/google/android/gms/b/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    if-nez v0, :cond_3

    invoke-interface {p1}, Lcom/google/android/gms/maps/model/internal/s;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/k/a/db;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :goto_2
    return-object v1

    :cond_0
    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1

    :cond_1
    move-object v0, v1

    goto :goto_1

    :catch_1
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1

    :cond_2
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/h;->c:Lcom/google/maps/api/android/lib6/c/g;

    invoke-interface {p1}, Lcom/google/android/gms/maps/model/internal/s;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/maps/api/android/lib6/c/g;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/h;->c:Lcom/google/maps/api/android/lib6/c/g;

    invoke-interface {p1}, Lcom/google/android/gms/maps/model/internal/s;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/maps/api/android/lib6/c/g;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/h;->c:Lcom/google/maps/api/android/lib6/c/g;

    :cond_3
    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/h;->b:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/h;->b:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/h;->b:Landroid/view/ViewGroup;

    :cond_4
    move-object v1, v0

    goto :goto_2
.end method

.method public static a(Lcom/google/maps/api/android/lib6/c/bz;Landroid/content/Context;Landroid/content/res/Resources;)Lcom/google/maps/api/android/lib6/c/h;
    .locals 3

    const/4 v2, -0x2

    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    sget v1, Lcom/google/android/gms/maps/ab;->O:I

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-static {p1}, Lcom/google/maps/api/android/lib6/c/g;->a(Landroid/content/Context;)Lcom/google/maps/api/android/lib6/c/g;

    move-result-object v1

    new-instance v2, Lcom/google/maps/api/android/lib6/c/h;

    invoke-direct {v2, p0, v0, v1}, Lcom/google/maps/api/android/lib6/c/h;-><init>(Lcom/google/maps/api/android/lib6/c/bz;Landroid/view/ViewGroup;Lcom/google/maps/api/android/lib6/c/g;)V

    return-object v2
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/maps/model/internal/s;II)Landroid/graphics/Bitmap;
    .locals 7

    const/4 v6, 0x0

    const/4 v5, -0x2

    const/high16 v3, -0x80000000

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/maps/api/android/lib6/c/h;->a(Lcom/google/android/gms/maps/model/internal/s;)Landroid/view/View;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    if-nez v1, :cond_1

    :cond_0
    :goto_1
    return-object v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    move-object v1, v0

    goto :goto_0

    :cond_1
    invoke-static {p2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {p3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    new-instance v4, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v4, v5, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v1, v2, v3}, Landroid/view/View;->measure(II)V

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    invoke-virtual {v1, v6, v6, v2, v3}, Landroid/view/View;->layout(IIII)V

    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/maps/internal/n;)V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/h;->a:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/c/h;->d:Lcom/google/android/gms/maps/internal/n;

    return-void
.end method
