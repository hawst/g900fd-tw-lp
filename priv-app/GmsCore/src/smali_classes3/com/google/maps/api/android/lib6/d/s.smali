.class public final Lcom/google/maps/api/android/lib6/d/s;
.super Lcom/google/maps/api/android/lib6/c/a;


# direct methods
.method protected constructor <init>(Landroid/view/View;Landroid/view/View;Landroid/view/View;Ljava/util/concurrent/Executor;)V
    .locals 6

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/maps/api/android/lib6/c/a;-><init>(Landroid/view/View;Landroid/view/View;Landroid/view/View;ZLjava/util/concurrent/Executor;)V

    return-void
.end method

.method static synthetic a(Lcom/google/maps/api/android/lib6/d/s;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/s;->a:Landroid/view/View;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/maps/internal/cc;Landroid/graphics/Bitmap;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/google/maps/api/android/lib6/d/s;->b(Lcom/google/android/gms/maps/internal/cc;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method static synthetic a(Lcom/google/maps/api/android/lib6/d/s;Landroid/graphics/Bitmap;Z)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/google/maps/api/android/lib6/d/s;->a(Landroid/graphics/Bitmap;Z)V

    return-void
.end method


# virtual methods
.method protected final a(Landroid/graphics/Bitmap;Lcom/google/android/gms/maps/internal/cc;)V
    .locals 3

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/s;->b:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/maps/api/android/lib6/d/t;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2, p2}, Lcom/google/maps/api/android/lib6/d/t;-><init>(Lcom/google/maps/api/android/lib6/d/s;Landroid/graphics/Bitmap;ZLcom/google/android/gms/maps/internal/cc;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
