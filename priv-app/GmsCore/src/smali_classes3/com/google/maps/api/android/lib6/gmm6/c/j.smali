.class public final Lcom/google/maps/api/android/lib6/gmm6/c/j;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/maps/api/android/lib6/c/ak;


# instance fields
.field a:Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;

.field final b:Lcom/google/maps/api/android/lib6/c/aj;

.field private final c:Lcom/google/maps/api/android/lib6/gmm6/c/h;


# direct methods
.method public constructor <init>(Lcom/google/maps/api/android/lib6/c/aj;Lcom/google/maps/api/android/lib6/gmm6/c/h;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    iput-object p2, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->c:Lcom/google/maps/api/android/lib6/gmm6/c/h;

    return-void
.end method

.method private a(Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLng;Landroid/graphics/Bitmap;FFFFZZZFF)Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;
    .locals 5

    new-instance v2, Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;

    invoke-static {p2}, Lcom/google/maps/api/android/lib6/gmm6/c/f;->b(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/maps/api/android/lib6/gmm6/l/h;

    move-result-object v1

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, p4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, p5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-direct {v2, v1, p3, v3, v4}, Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;-><init>(Lcom/google/maps/api/android/lib6/gmm6/l/h;Landroid/graphics/Bitmap;II)V

    invoke-virtual {v2, p1}, Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;->b(Ljava/lang/String;)V

    invoke-virtual {v2, p8}, Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;->a(Z)V

    invoke-virtual {v2, p9}, Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;->c(Z)V

    if-nez p10, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v1}, Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;->b(Z)V

    move/from16 v0, p11

    invoke-virtual {v2, v0}, Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;->a(F)V

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, p6

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, p7

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-virtual {v2, v1, v3}, Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;->b(II)V

    move/from16 v0, p12

    invoke-virtual {v2, v0}, Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;->b(F)V

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v2, v1}, Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;->a(Lcom/google/maps/api/android/lib6/c/aj;)V

    return-object v2

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 13

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->q()Landroid/graphics/Bitmap;

    move-result-object v3

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->p()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->s()F

    move-result v4

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->u()F

    move-result v5

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->w()F

    move-result v6

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->y()F

    move-result v7

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->B()Z

    move-result v8

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->C()Z

    move-result v9

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->D()Z

    move-result v10

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->E()F

    move-result v11

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->F()F

    move-result v12

    move-object v0, p0

    invoke-direct/range {v0 .. v12}, Lcom/google/maps/api/android/lib6/gmm6/c/j;->a(Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLng;Landroid/graphics/Bitmap;FFFFZZZFF)Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->a:Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->c:Lcom/google/maps/api/android/lib6/gmm6/c/h;

    invoke-virtual {v0, p0}, Lcom/google/maps/api/android/lib6/gmm6/c/h;->a(Lcom/google/maps/api/android/lib6/gmm6/c/j;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->c:Lcom/google/maps/api/android/lib6/gmm6/c/h;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/c/h;->a()V

    return-void
.end method

.method public final a(I)V
    .locals 14

    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->c()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->a:Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;

    invoke-static {v0}, Lcom/google/maps/api/android/lib6/gmm6/c/f;->b(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/maps/api/android/lib6/gmm6/l/h;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;->a(Lcom/google/maps/api/android/lib6/gmm6/l/h;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->c:Lcom/google/maps/api/android/lib6/gmm6/c/h;

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/gmm6/c/h;->b:Lcom/google/maps/api/android/lib6/gmm6/o/bm;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/o/bm;->d()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->c:Lcom/google/maps/api/android/lib6/gmm6/c/h;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/c/h;->a()V

    :cond_0
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->d()Ljava/lang/String;

    move-result-object v13

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->e()Ljava/lang/String;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->c:Lcom/google/maps/api/android/lib6/gmm6/c/h;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->a:Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;

    invoke-virtual {v0, v1}, Lcom/google/maps/api/android/lib6/gmm6/c/h;->a(Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->p()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->q()Landroid/graphics/Bitmap;

    move-result-object v3

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->r()F

    move-result v4

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->t()F

    move-result v5

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->v()F

    move-result v6

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->x()F

    move-result v7

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->B()Z

    move-result v8

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->C()Z

    move-result v9

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->D()Z

    move-result v10

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->E()F

    move-result v11

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->F()F

    move-result v12

    move-object v0, p0

    invoke-direct/range {v0 .. v12}, Lcom/google/maps/api/android/lib6/gmm6/c/j;->a(Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLng;Landroid/graphics/Bitmap;FFFFZZZFF)Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->a:Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->a:Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;

    invoke-virtual {v0, v13}, Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->a:Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;

    invoke-static {}, Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;->o()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->c:Lcom/google/maps/api/android/lib6/gmm6/c/h;

    invoke-virtual {v0, p0}, Lcom/google/maps/api/android/lib6/gmm6/c/h;->a(Lcom/google/maps/api/android/lib6/gmm6/c/j;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->c:Lcom/google/maps/api/android/lib6/gmm6/c/h;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/c/h;->a()V

    :cond_1
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->s()F

    move-result v0

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v1}, Lcom/google/maps/api/android/lib6/c/aj;->u()F

    move-result v1

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->a:Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;

    iget-object v3, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->a:Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;

    invoke-virtual {v3}, Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;->k()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iget-object v3, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->a:Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;

    invoke-virtual {v3}, Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;->k()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {v2, v0, v1}, Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;->a(II)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->c:Lcom/google/maps/api/android/lib6/gmm6/c/h;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/c/h;->a()V

    :cond_2
    and-int/lit8 v0, p1, 0x8

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->D()Z

    move-result v0

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->a:Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;->b(Z)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->c:Lcom/google/maps/api/android/lib6/gmm6/c/h;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/c/h;->a()V

    :cond_3
    and-int/lit8 v0, p1, 0x10

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->a:Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v1}, Lcom/google/maps/api/android/lib6/c/aj;->E()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;->a(F)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->c:Lcom/google/maps/api/android/lib6/gmm6/c/h;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/c/h;->a()V

    :cond_4
    and-int/lit8 v0, p1, 0x40

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->C()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->c:Lcom/google/maps/api/android/lib6/gmm6/c/h;

    invoke-virtual {v1, p0}, Lcom/google/maps/api/android/lib6/gmm6/c/h;->c(Lcom/google/maps/api/android/lib6/gmm6/c/j;)V

    :cond_5
    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->a:Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;

    invoke-virtual {v1, v0}, Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;->c(Z)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->c:Lcom/google/maps/api/android/lib6/gmm6/c/h;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/c/h;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->c:Lcom/google/maps/api/android/lib6/gmm6/c/h;

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/gmm6/c/h;->b:Lcom/google/maps/api/android/lib6/gmm6/o/bm;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/o/bm;->d()V

    :cond_6
    and-int/lit16 v0, p1, 0x400

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->a:Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v1}, Lcom/google/maps/api/android/lib6/c/aj;->F()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;->b(F)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->c:Lcom/google/maps/api/android/lib6/gmm6/c/h;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/c/h;->a()V

    :cond_7
    and-int/lit16 v0, p1, 0x200

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->w()F

    move-result v0

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v1}, Lcom/google/maps/api/android/lib6/c/aj;->y()F

    move-result v1

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->a:Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;

    iget-object v3, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->a:Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;

    invoke-virtual {v3}, Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;->k()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iget-object v3, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->a:Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;

    invoke-virtual {v3}, Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;->k()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {v2, v0, v1}, Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;->b(II)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->c:Lcom/google/maps/api/android/lib6/gmm6/c/h;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/c/h;->a()V

    :cond_8
    and-int/lit16 v0, p1, 0x80

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->z()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->a:Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;

    invoke-virtual {v1, v0}, Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->c:Lcom/google/maps/api/android/lib6/gmm6/c/h;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/c/h;->a()V

    :cond_9
    and-int/lit8 v0, p1, 0x20

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->a:Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v1}, Lcom/google/maps/api/android/lib6/c/aj;->B()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;->a(Z)V

    :cond_a
    and-int/lit16 v0, p1, 0x100

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->a:Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->A()Ljava/lang/String;

    invoke-static {}, Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;->o()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->c:Lcom/google/maps/api/android/lib6/gmm6/c/h;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/c/h;->a()V

    :cond_b
    return-void

    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->c:Lcom/google/maps/api/android/lib6/gmm6/c/h;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->a:Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;

    invoke-virtual {v0, v1}, Lcom/google/maps/api/android/lib6/gmm6/c/h;->a(Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->c:Lcom/google/maps/api/android/lib6/gmm6/c/h;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/c/h;->a()V

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->c:Lcom/google/maps/api/android/lib6/gmm6/c/h;

    invoke-virtual {v0, p0}, Lcom/google/maps/api/android/lib6/gmm6/c/h;->b(Lcom/google/maps/api/android/lib6/gmm6/c/j;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->c:Lcom/google/maps/api/android/lib6/gmm6/c/h;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/c/h;->a()V

    return-void
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->c:Lcom/google/maps/api/android/lib6/gmm6/c/h;

    invoke-virtual {v0, p0}, Lcom/google/maps/api/android/lib6/gmm6/c/h;->c(Lcom/google/maps/api/android/lib6/gmm6/c/j;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->c:Lcom/google/maps/api/android/lib6/gmm6/c/h;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/c/h;->a()V

    return-void
.end method

.method public final e()V
    .locals 10

    const-wide v8, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->a:Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;

    invoke-virtual {v1}, Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;->j()Lcom/google/maps/api/android/lib6/a/a/f;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/maps/model/LatLng;

    iget v3, v1, Lcom/google/maps/api/android/lib6/a/a/f;->a:I

    int-to-double v4, v3

    mul-double/2addr v4, v8

    iget v1, v1, Lcom/google/maps/api/android/lib6/a/a/f;->b:I

    int-to-double v6, v1

    mul-double/2addr v6, v8

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v0, v2}, Lcom/google/maps/api/android/lib6/c/aj;->b(Lcom/google/android/gms/maps/model/LatLng;)V

    return-void
.end method

.method public final f()Z
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->c:Lcom/google/maps/api/android/lib6/gmm6/c/h;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->a:Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;

    invoke-virtual {v0, v1}, Lcom/google/maps/api/android/lib6/gmm6/c/h;->b(Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;)Z

    move-result v0

    return v0
.end method

.method public final g()Landroid/graphics/Rect;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/j;->a:Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;->t()Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method
