.class final Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;
.super Landroid/support/v4/widget/ab;


# static fields
.field private static final b:[F

.field private static final c:[F


# instance fields
.field private A:Z

.field private B:Z

.field private C:[[I

.field private final D:F

.field private E:Z

.field private F:Z

.field private d:Lcom/google/maps/api/android/lib6/gmm6/streetview/q;

.field private e:[Lcom/google/maps/api/android/lib6/gmm6/streetview/t;

.field private f:I

.field private g:I

.field private h:F

.field private final i:Lcom/google/maps/api/android/lib6/gmm6/streetview/ar;

.field private final j:Lcom/google/maps/api/android/lib6/gmm6/streetview/ar;

.field private k:Z

.field private l:Z

.field private final m:Lcom/google/maps/api/android/lib6/gmm6/streetview/bd;

.field private final n:[F

.field private o:Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;

.field private final p:[Ljava/lang/CharSequence;

.field private final q:[Ljava/lang/CharSequence;

.field private final r:Landroid/graphics/Paint;

.field private final s:Landroid/graphics/Paint;

.field private t:Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;

.field private u:Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;

.field private final v:Ljava/lang/Object;

.field private w:I

.field private x:I

.field private y:J

.field private z:J


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/16 v0, 0x12

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->b:[F

    const/16 v0, 0x12

    new-array v0, v0, [F

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->c:[F

    const/16 v0, 0xf

    new-array v0, v0, [F

    aput v4, v0, v3

    const/high16 v1, 0x3f800000    # 1.0f

    aput v1, v0, v5

    aput v4, v0, v6

    const/high16 v1, 0x3f800000    # 1.0f

    aput v1, v0, v7

    const/4 v1, 0x4

    aput v4, v0, v1

    const/4 v1, 0x5

    const/high16 v2, 0x3f800000    # 1.0f

    aput v2, v0, v1

    const/4 v1, 0x6

    const/high16 v2, 0x3f800000    # 1.0f

    aput v2, v0, v1

    const/4 v1, 0x7

    aput v4, v0, v1

    const/16 v1, 0x8

    const/high16 v2, -0x40800000    # -1.0f

    aput v2, v0, v1

    const/16 v1, 0x9

    const/high16 v2, -0x40800000    # -1.0f

    aput v2, v0, v1

    const/16 v1, 0xa

    aput v4, v0, v1

    const/16 v1, 0xb

    const/high16 v2, -0x40800000    # -1.0f

    aput v2, v0, v1

    const/16 v1, 0xc

    const/high16 v2, -0x40800000    # -1.0f

    aput v2, v0, v1

    const/16 v1, 0xd

    aput v4, v0, v1

    const/16 v1, 0xe

    const/high16 v2, 0x3f800000    # 1.0f

    aput v2, v0, v1

    const/4 v0, 0x6

    new-array v0, v0, [B

    aput-byte v3, v0, v3

    aput-byte v5, v0, v5

    aput-byte v6, v0, v6

    aput-byte v7, v0, v7

    const/4 v1, 0x4

    const/4 v2, 0x4

    aput-byte v2, v0, v1

    const/4 v1, 0x5

    aput-byte v3, v0, v1

    const/16 v0, 0xc

    new-array v0, v0, [B

    aput-byte v3, v0, v3

    aput-byte v5, v0, v5

    aput-byte v6, v0, v6

    aput-byte v3, v0, v7

    const/4 v1, 0x4

    aput-byte v6, v0, v1

    const/4 v1, 0x5

    aput-byte v7, v0, v1

    const/4 v1, 0x6

    aput-byte v3, v0, v1

    const/4 v1, 0x7

    aput-byte v7, v0, v1

    const/16 v1, 0x8

    const/4 v2, 0x4

    aput-byte v2, v0, v1

    const/16 v1, 0x9

    aput-byte v3, v0, v1

    const/16 v1, 0xa

    const/4 v2, 0x4

    aput-byte v2, v0, v1

    const/16 v1, 0xb

    aput-byte v5, v0, v1

    return-void

    :array_0
    .array-data 4
        0x0
        -0x40b33333    # -0.8f
        0x3ed78812    # 0.42096f
        -0x41c538ef    # -0.1824f
        -0x40b33333    # -0.8f
        0x3e9a176e    # 0.30096f
        -0x41c538ef    # -0.1824f
        -0x40b33333    # -0.8f
        0x3e4ccccd    # 0.2f
        0x0
        -0x40b33333    # -0.8f
        0x3ea3d70a    # 0.32f
        0x3e3ac711    # 0.1824f
        -0x40b33333    # -0.8f
        0x3e4ccccd    # 0.2f
        0x3e3ac711    # 0.1824f
        -0x40b33333    # -0.8f
        0x3e9a176e    # 0.30096f
    .end array-data

    :array_1
    .array-data 4
        0x0
        -0x40a8f5c2    # -0.84000003f
        0x3ed78812    # 0.42096f
        -0x41c538ef    # -0.1824f
        -0x40a8f5c2    # -0.84000003f
        0x3e9a176e    # 0.30096f
        -0x41c538ef    # -0.1824f
        -0x40a8f5c2    # -0.84000003f
        0x3e4ccccd    # 0.2f
        0x0
        -0x40a8f5c2    # -0.84000003f
        0x3ea3d70a    # 0.32f
        0x3e3ac711    # 0.1824f
        -0x40a8f5c2    # -0.84000003f
        0x3e4ccccd    # 0.2f
        0x3e3ac711    # 0.1824f
        -0x40a8f5c2    # -0.84000003f
        0x3e9a176e    # 0.30096f
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/maps/api/android/lib6/gmm6/streetview/bd;Landroid/view/View;)V
    .locals 7

    const/4 v6, -0x1

    const/high16 v5, 0x42c80000    # 100.0f

    const/16 v4, 0xff

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p3}, Landroid/support/v4/widget/ab;-><init>(Landroid/view/View;)V

    iput-boolean v2, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->A:Z

    iput-boolean v2, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->B:Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->D:F

    new-instance v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ar;

    sget-object v1, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->b:[F

    invoke-direct {v0, v1}, Lcom/google/maps/api/android/lib6/gmm6/streetview/ar;-><init>([F)V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->i:Lcom/google/maps/api/android/lib6/gmm6/streetview/ar;

    new-instance v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ar;

    sget-object v1, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->c:[F

    invoke-direct {v0, v1}, Lcom/google/maps/api/android/lib6/gmm6/streetview/ar;-><init>([F)V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->j:Lcom/google/maps/api/android/lib6/gmm6/streetview/ar;

    iput-object p2, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->m:Lcom/google/maps/api/android/lib6/gmm6/streetview/bd;

    invoke-direct {p0}, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->f()V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/maps/x;->a:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->p:[Ljava/lang/CharSequence;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/maps/x;->b:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->q:[Ljava/lang/CharSequence;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->r:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->r:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->r:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->r:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->r:Landroid/graphics/Paint;

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->r:Landroid/graphics/Paint;

    invoke-virtual {v0, v6, v4, v4, v4}, Landroid/graphics/Paint;->setARGB(IIII)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->r:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->r:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->s:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->s:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->s:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->s:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->s:Landroid/graphics/Paint;

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->s:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->s:Landroid/graphics/Paint;

    const/high16 v1, 0x40400000    # 3.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->s:Landroid/graphics/Paint;

    invoke-virtual {v0, v6, v3, v3, v3}, Landroid/graphics/Paint;->setARGB(IIII)V

    new-instance v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;

    invoke-direct {v0}, Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;-><init>()V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->t:Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;

    new-instance v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;

    invoke-direct {v0}, Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;-><init>()V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->u:Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->u:Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->v:Ljava/lang/Object;

    const/16 v0, 0x8

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->n:[F

    return-void
.end method

.method private a(Ljavax/microedition/khronos/opengles/GL10;Lcom/google/maps/api/android/lib6/c/bx;)V
    .locals 12

    const/4 v11, 0x1

    const/4 v7, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    const/16 v10, 0x1700

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->C:[[I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->o:Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;

    iget v2, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->f:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->g:I

    int-to-float v4, v3

    const/4 v3, 0x3

    invoke-virtual {v0, v11, v3}, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->a(II)V

    const/16 v3, 0xde1

    iget v5, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->f:I

    invoke-interface {p1, v3, v5}, Ljavax/microedition/khronos/opengles/GL10;->glBindTexture(II)V

    const/16 v3, 0x1d00

    invoke-interface {p1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glShadeModel(I)V

    const/16 v3, 0xbe2

    invoke-interface {p1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    const/16 v3, 0x303

    invoke-interface {p1, v11, v3}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/high16 v3, 0x10000

    const/high16 v5, 0x10000

    const/high16 v8, 0x10000

    const/high16 v9, 0x10000

    invoke-interface {p1, v3, v5, v8, v9}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    iget-boolean v0, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->m:Z

    if-eqz v0, :cond_2

    const/16 v0, 0x1701

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    move-object v0, p1

    move v3, v1

    move v5, v1

    invoke-interface/range {v0 .. v6}, Ljavax/microedition/khronos/opengles/GL10;->glOrthof(FFFFFF)V

    invoke-interface {p1, v10}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    :cond_2
    invoke-interface {p1, v10}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    invoke-virtual {p2}, Lcom/google/maps/api/android/lib6/c/bx;->a()[F

    move-result-object v0

    invoke-interface {p1, v0, v7}, Ljavax/microedition/khronos/opengles/GL10;->glMultMatrixf([FI)V

    move v2, v7

    :goto_1
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->e:[Lcom/google/maps/api/android/lib6/gmm6/streetview/t;

    array-length v0, v0

    if-ge v2, v0, :cond_6

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->C:[[I

    aget-object v0, v0, v2

    aget v0, v0, v7

    const/4 v3, -0x1

    if-eq v0, v3, :cond_5

    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    const/high16 v0, 0x43340000    # 180.0f

    iget-object v3, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->d:Lcom/google/maps/api/android/lib6/gmm6/streetview/q;

    iget v3, v3, Lcom/google/maps/api/android/lib6/gmm6/streetview/q;->r:F

    sub-float/2addr v0, v3

    invoke-static {v0}, Lcom/google/maps/api/android/lib6/c/bj;->p(F)F

    move-result v0

    invoke-static {v0}, Landroid/util/FloatMath;->cos(F)F

    move-result v3

    invoke-static {v0}, Landroid/util/FloatMath;->sin(F)F

    move-result v0

    iget-object v4, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->d:Lcom/google/maps/api/android/lib6/gmm6/streetview/q;

    iget v4, v4, Lcom/google/maps/api/android/lib6/gmm6/streetview/q;->s:F

    neg-float v4, v4

    neg-float v0, v0

    invoke-interface {p1, v4, v3, v1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->e:[Lcom/google/maps/api/android/lib6/gmm6/streetview/t;

    aget-object v0, v0, v2

    iget v0, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/t;->a:F

    neg-float v0, v0

    invoke-interface {p1, v0, v1, v6, v1}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    const/high16 v0, -0x3f600000    # -5.0f

    invoke-interface {p1, v1, v1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    const/high16 v0, -0x3f600000    # -5.0f

    invoke-interface {p1, v1, v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    const/high16 v0, -0x3d4c0000    # -90.0f

    invoke-interface {p1, v0, v6, v1, v1}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    const/high16 v0, 0x42b40000    # 90.0f

    invoke-interface {p1, v0, v1, v1, v6}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    const v0, 0x3ccccccd    # 0.025f

    const v3, 0x3ccccccd    # 0.025f

    const v4, 0x3ccccccd    # 0.025f

    invoke-interface {p1, v0, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    const/high16 v0, -0x3d900000    # -60.0f

    iget v3, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->D:F

    mul-float/2addr v0, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v0, v3

    invoke-interface {p1, v1, v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    iget-object v3, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->o:Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->C:[[I

    aget-object v0, v0, v2

    aget v0, v0, v7

    const/4 v4, 0x3

    const/4 v5, 0x3

    invoke-virtual {v3, v4, v5}, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->a(II)V

    invoke-static {v1}, Landroid/util/FloatMath;->floor(F)F

    move-result v4

    invoke-static {v1}, Landroid/util/FloatMath;->floor(F)F

    move-result v5

    iget-boolean v8, v3, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->m:Z

    if-eqz v8, :cond_3

    invoke-interface {p1, v10}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-interface {p1, v4, v5, v1}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    :cond_3
    iget-object v4, v3, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->l:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/aj;

    const/16 v4, 0xde1

    invoke-interface {p1, v4}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/aj;->a:Lcom/google/maps/api/android/lib6/gmm6/streetview/aa;

    invoke-virtual {v0, p1, v11}, Lcom/google/maps/api/android/lib6/gmm6/streetview/aa;->a(Ljavax/microedition/khronos/opengles/GL10;Z)V

    iget-boolean v0, v3, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->m:Z

    if-eqz v0, :cond_4

    invoke-interface {p1, v10}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    :cond_4
    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    :cond_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_1

    :cond_6
    invoke-interface {p1, v10}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->o:Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v11}, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->a(II)V

    const/16 v1, 0xbe2

    invoke-interface {p1, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    iget-boolean v0, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->m:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x1701

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    invoke-interface {p1, v10}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto/16 :goto_0
.end method

.method private b(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->l:Z

    iput-boolean v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->k:Z

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->o:Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->o:Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;

    invoke-virtual {v0, p1}, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->b(Ljavax/microedition/khronos/opengles/GL10;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->o:Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;

    :cond_0
    return-void
.end method

.method private b(Ljavax/microedition/khronos/opengles/GL10;Lcom/google/maps/api/android/lib6/c/bx;)V
    .locals 30

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->h:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    const/16 v2, 0x1701

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface/range {p1 .. p1}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    invoke-virtual/range {p2 .. p2}, Lcom/google/maps/api/android/lib6/c/bx;->c()F

    move-result v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->h:F

    invoke-static {v3}, Lcom/google/maps/api/android/lib6/gmm6/streetview/be;->a(F)F

    move-result v3

    mul-float/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->h:F

    const v4, 0x3dcccccd    # 0.1f

    const/high16 v5, 0x42c80000    # 100.0f

    move-object/from16 v0, p1

    invoke-static {v0, v2, v3, v4, v5}, Landroid/opengl/GLU;->gluPerspective(Ljavax/microedition/khronos/opengles/GL10;FFFF)V

    const/16 v2, 0x1700

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface/range {p1 .. p1}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/high16 v4, -0x40000000    # -2.0f

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    invoke-virtual/range {p2 .. p2}, Lcom/google/maps/api/android/lib6/c/bx;->a()[F

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glMultMatrixf([FI)V

    const/4 v2, 0x0

    const/high16 v3, -0x40800000    # -1.0f

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    const/16 v2, 0x1d00

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljavax/microedition/khronos/opengles/GL10;->glShadeModel(I)V

    const/16 v2, 0xbe2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    const/16 v2, 0x302

    const/16 v3, 0x303

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->m:Lcom/google/maps/api/android/lib6/gmm6/streetview/bd;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/maps/api/android/lib6/gmm6/streetview/bd;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    const/16 v2, 0x1700

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    const/4 v2, 0x0

    move v13, v2

    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->e:[Lcom/google/maps/api/android/lib6/gmm6/streetview/t;

    array-length v2, v2

    if-ge v13, v2, :cond_10

    move-object/from16 v0, p2

    iget v2, v0, Lcom/google/maps/api/android/lib6/c/bx;->a:F

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->e:[Lcom/google/maps/api/android/lib6/gmm6/streetview/t;

    aget-object v5, v3, v13

    invoke-interface/range {p1 .. p1}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    iget v3, v5, Lcom/google/maps/api/android/lib6/gmm6/streetview/t;->a:F

    sub-float/2addr v2, v3

    const/high16 v3, 0x43340000    # 180.0f

    iget v4, v5, Lcom/google/maps/api/android/lib6/gmm6/streetview/t;->a:F

    sub-float/2addr v3, v4

    const/high16 v4, 0x43340000    # 180.0f

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->d:Lcom/google/maps/api/android/lib6/gmm6/streetview/q;

    iget v6, v6, Lcom/google/maps/api/android/lib6/gmm6/streetview/q;->r:F

    sub-float/2addr v4, v6

    invoke-static {v4}, Lcom/google/maps/api/android/lib6/c/bj;->p(F)F

    move-result v4

    invoke-static {v4}, Landroid/util/FloatMath;->cos(F)F

    move-result v6

    invoke-static {v4}, Landroid/util/FloatMath;->sin(F)F

    move-result v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->d:Lcom/google/maps/api/android/lib6/gmm6/streetview/q;

    iget v7, v7, Lcom/google/maps/api/android/lib6/gmm6/streetview/q;->s:F

    neg-float v7, v7

    const/4 v8, 0x0

    neg-float v4, v4

    move-object/from16 v0, p1

    invoke-interface {v0, v7, v6, v8, v4}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    const/4 v4, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v3, v4, v6, v7}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    iget v3, v5, Lcom/google/maps/api/android/lib6/gmm6/streetview/t;->d:I

    shr-int/lit8 v4, v3, 0x18

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x8

    const v6, 0x3f8ccccd    # 1.1f

    int-to-float v4, v4

    mul-float/2addr v4, v6

    float-to-int v4, v4

    const/high16 v6, 0x10000

    invoke-static {v4, v6}, Ljava/lang/Math;->min(II)I

    move-result v4

    shr-int/lit8 v6, v3, 0x10

    and-int/lit16 v6, v6, 0xff

    shl-int/lit8 v6, v6, 0x8

    shr-int/lit8 v7, v3, 0x8

    and-int/lit16 v7, v7, 0xff

    shl-int/lit8 v7, v7, 0x8

    shr-int/lit8 v3, v3, 0x0

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    move-object/from16 v0, p1

    invoke-interface {v0, v6, v7, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    invoke-static {v2}, Lcom/google/maps/api/android/lib6/c/bj;->p(F)F

    move-result v2

    invoke-static {v2}, Landroid/util/FloatMath;->cos(F)F

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v3, v2

    mul-float/2addr v3, v2

    const/high16 v4, 0x3f800000    # 1.0f

    sub-float/2addr v3, v4

    const/high16 v4, 0x40800000    # 4.0f

    mul-float/2addr v4, v2

    mul-float/2addr v4, v2

    const/high16 v6, 0x40400000    # 3.0f

    sub-float/2addr v4, v6

    mul-float/2addr v4, v2

    const v6, 0x3e4ccccd    # 0.2f

    const/high16 v7, 0x3e800000    # 0.25f

    const v8, 0x3f2e147b    # 0.68f

    const v9, 0x3eb5c28f    # 0.355f

    mul-float/2addr v2, v9

    add-float/2addr v2, v8

    const v8, 0x3ea3d70a    # 0.32f

    mul-float/2addr v3, v8

    sub-float/2addr v2, v3

    const v3, 0x3e5c28f6    # 0.215f

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    mul-float/2addr v2, v7

    add-float/2addr v2, v6

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v3, v4, v2}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->x:I

    if-ne v2, v13, :cond_5

    const/4 v2, 0x1

    :goto_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->w:I

    if-ne v3, v13, :cond_6

    const/4 v3, 0x1

    move v4, v3

    :goto_3
    if-eqz v4, :cond_11

    const/4 v2, 0x0

    move v3, v2

    :goto_4
    if-nez v3, :cond_1

    if-eqz v4, :cond_7

    :cond_1
    const/4 v2, 0x1

    move/from16 v20, v2

    :goto_5
    if-eqz v20, :cond_9

    if-eqz v3, :cond_8

    const v2, 0x8800

    const/16 v3, 0x6d00

    const v4, 0xad00

    const/high16 v6, 0x10000

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3, v4, v6}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    :goto_6
    invoke-interface/range {p1 .. p1}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    const v2, 0x3fa66666    # 1.3f

    const/high16 v3, 0x3f800000    # 1.0f

    const v4, 0x3fa66666    # 1.3f

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    :goto_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->m:Lcom/google/maps/api/android/lib6/gmm6/streetview/bd;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/maps/api/android/lib6/gmm6/streetview/bd;->b(Ljavax/microedition/khronos/opengles/GL10;)V

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->F:Z

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->h:F

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_3

    :cond_2
    const/4 v2, 0x0

    const/high16 v3, 0x3f000000    # 0.5f

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    :cond_3
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0x5000

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3, v4, v6}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->j:Lcom/google/maps/api/android/lib6/gmm6/streetview/ar;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/maps/api/android/lib6/gmm6/streetview/ar;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    const/high16 v2, 0x10000

    const/high16 v3, 0x10000

    const/high16 v4, 0x10000

    const/high16 v6, 0x10000

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3, v4, v6}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->i:Lcom/google/maps/api/android/lib6/gmm6/streetview/ar;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/maps/api/android/lib6/gmm6/streetview/ar;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->t:Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->m:Lcom/google/maps/api/android/lib6/gmm6/streetview/bd;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->n:[F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->i:Lcom/google/maps/api/android/lib6/gmm6/streetview/ar;

    move-object/from16 v23, v0

    iget v0, v5, Lcom/google/maps/api/android/lib6/gmm6/streetview/t;->b:I

    move/from16 v24, v0

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/google/maps/api/android/lib6/gmm6/streetview/bd;->b(Ljavax/microedition/khronos/opengles/GL10;)V

    move-object/from16 v0, v21

    iget v2, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;->d:I

    mul-int/lit8 v25, v2, 0x4

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;->a:[I

    move-object/from16 v26, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;->e:I

    move/from16 v27, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;->f:I

    move/from16 v28, v0

    move-object/from16 v0, v23

    iget-object v2, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ar;->a:Ljava/nio/FloatBuffer;

    invoke-virtual {v2}, Ljava/nio/FloatBuffer;->limit()I

    move-result v2

    div-int/lit8 v29, v2, 0x3

    const v7, 0x7fffffff

    const/high16 v6, -0x80000000

    const v5, 0x7fffffff

    const/high16 v4, -0x80000000

    const/4 v3, 0x0

    const/4 v2, 0x0

    move v14, v2

    move v15, v3

    move/from16 v16, v4

    move/from16 v17, v5

    move/from16 v18, v6

    move/from16 v19, v7

    :goto_8
    move/from16 v0, v29

    if-ge v14, v0, :cond_c

    mul-int/lit8 v2, v14, 0x3

    const/4 v3, 0x0

    move-object/from16 v0, v23

    iget-object v4, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ar;->a:Ljava/nio/FloatBuffer;

    invoke-virtual {v4, v2}, Ljava/nio/FloatBuffer;->get(I)F

    move-result v4

    aput v4, v11, v3

    const/4 v3, 0x1

    move-object/from16 v0, v23

    iget-object v4, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ar;->a:Ljava/nio/FloatBuffer;

    add-int/lit8 v5, v2, 0x1

    invoke-virtual {v4, v5}, Ljava/nio/FloatBuffer;->get(I)F

    move-result v4

    aput v4, v11, v3

    const/4 v3, 0x2

    move-object/from16 v0, v23

    iget-object v4, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ar;->a:Ljava/nio/FloatBuffer;

    add-int/lit8 v2, v2, 0x2

    invoke-virtual {v4, v2}, Ljava/nio/FloatBuffer;->get(I)F

    move-result v2

    aput v2, v11, v3

    const/4 v2, 0x3

    const/high16 v3, 0x3f800000    # 1.0f

    aput v3, v11, v2

    const/4 v2, 0x0

    aget v2, v11, v2

    const/4 v3, 0x1

    aget v3, v11, v3

    const/4 v4, 0x2

    aget v4, v11, v4

    move-object/from16 v0, v22

    iget-object v5, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/bd;->a:Lcom/google/maps/api/android/lib6/gmm6/streetview/an;

    iget-object v5, v5, Lcom/google/maps/api/android/lib6/gmm6/streetview/an;->a:[F

    const/4 v6, 0x0

    move-object/from16 v0, v22

    iget-object v7, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/bd;->a:Lcom/google/maps/api/android/lib6/gmm6/streetview/an;

    iget-object v7, v7, Lcom/google/maps/api/android/lib6/gmm6/streetview/an;->b:[F

    const/4 v8, 0x0

    move-object/from16 v0, v22

    iget-object v9, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/bd;->b:[I

    const/4 v10, 0x0

    const/4 v12, 0x4

    invoke-static/range {v2 .. v12}, Landroid/opengl/GLU;->gluProject(FFF[FI[FI[II[FI)I

    const/4 v2, 0x4

    aget v2, v11, v2

    float-to-int v3, v2

    const/4 v2, 0x5

    aget v2, v11, v2

    float-to-int v4, v2

    if-nez v15, :cond_4

    if-ltz v3, :cond_a

    if-ltz v4, :cond_a

    move/from16 v0, v27

    if-ge v3, v0, :cond_a

    move/from16 v0, v28

    if-ge v4, v0, :cond_a

    const/4 v2, 0x1

    :goto_9
    if-eqz v2, :cond_b

    :cond_4
    const/4 v2, 0x1

    :goto_a
    move/from16 v0, v19

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v7

    move/from16 v0, v18

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v6

    move/from16 v0, v17

    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    move-result v5

    move/from16 v0, v16

    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    add-int/lit8 v3, v14, 0x1

    move v14, v3

    move v15, v2

    move/from16 v16, v4

    move/from16 v17, v5

    move/from16 v18, v6

    move/from16 v19, v7

    goto/16 :goto_8

    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_2

    :cond_6
    const/4 v3, 0x0

    move v4, v3

    goto/16 :goto_3

    :cond_7
    const/4 v2, 0x0

    move/from16 v20, v2

    goto/16 :goto_5

    :cond_8
    const v2, 0xf600

    const v3, 0x8a00

    const/16 v4, 0x1f00

    const/high16 v6, 0x10000

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3, v4, v6}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    goto/16 :goto_6

    :cond_9
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const v6, 0x8000

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3, v4, v6}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    goto/16 :goto_7

    :cond_a
    const/4 v2, 0x0

    goto :goto_9

    :cond_b
    const/4 v2, 0x0

    goto :goto_a

    :cond_c
    if-eqz v15, :cond_d

    aput v19, v26, v25

    add-int/lit8 v2, v25, 0x1

    aput v17, v26, v2

    add-int/lit8 v2, v25, 0x2

    aput v18, v26, v2

    add-int/lit8 v2, v25, 0x3

    aput v16, v26, v2

    :cond_d
    if-eqz v15, :cond_e

    move-object/from16 v0, v21

    iget-object v2, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;->a:[I

    aget v2, v2, v25

    move-object/from16 v0, v21

    iget-object v3, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;->a:[I

    add-int/lit8 v4, v25, 0x1

    aget v3, v3, v4

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;->a:[I

    add-int/lit8 v5, v25, 0x2

    aget v4, v4, v5

    move-object/from16 v0, v21

    iget-object v5, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;->a:[I

    add-int/lit8 v6, v25, 0x3

    aget v5, v5, v6

    move-object/from16 v0, v21

    iget v6, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;->d:I

    mul-int/lit8 v6, v6, 0x4

    move-object/from16 v0, v21

    iget-object v7, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;->a:[I

    const/4 v8, 0x0

    sub-int v9, v4, v2

    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    move-result v8

    const/16 v9, 0x40

    sub-int v10, v5, v3

    invoke-static {v9, v10}, Ljava/lang/Math;->max(II)I

    move-result v9

    add-int/2addr v2, v4

    shr-int/lit8 v2, v2, 0x1

    add-int/2addr v3, v5

    shr-int/lit8 v3, v3, 0x1

    shr-int/lit8 v4, v8, 0x1

    sub-int/2addr v2, v4

    shr-int/lit8 v4, v9, 0x1

    sub-int/2addr v3, v4

    aput v2, v7, v6

    add-int/lit8 v4, v6, 0x1

    aput v3, v7, v4

    add-int/lit8 v4, v6, 0x2

    add-int/2addr v2, v8

    aput v2, v7, v4

    add-int/lit8 v2, v6, 0x3

    add-int/2addr v3, v9

    aput v3, v7, v2

    move-object/from16 v0, v21

    iget-object v2, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;->c:[I

    move-object/from16 v0, v21

    iget v3, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;->d:I

    aput v24, v2, v3

    move-object/from16 v0, v21

    iget-object v2, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;->b:[I

    move-object/from16 v0, v21

    iget v3, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;->d:I

    add-int/lit8 v4, v3, 0x1

    move-object/from16 v0, v21

    iput v4, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;->d:I

    aput v13, v2, v3

    :cond_e
    if-eqz v20, :cond_f

    invoke-interface/range {p1 .. p1}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    :cond_f
    invoke-interface/range {p1 .. p1}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    add-int/lit8 v2, v13, 0x1

    move v13, v2

    goto/16 :goto_1

    :cond_10
    const/16 v2, 0xbe2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    goto/16 :goto_0

    :cond_11
    move v3, v2

    goto/16 :goto_4
.end method

.method private f()V
    .locals 6

    const/16 v1, 0x800

    iget v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->D:F

    float-to-double v2, v0

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    cmpg-double v0, v2, v4

    if-gez v0, :cond_0

    const/16 v0, 0x400

    :goto_0
    new-instance v2, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v0, v3}, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;-><init>(IIZ)V

    iput-object v2, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->o:Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method protected final a(FF)I
    .locals 4

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->u:Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;

    float-to-int v1, p1

    iget v2, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->g:I

    float-to-int v3, p2

    sub-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;->a(II)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/high16 v0, -0x80000000

    :cond_0
    return v0
.end method

.method public final a(II)I
    .locals 2

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->v:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->u:Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;

    invoke-virtual {v0, p1, p2}, Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;->a(II)I

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(IJ)V
    .locals 0

    if-nez p1, :cond_0

    iput-wide p2, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->z:J

    :cond_0
    return-void
.end method

.method protected final a(ILandroid/support/v4/view/a/i;)V
    .locals 7

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->u:Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;->a:[I

    mul-int/lit8 v1, p1, 0x4

    new-instance v2, Landroid/graphics/Rect;

    aget v3, v0, v1

    iget v4, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->g:I

    add-int/lit8 v5, v1, 0x3

    aget v5, v0, v5

    sub-int/2addr v4, v5

    add-int/lit8 v5, v1, 0x2

    aget v5, v0, v5

    iget v6, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->g:I

    add-int/lit8 v1, v1, 0x1

    aget v0, v0, v1

    sub-int v0, v6, v0

    invoke-direct {v2, v3, v4, v5, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p2, v2}, Landroid/support/v4/view/a/i;->b(Landroid/graphics/Rect;)V

    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/i;->a(Z)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->u:Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;->c:[I

    aget v0, v0, p1

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->q:[Ljava/lang/CharSequence;

    aget-object v0, v1, v0

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/i;->c(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected final a(ILandroid/view/accessibility/AccessibilityEvent;)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->u:Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;->c:[I

    aget v0, v0, p1

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->q:[Ljava/lang/CharSequence;

    aget-object v0, v1, v0

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final a(Lcom/google/maps/api/android/lib6/gmm6/streetview/q;II)V
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->d:Lcom/google/maps/api/android/lib6/gmm6/streetview/q;

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->d:Lcom/google/maps/api/android/lib6/gmm6/streetview/q;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->d:Lcom/google/maps/api/android/lib6/gmm6/streetview/q;

    iget v2, v2, Lcom/google/maps/api/android/lib6/gmm6/streetview/q;->l:I

    if-ne v2, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->F:Z

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->d:Lcom/google/maps/api/android/lib6/gmm6/streetview/q;

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/q;->v:[Lcom/google/maps/api/android/lib6/gmm6/streetview/t;

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->e:[Lcom/google/maps/api/android/lib6/gmm6/streetview/t;

    iput p2, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->f:I

    iput p3, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->g:I

    int-to-float v0, p2

    int-to-float v2, p3

    div-float/2addr v0, v2

    iput v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->h:F

    iput-boolean v1, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->l:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->w:I

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method protected final a(Ljava/util/List;)V
    .locals 3

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->u:Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;

    iget-object v1, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;->b:[I

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->u:Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;

    iget v2, v2, Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;->d:I

    if-ge v0, v2, :cond_0

    aget v2, v1, v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->b(Ljavax/microedition/khronos/opengles/GL10;)V

    return-void
.end method

.method public final a(Ljavax/microedition/khronos/opengles/GL10;Lcom/google/maps/api/android/lib6/c/bx;J)V
    .locals 31

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->E:Z

    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->d:Lcom/google/maps/api/android/lib6/gmm6/streetview/q;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v2, 0x1

    new-array v3, v2, [I

    const/16 v2, 0xd33

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glGetIntegerv(I[II)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->d:Lcom/google/maps/api/android/lib6/gmm6/streetview/q;

    iget-object v2, v2, Lcom/google/maps/api/android/lib6/gmm6/streetview/q;->w:Lcom/google/maps/api/android/lib6/gmm6/streetview/s;

    if-eqz v2, :cond_7

    const/4 v2, 0x1

    :goto_1
    const/4 v4, 0x0

    aget v3, v3, v4

    const/16 v4, 0x800

    if-lt v3, v4, :cond_8

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->d:Lcom/google/maps/api/android/lib6/gmm6/streetview/q;

    iget-boolean v2, v2, Lcom/google/maps/api/android/lib6/gmm6/streetview/q;->a:Z

    if-nez v2, :cond_8

    const/4 v2, 0x1

    :goto_2
    const/4 v3, -0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->x:I

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->z:J

    sub-long v4, p3, v4

    const-wide/16 v6, 0x7d0

    cmp-long v3, v4, v6

    if-gez v3, :cond_3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->e:[Lcom/google/maps/api/android/lib6/gmm6/streetview/t;

    move-object/from16 v0, p2

    iget v4, v0, Lcom/google/maps/api/android/lib6/c/bx;->a:F

    invoke-static {v3, v4}, Lcom/google/maps/api/android/lib6/gmm6/streetview/q;->a([Lcom/google/maps/api/android/lib6/gmm6/streetview/t;F)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->x:I

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->z:J

    const-wide/16 v6, 0x7d0

    add-long/2addr v4, v6

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->y:J

    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->y:J

    cmp-long v3, v6, v4

    if-lez v3, :cond_3

    :cond_2
    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->y:J

    :cond_3
    if-eqz v2, :cond_9

    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->F:Z

    if-nez v2, :cond_9

    const/4 v2, 0x1

    :goto_3
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->l:Z

    if-nez v3, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->d:Lcom/google/maps/api/android/lib6/gmm6/streetview/q;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v3, :cond_a

    :cond_4
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->t:Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->f:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->g:I

    const/4 v5, 0x0

    iput v5, v2, Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;->d:I

    iput v3, v2, Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;->e:I

    iput v4, v2, Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;->f:I

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->F:Z

    if-nez v2, :cond_5

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->A:Z

    if-eqz v2, :cond_5

    invoke-direct/range {p0 .. p2}, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->a(Ljavax/microedition/khronos/opengles/GL10;Lcom/google/maps/api/android/lib6/c/bx;)V

    :cond_5
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->B:Z

    if-eqz v2, :cond_6

    invoke-direct/range {p0 .. p2}, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->b(Ljavax/microedition/khronos/opengles/GL10;Lcom/google/maps/api/android/lib6/c/bx;)V

    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->v:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->u:Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->t:Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->u:Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->t:Lcom/google/maps/api/android/lib6/gmm6/streetview/aq;

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_2

    :cond_9
    const/4 v2, 0x0

    goto :goto_3

    :cond_a
    :try_start_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->o:Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;

    if-nez v3, :cond_b

    invoke-direct/range {p0 .. p0}, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->f()V

    :cond_b
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->k:Z

    if-nez v3, :cond_c

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->o:Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->k:Z

    :cond_c
    if-eqz v2, :cond_16

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->o:Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;

    const/4 v2, 0x1

    const/4 v4, 0x2

    invoke-virtual {v3, v2, v4}, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->a(II)V

    iget-object v2, v3, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->l:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    const/4 v2, 0x0

    iput v2, v3, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->i:I

    const/4 v2, 0x0

    iput v2, v3, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->j:I

    const/4 v2, 0x0

    iput v2, v3, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->k:I

    iget-boolean v2, v3, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->c:Z

    if-eqz v2, :cond_e

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    :goto_5
    iget v4, v3, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->a:I

    iget v5, v3, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->b:I

    invoke-static {v4, v5, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, v3, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->d:Landroid/graphics/Bitmap;

    new-instance v2, Landroid/graphics/Canvas;

    iget-object v4, v3, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->d:Landroid/graphics/Bitmap;

    invoke-direct {v2, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v2, v3, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->e:Landroid/graphics/Canvas;

    iget-object v2, v3, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->d:Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/graphics/Bitmap;->eraseColor(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->e:[Lcom/google/maps/api/android/lib6/gmm6/streetview/t;

    array-length v0, v2

    move/from16 v25, v0

    const/4 v2, 0x3

    move/from16 v0, v25

    filled-new-array {v0, v2}, [I

    move-result-object v2

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v3, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [[I

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->C:[[I

    const/4 v2, 0x0

    move/from16 v20, v2

    :goto_6
    move/from16 v0, v20

    move/from16 v1, v25

    if-ge v0, v1, :cond_15

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->e:[Lcom/google/maps/api/android/lib6/gmm6/streetview/t;

    aget-object v2, v2, v20

    iget-object v3, v2, Lcom/google/maps/api/android/lib6/gmm6/streetview/t;->e:Ljava/lang/String;

    if-eqz v3, :cond_13

    iget-object v3, v2, Lcom/google/maps/api/android/lib6/gmm6/streetview/t;->e:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_13

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v2, Lcom/google/maps/api/android/lib6/gmm6/streetview/t;->e:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->p:[Ljava/lang/CharSequence;

    iget v2, v2, Lcom/google/maps/api/android/lib6/gmm6/streetview/t;->b:I

    aget-object v2, v4, v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v2, 0x0

    move/from16 v21, v2

    :goto_7
    const/4 v2, 0x3

    move/from16 v0, v21

    if-ge v0, v2, :cond_14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->C:[[I

    aget-object v26, v2, v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->o:Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->r:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->s:Landroid/graphics/Paint;

    const/4 v4, 0x2

    const/4 v5, 0x2

    move-object/from16 v0, v27

    invoke-virtual {v0, v4, v5}, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->a(II)V

    if-eqz v3, :cond_f

    if-eqz v2, :cond_f

    const/4 v4, 0x1

    move v10, v4

    :goto_8
    new-instance v12, Landroid/graphics/Rect;

    invoke-direct {v12}, Landroid/graphics/Rect;-><init>()V

    if-eqz v11, :cond_d

    invoke-virtual {v11}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v4

    float-to-int v4, v4

    add-int/lit8 v4, v4, 0x1

    div-int/lit8 v4, v4, 0x2

    iget v5, v12, Landroid/graphics/Rect;->top:I

    add-int/2addr v5, v4

    iput v5, v12, Landroid/graphics/Rect;->top:I

    iget v5, v12, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v5, v4

    iput v5, v12, Landroid/graphics/Rect;->bottom:I

    iget v5, v12, Landroid/graphics/Rect;->left:I

    add-int/2addr v5, v4

    iput v5, v12, Landroid/graphics/Rect;->left:I

    iget v5, v12, Landroid/graphics/Rect;->right:I

    add-int/2addr v4, v5

    iput v4, v12, Landroid/graphics/Rect;->right:I

    :cond_d
    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    if-eqz v10, :cond_19

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v5, 0x14

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-virtual {v2}, Landroid/graphics/Paint;->ascent()F

    move-result v4

    neg-float v4, v4

    invoke-static {v4}, Landroid/util/FloatMath;->ceil(F)F

    move-result v4

    float-to-int v7, v4

    invoke-virtual {v2}, Landroid/graphics/Paint;->descent()F

    move-result v4

    invoke-static {v4}, Landroid/util/FloatMath;->ceil(F)F

    move-result v4

    float-to-int v6, v4

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;II)F

    move-result v4

    invoke-static {v4}, Landroid/util/FloatMath;->ceil(F)F

    move-result v4

    float-to-int v4, v4

    move v9, v7

    :goto_9
    add-int/2addr v6, v9

    move-object/from16 v0, v27

    iget v7, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->a:I

    invoke-static {v7, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    iget v7, v12, Landroid/graphics/Rect;->top:I

    iget v8, v12, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v7, v8

    iget v8, v12, Landroid/graphics/Rect;->left:I

    iget v13, v12, Landroid/graphics/Rect;->right:I

    add-int/2addr v13, v8

    const/4 v8, 0x0

    add-int v14, v6, v7

    invoke-static {v8, v14}, Ljava/lang/Math;->max(II)I

    move-result v28

    const/4 v8, 0x0

    add-int v14, v4, v13

    invoke-static {v8, v14}, Ljava/lang/Math;->max(II)I

    move-result v8

    sub-int v7, v28, v7

    sub-int v13, v8, v13

    sub-int v6, v7, v6

    div-int/lit8 v14, v6, 0x2

    sub-int v4, v13, v4

    div-int/lit8 v13, v4, 0x2

    move-object/from16 v0, v27

    iget v7, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->i:I

    move-object/from16 v0, v27

    iget v6, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->j:I

    move-object/from16 v0, v27

    iget v4, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->k:I

    move-object/from16 v0, v27

    iget v15, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->a:I

    if-le v8, v15, :cond_18

    move-object/from16 v0, v27

    iget v8, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->a:I

    move/from16 v24, v8

    :goto_a
    add-int v8, v7, v24

    move-object/from16 v0, v27

    iget v15, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->a:I

    if-le v8, v15, :cond_17

    const/4 v7, 0x0

    add-int/2addr v6, v4

    const/4 v4, 0x0

    move/from16 v22, v6

    move/from16 v23, v7

    :goto_b
    move/from16 v0, v28

    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    move-result v29

    add-int v4, v22, v29

    move-object/from16 v0, v27

    iget v6, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->b:I

    if-le v4, v6, :cond_10

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Out of texture space."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v2

    invoke-direct/range {p0 .. p1}, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->b(Ljavax/microedition/khronos/opengles/GL10;)V

    invoke-direct/range {p0 .. p0}, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->f()V

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->E:Z

    goto/16 :goto_0

    :cond_e
    :try_start_3
    sget-object v2, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    goto/16 :goto_5

    :cond_f
    const/4 v4, 0x0

    move v10, v4

    goto/16 :goto_8

    :cond_10
    add-int v15, v23, v24

    add-int v4, v22, v9

    add-int v9, v22, v28

    if-eqz v10, :cond_12

    iget v6, v12, Landroid/graphics/Rect;->left:I

    add-int v6, v6, v23

    add-int/2addr v6, v13

    int-to-float v6, v6

    iget v7, v12, Landroid/graphics/Rect;->top:I

    add-int/2addr v4, v7

    add-int/2addr v4, v14

    int-to-float v7, v4

    new-instance v8, Landroid/graphics/Path;

    invoke-direct {v8}, Landroid/graphics/Path;-><init>()V

    const/4 v4, 0x0

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Paint;->getTextPath(Ljava/lang/String;IIFFLandroid/graphics/Path;)V

    if-eqz v11, :cond_11

    move-object/from16 v0, v27

    iget-object v4, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->e:Landroid/graphics/Canvas;

    invoke-virtual {v4, v8, v11}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :cond_11
    move-object/from16 v0, v27

    iget-object v4, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->e:Landroid/graphics/Canvas;

    invoke-virtual {v4, v8, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :cond_12
    new-instance v4, Lcom/google/maps/api/android/lib6/gmm6/streetview/aa;

    const/4 v2, 0x2

    const/4 v5, 0x2

    invoke-direct {v4, v2, v5}, Lcom/google/maps/api/android/lib6/gmm6/streetview/aa;-><init>(II)V

    move/from16 v0, v23

    int-to-float v2, v0

    move-object/from16 v0, v27

    iget v5, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->g:F

    mul-float v10, v2, v5

    int-to-float v2, v15

    move-object/from16 v0, v27

    iget v5, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->g:F

    mul-float v18, v2, v5

    move/from16 v0, v22

    int-to-float v2, v0

    move-object/from16 v0, v27

    iget v5, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->h:F

    mul-float/2addr v2, v5

    int-to-float v5, v9

    move-object/from16 v0, v27

    iget v6, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->h:F

    mul-float v11, v5, v6

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v11}, Lcom/google/maps/api/android/lib6/gmm6/streetview/aa;->a(IIFFFFF)V

    const/4 v13, 0x1

    const/4 v14, 0x0

    move/from16 v0, v24

    int-to-float v15, v0

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object v12, v4

    move/from16 v19, v11

    invoke-virtual/range {v12 .. v19}, Lcom/google/maps/api/android/lib6/gmm6/streetview/aa;->a(IIFFFFF)V

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    move/from16 v0, v28

    int-to-float v8, v0

    const/4 v9, 0x0

    move v11, v2

    invoke-virtual/range {v4 .. v11}, Lcom/google/maps/api/android/lib6/gmm6/streetview/aa;->a(IIFFFFF)V

    const/4 v5, 0x1

    const/4 v6, 0x1

    move/from16 v0, v24

    int-to-float v7, v0

    move/from16 v0, v28

    int-to-float v8, v0

    const/4 v9, 0x0

    move/from16 v10, v18

    move v11, v2

    invoke-virtual/range {v4 .. v11}, Lcom/google/maps/api/android/lib6/gmm6/streetview/aa;->a(IIFFFFF)V

    add-int v2, v23, v24

    move-object/from16 v0, v27

    iput v2, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->i:I

    move/from16 v0, v22

    move-object/from16 v1, v27

    iput v0, v1, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->j:I

    move/from16 v0, v29

    move-object/from16 v1, v27

    iput v0, v1, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->k:I

    move-object/from16 v0, v27

    iget-object v2, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->l:Ljava/util/ArrayList;

    new-instance v5, Lcom/google/maps/api/android/lib6/gmm6/streetview/aj;

    add-int v8, v22, v28

    move/from16 v0, v28

    neg-int v10, v0

    move-object v6, v4

    move/from16 v7, v23

    move/from16 v9, v24

    invoke-direct/range {v5 .. v10}, Lcom/google/maps/api/android/lib6/gmm6/streetview/aj;-><init>(Lcom/google/maps/api/android/lib6/gmm6/streetview/aa;IIII)V

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, v27

    iget-object v2, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->l:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    aput v2, v26, v21

    add-int/lit8 v2, v21, 0x1

    move/from16 v21, v2

    goto/16 :goto_7

    :cond_13
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->C:[[I

    aget-object v2, v2, v20

    const/4 v3, 0x0

    const/4 v4, -0x1

    aput v4, v2, v3

    :cond_14
    add-int/lit8 v2, v20, 0x1

    move/from16 v20, v2

    goto/16 :goto_6

    :cond_15
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->o:Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;

    const/4 v3, 0x2

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->a(II)V

    const/16 v3, 0xde1

    iget v4, v2, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->f:I

    move-object/from16 v0, p1

    invoke-interface {v0, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glBindTexture(II)V

    const/16 v3, 0xde1

    const/4 v4, 0x0

    iget-object v5, v2, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->d:Landroid/graphics/Bitmap;

    const/4 v6, 0x0

    invoke-static {v3, v4, v5, v6}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    iget-object v3, v2, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    const/4 v3, 0x0

    iput-object v3, v2, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->d:Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    iput-object v3, v2, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->e:Landroid/graphics/Canvas;

    :cond_16
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->l:Z
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_4

    :cond_17
    move/from16 v22, v6

    move/from16 v23, v7

    goto/16 :goto_b

    :cond_18
    move/from16 v24, v8

    goto/16 :goto_a

    :cond_19
    move v9, v7

    goto/16 :goto_9
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->A:Z

    return-void
.end method

.method public final b(I)Lcom/google/maps/api/android/lib6/gmm6/streetview/t;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->e:[Lcom/google/maps/api/android/lib6/gmm6/streetview/t;

    aget-object v0, v0, p1
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->B:Z

    return-void
.end method

.method public final c(I)V
    .locals 0

    iput p1, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->w:I

    return-void
.end method

.method protected final c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final d()J
    .locals 2

    iget-wide v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->y:J

    return-wide v0
.end method

.method public final e()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ap;->A:Z

    return v0
.end method
