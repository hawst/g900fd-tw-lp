.class public final Lcom/google/maps/api/android/lib6/gmm6/c/h;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/maps/api/android/lib6/c/am;
.implements Lcom/google/maps/api/android/lib6/gmm6/o/ax;
.implements Lcom/google/maps/api/android/lib6/gmm6/o/bk;
.implements Lcom/google/maps/api/android/lib6/gmm6/o/bn;


# instance fields
.field final a:Lcom/google/maps/api/android/lib6/gmm6/c/aq;

.field final b:Lcom/google/maps/api/android/lib6/gmm6/o/bm;

.field private final c:Ljava/util/Map;

.field private final d:Lcom/google/maps/api/android/lib6/gmm6/c/ap;

.field private final e:Lcom/google/maps/api/android/lib6/gmm6/o/bj;

.field private final f:Lcom/google/maps/api/android/lib6/c/bz;


# direct methods
.method private constructor <init>(Lcom/google/maps/api/android/lib6/gmm6/c/aq;Lcom/google/maps/api/android/lib6/gmm6/c/ap;Lcom/google/maps/api/android/lib6/gmm6/o/bm;Lcom/google/maps/api/android/lib6/gmm6/o/bj;Lcom/google/maps/api/android/lib6/c/bz;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/k/c/hd;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/h;->c:Ljava/util/Map;

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/h;->a:Lcom/google/maps/api/android/lib6/gmm6/c/aq;

    iput-object p2, p0, Lcom/google/maps/api/android/lib6/gmm6/c/h;->d:Lcom/google/maps/api/android/lib6/gmm6/c/ap;

    iput-object p3, p0, Lcom/google/maps/api/android/lib6/gmm6/c/h;->b:Lcom/google/maps/api/android/lib6/gmm6/o/bm;

    iput-object p4, p0, Lcom/google/maps/api/android/lib6/gmm6/c/h;->e:Lcom/google/maps/api/android/lib6/gmm6/o/bj;

    iput-object p5, p0, Lcom/google/maps/api/android/lib6/gmm6/c/h;->f:Lcom/google/maps/api/android/lib6/c/bz;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/h;->b:Lcom/google/maps/api/android/lib6/gmm6/o/bm;

    iput-object p0, v0, Lcom/google/maps/api/android/lib6/gmm6/o/aw;->b:Lcom/google/maps/api/android/lib6/gmm6/o/ax;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/h;->b:Lcom/google/maps/api/android/lib6/gmm6/o/bm;

    iput-object p0, v0, Lcom/google/maps/api/android/lib6/gmm6/o/bm;->d:Lcom/google/maps/api/android/lib6/gmm6/o/bn;

    invoke-virtual {p4, p0}, Lcom/google/maps/api/android/lib6/gmm6/o/bj;->a(Lcom/google/maps/api/android/lib6/gmm6/o/bk;)V

    return-void
.end method

.method public static a(Lcom/google/maps/api/android/lib6/gmm6/c/aq;Lcom/google/maps/api/android/lib6/gmm6/c/ap;Lcom/google/maps/api/android/lib6/c/bz;)Lcom/google/maps/api/android/lib6/gmm6/c/h;
    .locals 6

    sget-object v0, Lcom/google/maps/api/android/lib6/gmm6/o/bp;->n:Lcom/google/maps/api/android/lib6/gmm6/o/bp;

    invoke-interface {p0, v0}, Lcom/google/maps/api/android/lib6/gmm6/c/aq;->a(Lcom/google/maps/api/android/lib6/gmm6/o/bp;)Lcom/google/maps/api/android/lib6/gmm6/o/bm;

    move-result-object v3

    invoke-interface {p0}, Lcom/google/maps/api/android/lib6/gmm6/c/aq;->e()Lcom/google/maps/api/android/lib6/gmm6/o/bj;

    move-result-object v4

    new-instance v0, Lcom/google/maps/api/android/lib6/gmm6/c/h;

    move-object v1, p0

    move-object v2, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/maps/api/android/lib6/gmm6/c/h;-><init>(Lcom/google/maps/api/android/lib6/gmm6/c/aq;Lcom/google/maps/api/android/lib6/gmm6/c/ap;Lcom/google/maps/api/android/lib6/gmm6/o/bm;Lcom/google/maps/api/android/lib6/gmm6/o/bj;Lcom/google/maps/api/android/lib6/c/bz;)V

    return-object v0
.end method

.method private c(Lcom/google/maps/api/android/lib6/gmm6/o/av;)Lcom/google/maps/api/android/lib6/c/aj;
    .locals 2

    invoke-static {p1}, Lcom/google/maps/api/android/lib6/gmm6/c/h;->d(Lcom/google/maps/api/android/lib6/gmm6/o/av;)Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;->p()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/h;->f:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v1}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/h;->c:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/api/android/lib6/c/aj;

    goto :goto_0
.end method

.method private static d(Lcom/google/maps/api/android/lib6/gmm6/o/av;)Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;
    .locals 1

    instance-of v0, p0, Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;

    if-nez v0, :cond_0

    const/4 p0, 0x0

    :goto_0
    return-object p0

    :cond_0
    check-cast p0, Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/maps/api/android/lib6/c/aj;)Lcom/google/maps/api/android/lib6/c/ak;
    .locals 1

    new-instance v0, Lcom/google/maps/api/android/lib6/gmm6/c/j;

    invoke-direct {v0, p1, p0}, Lcom/google/maps/api/android/lib6/gmm6/c/j;-><init>(Lcom/google/maps/api/android/lib6/c/aj;Lcom/google/maps/api/android/lib6/gmm6/c/h;)V

    return-object v0
.end method

.method protected final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/h;->a:Lcom/google/maps/api/android/lib6/gmm6/c/aq;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/gmm6/c/aq;->d()V

    return-void
.end method

.method protected final a(Lcom/google/maps/api/android/lib6/gmm6/c/j;)V
    .locals 3

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/h;->b:Lcom/google/maps/api/android/lib6/gmm6/o/bm;

    iget-object v1, p1, Lcom/google/maps/api/android/lib6/gmm6/c/j;->a:Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;

    invoke-virtual {v0, v1}, Lcom/google/maps/api/android/lib6/gmm6/o/bm;->a(Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;)V

    iget-object v0, p1, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/h;->c:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/gmm6/c/h;->a()V

    return-void
.end method

.method public final a(Lcom/google/maps/api/android/lib6/gmm6/o/av;)V
    .locals 7

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/h;->f:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    invoke-direct {p0, p1}, Lcom/google/maps/api/android/lib6/gmm6/c/h;->c(Lcom/google/maps/api/android/lib6/gmm6/o/av;)Lcom/google/maps/api/android/lib6/c/aj;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->G()Lcom/google/maps/api/android/lib6/c/al;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/maps/api/android/lib6/c/al;->h(Lcom/google/maps/api/android/lib6/c/aj;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/google/maps/api/android/lib6/gmm6/c/h;->d(Lcom/google/maps/api/android/lib6/gmm6/o/av;)Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v6, p0, Lcom/google/maps/api/android/lib6/gmm6/c/h;->d:Lcom/google/maps/api/android/lib6/gmm6/c/ap;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;->d()Lcom/google/maps/api/android/lib6/gmm6/l/h;

    move-result-object v1

    iget-object v0, v6, Lcom/google/maps/api/android/lib6/gmm6/c/ap;->g:Lcom/google/maps/api/android/lib6/gmm6/c/aq;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/gmm6/c/aq;->u()Lcom/google/maps/api/android/lib6/gmm6/o/b/b;

    move-result-object v4

    new-instance v0, Lcom/google/maps/api/android/lib6/gmm6/o/b/c;

    invoke-virtual {v4}, Lcom/google/maps/api/android/lib6/gmm6/o/b/b;->l()F

    move-result v2

    invoke-virtual {v4}, Lcom/google/maps/api/android/lib6/gmm6/o/b/b;->k()F

    move-result v3

    invoke-virtual {v4}, Lcom/google/maps/api/android/lib6/gmm6/o/b/b;->j()F

    move-result v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/maps/api/android/lib6/gmm6/o/b/c;-><init>(Lcom/google/maps/api/android/lib6/gmm6/l/h;FFFF)V

    const/16 v1, 0x12c

    invoke-virtual {v6, v0, v1}, Lcom/google/maps/api/android/lib6/gmm6/c/ap;->a(Lcom/google/maps/api/android/lib6/gmm6/o/b/d;I)V

    goto :goto_0
.end method

.method public final a(Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/h;->c:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;->p()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/h;->b:Lcom/google/maps/api/android/lib6/gmm6/o/bm;

    invoke-virtual {v0, p1}, Lcom/google/maps/api/android/lib6/gmm6/o/bm;->b(Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;)V

    return-void
.end method

.method public final b(Lcom/google/maps/api/android/lib6/gmm6/c/j;)V
    .locals 6

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/gmm6/c/h;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/h;->a:Lcom/google/maps/api/android/lib6/gmm6/c/aq;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/gmm6/c/aq;->f()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Lcom/google/maps/api/android/lib6/gmm6/c/i;

    invoke-direct {v1, p0, p1}, Lcom/google/maps/api/android/lib6/gmm6/c/i;-><init>(Lcom/google/maps/api/android/lib6/gmm6/c/h;Lcom/google/maps/api/android/lib6/gmm6/c/j;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    :cond_0
    iget-object v2, p1, Lcom/google/maps/api/android/lib6/gmm6/c/j;->a:Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;

    iget-object v0, p1, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v2}, Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;->e()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->G()Lcom/google/maps/api/android/lib6/c/al;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/al;->b()Lcom/google/android/gms/maps/internal/q;

    move-result-object v0

    iget-object v3, p1, Lcom/google/maps/api/android/lib6/gmm6/c/j;->b:Lcom/google/maps/api/android/lib6/c/aj;

    iget-object v4, p0, Lcom/google/maps/api/android/lib6/gmm6/c/h;->a:Lcom/google/maps/api/android/lib6/gmm6/c/aq;

    invoke-interface {v4}, Lcom/google/maps/api/android/lib6/gmm6/c/aq;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/google/maps/api/android/lib6/gmm6/c/h;->a:Lcom/google/maps/api/android/lib6/gmm6/c/aq;

    invoke-interface {v5}, Lcom/google/maps/api/android/lib6/gmm6/c/aq;->getHeight()I

    move-result v5

    invoke-interface {v0, v3, v4, v5}, Lcom/google/android/gms/maps/internal/q;->a(Lcom/google/android/gms/maps/model/internal/s;II)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/h;->a:Lcom/google/maps/api/android/lib6/gmm6/c/aq;

    new-instance v3, Lcom/google/maps/api/android/lib6/gmm6/o/ay;

    invoke-direct {v3, v0}, Lcom/google/maps/api/android/lib6/gmm6/o/ay;-><init>(Landroid/graphics/Bitmap;)V

    invoke-interface {v1, v2, v3}, Lcom/google/maps/api/android/lib6/gmm6/c/aq;->a(Lcom/google/maps/api/android/lib6/gmm6/o/av;Lcom/google/maps/api/android/lib6/gmm6/o/ay;)V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    move-object v0, v1

    goto :goto_1
.end method

.method public final b(Lcom/google/maps/api/android/lib6/gmm6/o/av;)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/h;->f:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    invoke-direct {p0, p1}, Lcom/google/maps/api/android/lib6/gmm6/c/h;->c(Lcom/google/maps/api/android/lib6/gmm6/o/av;)Lcom/google/maps/api/android/lib6/c/aj;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->G()Lcom/google/maps/api/android/lib6/c/al;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/maps/api/android/lib6/c/al;->i(Lcom/google/maps/api/android/lib6/c/aj;)V

    :cond_0
    return-void
.end method

.method final b()Z
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/h;->a:Lcom/google/maps/api/android/lib6/gmm6/c/aq;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/gmm6/c/aq;->getWidth()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/h;->a:Lcom/google/maps/api/android/lib6/gmm6/c/aq;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/gmm6/c/aq;->getHeight()I

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final b(Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;)Z
    .locals 2

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/h;->e:Lcom/google/maps/api/android/lib6/gmm6/o/bj;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p1}, Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;->n()Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/h;->b:Lcom/google/maps/api/android/lib6/gmm6/o/bm;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/o/bm;->h()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/google/maps/api/android/lib6/gmm6/c/j;)V
    .locals 2

    iget-object v0, p1, Lcom/google/maps/api/android/lib6/gmm6/c/j;->a:Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;->e()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/h;->e:Lcom/google/maps/api/android/lib6/gmm6/o/bj;

    monitor-enter v1

    :try_start_0
    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/h;->a:Lcom/google/maps/api/android/lib6/gmm6/c/aq;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/gmm6/c/aq;->t()V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c(Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/h;->f:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    invoke-direct {p0, p1}, Lcom/google/maps/api/android/lib6/gmm6/c/h;->c(Lcom/google/maps/api/android/lib6/gmm6/o/av;)Lcom/google/maps/api/android/lib6/c/aj;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->G()Lcom/google/maps/api/android/lib6/c/al;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/maps/api/android/lib6/c/al;->e(Lcom/google/maps/api/android/lib6/c/aj;)V

    :cond_0
    return-void
.end method

.method public final d(Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/h;->f:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    invoke-direct {p0, p1}, Lcom/google/maps/api/android/lib6/gmm6/c/h;->c(Lcom/google/maps/api/android/lib6/gmm6/o/av;)Lcom/google/maps/api/android/lib6/c/aj;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->G()Lcom/google/maps/api/android/lib6/c/al;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/maps/api/android/lib6/c/al;->f(Lcom/google/maps/api/android/lib6/c/aj;)V

    :cond_0
    return-void
.end method

.method public final e(Lcom/google/maps/api/android/lib6/gmm6/o/c/b/af;)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/h;->f:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    invoke-direct {p0, p1}, Lcom/google/maps/api/android/lib6/gmm6/c/h;->c(Lcom/google/maps/api/android/lib6/gmm6/o/av;)Lcom/google/maps/api/android/lib6/c/aj;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->G()Lcom/google/maps/api/android/lib6/c/al;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/maps/api/android/lib6/c/al;->g(Lcom/google/maps/api/android/lib6/c/aj;)V

    :cond_0
    return-void
.end method
