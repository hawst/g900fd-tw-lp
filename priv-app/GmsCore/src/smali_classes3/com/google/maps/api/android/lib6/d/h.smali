.class public final Lcom/google/maps/api/android/lib6/d/h;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/maps/api/android/lib6/c/ad;


# direct methods
.method private constructor <init>(Landroid/os/Handler;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "Handler is null"

    invoke-static {p1, v0}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public static a(Landroid/os/Handler;)Lcom/google/maps/api/android/lib6/d/h;
    .locals 1

    new-instance v0, Lcom/google/maps/api/android/lib6/d/h;

    invoke-direct {v0, p0}, Lcom/google/maps/api/android/lib6/d/h;-><init>(Landroid/os/Handler;)V

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/maps/internal/as;)V
    .locals 2

    const/4 v0, 0x5

    const-string v1, "Map Loaded callback is not supported in Lite Mode"

    invoke-static {v0, v1}, Lcom/google/maps/api/android/lib6/c/by;->a(ILjava/lang/String;)V

    return-void
.end method

.method public final b(Lcom/google/android/gms/maps/internal/as;)V
    .locals 2

    const/4 v0, 0x5

    const-string v1, "Map Loaded callback is not supported in Lite Mode"

    invoke-static {v0, v1}, Lcom/google/maps/api/android/lib6/c/by;->a(ILjava/lang/String;)V

    return-void
.end method
