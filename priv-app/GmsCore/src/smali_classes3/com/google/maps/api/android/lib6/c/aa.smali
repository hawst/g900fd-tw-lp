.class public Lcom/google/maps/api/android/lib6/c/aa;
.super Lcom/google/android/gms/maps/internal/x;


# instance fields
.field private final a:Lcom/google/maps/api/android/lib6/c/ac;

.field private b:Lcom/google/maps/api/android/lib6/c/ey;

.field private c:Lcom/google/android/gms/maps/GoogleMapOptions;

.field private final d:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/maps/api/android/lib6/c/aa;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lcom/google/maps/api/android/lib6/c/ac;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/maps/internal/x;-><init>()V

    invoke-static {p1}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/api/android/lib6/c/ac;

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/aa;->a:Lcom/google/maps/api/android/lib6/c/ac;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/aa;->d:Ljava/util/List;

    return-void
.end method

.method public static a(Landroid/app/Activity;)Lcom/google/maps/api/android/lib6/c/aa;
    .locals 3

    invoke-static {p0}, Lcom/google/maps/api/android/lib6/c/by;->a(Landroid/app/Activity;)Z

    move-result v0

    new-instance v1, Lcom/google/maps/api/android/lib6/c/aa;

    new-instance v2, Lcom/google/maps/api/android/lib6/c/ab;

    invoke-direct {v2, v0}, Lcom/google/maps/api/android/lib6/c/ab;-><init>(Z)V

    invoke-direct {v1, v2}, Lcom/google/maps/api/android/lib6/c/aa;-><init>(Lcom/google/maps/api/android/lib6/c/ac;)V

    return-object v1
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/b/l;Lcom/google/android/gms/b/l;Landroid/os/Bundle;)Lcom/google/android/gms/b/l;
    .locals 4

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aa;->b:Lcom/google/maps/api/android/lib6/c/ey;

    if-nez v0, :cond_1

    invoke-static {p1}, Lcom/google/android/gms/b/p;->a(Lcom/google/android/gms/b/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/aa;->a:Lcom/google/maps/api/android/lib6/c/ac;

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/c/aa;->c:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-interface {v1, v0, v2}, Lcom/google/maps/api/android/lib6/c/ac;->a(Landroid/view/LayoutInflater;Lcom/google/android/gms/maps/GoogleMapOptions;)Lcom/google/maps/api/android/lib6/c/ey;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/aa;->b:Lcom/google/maps/api/android/lib6/c/ey;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aa;->b:Lcom/google/maps/api/android/lib6/c/ey;

    invoke-interface {v0, p3}, Lcom/google/maps/api/android/lib6/c/ey;->a(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aa;->b:Lcom/google/maps/api/android/lib6/c/ey;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/ey;->C()Landroid/view/View;

    move-result-object v1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aa;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/internal/ay;

    :try_start_0
    iget-object v3, p0, Lcom/google/maps/api/android/lib6/c/aa;->b:Lcom/google/maps/api/android/lib6/c/ey;

    invoke-interface {v3, v0}, Lcom/google/maps/api/android/lib6/c/ey;->a(Lcom/google/android/gms/maps/internal/ay;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1

    :cond_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aa;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    move-object v0, v1

    :goto_1
    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v0

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aa;->b:Lcom/google/maps/api/android/lib6/c/ey;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/ey;->C()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method public final a()Lcom/google/android/gms/maps/internal/k;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aa;->b:Lcom/google/maps/api/android/lib6/c/ey;

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aa;->c:Lcom/google/android/gms/maps/GoogleMapOptions;

    if-nez v0, :cond_0

    const-string v0, "MapOptions"

    invoke-static {p1, v0}, Lcom/google/android/gms/maps/internal/cs;->a(Landroid/os/Bundle;Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/GoogleMapOptions;

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/aa;->c:Lcom/google/android/gms/maps/GoogleMapOptions;

    :cond_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aa;->c:Lcom/google/android/gms/maps/GoogleMapOptions;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-direct {v0}, Lcom/google/android/gms/maps/GoogleMapOptions;-><init>()V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/aa;->c:Lcom/google/android/gms/maps/GoogleMapOptions;

    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/gms/b/l;Lcom/google/android/gms/maps/GoogleMapOptions;Landroid/os/Bundle;)V
    .locals 0

    iput-object p2, p0, Lcom/google/maps/api/android/lib6/c/aa;->c:Lcom/google/android/gms/maps/GoogleMapOptions;

    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/internal/ay;)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aa;->b:Lcom/google/maps/api/android/lib6/c/ey;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aa;->b:Lcom/google/maps/api/android/lib6/c/ey;

    invoke-interface {v0, p1}, Lcom/google/maps/api/android/lib6/c/ey;->a(Lcom/google/android/gms/maps/internal/ay;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1

    :cond_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aa;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aa;->b:Lcom/google/maps/api/android/lib6/c/ey;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/ey;->o()V

    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aa;->b:Lcom/google/maps/api/android/lib6/c/ey;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aa;->c:Lcom/google/android/gms/maps/GoogleMapOptions;

    if-eqz v0, :cond_0

    const-string v0, "MapOptions"

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/aa;->c:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/maps/internal/cs;->a(Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aa;->b:Lcom/google/maps/api/android/lib6/c/ey;

    invoke-interface {v0, p1}, Lcom/google/maps/api/android/lib6/c/ey;->b(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aa;->b:Lcom/google/maps/api/android/lib6/c/ey;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/ey;->p()V

    return-void
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aa;->b:Lcom/google/maps/api/android/lib6/c/ey;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/ey;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aa;->b:Lcom/google/maps/api/android/lib6/c/ey;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/ey;->q()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/aa;->b:Lcom/google/maps/api/android/lib6/c/ey;

    :cond_0
    return-void
.end method

.method public final e()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aa;->b:Lcom/google/maps/api/android/lib6/c/ey;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aa;->b:Lcom/google/maps/api/android/lib6/c/ey;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/ey;->q()V

    iput-object v1, p0, Lcom/google/maps/api/android/lib6/c/aa;->b:Lcom/google/maps/api/android/lib6/c/ey;

    :cond_0
    iput-object v1, p0, Lcom/google/maps/api/android/lib6/c/aa;->c:Lcom/google/android/gms/maps/GoogleMapOptions;

    return-void
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aa;->b:Lcom/google/maps/api/android/lib6/c/ey;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/ey;->r()V

    return-void
.end method

.method public final g()Z
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aa;->b:Lcom/google/maps/api/android/lib6/c/ey;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
