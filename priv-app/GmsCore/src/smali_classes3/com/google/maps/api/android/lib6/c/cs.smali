.class public final Lcom/google/maps/api/android/lib6/c/cs;
.super Lcom/google/android/gms/maps/model/internal/e;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/maps/model/internal/e;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/b/l;
    .locals 1

    invoke-static {}, Lcom/google/maps/api/android/lib6/c/ct;->a()Lcom/google/maps/api/android/lib6/c/cy;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(F)Lcom/google/android/gms/b/l;
    .locals 1

    invoke-static {p1}, Lcom/google/maps/api/android/lib6/c/ct;->a(F)Lcom/google/maps/api/android/lib6/c/cv;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Lcom/google/android/gms/b/l;
    .locals 1

    invoke-static {p1}, Lcom/google/maps/api/android/lib6/c/ct;->a(I)Lcom/google/maps/api/android/lib6/c/da;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/graphics/Bitmap;)Lcom/google/android/gms/b/l;
    .locals 1

    invoke-static {p1}, Lcom/google/maps/api/android/lib6/c/ct;->a(Landroid/graphics/Bitmap;)Lcom/google/maps/api/android/lib6/c/cx;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/maps/model/internal/BitmapDescriptorParcelable;)Lcom/google/android/gms/b/l;
    .locals 4

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/internal/BitmapDescriptorParcelable;->b()B

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/internal/BitmapDescriptorParcelable;->c()Landroid/os/Bundle;

    move-result-object v1

    packed-switch v0, :pswitch_data_0

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown type of BitmapDescriptor: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/internal/BitmapDescriptorParcelable;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/c/cs;->a(Landroid/graphics/Bitmap;)Lcom/google/android/gms/b/l;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_1
    const-string v0, "resourceId"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/google/maps/api/android/lib6/c/ct;->b(I)Lcom/google/maps/api/android/lib6/c/cy;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/b/l;
    .locals 1

    invoke-static {p1}, Lcom/google/maps/api/android/lib6/c/ct;->a(Ljava/lang/String;)Lcom/google/maps/api/android/lib6/c/cu;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/gms/b/l;
    .locals 1

    invoke-static {p1}, Lcom/google/maps/api/android/lib6/c/ct;->b(Ljava/lang/String;)Lcom/google/maps/api/android/lib6/c/cw;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/String;)Lcom/google/android/gms/b/l;
    .locals 1

    invoke-static {p1}, Lcom/google/maps/api/android/lib6/c/ct;->c(Ljava/lang/String;)Lcom/google/maps/api/android/lib6/c/cz;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v0

    return-object v0
.end method
