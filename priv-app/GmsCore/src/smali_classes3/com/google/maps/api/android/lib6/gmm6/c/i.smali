.class final Lcom/google/maps/api/android/lib6/gmm6/c/i;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field private synthetic a:Lcom/google/maps/api/android/lib6/gmm6/c/j;

.field private synthetic b:Lcom/google/maps/api/android/lib6/gmm6/c/h;


# direct methods
.method constructor <init>(Lcom/google/maps/api/android/lib6/gmm6/c/h;Lcom/google/maps/api/android/lib6/gmm6/c/j;)V
    .locals 0

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/i;->b:Lcom/google/maps/api/android/lib6/gmm6/c/h;

    iput-object p2, p0, Lcom/google/maps/api/android/lib6/gmm6/c/i;->a:Lcom/google/maps/api/android/lib6/gmm6/c/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 3

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/i;->b:Lcom/google/maps/api/android/lib6/gmm6/c/h;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/c/h;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/i;->b:Lcom/google/maps/api/android/lib6/gmm6/c/h;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/i;->a:Lcom/google/maps/api/android/lib6/gmm6/c/j;

    invoke-virtual {v0, v1}, Lcom/google/maps/api/android/lib6/gmm6/c/h;->b(Lcom/google/maps/api/android/lib6/gmm6/c/j;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/i;->b:Lcom/google/maps/api/android/lib6/gmm6/c/h;

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/gmm6/c/h;->a:Lcom/google/maps/api/android/lib6/gmm6/c/aq;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/gmm6/c/aq;->f()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_1

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0
.end method
