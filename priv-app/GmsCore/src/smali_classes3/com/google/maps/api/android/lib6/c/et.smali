.class public final Lcom/google/maps/api/android/lib6/c/et;
.super Landroid/support/v4/widget/ab;


# instance fields
.field private final b:Lcom/google/maps/api/android/lib6/c/am;

.field private c:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/google/maps/api/android/lib6/c/am;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/support/v4/widget/ab;-><init>(Landroid/view/View;)V

    iput-object p2, p0, Lcom/google/maps/api/android/lib6/c/et;->b:Lcom/google/maps/api/android/lib6/c/am;

    return-void
.end method

.method private static a(Lcom/google/maps/api/android/lib6/c/aj;)Ljava/lang/String;
    .locals 4

    if-nez p0, :cond_1

    const-string v0, ""

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const-string v0, ""

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/aj;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/aj;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, Lcom/google/k/a/db;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ". "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_2
    invoke-static {v2}, Lcom/google/k/a/db;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected final a(FF)I
    .locals 4

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/et;->c:Ljava/util/List;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/et;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/et;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->H()Landroid/graphics/Rect;

    move-result-object v0

    if-eqz v0, :cond_0

    float-to-int v2, p1

    float-to-int v3, p2

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_1
    return v1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/high16 v1, -0x80000000

    goto :goto_1
.end method

.method protected final a(ILandroid/support/v4/view/a/i;)V
    .locals 3

    const/4 v2, -0x1

    const/4 v1, -0x2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/et;->c:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/et;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    const-string v0, ""

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/i;->c(Ljava/lang/CharSequence;)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v1, v1, v2, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/i;->b(Landroid/graphics/Rect;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/et;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/api/android/lib6/c/aj;

    invoke-static {v0}, Lcom/google/maps/api/android/lib6/c/et;->a(Lcom/google/maps/api/android/lib6/c/aj;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/support/v4/view/a/i;->c(Ljava/lang/CharSequence;)V

    const/16 v1, 0x10

    invoke-virtual {p2, v1}, Landroid/support/v4/view/a/i;->a(I)V

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->H()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/i;->b(Landroid/graphics/Rect;)V

    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/i;->a(Z)V

    goto :goto_0
.end method

.method protected final a(ILandroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/et;->c:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/et;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/et;->b:Lcom/google/maps/api/android/lib6/c/am;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/am;->c()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/et;->c:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/et;->c:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/et;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_3

    :cond_2
    const-string v0, ""

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/et;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/api/android/lib6/c/aj;

    invoke-static {v0}, Lcom/google/maps/api/android/lib6/c/et;->a(Lcom/google/maps/api/android/lib6/c/aj;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected final a(Ljava/util/List;)V
    .locals 3

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/et;->b:Lcom/google/maps/api/android/lib6/c/am;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/am;->c()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/et;->c:Ljava/util/List;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/et;->c:Ljava/util/List;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/et;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected final c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final d()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/et;->b()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/et;->c:Ljava/util/List;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/et;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/c/et;->a(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
