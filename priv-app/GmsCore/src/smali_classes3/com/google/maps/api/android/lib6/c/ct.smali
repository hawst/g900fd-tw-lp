.class public abstract Lcom/google/maps/api/android/lib6/c/ct;
.super Ljava/lang/Object;


# static fields
.field private static final a:Lcom/google/maps/api/android/lib6/c/cy;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/google/maps/api/android/lib6/c/cy;

    sget v1, Lcom/google/android/gms/maps/ab;->G:I

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/maps/api/android/lib6/c/cy;-><init>(IB)V

    sput-object v0, Lcom/google/maps/api/android/lib6/c/ct;->a:Lcom/google/maps/api/android/lib6/c/cy;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    invoke-direct {p0}, Lcom/google/maps/api/android/lib6/c/ct;-><init>()V

    return-void
.end method

.method static a(Lcom/google/android/gms/b/l;)Lcom/google/maps/api/android/lib6/c/ct;
    .locals 1

    if-nez p0, :cond_0

    sget-object v0, Lcom/google/maps/api/android/lib6/c/ct;->a:Lcom/google/maps/api/android/lib6/c/cy;

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/b/p;->a(Lcom/google/android/gms/b/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/api/android/lib6/c/ct;

    goto :goto_0
.end method

.method static a(Lcom/google/android/gms/maps/model/a;)Lcom/google/maps/api/android/lib6/c/ct;
    .locals 1

    if-nez p0, :cond_0

    sget-object v0, Lcom/google/maps/api/android/lib6/c/ct;->a:Lcom/google/maps/api/android/lib6/c/cy;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/maps/model/a;->a()Lcom/google/android/gms/b/l;

    move-result-object v0

    invoke-static {v0}, Lcom/google/maps/api/android/lib6/c/ct;->a(Lcom/google/android/gms/b/l;)Lcom/google/maps/api/android/lib6/c/ct;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Lcom/google/maps/api/android/lib6/c/cu;
    .locals 2

    new-instance v0, Lcom/google/maps/api/android/lib6/c/cu;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/maps/api/android/lib6/c/cu;-><init>(Ljava/lang/String;B)V

    return-object v0
.end method

.method public static a(F)Lcom/google/maps/api/android/lib6/c/cv;
    .locals 2

    new-instance v0, Lcom/google/maps/api/android/lib6/c/cv;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/ct;->a:Lcom/google/maps/api/android/lib6/c/cy;

    invoke-direct {v0, v1, p0}, Lcom/google/maps/api/android/lib6/c/cv;-><init>(Lcom/google/maps/api/android/lib6/c/ct;F)V

    return-object v0
.end method

.method public static a(Landroid/graphics/Bitmap;)Lcom/google/maps/api/android/lib6/c/cx;
    .locals 2

    new-instance v0, Lcom/google/maps/api/android/lib6/c/cx;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/maps/api/android/lib6/c/cx;-><init>(Landroid/graphics/Bitmap;B)V

    return-object v0
.end method

.method public static a()Lcom/google/maps/api/android/lib6/c/cy;
    .locals 1

    sget-object v0, Lcom/google/maps/api/android/lib6/c/ct;->a:Lcom/google/maps/api/android/lib6/c/cy;

    return-object v0
.end method

.method public static a(I)Lcom/google/maps/api/android/lib6/c/da;
    .locals 2

    new-instance v0, Lcom/google/maps/api/android/lib6/c/da;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/maps/api/android/lib6/c/da;-><init>(IB)V

    return-object v0
.end method

.method public static b(Ljava/lang/String;)Lcom/google/maps/api/android/lib6/c/cw;
    .locals 2

    new-instance v0, Lcom/google/maps/api/android/lib6/c/cw;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/maps/api/android/lib6/c/cw;-><init>(Ljava/lang/String;B)V

    return-object v0
.end method

.method public static b(I)Lcom/google/maps/api/android/lib6/c/cy;
    .locals 2

    new-instance v0, Lcom/google/maps/api/android/lib6/c/cy;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/maps/api/android/lib6/c/cy;-><init>(IB)V

    return-object v0
.end method

.method public static c(Ljava/lang/String;)Lcom/google/maps/api/android/lib6/c/cz;
    .locals 2

    new-instance v0, Lcom/google/maps/api/android/lib6/c/cz;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/maps/api/android/lib6/c/cz;-><init>(Ljava/lang/String;B)V

    return-object v0
.end method


# virtual methods
.method public abstract a(Landroid/content/Context;)Landroid/graphics/Bitmap;
.end method
