.class public Lcom/google/maps/api/android/lib6/gmm6/o/cc;
.super Landroid/view/SurfaceView;

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# static fields
.field private static final a:Lcom/google/maps/api/android/lib6/gmm6/o/cl;


# instance fields
.field private b:Z

.field private c:Z

.field final g:Ljava/lang/ref/WeakReference;

.field h:Lcom/google/maps/api/android/lib6/gmm6/o/ck;

.field i:Lcom/google/maps/api/android/lib6/gmm6/o/cn;

.field j:Lcom/google/maps/api/android/lib6/gmm6/o/cg;

.field k:Lcom/google/maps/api/android/lib6/gmm6/o/ch;

.field l:Lcom/google/maps/api/android/lib6/gmm6/o/ci;

.field public m:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/maps/api/android/lib6/gmm6/o/cl;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/maps/api/android/lib6/gmm6/o/cl;-><init>(B)V

    sput-object v0, Lcom/google/maps/api/android/lib6/gmm6/o/cc;->a:Lcom/google/maps/api/android/lib6/gmm6/o/cl;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/o/cc;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/gmm6/o/cc;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x9

    if-ge v1, v2, :cond_0

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setFormat(I)V

    :cond_0
    return-void
.end method

.method static synthetic F()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method static synthetic G()Lcom/google/maps/api/android/lib6/gmm6/o/cm;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method static synthetic H()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method static synthetic I()Lcom/google/maps/api/android/lib6/gmm6/o/cl;
    .locals 1

    sget-object v0, Lcom/google/maps/api/android/lib6/gmm6/o/cc;->a:Lcom/google/maps/api/android/lib6/gmm6/o/cl;

    return-object v0
.end method

.method static synthetic a(Lcom/google/maps/api/android/lib6/gmm6/o/cc;)Lcom/google/maps/api/android/lib6/gmm6/o/cg;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/o/cc;->j:Lcom/google/maps/api/android/lib6/gmm6/o/cg;

    return-object v0
.end method

.method static synthetic b(Lcom/google/maps/api/android/lib6/gmm6/o/cc;)Lcom/google/maps/api/android/lib6/gmm6/o/ch;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/o/cc;->k:Lcom/google/maps/api/android/lib6/gmm6/o/ch;

    return-object v0
.end method

.method static synthetic c(Lcom/google/maps/api/android/lib6/gmm6/o/cc;)Lcom/google/maps/api/android/lib6/gmm6/o/ci;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/o/cc;->l:Lcom/google/maps/api/android/lib6/gmm6/o/ci;

    return-object v0
.end method

.method static synthetic d(Lcom/google/maps/api/android/lib6/gmm6/o/cc;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/gmm6/o/cc;->m:Z

    return v0
.end method

.method static synthetic e(Lcom/google/maps/api/android/lib6/gmm6/o/cc;)Lcom/google/maps/api/android/lib6/gmm6/o/cn;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/o/cc;->i:Lcom/google/maps/api/android/lib6/gmm6/o/cn;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/o/cc;->h:Lcom/google/maps/api/android/lib6/gmm6/o/ck;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/o/ck;->f()V

    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/o/cc;->h:Lcom/google/maps/api/android/lib6/gmm6/o/ck;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/o/ck;->e()V

    return-void
.end method

.method c()V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/o/cc;->h:Lcom/google/maps/api/android/lib6/gmm6/o/ck;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setRenderer has already been called for this instance."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method protected finalize()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/o/cc;->h:Lcom/google/maps/api/android/lib6/gmm6/o/ck;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/o/cc;->h:Lcom/google/maps/api/android/lib6/gmm6/o/ck;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/o/ck;->g()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    return-void

    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public final h(Z)V
    .locals 1

    iput-boolean p1, p0, Lcom/google/maps/api/android/lib6/gmm6/o/cc;->c:Z

    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/gmm6/o/cc;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/o/cc;->h:Lcom/google/maps/api/android/lib6/gmm6/o/ck;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/o/cc;->h:Lcom/google/maps/api/android/lib6/gmm6/o/ck;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/o/ck;->h()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/o/cc;->h:Lcom/google/maps/api/android/lib6/gmm6/o/ck;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/o/ck;->g()V

    :cond_0
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 4

    const/4 v1, 0x1

    invoke-super {p0}, Landroid/view/SurfaceView;->onAttachedToWindow()V

    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/gmm6/o/cc;->b:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/o/cc;->i:Lcom/google/maps/api/android/lib6/gmm6/o/cn;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/o/cc;->h:Lcom/google/maps/api/android/lib6/gmm6/o/ck;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/o/cc;->h:Lcom/google/maps/api/android/lib6/gmm6/o/ck;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/o/ck;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/o/cc;->h:Lcom/google/maps/api/android/lib6/gmm6/o/ck;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/o/cc;->h:Lcom/google/maps/api/android/lib6/gmm6/o/ck;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/o/ck;->a()I

    move-result v0

    :goto_0
    new-instance v2, Lcom/google/maps/api/android/lib6/gmm6/o/ck;

    iget-object v3, p0, Lcom/google/maps/api/android/lib6/gmm6/o/cc;->g:Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v3}, Lcom/google/maps/api/android/lib6/gmm6/o/ck;-><init>(Ljava/lang/ref/WeakReference;)V

    iput-object v2, p0, Lcom/google/maps/api/android/lib6/gmm6/o/cc;->h:Lcom/google/maps/api/android/lib6/gmm6/o/ck;

    if-eq v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/o/cc;->h:Lcom/google/maps/api/android/lib6/gmm6/o/ck;

    invoke-virtual {v1, v0}, Lcom/google/maps/api/android/lib6/gmm6/o/ck;->a(I)V

    :cond_1
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/o/cc;->h:Lcom/google/maps/api/android/lib6/gmm6/o/ck;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/o/ck;->start()V

    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/maps/api/android/lib6/gmm6/o/cc;->b:Z

    return-void

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/gmm6/o/cc;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/o/cc;->h:Lcom/google/maps/api/android/lib6/gmm6/o/ck;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/o/cc;->h:Lcom/google/maps/api/android/lib6/gmm6/o/ck;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/o/ck;->g()V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/maps/api/android/lib6/gmm6/o/cc;->b:Z

    invoke-super {p0}, Landroid/view/SurfaceView;->onDetachedFromWindow()V

    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/o/cc;->h:Lcom/google/maps/api/android/lib6/gmm6/o/ck;

    invoke-virtual {v0, p3, p4}, Lcom/google/maps/api/android/lib6/gmm6/o/ck;->a(II)V

    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/o/cc;->h:Lcom/google/maps/api/android/lib6/gmm6/o/ck;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/o/ck;->c()V

    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/o/cc;->h:Lcom/google/maps/api/android/lib6/gmm6/o/ck;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/o/ck;->d()V

    return-void
.end method

.method public y()V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/o/cc;->h:Lcom/google/maps/api/android/lib6/gmm6/o/ck;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/gmm6/o/ck;->b()V

    return-void
.end method
