.class final Lcom/google/maps/api/android/lib6/c/di;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/android/volley/y;


# instance fields
.field private final a:Landroid/support/v4/g/h;

.field private final b:Lcom/android/volley/y;


# direct methods
.method public constructor <init>(Landroid/support/v4/g/h;Lcom/android/volley/y;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/c/di;->a:Landroid/support/v4/g/h;

    iput-object p2, p0, Lcom/google/maps/api/android/lib6/c/di;->b:Lcom/android/volley/y;

    return-void
.end method


# virtual methods
.method public final a(Lcom/android/volley/p;Lcom/android/volley/ac;)V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/di;->b:Lcom/android/volley/y;

    invoke-interface {v0, p1, p2}, Lcom/android/volley/y;->a(Lcom/android/volley/p;Lcom/android/volley/ac;)V

    return-void
.end method

.method public final a(Lcom/android/volley/p;Lcom/android/volley/v;)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/di;->a:Landroid/support/v4/g/h;

    invoke-virtual {p1}, Lcom/android/volley/p;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/di;->b:Lcom/android/volley/y;

    invoke-interface {v0, p1, p2}, Lcom/android/volley/y;->a(Lcom/android/volley/p;Lcom/android/volley/v;)V

    return-void
.end method

.method public final a(Lcom/android/volley/p;Lcom/android/volley/v;Ljava/lang/Runnable;)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/di;->a:Landroid/support/v4/g/h;

    invoke-virtual {p1}, Lcom/android/volley/p;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/di;->b:Lcom/android/volley/y;

    invoke-interface {v0, p1, p2, p3}, Lcom/android/volley/y;->a(Lcom/android/volley/p;Lcom/android/volley/v;Ljava/lang/Runnable;)V

    return-void
.end method
