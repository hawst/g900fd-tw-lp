.class public final Lcom/google/maps/api/android/lib6/d/i;
.super Landroid/view/View;

# interfaces
.implements Lcom/google/maps/api/android/lib6/c/ae;


# instance fields
.field private final a:Lcom/google/maps/api/android/lib6/d/k;

.field private final b:Lcom/google/maps/api/android/lib6/d/m;

.field private final c:Lcom/google/maps/api/android/lib6/d/g;

.field private final d:Lcom/google/maps/api/android/lib6/d/h;

.field private final e:Landroid/content/res/Resources;

.field private final f:Lcom/google/maps/api/android/lib6/d/f;

.field private final g:Lcom/google/maps/api/android/lib6/d/a;

.field private h:Lcom/google/maps/api/android/lib6/d/p;

.field private final i:Landroid/view/GestureDetector;

.field private final j:Lcom/google/maps/api/android/lib6/d/l;

.field private k:Lcom/google/android/gms/maps/internal/ap;

.field private l:Lcom/google/android/gms/maps/internal/av;

.field private final m:Landroid/content/Context;

.field private final n:Lcom/google/maps/api/android/lib6/c/ek;

.field private final o:Lcom/google/maps/api/android/lib6/c/af;

.field private final p:Lcom/google/maps/api/android/lib6/c/et;

.field private final q:Lcom/google/maps/api/android/lib6/d/s;


# direct methods
.method private constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;Lcom/google/maps/api/android/lib6/c/dz;Landroid/view/View;Lcom/google/maps/api/android/lib6/c/dd;Lcom/google/maps/api/android/lib6/c/ek;Landroid/widget/TextView;)V
    .locals 8

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/d/i;->m:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/maps/api/android/lib6/d/i;->e:Landroid/content/res/Resources;

    new-instance v0, Lcom/google/maps/api/android/lib6/d/m;

    invoke-direct {v0, p0}, Lcom/google/maps/api/android/lib6/d/m;-><init>(Lcom/google/maps/api/android/lib6/d/i;)V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->b:Lcom/google/maps/api/android/lib6/d/m;

    new-instance v0, Lcom/google/maps/api/android/lib6/d/k;

    invoke-direct {v0, p0}, Lcom/google/maps/api/android/lib6/d/k;-><init>(Lcom/google/maps/api/android/lib6/d/i;)V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->a:Lcom/google/maps/api/android/lib6/d/k;

    new-instance v7, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v7, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v0, Lcom/google/maps/api/android/lib6/d/g;

    invoke-direct {v0, p0, p2}, Lcom/google/maps/api/android/lib6/d/g;-><init>(Lcom/google/maps/api/android/lib6/d/i;Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->c:Lcom/google/maps/api/android/lib6/d/g;

    invoke-static {v7}, Lcom/google/maps/api/android/lib6/d/h;->a(Landroid/os/Handler;)Lcom/google/maps/api/android/lib6/d/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->d:Lcom/google/maps/api/android/lib6/d/h;

    iget-object v0, p5, Lcom/google/maps/api/android/lib6/c/dd;->e:Lcom/google/maps/api/android/lib6/c/af;

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->o:Lcom/google/maps/api/android/lib6/c/af;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/maps/api/android/lib6/d/i;->setLayerType(ILandroid/graphics/Paint;)V

    :cond_0
    new-instance v0, Lcom/google/maps/api/android/lib6/d/l;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/d/i;->b:Lcom/google/maps/api/android/lib6/d/m;

    invoke-direct {v0, p0, v1}, Lcom/google/maps/api/android/lib6/d/l;-><init>(Lcom/google/maps/api/android/lib6/d/i;Lcom/google/maps/api/android/lib6/d/m;)V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->j:Lcom/google/maps/api/android/lib6/d/l;

    new-instance v0, Landroid/view/GestureDetector;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/d/i;->j:Lcom/google/maps/api/android/lib6/d/l;

    invoke-direct {v0, v1}, Landroid/view/GestureDetector;-><init>(Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->i:Landroid/view/GestureDetector;

    new-instance v0, Lcom/google/maps/api/android/lib6/d/f;

    invoke-direct {v0, p0}, Lcom/google/maps/api/android/lib6/d/f;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->f:Lcom/google/maps/api/android/lib6/d/f;

    new-instance v0, Lcom/google/maps/api/android/lib6/d/a;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/d/i;->f:Lcom/google/maps/api/android/lib6/d/f;

    invoke-virtual {p3}, Lcom/google/maps/api/android/lib6/c/dz;->b()Lcom/android/volley/s;

    move-result-object v3

    iget-object v4, p0, Lcom/google/maps/api/android/lib6/d/i;->c:Lcom/google/maps/api/android/lib6/d/g;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/d/i;->m:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/maps/api/android/lib6/c/by;->b(Landroid/content/Context;)Z

    move-result v6

    move-object v2, p7

    invoke-direct/range {v0 .. v6}, Lcom/google/maps/api/android/lib6/d/a;-><init>(Lcom/google/maps/api/android/lib6/d/f;Landroid/widget/TextView;Lcom/android/volley/s;Lcom/google/maps/api/android/lib6/d/g;Ljava/util/Calendar;Z)V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->g:Lcom/google/maps/api/android/lib6/d/a;

    iput-object p6, p0, Lcom/google/maps/api/android/lib6/d/i;->n:Lcom/google/maps/api/android/lib6/c/ek;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/d/i;->setFocusable(Z)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/d/i;->setClickable(Z)V

    new-instance v0, Lcom/google/maps/api/android/lib6/c/et;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/d/i;->b:Lcom/google/maps/api/android/lib6/d/m;

    invoke-direct {v0, p0, v1}, Lcom/google/maps/api/android/lib6/c/et;-><init>(Landroid/view/View;Lcom/google/maps/api/android/lib6/c/am;)V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->p:Lcom/google/maps/api/android/lib6/c/et;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->p:Lcom/google/maps/api/android/lib6/c/et;

    invoke-static {p0, v0}, Landroid/support/v4/view/ay;->a(Landroid/view/View;Landroid/support/v4/view/a;)V

    new-instance v0, Lcom/google/maps/api/android/lib6/d/s;

    iget-object v1, p5, Lcom/google/maps/api/android/lib6/c/dd;->a:Landroid/widget/RelativeLayout;

    invoke-static {v7}, Lcom/google/maps/api/android/lib6/c/ah;->a(Landroid/os/Handler;)Ljava/util/concurrent/Executor;

    move-result-object v2

    invoke-direct {v0, p0, p4, v1, v2}, Lcom/google/maps/api/android/lib6/d/s;-><init>(Landroid/view/View;Landroid/view/View;Landroid/view/View;Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->q:Lcom/google/maps/api/android/lib6/d/s;

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/res/Resources;Lcom/google/maps/api/android/lib6/c/dz;Ljava/util/concurrent/ScheduledExecutorService;Landroid/view/View;Lcom/google/maps/api/android/lib6/c/dd;Lcom/google/maps/api/android/lib6/c/ek;Landroid/widget/TextView;)Lcom/google/maps/api/android/lib6/d/i;
    .locals 8

    new-instance v0, Lcom/google/maps/api/android/lib6/d/i;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object v7, p7

    invoke-direct/range {v0 .. v7}, Lcom/google/maps/api/android/lib6/d/i;-><init>(Landroid/content/Context;Landroid/content/res/Resources;Lcom/google/maps/api/android/lib6/c/dz;Landroid/view/View;Lcom/google/maps/api/android/lib6/c/dd;Lcom/google/maps/api/android/lib6/c/ek;Landroid/widget/TextView;)V

    return-object v0
.end method


# virtual methods
.method public final A()V
    .locals 0

    return-void
.end method

.method public final B()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final C()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final D()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final E()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final a()V
    .locals 0

    return-void
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->g:Lcom/google/maps/api/android/lib6/d/a;

    invoke-virtual {v0, p1}, Lcom/google/maps/api/android/lib6/d/a;->a(I)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/internal/ap;)V
    .locals 0

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/d/i;->k:Lcom/google/android/gms/maps/internal/ap;

    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/internal/av;)V
    .locals 0

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/d/i;->l:Lcom/google/android/gms/maps/internal/av;

    return-void
.end method

.method public final a(Z)V
    .locals 2

    const/4 v0, 0x5

    const-string v1, "Traffic is not supported in Lite Mode"

    invoke-static {v0, v1}, Lcom/google/maps/api/android/lib6/c/by;->a(ILjava/lang/String;)V

    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 4

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->h:Lcom/google/maps/api/android/lib6/d/p;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->k:Lcom/google/android/gms/maps/internal/ap;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->b:Lcom/google/maps/api/android/lib6/d/m;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/d/m;->a()Lcom/google/maps/api/android/lib6/c/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/d/i;->n:Lcom/google/maps/api/android/lib6/c/ek;

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/d/i;->h:Lcom/google/maps/api/android/lib6/d/p;

    iget-object v2, v2, Lcom/google/maps/api/android/lib6/d/p;->e:Lcom/google/android/gms/maps/model/CameraPosition;

    iget-object v3, p0, Lcom/google/maps/api/android/lib6/d/i;->b:Lcom/google/maps/api/android/lib6/d/m;

    invoke-virtual {v3}, Lcom/google/maps/api/android/lib6/d/m;->b()Z

    move-result v3

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/maps/api/android/lib6/c/ek;->a(Lcom/google/android/gms/maps/model/CameraPosition;Lcom/google/maps/api/android/lib6/c/aj;Z)V

    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->h:Lcom/google/maps/api/android/lib6/d/p;

    new-instance v1, Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/google/maps/api/android/lib6/d/p;->a(Landroid/graphics/Point;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/google/maps/api/android/lib6/d/i;->k:Lcom/google/android/gms/maps/internal/ap;

    invoke-interface {v1, v0}, Lcom/google/android/gms/maps/internal/ap;->a(Lcom/google/android/gms/maps/model/LatLng;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final b()V
    .locals 0

    return-void
.end method

.method public final b(Landroid/view/MotionEvent;)Z
    .locals 4

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->h:Lcom/google/maps/api/android/lib6/d/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->l:Lcom/google/android/gms/maps/internal/av;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->h:Lcom/google/maps/api/android/lib6/d/p;

    new-instance v1, Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/google/maps/api/android/lib6/d/p;->a(Landroid/graphics/Point;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/google/maps/api/android/lib6/d/i;->l:Lcom/google/android/gms/maps/internal/av;

    invoke-interface {v1, v0}, Lcom/google/android/gms/maps/internal/av;->a(Lcom/google/android/gms/maps/model/LatLng;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final b(Z)Z
    .locals 2

    const/4 v0, 0x5

    const-string v1, "Indoor is not supported in Lite Mode"

    invoke-static {v0, v1}, Lcom/google/maps/api/android/lib6/c/by;->a(ILjava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method

.method public final c()V
    .locals 0

    return-void
.end method

.method public final c(Z)V
    .locals 2

    const/4 v0, 0x5

    const-string v1, "Buildings are not supported in Lite Mode"

    invoke-static {v0, v1}, Lcom/google/maps/api/android/lib6/c/by;->a(ILjava/lang/String;)V

    return-void
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->g:Lcom/google/maps/api/android/lib6/d/a;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/d/a;->a()V

    return-void
.end method

.method public final d(Z)V
    .locals 2

    const/4 v0, 0x5

    const-string v1, "Toggling gestures is not supported in Lite Mode"

    invoke-static {v0, v1}, Lcom/google/maps/api/android/lib6/c/by;->a(ILjava/lang/String;)V

    return-void
.end method

.method public final dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->p:Lcom/google/maps/api/android/lib6/c/et;

    invoke-virtual {v0, p1}, Lcom/google/maps/api/android/lib6/c/et;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final e(Z)V
    .locals 2

    const/4 v0, 0x5

    const-string v1, "Toggling gestures is not supported in Lite Mode"

    invoke-static {v0, v1}, Lcom/google/maps/api/android/lib6/c/by;->a(ILjava/lang/String;)V

    return-void
.end method

.method public final f()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public final f(Z)V
    .locals 2

    const/4 v0, 0x5

    const-string v1, "Toggling gestures is not supported in Lite Mode"

    invoke-static {v0, v1}, Lcom/google/maps/api/android/lib6/c/by;->a(ILjava/lang/String;)V

    return-void
.end method

.method public final g()Lcom/google/maps/api/android/lib6/c/dj;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->c:Lcom/google/maps/api/android/lib6/d/g;

    return-object v0
.end method

.method public final g(Z)V
    .locals 2

    const/4 v0, 0x5

    const-string v1, "Toggling gestures is not supported in Lite Mode"

    invoke-static {v0, v1}, Lcom/google/maps/api/android/lib6/c/by;->a(ILjava/lang/String;)V

    return-void
.end method

.method public final getResources()Landroid/content/res/Resources;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->e:Landroid/content/res/Resources;

    return-object v0
.end method

.method public final h()Lcom/google/maps/api/android/lib6/c/am;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->b:Lcom/google/maps/api/android/lib6/d/m;

    return-object v0
.end method

.method public final i()Lcom/google/maps/api/android/lib6/c/at;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->b:Lcom/google/maps/api/android/lib6/d/m;

    return-object v0
.end method

.method public final j()Lcom/google/maps/api/android/lib6/c/aq;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->a:Lcom/google/maps/api/android/lib6/d/k;

    return-object v0
.end method

.method public final k()Lcom/google/maps/api/android/lib6/c/e;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final l()Lcom/google/maps/api/android/lib6/c/bi;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->q:Lcom/google/maps/api/android/lib6/d/s;

    return-object v0
.end method

.method public final m()Lcom/google/maps/api/android/lib6/c/ad;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->d:Lcom/google/maps/api/android/lib6/d/h;

    return-object v0
.end method

.method public final n()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final o()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected final onDraw(Landroid/graphics/Canvas;)V
    .locals 11

    const/high16 v10, 0x40000000    # 2.0f

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->f:Lcom/google/maps/api/android/lib6/d/f;

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/d/i;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/d/i;->getHeight()I

    move-result v2

    iget-object v3, v0, Lcom/google/maps/api/android/lib6/d/f;->a:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_2

    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iget-object v4, v0, Lcom/google/maps/api/android/lib6/d/f;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    sub-int/2addr v1, v4

    int-to-float v1, v1

    div-float/2addr v1, v10

    iget-object v4, v0, Lcom/google/maps/api/android/lib6/d/f;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    sub-int/2addr v2, v4

    int-to-float v2, v2

    div-float/2addr v2, v10

    iget-object v4, v0, Lcom/google/maps/api/android/lib6/d/f;->a:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v4, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :goto_0
    iget-object v0, v0, Lcom/google/maps/api/android/lib6/d/f;->b:Lcom/google/maps/api/android/lib6/d/p;

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->h:Lcom/google/maps/api/android/lib6/d/p;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->h:Lcom/google/maps/api/android/lib6/d/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->b:Lcom/google/maps/api/android/lib6/d/m;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/d/i;->h:Lcom/google/maps/api/android/lib6/d/p;

    invoke-virtual {v0, p1, v1}, Lcom/google/maps/api/android/lib6/d/m;->a(Landroid/graphics/Canvas;Lcom/google/maps/api/android/lib6/d/p;)V

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/d/i;->a:Lcom/google/maps/api/android/lib6/d/k;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->h:Lcom/google/maps/api/android/lib6/d/p;

    iget-boolean v2, v1, Lcom/google/maps/api/android/lib6/d/k;->b:Z

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/google/maps/api/android/lib6/d/k;->d:Landroid/location/Location;

    if-nez v2, :cond_3

    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->p:Lcom/google/maps/api/android/lib6/c/et;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/et;->d()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->b:Lcom/google/maps/api/android/lib6/d/m;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/d/m;->b()Z

    move-result v0

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/d/i;->b:Lcom/google/maps/api/android/lib6/d/m;

    invoke-virtual {v1}, Lcom/google/maps/api/android/lib6/d/m;->a()Lcom/google/maps/api/android/lib6/c/aj;

    move-result-object v1

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/d/i;->b:Lcom/google/maps/api/android/lib6/d/m;

    iget-object v3, v2, Lcom/google/maps/api/android/lib6/d/m;->e:Lcom/google/maps/api/android/lib6/c/aj;

    if-eqz v3, :cond_1

    iget-object v3, v2, Lcom/google/maps/api/android/lib6/d/m;->c:Ljava/util/List;

    iget-object v4, v2, Lcom/google/maps/api/android/lib6/d/m;->e:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v2, v2, Lcom/google/maps/api/android/lib6/d/m;->e:Lcom/google/maps/api/android/lib6/c/aj;

    :cond_1
    if-eqz v1, :cond_8

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/d/i;->o:Lcom/google/maps/api/android/lib6/c/af;

    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1, v0}, Lcom/google/maps/api/android/lib6/c/af;->a(ZLcom/google/maps/api/android/lib6/c/aj;Z)V

    :goto_2
    return-void

    :cond_2
    invoke-static {p1}, Lcom/google/maps/api/android/lib6/d/f;->a(Landroid/graphics/Canvas;)V

    goto :goto_0

    :cond_3
    new-instance v2, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v3, v1, Lcom/google/maps/api/android/lib6/d/k;->d:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    iget-object v3, v1, Lcom/google/maps/api/android/lib6/d/k;->d:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-interface {v0, v2}, Lcom/google/maps/api/android/lib6/c/ba;->a(Lcom/google/android/gms/maps/model/LatLng;)Landroid/graphics/Point;

    move-result-object v3

    iget-object v4, v1, Lcom/google/maps/api/android/lib6/d/k;->d:Landroid/location/Location;

    invoke-virtual {v4}, Landroid/location/Location;->hasAccuracy()Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, v1, Lcom/google/maps/api/android/lib6/d/k;->d:Landroid/location/Location;

    invoke-virtual {v4}, Landroid/location/Location;->getAccuracy()F

    move-result v4

    float-to-double v4, v4

    invoke-static {v4, v5}, Lcom/google/maps/api/android/lib6/c/ej;->a(D)D

    move-result-wide v4

    new-instance v6, Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v8, v2, Lcom/google/android/gms/maps/model/LatLng;->a:D

    add-double/2addr v4, v8

    iget-wide v8, v2, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-direct {v6, v4, v5, v8, v9}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-interface {v0, v6}, Lcom/google/maps/api/android/lib6/c/ba;->a(Lcom/google/android/gms/maps/model/LatLng;)Landroid/graphics/Point;

    move-result-object v0

    iget v2, v3, Landroid/graphics/Point;->y:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    sub-int v0, v2, v0

    int-to-float v0, v0

    iget-object v2, v1, Lcom/google/maps/api/android/lib6/d/k;->c:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v2, v1, Lcom/google/maps/api/android/lib6/d/k;->c:Landroid/graphics/Paint;

    invoke-virtual {v2, v10}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v2, v1, Lcom/google/maps/api/android/lib6/d/k;->c:Landroid/graphics/Paint;

    iget-object v4, v1, Lcom/google/maps/api/android/lib6/d/k;->a:Landroid/content/res/Resources;

    sget v5, Lcom/google/android/gms/maps/z;->b:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    iget v2, v3, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget v4, v3, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    iget-object v5, v1, Lcom/google/maps/api/android/lib6/d/k;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v4, v0, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    iget-object v2, v1, Lcom/google/maps/api/android/lib6/d/k;->c:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v2, v1, Lcom/google/maps/api/android/lib6/d/k;->c:Landroid/graphics/Paint;

    iget-object v4, v1, Lcom/google/maps/api/android/lib6/d/k;->a:Landroid/content/res/Resources;

    sget v5, Lcom/google/android/gms/maps/z;->a:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    iget v2, v3, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget v4, v3, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    iget-object v5, v1, Lcom/google/maps/api/android/lib6/d/k;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v4, v0, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    iget-object v0, v1, Lcom/google/maps/api/android/lib6/d/k;->c:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->reset()V

    :cond_4
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    iget-object v0, v1, Lcom/google/maps/api/android/lib6/d/k;->d:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->hasBearing()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, v1, Lcom/google/maps/api/android/lib6/d/k;->d:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getBearing()F

    move-result v0

    invoke-virtual {v2, v0}, Landroid/graphics/Matrix;->setRotate(F)V

    iget-object v0, v1, Lcom/google/maps/api/android/lib6/d/k;->f:Landroid/graphics/Bitmap;

    if-nez v0, :cond_5

    iget-object v0, v1, Lcom/google/maps/api/android/lib6/d/k;->a:Landroid/content/res/Resources;

    sget v4, Lcom/google/android/gms/maps/ab;->i:I

    invoke-static {v0, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, v1, Lcom/google/maps/api/android/lib6/d/k;->f:Landroid/graphics/Bitmap;

    :cond_5
    iget-object v0, v1, Lcom/google/maps/api/android/lib6/d/k;->f:Landroid/graphics/Bitmap;

    :goto_3
    invoke-static {v0}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    neg-int v4, v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    neg-int v5, v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    iget-object v4, v1, Lcom/google/maps/api/android/lib6/d/k;->a:Landroid/content/res/Resources;

    sget v5, Lcom/google/android/gms/maps/aa;->l:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    div-float v5, v4, v5

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v4, v6

    invoke-virtual {v2, v5, v4}, Landroid/graphics/Matrix;->postScale(FF)Z

    iget v4, v3, Landroid/graphics/Point;->x:I

    int-to-float v4, v4

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    invoke-virtual {v2, v4, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v1, v1, Lcom/google/maps/api/android/lib6/d/k;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    goto/16 :goto_1

    :cond_6
    iget-object v0, v1, Lcom/google/maps/api/android/lib6/d/k;->e:Landroid/graphics/Bitmap;

    if-nez v0, :cond_7

    iget-object v0, v1, Lcom/google/maps/api/android/lib6/d/k;->a:Landroid/content/res/Resources;

    sget v4, Lcom/google/android/gms/maps/ab;->a:I

    invoke-static {v0, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, v1, Lcom/google/maps/api/android/lib6/d/k;->e:Landroid/graphics/Bitmap;

    :cond_7
    iget-object v0, v1, Lcom/google/maps/api/android/lib6/d/k;->e:Landroid/graphics/Bitmap;

    goto :goto_3

    :cond_8
    if-nez v0, :cond_9

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/d/i;->o:Lcom/google/maps/api/android/lib6/c/af;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v1, v0}, Lcom/google/maps/api/android/lib6/c/af;->a(ZLcom/google/maps/api/android/lib6/c/aj;Z)V

    goto/16 :goto_2

    :cond_9
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->o:Lcom/google/maps/api/android/lib6/c/af;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/af;->b()V

    goto/16 :goto_2
.end method

.method protected final onSizeChanged(IIII)V
    .locals 1

    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->g:Lcom/google/maps/api/android/lib6/d/a;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/d/a;->a()V

    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->i:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    const/4 v0, 0x1

    return v0
.end method

.method public final p()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final q()Ljava/lang/String;
    .locals 1

    const-string v0, "L"

    return-object v0
.end method

.method public final synthetic r()Lcom/google/maps/api/android/lib6/c/ba;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/i;->c:Lcom/google/maps/api/android/lib6/d/g;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/d/g;->b()Lcom/google/maps/api/android/lib6/d/p;

    move-result-object v0

    return-object v0
.end method

.method public final setPadding(IIII)V
    .locals 0

    return-void
.end method
