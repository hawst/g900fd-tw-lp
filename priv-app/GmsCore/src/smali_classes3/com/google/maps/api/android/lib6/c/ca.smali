.class public final Lcom/google/maps/api/android/lib6/c/ca;
.super Lcom/google/maps/api/android/lib6/c/bz;


# static fields
.field private static final c:Lcom/google/maps/api/android/lib6/c/bz;


# instance fields
.field private final a:Ljava/lang/Thread;

.field private final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-class v0, Lcom/google/maps/api/android/lib6/c/ca;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    new-instance v0, Lcom/google/maps/api/android/lib6/c/ca;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    const-string v2, "Not on the main thread"

    invoke-direct {v0, v1, v2}, Lcom/google/maps/api/android/lib6/c/ca;-><init>(Ljava/lang/Thread;Ljava/lang/String;)V

    sput-object v0, Lcom/google/maps/api/android/lib6/c/ca;->c:Lcom/google/maps/api/android/lib6/c/bz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/Thread;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/maps/api/android/lib6/c/bz;-><init>()V

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/c/ca;->a:Ljava/lang/Thread;

    iput-object p2, p0, Lcom/google/maps/api/android/lib6/c/ca;->b:Ljava/lang/String;

    return-void
.end method

.method public static b()Lcom/google/maps/api/android/lib6/c/bz;
    .locals 1

    sget-object v0, Lcom/google/maps/api/android/lib6/c/ca;->c:Lcom/google/maps/api/android/lib6/c/bz;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/ca;->a:Ljava/lang/Thread;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/ca;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/k/a/cj;->b(ZLjava/lang/Object;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
