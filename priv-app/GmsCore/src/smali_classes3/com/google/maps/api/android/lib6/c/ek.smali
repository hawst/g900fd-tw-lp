.class public final Lcom/google/maps/api/android/lib6/c/ek;
.super Ljava/lang/Object;


# instance fields
.field final a:Lcom/google/maps/api/android/lib6/c/cd;

.field private final b:Landroid/content/Context;

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/maps/api/android/lib6/c/cd;)V
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lcom/google/maps/api/android/lib6/c/ek;->d:Z

    iput-boolean v1, p0, Lcom/google/maps/api/android/lib6/c/ek;->e:Z

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/c/ek;->b:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/maps/api/android/lib6/c/ek;->a:Lcom/google/maps/api/android/lib6/c/cd;

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/c/ek;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.google.android.apps.gmm"

    invoke-static {v3, v2}, Lcom/google/maps/api/android/lib6/c/ek;->a(Ljava/lang/String;Landroid/content/pm/PackageManager;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "com.google.android.apps.gmm"

    iput-object v3, p0, Lcom/google/maps/api/android/lib6/c/ek;->c:Ljava/lang/String;

    :goto_0
    iget-object v3, p0, Lcom/google/maps/api/android/lib6/c/ek;->c:Ljava/lang/String;

    if-eqz v3, :cond_0

    :try_start_0
    iget-object v3, p0, Lcom/google/maps/api/android/lib6/c/ek;->c:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    iget v3, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    const v2, 0x29b92700

    if-le v3, v2, :cond_3

    move v2, v0

    :goto_1
    iput-boolean v2, p0, Lcom/google/maps/api/android/lib6/c/ek;->d:Z

    const v2, 0x29e6edc0

    if-le v3, v2, :cond_4

    :goto_2
    iput-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/ek;->e:Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_3
    return-void

    :cond_1
    const-string v3, "com.google.android.apps.maps"

    invoke-static {v3, v2}, Lcom/google/maps/api/android/lib6/c/ek;->a(Ljava/lang/String;Landroid/content/pm/PackageManager;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "com.google.android.apps.maps"

    iput-object v3, p0, Lcom/google/maps/api/android/lib6/c/ek;->c:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/maps/api/android/lib6/c/ek;->c:Ljava/lang/String;

    goto :goto_0

    :cond_3
    move v2, v1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2

    :catch_0
    move-exception v0

    goto :goto_3
.end method

.method private static a(Ljava/lang/String;Landroid/content/pm/PackageManager;)Z
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p1, p0, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/maps/model/CameraPosition;Lcom/google/maps/api/android/lib6/c/aj;Z)V
    .locals 4

    if-eqz p3, :cond_2

    if-nez p2, :cond_1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ek;->a:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->bY:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "geo:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, Lcom/google/android/gms/maps/model/CameraPosition;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, v1, Lcom/google/android/gms/maps/model/LatLng;->a:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/gms/maps/model/CameraPosition;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, v1, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "?z="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/maps/api/android/lib6/c/ek;->d:Z

    if-eqz v1, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Lcom/google/android/gms/maps/model/CameraPosition;->b:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/google/maps/api/android/lib6/c/aj;->c()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "&q="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, v1, Lcom/google/android/gms/maps/model/LatLng;->a:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, v1, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/maps/api/android/lib6/c/aj;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/k/a/db;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/google/maps/api/android/lib6/c/ek;->e:Z

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/c/ek;->a(Ljava/lang/String;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ek;->a:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->bX:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    goto/16 :goto_0

    :cond_2
    if-eqz p2, :cond_3

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ek;->a:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->bW:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ek;->a:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->bV:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    goto/16 :goto_0

    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Lcom/google/android/gms/maps/model/CameraPosition;->b:F

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1
.end method

.method final a(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/ek;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/ek;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/ek;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method
