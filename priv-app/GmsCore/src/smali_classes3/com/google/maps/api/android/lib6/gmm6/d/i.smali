.class final Lcom/google/maps/api/android/lib6/gmm6/d/i;
.super Lcom/google/maps/api/android/lib6/b/b;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/google/p/a/b/b/f;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/p/a/b/b/f;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/maps/api/android/lib6/b/b;-><init>()V

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/gmm6/d/i;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/maps/api/android/lib6/gmm6/d/i;->b:Lcom/google/p/a/b/b/f;

    return-void
.end method

.method static synthetic a(Lcom/google/maps/api/android/lib6/gmm6/d/i;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/d/i;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/google/maps/api/android/lib6/gmm6/d/i;)Lcom/google/p/a/b/b/f;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/d/i;->b:Lcom/google/p/a/b/b/f;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/io/DataOutput;)V
    .locals 9

    const/4 v8, -0x1

    new-instance v1, Lcom/google/p/a/b/b/f;

    sget-object v0, Lcom/google/af/d/a/a/l;->a:Lcom/google/p/a/b/b/h;

    invoke-direct {v1, v0}, Lcom/google/p/a/b/b/f;-><init>(Lcom/google/p/a/b/b/h;)V

    const-class v2, Lcom/google/maps/api/android/lib6/gmm6/d/g;

    monitor-enter v2

    :try_start_0
    invoke-static {}, Lcom/google/maps/api/android/lib6/gmm6/d/g;->f()Lcom/google/p/a/b/b/f;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/google/p/a/b/b/f;->j(I)I

    move-result v3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_2

    invoke-static {}, Lcom/google/maps/api/android/lib6/gmm6/d/g;->f()Lcom/google/p/a/b/b/f;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5, v0}, Lcom/google/p/a/b/b/f;->c(II)Lcom/google/p/a/b/b/f;

    move-result-object v4

    new-instance v5, Lcom/google/p/a/b/b/f;

    sget-object v6, Lcom/google/af/d/a/a/l;->d:Lcom/google/p/a/b/b/h;

    invoke-direct {v5, v6}, Lcom/google/p/a/b/b/f;-><init>(Lcom/google/p/a/b/b/h;)V

    const/4 v6, 0x1

    const/4 v7, -0x1

    invoke-static {v4, v6, v7}, Lcom/google/p/a/b/b/j;->a(Lcom/google/p/a/b/b/f;II)I

    move-result v6

    if-eq v6, v8, :cond_0

    const/4 v7, 0x1

    invoke-virtual {v5, v7, v6}, Lcom/google/p/a/b/b/f;->f(II)Lcom/google/p/a/b/b/f;

    :cond_0
    const/4 v6, 0x2

    invoke-virtual {v4, v6}, Lcom/google/p/a/b/b/f;->i(I)Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v6, 0x2

    invoke-virtual {v4, v6}, Lcom/google/p/a/b/b/f;->e(I)J

    move-result-wide v6

    const/4 v4, 0x2

    invoke-virtual {v5, v4, v6, v7}, Lcom/google/p/a/b/b/f;->a(IJ)Lcom/google/p/a/b/b/f;

    :cond_1
    const/4 v4, 0x1

    invoke-virtual {v1, v4, v5}, Lcom/google/p/a/b/b/f;->a(ILcom/google/p/a/b/b/f;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/d/i;->b:Lcom/google/p/a/b/b/f;

    invoke-static {v1, v0}, Lcom/google/maps/api/android/lib6/gmm6/d/g;->a(Lcom/google/p/a/b/b/f;Lcom/google/p/a/b/b/f;)V

    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v1, p1}, Lcom/google/p/a/b/b/f;->a(Ljava/io/OutputStream;)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final a()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final a(Ljava/io/DataInput;)Z
    .locals 8

    const/4 v0, 0x0

    const/4 v1, 0x1

    sget-object v2, Lcom/google/af/d/a/a/l;->c:Lcom/google/p/a/b/b/h;

    invoke-static {v2, p1}, Lcom/google/p/a/b/b/j;->a(Lcom/google/p/a/b/b/h;Ljava/io/DataInput;)Lcom/google/p/a/b/b/f;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/p/a/b/b/f;->j(I)I

    move-result v4

    const-class v5, Lcom/google/maps/api/android/lib6/gmm6/d/g;

    monitor-enter v5

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_1

    const/4 v6, 0x1

    :try_start_0
    invoke-virtual {v3, v6, v2}, Lcom/google/p/a/b/b/f;->c(II)Lcom/google/p/a/b/b/f;

    move-result-object v6

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Lcom/google/p/a/b/b/f;->i(I)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-static {v6}, Lcom/google/maps/api/android/lib6/gmm6/d/g;->a(Lcom/google/p/a/b/b/f;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-static {v6}, Lcom/google/maps/api/android/lib6/gmm6/d/g;->b(Lcom/google/p/a/b/b/f;)V

    move v0, v1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_2

    invoke-static {}, Lcom/google/maps/api/android/lib6/gmm6/d/g;->f()Lcom/google/p/a/b/b/f;

    move-result-object v0

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/gmm6/d/i;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/maps/api/android/lib6/gmm6/d/g;->a(Lcom/google/p/a/b/b/f;Ljava/lang/String;)Z

    :cond_2
    invoke-static {}, Lcom/google/maps/api/android/lib6/gmm6/d/g;->g()Z

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Lcom/google/maps/api/android/lib6/gmm6/d/g;->h()V

    invoke-static {}, Lcom/google/maps/api/android/lib6/gmm6/d/g;->i()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/google/maps/api/android/lib6/gmm6/d/g;->j()Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    :try_start_1
    invoke-static {}, Lcom/google/maps/api/android/lib6/gmm6/d/g;->k()Z

    invoke-static {}, Lcom/google/maps/api/android/lib6/gmm6/d/g;->l()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/google/maps/api/android/lib6/b/h;->a()Lcom/google/maps/api/android/lib6/b/h;

    move-result-object v0

    iget-object v3, p0, Lcom/google/maps/api/android/lib6/gmm6/d/i;->a:Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/maps/api/android/lib6/gmm6/d/i;->b:Lcom/google/p/a/b/b/f;

    invoke-static {v0, v3, v4, v5}, Lcom/google/maps/api/android/lib6/gmm6/d/g;->a(Lcom/google/maps/api/android/lib6/b/h;Ljava/lang/String;ZLcom/google/p/a/b/b/f;)V

    :goto_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_3
    return v1

    :catchall_0
    move-exception v0

    monitor-exit v5

    throw v0

    :cond_4
    :try_start_2
    new-instance v0, Lcom/google/maps/api/android/lib6/gmm6/d/j;

    invoke-static {}, Lcom/google/maps/api/android/lib6/b/y;->a()Lcom/google/p/a/d/g;

    move-result-object v3

    invoke-direct {v0, p0, v3}, Lcom/google/maps/api/android/lib6/gmm6/d/j;-><init>(Lcom/google/maps/api/android/lib6/gmm6/d/i;Lcom/google/p/a/d/g;)V

    invoke-static {v0}, Lcom/google/maps/api/android/lib6/gmm6/d/g;->a(Lcom/google/p/a/d/h;)Lcom/google/p/a/d/h;

    invoke-static {}, Lcom/google/maps/api/android/lib6/gmm6/d/g;->m()Lcom/google/p/a/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/p/a/d/h;->j()V

    invoke-static {}, Lcom/google/maps/api/android/lib6/gmm6/d/g;->m()Lcom/google/p/a/d/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/p/a/d/h;->d()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final h()I
    .locals 1

    const/16 v0, 0x4b

    return v0
.end method
