.class public final Lcom/google/maps/api/android/lib6/c/dx;
.super Lcom/google/android/gms/maps/model/internal/h;

# interfaces
.implements Lcom/google/maps/api/android/lib6/c/as;
.implements Lcom/google/maps/api/android/lib6/c/au;


# static fields
.field private static final a:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static final b:Lcom/google/android/gms/maps/model/CircleOptions;


# instance fields
.field private final c:Ljava/lang/String;

.field private final d:Lcom/google/maps/api/android/lib6/c/ar;

.field private final e:Lcom/google/maps/api/android/lib6/c/cd;

.field private f:Lcom/google/maps/api/android/lib6/c/av;

.field private g:Lcom/google/android/gms/maps/model/LatLng;

.field private h:D

.field private i:Ljava/util/List;

.field private j:I

.field private k:I

.field private l:F

.field private m:F

.field private n:Z

.field private o:Z

.field private p:Lcom/google/maps/api/android/lib6/c/bz;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/google/maps/api/android/lib6/c/dx;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Lcom/google/android/gms/maps/model/CircleOptions;

    invoke-direct {v0}, Lcom/google/android/gms/maps/model/CircleOptions;-><init>()V

    sput-object v0, Lcom/google/maps/api/android/lib6/c/dx;->b:Lcom/google/android/gms/maps/model/CircleOptions;

    return-void
.end method

.method constructor <init>(Lcom/google/android/gms/maps/model/CircleOptions;Lcom/google/maps/api/android/lib6/c/ar;Lcom/google/maps/api/android/lib6/c/cd;Lcom/google/maps/api/android/lib6/c/bz;)V
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/maps/model/internal/h;-><init>()V

    invoke-static {p2}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/api/android/lib6/c/ar;

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->d:Lcom/google/maps/api/android/lib6/c/ar;

    invoke-static {p3}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/api/android/lib6/c/cd;

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->e:Lcom/google/maps/api/android/lib6/c/cd;

    iput-object p4, p0, Lcom/google/maps/api/android/lib6/c/dx;->p:Lcom/google/maps/api/android/lib6/c/bz;

    const-string v0, "ci%d"

    new-array v3, v1, [Ljava/lang/Object;

    sget-object v4, Lcom/google/maps/api/android/lib6/c/dx;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->c:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/CircleOptions;->d()F

    move-result v0

    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_5

    move v0, v1

    :goto_0
    const-string v3, "line width is negative"

    invoke-static {v0, v3}, Lcom/google/k/a/cj;->a(ZLjava/lang/Object;)V

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/CircleOptions;->b()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    invoke-static {v0}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/CircleOptions;->c()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmpl-double v0, v4, v6

    if-ltz v0, :cond_6

    :goto_1
    const-string v0, "radius is negative"

    invoke-static {v1, v0}, Lcom/google/k/a/cj;->a(ZLjava/lang/Object;)V

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/CircleOptions;->b()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->g:Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/CircleOptions;->c()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->h:D

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/CircleOptions;->d()F

    move-result v0

    iput v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->l:F

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/CircleOptions;->e()I

    move-result v0

    iput v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->j:I

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/CircleOptions;->f()I

    move-result v0

    iput v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->k:I

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/CircleOptions;->g()F

    move-result v0

    iput v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->m:F

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/CircleOptions;->h()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->n:Z

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->g:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, p0, Lcom/google/maps/api/android/lib6/c/dx;->h:D

    invoke-static {v0, v2, v3}, Lcom/google/maps/api/android/lib6/c/ej;->b(Lcom/google/android/gms/maps/model/LatLng;D)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->i:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/CircleOptions;->f()I

    move-result v0

    sget-object v1, Lcom/google/maps/api/android/lib6/c/dx;->b:Lcom/google/android/gms/maps/model/CircleOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/CircleOptions;->f()I

    move-result v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->e:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->V:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/CircleOptions;->e()I

    move-result v0

    sget-object v1, Lcom/google/maps/api/android/lib6/c/dx;->b:Lcom/google/android/gms/maps/model/CircleOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/CircleOptions;->e()I

    move-result v1

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->e:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->U:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/CircleOptions;->d()F

    move-result v0

    sget-object v1, Lcom/google/maps/api/android/lib6/c/dx;->b:Lcom/google/android/gms/maps/model/CircleOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/CircleOptions;->d()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->e:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->T:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/CircleOptions;->h()Z

    move-result v0

    sget-object v1, Lcom/google/maps/api/android/lib6/c/dx;->b:Lcom/google/android/gms/maps/model/CircleOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/CircleOptions;->h()Z

    move-result v1

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->e:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->X:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/CircleOptions;->g()F

    move-result v0

    sget-object v1, Lcom/google/maps/api/android/lib6/c/dx;->b:Lcom/google/android/gms/maps/model/CircleOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/CircleOptions;->g()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->e:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->W:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    :cond_4
    return-void

    :cond_5
    move v0, v2

    goto/16 :goto_0

    :cond_6
    move v1, v2

    goto/16 :goto_1
.end method

.method private c(I)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->o:Z

    if-eqz v0, :cond_1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    return-void

    :cond_1
    monitor-exit p0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->f:Lcom/google/maps/api/android/lib6/c/av;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->f:Lcom/google/maps/api/android/lib6/c/av;

    invoke-interface {v0, p1}, Lcom/google/maps/api/android/lib6/c/av;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->p:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->e:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->Q:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/dx;->l()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->d:Lcom/google/maps/api/android/lib6/c/ar;

    invoke-virtual {v0, p0}, Lcom/google/maps/api/android/lib6/c/ar;->b(Lcom/google/maps/api/android/lib6/c/as;)V

    return-void
.end method

.method public final a(D)V
    .locals 3

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->p:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->e:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->S:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, Lcom/google/maps/api/android/lib6/c/dx;->h:D

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->g:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v0, p1, p2}, Lcom/google/maps/api/android/lib6/c/ej;->b(Lcom/google/android/gms/maps/model/LatLng;D)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->i:Ljava/util/List;

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/c/dx;->c(I)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(F)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->p:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->e:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->T:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "line width is negative"

    invoke-static {v0, v1}, Lcom/google/k/a/cj;->a(ZLjava/lang/Object;)V

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/google/maps/api/android/lib6/c/dx;->l:F

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/c/dx;->c(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->p:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->e:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->U:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/google/maps/api/android/lib6/c/dx;->j:I

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/c/dx;->c(I)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLng;)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->p:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->e:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->R:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/maps/api/android/lib6/c/dx;->g:Lcom/google/android/gms/maps/model/LatLng;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-wide v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->h:D

    invoke-static {p1, v0, v1}, Lcom/google/maps/api/android/lib6/c/ej;->b(Lcom/google/android/gms/maps/model/LatLng;D)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->i:Ljava/util/List;

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/c/dx;->c(I)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final a(Lcom/google/maps/api/android/lib6/c/av;)V
    .locals 0

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/c/dx;->f:Lcom/google/maps/api/android/lib6/c/av;

    return-void
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->p:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->e:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->X:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/google/maps/api/android/lib6/c/dx;->n:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v0, 0x20

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/c/dx;->c(I)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/android/gms/maps/model/internal/g;)Z
    .locals 1

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final b(F)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->p:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->e:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->W:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/google/maps/api/android/lib6/c/dx;->m:F

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v0, 0x40

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/c/dx;->c(I)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->p:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->e:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->V:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/google/maps/api/android/lib6/c/dx;->k:I

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/c/dx;->c(I)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()Lcom/google/android/gms/maps/model/LatLng;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->p:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->g:Lcom/google/android/gms/maps/model/LatLng;

    return-object v0
.end method

.method public final d()D
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->p:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-wide v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->h:D

    return-wide v0
.end method

.method public final e()F
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->p:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/dx;->o()F

    move-result v0

    return v0
.end method

.method public final f()I
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->p:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/dx;->p()I

    move-result v0

    return v0
.end method

.method public final g()I
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->p:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/dx;->q()I

    move-result v0

    return v0
.end method

.method public final h()F
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->p:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/dx;->s()F

    move-result v0

    return v0
.end method

.method public final i()Z
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->p:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/dx;->r()Z

    move-result v0

    return v0
.end method

.method public final j()I
    .locals 1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public final l()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->o:Z

    if-eqz v0, :cond_1

    monitor-exit p0

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->o:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->f:Lcom/google/maps/api/android/lib6/c/av;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->f:Lcom/google/maps/api/android/lib6/c/av;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/av;->a()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized m()Ljava/util/List;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->i:Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized n()Ljava/util/List;
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized o()F
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->l:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized p()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->j:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized q()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->k:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized r()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->n:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized s()F
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/maps/api/android/lib6/c/dx;->m:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
