.class public final Lcom/google/maps/api/android/lib6/d/k;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/maps/api/android/lib6/c/aq;


# instance fields
.field final a:Landroid/content/res/Resources;

.field b:Z

.field c:Landroid/graphics/Paint;

.field d:Landroid/location/Location;

.field e:Landroid/graphics/Bitmap;

.field f:Landroid/graphics/Bitmap;

.field private final g:Lcom/google/maps/api/android/lib6/d/i;


# direct methods
.method public constructor <init>(Lcom/google/maps/api/android/lib6/d/i;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/maps/api/android/lib6/d/k;->b:Z

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/d/k;->c:Landroid/graphics/Paint;

    invoke-static {p1}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/api/android/lib6/d/i;

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/d/k;->g:Lcom/google/maps/api/android/lib6/d/i;

    invoke-virtual {p1}, Lcom/google/maps/api/android/lib6/d/i;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/d/k;->a:Landroid/content/res/Resources;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/d/k;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/k;->g:Lcom/google/maps/api/android/lib6/d/i;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/d/i;->invalidate()V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/maps/api/android/lib6/d/k;->b:Z

    return-void
.end method

.method public final a(Landroid/location/Location;)V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/k;->d:Landroid/location/Location;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/k;->g:Lcom/google/maps/api/android/lib6/d/i;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/d/i;->invalidate()V

    :cond_0
    iput-object p1, p0, Lcom/google/maps/api/android/lib6/d/k;->d:Landroid/location/Location;

    return-void
.end method

.method public final b()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/d/k;->b:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/k;->g:Lcom/google/maps/api/android/lib6/d/i;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/d/i;->invalidate()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/maps/api/android/lib6/d/k;->b:Z

    return-void
.end method
