.class public final Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;
.super Ljava/lang/Object;


# instance fields
.field a:I

.field b:I

.field final c:Z

.field d:Landroid/graphics/Bitmap;

.field e:Landroid/graphics/Canvas;

.field f:I

.field g:F

.field h:F

.field i:I

.field j:I

.field k:I

.field final l:Ljava/util/ArrayList;

.field final m:Z

.field private n:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/16 v0, 0x200

    const/16 v1, 0x80

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;-><init>(IIZ)V

    return-void
.end method

.method public constructor <init>(IIZ)V
    .locals 2

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->l:Ljava/util/ArrayList;

    iput p1, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->a:I

    iput p2, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->b:I

    iget v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->a:I

    int-to-float v0, v0

    div-float v0, v1, v0

    iput v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->g:F

    iget v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->b:I

    int-to-float v0, v0

    div-float v0, v1, v0

    iput v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->h:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->c:Z

    iput-boolean p3, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->m:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->n:I

    return-void
.end method


# virtual methods
.method final a(II)V
    .locals 2

    iget v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->n:I

    if-eq v0, p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Can\'t call this method now."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput p2, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->n:I

    return-void
.end method

.method public final a(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 6

    const/4 v5, 0x0

    const v4, 0x47012f00    # 33071.0f

    const/high16 v3, 0x46180000    # 9728.0f

    const/4 v2, 0x1

    const/16 v1, 0xde1

    iput v2, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->n:I

    new-array v0, v2, [I

    invoke-interface {p1, v2, v0, v5}, Ljavax/microedition/khronos/opengles/GL10;->glGenTextures(I[II)V

    aget v0, v0, v5

    iput v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->f:I

    iget v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->f:I

    invoke-interface {p1, v1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glBindTexture(II)V

    const/16 v0, 0x2801

    invoke-interface {p1, v1, v0, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    const/16 v0, 0x2800

    invoke-interface {p1, v1, v0, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    const/16 v0, 0x2802

    invoke-interface {p1, v1, v0, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    const/16 v0, 0x2803

    invoke-interface {p1, v1, v0, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTexParameterf(IIF)V

    const/16 v0, 0x2300

    const/16 v1, 0x2200

    const v2, 0x45f00800    # 7681.0f

    invoke-interface {p1, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvf(IIF)V

    return-void
.end method

.method public final b(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->n:I

    if-lez v0, :cond_0

    new-array v0, v3, [I

    iget v1, p0, Lcom/google/maps/api/android/lib6/gmm6/streetview/ai;->f:I

    aput v1, v0, v2

    invoke-interface {p1, v3, v0, v2}, Ljavax/microedition/khronos/opengles/GL10;->glDeleteTextures(I[II)V

    :cond_0
    return-void
.end method
