.class final Lcom/google/maps/api/android/lib6/c/ch;
.super Ljava/lang/Object;


# instance fields
.field public final a:Lcom/google/maps/api/android/lib6/c/cf;

.field public final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/maps/api/android/lib6/c/cf;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/c/ch;->a:Lcom/google/maps/api/android/lib6/c/cf;

    iput-object p2, p0, Lcom/google/maps/api/android/lib6/c/ch;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    instance-of v0, p1, Lcom/google/maps/api/android/lib6/c/ch;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/maps/api/android/lib6/c/ch;

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/c/ch;->a:Lcom/google/maps/api/android/lib6/c/cf;

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/c/cf;->ca:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/ch;->a:Lcom/google/maps/api/android/lib6/c/cf;

    iget-object v1, v1, Lcom/google/maps/api/android/lib6/c/cf;->ca:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/maps/api/android/lib6/c/ch;

    iget-object v0, p1, Lcom/google/maps/api/android/lib6/c/ch;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/ch;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/ch;->a:Lcom/google/maps/api/android/lib6/c/cf;

    iget-object v1, v1, Lcom/google/maps/api/android/lib6/c/cf;->ca:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/ch;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
