.class public final Lcom/google/maps/api/android/lib6/c/bo;
.super Lcom/google/android/gms/maps/internal/cg;

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static j:Lcom/google/android/gms/maps/model/StreetViewPanoramaCamera;


# instance fields
.field private final a:Lcom/google/maps/api/android/lib6/c/bt;

.field private final b:Lcom/google/maps/api/android/lib6/c/bz;

.field private final c:Lcom/google/android/gms/maps/StreetViewPanoramaOptions;

.field private final d:Landroid/widget/FrameLayout;

.field private final e:Lcom/google/maps/api/android/lib6/c/bs;

.field private final f:Landroid/content/Context;

.field private final g:Lcom/google/maps/api/android/lib6/c/cd;

.field private h:Lcom/google/maps/api/android/lib6/c/br;

.field private final i:Lcom/google/maps/api/android/lib6/c/bw;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Lcom/google/android/gms/maps/model/StreetViewPanoramaCamera;

    invoke-direct {v0, v1, v1, v1}, Lcom/google/android/gms/maps/model/StreetViewPanoramaCamera;-><init>(FFF)V

    sput-object v0, Lcom/google/maps/api/android/lib6/c/bo;->j:Lcom/google/android/gms/maps/model/StreetViewPanoramaCamera;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/maps/api/android/lib6/c/bt;Lcom/google/android/gms/maps/StreetViewPanoramaOptions;Lcom/google/maps/api/android/lib6/c/bz;Lcom/google/maps/api/android/lib6/c/bs;Landroid/widget/FrameLayout;Lcom/google/maps/api/android/lib6/c/cd;Lcom/google/maps/api/android/lib6/c/bw;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/gms/maps/internal/cg;-><init>()V

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/c/bo;->f:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/maps/api/android/lib6/c/bo;->a:Lcom/google/maps/api/android/lib6/c/bt;

    iput-object p3, p0, Lcom/google/maps/api/android/lib6/c/bo;->c:Lcom/google/android/gms/maps/StreetViewPanoramaOptions;

    iput-object p4, p0, Lcom/google/maps/api/android/lib6/c/bo;->b:Lcom/google/maps/api/android/lib6/c/bz;

    iput-object p5, p0, Lcom/google/maps/api/android/lib6/c/bo;->e:Lcom/google/maps/api/android/lib6/c/bs;

    iput-object p6, p0, Lcom/google/maps/api/android/lib6/c/bo;->d:Landroid/widget/FrameLayout;

    iput-object p7, p0, Lcom/google/maps/api/android/lib6/c/bo;->g:Lcom/google/maps/api/android/lib6/c/cd;

    iput-object p8, p0, Lcom/google/maps/api/android/lib6/c/bo;->i:Lcom/google/maps/api/android/lib6/c/bw;

    new-instance v0, Lcom/google/maps/api/android/lib6/c/bp;

    invoke-direct {v0, p0}, Lcom/google/maps/api/android/lib6/c/bp;-><init>(Lcom/google/maps/api/android/lib6/c/bo;)V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->h:Lcom/google/maps/api/android/lib6/c/br;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->a:Lcom/google/maps/api/android/lib6/c/bt;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/bo;->h:Lcom/google/maps/api/android/lib6/c/br;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/bt;->a(Lcom/google/maps/api/android/lib6/c/br;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->e:Lcom/google/maps/api/android/lib6/c/bs;

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/c/bs;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->c:Lcom/google/android/gms/maps/StreetViewPanoramaOptions;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/StreetViewPanoramaOptions;->k()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->c:Lcom/google/android/gms/maps/StreetViewPanoramaOptions;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/StreetViewPanoramaOptions;->k()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/c/bo;->c(Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->c:Lcom/google/android/gms/maps/StreetViewPanoramaOptions;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/StreetViewPanoramaOptions;->l()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->c:Lcom/google/android/gms/maps/StreetViewPanoramaOptions;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/StreetViewPanoramaOptions;->l()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/c/bo;->a(Z)V

    :cond_1
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->c:Lcom/google/android/gms/maps/StreetViewPanoramaOptions;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/StreetViewPanoramaOptions;->m()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->c:Lcom/google/android/gms/maps/StreetViewPanoramaOptions;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/StreetViewPanoramaOptions;->m()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/c/bo;->b(Z)V

    :cond_2
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->c:Lcom/google/android/gms/maps/StreetViewPanoramaOptions;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/StreetViewPanoramaOptions;->n()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->c:Lcom/google/android/gms/maps/StreetViewPanoramaOptions;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/StreetViewPanoramaOptions;->n()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/c/bo;->d(Z)V

    :cond_3
    return-void
.end method

.method public static a(Landroid/view/LayoutInflater;Lcom/google/android/gms/maps/StreetViewPanoramaOptions;)Lcom/google/maps/api/android/lib6/c/bo;
    .locals 10

    invoke-virtual {p0}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/maps/api/android/lib6/c/dz;->a(Landroid/content/Context;)Lcom/google/maps/api/android/lib6/c/dz;

    move-result-object v2

    const/4 v1, 0x0

    invoke-static {v0, v2, v1}, Lcom/google/maps/api/android/lib6/c/i;->a(Landroid/content/Context;Lcom/google/maps/api/android/lib6/c/dz;Z)Landroid/content/Context;

    move-result-object v1

    new-instance v6, Landroid/widget/FrameLayout;

    invoke-direct {v6, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    invoke-static {v1, v2}, Lcom/google/maps/api/android/lib6/gmm6/streetview/f;->a(Landroid/content/Context;Lcom/google/maps/api/android/lib6/c/dz;)Lcom/google/maps/api/android/lib6/gmm6/streetview/f;

    move-result-object v2

    invoke-static {}, Lcom/google/maps/api/android/lib6/c/m;->a()Landroid/content/res/Resources;

    move-result-object v0

    new-instance v5, Lcom/google/maps/api/android/lib6/c/bs;

    invoke-direct {v5, v1, v0}, Lcom/google/maps/api/android/lib6/c/bs;-><init>(Landroid/content/Context;Landroid/content/res/Resources;)V

    invoke-static {}, Lcom/google/maps/api/android/lib6/c/ca;->b()Lcom/google/maps/api/android/lib6/c/bz;

    move-result-object v4

    invoke-static {}, Lcom/google/maps/api/android/lib6/c/cg;->b()Lcom/google/maps/api/android/lib6/c/cd;

    move-result-object v7

    invoke-static {p1}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/gms/maps/StreetViewPanoramaOptions;->g()Lcom/google/android/gms/maps/model/StreetViewPanoramaCamera;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/maps/api/android/lib6/c/bo;->j:Lcom/google/android/gms/maps/model/StreetViewPanoramaCamera;

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/maps/StreetViewPanoramaOptions;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/gms/maps/StreetViewPanoramaOptions;->h()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v8

    invoke-virtual {p1}, Lcom/google/android/gms/maps/StreetViewPanoramaOptions;->i()Ljava/lang/Integer;

    move-result-object v9

    invoke-static {v0}, Lcom/google/maps/api/android/lib6/gmm6/c/f;->a(Lcom/google/android/gms/maps/model/StreetViewPanoramaCamera;)Lcom/google/maps/api/android/lib6/c/bx;

    move-result-object v0

    invoke-interface {v2, v3, v8, v9, v0}, Lcom/google/maps/api/android/lib6/c/bt;->a(Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLng;Ljava/lang/Integer;Lcom/google/maps/api/android/lib6/c/bx;)V

    invoke-static {v1}, Lcom/google/maps/api/android/lib6/c/bb;->a(Landroid/content/Context;)Lcom/google/maps/api/android/lib6/c/bb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bb;->a()V

    new-instance v8, Lcom/google/maps/api/android/lib6/c/bw;

    invoke-direct {v8, v1}, Lcom/google/maps/api/android/lib6/c/bw;-><init>(Landroid/content/Context;)V

    invoke-interface {v2}, Lcom/google/maps/api/android/lib6/c/bt;->l()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    iget-object v0, v5, Lcom/google/maps/api/android/lib6/c/bs;->a:Landroid/widget/RelativeLayout;

    invoke-virtual {v6, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    sget-object v0, Lcom/google/maps/api/android/lib6/c/cf;->bG:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v7, v0}, Lcom/google/maps/api/android/lib6/c/cd;->a(Lcom/google/maps/api/android/lib6/c/cf;)V

    new-instance v0, Lcom/google/maps/api/android/lib6/c/bo;

    move-object v3, p1

    invoke-direct/range {v0 .. v8}, Lcom/google/maps/api/android/lib6/c/bo;-><init>(Landroid/content/Context;Lcom/google/maps/api/android/lib6/c/bt;Lcom/google/android/gms/maps/StreetViewPanoramaOptions;Lcom/google/maps/api/android/lib6/c/bz;Lcom/google/maps/api/android/lib6/c/bs;Landroid/widget/FrameLayout;Lcom/google/maps/api/android/lib6/c/cd;Lcom/google/maps/api/android/lib6/c/bw;)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/maps/api/android/lib6/c/bo;)Lcom/google/maps/api/android/lib6/c/bs;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->e:Lcom/google/maps/api/android/lib6/c/bs;

    return-object v0
.end method

.method static synthetic b(Lcom/google/maps/api/android/lib6/c/bo;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->f:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic c(Lcom/google/maps/api/android/lib6/c/bo;)Lcom/google/maps/api/android/lib6/c/bt;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->a:Lcom/google/maps/api/android/lib6/c/bt;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/maps/model/StreetViewPanoramaOrientation;)Lcom/google/android/gms/b/l;
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->b:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->g:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->bT:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->a:Lcom/google/maps/api/android/lib6/c/bt;

    invoke-interface {v0, p1}, Lcom/google/maps/api/android/lib6/c/bt;->a(Lcom/google/android/gms/maps/model/StreetViewPanoramaOrientation;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/b/l;)Lcom/google/android/gms/maps/model/StreetViewPanoramaOrientation;
    .locals 3

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->b:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->g:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->bS:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    invoke-static {p1}, Lcom/google/android/gms/b/p;->a(Lcom/google/android/gms/b/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Point;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/bo;->a:Lcom/google/maps/api/android/lib6/c/bt;

    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-interface {v1, v2, v0}, Lcom/google/maps/api/android/lib6/c/bt;->a(II)Lcom/google/android/gms/maps/model/StreetViewPanoramaOrientation;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    const-string v0, "camera"

    invoke-static {p1, v0}, Lcom/google/android/gms/maps/internal/cs;->a(Landroid/os/Bundle;Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/StreetViewPanoramaCamera;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->c:Lcom/google/android/gms/maps/StreetViewPanoramaOptions;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/StreetViewPanoramaOptions;->g()Lcom/google/android/gms/maps/model/StreetViewPanoramaCamera;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->c:Lcom/google/android/gms/maps/StreetViewPanoramaOptions;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/StreetViewPanoramaOptions;->g()Lcom/google/android/gms/maps/model/StreetViewPanoramaCamera;

    move-result-object v0

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/google/maps/api/android/lib6/c/bo;->a:Lcom/google/maps/api/android/lib6/c/bt;

    if-eqz p1, :cond_2

    const-string v1, "position"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "position"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-interface {v2, v0, v1}, Lcom/google/maps/api/android/lib6/c/bt;->a(Lcom/google/android/gms/maps/model/StreetViewPanoramaCamera;Ljava/lang/String;)V

    return-void

    :cond_1
    sget-object v0, Lcom/google/maps/api/android/lib6/c/bo;->j:Lcom/google/android/gms/maps/model/StreetViewPanoramaCamera;

    goto :goto_0

    :cond_2
    const-string v1, ""

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/maps/internal/bn;)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->b:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->g:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->bQ:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->a:Lcom/google/maps/api/android/lib6/c/bt;

    invoke-interface {v0, p1}, Lcom/google/maps/api/android/lib6/c/bt;->a(Lcom/google/android/gms/maps/internal/bn;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/internal/bq;)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->b:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->g:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->bP:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->a:Lcom/google/maps/api/android/lib6/c/bt;

    invoke-interface {v0, p1}, Lcom/google/maps/api/android/lib6/c/bt;->a(Lcom/google/android/gms/maps/internal/bq;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/internal/bt;)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->b:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->g:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->bR:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->a:Lcom/google/maps/api/android/lib6/c/bt;

    invoke-interface {v0, p1}, Lcom/google/maps/api/android/lib6/c/bt;->a(Lcom/google/android/gms/maps/internal/bt;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLng;)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->b:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->g:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->bN:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->a:Lcom/google/maps/api/android/lib6/c/bt;

    invoke-interface {v0, p1}, Lcom/google/maps/api/android/lib6/c/bt;->a(Lcom/google/android/gms/maps/model/LatLng;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLng;I)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->b:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->g:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->bO:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->a:Lcom/google/maps/api/android/lib6/c/bt;

    invoke-interface {v0, p1, p2}, Lcom/google/maps/api/android/lib6/c/bt;->a(Lcom/google/android/gms/maps/model/LatLng;I)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/model/StreetViewPanoramaCamera;J)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->b:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->g:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->bL:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->a:Lcom/google/maps/api/android/lib6/c/bt;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/maps/api/android/lib6/c/bt;->a(Lcom/google/android/gms/maps/model/StreetViewPanoramaCamera;J)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->b:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->g:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->bM:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->a:Lcom/google/maps/api/android/lib6/c/bt;

    invoke-interface {v0, p1}, Lcom/google/maps/api/android/lib6/c/bt;->a(Ljava/lang/String;)V

    return-void
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->b:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->g:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->bH:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->a:Lcom/google/maps/api/android/lib6/c/bt;

    invoke-interface {v0, p1}, Lcom/google/maps/api/android/lib6/c/bt;->a(Z)V

    return-void
.end method

.method public final a()Z
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->b:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->a:Lcom/google/maps/api/android/lib6/c/bt;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/bt;->h()Z

    move-result v0

    return v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "StreetViewPanoramaOptions"

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/bo;->c:Lcom/google/android/gms/maps/StreetViewPanoramaOptions;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/maps/internal/cs;->a(Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "camera"

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/bo;->e()Lcom/google/android/gms/maps/model/StreetViewPanoramaCamera;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/maps/internal/cs;->a(Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->a:Lcom/google/maps/api/android/lib6/c/bt;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/bt;->e()Lcom/google/android/gms/maps/model/StreetViewPanoramaLocation;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "position"

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/bo;->a:Lcom/google/maps/api/android/lib6/c/bt;

    invoke-interface {v1}, Lcom/google/maps/api/android/lib6/c/bt;->e()Lcom/google/android/gms/maps/model/StreetViewPanoramaLocation;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/maps/model/StreetViewPanoramaLocation;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method final b(Lcom/google/android/gms/maps/model/LatLng;)V
    .locals 2

    new-instance v0, Lcom/google/maps/api/android/lib6/c/bf;

    const/high16 v1, 0x41a80000    # 21.0f

    invoke-direct {v0, p1, v1}, Lcom/google/maps/api/android/lib6/c/bf;-><init>(Lcom/google/android/gms/maps/model/LatLng;F)V

    new-instance v1, Lcom/google/maps/api/android/lib6/c/bq;

    invoke-direct {v1, p0}, Lcom/google/maps/api/android/lib6/c/bq;-><init>(Lcom/google/maps/api/android/lib6/c/bo;)V

    invoke-virtual {v0, v1}, Lcom/google/maps/api/android/lib6/c/bf;->a(Lcom/google/maps/api/android/lib6/c/bg;)V

    invoke-static {}, Lcom/google/maps/api/android/lib6/b/h;->a()Lcom/google/maps/api/android/lib6/b/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/maps/api/android/lib6/b/h;->c(Lcom/google/maps/api/android/lib6/b/g;)V

    return-void
.end method

.method public final b(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->b:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->g:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->bI:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->a:Lcom/google/maps/api/android/lib6/c/bt;

    invoke-interface {v0, p1}, Lcom/google/maps/api/android/lib6/c/bt;->b(Z)V

    return-void
.end method

.method public final b()Z
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->b:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->a:Lcom/google/maps/api/android/lib6/c/bt;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/bt;->i()Z

    move-result v0

    return v0
.end method

.method public final c(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->b:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->g:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->bJ:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->a:Lcom/google/maps/api/android/lib6/c/bt;

    invoke-interface {v0, p1}, Lcom/google/maps/api/android/lib6/c/bt;->c(Z)V

    return-void
.end method

.method public final c()Z
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->b:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->a:Lcom/google/maps/api/android/lib6/c/bt;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/bt;->j()Z

    move-result v0

    return v0
.end method

.method public final d(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->b:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->g:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->bK:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->a:Lcom/google/maps/api/android/lib6/c/bt;

    invoke-interface {v0, p1}, Lcom/google/maps/api/android/lib6/c/bt;->d(Z)V

    return-void
.end method

.method public final d()Z
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->b:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->a:Lcom/google/maps/api/android/lib6/c/bt;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/bt;->k()Z

    move-result v0

    return v0
.end method

.method public final e()Lcom/google/android/gms/maps/model/StreetViewPanoramaCamera;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->b:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->a:Lcom/google/maps/api/android/lib6/c/bt;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/bt;->f()Lcom/google/android/gms/maps/model/StreetViewPanoramaCamera;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lcom/google/android/gms/maps/model/StreetViewPanoramaLocation;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->b:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->a:Lcom/google/maps/api/android/lib6/c/bt;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/bt;->e()Lcom/google/android/gms/maps/model/StreetViewPanoramaLocation;

    move-result-object v0

    return-object v0
.end method

.method public final g()V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->a:Lcom/google/maps/api/android/lib6/c/bt;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/bt;->c()V

    return-void
.end method

.method public final h()V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->a:Lcom/google/maps/api/android/lib6/c/bt;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/bt;->b()V

    return-void
.end method

.method public final i()V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->g:Lcom/google/maps/api/android/lib6/c/cd;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/cd;->a()V

    return-void
.end method

.method public final j()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->d:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method public final k()Z
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->c:Lcom/google/android/gms/maps/StreetViewPanoramaOptions;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/StreetViewPanoramaOptions;->o()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->c:Lcom/google/android/gms/maps/StreetViewPanoramaOptions;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/StreetViewPanoramaOptions;->o()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->e:Lcom/google/maps/api/android/lib6/c/bs;

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/c/bs;->b:Landroid/widget/TextView;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bo;->i:Lcom/google/maps/api/android/lib6/c/bw;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/bo;->a:Lcom/google/maps/api/android/lib6/c/bt;

    invoke-interface {v1}, Lcom/google/maps/api/android/lib6/c/bt;->e()Lcom/google/android/gms/maps/model/StreetViewPanoramaLocation;

    move-result-object v1

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/c/bo;->a:Lcom/google/maps/api/android/lib6/c/bt;

    invoke-interface {v2}, Lcom/google/maps/api/android/lib6/c/bt;->f()Lcom/google/android/gms/maps/model/StreetViewPanoramaCamera;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/maps/api/android/lib6/c/bw;->a(Lcom/google/android/gms/maps/model/StreetViewPanoramaLocation;Lcom/google/android/gms/maps/model/StreetViewPanoramaCamera;)V

    :cond_0
    return-void
.end method
