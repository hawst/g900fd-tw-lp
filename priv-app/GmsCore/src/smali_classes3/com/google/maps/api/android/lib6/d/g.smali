.class public final Lcom/google/maps/api/android/lib6/d/g;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/maps/api/android/lib6/c/dj;


# instance fields
.field private final b:Lcom/google/maps/api/android/lib6/d/i;

.field private final c:D

.field private d:Lcom/google/android/gms/maps/model/CameraPosition;

.field private e:Lcom/google/android/gms/maps/internal/ad;

.field private f:Ljava/util/Collection;

.field private g:I

.field private h:I

.field private i:I

.field private j:I


# direct methods
.method public constructor <init>(Lcom/google/maps/api/android/lib6/d/i;Landroid/content/res/Resources;)V
    .locals 5

    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/gms/maps/model/CameraPosition;

    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    invoke-direct {v1, v2, v3, v2, v3}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    const/high16 v2, 0x40400000    # 3.0f

    invoke-direct {v0, v1, v2, v4, v4}, Lcom/google/android/gms/maps/model/CameraPosition;-><init>(Lcom/google/android/gms/maps/model/LatLng;FFF)V

    invoke-static {v0}, Lcom/google/maps/api/android/lib6/d/g;->b(Lcom/google/android/gms/maps/model/CameraPosition;)Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/d/g;->d:Lcom/google/android/gms/maps/model/CameraPosition;

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/d/g;->b:Lcom/google/maps/api/android/lib6/d/i;

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    invoke-virtual {p2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/maps/api/android/lib6/d/g;->c:D

    return-void
.end method

.method private a(Lcom/google/android/gms/maps/model/CameraPosition;)V
    .locals 3

    invoke-static {p1}, Lcom/google/maps/api/android/lib6/d/g;->b(Lcom/google/android/gms/maps/model/CameraPosition;)Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/d/g;->d:Lcom/google/android/gms/maps/model/CameraPosition;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/g;->b:Lcom/google/maps/api/android/lib6/d/i;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/d/i;->d()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/g;->f:Ljava/util/Collection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/g;->f:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/internal/ad;

    :try_start_0
    iget-object v2, p0, Lcom/google/maps/api/android/lib6/d/g;->d:Lcom/google/android/gms/maps/model/CameraPosition;

    invoke-interface {v0, v2}, Lcom/google/android/gms/maps/internal/ad;->a(Lcom/google/android/gms/maps/model/CameraPosition;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1

    :cond_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/g;->e:Lcom/google/android/gms/maps/internal/ad;

    if-eqz v0, :cond_1

    :try_start_1
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/g;->e:Lcom/google/android/gms/maps/internal/ad;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/d/g;->d:Lcom/google/android/gms/maps/model/CameraPosition;

    invoke-interface {v0, v1}, Lcom/google/android/gms/maps/internal/ad;->a(Lcom/google/android/gms/maps/model/CameraPosition;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    return-void

    :catch_1
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method private static b(Lcom/google/android/gms/maps/model/CameraPosition;)Lcom/google/android/gms/maps/model/CameraPosition;
    .locals 4

    const/4 v2, 0x5

    const/4 v3, 0x0

    iget v0, p0, Lcom/google/android/gms/maps/model/CameraPosition;->c:F

    cmpl-float v0, v0, v3

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/maps/model/CameraPosition;->d:F

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "Non zero bearing and tilt are not supported in Lite Mode"

    invoke-static {v2, v0}, Lcom/google/maps/api/android/lib6/c/by;->a(ILjava/lang/String;)V

    :cond_1
    iget v0, p0, Lcom/google/android/gms/maps/model/CameraPosition;->b:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Lcom/google/k/g/a;->a(D)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "Non integer zooms are not supported in Lite Mode"

    invoke-static {v2, v0}, Lcom/google/maps/api/android/lib6/c/by;->a(ILjava/lang/String;)V

    :cond_2
    const/high16 v0, 0x41b00000    # 22.0f

    iget v1, p0, Lcom/google/android/gms/maps/model/CameraPosition;->b:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    new-instance v1, Lcom/google/android/gms/maps/model/CameraPosition;

    iget-object v2, p0, Lcom/google/android/gms/maps/model/CameraPosition;->a:Lcom/google/android/gms/maps/model/LatLng;

    int-to-float v0, v0

    invoke-direct {v1, v2, v0, v3, v3}, Lcom/google/android/gms/maps/model/CameraPosition;-><init>(Lcom/google/android/gms/maps/model/LatLng;FFF)V

    return-object v1
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/maps/model/LatLng;)F
    .locals 1

    const/high16 v0, 0x41b00000    # 22.0f

    return v0
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLngBounds;)Lcom/google/android/gms/maps/model/CameraPosition;
    .locals 14

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/g;->b:Lcom/google/maps/api/android/lib6/d/i;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/d/i;->getWidth()I

    move-result v6

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/g;->b:Lcom/google/maps/api/android/lib6/d/i;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/d/i;->getHeight()I

    move-result v7

    iget-wide v2, p0, Lcom/google/maps/api/android/lib6/d/g;->c:D

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/LatLngBounds;->b()Lcom/google/android/gms/maps/model/LatLng;

    const-wide/high16 v4, 0x4036000000000000L    # 22.0

    iget-object v0, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->a:Lcom/google/android/gms/maps/model/LatLng;

    const-wide/high16 v8, 0x4036000000000000L    # 22.0

    invoke-static {v0, v8, v9, v2, v3}, Lcom/google/maps/api/android/lib6/d/p;->a(Lcom/google/android/gms/maps/model/LatLng;DD)Lcom/google/maps/api/android/lib6/d/q;

    move-result-object v8

    iget-object v0, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->b:Lcom/google/android/gms/maps/model/LatLng;

    const-wide/high16 v10, 0x4036000000000000L    # 22.0

    invoke-static {v0, v10, v11, v2, v3}, Lcom/google/maps/api/android/lib6/d/p;->a(Lcom/google/android/gms/maps/model/LatLng;DD)Lcom/google/maps/api/android/lib6/d/q;

    move-result-object v1

    iget-object v0, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v10, v0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    iget-object v0, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->b:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v12, v0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    cmpl-double v0, v10, v12

    if-lez v0, :cond_2

    new-instance v0, Lcom/google/maps/api/android/lib6/d/q;

    iget-wide v10, v1, Lcom/google/maps/api/android/lib6/d/q;->a:J

    const-wide/high16 v12, 0x4036000000000000L    # 22.0

    invoke-static {v12, v13, v2, v3}, Lcom/google/maps/api/android/lib6/d/p;->a(DD)D

    move-result-wide v2

    double-to-int v2, v2

    int-to-long v2, v2

    add-long/2addr v2, v10

    iget-wide v10, v1, Lcom/google/maps/api/android/lib6/d/q;->b:J

    invoke-direct {v0, v2, v3, v10, v11}, Lcom/google/maps/api/android/lib6/d/q;-><init>(JJ)V

    :goto_0
    iget-wide v2, v0, Lcom/google/maps/api/android/lib6/d/q;->a:J

    iget-wide v10, v8, Lcom/google/maps/api/android/lib6/d/q;->a:J

    sub-long/2addr v2, v10

    iget-wide v8, v8, Lcom/google/maps/api/android/lib6/d/q;->b:J

    iget-wide v0, v0, Lcom/google/maps/api/android/lib6/d/q;->b:J

    sub-long v0, v8, v0

    :goto_1
    int-to-long v8, v6

    cmp-long v8, v2, v8

    if-gtz v8, :cond_0

    int-to-long v8, v7

    cmp-long v8, v0, v8

    if-lez v8, :cond_1

    :cond_0
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v4, v8

    const/4 v8, 0x1

    shr-long/2addr v2, v8

    const/4 v8, 0x1

    shr-long/2addr v0, v8

    goto :goto_1

    :cond_1
    new-instance v0, Lcom/google/android/gms/maps/model/CameraPosition;

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/LatLngBounds;->b()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    double-to-float v2, v4

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/maps/model/CameraPosition;-><init>(Lcom/google/android/gms/maps/model/LatLng;FFF)V

    return-object v0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public final a()V
    .locals 0

    return-void
.end method

.method public final a(F)V
    .locals 2

    const/4 v0, 0x5

    const-string v1, "zoomByCumulative is not supported in Lite Mode"

    invoke-static {v0, v1}, Lcom/google/maps/api/android/lib6/c/by;->a(ILjava/lang/String;)V

    return-void
.end method

.method public final a(FFI)V
    .locals 2

    const/4 v0, 0x5

    const-string v1, "scrollBy is not supported in Lite Mode"

    invoke-static {v0, v1}, Lcom/google/maps/api/android/lib6/c/by;->a(ILjava/lang/String;)V

    return-void
.end method

.method public final a(FI)V
    .locals 5

    new-instance v0, Lcom/google/android/gms/maps/model/CameraPosition;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/d/g;->d:Lcom/google/android/gms/maps/model/CameraPosition;

    iget-object v1, v1, Lcom/google/android/gms/maps/model/CameraPosition;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/d/g;->d:Lcom/google/android/gms/maps/model/CameraPosition;

    iget v2, v2, Lcom/google/android/gms/maps/model/CameraPosition;->b:F

    add-float/2addr v2, p1

    iget-object v3, p0, Lcom/google/maps/api/android/lib6/d/g;->d:Lcom/google/android/gms/maps/model/CameraPosition;

    iget v3, v3, Lcom/google/android/gms/maps/model/CameraPosition;->c:F

    iget-object v4, p0, Lcom/google/maps/api/android/lib6/d/g;->d:Lcom/google/android/gms/maps/model/CameraPosition;

    iget v4, v4, Lcom/google/android/gms/maps/model/CameraPosition;->d:F

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/maps/model/CameraPosition;-><init>(Lcom/google/android/gms/maps/model/LatLng;FFF)V

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/d/g;->a(Lcom/google/android/gms/maps/model/CameraPosition;)V

    return-void
.end method

.method public final a(FIII)V
    .locals 2

    const/4 v0, 0x5

    const-string v1, "zoomBy with focus is not supported in Lite Mode"

    invoke-static {v0, v1}, Lcom/google/maps/api/android/lib6/c/by;->a(ILjava/lang/String;)V

    return-void
.end method

.method public final a(IIII)V
    .locals 0

    iput p1, p0, Lcom/google/maps/api/android/lib6/d/g;->g:I

    iput p2, p0, Lcom/google/maps/api/android/lib6/d/g;->h:I

    iput p3, p0, Lcom/google/maps/api/android/lib6/d/g;->i:I

    iput p4, p0, Lcom/google/maps/api/android/lib6/d/g;->j:I

    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/internal/ad;)V
    .locals 0

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/d/g;->e:Lcom/google/android/gms/maps/internal/ad;

    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/model/CameraPosition;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/maps/api/android/lib6/d/g;->a(Lcom/google/android/gms/maps/model/CameraPosition;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLng;FI)V
    .locals 3

    new-instance v0, Lcom/google/android/gms/maps/model/CameraPosition;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/d/g;->d:Lcom/google/android/gms/maps/model/CameraPosition;

    iget v1, v1, Lcom/google/android/gms/maps/model/CameraPosition;->c:F

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/d/g;->d:Lcom/google/android/gms/maps/model/CameraPosition;

    iget v2, v2, Lcom/google/android/gms/maps/model/CameraPosition;->d:F

    invoke-direct {v0, p1, p2, v1, v2}, Lcom/google/android/gms/maps/model/CameraPosition;-><init>(Lcom/google/android/gms/maps/model/LatLng;FFF)V

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/d/g;->a(Lcom/google/android/gms/maps/model/CameraPosition;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLng;I)V
    .locals 4

    new-instance v0, Lcom/google/android/gms/maps/model/CameraPosition;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/d/g;->d:Lcom/google/android/gms/maps/model/CameraPosition;

    iget v1, v1, Lcom/google/android/gms/maps/model/CameraPosition;->b:F

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/d/g;->d:Lcom/google/android/gms/maps/model/CameraPosition;

    iget v2, v2, Lcom/google/android/gms/maps/model/CameraPosition;->c:F

    iget-object v3, p0, Lcom/google/maps/api/android/lib6/d/g;->d:Lcom/google/android/gms/maps/model/CameraPosition;

    iget v3, v3, Lcom/google/android/gms/maps/model/CameraPosition;->d:F

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/google/android/gms/maps/model/CameraPosition;-><init>(Lcom/google/android/gms/maps/model/LatLng;FFF)V

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/d/g;->a(Lcom/google/android/gms/maps/model/CameraPosition;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLngBounds;II)V
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/maps/api/android/lib6/d/g;->a(Lcom/google/android/gms/maps/model/LatLngBounds;)Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/d/g;->a(Lcom/google/android/gms/maps/model/CameraPosition;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLngBounds;IIII)V
    .locals 2

    const/4 v0, 0x5

    const-string v1, "newLatLngBounds with size is not supported in Lite Mode"

    invoke-static {v0, v1}, Lcom/google/maps/api/android/lib6/c/by;->a(ILjava/lang/String;)V

    return-void
.end method

.method public final a(Lcom/google/maps/api/android/lib6/c/dk;ILcom/google/android/gms/maps/internal/e;Lcom/google/maps/api/android/lib6/c/cd;)V
    .locals 3

    const/4 v1, 0x1

    if-nez p2, :cond_0

    if-nez p3, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    const-string v2, "Callback supplied with instantaneous camera movement"

    invoke-static {v0, v2}, Lcom/google/k/a/cj;->a(ZLjava/lang/Object;)V

    const-string v0, "Camera moved during a cancellation"

    invoke-static {v1, v0}, Lcom/google/k/a/cj;->b(ZLjava/lang/Object;)V

    invoke-interface {p1, p0, p2, p4}, Lcom/google/maps/api/android/lib6/c/dk;->a(Lcom/google/maps/api/android/lib6/c/dj;ILcom/google/maps/api/android/lib6/c/cd;)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lcom/google/maps/api/android/lib6/d/p;
    .locals 10

    new-instance v0, Lcom/google/maps/api/android/lib6/d/p;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/d/g;->d:Lcom/google/android/gms/maps/model/CameraPosition;

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/d/g;->b:Lcom/google/maps/api/android/lib6/d/i;

    invoke-virtual {v2}, Lcom/google/maps/api/android/lib6/d/i;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/google/maps/api/android/lib6/d/g;->b:Lcom/google/maps/api/android/lib6/d/i;

    invoke-virtual {v3}, Lcom/google/maps/api/android/lib6/d/i;->getHeight()I

    move-result v3

    iget-wide v4, p0, Lcom/google/maps/api/android/lib6/d/g;->c:D

    iget v6, p0, Lcom/google/maps/api/android/lib6/d/g;->g:I

    iget v7, p0, Lcom/google/maps/api/android/lib6/d/g;->h:I

    iget v8, p0, Lcom/google/maps/api/android/lib6/d/g;->i:I

    iget v9, p0, Lcom/google/maps/api/android/lib6/d/g;->j:I

    invoke-direct/range {v0 .. v9}, Lcom/google/maps/api/android/lib6/d/p;-><init>(Lcom/google/android/gms/maps/model/CameraPosition;IIDIIII)V

    return-object v0
.end method

.method public final b(FI)V
    .locals 4

    new-instance v0, Lcom/google/android/gms/maps/model/CameraPosition;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/d/g;->d:Lcom/google/android/gms/maps/model/CameraPosition;

    iget-object v1, v1, Lcom/google/android/gms/maps/model/CameraPosition;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/d/g;->d:Lcom/google/android/gms/maps/model/CameraPosition;

    iget v2, v2, Lcom/google/android/gms/maps/model/CameraPosition;->c:F

    iget-object v3, p0, Lcom/google/maps/api/android/lib6/d/g;->d:Lcom/google/android/gms/maps/model/CameraPosition;

    iget v3, v3, Lcom/google/android/gms/maps/model/CameraPosition;->d:F

    invoke-direct {v0, v1, p1, v2, v3}, Lcom/google/android/gms/maps/model/CameraPosition;-><init>(Lcom/google/android/gms/maps/model/LatLng;FFF)V

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/d/g;->a(Lcom/google/android/gms/maps/model/CameraPosition;)V

    return-void
.end method

.method public final b(Lcom/google/android/gms/maps/internal/ad;)V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/g;->f:Ljava/util/Collection;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/d/g;->f:Ljava/util/Collection;

    :cond_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/g;->f:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final c()Lcom/google/android/gms/maps/model/CameraPosition;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/g;->d:Lcom/google/android/gms/maps/model/CameraPosition;

    return-object v0
.end method

.method public final c(Lcom/google/android/gms/maps/internal/ad;)V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/g;->f:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/g;->f:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/d/g;->f:Ljava/util/Collection;

    :cond_0
    return-void
.end method

.method public final d()F
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
