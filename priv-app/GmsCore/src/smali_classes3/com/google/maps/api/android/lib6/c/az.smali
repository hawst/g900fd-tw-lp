.class public final Lcom/google/maps/api/android/lib6/c/az;
.super Lcom/google/android/gms/maps/internal/ca;


# instance fields
.field private final a:Lcom/google/maps/api/android/lib6/c/cd;

.field private final b:Lcom/google/maps/api/android/lib6/c/ba;


# direct methods
.method public constructor <init>(Lcom/google/maps/api/android/lib6/c/cd;Lcom/google/maps/api/android/lib6/c/ba;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/maps/internal/ca;-><init>()V

    iput-object p2, p0, Lcom/google/maps/api/android/lib6/c/az;->b:Lcom/google/maps/api/android/lib6/c/ba;

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/c/az;->a:Lcom/google/maps/api/android/lib6/c/cd;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/b/l;
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/az;->a:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->bu:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/az;->b:Lcom/google/maps/api/android/lib6/c/ba;

    invoke-interface {v0, p1}, Lcom/google/maps/api/android/lib6/c/ba;->a(Lcom/google/android/gms/maps/model/LatLng;)Landroid/graphics/Point;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/b/l;)Lcom/google/android/gms/maps/model/LatLng;
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/az;->a:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->bs:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/az;->b:Lcom/google/maps/api/android/lib6/c/ba;

    invoke-static {p1}, Lcom/google/android/gms/b/p;->a(Lcom/google/android/gms/b/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Point;

    invoke-interface {v1, v0}, Lcom/google/maps/api/android/lib6/c/ba;->a(Landroid/graphics/Point;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/maps/internal/Point;)Lcom/google/android/gms/maps/model/LatLng;
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/az;->a:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->bs:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/az;->b:Lcom/google/maps/api/android/lib6/c/ba;

    invoke-virtual {p1}, Lcom/google/android/gms/maps/internal/Point;->b()Landroid/graphics/Point;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/ba;->a(Landroid/graphics/Point;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    return-object v0
.end method

.method public final a()Lcom/google/android/gms/maps/model/VisibleRegion;
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/az;->a:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->bt:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/az;->b:Lcom/google/maps/api/android/lib6/c/ba;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/ba;->a()Lcom/google/android/gms/maps/model/VisibleRegion;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/internal/Point;
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/az;->a:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->bu:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    new-instance v0, Lcom/google/android/gms/maps/internal/Point;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/az;->b:Lcom/google/maps/api/android/lib6/c/ba;

    invoke-interface {v1, p1}, Lcom/google/maps/api/android/lib6/c/ba;->a(Lcom/google/android/gms/maps/model/LatLng;)Landroid/graphics/Point;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/maps/internal/Point;-><init>(Landroid/graphics/Point;)V

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/az;->b:Lcom/google/maps/api/android/lib6/c/ba;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
