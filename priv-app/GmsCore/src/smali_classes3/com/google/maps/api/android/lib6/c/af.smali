.class public final Lcom/google/maps/api/android/lib6/c/af;
.super Lcom/google/android/gms/maps/internal/ae;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Landroid/widget/ImageView;

.field private final b:Landroid/widget/ImageView;

.field private final c:Landroid/widget/LinearLayout;

.field private d:Z

.field private e:Z

.field private f:Lcom/google/maps/api/android/lib6/c/ek;

.field private g:Lcom/google/maps/api/android/lib6/c/aj;

.field private h:Z

.field private i:Lcom/google/android/gms/maps/model/CameraPosition;

.field private j:I

.field private k:I

.field private l:I


# direct methods
.method private constructor <init>(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/content/res/Resources;Lcom/google/maps/api/android/lib6/c/ek;Z)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/maps/internal/ae;-><init>()V

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/c/af;->c:Landroid/widget/LinearLayout;

    iput-object p2, p0, Lcom/google/maps/api/android/lib6/c/af;->a:Landroid/widget/ImageView;

    iput-object p3, p0, Lcom/google/maps/api/android/lib6/c/af;->b:Landroid/widget/ImageView;

    invoke-static {p5}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/api/android/lib6/c/ek;

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/af;->f:Lcom/google/maps/api/android/lib6/c/ek;

    iput-boolean p6, p0, Lcom/google/maps/api/android/lib6/c/af;->e:Z

    sget v0, Lcom/google/android/gms/maps/aa;->c:I

    invoke-virtual {p4, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/maps/api/android/lib6/c/af;->j:I

    sget v0, Lcom/google/android/gms/maps/aa;->b:I

    invoke-virtual {p4, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/maps/api/android/lib6/c/af;->k:I

    sget v0, Lcom/google/android/gms/maps/aa;->a:I

    invoke-virtual {p4, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/maps/api/android/lib6/c/af;->l:I

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/res/Resources;Lcom/google/maps/api/android/lib6/c/ek;Z)Lcom/google/maps/api/android/lib6/c/af;
    .locals 7

    new-instance v1, Landroid/widget/LinearLayout;

    invoke-direct {v1, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    const-string v0, "GoogleMapToolbar"

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setTag(Ljava/lang/Object;)V

    new-instance v2, Landroid/widget/ImageView;

    invoke-direct {v2, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    sget v0, Lcom/google/android/gms/maps/ab;->N:I

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    sget v0, Lcom/google/android/gms/maps/ae;->f:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    const-string v0, "GoogleMapOpenGmmButton"

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    new-instance v3, Landroid/widget/ImageView;

    invoke-direct {v3, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    sget v0, Lcom/google/android/gms/maps/ab;->M:I

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    sget v0, Lcom/google/android/gms/maps/ae;->b:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    const-string v0, "GoogleMapDirectionsButton"

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v4, Lcom/google/android/gms/maps/z;->f:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setColorFilter(I)V

    new-instance v0, Lcom/google/maps/api/android/lib6/c/af;

    move-object v4, p1

    move-object v5, p2

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/maps/api/android/lib6/c/af;-><init>(Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/content/res/Resources;Lcom/google/maps/api/android/lib6/c/ek;Z)V

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method private a(Landroid/widget/ImageView;I)V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    invoke-virtual {p1}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    invoke-direct {v1, v2, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    sget v0, Lcom/google/android/gms/maps/ab;->d:I

    if-ne p2, v0, :cond_0

    iget v0, p0, Lcom/google/maps/api/android/lib6/c/af;->k:I

    iget v1, p0, Lcom/google/maps/api/android/lib6/c/af;->j:I

    iget v2, p0, Lcom/google/maps/api/android/lib6/c/af;->l:I

    invoke-virtual {p1, v0, v3, v1, v2}, Landroid/widget/ImageView;->setPadding(IIII)V

    :goto_0
    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    return-void

    :cond_0
    sget v0, Lcom/google/android/gms/maps/ab;->b:I

    if-ne p2, v0, :cond_1

    iget v0, p0, Lcom/google/maps/api/android/lib6/c/af;->j:I

    iget v1, p0, Lcom/google/maps/api/android/lib6/c/af;->k:I

    iget v2, p0, Lcom/google/maps/api/android/lib6/c/af;->l:I

    invoke-virtual {p1, v0, v3, v1, v2}, Landroid/widget/ImageView;->setPadding(IIII)V

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/google/maps/api/android/lib6/c/af;->l:I

    invoke-virtual {p1, v3, v3, v3, v0}, Landroid/widget/ImageView;->setPadding(IIII)V

    goto :goto_0
.end method

.method private b(Z)V
    .locals 4

    const/4 v1, 0x0

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/af;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-ne p1, v0, :cond_1

    :goto_1
    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/af;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/c/af;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLeft()I

    move-result v2

    sub-int v2, v0, v2

    if-eqz p1, :cond_2

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    int-to-float v2, v2

    invoke-direct {v0, v2, v3, v3, v3}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/c/af;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    :goto_2
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/af;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_1

    :cond_2
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    int-to-float v1, v2

    invoke-direct {v0, v3, v1, v3, v3}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    goto :goto_2
.end method


# virtual methods
.method public final a()Ljava/lang/Boolean;
    .locals 1

    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/af;->d:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/maps/model/CameraPosition;)V
    .locals 2

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/c/af;->i:Lcom/google/android/gms/maps/model/CameraPosition;

    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/af;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/af;->g:Lcom/google/maps/api/android/lib6/c/aj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/af;->g:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->G()Lcom/google/maps/api/android/lib6/c/al;

    move-result-object v0

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/af;->g:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0, v1}, Lcom/google/maps/api/android/lib6/c/al;->j(Lcom/google/maps/api/android/lib6/c/aj;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/af;->b()V

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/maps/api/android/lib6/c/aj;)V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/af;->g:Lcom/google/maps/api/android/lib6/c/aj;

    if-ne p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/af;->e:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/af;->b()V

    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 2

    iput-boolean p1, p0, Lcom/google/maps/api/android/lib6/c/af;->d:Z

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/af;->c:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public final a(ZLcom/google/maps/api/android/lib6/c/aj;Z)V
    .locals 3

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/af;->d:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/maps/api/android/lib6/c/af;->b:Landroid/widget/ImageView;

    if-eqz p1, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/af;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iput-object p2, p0, Lcom/google/maps/api/android/lib6/c/af;->g:Lcom/google/maps/api/android/lib6/c/aj;

    iput-boolean p3, p0, Lcom/google/maps/api/android/lib6/c/af;->h:Z

    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/af;->a:Landroid/widget/ImageView;

    sget v2, Lcom/google/android/gms/maps/ab;->d:I

    invoke-direct {p0, v0, v2}, Lcom/google/maps/api/android/lib6/c/af;->a(Landroid/widget/ImageView;I)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/af;->b:Landroid/widget/ImageView;

    sget v2, Lcom/google/android/gms/maps/ab;->b:I

    invoke-direct {p0, v0, v2}, Lcom/google/maps/api/android/lib6/c/af;->a(Landroid/widget/ImageView;I)V

    :goto_2
    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/af;->e:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/c/af;->b(Z)V

    :cond_1
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/af;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    :cond_2
    const/16 v0, 0x8

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/af;->a:Landroid/widget/ImageView;

    sget v2, Lcom/google/android/gms/maps/ab;->e:I

    invoke-direct {p0, v0, v2}, Lcom/google/maps/api/android/lib6/c/af;->a(Landroid/widget/ImageView;I)V

    goto :goto_2
.end method

.method public final b()V
    .locals 2

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/af;->g:Lcom/google/maps/api/android/lib6/c/aj;

    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/af;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/c/af;->b(Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/af;->c:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    return-void
.end method

.method public final c()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/af;->c:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 6

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/af;->a:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/af;->f:Lcom/google/maps/api/android/lib6/c/ek;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/af;->i:Lcom/google/android/gms/maps/model/CameraPosition;

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/c/af;->g:Lcom/google/maps/api/android/lib6/c/aj;

    iget-boolean v3, p0, Lcom/google/maps/api/android/lib6/c/af;->h:Z

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/maps/api/android/lib6/c/ek;->a(Lcom/google/android/gms/maps/model/CameraPosition;Lcom/google/maps/api/android/lib6/c/aj;Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/af;->b:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/af;->f:Lcom/google/maps/api/android/lib6/c/ek;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/af;->i:Lcom/google/android/gms/maps/model/CameraPosition;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/af;->g:Lcom/google/maps/api/android/lib6/c/aj;

    iget-object v2, v0, Lcom/google/maps/api/android/lib6/c/ek;->a:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v3, Lcom/google/maps/api/android/lib6/c/cf;->bZ:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v2, v3}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    invoke-virtual {v1}, Lcom/google/maps/api/android/lib6/c/aj;->c()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "http://maps.google.com/maps?saddr=&daddr="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, v1, Lcom/google/android/gms/maps/model/LatLng;->a:D

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, v1, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/maps/api/android/lib6/c/ek;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
