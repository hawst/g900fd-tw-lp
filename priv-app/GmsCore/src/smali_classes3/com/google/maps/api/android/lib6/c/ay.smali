.class public final Lcom/google/maps/api/android/lib6/c/ay;
.super Lcom/google/android/gms/maps/model/internal/z;

# interfaces
.implements Lcom/google/maps/api/android/lib6/c/as;
.implements Lcom/google/maps/api/android/lib6/c/au;


# static fields
.field private static a:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static final b:Lcom/google/android/gms/maps/model/PolylineOptions;


# instance fields
.field private final c:Ljava/lang/String;

.field private final d:Lcom/google/maps/api/android/lib6/c/ar;

.field private final e:Lcom/google/maps/api/android/lib6/c/cd;

.field private f:Lcom/google/maps/api/android/lib6/c/av;

.field private g:Lcom/google/maps/api/android/lib6/c/bz;

.field private h:Ljava/util/List;

.field private i:Ljava/util/List;

.field private j:I

.field private k:F

.field private l:Z

.field private m:F

.field private n:Z

.field private o:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/google/maps/api/android/lib6/c/ay;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Lcom/google/android/gms/maps/model/PolylineOptions;

    invoke-direct {v0}, Lcom/google/android/gms/maps/model/PolylineOptions;-><init>()V

    sput-object v0, Lcom/google/maps/api/android/lib6/c/ay;->b:Lcom/google/android/gms/maps/model/PolylineOptions;

    return-void
.end method

.method constructor <init>(Lcom/google/android/gms/maps/model/PolylineOptions;Lcom/google/maps/api/android/lib6/c/ar;Lcom/google/maps/api/android/lib6/c/cd;Lcom/google/maps/api/android/lib6/c/bz;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/maps/model/internal/z;-><init>()V

    invoke-static {p2}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/api/android/lib6/c/ar;

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->d:Lcom/google/maps/api/android/lib6/c/ar;

    invoke-static {p3}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/api/android/lib6/c/cd;

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->e:Lcom/google/maps/api/android/lib6/c/cd;

    iput-object p4, p0, Lcom/google/maps/api/android/lib6/c/ay;->g:Lcom/google/maps/api/android/lib6/c/bz;

    const-string v0, "pl%d"

    new-array v3, v1, [Ljava/lang/Object;

    sget-object v4, Lcom/google/maps/api/android/lib6/c/ay;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->c:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/PolylineOptions;->c()F

    move-result v0

    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_5

    move v0, v1

    :goto_0
    const-string v1, "line width is negative"

    invoke-static {v0, v1}, Lcom/google/k/a/cj;->a(ZLjava/lang/Object;)V

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/PolylineOptions;->c()F

    move-result v0

    iput v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->k:F

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/PolylineOptions;->d()I

    move-result v0

    iput v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->j:I

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/PolylineOptions;->e()F

    move-result v0

    iput v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->m:F

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/PolylineOptions;->f()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->o:Z

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/PolylineOptions;->g()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->n:Z

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/PolylineOptions;->b()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/k/c/gy;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->h:Ljava/util/List;

    invoke-direct {p0}, Lcom/google/maps/api/android/lib6/c/ay;->j()V

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/PolylineOptions;->d()I

    move-result v0

    sget-object v1, Lcom/google/maps/api/android/lib6/c/ay;->b:Lcom/google/android/gms/maps/model/PolylineOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/PolylineOptions;->d()I

    move-result v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->e:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->B:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/PolylineOptions;->c()F

    move-result v0

    sget-object v1, Lcom/google/maps/api/android/lib6/c/ay;->b:Lcom/google/android/gms/maps/model/PolylineOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/PolylineOptions;->c()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->e:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->A:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/PolylineOptions;->g()Z

    move-result v0

    sget-object v1, Lcom/google/maps/api/android/lib6/c/ay;->b:Lcom/google/android/gms/maps/model/PolylineOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/PolylineOptions;->g()Z

    move-result v1

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->e:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->E:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/PolylineOptions;->f()Z

    move-result v0

    sget-object v1, Lcom/google/maps/api/android/lib6/c/ay;->b:Lcom/google/android/gms/maps/model/PolylineOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/PolylineOptions;->f()Z

    move-result v1

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->e:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->D:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/PolylineOptions;->e()F

    move-result v0

    sget-object v1, Lcom/google/maps/api/android/lib6/c/ay;->b:Lcom/google/android/gms/maps/model/PolylineOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/PolylineOptions;->e()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->e:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->C:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    :cond_4
    return-void

    :cond_5
    move v0, v2

    goto/16 :goto_0
.end method

.method private b(I)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->l:Z

    if-eqz v0, :cond_0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    return-void

    :cond_0
    monitor-exit p0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->f:Lcom/google/maps/api/android/lib6/c/av;

    invoke-interface {v0, p1}, Lcom/google/maps/api/android/lib6/c/av;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized j()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->n:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->h:Ljava/util/List;

    invoke-static {v0}, Lcom/google/maps/api/android/lib6/c/ej;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->i:Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->h:Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->e:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->y:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/ay;->l()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->d:Lcom/google/maps/api/android/lib6/c/ar;

    invoke-virtual {v0, p0}, Lcom/google/maps/api/android/lib6/c/ar;->b(Lcom/google/maps/api/android/lib6/c/as;)V

    return-void
.end method

.method public final a(F)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->e:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->A:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "line width is negative"

    invoke-static {v0, v1}, Lcom/google/k/a/cj;->a(ZLjava/lang/Object;)V

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/google/maps/api/android/lib6/c/ay;->k:F

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/c/ay;->b(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->e:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->B:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/google/maps/api/android/lib6/c/ay;->j:I

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/c/ay;->b(I)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final a(Lcom/google/maps/api/android/lib6/c/av;)V
    .locals 0

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/c/ay;->f:Lcom/google/maps/api/android/lib6/c/av;

    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->e:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->z:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    invoke-static {p1}, Lcom/google/k/c/gy;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->h:Ljava/util/List;

    invoke-direct {p0}, Lcom/google/maps/api/android/lib6/c/ay;->j()V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/c/ay;->b(I)V

    return-void
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->e:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->D:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/google/maps/api/android/lib6/c/ay;->o:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v0, 0x20

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/c/ay;->b(I)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/android/gms/maps/model/internal/y;)Z
    .locals 1

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final b(F)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->e:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->C:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/google/maps/api/android/lib6/c/ay;->m:F

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v0, 0x40

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/c/ay;->b(I)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->e:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->E:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    iput-boolean p1, p0, Lcom/google/maps/api/android/lib6/c/ay;->n:Z

    invoke-direct {p0}, Lcom/google/maps/api/android/lib6/c/ay;->j()V

    return-void
.end method

.method public final c()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->h:Ljava/util/List;

    invoke-static {v0}, Lcom/google/k/c/gy;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public final d()F
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/ay;->o()F

    move-result v0

    return v0
.end method

.method public final e()I
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/ay;->p()I

    move-result v0

    return v0
.end method

.method public final f()F
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/ay;->s()F

    move-result v0

    return v0
.end method

.method public final g()Z
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/ay;->r()Z

    move-result v0

    return v0
.end method

.method public final h()Z
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->n:Z

    return v0
.end method

.method public final i()I
    .locals 1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public final l()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->l:Z

    if-eqz v0, :cond_0

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->l:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->f:Lcom/google/maps/api/android/lib6/c/av;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/av;->a()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized m()Ljava/util/List;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->i:Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final n()Ljava/util/List;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized o()F
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->k:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized p()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->j:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final q()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final declared-synchronized r()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->o:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized s()F
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/maps/api/android/lib6/c/ay;->m:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
