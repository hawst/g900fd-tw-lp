.class public final Lcom/google/maps/api/android/lib6/c/ej;
.super Ljava/lang/Object;


# direct methods
.method public static a(D)D
    .locals 2

    const-wide v0, 0x41584db040000000L    # 6371009.0

    div-double v0, p0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public static a(DD)D
    .locals 4

    sub-double v0, p0, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    const-wide v2, 0x4076800000000000L    # 360.0

    sub-double/2addr v2, v0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method public static a(Lcom/google/android/gms/maps/model/LatLng;D)D
    .locals 5

    iget-wide v0, p0, Lcom/google/android/gms/maps/model/LatLng;->a:D

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    const-wide v2, 0x41584db040000000L    # 6371009.0

    mul-double/2addr v0, v2

    div-double v0, p1, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public static a(Ljava/util/List;)Ljava/util/List;
    .locals 12

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p0}, Lcom/google/k/c/gy;->b(Ljava/lang/Iterable;)Ljava/util/LinkedList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, v8

    :goto_0
    return-object v0

    :cond_0
    iget-wide v2, v0, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iget-wide v4, v1, Lcom/google/android/gms/maps/model/LatLng;->a:D

    neg-double v4, v4

    cmpl-double v2, v2, v4

    if-nez v2, :cond_2

    iget-wide v2, v0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    iget-wide v4, v1, Lcom/google/android/gms/maps/model/LatLng;->b:D

    sub-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    const-wide v4, 0x4066800000000000L    # 180.0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_2

    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_3

    new-instance v2, Lcom/google/android/gms/maps/model/LatLng;

    const-wide/16 v4, 0x0

    iget-wide v6, v0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    iget-wide v10, v1, Lcom/google/android/gms/maps/model/LatLng;->b:D

    add-double/2addr v6, v10

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    div-double/2addr v6, v10

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    move-object v1, v2

    :goto_2
    invoke-virtual {v9, v1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    :goto_3
    invoke-virtual {v9}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    invoke-virtual {v9}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, v0, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iget-wide v4, v1, Lcom/google/android/gms/maps/model/LatLng;->a:D

    sub-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    iget-wide v4, v0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    iget-wide v6, v1, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-static {v4, v5, v6, v7}, Lcom/google/maps/api/android/lib6/c/ej;->a(DD)D

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    const-wide/high16 v4, 0x4010000000000000L    # 4.0

    cmpg-double v2, v2, v4

    if-gez v2, :cond_0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {v9}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/LatLng;

    goto :goto_3

    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    :cond_3
    invoke-static {v0}, Lcom/google/maps/api/android/lib6/c/be;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/maps/api/android/lib6/c/be;

    move-result-object v6

    invoke-static {v1}, Lcom/google/maps/api/android/lib6/c/be;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/maps/api/android/lib6/c/be;

    move-result-object v7

    new-instance v1, Lcom/google/maps/api/android/lib6/c/be;

    iget-wide v2, v6, Lcom/google/maps/api/android/lib6/c/be;->a:D

    iget-wide v4, v7, Lcom/google/maps/api/android/lib6/c/be;->a:D

    add-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    div-double/2addr v2, v4

    iget-wide v4, v6, Lcom/google/maps/api/android/lib6/c/be;->b:D

    iget-wide v10, v7, Lcom/google/maps/api/android/lib6/c/be;->b:D

    add-double/2addr v4, v10

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    div-double/2addr v4, v10

    iget-wide v10, v6, Lcom/google/maps/api/android/lib6/c/be;->c:D

    iget-wide v6, v7, Lcom/google/maps/api/android/lib6/c/be;->c:D

    add-double/2addr v6, v10

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    div-double/2addr v6, v10

    invoke-direct/range {v1 .. v7}, Lcom/google/maps/api/android/lib6/c/be;-><init>(DDD)V

    iget-wide v2, v1, Lcom/google/maps/api/android/lib6/c/be;->a:D

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_4

    iget-wide v2, v1, Lcom/google/maps/api/android/lib6/c/be;->b:D

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_4

    iget-wide v2, v1, Lcom/google/maps/api/android/lib6/c/be;->c:D

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_4

    new-instance v0, Ljava/lang/ArithmeticException;

    invoke-direct {v0}, Ljava/lang/ArithmeticException;-><init>()V

    throw v0

    :cond_4
    iget-wide v2, v1, Lcom/google/maps/api/android/lib6/c/be;->c:D

    iget-wide v4, v1, Lcom/google/maps/api/android/lib6/c/be;->a:D

    iget-wide v6, v1, Lcom/google/maps/api/android/lib6/c/be;->a:D

    mul-double/2addr v4, v6

    iget-wide v6, v1, Lcom/google/maps/api/android/lib6/c/be;->b:D

    iget-wide v10, v1, Lcom/google/maps/api/android/lib6/c/be;->b:D

    mul-double/2addr v6, v10

    add-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    iget-wide v2, v1, Lcom/google/maps/api/android/lib6/c/be;->b:D

    const-wide/16 v6, 0x0

    cmpl-double v2, v2, v6

    if-nez v2, :cond_5

    iget-wide v2, v1, Lcom/google/maps/api/android/lib6/c/be;->a:D

    const-wide/16 v6, 0x0

    cmpl-double v2, v2, v6

    if-nez v2, :cond_5

    const-wide/16 v2, 0x0

    :goto_4
    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    invoke-static {v2, v3}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v2

    invoke-direct {v1, v4, v5, v2, v3}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    goto/16 :goto_2

    :cond_5
    iget-wide v2, v1, Lcom/google/maps/api/android/lib6/c/be;->b:D

    iget-wide v6, v1, Lcom/google/maps/api/android/lib6/c/be;->a:D

    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    goto :goto_4

    :cond_6
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v0, v8

    goto/16 :goto_0
.end method

.method public static b(Lcom/google/android/gms/maps/model/LatLng;D)Ljava/util/List;
    .locals 27

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/google/android/gms/maps/model/LatLng;->a:D

    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v8

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-static {v10, v11}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v10

    const-wide v12, 0x41584db040000000L    # 6371009.0

    div-double v12, p1, v12

    invoke-static {v12, v13}, Ljava/lang/Math;->cos(D)D

    move-result-wide v14

    invoke-static {v12, v13}, Ljava/lang/Math;->sin(D)D

    move-result-wide v12

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v16

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    const/4 v6, 0x0

    :goto_0
    const/16 v18, 0x64

    move/from16 v0, v18

    if-ge v6, v0, :cond_0

    const-wide v18, 0x401921fb54442d18L    # 6.283185307179586

    int-to-double v0, v6

    move-wide/from16 v20, v0

    mul-double v18, v18, v20

    const-wide v20, 0x4058c00000000000L    # 99.0

    div-double v18, v18, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->cos(D)D

    move-result-wide v20

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->sin(D)D

    move-result-wide v18

    mul-double v22, v8, v14

    mul-double v24, v16, v12

    mul-double v20, v20, v24

    add-double v20, v20, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->asin(D)D

    move-result-wide v22

    mul-double v18, v18, v12

    mul-double v18, v18, v16

    mul-double v20, v20, v8

    sub-double v20, v14, v20

    invoke-static/range {v18 .. v21}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v18

    add-double v18, v18, v10

    new-instance v20, Lcom/google/android/gms/maps/model/LatLng;

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v22

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v18

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    move-wide/from16 v3, v18

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    move-object/from16 v0, v20

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_0
    return-object v7
.end method
