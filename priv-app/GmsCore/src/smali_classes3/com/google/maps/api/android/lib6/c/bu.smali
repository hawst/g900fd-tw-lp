.class public final Lcom/google/maps/api/android/lib6/c/bu;
.super Lcom/google/android/gms/maps/internal/cm;


# instance fields
.field private a:Lcom/google/maps/api/android/lib6/c/bo;

.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/gms/maps/StreetViewPanoramaOptions;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/maps/StreetViewPanoramaOptions;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/maps/internal/cm;-><init>()V

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/c/bu;->b:Landroid/content/Context;

    if-eqz p2, :cond_0

    :goto_0
    iput-object p2, p0, Lcom/google/maps/api/android/lib6/c/bu;->c:Lcom/google/android/gms/maps/StreetViewPanoramaOptions;

    return-void

    :cond_0
    new-instance p2, Lcom/google/android/gms/maps/StreetViewPanoramaOptions;

    invoke-direct {p2}, Lcom/google/android/gms/maps/StreetViewPanoramaOptions;-><init>()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/maps/api/android/lib6/c/bu;)Lcom/google/maps/api/android/lib6/c/bo;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bu;->a:Lcom/google/maps/api/android/lib6/c/bo;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/maps/internal/cf;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bu;->a:Lcom/google/maps/api/android/lib6/c/bo;

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bu;->b:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bu;->b:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/maps/api/android/lib6/c/by;->a(Landroid/app/Activity;)Z

    :cond_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bu;->b:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/bu;->c:Lcom/google/android/gms/maps/StreetViewPanoramaOptions;

    invoke-static {v0, v1}, Lcom/google/maps/api/android/lib6/c/bo;->a(Landroid/view/LayoutInflater;Lcom/google/android/gms/maps/StreetViewPanoramaOptions;)Lcom/google/maps/api/android/lib6/c/bo;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/bu;->a:Lcom/google/maps/api/android/lib6/c/bo;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bu;->a:Lcom/google/maps/api/android/lib6/c/bo;

    invoke-virtual {v0, p1}, Lcom/google/maps/api/android/lib6/c/bo;->a(Landroid/os/Bundle;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/internal/bw;)V
    .locals 2

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/google/maps/api/android/lib6/c/bv;

    invoke-direct {v1, p0, p1}, Lcom/google/maps/api/android/lib6/c/bv;-><init>(Lcom/google/maps/api/android/lib6/c/bu;Lcom/google/android/gms/maps/internal/bw;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bu;->a:Lcom/google/maps/api/android/lib6/c/bo;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bo;->g()V

    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bu;->a:Lcom/google/maps/api/android/lib6/c/bo;

    invoke-virtual {v0, p1}, Lcom/google/maps/api/android/lib6/c/bo;->b(Landroid/os/Bundle;)V

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bu;->a:Lcom/google/maps/api/android/lib6/c/bo;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bo;->h()V

    return-void
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bu;->a:Lcom/google/maps/api/android/lib6/c/bo;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bo;->i()V

    return-void
.end method

.method public final e()V
    .locals 0

    return-void
.end method

.method public final f()Lcom/google/android/gms/b/l;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bu;->a:Lcom/google/maps/api/android/lib6/c/bo;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bo;->j()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v0

    return-object v0
.end method
