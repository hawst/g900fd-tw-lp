.class public final Lcom/google/maps/api/android/lib6/d/o;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/maps/api/android/lib6/c/av;
.implements Lcom/google/maps/api/android/lib6/d/r;


# instance fields
.field private final a:Lcom/google/maps/api/android/lib6/c/au;

.field private final b:Z

.field private c:Lcom/google/maps/api/android/lib6/d/m;

.field private d:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Lcom/google/maps/api/android/lib6/c/au;ZLcom/google/maps/api/android/lib6/d/m;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/d/o;->d:Landroid/graphics/Paint;

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/d/o;->a:Lcom/google/maps/api/android/lib6/c/au;

    iput-object p3, p0, Lcom/google/maps/api/android/lib6/d/o;->c:Lcom/google/maps/api/android/lib6/d/m;

    iput-boolean p2, p0, Lcom/google/maps/api/android/lib6/d/o;->b:Z

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/o;->c:Lcom/google/maps/api/android/lib6/d/m;

    iget-object v1, v0, Lcom/google/maps/api/android/lib6/d/m;->a:Ljava/util/List;

    invoke-interface {v1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/d/m;->d:Lcom/google/maps/api/android/lib6/d/i;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/d/i;->invalidate()V

    return-void
.end method

.method private static a(Landroid/graphics/Path;Ljava/util/List;Lcom/google/maps/api/android/lib6/d/p;)V
    .locals 12

    const/4 v6, 0x0

    const/high16 v11, 0x40000000    # 2.0f

    invoke-virtual {p2}, Lcom/google/maps/api/android/lib6/d/p;->b()J

    move-result-wide v0

    long-to-float v8, v0

    iget v0, p2, Lcom/google/maps/api/android/lib6/d/p;->f:I

    int-to-float v0, v0

    sub-float/2addr v0, v8

    mul-float v1, v11, v8

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v9, v0

    neg-int v0, v9

    move v7, v0

    :goto_0
    if-gt v7, v9, :cond_3

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {p2, v0}, Lcom/google/maps/api/android/lib6/d/p;->a(Lcom/google/android/gms/maps/model/LatLng;)Landroid/graphics/Point;

    move-result-object v3

    iget v0, v3, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    int-to-float v1, v7

    mul-float/2addr v1, v8

    add-float/2addr v0, v1

    iget v1, v3, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    invoke-virtual {p0, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    const/4 v0, 0x1

    move v1, v0

    move-object v2, v3

    move v5, v6

    move-object v4, v3

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {p2, v0}, Lcom/google/maps/api/android/lib6/d/p;->a(Lcom/google/android/gms/maps/model/LatLng;)Landroid/graphics/Point;

    move-result-object v4

    iget v0, v4, Landroid/graphics/Point;->x:I

    iget v10, v2, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, v10

    int-to-float v0, v0

    div-float v10, v8, v11

    cmpl-float v0, v0, v10

    if-lez v0, :cond_0

    add-int/lit8 v2, v5, -0x1

    :goto_2
    iget v0, v4, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    add-int v5, v7, v2

    int-to-float v5, v5

    mul-float/2addr v5, v8

    add-float/2addr v0, v5

    iget v5, v4, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    invoke-virtual {p0, v0, v5}, Landroid/graphics/Path;->lineTo(FF)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v5, v2

    move-object v2, v4

    goto :goto_1

    :cond_0
    iget v0, v4, Landroid/graphics/Point;->x:I

    iget v2, v2, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, v2

    int-to-float v0, v0

    neg-float v2, v8

    div-float/2addr v2, v11

    cmpg-float v0, v0, v2

    if-gez v0, :cond_4

    add-int/lit8 v2, v5, 0x1

    goto :goto_2

    :cond_1
    iget v0, v3, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    iget v1, v4, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    int-to-float v2, v5

    mul-float/2addr v2, v8

    add-float/2addr v1, v2

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    iget v0, v3, Landroid/graphics/Point;->y:I

    iget v1, v4, Landroid/graphics/Point;->y:I

    if-ne v0, v1, :cond_2

    invoke-virtual {p0}, Landroid/graphics/Path;->close()V

    :cond_2
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    :cond_3
    return-void

    :cond_4
    move v2, v5

    goto :goto_2
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/o;->c:Lcom/google/maps/api/android/lib6/d/m;

    iget-object v1, v0, Lcom/google/maps/api/android/lib6/d/m;->a:Ljava/util/List;

    invoke-interface {v1, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/d/m;->d:Lcom/google/maps/api/android/lib6/d/i;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/d/i;->invalidate()V

    return-void
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/o;->c:Lcom/google/maps/api/android/lib6/d/m;

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/d/m;->d:Lcom/google/maps/api/android/lib6/d/i;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/d/i;->invalidate()V

    return-void
.end method

.method public final a(Landroid/graphics/Canvas;Lcom/google/maps/api/android/lib6/d/p;)V
    .locals 3

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/o;->a:Lcom/google/maps/api/android/lib6/c/au;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/au;->r()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/o;->a:Lcom/google/maps/api/android/lib6/c/au;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/au;->m()Ljava/util/List;

    move-result-object v0

    invoke-static {v1, v0, p2}, Lcom/google/maps/api/android/lib6/d/o;->a(Landroid/graphics/Path;Ljava/util/List;Lcom/google/maps/api/android/lib6/d/p;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/o;->a:Lcom/google/maps/api/android/lib6/c/au;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/au;->n()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-static {v1, v0, p2}, Lcom/google/maps/api/android/lib6/d/o;->a(Landroid/graphics/Path;Ljava/util/List;Lcom/google/maps/api/android/lib6/d/p;)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/o;->d:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    sget-object v0, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v1, v0}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/d/o;->b:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/o;->a:Lcom/google/maps/api/android/lib6/c/au;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/au;->q()I

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/o;->d:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/d/o;->a:Lcom/google/maps/api/android/lib6/c/au;

    invoke-interface {v2}, Lcom/google/maps/api/android/lib6/c/au;->q()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/o;->d:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/o;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :cond_3
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/o;->a:Lcom/google/maps/api/android/lib6/c/au;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/au;->p()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/o;->d:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/d/o;->a:Lcom/google/maps/api/android/lib6/c/au;

    invoke-interface {v2}, Lcom/google/maps/api/android/lib6/c/au;->p()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/o;->d:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/d/o;->a:Lcom/google/maps/api/android/lib6/c/au;

    invoke-interface {v2}, Lcom/google/maps/api/android/lib6/c/au;->o()F

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/o;->d:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/o;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto/16 :goto_0
.end method

.method public final b()F
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/o;->a:Lcom/google/maps/api/android/lib6/c/au;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/au;->s()F

    move-result v0

    return v0
.end method
