.class public final Lcom/google/maps/api/android/lib6/c/dg;
.super Lcom/android/volley/s;


# instance fields
.field private final d:Landroid/support/v4/g/h;

.field private final e:Lcom/android/volley/g;


# direct methods
.method constructor <init>(Lcom/android/volley/b;Lcom/android/volley/j;Landroid/support/v4/g/h;Lcom/android/volley/y;)V
    .locals 2

    const/4 v0, 0x4

    new-instance v1, Lcom/google/maps/api/android/lib6/c/di;

    invoke-direct {v1, p3, p4}, Lcom/google/maps/api/android/lib6/c/di;-><init>(Landroid/support/v4/g/h;Lcom/android/volley/y;)V

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/android/volley/s;-><init>(Lcom/android/volley/b;Lcom/android/volley/j;ILcom/android/volley/y;)V

    iput-object p3, p0, Lcom/google/maps/api/android/lib6/c/dg;->d:Landroid/support/v4/g/h;

    new-instance v0, Lcom/android/volley/g;

    invoke-static {}, Lcom/google/k/k/a/bg;->a()Lcom/google/k/k/a/bf;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/volley/g;-><init>(Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/dg;->e:Lcom/android/volley/g;

    return-void
.end method


# virtual methods
.method public final a(Lcom/android/volley/p;)Lcom/android/volley/p;
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dg;->d:Landroid/support/v4/g/h;

    invoke-virtual {p1}, Lcom/android/volley/p;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/volley/v;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/dg;->e:Lcom/android/volley/g;

    invoke-virtual {v1, p1, v0}, Lcom/android/volley/g;->a(Lcom/android/volley/p;Lcom/android/volley/v;)V

    :goto_0
    return-object p1

    :cond_0
    invoke-super {p0, p1}, Lcom/android/volley/s;->a(Lcom/android/volley/p;)Lcom/android/volley/p;

    move-result-object p1

    goto :goto_0
.end method
