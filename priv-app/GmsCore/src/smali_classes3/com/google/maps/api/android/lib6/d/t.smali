.class final Lcom/google/maps/api/android/lib6/d/t;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Landroid/graphics/Bitmap;

.field private synthetic b:Z

.field private synthetic c:Lcom/google/android/gms/maps/internal/cc;

.field private synthetic d:Lcom/google/maps/api/android/lib6/d/s;


# direct methods
.method constructor <init>(Lcom/google/maps/api/android/lib6/d/s;Landroid/graphics/Bitmap;ZLcom/google/android/gms/maps/internal/cc;)V
    .locals 1

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/d/t;->d:Lcom/google/maps/api/android/lib6/d/s;

    iput-object p2, p0, Lcom/google/maps/api/android/lib6/d/t;->a:Landroid/graphics/Bitmap;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/maps/api/android/lib6/d/t;->b:Z

    iput-object p4, p0, Lcom/google/maps/api/android/lib6/d/t;->c:Lcom/google/android/gms/maps/internal/cc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/d/t;->a:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/d/t;->d:Lcom/google/maps/api/android/lib6/d/s;

    invoke-static {v1}, Lcom/google/maps/api/android/lib6/d/s;->a(Lcom/google/maps/api/android/lib6/d/s;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/t;->d:Lcom/google/maps/api/android/lib6/d/s;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/d/t;->a:Landroid/graphics/Bitmap;

    iget-boolean v2, p0, Lcom/google/maps/api/android/lib6/d/t;->b:Z

    invoke-static {v0, v1, v2}, Lcom/google/maps/api/android/lib6/d/s;->a(Lcom/google/maps/api/android/lib6/d/s;Landroid/graphics/Bitmap;Z)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/t;->d:Lcom/google/maps/api/android/lib6/d/s;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/t;->c:Lcom/google/android/gms/maps/internal/cc;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/d/t;->a:Landroid/graphics/Bitmap;

    invoke-static {v0, v1}, Lcom/google/maps/api/android/lib6/d/s;->a(Lcom/google/android/gms/maps/internal/cc;Landroid/graphics/Bitmap;)V

    return-void
.end method
