.class public Lcom/google/maps/api/android/lib6/c/ag;
.super Lcom/google/android/gms/maps/internal/aa;


# instance fields
.field private a:Lcom/google/maps/api/android/lib6/c/ey;

.field private final b:Lcom/google/android/gms/maps/GoogleMapOptions;

.field private final c:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/maps/api/android/lib6/c/ag;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/maps/GoogleMapOptions;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/maps/internal/aa;-><init>()V

    invoke-static {p1}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/ag;->c:Landroid/content/Context;

    if-eqz p2, :cond_0

    :goto_0
    iput-object p2, p0, Lcom/google/maps/api/android/lib6/c/ag;->b:Lcom/google/android/gms/maps/GoogleMapOptions;

    return-void

    :cond_0
    new-instance p2, Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-direct {p2}, Lcom/google/android/gms/maps/GoogleMapOptions;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/maps/internal/k;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ag;->a:Lcom/google/maps/api/android/lib6/c/ey;

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/ag;->c:Landroid/content/Context;

    instance-of v1, v1, Landroid/app/Activity;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ag;->c:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/maps/api/android/lib6/c/by;->a(Landroid/app/Activity;)Z

    move-result v0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ag;->c:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/c/ag;->b:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-static {v0, v2, v1}, Lcom/google/maps/api/android/lib6/c/el;->a(Landroid/view/LayoutInflater;Lcom/google/android/gms/maps/GoogleMapOptions;Z)Lcom/google/maps/api/android/lib6/c/el;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/ag;->a:Lcom/google/maps/api/android/lib6/c/ey;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ag;->a:Lcom/google/maps/api/android/lib6/c/ey;

    invoke-interface {v0, p1}, Lcom/google/maps/api/android/lib6/c/ey;->a(Landroid/os/Bundle;)V

    return-void

    :cond_0
    move v1, v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/maps/internal/ay;)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ag;->a:Lcom/google/maps/api/android/lib6/c/ey;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ag;->a:Lcom/google/maps/api/android/lib6/c/ey;

    invoke-interface {v0, p1}, Lcom/google/maps/api/android/lib6/c/ey;->a(Lcom/google/android/gms/maps/internal/ay;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ag;->a:Lcom/google/maps/api/android/lib6/c/ey;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/ey;->o()V

    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ag;->a:Lcom/google/maps/api/android/lib6/c/ey;

    invoke-interface {v0, p1}, Lcom/google/maps/api/android/lib6/c/ey;->b(Landroid/os/Bundle;)V

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ag;->a:Lcom/google/maps/api/android/lib6/c/ey;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/ey;->p()V

    return-void
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ag;->a:Lcom/google/maps/api/android/lib6/c/ey;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/ey;->q()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/ag;->a:Lcom/google/maps/api/android/lib6/c/ey;

    return-void
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ag;->a:Lcom/google/maps/api/android/lib6/c/ey;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/ey;->r()V

    return-void
.end method

.method public final f()Lcom/google/android/gms/b/l;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/ag;->a:Lcom/google/maps/api/android/lib6/c/ey;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/ey;->C()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v0

    return-object v0
.end method
