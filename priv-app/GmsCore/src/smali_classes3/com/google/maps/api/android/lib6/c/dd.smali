.class public final Lcom/google/maps/api/android/lib6/c/dd;
.super Ljava/lang/Object;


# instance fields
.field public final a:Landroid/widget/RelativeLayout;

.field b:Lcom/google/maps/api/android/lib6/c/cm;

.field c:Lcom/google/maps/api/android/lib6/c/an;

.field d:Lcom/google/maps/api/android/lib6/c/n;

.field public e:Lcom/google/maps/api/android/lib6/c/af;

.field f:Lcom/google/maps/api/android/lib6/c/dy;

.field private final g:Landroid/content/res/Resources;

.field private final h:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;Lcom/google/maps/api/android/lib6/c/af;)V
    .locals 9

    const/4 v5, 0x2

    const/4 v4, -0x1

    const/4 v8, 0x1

    const/4 v7, -0x2

    const/4 v6, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-direct {v0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/dd;->a:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dd;->a:Landroid/widget/RelativeLayout;

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/c/dd;->h:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/maps/api/android/lib6/c/dd;->g:Landroid/content/res/Resources;

    iput-object p3, p0, Lcom/google/maps/api/android/lib6/c/dd;->e:Lcom/google/maps/api/android/lib6/c/af;

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/dd;->g:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/gms/maps/aa;->f:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/c/dd;->g:Landroid/content/res/Resources;

    sget v3, Lcom/google/android/gms/maps/aa;->e:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/dd;->g:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/gms/maps/aa;->d:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/c/dd;->g:Landroid/content/res/Resources;

    sget v3, Lcom/google/android/gms/maps/aa;->d:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v6, v1, v2, v6}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/dd;->h:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/c/dd;->g:Landroid/content/res/Resources;

    new-instance v3, Landroid/view/View;

    invoke-direct {v3, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/google/android/gms/maps/ab;->c:I

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const/16 v1, 0x8

    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    sget v1, Lcom/google/android/gms/maps/ae;->e:I

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    const-string v1, "GoogleMapMyLocationButton"

    invoke-virtual {v3, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    new-instance v1, Lcom/google/maps/api/android/lib6/c/an;

    invoke-direct {v1, v3}, Lcom/google/maps/api/android/lib6/c/an;-><init>(Landroid/view/View;)V

    iput-object v1, p0, Lcom/google/maps/api/android/lib6/c/dd;->c:Lcom/google/maps/api/android/lib6/c/an;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/dd;->c:Lcom/google/maps/api/android/lib6/c/an;

    iget-object v1, v1, Lcom/google/maps/api/android/lib6/c/an;->a:Landroid/view/View;

    invoke-direct {p0, v1, v5, v0}, Lcom/google/maps/api/android/lib6/c/dd;->a(Landroid/view/View;ILandroid/widget/RelativeLayout$LayoutParams;)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v7, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v0, v5, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/dd;->g:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/gms/maps/aa;->d:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/c/dd;->g:Landroid/content/res/Resources;

    sget v3, Lcom/google/android/gms/maps/aa;->d:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iget-object v3, p0, Lcom/google/maps/api/android/lib6/c/dd;->g:Landroid/content/res/Resources;

    sget v4, Lcom/google/android/gms/maps/aa;->d:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v0, v6, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/dd;->h:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/c/dd;->g:Landroid/content/res/Resources;

    new-instance v3, Lcom/google/maps/api/android/lib6/c/p;

    invoke-direct {v3, v1, v2}, Lcom/google/maps/api/android/lib6/c/p;-><init>(Landroid/content/Context;Landroid/content/res/Resources;)V

    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    sget v5, Lcom/google/android/gms/maps/aa;->f:I

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-direct {v4, v5, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v5, 0xf

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v3, v4}, Lcom/google/maps/api/android/lib6/c/p;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    sget v4, Lcom/google/android/gms/maps/ab;->K:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/google/maps/api/android/lib6/c/p;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v3, v6}, Lcom/google/maps/api/android/lib6/c/p;->setCacheColorHint(I)V

    invoke-virtual {v3, v8}, Lcom/google/maps/api/android/lib6/c/p;->setChoiceMode(I)V

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, v6}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v3, v2}, Lcom/google/maps/api/android/lib6/c/p;->setDivider(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v3, v6}, Lcom/google/maps/api/android/lib6/c/p;->setVerticalScrollBarEnabled(Z)V

    invoke-virtual {v3, v8}, Lcom/google/maps/api/android/lib6/c/p;->setScrollingCacheEnabled(Z)V

    invoke-virtual {v3, v8}, Lcom/google/maps/api/android/lib6/c/p;->setSmoothScrollbarEnabled(Z)V

    const/16 v2, 0x8

    invoke-virtual {v3, v2}, Lcom/google/maps/api/android/lib6/c/p;->setVisibility(I)V

    new-instance v2, Landroid/widget/RelativeLayout;

    invoke-direct {v2, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    const/16 v1, 0x8

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    new-instance v1, Lcom/google/maps/api/android/lib6/c/n;

    invoke-direct {v1, v3, v2}, Lcom/google/maps/api/android/lib6/c/n;-><init>(Lcom/google/maps/api/android/lib6/c/p;Landroid/view/View;)V

    iget-object v2, v1, Lcom/google/maps/api/android/lib6/c/n;->a:Lcom/google/maps/api/android/lib6/c/p;

    new-instance v3, Lcom/google/maps/api/android/lib6/c/o;

    invoke-direct {v3, v1}, Lcom/google/maps/api/android/lib6/c/o;-><init>(Lcom/google/maps/api/android/lib6/c/n;)V

    invoke-virtual {v2, v3}, Lcom/google/maps/api/android/lib6/c/p;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iput-object v1, p0, Lcom/google/maps/api/android/lib6/c/dd;->d:Lcom/google/maps/api/android/lib6/c/n;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/dd;->d:Lcom/google/maps/api/android/lib6/c/n;

    iget-object v1, v1, Lcom/google/maps/api/android/lib6/c/n;->b:Landroid/view/View;

    const/4 v2, 0x3

    invoke-direct {p0, v1, v2, v0}, Lcom/google/maps/api/android/lib6/c/dd;->a(Landroid/view/View;ILandroid/widget/RelativeLayout$LayoutParams;)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v7, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/dd;->g:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/gms/maps/aa;->h:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/c/dd;->g:Landroid/content/res/Resources;

    sget v3, Lcom/google/android/gms/maps/aa;->i:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v6, v6, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/dd;->h:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/c/dd;->g:Landroid/content/res/Resources;

    invoke-static {v1, v2}, Lcom/google/maps/api/android/lib6/c/cm;->a(Landroid/content/Context;Landroid/content/res/Resources;)Lcom/google/maps/api/android/lib6/c/cm;

    move-result-object v1

    iput-object v1, p0, Lcom/google/maps/api/android/lib6/c/dd;->b:Lcom/google/maps/api/android/lib6/c/cm;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/dd;->b:Lcom/google/maps/api/android/lib6/c/cm;

    invoke-virtual {v1}, Lcom/google/maps/api/android/lib6/c/cm;->a()Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1, v8, v0}, Lcom/google/maps/api/android/lib6/c/dd;->a(Landroid/view/View;ILandroid/widget/RelativeLayout$LayoutParams;)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v7, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v6, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    iput-boolean v8, v0, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/dd;->g:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/gms/maps/aa;->h:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/c/dd;->g:Landroid/content/res/Resources;

    sget v3, Lcom/google/android/gms/maps/aa;->i:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v6, v6, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/dd;->e:Lcom/google/maps/api/android/lib6/c/af;

    invoke-virtual {v1}, Lcom/google/maps/api/android/lib6/c/af;->c()Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x4

    invoke-direct {p0, v1, v2, v0}, Lcom/google/maps/api/android/lib6/c/dd;->a(Landroid/view/View;ILandroid/widget/RelativeLayout$LayoutParams;)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v7, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/dd;->g:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/gms/maps/aa;->d:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/c/dd;->g:Landroid/content/res/Resources;

    sget v3, Lcom/google/android/gms/maps/aa;->d:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v1, v2, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    new-instance v1, Lcom/google/maps/api/android/lib6/c/dy;

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/c/dd;->h:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/google/maps/api/android/lib6/c/dy;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/maps/api/android/lib6/c/dd;->f:Lcom/google/maps/api/android/lib6/c/dy;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/dd;->f:Lcom/google/maps/api/android/lib6/c/dy;

    const/4 v2, 0x5

    invoke-direct {p0, v1, v2, v0}, Lcom/google/maps/api/android/lib6/c/dd;->a(Landroid/view/View;ILandroid/widget/RelativeLayout$LayoutParams;)V

    return-void
.end method

.method private a(Landroid/view/View;ILandroid/widget/RelativeLayout$LayoutParams;)V
    .locals 1

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setId(I)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dd;->a:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1, p3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method
