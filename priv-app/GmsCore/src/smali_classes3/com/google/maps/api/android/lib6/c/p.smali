.class public final Lcom/google/maps/api/android/lib6/c/p;
.super Landroid/widget/ListView;

# interfaces
.implements Lcom/google/maps/api/android/lib6/c/f;


# instance fields
.field private a:I

.field private b:Lcom/google/maps/api/android/lib6/c/fa;

.field private c:Lcom/google/maps/api/android/lib6/c/e;

.field private d:I

.field private e:Lcom/google/maps/api/android/lib6/c/v;

.field private final f:Ljava/util/Set;

.field private final g:Landroid/content/res/Resources;

.field private final h:Lcom/google/maps/api/android/lib6/c/bz;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/maps/api/android/lib6/c/p;-><init>(Landroid/content/Context;Landroid/content/res/Resources;B)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;B)V
    .locals 2

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v1, p0, Lcom/google/maps/api/android/lib6/c/p;->a:I

    iput v1, p0, Lcom/google/maps/api/android/lib6/c/p;->d:I

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/p;->f:Ljava/util/Set;

    invoke-static {}, Lcom/google/maps/api/android/lib6/c/ca;->b()Lcom/google/maps/api/android/lib6/c/bz;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/p;->h:Lcom/google/maps/api/android/lib6/c/bz;

    iput-object p2, p0, Lcom/google/maps/api/android/lib6/c/p;->g:Landroid/content/res/Resources;

    return-void
.end method

.method static synthetic a(Lcom/google/maps/api/android/lib6/c/p;)Landroid/content/res/Resources;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/p;->g:Landroid/content/res/Resources;

    return-object v0
.end method

.method private a(Lcom/google/maps/api/android/lib6/c/fa;Lcom/google/maps/api/android/lib6/a/a/g;)V
    .locals 8

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v2, -0x1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/p;->h:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/p;->b:Lcom/google/maps/api/android/lib6/c/fa;

    invoke-static {p1, v0}, Lcom/google/maps/api/android/lib6/c/p;->b(Lcom/google/maps/api/android/lib6/c/fa;Lcom/google/maps/api/android/lib6/c/fa;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/p;->clearAnimation()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/p;->b:Lcom/google/maps/api/android/lib6/c/fa;

    iput v2, p0, Lcom/google/maps/api/android/lib6/c/p;->d:I

    iput v2, p0, Lcom/google/maps/api/android/lib6/c/p;->a:I

    if-eqz p1, :cond_0

    if-eqz p1, :cond_3

    invoke-interface {p1}, Lcom/google/maps/api/android/lib6/c/fa;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-interface {p1}, Lcom/google/maps/api/android/lib6/c/fa;->b()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lt v4, v0, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_0

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/c/p;->b:Lcom/google/maps/api/android/lib6/c/fa;

    invoke-virtual {p0, v3}, Lcom/google/maps/api/android/lib6/c/p;->setVisibility(I)V

    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v6, v7}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    const-wide/16 v4, 0x1f4

    invoke-virtual {v0, v4, v5}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    new-instance v1, Lcom/google/maps/api/android/lib6/c/q;

    invoke-direct {v1, p0}, Lcom/google/maps/api/android/lib6/c/q;-><init>(Lcom/google/maps/api/android/lib6/c/p;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/c/p;->startAnimation(Landroid/view/animation/Animation;)V

    new-instance v0, Lcom/google/maps/api/android/lib6/c/v;

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/p;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v4, p0, Lcom/google/maps/api/android/lib6/c/p;->b:Lcom/google/maps/api/android/lib6/c/fa;

    invoke-direct {v0, p0, v1, v4}, Lcom/google/maps/api/android/lib6/c/v;-><init>(Lcom/google/maps/api/android/lib6/c/p;Landroid/content/Context;Lcom/google/maps/api/android/lib6/c/fa;)V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/p;->e:Lcom/google/maps/api/android/lib6/c/v;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/p;->e:Lcom/google/maps/api/android/lib6/c/v;

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/c/p;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/p;->h:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/p;->b:Lcom/google/maps/api/android/lib6/c/fa;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/maps/api/android/lib6/c/p;->d:I

    if-eq v2, v0, :cond_0

    iput v2, p0, Lcom/google/maps/api/android/lib6/c/p;->d:I

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/p;->e:Lcom/google/maps/api/android/lib6/c/v;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/v;->notifyDataSetChanged()V

    :cond_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/p;->b:Lcom/google/maps/api/android/lib6/c/fa;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/p;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/c/p;->setVisibility(I)V

    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v7, v6}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    const-wide/16 v4, 0x1f4

    invoke-virtual {v0, v4, v5}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    new-instance v1, Lcom/google/maps/api/android/lib6/c/r;

    invoke-direct {v1, p0}, Lcom/google/maps/api/android/lib6/c/r;-><init>(Lcom/google/maps/api/android/lib6/c/p;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/c/p;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_1
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/p;->b:Lcom/google/maps/api/android/lib6/c/fa;

    if-nez v0, :cond_4

    :goto_2
    return-void

    :cond_2
    const/4 v0, 0x2

    goto :goto_0

    :cond_3
    move v0, v3

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/p;->b:Lcom/google/maps/api/android/lib6/c/fa;

    if-nez v1, :cond_6

    move v0, v2

    :cond_5
    :goto_3
    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/c/p;->a(I)V

    invoke-direct {p0}, Lcom/google/maps/api/android/lib6/c/p;->b()V

    goto :goto_2

    :cond_6
    if-nez p2, :cond_8

    invoke-interface {v1}, Lcom/google/maps/api/android/lib6/c/fa;->d()Z

    move-result v0

    if-eqz v0, :cond_9

    move v0, v3

    :cond_7
    :goto_4
    if-gez v0, :cond_5

    move v0, v2

    goto :goto_3

    :cond_8
    invoke-interface {v1, p2}, Lcom/google/maps/api/android/lib6/c/fa;->b(Lcom/google/maps/api/android/lib6/a/a/g;)I

    move-result v0

    if-ltz v0, :cond_7

    invoke-interface {v1}, Lcom/google/maps/api/android/lib6/c/fa;->d()Z

    move-result v1

    if-eqz v1, :cond_7

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_9
    move v0, v2

    goto :goto_4
.end method

.method static synthetic a(Lcom/google/maps/api/android/lib6/c/p;Lcom/google/maps/api/android/lib6/c/fa;Lcom/google/maps/api/android/lib6/a/a/g;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/maps/api/android/lib6/c/p;->a(Lcom/google/maps/api/android/lib6/c/fa;Lcom/google/maps/api/android/lib6/a/a/g;)V

    return-void
.end method

.method static synthetic a(Lcom/google/maps/api/android/lib6/c/fa;Lcom/google/maps/api/android/lib6/c/fa;)Z
    .locals 1

    invoke-static {p0, p1}, Lcom/google/maps/api/android/lib6/c/p;->b(Lcom/google/maps/api/android/lib6/c/fa;Lcom/google/maps/api/android/lib6/c/fa;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/google/maps/api/android/lib6/c/p;)I
    .locals 1

    iget v0, p0, Lcom/google/maps/api/android/lib6/c/p;->a:I

    return v0
.end method

.method private b()V
    .locals 2

    iget v0, p0, Lcom/google/maps/api/android/lib6/c/p;->a:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/maps/api/android/lib6/c/p;->a:I

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/c/p;->smoothScrollToPosition(I)V

    :cond_0
    return-void
.end method

.method private static b(Lcom/google/maps/api/android/lib6/c/fa;Lcom/google/maps/api/android/lib6/c/fa;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    if-eqz p0, :cond_1

    if-nez p1, :cond_2

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-interface {p0}, Lcom/google/maps/api/android/lib6/c/fa;->a()Lcom/google/maps/api/android/lib6/a/a/d;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/maps/api/android/lib6/c/fa;->a()Lcom/google/maps/api/android/lib6/a/a/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/maps/api/android/lib6/a/a/d;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/maps/api/android/lib6/c/p;)I
    .locals 1

    iget v0, p0, Lcom/google/maps/api/android/lib6/c/p;->d:I

    return v0
.end method

.method static synthetic d(Lcom/google/maps/api/android/lib6/c/p;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/p;->f:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic e(Lcom/google/maps/api/android/lib6/c/p;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/maps/api/android/lib6/c/p;->b()V

    return-void
.end method

.method static synthetic f(Lcom/google/maps/api/android/lib6/c/p;)Lcom/google/maps/api/android/lib6/c/e;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/p;->c:Lcom/google/maps/api/android/lib6/c/e;

    return-object v0
.end method

.method static synthetic g(Lcom/google/maps/api/android/lib6/c/p;)Lcom/google/maps/api/android/lib6/c/fa;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/p;->b:Lcom/google/maps/api/android/lib6/c/fa;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    new-instance v0, Lcom/google/maps/api/android/lib6/c/s;

    invoke-direct {v0, p0}, Lcom/google/maps/api/android/lib6/c/s;-><init>(Lcom/google/maps/api/android/lib6/c/p;)V

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/c/p;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(I)V
    .locals 2

    iget v0, p0, Lcom/google/maps/api/android/lib6/c/p;->a:I

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput p1, p0, Lcom/google/maps/api/android/lib6/c/p;->a:I

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/p;->e:Lcom/google/maps/api/android/lib6/c/v;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/v;->notifyDataSetChanged()V

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/maps/api/android/lib6/c/p;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/api/android/lib6/c/w;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/c/w;->a:Lcom/google/maps/api/android/lib6/c/d;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/p;->c:Lcom/google/maps/api/android/lib6/c/e;

    if-eqz v1, :cond_0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/p;->c:Lcom/google/maps/api/android/lib6/c/e;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/p;->b:Lcom/google/maps/api/android/lib6/c/fa;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/e;->d(Lcom/google/maps/api/android/lib6/c/fa;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/p;->c:Lcom/google/maps/api/android/lib6/c/e;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/d;->a()Lcom/google/maps/api/android/lib6/a/a/g;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/maps/api/android/lib6/c/e;->a(Lcom/google/maps/api/android/lib6/a/a/g;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/maps/api/android/lib6/c/e;)V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/p;->c:Lcom/google/maps/api/android/lib6/c/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/p;->c:Lcom/google/maps/api/android/lib6/c/e;

    invoke-interface {v0, p0}, Lcom/google/maps/api/android/lib6/c/e;->b(Lcom/google/maps/api/android/lib6/c/f;)V

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/p;->a()V

    invoke-interface {p1, p0}, Lcom/google/maps/api/android/lib6/c/e;->a(Lcom/google/maps/api/android/lib6/c/f;)V

    :cond_1
    iput-object p1, p0, Lcom/google/maps/api/android/lib6/c/p;->c:Lcom/google/maps/api/android/lib6/c/e;

    return-void
.end method

.method public final a(Lcom/google/maps/api/android/lib6/c/fa;)V
    .locals 1

    new-instance v0, Lcom/google/maps/api/android/lib6/c/t;

    invoke-direct {v0, p0, p1}, Lcom/google/maps/api/android/lib6/c/t;-><init>(Lcom/google/maps/api/android/lib6/c/p;Lcom/google/maps/api/android/lib6/c/fa;)V

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/c/p;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method final b(Lcom/google/maps/api/android/lib6/c/e;)V
    .locals 2

    invoke-interface {p1}, Lcom/google/maps/api/android/lib6/c/e;->c()Lcom/google/maps/api/android/lib6/c/fa;

    move-result-object v1

    const/4 v0, 0x0

    if-eqz v1, :cond_0

    invoke-interface {v1}, Lcom/google/maps/api/android/lib6/c/fa;->a()Lcom/google/maps/api/android/lib6/a/a/d;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/maps/api/android/lib6/c/e;->a(Lcom/google/maps/api/android/lib6/a/a/d;)Lcom/google/maps/api/android/lib6/a/a/g;

    move-result-object v0

    :cond_0
    invoke-direct {p0, v1, v0}, Lcom/google/maps/api/android/lib6/c/p;->a(Lcom/google/maps/api/android/lib6/c/fa;Lcom/google/maps/api/android/lib6/a/a/g;)V

    return-void
.end method

.method protected final onSizeChanged(IIII)V
    .locals 1

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ListView;->onSizeChanged(IIII)V

    new-instance v0, Lcom/google/maps/api/android/lib6/c/u;

    invoke-direct {v0, p0}, Lcom/google/maps/api/android/lib6/c/u;-><init>(Lcom/google/maps/api/android/lib6/c/p;)V

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/c/p;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
