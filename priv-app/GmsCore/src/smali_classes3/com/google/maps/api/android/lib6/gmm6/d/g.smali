.class public final Lcom/google/maps/api/android/lib6/gmm6/d/g;
.super Ljava/lang/Object;


# static fields
.field private static volatile a:Z

.field private static volatile b:Lcom/google/p/a/b/b/f;

.field private static volatile c:Lcom/google/maps/api/android/lib6/gmm6/d/c;

.field private static volatile d:Lcom/google/maps/api/android/lib6/gmm6/d/k;

.field private static volatile e:Lcom/google/maps/api/android/lib6/gmm6/d/f;

.field private static volatile f:Lcom/google/maps/api/android/lib6/gmm6/d/l;

.field private static volatile g:Lcom/google/maps/api/android/lib6/gmm6/d/e;

.field private static volatile h:Lcom/google/maps/api/android/lib6/gmm6/d/a;

.field private static volatile i:Lcom/google/maps/api/android/lib6/gmm6/d/b;

.field private static volatile j:Lcom/google/maps/api/android/lib6/gmm6/d/d;

.field private static volatile k:Lcom/google/p/a/b/b/f;

.field private static volatile l:Lcom/google/p/a/d/h;

.field private static volatile m:Z

.field private static volatile n:Z

.field private static o:Ljava/lang/Object;

.field private static p:Z

.field private static q:Z

.field private static r:Ljava/util/Map;

.field private static s:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v3, 0x8

    const/4 v1, 0x0

    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/maps/api/android/lib6/gmm6/d/g;->a:Z

    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/api/android/lib6/gmm6/d/g;->l:Lcom/google/p/a/d/h;

    sput-boolean v1, Lcom/google/maps/api/android/lib6/gmm6/d/g;->m:Z

    sput-boolean v1, Lcom/google/maps/api/android/lib6/gmm6/d/g;->n:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/maps/api/android/lib6/gmm6/d/g;->o:Ljava/lang/Object;

    sput-boolean v1, Lcom/google/maps/api/android/lib6/gmm6/d/g;->p:Z

    sput-boolean v1, Lcom/google/maps/api/android/lib6/gmm6/d/g;->q:Z

    invoke-static {}, Lcom/google/k/c/ft;->b()Lcom/google/k/c/fu;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/fu;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/fu;

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/fu;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/fu;

    move-result-object v0

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/fu;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/fu;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/fu;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/fu;

    move-result-object v0

    const/16 v1, 0x9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/fu;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/fu;

    move-result-object v0

    const/16 v1, 0xc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xe

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/fu;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/fu;

    move-result-object v0

    const/16 v1, 0xd

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xf

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/fu;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/fu;

    move-result-object v0

    const/16 v1, 0x25

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x27

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/fu;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/fu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/k/c/fu;->a()Lcom/google/k/c/ft;

    move-result-object v0

    sput-object v0, Lcom/google/maps/api/android/lib6/gmm6/d/g;->r:Ljava/util/Map;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/maps/api/android/lib6/gmm6/d/g;->s:Ljava/util/List;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized a()Lcom/google/maps/api/android/lib6/gmm6/d/c;
    .locals 2

    const-class v0, Lcom/google/maps/api/android/lib6/gmm6/d/g;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/google/maps/api/android/lib6/gmm6/d/g;->c:Lcom/google/maps/api/android/lib6/gmm6/d/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method static synthetic a(Lcom/google/p/a/d/h;)Lcom/google/p/a/d/h;
    .locals 0

    sput-object p0, Lcom/google/maps/api/android/lib6/gmm6/d/g;->l:Lcom/google/p/a/d/h;

    return-object p0
.end method

.method public static declared-synchronized a(Lcom/google/maps/api/android/lib6/b/h;Lcom/google/p/a/b/b/f;)V
    .locals 9

    const/4 v2, 0x0

    const-class v4, Lcom/google/maps/api/android/lib6/gmm6/d/g;

    monitor-enter v4

    :try_start_0
    const-string v5, "ServerControlledParametersManager.data"

    sget-object v0, Lcom/google/maps/api/android/lib6/gmm6/d/g;->b:Lcom/google/p/a/b/b/f;

    if-nez v0, :cond_5

    new-instance v6, Lcom/google/p/a/b/b/f;

    sget-object v0, Lcom/google/af/d/a/a/l;->c:Lcom/google/p/a/b/b/h;

    invoke-direct {v6, v0}, Lcom/google/p/a/b/b/f;-><init>(Lcom/google/p/a/b/b/h;)V

    sget-object v0, Lcom/google/maps/api/android/lib6/gmm6/d/g;->r:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    new-instance v3, Lcom/google/p/a/b/b/f;

    sget-object v1, Lcom/google/af/d/a/a/l;->d:Lcom/google/p/a/b/b/h;

    invoke-direct {v3, v1}, Lcom/google/p/a/b/b/f;-><init>(Lcom/google/p/a/b/b/h;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v8, 0x1

    invoke-virtual {v3, v8, v1}, Lcom/google/p/a/b/b/f;->f(II)Lcom/google/p/a/b/b/f;

    sparse-switch v1, :sswitch_data_0

    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v6, v1, v0}, Lcom/google/p/a/b/b/f;->a(ILcom/google/p/a/b/b/f;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    :sswitch_0
    :try_start_1
    new-instance v1, Lcom/google/p/a/b/b/f;

    sget-object v8, Lcom/google/af/d/a/a/l;->e:Lcom/google/p/a/b/b/h;

    invoke-direct {v1, v8}, Lcom/google/p/a/b/b/f;-><init>(Lcom/google/p/a/b/b/h;)V

    invoke-virtual {v3, v0, v1}, Lcom/google/p/a/b/b/f;->b(ILcom/google/p/a/b/b/f;)Lcom/google/p/a/b/b/f;

    move-object v0, v3

    goto :goto_1

    :sswitch_1
    new-instance v1, Lcom/google/p/a/b/b/f;

    sget-object v8, Lcom/google/af/d/a/a/l;->k:Lcom/google/p/a/b/b/h;

    invoke-direct {v1, v8}, Lcom/google/p/a/b/b/f;-><init>(Lcom/google/p/a/b/b/h;)V

    invoke-virtual {v3, v0, v1}, Lcom/google/p/a/b/b/f;->b(ILcom/google/p/a/b/b/f;)Lcom/google/p/a/b/b/f;

    move-object v0, v3

    goto :goto_1

    :sswitch_2
    new-instance v1, Lcom/google/p/a/b/b/f;

    sget-object v8, Lcom/google/af/d/a/a/l;->l:Lcom/google/p/a/b/b/h;

    invoke-direct {v1, v8}, Lcom/google/p/a/b/b/f;-><init>(Lcom/google/p/a/b/b/h;)V

    invoke-virtual {v3, v0, v1}, Lcom/google/p/a/b/b/f;->b(ILcom/google/p/a/b/b/f;)Lcom/google/p/a/b/b/f;

    move-object v0, v3

    goto :goto_1

    :sswitch_3
    new-instance v1, Lcom/google/p/a/b/b/f;

    sget-object v8, Lcom/google/af/d/a/a/l;->m:Lcom/google/p/a/b/b/h;

    invoke-direct {v1, v8}, Lcom/google/p/a/b/b/f;-><init>(Lcom/google/p/a/b/b/h;)V

    invoke-virtual {v3, v0, v1}, Lcom/google/p/a/b/b/f;->b(ILcom/google/p/a/b/b/f;)Lcom/google/p/a/b/b/f;

    move-object v0, v3

    goto :goto_1

    :sswitch_4
    new-instance v1, Lcom/google/p/a/b/b/f;

    sget-object v8, Lcom/google/af/d/a/a/l;->n:Lcom/google/p/a/b/b/h;

    invoke-direct {v1, v8}, Lcom/google/p/a/b/b/f;-><init>(Lcom/google/p/a/b/b/h;)V

    invoke-virtual {v3, v0, v1}, Lcom/google/p/a/b/b/f;->b(ILcom/google/p/a/b/b/f;)Lcom/google/p/a/b/b/f;

    move-object v0, v3

    goto :goto_1

    :sswitch_5
    new-instance v1, Lcom/google/p/a/b/b/f;

    sget-object v8, Lcom/google/af/d/a/a/l;->f:Lcom/google/p/a/b/b/h;

    invoke-direct {v1, v8}, Lcom/google/p/a/b/b/f;-><init>(Lcom/google/p/a/b/b/h;)V

    invoke-virtual {v3, v0, v1}, Lcom/google/p/a/b/b/f;->b(ILcom/google/p/a/b/b/f;)Lcom/google/p/a/b/b/f;

    move-object v0, v3

    goto :goto_1

    :sswitch_6
    new-instance v1, Lcom/google/p/a/b/b/f;

    sget-object v8, Lcom/google/af/d/a/a/l;->g:Lcom/google/p/a/b/b/h;

    invoke-direct {v1, v8}, Lcom/google/p/a/b/b/f;-><init>(Lcom/google/p/a/b/b/h;)V

    invoke-virtual {v3, v0, v1}, Lcom/google/p/a/b/b/f;->b(ILcom/google/p/a/b/b/f;)Lcom/google/p/a/b/b/f;

    move-object v0, v3

    goto :goto_1

    :sswitch_7
    new-instance v1, Lcom/google/p/a/b/b/f;

    sget-object v8, Lcom/google/af/d/a/a/l;->h:Lcom/google/p/a/b/b/h;

    invoke-direct {v1, v8}, Lcom/google/p/a/b/b/f;-><init>(Lcom/google/p/a/b/b/h;)V

    invoke-virtual {v3, v0, v1}, Lcom/google/p/a/b/b/f;->b(ILcom/google/p/a/b/b/f;)Lcom/google/p/a/b/b/f;

    move-object v0, v3

    goto :goto_1

    :sswitch_8
    new-instance v1, Lcom/google/p/a/b/b/f;

    sget-object v8, Lcom/google/maps/b/a/e;->b:Lcom/google/p/a/b/b/h;

    invoke-direct {v1, v8}, Lcom/google/p/a/b/b/f;-><init>(Lcom/google/p/a/b/b/h;)V

    invoke-virtual {v3, v0, v1}, Lcom/google/p/a/b/b/f;->b(ILcom/google/p/a/b/b/f;)Lcom/google/p/a/b/b/f;

    move-object v0, v3

    goto :goto_1

    :cond_1
    sput-object v6, Lcom/google/maps/api/android/lib6/gmm6/d/g;->b:Lcom/google/p/a/b/b/f;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-static {}, Lcom/google/maps/api/android/lib6/b/e;->e()Lcom/google/maps/api/android/lib6/b/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/b/e;->c()Lcom/google/p/a/b/x;

    move-result-object v0

    invoke-interface {v0, v5}, Lcom/google/p/a/b/x;->b(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_3

    new-instance v1, Lcom/google/p/a/b/b/f;

    sget-object v3, Lcom/google/af/d/a/a/l;->c:Lcom/google/p/a/b/b/h;

    invoke-direct {v1, v3}, Lcom/google/p/a/b/b/f;-><init>(Lcom/google/p/a/b/b/h;)V

    invoke-virtual {v1, v0}, Lcom/google/p/a/b/b/f;->a([B)Lcom/google/p/a/b/b/f;

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/google/p/a/b/b/f;->j(I)I

    move-result v3

    const-class v6, Lcom/google/maps/api/android/lib6/gmm6/d/g;

    monitor-enter v6
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v0, v2

    :goto_2
    if-ge v0, v3, :cond_2

    const/4 v7, 0x1

    :try_start_3
    invoke-virtual {v1, v7, v0}, Lcom/google/p/a/b/b/f;->c(II)Lcom/google/p/a/b/b/f;

    move-result-object v7

    invoke-static {v7}, Lcom/google/maps/api/android/lib6/gmm6/d/g;->d(Lcom/google/p/a/b/b/f;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/maps/api/android/lib6/gmm6/d/g;->q:Z

    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    invoke-static {}, Lcom/google/maps/api/android/lib6/gmm6/d/g;->n()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_3
    :goto_3
    :try_start_5
    sget-object v0, Lcom/google/maps/api/android/lib6/gmm6/d/g;->b:Lcom/google/p/a/b/b/f;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/f;->j(I)I

    move-result v1

    move v0, v2

    :goto_4
    if-ge v0, v1, :cond_4

    sget-object v2, Lcom/google/maps/api/android/lib6/gmm6/d/g;->b:Lcom/google/p/a/b/b/f;

    const/4 v3, 0x1

    invoke-virtual {v2, v3, v0}, Lcom/google/p/a/b/b/f;->c(II)Lcom/google/p/a/b/b/f;

    move-result-object v2

    invoke-static {v2}, Lcom/google/maps/api/android/lib6/gmm6/d/g;->c(Lcom/google/p/a/b/b/f;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit v6

    throw v0
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catch_0
    move-exception v0

    goto :goto_3

    :cond_4
    :try_start_7
    invoke-static {p0, v5, p1}, Lcom/google/maps/api/android/lib6/gmm6/d/g;->a(Lcom/google/maps/api/android/lib6/b/h;Ljava/lang/String;Lcom/google/p/a/b/b/f;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_5
    monitor-exit v4

    return-void

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3 -> :sswitch_1
        0x6 -> :sswitch_2
        0x8 -> :sswitch_3
        0x9 -> :sswitch_4
        0xb -> :sswitch_5
        0xc -> :sswitch_6
        0xd -> :sswitch_7
        0x25 -> :sswitch_8
    .end sparse-switch
.end method

.method private static declared-synchronized a(Lcom/google/maps/api/android/lib6/b/h;Ljava/lang/String;Lcom/google/p/a/b/b/f;)V
    .locals 3

    const-class v1, Lcom/google/maps/api/android/lib6/gmm6/d/g;

    monitor-enter v1

    if-nez p0, :cond_0

    :goto_0
    monitor-exit v1

    return-void

    :cond_0
    :try_start_0
    sget-object v2, Lcom/google/maps/api/android/lib6/gmm6/d/g;->o:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    sget-object v0, Lcom/google/maps/api/android/lib6/gmm6/d/g;->l:Lcom/google/p/a/d/h;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/maps/api/android/lib6/gmm6/d/g;->l:Lcom/google/p/a/d/h;

    invoke-virtual {v0}, Lcom/google/p/a/d/h;->a()I

    const/4 v0, 0x0

    sput-object v0, Lcom/google/maps/api/android/lib6/gmm6/d/g;->l:Lcom/google/p/a/d/h;

    :cond_1
    sget-boolean v0, Lcom/google/maps/api/android/lib6/gmm6/d/g;->n:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/maps/api/android/lib6/gmm6/d/g;->m:Z

    :goto_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v2

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2
    const/4 v0, 0x1

    :try_start_3
    sput-boolean v0, Lcom/google/maps/api/android/lib6/gmm6/d/g;->n:Z

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/maps/api/android/lib6/gmm6/d/g;->m:Z

    new-instance v0, Lcom/google/maps/api/android/lib6/gmm6/d/i;

    invoke-direct {v0, p1, p2}, Lcom/google/maps/api/android/lib6/gmm6/d/i;-><init>(Ljava/lang/String;Lcom/google/p/a/b/b/f;)V

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/b/h;->c(Lcom/google/maps/api/android/lib6/b/g;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/maps/api/android/lib6/b/h;Ljava/lang/String;ZLcom/google/p/a/b/b/f;)V
    .locals 0

    invoke-static {p0, p1, p3}, Lcom/google/maps/api/android/lib6/gmm6/d/g;->a(Lcom/google/maps/api/android/lib6/b/h;Ljava/lang/String;Lcom/google/p/a/b/b/f;)V

    return-void
.end method

.method public static a(Lcom/google/maps/api/android/lib6/gmm6/d/h;)V
    .locals 3

    const/4 v0, 0x0

    const-class v1, Lcom/google/maps/api/android/lib6/gmm6/d/g;

    monitor-enter v1

    :try_start_0
    sget-boolean v2, Lcom/google/maps/api/android/lib6/gmm6/d/g;->p:Z

    if-nez v2, :cond_0

    sget-boolean v2, Lcom/google/maps/api/android/lib6/gmm6/d/g;->q:Z

    if-eqz v2, :cond_2

    :cond_0
    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p0, :cond_1

    invoke-interface {p0}, Lcom/google/maps/api/android/lib6/gmm6/d/h;->a()V

    :cond_1
    return-void

    :cond_2
    :try_start_1
    sget-object v2, Lcom/google/maps/api/android/lib6/gmm6/d/g;->s:Ljava/util/List;

    invoke-interface {v2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object p0, v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Lcom/google/p/a/b/b/f;Lcom/google/p/a/b/b/f;)V
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x1

    new-instance v0, Lcom/google/p/a/b/b/f;

    sget-object v1, Lcom/google/af/d/a/a/l;->b:Lcom/google/p/a/b/b/h;

    invoke-direct {v0, v1}, Lcom/google/p/a/b/b/f;-><init>(Lcom/google/p/a/b/b/h;)V

    invoke-static {}, Lcom/google/maps/api/android/lib6/gmm6/f;->a()Lcom/google/maps/api/android/lib6/gmm6/f;

    invoke-static {}, Lcom/google/maps/api/android/lib6/gmm6/f;->d()Z

    move-result v1

    invoke-virtual {v0, v2, v2}, Lcom/google/p/a/b/b/f;->a(IZ)Lcom/google/p/a/b/b/f;

    invoke-virtual {v0, v3, v1}, Lcom/google/p/a/b/b/f;->a(IZ)Lcom/google/p/a/b/b/f;

    const/4 v1, 0x4

    invoke-virtual {v0, v1, p1}, Lcom/google/p/a/b/b/f;->b(ILcom/google/p/a/b/b/f;)Lcom/google/p/a/b/b/f;

    invoke-virtual {p0, v3, v0}, Lcom/google/p/a/b/b/f;->a(ILcom/google/p/a/b/b/f;)V

    return-void
.end method

.method static synthetic a(Lcom/google/p/a/b/b/f;)Z
    .locals 1

    invoke-static {p0}, Lcom/google/maps/api/android/lib6/gmm6/d/g;->c(Lcom/google/p/a/b/b/f;)Z

    move-result v0

    return v0
.end method

.method static a(Lcom/google/p/a/b/b/f;Ljava/lang/String;)Z
    .locals 2

    :try_start_0
    invoke-static {}, Lcom/google/maps/api/android/lib6/b/e;->e()Lcom/google/maps/api/android/lib6/b/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/b/e;->c()Lcom/google/p/a/b/x;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/p/a/b/b/f;->e()[B

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/google/p/a/b/x;->a([BLjava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static declared-synchronized b()Lcom/google/maps/api/android/lib6/gmm6/d/f;
    .locals 2

    const-class v0, Lcom/google/maps/api/android/lib6/gmm6/d/g;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/google/maps/api/android/lib6/gmm6/d/g;->e:Lcom/google/maps/api/android/lib6/gmm6/d/f;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method static synthetic b(Lcom/google/p/a/b/b/f;)V
    .locals 0

    invoke-static {p0}, Lcom/google/maps/api/android/lib6/gmm6/d/g;->d(Lcom/google/p/a/b/b/f;)V

    return-void
.end method

.method public static declared-synchronized c()Lcom/google/maps/api/android/lib6/gmm6/d/l;
    .locals 2

    const-class v0, Lcom/google/maps/api/android/lib6/gmm6/d/g;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/google/maps/api/android/lib6/gmm6/d/g;->f:Lcom/google/maps/api/android/lib6/gmm6/d/l;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private static c(Lcom/google/p/a/b/b/f;)Z
    .locals 5

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v0, -0x1

    invoke-static {p0, v2, v0}, Lcom/google/p/a/b/b/j;->a(Lcom/google/p/a/b/b/f;II)I

    move-result v3

    sget-object v0, Lcom/google/maps/api/android/lib6/gmm6/d/g;->r:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/google/maps/api/android/lib6/gmm6/d/g;->r:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/p/a/b/b/f;->i(I)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {p0, v0}, Lcom/google/p/a/b/b/f;->f(I)Lcom/google/p/a/b/b/f;

    move-result-object v0

    sparse-switch v3, :sswitch_data_0

    move v0, v1

    goto :goto_0

    :sswitch_0
    sget-object v1, Lcom/google/maps/api/android/lib6/gmm6/d/g;->c:Lcom/google/maps/api/android/lib6/gmm6/d/c;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/google/maps/api/android/lib6/gmm6/d/g;->c:Lcom/google/maps/api/android/lib6/gmm6/d/c;

    invoke-virtual {v1, v0}, Lcom/google/maps/api/android/lib6/gmm6/d/c;->a(Lcom/google/p/a/b/b/f;)V

    :goto_1
    move v0, v2

    goto :goto_0

    :cond_1
    new-instance v1, Lcom/google/maps/api/android/lib6/gmm6/d/c;

    invoke-direct {v1, v0}, Lcom/google/maps/api/android/lib6/gmm6/d/c;-><init>(Lcom/google/p/a/b/b/f;)V

    sput-object v1, Lcom/google/maps/api/android/lib6/gmm6/d/g;->c:Lcom/google/maps/api/android/lib6/gmm6/d/c;

    goto :goto_1

    :sswitch_1
    new-instance v1, Lcom/google/maps/api/android/lib6/gmm6/d/k;

    invoke-direct {v1, v0}, Lcom/google/maps/api/android/lib6/gmm6/d/k;-><init>(Lcom/google/p/a/b/b/f;)V

    sput-object v1, Lcom/google/maps/api/android/lib6/gmm6/d/g;->d:Lcom/google/maps/api/android/lib6/gmm6/d/k;

    invoke-static {}, Lcom/google/maps/api/android/lib6/gmm6/f;->a()Lcom/google/maps/api/android/lib6/gmm6/f;

    invoke-static {}, Lcom/google/maps/api/android/lib6/gmm6/f;->h()V

    move v0, v2

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/maps/api/android/lib6/gmm6/d/f;

    invoke-direct {v1, v0}, Lcom/google/maps/api/android/lib6/gmm6/d/f;-><init>(Lcom/google/p/a/b/b/f;)V

    sput-object v1, Lcom/google/maps/api/android/lib6/gmm6/d/g;->e:Lcom/google/maps/api/android/lib6/gmm6/d/f;

    move v0, v2

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/maps/api/android/lib6/gmm6/d/l;

    invoke-direct {v1, v0}, Lcom/google/maps/api/android/lib6/gmm6/d/l;-><init>(Lcom/google/p/a/b/b/f;)V

    sput-object v1, Lcom/google/maps/api/android/lib6/gmm6/d/g;->f:Lcom/google/maps/api/android/lib6/gmm6/d/l;

    move v0, v2

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/maps/api/android/lib6/gmm6/d/e;

    invoke-direct {v1, v0}, Lcom/google/maps/api/android/lib6/gmm6/d/e;-><init>(Lcom/google/p/a/b/b/f;)V

    sput-object v1, Lcom/google/maps/api/android/lib6/gmm6/d/g;->g:Lcom/google/maps/api/android/lib6/gmm6/d/e;

    move v0, v2

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/maps/api/android/lib6/gmm6/d/a;

    invoke-direct {v1, v0}, Lcom/google/maps/api/android/lib6/gmm6/d/a;-><init>(Lcom/google/p/a/b/b/f;)V

    sput-object v1, Lcom/google/maps/api/android/lib6/gmm6/d/g;->h:Lcom/google/maps/api/android/lib6/gmm6/d/a;

    move v0, v2

    goto :goto_0

    :sswitch_6
    new-instance v1, Lcom/google/maps/api/android/lib6/gmm6/d/b;

    invoke-direct {v1, v0}, Lcom/google/maps/api/android/lib6/gmm6/d/b;-><init>(Lcom/google/p/a/b/b/f;)V

    sput-object v1, Lcom/google/maps/api/android/lib6/gmm6/d/g;->i:Lcom/google/maps/api/android/lib6/gmm6/d/b;

    move v0, v2

    goto :goto_0

    :sswitch_7
    new-instance v1, Lcom/google/maps/api/android/lib6/gmm6/d/d;

    invoke-direct {v1, v0}, Lcom/google/maps/api/android/lib6/gmm6/d/d;-><init>(Lcom/google/p/a/b/b/f;)V

    sput-object v1, Lcom/google/maps/api/android/lib6/gmm6/d/g;->j:Lcom/google/maps/api/android/lib6/gmm6/d/d;

    move v0, v2

    goto :goto_0

    :sswitch_8
    :try_start_0
    invoke-static {v0}, Lcom/google/p/a/b/b/j;->d(Lcom/google/p/a/b/b/f;)Lcom/google/p/a/b/b/f;

    move-result-object v0

    sput-object v0, Lcom/google/maps/api/android/lib6/gmm6/d/g;->k:Lcom/google/p/a/b/b/f;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v2

    goto :goto_0

    :catch_0
    move-exception v0

    move v0, v1

    goto/16 :goto_0

    :cond_2
    move v0, v1

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3 -> :sswitch_1
        0x6 -> :sswitch_2
        0x8 -> :sswitch_3
        0x9 -> :sswitch_4
        0xb -> :sswitch_5
        0xc -> :sswitch_6
        0xd -> :sswitch_7
        0x25 -> :sswitch_8
    .end sparse-switch
.end method

.method public static declared-synchronized d()Lcom/google/maps/api/android/lib6/gmm6/d/b;
    .locals 2

    const-class v0, Lcom/google/maps/api/android/lib6/gmm6/d/g;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/google/maps/api/android/lib6/gmm6/d/g;->i:Lcom/google/maps/api/android/lib6/gmm6/d/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private static d(Lcom/google/p/a/b/b/f;)V
    .locals 5

    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lcom/google/p/a/b/b/f;->d(I)I

    move-result v1

    sget-object v0, Lcom/google/maps/api/android/lib6/gmm6/d/g;->r:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/maps/api/android/lib6/gmm6/d/g;->b:Lcom/google/p/a/b/b/f;

    invoke-virtual {v0, v4}, Lcom/google/p/a/b/b/f;->j(I)I

    move-result v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_1

    sget-object v3, Lcom/google/maps/api/android/lib6/gmm6/d/g;->b:Lcom/google/p/a/b/b/f;

    invoke-virtual {v3, v4, v0}, Lcom/google/p/a/b/b/f;->c(II)Lcom/google/p/a/b/b/f;

    move-result-object v3

    invoke-virtual {v3, v4}, Lcom/google/p/a/b/b/f;->d(I)I

    move-result v3

    if-ne v1, v3, :cond_2

    sget-object v1, Lcom/google/maps/api/android/lib6/gmm6/d/g;->b:Lcom/google/p/a/b/b/f;

    invoke-virtual {v1, v4, v0}, Lcom/google/p/a/b/b/f;->e(II)V

    :cond_1
    sget-object v0, Lcom/google/maps/api/android/lib6/gmm6/d/g;->b:Lcom/google/p/a/b/b/f;

    invoke-virtual {v0, v4, p0}, Lcom/google/p/a/b/b/f;->a(ILcom/google/p/a/b/b/f;)V

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static declared-synchronized e()Lcom/google/p/a/b/b/f;
    .locals 2

    const-class v0, Lcom/google/maps/api/android/lib6/gmm6/d/g;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/google/maps/api/android/lib6/gmm6/d/g;->k:Lcom/google/p/a/b/b/f;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method static synthetic f()Lcom/google/p/a/b/b/f;
    .locals 1

    sget-object v0, Lcom/google/maps/api/android/lib6/gmm6/d/g;->b:Lcom/google/p/a/b/b/f;

    return-object v0
.end method

.method static synthetic g()Z
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/maps/api/android/lib6/gmm6/d/g;->p:Z

    return v0
.end method

.method static synthetic h()V
    .locals 0

    invoke-static {}, Lcom/google/maps/api/android/lib6/gmm6/d/g;->n()V

    return-void
.end method

.method static synthetic i()Z
    .locals 1

    sget-boolean v0, Lcom/google/maps/api/android/lib6/gmm6/d/g;->a:Z

    return v0
.end method

.method static synthetic j()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/google/maps/api/android/lib6/gmm6/d/g;->o:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic k()Z
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/maps/api/android/lib6/gmm6/d/g;->n:Z

    return v0
.end method

.method static synthetic l()Z
    .locals 1

    sget-boolean v0, Lcom/google/maps/api/android/lib6/gmm6/d/g;->m:Z

    return v0
.end method

.method static synthetic m()Lcom/google/p/a/d/h;
    .locals 1

    sget-object v0, Lcom/google/maps/api/android/lib6/gmm6/d/g;->l:Lcom/google/p/a/d/h;

    return-object v0
.end method

.method private static n()V
    .locals 3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const-class v1, Lcom/google/maps/api/android/lib6/gmm6/d/g;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/maps/api/android/lib6/gmm6/d/g;->s:Ljava/util/List;

    invoke-static {v0}, Lcom/google/k/c/gy;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    sget-object v2, Lcom/google/maps/api/android/lib6/gmm6/d/g;->s:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/api/android/lib6/gmm6/d/h;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/gmm6/d/h;->a()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    return-void
.end method
