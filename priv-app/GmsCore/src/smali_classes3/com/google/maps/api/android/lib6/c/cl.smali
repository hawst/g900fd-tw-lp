.class public final Lcom/google/maps/api/android/lib6/c/cl;
.super Ljava/lang/Object;


# instance fields
.field final a:Landroid/widget/RelativeLayout;

.field final b:Landroid/content/res/Resources;

.field c:Landroid/widget/ImageView;

.field d:Landroid/widget/TextView;

.field final e:Z

.field private final f:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;)V
    .locals 1

    invoke-static {p1}, Lcom/google/maps/api/android/lib6/c/by;->b(Landroid/content/Context;)Z

    move-result v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/maps/api/android/lib6/c/cl;-><init>(Landroid/content/Context;Landroid/content/res/Resources;Z)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;Z)V
    .locals 6

    const/16 v5, 0xc

    const/4 v0, 0x0

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Landroid/widget/RelativeLayout;

    invoke-direct {v1, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/maps/api/android/lib6/c/cl;->a:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/cl;->a:Landroid/widget/RelativeLayout;

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/c/cl;->f:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/maps/api/android/lib6/c/cl;->b:Landroid/content/res/Resources;

    iput-boolean p3, p0, Lcom/google/maps/api/android/lib6/c/cl;->e:Z

    new-instance v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/c/cl;->f:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/maps/api/android/lib6/c/cl;->c:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/cl;->c:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/c/cl;->b:Landroid/content/res/Resources;

    sget v3, Lcom/google/android/gms/maps/ab;->P:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/c/cl;->b:Landroid/content/res/Resources;

    sget v3, Lcom/google/android/gms/maps/aa;->m:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v1, v2, v2, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/c/cl;->c:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/cl;->c:Landroid/widget/ImageView;

    const-string v2, "GoogleWatermark"

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/cl;->a:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/c/cl;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/c/cl;->f:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/maps/api/android/lib6/c/cl;->d:Landroid/widget/TextView;

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0xb

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/c/cl;->d:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/cl;->d:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/c/cl;->b:Landroid/content/res/Resources;

    sget v3, Lcom/google/android/gms/maps/aa;->j:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v0, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/cl;->d:Landroid/widget/TextView;

    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/cl;->d:Landroid/widget/TextView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setSingleLine(Z)V

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/cl;->d:Landroid/widget/TextView;

    const-string v2, "GoogleCopyrights"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/cl;->d:Landroid/widget/TextView;

    iget-boolean v2, p0, Lcom/google/maps/api/android/lib6/c/cl;->e:Z

    if-eqz v2, :cond_0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/cl;->a:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/cl;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
