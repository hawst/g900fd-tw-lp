.class public final Lcom/google/maps/api/android/lib6/c/dz;
.super Landroid/content/BroadcastReceiver;


# static fields
.field private static a:Lcom/google/maps/api/android/lib6/c/dz;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ljava/util/Set;

.field private d:Lcom/android/volley/s;

.field private e:Z

.field private f:Lcom/google/maps/api/android/lib6/b/h;

.field private g:I


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/dz;->c:Ljava/util/Set;

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/c/dz;->b:Landroid/content/Context;

    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/maps/api/android/lib6/c/dz;
    .locals 2

    const-class v1, Lcom/google/maps/api/android/lib6/c/dz;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/maps/api/android/lib6/c/dz;->a:Lcom/google/maps/api/android/lib6/c/dz;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/maps/api/android/lib6/c/dz;

    invoke-direct {v0, p0}, Lcom/google/maps/api/android/lib6/c/dz;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/maps/api/android/lib6/c/dz;->a:Lcom/google/maps/api/android/lib6/c/dz;

    :cond_0
    sget-object v0, Lcom/google/maps/api/android/lib6/c/dz;->a:Lcom/google/maps/api/android/lib6/c/dz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private declared-synchronized e()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/dz;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dz;->b:Landroid/content/Context;

    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/dz;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()Lcom/google/maps/api/android/lib6/b/h;
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dz;->f:Lcom/google/maps/api/android/lib6/b/h;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dz;->b:Landroid/content/Context;

    invoke-static {}, Lcom/google/maps/api/android/lib6/c/m;->a()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/c/dz;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/google/maps/api/android/lib6/b/e;->a(Landroid/content/Context;)Lcom/google/maps/api/android/lib6/b/e;

    new-instance v3, Lcom/google/maps/api/android/lib6/b/i;

    invoke-direct {v3}, Lcom/google/maps/api/android/lib6/b/i;-><init>()V

    invoke-static {}, Lcom/google/maps/api/android/lib6/gmm6/f;->a()Lcom/google/maps/api/android/lib6/gmm6/f;

    invoke-static {}, Lcom/google/maps/api/android/lib6/gmm6/f;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/maps/api/android/lib6/b/i;->a(Ljava/lang/String;)Lcom/google/maps/api/android/lib6/b/i;

    move-result-object v3

    invoke-static {}, Lcom/google/maps/api/android/lib6/b/e;->q()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/maps/api/android/lib6/b/i;->b(Ljava/lang/String;)Lcom/google/maps/api/android/lib6/b/i;

    move-result-object v3

    const-string v4, "2.12.0"

    invoke-virtual {v3, v4}, Lcom/google/maps/api/android/lib6/b/i;->c(Ljava/lang/String;)Lcom/google/maps/api/android/lib6/b/i;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/maps/api/android/lib6/b/i;->a()Lcom/google/maps/api/android/lib6/b/i;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/maps/api/android/lib6/b/i;->b()Lcom/google/maps/api/android/lib6/b/i;

    move-result-object v3

    invoke-static {v0}, Lcom/google/maps/api/android/lib6/b/ae;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/maps/api/android/lib6/b/i;->d(Ljava/lang/String;)Lcom/google/maps/api/android/lib6/b/i;

    move-result-object v3

    invoke-static {}, Lcom/google/maps/api/android/lib6/gmm6/f;->a()Lcom/google/maps/api/android/lib6/gmm6/f;

    invoke-static {}, Lcom/google/maps/api/android/lib6/gmm6/f;->b()Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/maps/api/android/lib6/b/i;->a(Z)Lcom/google/maps/api/android/lib6/b/i;

    move-result-object v3

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-virtual {v3, v1}, Lcom/google/maps/api/android/lib6/b/i;->a(I)Lcom/google/maps/api/android/lib6/b/i;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/google/maps/api/android/lib6/b/i;->e(Ljava/lang/String;)Lcom/google/maps/api/android/lib6/b/i;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/maps/api/android/lib6/b/i;->c()Lcom/google/maps/api/android/lib6/b/h;

    move-result-object v1

    new-instance v2, Lcom/google/maps/api/android/lib6/c/eb;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/google/maps/api/android/lib6/c/eb;-><init>(B)V

    invoke-virtual {v1, v2}, Lcom/google/maps/api/android/lib6/b/h;->a(Lcom/google/maps/api/android/lib6/b/p;)V

    invoke-virtual {v1}, Lcom/google/maps/api/android/lib6/b/h;->m()V

    invoke-static {}, Lcom/google/maps/api/android/lib6/b/e;->e()Lcom/google/maps/api/android/lib6/b/e;

    invoke-static {}, Lcom/google/maps/api/android/lib6/b/e;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/maps/api/android/lib6/b/h;->b(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/maps/api/android/lib6/gmm6/c/g;->a(Landroid/content/Context;)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/maps/api/android/lib6/b/h;->c(Ljava/lang/String;)V

    const-string v0, "SYSTEM"

    invoke-virtual {v1, v0}, Lcom/google/maps/api/android/lib6/b/h;->a(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/maps/api/android/lib6/b/h;->n()V

    invoke-virtual {v1}, Lcom/google/maps/api/android/lib6/b/h;->d()V

    invoke-virtual {v1}, Lcom/google/maps/api/android/lib6/b/h;->d()V

    iput-object v1, p0, Lcom/google/maps/api/android/lib6/c/dz;->f:Lcom/google/maps/api/android/lib6/b/h;

    :cond_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dz;->f:Lcom/google/maps/api/android/lib6/b/h;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/maps/api/android/lib6/c/ee;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    const-string v0, "Listener is null."

    invoke-static {p1, v0}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dz;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/dz;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/dz;->e:Z

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dz;->b:Landroid/content/Context;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Z)Z
    .locals 4

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dz;->b:Landroid/content/Context;

    const-string v1, "android.permission.ACCESS_NETWORK_STATE"

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v2

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return p1

    :cond_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dz;->b:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    goto :goto_0
.end method

.method public final declared-synchronized b()Lcom/android/volley/s;
    .locals 15

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dz;->d:Lcom/android/volley/s;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dz;->b:Landroid/content/Context;

    new-instance v1, Ljava/io/File;

    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "com.google.android.gms.maps.volley"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/maps/api/android/lib6/b/e;->a(Landroid/content/Context;)Lcom/google/maps/api/android/lib6/b/e;

    const-string v2, "Android"

    sget-object v3, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {}, Lcom/google/maps/api/android/lib6/b/e;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "android"

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "2.12.0"

    invoke-static {}, Lcom/google/maps/api/android/lib6/gmm6/f;->a()Lcom/google/maps/api/android/lib6/gmm6/f;

    invoke-static {}, Lcom/google/maps/api/android/lib6/gmm6/f;->b()Z

    const-string v8, "Mobile"

    invoke-static {}, Lcom/google/maps/api/android/lib6/b/e;->q()Ljava/lang/String;

    move-result-object v9

    invoke-static {}, Lcom/google/maps/api/android/lib6/b/h;->p()Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Mozilla/5.0 (%s; U; %s; %s; ) AppleWebKit/0.0 (KHTML, like Gecko) Version/0.0; GmmClient:%s/%s/%s/%s/%s/%s"

    const/16 v13, 0x9

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v2, v13, v14

    const/4 v2, 0x1

    aput-object v3, v13, v2

    const/4 v2, 0x2

    aput-object v4, v13, v2

    const/4 v2, 0x3

    aput-object v5, v13, v2

    const/4 v2, 0x4

    aput-object v6, v13, v2

    const/4 v2, 0x5

    aput-object v7, v13, v2

    const/4 v2, 0x6

    aput-object v8, v13, v2

    const/4 v2, 0x7

    aput-object v9, v13, v2

    const/16 v2, 0x8

    aput-object v10, v13, v2

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v11, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/maps/api/android/lib6/b/e;->g()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-static {v0}, Lcom/google/maps/api/android/lib6/gmm6/c/g;->a(Landroid/content/Context;)Landroid/content/pm/PackageInfo;

    move-result-object v4

    iget v4, v4, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "/%s/%s/%s"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    const/4 v3, 0x1

    aput-object v4, v6, v3

    const/4 v3, 0x2

    aput-object v2, v6, v3

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/maps/api/android/lib6/c/ea;

    invoke-direct {v3, v2}, Lcom/google/maps/api/android/lib6/c/ea;-><init>(Ljava/lang/String;)V

    new-instance v2, Lcom/android/volley/toolbox/a;

    invoke-direct {v2, v3}, Lcom/android/volley/toolbox/a;-><init>(Lcom/android/volley/toolbox/j;)V

    new-instance v3, Lcom/android/volley/toolbox/d;

    const/high16 v4, 0x1400000

    invoke-direct {v3, v1, v4}, Lcom/android/volley/toolbox/d;-><init>(Ljava/io/File;I)V

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v0

    mul-int/lit16 v0, v0, 0x400

    mul-int/lit16 v0, v0, 0x400

    div-int/lit8 v0, v0, 0x8

    new-instance v1, Lcom/google/maps/api/android/lib6/c/dh;

    invoke-direct {v1, v0}, Lcom/google/maps/api/android/lib6/c/dh;-><init>(I)V

    new-instance v0, Lcom/android/volley/g;

    new-instance v4, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v0, v4}, Lcom/android/volley/g;-><init>(Landroid/os/Handler;)V

    new-instance v4, Lcom/google/maps/api/android/lib6/c/dg;

    invoke-direct {v4, v3, v2, v1, v0}, Lcom/google/maps/api/android/lib6/c/dg;-><init>(Lcom/android/volley/b;Lcom/android/volley/j;Landroid/support/v4/g/h;Lcom/android/volley/y;)V

    iput-object v4, p0, Lcom/google/maps/api/android/lib6/c/dz;->d:Lcom/android/volley/s;

    iget v0, p0, Lcom/google/maps/api/android/lib6/c/dz;->g:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dz;->d:Lcom/android/volley/s;

    invoke-virtual {v0}, Lcom/android/volley/s;->a()V

    :cond_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dz;->d:Lcom/android/volley/s;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()V
    .locals 1

    iget v0, p0, Lcom/google/maps/api/android/lib6/c/dz;->g:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dz;->d:Lcom/android/volley/s;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dz;->d:Lcom/android/volley/s;

    invoke-virtual {v0}, Lcom/android/volley/s;->a()V

    :cond_0
    iget v0, p0, Lcom/google/maps/api/android/lib6/c/dz;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/maps/api/android/lib6/c/dz;->g:I

    return-void
.end method

.method public final d()V
    .locals 1

    iget v0, p0, Lcom/google/maps/api/android/lib6/c/dz;->g:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/maps/api/android/lib6/c/dz;->g:I

    iget v0, p0, Lcom/google/maps/api/android/lib6/c/dz;->g:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dz;->d:Lcom/android/volley/s;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dz;->d:Lcom/android/volley/s;

    invoke-virtual {v0}, Lcom/android/volley/s;->b()V

    :cond_0
    return-void
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/c/dz;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/maps/api/android/lib6/c/dz;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v1, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/dz;->c:Ljava/util/Set;

    invoke-static {v0}, Lcom/google/k/c/ib;->a(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v0

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/dz;->c:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-direct {p0}, Lcom/google/maps/api/android/lib6/c/dz;->e()V

    :cond_0
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/api/android/lib6/c/ee;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/ee;->a()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_3
    invoke-direct {p0}, Lcom/google/maps/api/android/lib6/c/dz;->e()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    return-void
.end method
