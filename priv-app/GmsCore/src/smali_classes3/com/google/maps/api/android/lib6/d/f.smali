.class final Lcom/google/maps/api/android/lib6/d/f;
.super Ljava/lang/Object;


# instance fields
.field a:Landroid/graphics/Bitmap;

.field b:Lcom/google/maps/api/android/lib6/d/p;

.field private final c:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/d/f;->c:Landroid/view/View;

    return-void
.end method

.method static a(Landroid/graphics/Canvas;)V
    .locals 14

    const/4 v6, 0x0

    const/4 v2, 0x0

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {p0}, Landroid/graphics/Canvas;->getWidth()I

    move-result v12

    invoke-virtual {p0}, Landroid/graphics/Canvas;->getHeight()I

    move-result v13

    move v7, v6

    :goto_0
    if-ge v7, v12, :cond_0

    int-to-float v1, v7

    int-to-float v3, v7

    int-to-float v4, v13

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    add-int/lit8 v0, v7, 0xf

    move v7, v0

    goto :goto_0

    :cond_0
    move v0, v6

    :goto_1
    if-ge v0, v13, :cond_1

    int-to-float v8, v0

    int-to-float v9, v12

    int-to-float v10, v0

    move-object v6, p0

    move v7, v2

    move-object v11, v5

    invoke-virtual/range {v6 .. v11}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    add-int/lit8 v0, v0, 0xf

    goto :goto_1

    :cond_1
    return-void
.end method


# virtual methods
.method final a(Landroid/graphics/Bitmap;Lcom/google/maps/api/android/lib6/d/p;)V
    .locals 1

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/d/f;->a:Landroid/graphics/Bitmap;

    iput-object p2, p0, Lcom/google/maps/api/android/lib6/d/f;->b:Lcom/google/maps/api/android/lib6/d/p;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/f;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    return-void
.end method
