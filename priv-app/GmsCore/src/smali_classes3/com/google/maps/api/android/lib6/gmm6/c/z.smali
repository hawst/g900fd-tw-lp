.class public final Lcom/google/maps/api/android/lib6/gmm6/c/z;
.super Lcom/google/maps/api/android/lib6/c/a;


# direct methods
.method public constructor <init>(Lcom/google/maps/api/android/lib6/gmm6/c/aq;Landroid/view/View;Landroid/view/View;ZLjava/util/concurrent/Executor;)V
    .locals 6

    move-object v1, p1

    check-cast v1, Landroid/view/View;

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/maps/api/android/lib6/c/a;-><init>(Landroid/view/View;Landroid/view/View;Landroid/view/View;ZLjava/util/concurrent/Executor;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/maps/internal/cc;Landroid/graphics/Bitmap;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/google/maps/api/android/lib6/gmm6/c/z;->b(Lcom/google/android/gms/maps/internal/cc;Landroid/graphics/Bitmap;)V

    return-void
.end method


# virtual methods
.method protected final a(Landroid/graphics/Bitmap;Lcom/google/android/gms/maps/internal/cc;)V
    .locals 3

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/z;->a:Landroid/view/View;

    check-cast v0, Lcom/google/maps/api/android/lib6/gmm6/c/aq;

    invoke-interface {v0, p1}, Lcom/google/maps/api/android/lib6/gmm6/c/aq;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0}, Lcom/google/maps/api/android/lib6/gmm6/h;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/maps/api/android/lib6/gmm6/c/z;->a(Landroid/graphics/Bitmap;Z)V

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/z;->b:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/google/maps/api/android/lib6/gmm6/c/aa;

    invoke-direct {v2, p0, p2, v0}, Lcom/google/maps/api/android/lib6/gmm6/c/aa;-><init>(Lcom/google/maps/api/android/lib6/gmm6/c/z;Lcom/google/android/gms/maps/internal/cc;Landroid/graphics/Bitmap;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
