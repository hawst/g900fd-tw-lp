.class final Lcom/google/maps/api/android/lib6/c/bb;
.super Ljava/lang/Object;


# static fields
.field private static a:Lcom/google/maps/api/android/lib6/c/bb;

.field private static final b:Ljava/lang/Object;


# instance fields
.field private final c:Lcom/google/maps/api/android/lib6/b/o;

.field private final d:Lcom/google/maps/api/android/lib6/c/bd;

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/maps/api/android/lib6/c/bb;->b:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Lcom/google/maps/api/android/lib6/b/o;Lcom/google/maps/api/android/lib6/c/bd;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/c/bb;->c:Lcom/google/maps/api/android/lib6/b/o;

    iput-object p2, p0, Lcom/google/maps/api/android/lib6/c/bb;->d:Lcom/google/maps/api/android/lib6/c/bd;

    return-void
.end method

.method static a(Landroid/content/Context;)Lcom/google/maps/api/android/lib6/c/bb;
    .locals 6

    sget-object v1, Lcom/google/maps/api/android/lib6/c/bb;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/maps/api/android/lib6/c/bb;->a:Lcom/google/maps/api/android/lib6/c/bb;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/maps/api/android/lib6/c/bb;

    invoke-static {}, Lcom/google/maps/api/android/lib6/b/h;->a()Lcom/google/maps/api/android/lib6/b/h;

    move-result-object v2

    new-instance v3, Lcom/google/maps/api/android/lib6/c/bd;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "com.google.android.gms.maps._m_u"

    invoke-direct {v3, v4, v5}, Lcom/google/maps/api/android/lib6/c/bd;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-direct {v0, v2, v3}, Lcom/google/maps/api/android/lib6/c/bb;-><init>(Lcom/google/maps/api/android/lib6/b/o;Lcom/google/maps/api/android/lib6/c/bd;)V

    sput-object v0, Lcom/google/maps/api/android/lib6/c/bb;->a:Lcom/google/maps/api/android/lib6/c/bb;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object v0, Lcom/google/maps/api/android/lib6/c/bb;->a:Lcom/google/maps/api/android/lib6/c/bb;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/google/maps/api/android/lib6/c/bb;)Lcom/google/maps/api/android/lib6/c/bd;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bb;->d:Lcom/google/maps/api/android/lib6/c/bd;

    return-object v0
.end method

.method static a(Lcom/google/p/a/b/b/f;I)V
    .locals 4

    const/4 v3, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v3}, Lcom/google/p/a/b/b/f;->j(I)I

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v2

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_0

    invoke-virtual {p0, v3, v1}, Lcom/google/p/a/b/b/f;->e(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/google/maps/api/android/lib6/c/bb;)Lcom/google/maps/api/android/lib6/b/o;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bb;->c:Lcom/google/maps/api/android/lib6/b/o;

    return-object v0
.end method

.method private declared-synchronized b()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/bb;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/bb;->e:Z

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bb;->c:Lcom/google/maps/api/android/lib6/b/o;

    new-instance v1, Lcom/google/maps/api/android/lib6/c/bc;

    invoke-direct {v1, p0}, Lcom/google/maps/api/android/lib6/c/bc;-><init>(Lcom/google/maps/api/android/lib6/c/bb;)V

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/b/o;->c(Lcom/google/maps/api/android/lib6/b/g;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic c(Lcom/google/maps/api/android/lib6/c/bb;)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/bb;->e:Z

    return v0
.end method


# virtual methods
.method final declared-synchronized a()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/bb;->d:Lcom/google/maps/api/android/lib6/c/bd;

    sget-object v1, Lcom/google/af/d/a/a/g;->c:Lcom/google/p/a/b/b/h;

    invoke-virtual {v0, v1}, Lcom/google/maps/api/android/lib6/c/bd;->a(Lcom/google/p/a/b/b/h;)Lcom/google/p/a/b/b/f;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/p/a/b/b/f;

    sget-object v1, Lcom/google/af/d/a/a/g;->c:Lcom/google/p/a/b/b/h;

    invoke-direct {v0, v1}, Lcom/google/p/a/b/b/f;-><init>(Lcom/google/p/a/b/b/h;)V

    :cond_0
    new-instance v1, Lcom/google/p/a/b/b/f;

    sget-object v2, Lcom/google/af/d/a/a/g;->d:Lcom/google/p/a/b/b/h;

    invoke-direct {v1, v2}, Lcom/google/p/a/b/b/f;-><init>(Lcom/google/p/a/b/b/h;)V

    const/4 v2, 0x1

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/google/p/a/b/b/f;->a(II)V

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/google/p/a/b/b/f;->a(ILcom/google/p/a/b/b/f;)V

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/bb;->d:Lcom/google/maps/api/android/lib6/c/bd;

    invoke-virtual {v1, v0}, Lcom/google/maps/api/android/lib6/c/bd;->a(Lcom/google/p/a/b/b/f;)V

    invoke-direct {p0}, Lcom/google/maps/api/android/lib6/c/bb;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
