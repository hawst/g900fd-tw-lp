.class public Lcom/google/maps/api/android/lib6/c/dl;
.super Lcom/google/android/gms/maps/internal/c;


# static fields
.field private static final a:Landroid/os/Bundle;

.field private static final b:Lcom/google/maps/api/android/lib6/c/dk;

.field private static final c:Lcom/google/maps/api/android/lib6/c/dk;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lcom/google/maps/api/android/lib6/c/dl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    new-instance v0, Landroid/os/Bundle;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    sput-object v0, Lcom/google/maps/api/android/lib6/c/dl;->a:Landroid/os/Bundle;

    new-instance v0, Lcom/google/maps/api/android/lib6/c/dm;

    invoke-direct {v0}, Lcom/google/maps/api/android/lib6/c/dm;-><init>()V

    sput-object v0, Lcom/google/maps/api/android/lib6/c/dl;->b:Lcom/google/maps/api/android/lib6/c/dk;

    new-instance v0, Lcom/google/maps/api/android/lib6/c/dr;

    invoke-direct {v0}, Lcom/google/maps/api/android/lib6/c/dr;-><init>()V

    sput-object v0, Lcom/google/maps/api/android/lib6/c/dl;->c:Lcom/google/maps/api/android/lib6/c/dk;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/maps/internal/c;-><init>()V

    return-void
.end method

.method private static a(Landroid/os/Bundle;I)V
    .locals 4

    invoke-virtual {p0}, Landroid/os/Bundle;->size()I

    move-result v1

    if-ne v1, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Wrong number of parameters: expected "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " but found "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/b/l;
    .locals 1

    sget-object v0, Lcom/google/maps/api/android/lib6/c/dl;->b:Lcom/google/maps/api/android/lib6/c/dk;

    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(F)Lcom/google/android/gms/b/l;
    .locals 1

    new-instance v0, Lcom/google/maps/api/android/lib6/c/ds;

    invoke-direct {v0, p1}, Lcom/google/maps/api/android/lib6/c/ds;-><init>(F)V

    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(FF)Lcom/google/android/gms/b/l;
    .locals 1

    new-instance v0, Lcom/google/maps/api/android/lib6/c/dv;

    invoke-direct {v0, p1, p2}, Lcom/google/maps/api/android/lib6/c/dv;-><init>(FF)V

    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(FII)Lcom/google/android/gms/b/l;
    .locals 1

    new-instance v0, Lcom/google/maps/api/android/lib6/c/du;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/maps/api/android/lib6/c/du;-><init>(FII)V

    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/maps/model/CameraPosition;)Lcom/google/android/gms/b/l;
    .locals 1

    new-instance v0, Lcom/google/maps/api/android/lib6/c/dw;

    invoke-direct {v0, p1}, Lcom/google/maps/api/android/lib6/c/dw;-><init>(Lcom/google/android/gms/maps/model/CameraPosition;)V

    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/b/l;
    .locals 1

    new-instance v0, Lcom/google/maps/api/android/lib6/c/dn;

    invoke-direct {v0, p1}, Lcom/google/maps/api/android/lib6/c/dn;-><init>(Lcom/google/android/gms/maps/model/LatLng;)V

    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLng;F)Lcom/google/android/gms/b/l;
    .locals 1

    new-instance v0, Lcom/google/maps/api/android/lib6/c/do;

    invoke-direct {v0, p1, p2}, Lcom/google/maps/api/android/lib6/c/do;-><init>(Lcom/google/android/gms/maps/model/LatLng;F)V

    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLngBounds;I)Lcom/google/android/gms/b/l;
    .locals 1

    new-instance v0, Lcom/google/maps/api/android/lib6/c/dp;

    invoke-direct {v0, p1, p2}, Lcom/google/maps/api/android/lib6/c/dp;-><init>(Lcom/google/android/gms/maps/model/LatLngBounds;I)V

    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLngBounds;III)Lcom/google/android/gms/b/l;
    .locals 1

    new-instance v0, Lcom/google/maps/api/android/lib6/c/dq;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/google/maps/api/android/lib6/c/dq;-><init>(Lcom/google/android/gms/maps/model/LatLngBounds;III)V

    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/maps/model/internal/CameraUpdateParcelable;)Lcom/google/android/gms/b/l;
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/internal/CameraUpdateParcelable;->b()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/internal/CameraUpdateParcelable;->c()Landroid/os/Bundle;

    move-result-object v1

    const-class v2, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    invoke-static {v1, v5}, Lcom/google/maps/api/android/lib6/c/dl;->a(Landroid/os/Bundle;I)V

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/dl;->a()Lcom/google/android/gms/b/l;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-static {v1, v5}, Lcom/google/maps/api/android/lib6/c/dl;->a(Landroid/os/Bundle;I)V

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/dl;->b()Lcom/google/android/gms/b/l;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    invoke-static {v1, v3}, Lcom/google/maps/api/android/lib6/c/dl;->a(Landroid/os/Bundle;I)V

    const-string v0, "zoomLevel"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/c/dl;->a(F)Lcom/google/android/gms/b/l;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    invoke-static {v1, v3}, Lcom/google/maps/api/android/lib6/c/dl;->a(Landroid/os/Bundle;I)V

    const-string v0, "amount"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/c/dl;->b(F)Lcom/google/android/gms/b/l;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x3

    invoke-static {v1, v0}, Lcom/google/maps/api/android/lib6/c/dl;->a(Landroid/os/Bundle;I)V

    const-string v0, "amount"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    const-string v2, "screenFocusX"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const-string v3, "screenFocusY"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v0, v2, v1}, Lcom/google/maps/api/android/lib6/c/dl;->a(FII)Lcom/google/android/gms/b/l;

    move-result-object v0

    goto :goto_0

    :pswitch_5
    invoke-static {v1, v4}, Lcom/google/maps/api/android/lib6/c/dl;->a(Landroid/os/Bundle;I)V

    const-string v0, "xPixel"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    const-string v2, "yPixel"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/maps/api/android/lib6/c/dl;->a(FF)Lcom/google/android/gms/b/l;

    move-result-object v0

    goto :goto_0

    :pswitch_6
    invoke-static {v1, v3}, Lcom/google/maps/api/android/lib6/c/dl;->a(Landroid/os/Bundle;I)V

    const-string v0, "cameraPosition"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/CameraPosition;

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/c/dl;->a(Lcom/google/android/gms/maps/model/CameraPosition;)Lcom/google/android/gms/b/l;

    move-result-object v0

    goto :goto_0

    :pswitch_7
    invoke-static {v1, v3}, Lcom/google/maps/api/android/lib6/c/dl;->a(Landroid/os/Bundle;I)V

    const-string v0, "latLng"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/c/dl;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/b/l;

    move-result-object v0

    goto :goto_0

    :pswitch_8
    invoke-static {v1, v4}, Lcom/google/maps/api/android/lib6/c/dl;->a(Landroid/os/Bundle;I)V

    const-string v0, "latLng"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/LatLng;

    const-string v2, "zoomLevel"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/maps/api/android/lib6/c/dl;->a(Lcom/google/android/gms/maps/model/LatLng;F)Lcom/google/android/gms/b/l;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_9
    invoke-static {v1, v4}, Lcom/google/maps/api/android/lib6/c/dl;->a(Landroid/os/Bundle;I)V

    const-string v0, "latLngBounds"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/LatLngBounds;

    const-string v2, "padding"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/maps/api/android/lib6/c/dl;->a(Lcom/google/android/gms/maps/model/LatLngBounds;I)Lcom/google/android/gms/b/l;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_a
    const/4 v0, 0x4

    invoke-static {v1, v0}, Lcom/google/maps/api/android/lib6/c/dl;->a(Landroid/os/Bundle;I)V

    const-string v0, "latLngBounds"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/LatLngBounds;

    const-string v2, "width"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const-string v3, "height"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    const-string v4, "padding"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v0, v2, v3, v1}, Lcom/google/maps/api/android/lib6/c/dl;->a(Lcom/google/android/gms/maps/model/LatLngBounds;III)Lcom/google/android/gms/b/l;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public final b()Lcom/google/android/gms/b/l;
    .locals 1

    sget-object v0, Lcom/google/maps/api/android/lib6/c/dl;->c:Lcom/google/maps/api/android/lib6/c/dk;

    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final b(F)Lcom/google/android/gms/b/l;
    .locals 1

    new-instance v0, Lcom/google/maps/api/android/lib6/c/dt;

    invoke-direct {v0, p1}, Lcom/google/maps/api/android/lib6/c/dt;-><init>(F)V

    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v0

    return-object v0
.end method
