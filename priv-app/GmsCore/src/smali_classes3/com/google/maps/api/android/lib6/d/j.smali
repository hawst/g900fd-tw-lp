.class public final Lcom/google/maps/api/android/lib6/d/j;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/maps/api/android/lib6/c/ak;


# instance fields
.field final a:Lcom/google/maps/api/android/lib6/c/aj;

.field b:Landroid/graphics/Paint;

.field c:Landroid/graphics/Point;

.field d:Landroid/graphics/Point;

.field private final e:Lcom/google/maps/api/android/lib6/d/m;


# direct methods
.method public constructor <init>(Lcom/google/maps/api/android/lib6/c/aj;Lcom/google/maps/api/android/lib6/d/m;)V
    .locals 3

    const/4 v2, -0x1

    const/4 v1, -0x2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/d/j;->b:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v1, v1}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/d/j;->c:Landroid/graphics/Point;

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v2, v2}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/d/j;->d:Landroid/graphics/Point;

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/d/j;->a:Lcom/google/maps/api/android/lib6/c/aj;

    iput-object p2, p0, Lcom/google/maps/api/android/lib6/d/j;->e:Lcom/google/maps/api/android/lib6/d/m;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/j;->e:Lcom/google/maps/api/android/lib6/d/m;

    iget-object v1, v0, Lcom/google/maps/api/android/lib6/d/m;->b:Ljava/util/List;

    invoke-interface {v1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/d/m;->d:Lcom/google/maps/api/android/lib6/d/i;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/d/i;->invalidate()V

    return-void
.end method

.method public final a(I)V
    .locals 1

    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/j;->e:Lcom/google/maps/api/android/lib6/d/m;

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/d/m;->d:Lcom/google/maps/api/android/lib6/d/i;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/d/i;->invalidate()V

    :cond_0
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/j;->e:Lcom/google/maps/api/android/lib6/d/m;

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/d/m;->d:Lcom/google/maps/api/android/lib6/d/i;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/d/i;->invalidate()V

    :cond_1
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/j;->e:Lcom/google/maps/api/android/lib6/d/m;

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/d/m;->d:Lcom/google/maps/api/android/lib6/d/i;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/d/i;->invalidate()V

    :cond_2
    and-int/lit8 v0, p1, 0x40

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/j;->a:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->C()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/j;->e:Lcom/google/maps/api/android/lib6/d/m;

    invoke-virtual {v0, p0}, Lcom/google/maps/api/android/lib6/d/m;->a(Lcom/google/maps/api/android/lib6/d/j;)V

    :cond_3
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/j;->e:Lcom/google/maps/api/android/lib6/d/m;

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/d/m;->d:Lcom/google/maps/api/android/lib6/d/i;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/d/i;->invalidate()V

    :cond_4
    and-int/lit16 v0, p1, 0x400

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/j;->e:Lcom/google/maps/api/android/lib6/d/m;

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/d/m;->d:Lcom/google/maps/api/android/lib6/d/i;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/d/i;->invalidate()V

    :cond_5
    and-int/lit16 v0, p1, 0x200

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/j;->e:Lcom/google/maps/api/android/lib6/d/m;

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/d/m;->d:Lcom/google/maps/api/android/lib6/d/i;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/d/i;->invalidate()V

    :cond_6
    and-int/lit16 v0, p1, 0x80

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/j;->e:Lcom/google/maps/api/android/lib6/d/m;

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/d/m;->d:Lcom/google/maps/api/android/lib6/d/i;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/d/i;->invalidate()V

    :cond_7
    and-int/lit16 v0, p1, 0x100

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/j;->e:Lcom/google/maps/api/android/lib6/d/m;

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/d/m;->d:Lcom/google/maps/api/android/lib6/d/i;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/d/i;->invalidate()V

    :cond_8
    return-void
.end method

.method public final b()V
    .locals 3

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/j;->e:Lcom/google/maps/api/android/lib6/d/m;

    iget-object v1, v0, Lcom/google/maps/api/android/lib6/d/m;->b:Ljava/util/List;

    invoke-interface {v1, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    iget-object v1, v0, Lcom/google/maps/api/android/lib6/d/m;->e:Lcom/google/maps/api/android/lib6/c/aj;

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/d/j;->a:Lcom/google/maps/api/android/lib6/c/aj;

    if-ne v1, v2, :cond_0

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/maps/api/android/lib6/d/m;->e:Lcom/google/maps/api/android/lib6/c/aj;

    :cond_0
    iget-object v0, v0, Lcom/google/maps/api/android/lib6/d/m;->d:Lcom/google/maps/api/android/lib6/d/i;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/d/i;->invalidate()V

    return-void
.end method

.method public final c()V
    .locals 3

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/j;->e:Lcom/google/maps/api/android/lib6/d/m;

    iget-object v1, v0, Lcom/google/maps/api/android/lib6/d/m;->e:Lcom/google/maps/api/android/lib6/c/aj;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/maps/api/android/lib6/d/m;->e:Lcom/google/maps/api/android/lib6/c/aj;

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/d/j;->a:Lcom/google/maps/api/android/lib6/c/aj;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/d/j;->a:Lcom/google/maps/api/android/lib6/c/aj;

    iput-object v1, v0, Lcom/google/maps/api/android/lib6/d/m;->e:Lcom/google/maps/api/android/lib6/c/aj;

    :cond_0
    iget-object v0, v0, Lcom/google/maps/api/android/lib6/d/m;->d:Lcom/google/maps/api/android/lib6/d/i;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/d/i;->invalidate()V

    return-void
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/j;->e:Lcom/google/maps/api/android/lib6/d/m;

    invoke-virtual {v0, p0}, Lcom/google/maps/api/android/lib6/d/m;->a(Lcom/google/maps/api/android/lib6/d/j;)V

    return-void
.end method

.method public final e()V
    .locals 0

    return-void
.end method

.method public final f()Z
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/j;->e:Lcom/google/maps/api/android/lib6/d/m;

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/d/m;->e:Lcom/google/maps/api/android/lib6/c/aj;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/d/j;->a:Lcom/google/maps/api/android/lib6/c/aj;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Landroid/graphics/Rect;
    .locals 5

    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/d/j;->c:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/d/j;->c:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    iget-object v3, p0, Lcom/google/maps/api/android/lib6/d/j;->d:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    iget-object v4, p0, Lcom/google/maps/api/android/lib6/d/j;->d:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v0
.end method
