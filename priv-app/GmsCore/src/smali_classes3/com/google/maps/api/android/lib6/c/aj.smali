.class public final Lcom/google/maps/api/android/lib6/c/aj;
.super Lcom/google/android/gms/maps/model/internal/t;


# static fields
.field private static final a:Lcom/google/android/gms/maps/model/MarkerOptions;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Lcom/google/maps/api/android/lib6/c/al;

.field private final d:Lcom/google/maps/api/android/lib6/c/db;

.field private final e:Lcom/google/maps/api/android/lib6/c/cs;

.field private final f:Lcom/google/maps/api/android/lib6/c/cd;

.field private final g:Lcom/google/maps/api/android/lib6/c/bz;

.field private h:Lcom/google/maps/api/android/lib6/c/ak;

.field private i:Lcom/google/android/gms/maps/model/LatLng;

.field private j:Lcom/google/maps/api/android/lib6/c/ct;

.field private k:F

.field private l:F

.field private m:Z

.field private n:F

.field private o:Z

.field private p:Z

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:F

.field private t:F

.field private u:Z

.field private v:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v0}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    sput-object v0, Lcom/google/maps/api/android/lib6/c/aj;->a:Lcom/google/android/gms/maps/model/MarkerOptions;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/gms/maps/model/MarkerOptions;Lcom/google/maps/api/android/lib6/c/al;Lcom/google/maps/api/android/lib6/c/db;Lcom/google/maps/api/android/lib6/c/cs;Lcom/google/maps/api/android/lib6/c/bz;Lcom/google/maps/api/android/lib6/c/cd;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/gms/maps/model/internal/t;-><init>()V

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/c/aj;->b:Ljava/lang/String;

    invoke-static {p3}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/api/android/lib6/c/al;

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->c:Lcom/google/maps/api/android/lib6/c/al;

    iput-object p4, p0, Lcom/google/maps/api/android/lib6/c/aj;->d:Lcom/google/maps/api/android/lib6/c/db;

    iput-object p5, p0, Lcom/google/maps/api/android/lib6/c/aj;->e:Lcom/google/maps/api/android/lib6/c/cs;

    iput-object p7, p0, Lcom/google/maps/api/android/lib6/c/aj;->f:Lcom/google/maps/api/android/lib6/c/cd;

    iput-object p6, p0, Lcom/google/maps/api/android/lib6/c/aj;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/MarkerOptions;->d()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    invoke-static {v0}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/LatLng;

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->i:Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/MarkerOptions;->g()Lcom/google/android/gms/maps/model/a;

    move-result-object v0

    invoke-static {v0}, Lcom/google/maps/api/android/lib6/c/ct;->a(Lcom/google/android/gms/maps/model/a;)Lcom/google/maps/api/android/lib6/c/ct;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->j:Lcom/google/maps/api/android/lib6/c/ct;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->d:Lcom/google/maps/api/android/lib6/c/db;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/aj;->j:Lcom/google/maps/api/android/lib6/c/ct;

    invoke-virtual {v0, v1}, Lcom/google/maps/api/android/lib6/c/db;->a(Lcom/google/maps/api/android/lib6/c/ct;)V

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/MarkerOptions;->h()F

    move-result v0

    iput v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->k:F

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/MarkerOptions;->i()F

    move-result v0

    iput v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->l:F

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/MarkerOptions;->l()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->m:Z

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/MarkerOptions;->m()F

    move-result v0

    iput v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->n:F

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/MarkerOptions;->p()F

    move-result v0

    iput v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->v:F

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/MarkerOptions;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->q:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/MarkerOptions;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->r:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/MarkerOptions;->j()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->o:Z

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/MarkerOptions;->k()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->p:Z

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/MarkerOptions;->n()F

    move-result v0

    iput v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->s:F

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/MarkerOptions;->o()F

    move-result v0

    iput v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->t:F

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/MarkerOptions;->h()F

    move-result v0

    sget-object v1, Lcom/google/maps/api/android/lib6/c/aj;->a:Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/MarkerOptions;->h()F

    move-result v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/MarkerOptions;->i()F

    move-result v0

    sget-object v1, Lcom/google/maps/api/android/lib6/c/aj;->a:Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/MarkerOptions;->i()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->f:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->i:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    :cond_1
    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/MarkerOptions;->n()F

    move-result v0

    sget-object v1, Lcom/google/maps/api/android/lib6/c/aj;->a:Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/MarkerOptions;->n()F

    move-result v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/MarkerOptions;->o()F

    move-result v0

    sget-object v1, Lcom/google/maps/api/android/lib6/c/aj;->a:Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/MarkerOptions;->o()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->f:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->n:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    :cond_3
    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/MarkerOptions;->g()Lcom/google/android/gms/maps/model/a;

    move-result-object v0

    sget-object v1, Lcom/google/maps/api/android/lib6/c/aj;->a:Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/MarkerOptions;->g()Lcom/google/android/gms/maps/model/a;

    move-result-object v1

    if-eq v0, v1, :cond_4

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->f:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->h:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    :cond_4
    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/MarkerOptions;->e()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/maps/api/android/lib6/c/aj;->a:Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/MarkerOptions;->e()Ljava/lang/String;

    move-result-object v1

    if-eq v0, v1, :cond_5

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->f:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->f:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    :cond_5
    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/MarkerOptions;->f()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/maps/api/android/lib6/c/aj;->a:Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/MarkerOptions;->f()Ljava/lang/String;

    move-result-object v1

    if-eq v0, v1, :cond_6

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->f:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->g:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    :cond_6
    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/MarkerOptions;->j()Z

    move-result v0

    sget-object v1, Lcom/google/maps/api/android/lib6/c/aj;->a:Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/MarkerOptions;->j()Z

    move-result v1

    if-eq v0, v1, :cond_7

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->f:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->j:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    :cond_7
    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/MarkerOptions;->k()Z

    move-result v0

    sget-object v1, Lcom/google/maps/api/android/lib6/c/aj;->a:Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/MarkerOptions;->k()Z

    move-result v1

    if-eq v0, v1, :cond_8

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->f:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->k:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    :cond_8
    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/MarkerOptions;->l()Z

    move-result v0

    sget-object v1, Lcom/google/maps/api/android/lib6/c/aj;->a:Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/MarkerOptions;->l()Z

    move-result v1

    if-eq v0, v1, :cond_9

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->f:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->l:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    :cond_9
    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/MarkerOptions;->m()F

    move-result v0

    sget-object v1, Lcom/google/maps/api/android/lib6/c/aj;->a:Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/MarkerOptions;->m()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->f:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->m:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    :cond_a
    invoke-virtual {p2}, Lcom/google/android/gms/maps/model/MarkerOptions;->p()F

    move-result v0

    sget-object v1, Lcom/google/maps/api/android/lib6/c/aj;->a:Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/MarkerOptions;->p()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->f:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->o:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    :cond_b
    return-void
.end method

.method private a(I)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->u:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->c:Lcom/google/maps/api/android/lib6/c/al;

    invoke-virtual {v0, p0, p1}, Lcom/google/maps/api/android/lib6/c/al;->a(Lcom/google/maps/api/android/lib6/c/aj;I)V

    goto :goto_0
.end method


# virtual methods
.method public final A()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final declared-synchronized B()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->o:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized C()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->p:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized D()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->m:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized E()F
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->n:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized F()F
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->v:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final G()Lcom/google/maps/api/android/lib6/c/al;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->c:Lcom/google/maps/api/android/lib6/c/al;

    return-object v0
.end method

.method public final H()Landroid/graphics/Rect;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->h:Lcom/google/maps/api/android/lib6/c/ak;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/c/ak;->g()Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->f:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->d:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/aj;->o()V

    return-void
.end method

.method public final a(F)V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/google/maps/api/android/lib6/c/aj;->n:F

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/c/aj;->a(I)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(FF)V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/google/maps/api/android/lib6/c/aj;->k:F

    iput p2, p0, Lcom/google/maps/api/android/lib6/c/aj;->l:F

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/c/aj;->a(I)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/android/gms/b/l;)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->d:Lcom/google/maps/api/android/lib6/c/db;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/aj;->j:Lcom/google/maps/api/android/lib6/c/ct;

    invoke-virtual {v0, v1}, Lcom/google/maps/api/android/lib6/c/db;->c(Lcom/google/maps/api/android/lib6/c/ct;)V

    invoke-static {p1}, Lcom/google/maps/api/android/lib6/c/ct;->a(Lcom/google/android/gms/b/l;)Lcom/google/maps/api/android/lib6/c/ct;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->j:Lcom/google/maps/api/android/lib6/c/ct;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->d:Lcom/google/maps/api/android/lib6/c/db;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/aj;->j:Lcom/google/maps/api/android/lib6/c/ct;

    invoke-virtual {v0, v1}, Lcom/google/maps/api/android/lib6/c/db;->a(Lcom/google/maps/api/android/lib6/c/ct;)V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/c/aj;->a(I)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLng;)V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->f:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->e:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    invoke-virtual {p0, p1}, Lcom/google/maps/api/android/lib6/c/aj;->b(Lcom/google/android/gms/maps/model/LatLng;)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/c/aj;->a(I)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/model/internal/BitmapDescriptorParcelable;)V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->e:Lcom/google/maps/api/android/lib6/c/cs;

    invoke-virtual {v0, p1}, Lcom/google/maps/api/android/lib6/c/cs;->a(Lcom/google/android/gms/maps/model/internal/BitmapDescriptorParcelable;)Lcom/google/android/gms/b/l;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/maps/api/android/lib6/c/aj;->a(Lcom/google/android/gms/b/l;)V

    return-void
.end method

.method public final a(Lcom/google/maps/api/android/lib6/c/ak;)V
    .locals 0

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/c/aj;->h:Lcom/google/maps/api/android/lib6/c/ak;

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/maps/api/android/lib6/c/aj;->q:Ljava/lang/String;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v0, 0x80

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/c/aj;->a(I)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iput-boolean p1, p0, Lcom/google/maps/api/android/lib6/c/aj;->o:Z

    const/16 v0, 0x20

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/c/aj;->a(I)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/model/internal/s;)Z
    .locals 1

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b(F)V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/google/maps/api/android/lib6/c/aj;->v:F

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v0, 0x400

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/c/aj;->a(I)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(FF)V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/google/maps/api/android/lib6/c/aj;->s:F

    iput p2, p0, Lcom/google/maps/api/android/lib6/c/aj;->t:F

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v0, 0x200

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/c/aj;->a(I)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lcom/google/android/gms/maps/model/LatLng;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/maps/api/android/lib6/c/aj;->i:Lcom/google/android/gms/maps/model/LatLng;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/c/aj;->r:Ljava/lang/String;

    const/16 v0, 0x100

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/c/aj;->a(I)V

    return-void
.end method

.method public final b(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/google/maps/api/android/lib6/c/aj;->p:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v0, 0x40

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/c/aj;->a(I)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()Lcom/google/android/gms/maps/model/LatLng;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/aj;->p()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    return-object v0
.end method

.method public final c(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/google/maps/api/android/lib6/c/aj;->m:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/c/aj;->a(I)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/aj;->z()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Z
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/aj;->B()Z

    move-result v0

    return v0
.end method

.method public final g()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->u:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->f:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->p:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->c:Lcom/google/maps/api/android/lib6/c/al;

    invoke-virtual {v0, p0}, Lcom/google/maps/api/android/lib6/c/al;->c(Lcom/google/maps/api/android/lib6/c/aj;)V

    :cond_0
    return-void
.end method

.method public final h()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->u:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/aj;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->f:Lcom/google/maps/api/android/lib6/c/cd;

    sget-object v1, Lcom/google/maps/api/android/lib6/c/cf;->q:Lcom/google/maps/api/android/lib6/c/cf;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/c/cd;->b(Lcom/google/maps/api/android/lib6/c/cf;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->c:Lcom/google/maps/api/android/lib6/c/al;

    invoke-virtual {v0, p0}, Lcom/google/maps/api/android/lib6/c/al;->d(Lcom/google/maps/api/android/lib6/c/aj;)V

    :cond_0
    return-void
.end method

.method public final i()Z
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->u:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->c:Lcom/google/maps/api/android/lib6/c/al;

    invoke-virtual {v0, p0}, Lcom/google/maps/api/android/lib6/c/al;->b(Lcom/google/maps/api/android/lib6/c/aj;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Z
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/aj;->C()Z

    move-result v0

    return v0
.end method

.method public final k()I
    .locals 1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public final l()Z
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/aj;->D()Z

    move-result v0

    return v0
.end method

.method public final m()F
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/aj;->E()F

    move-result v0

    return v0
.end method

.method public final n()F
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/aj;->F()F

    move-result v0

    return v0
.end method

.method final o()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->u:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/aj;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/aj;->h()V

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->u:Z

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->d:Lcom/google/maps/api/android/lib6/c/db;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/aj;->j:Lcom/google/maps/api/android/lib6/c/ct;

    invoke-virtual {v0, v1}, Lcom/google/maps/api/android/lib6/c/db;->c(Lcom/google/maps/api/android/lib6/c/ct;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->c:Lcom/google/maps/api/android/lib6/c/al;

    invoke-virtual {v0, p0}, Lcom/google/maps/api/android/lib6/c/al;->a(Lcom/google/maps/api/android/lib6/c/aj;)V

    goto :goto_0
.end method

.method public final declared-synchronized p()Lcom/google/android/gms/maps/model/LatLng;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->i:Lcom/google/android/gms/maps/model/LatLng;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized q()Landroid/graphics/Bitmap;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->d:Lcom/google/maps/api/android/lib6/c/db;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/c/aj;->j:Lcom/google/maps/api/android/lib6/c/ct;

    invoke-virtual {v0, v1}, Lcom/google/maps/api/android/lib6/c/db;->b(Lcom/google/maps/api/android/lib6/c/ct;)Lcom/google/maps/api/android/lib6/c/dc;

    move-result-object v0

    iget-object v0, v0, Lcom/google/maps/api/android/lib6/c/dc;->b:Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final r()F
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/aj;->s()F

    move-result v0

    return v0
.end method

.method public final declared-synchronized s()F
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->k:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final t()F
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/aj;->u()F

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final declared-synchronized u()F
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->l:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final v()F
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/aj;->w()F

    move-result v0

    return v0
.end method

.method public final declared-synchronized w()F
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->s:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final x()F
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->g:Lcom/google/maps/api/android/lib6/c/bz;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/bz;->a()V

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/c/aj;->y()F

    move-result v0

    return v0
.end method

.method public final declared-synchronized y()F
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->t:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized z()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/c/aj;->q:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
