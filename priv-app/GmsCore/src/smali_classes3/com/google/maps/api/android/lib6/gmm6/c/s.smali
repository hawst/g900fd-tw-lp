.class public final Lcom/google/maps/api/android/lib6/gmm6/c/s;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/maps/api/android/lib6/c/aq;


# instance fields
.field private final a:Lcom/google/maps/api/android/lib6/gmm6/c/aq;

.field private final b:Landroid/content/res/Resources;

.field private c:Lcom/google/maps/api/android/lib6/gmm6/o/c;


# direct methods
.method public constructor <init>(Lcom/google/maps/api/android/lib6/gmm6/c/aq;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/k/a/cj;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/api/android/lib6/gmm6/c/aq;

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/s;->a:Lcom/google/maps/api/android/lib6/gmm6/c/aq;

    invoke-interface {p1}, Lcom/google/maps/api/android/lib6/gmm6/c/aq;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/s;->b:Landroid/content/res/Resources;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/s;->c:Lcom/google/maps/api/android/lib6/gmm6/o/c;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/s;->a:Lcom/google/maps/api/android/lib6/gmm6/c/aq;

    invoke-interface {v0}, Lcom/google/maps/api/android/lib6/gmm6/c/aq;->s()Lcom/google/maps/api/android/lib6/gmm6/o/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/s;->c:Lcom/google/maps/api/android/lib6/gmm6/o/c;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/s;->c:Lcom/google/maps/api/android/lib6/gmm6/o/c;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/s;->b:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/gms/maps/aa;->l:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/gmm6/c/s;->b:Landroid/content/res/Resources;

    sget v3, Lcom/google/android/gms/maps/ac;->b:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iget-object v3, p0, Lcom/google/maps/api/android/lib6/gmm6/c/s;->b:Landroid/content/res/Resources;

    sget v4, Lcom/google/android/gms/maps/ac;->a:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/maps/api/android/lib6/gmm6/o/c;->a(FII)V

    :cond_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/s;->a:Lcom/google/maps/api/android/lib6/gmm6/c/aq;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/s;->c:Lcom/google/maps/api/android/lib6/gmm6/o/c;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/gmm6/c/aq;->a(Lcom/google/maps/api/android/lib6/gmm6/o/bo;)V

    return-void
.end method

.method public final a(Landroid/location/Location;)V
    .locals 5

    const/4 v4, 0x1

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/google/maps/api/android/lib6/gmm6/l/h;->a(DD)Lcom/google/maps/api/android/lib6/gmm6/l/h;

    move-result-object v0

    new-instance v1, Lcom/google/maps/api/android/lib6/gmm6/l/g;

    invoke-virtual {p1}, Landroid/location/Location;->getBearing()F

    move-result v2

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v3

    float-to-int v3, v3

    invoke-direct {v1, v0, v2, v3}, Lcom/google/maps/api/android/lib6/gmm6/l/g;-><init>(Lcom/google/maps/api/android/lib6/gmm6/l/h;FI)V

    iput-object v0, v1, Lcom/google/maps/api/android/lib6/gmm6/l/g;->d:Lcom/google/maps/api/android/lib6/gmm6/l/h;

    invoke-virtual {p1}, Landroid/location/Location;->hasBearing()Z

    move-result v0

    iput-boolean v0, v1, Lcom/google/maps/api/android/lib6/gmm6/l/g;->e:Z

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/s;->c:Lcom/google/maps/api/android/lib6/gmm6/o/c;

    invoke-virtual {v0, v1}, Lcom/google/maps/api/android/lib6/gmm6/o/c;->a(Lcom/google/maps/api/android/lib6/gmm6/l/g;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/s;->a:Lcom/google/maps/api/android/lib6/gmm6/c/aq;

    invoke-interface {v0, v4, v4}, Lcom/google/maps/api/android/lib6/gmm6/c/aq;->a(ZZ)V

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/gmm6/c/s;->a:Lcom/google/maps/api/android/lib6/gmm6/c/aq;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/gmm6/c/s;->c:Lcom/google/maps/api/android/lib6/gmm6/o/c;

    invoke-interface {v0, v1}, Lcom/google/maps/api/android/lib6/gmm6/c/aq;->b(Lcom/google/maps/api/android/lib6/gmm6/o/bo;)V

    return-void
.end method
