.class Lcom/google/maps/api/android/lib6/d/a;
.super Ljava/lang/Object;


# static fields
.field private static final a:Lcom/android/volley/w;


# instance fields
.field private final b:Lcom/google/maps/api/android/lib6/d/f;

.field private final c:Landroid/widget/TextView;

.field private final d:Lcom/android/volley/s;

.field private final e:Lcom/google/maps/api/android/lib6/d/g;

.field private final f:Ljava/util/Calendar;

.field private final g:Z

.field private h:I

.field private i:Lcom/google/p/a/b/b/f;

.field private j:Ljava/lang/String;

.field private k:Lcom/android/volley/toolbox/u;

.field private l:Lcom/android/volley/toolbox/ab;

.field private m:Lcom/google/maps/api/android/lib6/d/p;

.field private n:Landroid/graphics/Bitmap;

.field private o:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/maps/api/android/lib6/d/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    new-instance v0, Lcom/google/maps/api/android/lib6/d/b;

    invoke-direct {v0}, Lcom/google/maps/api/android/lib6/d/b;-><init>()V

    sput-object v0, Lcom/google/maps/api/android/lib6/d/a;->a:Lcom/android/volley/w;

    return-void
.end method

.method constructor <init>(Lcom/google/maps/api/android/lib6/d/f;Landroid/widget/TextView;Lcom/android/volley/s;Lcom/google/maps/api/android/lib6/d/g;Ljava/util/Calendar;Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/maps/api/android/lib6/d/a;->h:I

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/d/a;->b:Lcom/google/maps/api/android/lib6/d/f;

    iput-object p2, p0, Lcom/google/maps/api/android/lib6/d/a;->c:Landroid/widget/TextView;

    iput-object p3, p0, Lcom/google/maps/api/android/lib6/d/a;->d:Lcom/android/volley/s;

    iput-object p4, p0, Lcom/google/maps/api/android/lib6/d/a;->e:Lcom/google/maps/api/android/lib6/d/g;

    iput-object p5, p0, Lcom/google/maps/api/android/lib6/d/a;->f:Ljava/util/Calendar;

    iput-boolean p6, p0, Lcom/google/maps/api/android/lib6/d/a;->g:Z

    new-instance v0, Lcom/google/maps/api/android/lib6/d/c;

    invoke-direct {v0, p0}, Lcom/google/maps/api/android/lib6/d/c;-><init>(Lcom/google/maps/api/android/lib6/d/a;)V

    invoke-static {v0}, Lcom/google/maps/api/android/lib6/gmm6/d/g;->a(Lcom/google/maps/api/android/lib6/gmm6/d/h;)V

    return-void
.end method

.method private static a(ID)I
    .locals 5

    int-to-double v0, p0

    div-double/2addr v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    const-wide/high16 v2, 0x40a0000000000000L    # 2048.0

    div-double/2addr v2, p1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/google/maps/api/android/lib6/d/a;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/d/a;->n:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic a(Lcom/google/maps/api/android/lib6/d/a;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/d/a;->o:Ljava/lang/String;

    return-object p1
.end method

.method private a(Lcom/google/p/a/b/b/f;)Ljava/lang/String;
    .locals 3

    :try_start_0
    invoke-virtual {p1}, Lcom/google/p/a/b/b/f;->e()[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/d/a;->j:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "bpb="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/google/k/e/n;->a()Lcom/google/k/e/n;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/k/e/n;->a([B)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method static synthetic a(Lcom/google/maps/api/android/lib6/d/a;)V
    .locals 3

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/a;->n:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/d/a;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/a;->o:Ljava/lang/String;

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/a;->b:Lcom/google/maps/api/android/lib6/d/f;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/d/a;->n:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/d/a;->m:Lcom/google/maps/api/android/lib6/d/p;

    invoke-virtual {v0, v1, v2}, Lcom/google/maps/api/android/lib6/d/f;->a(Landroid/graphics/Bitmap;Lcom/google/maps/api/android/lib6/d/p;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/a;->o:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/a;->c:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/d/a;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-void
.end method

.method private static a(Lcom/google/p/a/b/b/f;I)V
    .locals 6

    const/16 v5, 0xc

    const/4 v4, 0x4

    const/4 v2, 0x0

    const/4 v1, 0x1

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "Shouldn\'t fetch for MAP_TYPE_NONE"

    invoke-static {v0, v3}, Lcom/google/k/a/cj;->a(ZLjava/lang/Object;)V

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/p/a/b/b/f;->f(I)Lcom/google/p/a/b/b/f;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/p/a/b/b/f;

    sget-object v3, Lcom/google/maps/b/a/g;->a:Lcom/google/p/a/b/b/h;

    invoke-direct {v0, v3}, Lcom/google/p/a/b/b/f;-><init>(Lcom/google/p/a/b/b/h;)V

    :cond_0
    packed-switch p1, :pswitch_data_0

    invoke-static {p0, v2}, Lcom/google/maps/api/android/lib6/d/a;->b(Lcom/google/p/a/b/b/f;I)V

    :goto_1
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :pswitch_0
    invoke-static {p0, v1}, Lcom/google/maps/api/android/lib6/d/a;->b(Lcom/google/p/a/b/b/f;I)V

    goto :goto_1

    :pswitch_1
    invoke-static {p0, v1}, Lcom/google/maps/api/android/lib6/d/a;->b(Lcom/google/p/a/b/b/f;I)V

    invoke-static {p0, v2}, Lcom/google/maps/api/android/lib6/d/a;->b(Lcom/google/p/a/b/b/f;I)V

    new-instance v2, Lcom/google/p/a/b/b/f;

    sget-object v3, Lcom/google/maps/b/a/j;->a:Lcom/google/p/a/b/b/h;

    invoke-direct {v2, v3}, Lcom/google/p/a/b/b/f;-><init>(Lcom/google/p/a/b/b/h;)V

    invoke-virtual {v2, v1, v4}, Lcom/google/p/a/b/b/f;->f(II)Lcom/google/p/a/b/b/f;

    invoke-virtual {v0, v5, v2}, Lcom/google/p/a/b/b/f;->a(ILcom/google/p/a/b/b/f;)V

    goto :goto_1

    :pswitch_2
    invoke-static {p0, v4}, Lcom/google/maps/api/android/lib6/d/a;->b(Lcom/google/p/a/b/b/f;I)V

    invoke-static {p0, v2}, Lcom/google/maps/api/android/lib6/d/a;->b(Lcom/google/p/a/b/b/f;I)V

    new-instance v2, Lcom/google/p/a/b/b/f;

    sget-object v3, Lcom/google/maps/b/a/j;->a:Lcom/google/p/a/b/b/h;

    invoke-direct {v2, v3}, Lcom/google/p/a/b/b/f;-><init>(Lcom/google/p/a/b/b/h;)V

    const/4 v3, 0x5

    invoke-virtual {v2, v1, v3}, Lcom/google/p/a/b/b/f;->f(II)Lcom/google/p/a/b/b/f;

    invoke-virtual {v0, v5, v2}, Lcom/google/p/a/b/b/f;->a(ILcom/google/p/a/b/b/f;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Lcom/google/p/a/b/b/f;Lcom/google/maps/api/android/lib6/d/p;)V
    .locals 8

    const/4 v7, 0x2

    const/4 v6, 0x1

    iget-object v0, p1, Lcom/google/maps/api/android/lib6/d/p;->e:Lcom/google/android/gms/maps/model/CameraPosition;

    iget v0, v0, Lcom/google/android/gms/maps/model/CameraPosition;->b:F

    float-to-int v0, v0

    new-instance v1, Lcom/google/p/a/b/b/f;

    sget-object v2, Lcom/google/maps/b/a/i;->a:Lcom/google/p/a/b/b/h;

    invoke-direct {v1, v2}, Lcom/google/p/a/b/b/f;-><init>(Lcom/google/p/a/b/b/h;)V

    new-instance v2, Lcom/google/p/a/b/b/f;

    sget-object v3, Lcom/google/maps/b/a/i;->b:Lcom/google/p/a/b/b/h;

    invoke-direct {v2, v3}, Lcom/google/p/a/b/b/f;-><init>(Lcom/google/p/a/b/b/h;)V

    invoke-virtual {v2, v7, v0}, Lcom/google/p/a/b/b/f;->f(II)Lcom/google/p/a/b/b/f;

    new-instance v0, Lcom/google/p/a/b/b/f;

    sget-object v3, Lcom/google/maps/b/a/i;->c:Lcom/google/p/a/b/b/h;

    invoke-direct {v0, v3}, Lcom/google/p/a/b/b/f;-><init>(Lcom/google/p/a/b/b/h;)V

    iget v3, p1, Lcom/google/maps/api/android/lib6/d/p;->f:I

    iget-wide v4, p1, Lcom/google/maps/api/android/lib6/d/p;->h:D

    invoke-static {v3, v4, v5}, Lcom/google/maps/api/android/lib6/d/a;->a(ID)I

    move-result v3

    invoke-virtual {v0, v6, v3}, Lcom/google/p/a/b/b/f;->f(II)Lcom/google/p/a/b/b/f;

    iget v3, p1, Lcom/google/maps/api/android/lib6/d/p;->g:I

    iget-wide v4, p1, Lcom/google/maps/api/android/lib6/d/p;->h:D

    invoke-static {v3, v4, v5}, Lcom/google/maps/api/android/lib6/d/a;->a(ID)I

    move-result v3

    invoke-virtual {v0, v7, v3}, Lcom/google/p/a/b/b/f;->f(II)Lcom/google/p/a/b/b/f;

    invoke-virtual {v2, v6, v0}, Lcom/google/p/a/b/b/f;->b(ILcom/google/p/a/b/b/f;)Lcom/google/p/a/b/b/f;

    new-instance v3, Lcom/google/p/a/b/b/f;

    sget-object v0, Lcom/google/o/a/a/a/e;->a:Lcom/google/p/a/b/b/h;

    invoke-direct {v3, v0}, Lcom/google/p/a/b/b/f;-><init>(Lcom/google/p/a/b/b/h;)V

    iget v0, p1, Lcom/google/maps/api/android/lib6/d/p;->a:I

    iget v4, p1, Lcom/google/maps/api/android/lib6/d/p;->c:I

    if-ne v0, v4, :cond_0

    iget v0, p1, Lcom/google/maps/api/android/lib6/d/p;->b:I

    iget v4, p1, Lcom/google/maps/api/android/lib6/d/p;->d:I

    if-ne v0, v4, :cond_0

    iget-object v0, p1, Lcom/google/maps/api/android/lib6/d/p;->e:Lcom/google/android/gms/maps/model/CameraPosition;

    iget-object v0, v0, Lcom/google/android/gms/maps/model/CameraPosition;->a:Lcom/google/android/gms/maps/model/LatLng;

    :goto_0
    iget-wide v4, v0, Lcom/google/android/gms/maps/model/LatLng;->a:D

    invoke-static {v4, v5}, Lcom/google/maps/api/android/lib6/gmm6/c/f;->b(D)I

    move-result v4

    invoke-virtual {v3, v6, v4}, Lcom/google/p/a/b/b/f;->f(II)Lcom/google/p/a/b/b/f;

    iget-wide v4, v0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-static {v4, v5}, Lcom/google/maps/api/android/lib6/gmm6/c/f;->b(D)I

    move-result v0

    invoke-virtual {v3, v7, v0}, Lcom/google/p/a/b/b/f;->f(II)Lcom/google/p/a/b/b/f;

    const/4 v0, 0x3

    invoke-virtual {v2, v0, v3}, Lcom/google/p/a/b/b/f;->b(ILcom/google/p/a/b/b/f;)Lcom/google/p/a/b/b/f;

    const/4 v0, 0x4

    invoke-virtual {v1, v0, v2}, Lcom/google/p/a/b/b/f;->b(ILcom/google/p/a/b/b/f;)Lcom/google/p/a/b/b/f;

    invoke-virtual {p0, v6, v1}, Lcom/google/p/a/b/b/f;->a(ILcom/google/p/a/b/b/f;)V

    return-void

    :cond_0
    new-instance v0, Landroid/graphics/Point;

    iget v4, p1, Lcom/google/maps/api/android/lib6/d/p;->f:I

    div-int/lit8 v4, v4, 0x2

    iget v5, p1, Lcom/google/maps/api/android/lib6/d/p;->g:I

    div-int/lit8 v5, v5, 0x2

    invoke-direct {v0, v4, v5}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {p1, v0}, Lcom/google/maps/api/android/lib6/d/p;->a(Landroid/graphics/Point;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    goto :goto_0
.end method

.method private static b(Lcom/google/p/a/b/b/f;)Lcom/google/p/a/b/b/f;
    .locals 3

    const/4 v2, 0x5

    :try_start_0
    invoke-static {p0}, Lcom/google/p/a/b/b/j;->d(Lcom/google/p/a/b/b/f;)Lcom/google/p/a/b/b/f;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/google/p/a/b/b/f;->e(II)V

    const/4 v1, 0x4

    invoke-virtual {v0, v1, v2}, Lcom/google/p/a/b/b/f;->f(II)Lcom/google/p/a/b/b/f;

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private b()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/a;->k:Lcom/android/volley/toolbox/u;

    invoke-virtual {v0}, Lcom/android/volley/toolbox/u;->g()V

    iput-object v1, p0, Lcom/google/maps/api/android/lib6/d/a;->k:Lcom/android/volley/toolbox/u;

    iput-object v1, p0, Lcom/google/maps/api/android/lib6/d/a;->n:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/a;->l:Lcom/android/volley/toolbox/ab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/a;->l:Lcom/android/volley/toolbox/ab;

    invoke-virtual {v0}, Lcom/android/volley/toolbox/ab;->g()V

    iput-object v1, p0, Lcom/google/maps/api/android/lib6/d/a;->l:Lcom/android/volley/toolbox/ab;

    iput-object v1, p0, Lcom/google/maps/api/android/lib6/d/a;->o:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method private static b(Lcom/google/p/a/b/b/f;I)V
    .locals 3

    new-instance v0, Lcom/google/p/a/b/b/f;

    sget-object v1, Lcom/google/maps/b/a/b;->a:Lcom/google/p/a/b/b/h;

    invoke-direct {v0, v1}, Lcom/google/p/a/b/b/f;-><init>(Lcom/google/p/a/b/b/h;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lcom/google/p/a/b/b/f;->f(II)Lcom/google/p/a/b/b/f;

    const/4 v1, 0x3

    const v2, 0xf423f

    invoke-virtual {v0, v1, v2}, Lcom/google/p/a/b/b/f;->f(II)Lcom/google/p/a/b/b/f;

    const/4 v1, 0x2

    invoke-virtual {p0, v1, v0}, Lcom/google/p/a/b/b/f;->a(ILcom/google/p/a/b/b/f;)V

    return-void
.end method


# virtual methods
.method final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const-string v0, "["

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "]"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\""

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\u00a9"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/d/a;->f:Ljava/util/Calendar;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-static {v2}, Lcom/google/k/a/ca;->a(Ljava/lang/String;)Lcom/google/k/a/ca;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/k/a/ca;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final a()V
    .locals 12

    const/4 v0, 0x1

    const/4 v10, 0x0

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/d/a;->e:Lcom/google/maps/api/android/lib6/d/g;

    invoke-virtual {v1}, Lcom/google/maps/api/android/lib6/d/g;->b()Lcom/google/maps/api/android/lib6/d/p;

    move-result-object v9

    iget v1, v9, Lcom/google/maps/api/android/lib6/d/p;->f:I

    if-lez v1, :cond_2

    iget v1, v9, Lcom/google/maps/api/android/lib6/d/p;->g:I

    if-lez v1, :cond_2

    move v1, v0

    :goto_0
    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/d/a;->i:Lcom/google/p/a/b/b/f;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/d/a;->j:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/k/a/db;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    :goto_1
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/a;->i:Lcom/google/p/a/b/b/f;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/google/p/a/b/b/f;->f(I)Lcom/google/p/a/b/b/f;

    move-result-object v11

    const/4 v0, 0x5

    invoke-virtual {v11, v0}, Lcom/google/p/a/b/b/f;->f(I)Lcom/google/p/a/b/b/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/p/a/b/b/f;->b()F

    move-result v4

    new-instance v0, Lcom/google/maps/api/android/lib6/d/p;

    iget-object v1, v9, Lcom/google/maps/api/android/lib6/d/p;->e:Lcom/google/android/gms/maps/model/CameraPosition;

    iget v2, v9, Lcom/google/maps/api/android/lib6/d/p;->f:I

    iget v3, v9, Lcom/google/maps/api/android/lib6/d/p;->g:I

    float-to-double v4, v4

    iget v6, v9, Lcom/google/maps/api/android/lib6/d/p;->a:I

    iget v7, v9, Lcom/google/maps/api/android/lib6/d/p;->b:I

    iget v8, v9, Lcom/google/maps/api/android/lib6/d/p;->c:I

    iget v9, v9, Lcom/google/maps/api/android/lib6/d/p;->d:I

    invoke-direct/range {v0 .. v9}, Lcom/google/maps/api/android/lib6/d/p;-><init>(Lcom/google/android/gms/maps/model/CameraPosition;IIDIIII)V

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/d/a;->m:Lcom/google/maps/api/android/lib6/d/p;

    iget v0, p0, Lcom/google/maps/api/android/lib6/d/a;->h:I

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/a;->k:Lcom/android/volley/toolbox/u;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/maps/api/android/lib6/d/a;->b()V

    :cond_0
    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/a;->b:Lcom/google/maps/api/android/lib6/d/f;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/maps/api/android/lib6/d/a;->m:Lcom/google/maps/api/android/lib6/d/p;

    invoke-virtual {v0, v1, v2}, Lcom/google/maps/api/android/lib6/d/f;->a(Landroid/graphics/Bitmap;Lcom/google/maps/api/android/lib6/d/p;)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/a;->c:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_2
    return-void

    :cond_2
    move v1, v10

    goto :goto_0

    :cond_3
    move v0, v10

    goto :goto_1

    :cond_4
    :try_start_0
    invoke-static {v11}, Lcom/google/p/a/b/b/j;->d(Lcom/google/p/a/b/b/f;)Lcom/google/p/a/b/b/f;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/d/a;->m:Lcom/google/maps/api/android/lib6/d/p;

    invoke-static {v0, v1}, Lcom/google/maps/api/android/lib6/d/a;->a(Lcom/google/p/a/b/b/f;Lcom/google/maps/api/android/lib6/d/p;)V

    iget v1, p0, Lcom/google/maps/api/android/lib6/d/a;->h:I

    invoke-static {v0, v1}, Lcom/google/maps/api/android/lib6/d/a;->a(Lcom/google/p/a/b/b/f;I)V

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/d/a;->a(Lcom/google/p/a/b/b/f;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/a;->k:Lcom/android/volley/toolbox/u;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/a;->k:Lcom/android/volley/toolbox/u;

    invoke-virtual {v0}, Lcom/android/volley/toolbox/u;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/maps/api/android/lib6/d/a;->b()V

    :cond_5
    new-instance v2, Lcom/google/maps/api/android/lib6/d/d;

    invoke-direct {v2, p0}, Lcom/google/maps/api/android/lib6/d/d;-><init>(Lcom/google/maps/api/android/lib6/d/a;)V

    new-instance v0, Lcom/android/volley/toolbox/u;

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    sget-object v6, Lcom/google/maps/api/android/lib6/d/a;->a:Lcom/android/volley/w;

    move v3, v10

    move v4, v10

    invoke-direct/range {v0 .. v6}, Lcom/android/volley/toolbox/u;-><init>(Ljava/lang/String;Lcom/android/volley/x;IILandroid/graphics/Bitmap$Config;Lcom/android/volley/w;)V

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/d/a;->d:Lcom/android/volley/s;

    invoke-virtual {v1, v0}, Lcom/android/volley/s;->a(Lcom/android/volley/p;)Lcom/android/volley/p;

    move-result-object v0

    check-cast v0, Lcom/android/volley/toolbox/u;

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/d/a;->k:Lcom/android/volley/toolbox/u;

    iget-boolean v0, p0, Lcom/google/maps/api/android/lib6/d/a;->g:Z

    if-eqz v0, :cond_1

    invoke-static {v11}, Lcom/google/maps/api/android/lib6/d/a;->b(Lcom/google/p/a/b/b/f;)Lcom/google/p/a/b/b/f;

    move-result-object v0

    iget-object v1, p0, Lcom/google/maps/api/android/lib6/d/a;->m:Lcom/google/maps/api/android/lib6/d/p;

    invoke-static {v0, v1}, Lcom/google/maps/api/android/lib6/d/a;->a(Lcom/google/p/a/b/b/f;Lcom/google/maps/api/android/lib6/d/p;)V

    iget v1, p0, Lcom/google/maps/api/android/lib6/d/a;->h:I

    invoke-static {v0, v1}, Lcom/google/maps/api/android/lib6/d/a;->a(Lcom/google/p/a/b/b/f;I)V

    invoke-direct {p0, v0}, Lcom/google/maps/api/android/lib6/d/a;->a(Lcom/google/p/a/b/b/f;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/maps/api/android/lib6/d/e;

    invoke-direct {v1, p0}, Lcom/google/maps/api/android/lib6/d/e;-><init>(Lcom/google/maps/api/android/lib6/d/a;)V

    new-instance v2, Lcom/android/volley/toolbox/ab;

    sget-object v3, Lcom/google/maps/api/android/lib6/d/a;->a:Lcom/android/volley/w;

    invoke-direct {v2, v0, v1, v3, v10}, Lcom/android/volley/toolbox/ab;-><init>(Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;B)V

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/a;->d:Lcom/android/volley/s;

    invoke-virtual {v0, v2}, Lcom/android/volley/s;->a(Lcom/android/volley/p;)Lcom/android/volley/p;

    move-result-object v0

    check-cast v0, Lcom/android/volley/toolbox/ab;

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/d/a;->l:Lcom/android/volley/toolbox/ab;

    goto :goto_2

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method final a(I)V
    .locals 0

    iput p1, p0, Lcom/google/maps/api/android/lib6/d/a;->h:I

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/d/a;->a()V

    return-void
.end method

.method final a(Lcom/google/maps/api/android/lib6/gmm6/d/l;Lcom/google/p/a/b/b/f;)V
    .locals 1

    iget-object v0, p1, Lcom/google/maps/api/android/lib6/gmm6/d/l;->f:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/k/a/db;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/google/maps/api/android/lib6/gmm6/d/l;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/d/a;->j:Ljava/lang/String;

    :goto_0
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/google/p/a/b/b/f;->f(I)Lcom/google/p/a/b/b/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/d/a;->i:Lcom/google/p/a/b/b/f;

    invoke-virtual {p0}, Lcom/google/maps/api/android/lib6/d/a;->a()V

    return-void

    :cond_0
    const-string v0, "https://www.google.com/maps/vt/"

    iput-object v0, p0, Lcom/google/maps/api/android/lib6/d/a;->j:Ljava/lang/String;

    goto :goto_0
.end method
