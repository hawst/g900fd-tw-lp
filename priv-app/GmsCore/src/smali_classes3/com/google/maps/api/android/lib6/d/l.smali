.class public final Lcom/google/maps/api/android/lib6/d/l;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;


# instance fields
.field private a:Lcom/google/maps/api/android/lib6/d/i;

.field private b:Lcom/google/maps/api/android/lib6/d/m;


# direct methods
.method constructor <init>(Lcom/google/maps/api/android/lib6/d/i;Lcom/google/maps/api/android/lib6/d/m;)V
    .locals 0

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    iput-object p1, p0, Lcom/google/maps/api/android/lib6/d/l;->a:Lcom/google/maps/api/android/lib6/d/i;

    iput-object p2, p0, Lcom/google/maps/api/android/lib6/d/l;->b:Lcom/google/maps/api/android/lib6/d/m;

    return-void
.end method


# virtual methods
.method public final onLongPress(Landroid/view/MotionEvent;)V
    .locals 1

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/l;->a:Lcom/google/maps/api/android/lib6/d/i;

    invoke-virtual {v0, p1}, Lcom/google/maps/api/android/lib6/d/i;->b(Landroid/view/MotionEvent;)Z

    return-void
.end method

.method public final onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 12

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x1

    iget-object v7, p0, Lcom/google/maps/api/android/lib6/d/l;->b:Lcom/google/maps/api/android/lib6/d/m;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    iget-object v0, v7, Lcom/google/maps/api/android/lib6/d/m;->e:Lcom/google/maps/api/android/lib6/c/aj;

    if-eqz v0, :cond_1

    iget v0, v7, Lcom/google/maps/api/android/lib6/d/m;->f:F

    cmpl-float v0, v8, v0

    if-ltz v0, :cond_1

    iget v0, v7, Lcom/google/maps/api/android/lib6/d/m;->f:F

    iget v1, v7, Lcom/google/maps/api/android/lib6/d/m;->h:F

    add-float/2addr v0, v1

    cmpg-float v0, v8, v0

    if-gtz v0, :cond_1

    iget v0, v7, Lcom/google/maps/api/android/lib6/d/m;->g:F

    cmpl-float v0, v9, v0

    if-ltz v0, :cond_1

    iget v0, v7, Lcom/google/maps/api/android/lib6/d/m;->g:F

    iget v1, v7, Lcom/google/maps/api/android/lib6/d/m;->i:F

    add-float/2addr v0, v1

    cmpg-float v0, v9, v0

    if-gtz v0, :cond_1

    iget-object v0, v7, Lcom/google/maps/api/android/lib6/d/m;->e:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->G()Lcom/google/maps/api/android/lib6/c/al;

    move-result-object v0

    iget-object v1, v7, Lcom/google/maps/api/android/lib6/d/m;->e:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0, v1}, Lcom/google/maps/api/android/lib6/c/al;->i(Lcom/google/maps/api/android/lib6/c/aj;)V

    move v0, v5

    :goto_0
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/maps/api/android/lib6/d/l;->a:Lcom/google/maps/api/android/lib6/d/i;

    invoke-virtual {v0, p1}, Lcom/google/maps/api/android/lib6/d/i;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    :cond_0
    return v0

    :cond_1
    const v3, 0x7149f2ca    # 1.0E30f

    iget-object v0, v7, Lcom/google/maps/api/android/lib6/d/m;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move-object v1, v4

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->j()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->H()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v11

    sub-float v11, v8, v11

    invoke-virtual {v2}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v2

    sub-float v2, v9, v2

    mul-float/2addr v11, v11

    mul-float/2addr v2, v2

    add-float/2addr v2, v11

    cmpg-float v11, v2, v3

    if-gez v11, :cond_9

    move v1, v2

    :goto_2
    move v3, v1

    move-object v1, v0

    goto :goto_1

    :cond_2
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/google/maps/api/android/lib6/c/aj;->j()Z

    move-result v0

    if-nez v0, :cond_5

    :cond_3
    move v0, v6

    :goto_3
    if-eqz v0, :cond_7

    invoke-virtual {v1}, Lcom/google/maps/api/android/lib6/c/aj;->g()V

    invoke-virtual {v1}, Lcom/google/maps/api/android/lib6/c/aj;->G()Lcom/google/maps/api/android/lib6/c/al;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/maps/api/android/lib6/c/al;->h(Lcom/google/maps/api/android/lib6/c/aj;)Z

    iget-object v0, v7, Lcom/google/maps/api/android/lib6/d/m;->e:Lcom/google/maps/api/android/lib6/c/aj;

    if-eqz v0, :cond_4

    iget-object v0, v7, Lcom/google/maps/api/android/lib6/d/m;->e:Lcom/google/maps/api/android/lib6/c/aj;

    if-eq v0, v1, :cond_4

    iget-object v0, v7, Lcom/google/maps/api/android/lib6/d/m;->e:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->h()V

    :cond_4
    iput-object v1, v7, Lcom/google/maps/api/android/lib6/d/m;->e:Lcom/google/maps/api/android/lib6/c/aj;

    iget-object v0, v7, Lcom/google/maps/api/android/lib6/d/m;->d:Lcom/google/maps/api/android/lib6/d/i;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/d/i;->invalidate()V

    move v0, v5

    goto :goto_0

    :cond_5
    invoke-virtual {v1}, Lcom/google/maps/api/android/lib6/c/aj;->H()Landroid/graphics/Rect;

    move-result-object v0

    new-instance v2, Landroid/graphics/Rect;

    iget v3, v0, Landroid/graphics/Rect;->left:I

    add-int/lit8 v3, v3, -0xa

    iget v10, v0, Landroid/graphics/Rect;->top:I

    add-int/lit8 v10, v10, -0xa

    iget v11, v0, Landroid/graphics/Rect;->right:I

    add-int/lit8 v11, v11, 0xa

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v0, v0, 0xa

    invoke-direct {v2, v3, v10, v11, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    iget v0, v2, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    cmpl-float v0, v8, v0

    if-ltz v0, :cond_6

    iget v0, v2, Landroid/graphics/Rect;->right:I

    int-to-float v0, v0

    cmpg-float v0, v8, v0

    if-gtz v0, :cond_6

    iget v0, v2, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    cmpl-float v0, v9, v0

    if-ltz v0, :cond_6

    iget v0, v2, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    cmpg-float v0, v9, v0

    if-gtz v0, :cond_6

    move v0, v5

    goto :goto_3

    :cond_6
    move v0, v6

    goto :goto_3

    :cond_7
    iget-object v0, v7, Lcom/google/maps/api/android/lib6/d/m;->e:Lcom/google/maps/api/android/lib6/c/aj;

    if-eqz v0, :cond_8

    iget-object v0, v7, Lcom/google/maps/api/android/lib6/d/m;->e:Lcom/google/maps/api/android/lib6/c/aj;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/c/aj;->h()V

    iput-object v4, v7, Lcom/google/maps/api/android/lib6/d/m;->e:Lcom/google/maps/api/android/lib6/c/aj;

    iget-object v0, v7, Lcom/google/maps/api/android/lib6/d/m;->d:Lcom/google/maps/api/android/lib6/d/i;

    invoke-virtual {v0}, Lcom/google/maps/api/android/lib6/d/i;->invalidate()V

    move v0, v5

    goto/16 :goto_0

    :cond_8
    move v0, v6

    goto/16 :goto_0

    :cond_9
    move-object v0, v1

    move v1, v3

    goto :goto_2
.end method
